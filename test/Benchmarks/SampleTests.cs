﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Engines;
using Newtonsoft.Json;
using Ruler.Rules;
using Ruler.Rules.Serialization;
using StorageConfigurator.Rules;

namespace Benchmarks
{
    [SimpleJob(RunStrategy.Monitoring, launchCount: 1, warmupCount: 0, targetCount: 50)]
    public class SampleTests
    {
        #region Private Fields

        private IPartSerializerSettings _partSerializerSettings;

        #endregion

        #region Constructor

        public SampleTests()
        {
            _partSerializerSettings = new DefaultPartSerializerSettings();
        }

        #endregion

        #region Setup

        [IterationSetup]
        public void IterationSetup()
        {
            // TODO: Include any logic here that should be executed before each iteration
            //  For example, instantiating a part and getting it in the state it should be before
            //  we actually do the thing we'd like to benchmark
        }

        #endregion

        #region Benchmarks

        [Benchmark]
        public string SerializeForWeb()
        {
            var part = Part.CreateRootPart<MyPart>();
            // Note that we want to return the json rather than just assign to a variable.
            //  Otherwise the serialization might be left out of the optimized build since
            //  it would not be used anywhere.
            //  http://benchmarkdotnet.org/Guides/GoodPractices.htm#avoid-dead-code-elimination
            return JsonConvert.SerializeObject(part, _partSerializerSettings.WebFullModel);
        }

        #endregion
    }
}
