﻿using Ruler.InvUtils;

namespace StorageConfigurator.Modeler.Tests
{
    public class InventorFixture
    {
        public InventorFixture()
        {
            this.HostApplication = InventorAppUtils.GetInventorInstance();
            this.InventorHost = new InventorApplicationHost(this.HostApplication);
        }

        public global::Inventor.Application HostApplication { get; }
        public IInventorHost InventorHost { get; }
    }
}
