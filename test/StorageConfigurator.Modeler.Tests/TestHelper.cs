﻿using System.IO;
using System.Reflection;
using Ruler.InvUtils;
using Ruler.Modeler;

namespace StorageConfigurator.Modeler.Tests
{
    public class TestHelper
    {
        public static string CreateFromJson(IInventorHost inventorHost, string jsonName)
        {
            var jsonFile = GetTestData(jsonName);
            var occurrence = Occurrence.FromJson(jsonFile);
            var result = occurrence.Render(inventorHost);
            return result.ComponentsByRefChain[occurrence.RefChain].Path;
        }

        public static string GetTestData(string filename)
        {
            string summary;
            var assembly = Assembly.GetExecutingAssembly();

            using (var reader = new StreamReader(assembly.GetManifestResourceStream($"StorageConfigurator.Modeler.Tests.TestData.{filename}.json")))
            {
                summary = reader.ReadToEnd().ToString();
            }

            return summary;
        }

        public static string GetTestDataFilePath(string filename, string subFolder = "")
        {
            return Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.Parent.FullName,
                "TestData", subFolder, filename);
        }

        public static string GetTestCadFilePath(global::Inventor.Application inventorApp, string filename, string subfolder = "TestFiles")
        {
            return Path.Combine(Path.GetDirectoryName(inventorApp.DesignProjectManager.ActiveDesignProject.FullFileName), subfolder, filename);
        }

        public static string GetSolutionItemFilePath(string filename, string subFolder = "")
        {
            return Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.Parent.Parent.Parent.FullName,
                subFolder, filename);
        }
    }
}
