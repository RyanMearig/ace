﻿using System.Collections.Generic;
using Inventor;
using Microsoft.Extensions.Configuration;
using Ruler.Drawings;
using Ruler.InvUtils;

using Xunit;

namespace StorageConfigurator.Modeler.Tests
{
    public class DrawingTests: IClassFixture<InventorFixture>
    {
        private readonly InventorFixture _fixture;
        private const string DefaultDrawingPath = @"C:\Temp\";
        private const string TestDataFolderName = @"TestData\";
        private const string DrawingXMLFolderName = @"DrawingXML\";
        private const string InventorFactoryFilesFolder = @"Factory Files\";
        private string _dataFolder = "ModelData" + ".";

        public DrawingTests(InventorFixture fixture)
        {
            _fixture = fixture;
        }

        #region Render Tests

        [Fact]
        public void RenderTest_FromJSON()
        {
            var member = TestHelper.CreateFromJson(_fixture.InventorHost, "Footplate_FP1K-A");

            Assert.True(System.IO.File.Exists(member));
        }

        [Fact]
        public void RenderFrameUprightTest_FromJSON()
        {
            var member = TestHelper.CreateFromJson(_fixture.InventorHost, "FrameUpright");

            Assert.True(System.IO.File.Exists(member));
        }

        [Theory]
        [InlineData("FP1K-A")]
        [InlineData("FP1K-B")]
        [InlineData("FP1K-C")]
        [InlineData("FP1K-D")]
        [InlineData("FP2K-A")]
        [InlineData("FP2K-B")]
        [InlineData("FP3-A")]
        [InlineData("FP3-B")]
        [InlineData("FP3-C")]
        [InlineData("FP3-D")]
        [InlineData("FP4-A")]
        [InlineData("FP4-B")]
        public void RenderFootplateTest_FromJSON(string type)
        {
            var member = TestHelper.CreateFromJson(_fixture.InventorHost, $"Footplate_{type}");

            Assert.True(System.IO.File.Exists(member));
        }

        [Fact]
        public void RenderShelfLoadBeamTest_FromJSON()
        {
            var member = TestHelper.CreateFromJson(_fixture.InventorHost, "ShelfLoadBeam");

            Assert.True(System.IO.File.Exists(member));
        }

        [Fact]
        public void RenderSimpleTest()
        {
            var xmlPath = TestHelper.GetSolutionItemFilePath("StorageSystem.xml", DrawingXMLFolderName);

            Assert.True(System.IO.File.Exists(xmlPath), "XML does not exist at " + xmlPath);

            var modelPath = TestHelper.CreateFromJson(_fixture.InventorHost, "SimpleStorageSystem");

            Assert.True(System.IO.File.Exists(modelPath), "Model does not exist at " + modelPath);
        }

        [Fact]
        public void RenderSlantLegSystemTest()
        {
            var xmlPath = TestHelper.GetSolutionItemFilePath("StorageSystem.xml", DrawingXMLFolderName);

            Assert.True(System.IO.File.Exists(xmlPath), "XML does not exist at " + xmlPath);

            var modelPath = TestHelper.CreateFromJson(_fixture.InventorHost, "SlantLegStorageSystem");

            Assert.True(System.IO.File.Exists(modelPath), "Model does not exist at " + modelPath);
        }

        [Fact]
        public void RenderBentLegSystemTest()
        {
            var xmlPath = TestHelper.GetSolutionItemFilePath("StorageSystem.xml", DrawingXMLFolderName);

            Assert.True(System.IO.File.Exists(xmlPath), "XML does not exist at " + xmlPath);

            var modelPath = TestHelper.CreateFromJson(_fixture.InventorHost, "BentLegStorageSystem");

            Assert.True(System.IO.File.Exists(modelPath), "Model does not exist at " + modelPath);
        }

        [Fact]
        public void RenderStandardOptionSystemTest()
        {
            var xmlPath = TestHelper.GetSolutionItemFilePath("StorageSystem.xml", DrawingXMLFolderName);

            Assert.True(System.IO.File.Exists(xmlPath), "XML does not exist at " + xmlPath);

            var modelPath = TestHelper.CreateFromJson(_fixture.InventorHost, "StandardOptionStorageSystem");

            Assert.True(System.IO.File.Exists(modelPath), "Model does not exist at " + modelPath);
        }

        #endregion

        #region Drawing Tests

        [Fact(Skip = "Manual Testing")]
        public void RenderAndCreateDwgTest()
        {
            //var xmlPath = TestHelper.GetSolutionItemFilePath("StorageSystem.xml", DrawingXMLFolderName);
            var xmlPath = TestHelper.GetSolutionItemFilePath("Upright.xml", DrawingXMLFolderName);

            Assert.True(System.IO.File.Exists(xmlPath), "XML does not exist at " + xmlPath);

            //var modelPath = @"C:\Projects\Advance\StorageConfigurator\Inventor Models\Designs\123_Frame Tie Test\123.StorageSystem\123.StorageSystem.iam";// TestHelper.CreateFromJson(_fixture.InventorHost, "SimpleStorageSystem");
            //var modelPath = @"C:\Projects\Advance\StorageConfigurator\Inventor Models\Designs\987_zzz\Storage System\Storage System.iam";
            var modelPath = @"C:\Projects\Advance\StorageConfigurator\Inventor Models\Designs\123_Frame Tie Test\123.StorageSystem\FrameUpright.7100f96cb645ce17ccdb2471820424d3.iam";
            
            Assert.True(System.IO.File.Exists(modelPath), "Model does not exist at " + modelPath);

            ComponentFactory.RegisterComponent("AdvanceRefAutoCADBlock", (p, e, o) => new StorageConfigurator.Drawings.AdvanceRefAutoCADBlockComponent(p, e, o));
            _fixture.HostApplication.Visible = false;
            _fixture.HostApplication.Documents.CloseAll();
            _fixture.HostApplication.OpenDocument(modelPath);
            string dwgPath = "";
            var eData = new Dictionary<string, object>()
            {
                {"name", "Name Proj"},
                {"drawingNumber", "123456"},
                {"advancedSalesName", "Mr. Sales Name"},
                {"advancedProjectManagerName", "Mr. PM"},
                {"jobsiteName", "Tank Connection Jobsite"},
                {"jobsiteAddress", "345 Knob Creek St"},
                {"jobsiteCity", "Jonesboro"},
                {"jobsiteState", "AR"},
                {"jobsiteZip", "72455"},
                {"customerName", "Yuengling"},
                {"customerAddress", "3 W Dr."},
                {"customerCity", "Pottsville"},
                {"customerState", "PA"},
                {"customerZip", "65412"},
                {"customerContactName", "Mr. Oscar Customer"},
                {"seismicEngName", "Quake, Shake, Bake"},
                {"seismicEngAddress", "5.9 Shake St."},
                {"seismicEngCity", "Earth"},
                {"seismicEngState", "FL"},
                {"seismicEngZip", "98745"},
                {"seismicEngContactName", "Mr. Quake"},
                {"seismicEngContactPhone", "879-124-3131"},
                {"seismicEngContactEmail", "support@qsb.com"}
            };
            try
            { 
                var compOptions = new DrawingComponentOptions() { ExtraData = eData };
                var dwg = DrawingFactory.CreateDrawing(_fixture.InventorHost, modelPath, xmlPath, compOptions);

                dwgPath = dwg.Render(new DrawingRenderOptions { CloseDrawing = false });
            }
            finally
            {
                _fixture.HostApplication.Visible = true;
                _fixture.HostApplication.UserInterfaceManager.UserInteractionDisabled = false;
            }

            Assert.True(System.IO.File.Exists(dwgPath));
        }

        [Fact(Skip = "Manual Testing")]
        public void RenderAndCreateFootplateInstallationDrawingTest()
        {
            var xmlPath = TestHelper.GetSolutionItemFilePath("SketchSymbolTest.xml", DrawingXMLFolderName);

            Assert.True(System.IO.File.Exists(xmlPath), "XML does not exist at " + xmlPath);

            var modelPath = TestHelper.CreateFromJson(_fixture.InventorHost, "SimpleStorageSystem");

            Assert.True(System.IO.File.Exists(modelPath), "Model does not exist at " + modelPath);

            ComponentFactory.RegisterComponent("AdvanceRefSketchSymbol", (p, e, o) => new StorageConfigurator.Drawings.AdvanceRefSketchSymbolComponent(p, e, o));
            //_fixture.HostApplication.Visible = false;
            _fixture.HostApplication.Documents.CloseAll();
            _fixture.HostApplication.OpenDocument(modelPath);
            string dwgPath = "";
            var eData = new Dictionary<string, object>()
            {
                {"name", "Name Proj"},
                {"drawingNumber", "123456"},
                {"advancedSalesName", "Mr. Sales Name"},
                {"advancedProjectManagerName", "Mr. PM"},
                {"jobsiteName", "Tank Connection Jobsite"},
                {"jobsiteAddress", "345 Knob Creek St"},
                {"jobsiteCity", "Jonesboro"},
                {"jobsiteState", "AR"},
                {"jobsiteZip", "72455"},
                {"customerName", "Yuengling"},
                {"customerAddress", "3 W Dr."},
                {"customerCity", "Pottsville"},
                {"customerState", "PA"},
                {"customerZip", "65412"},
                {"customerContactName", "Mr. Oscar Customer"},
                {"seismicEngName", "Quake, Shake, Bake"},
                {"seismicEngAddress", "5.9 Shake St."},
                {"seismicEngCity", "Earth"},
                {"seismicEngState", "FL"},
                {"seismicEngZip", "98745"},
                {"seismicEngContactName", "Mr. Quake"},
                {"seismicEngContactPhone", "879-124-3131"},
                {"seismicEngContactEmail", "support@qsb.com"}
            };
            try
            {
                var compOptions = new DrawingComponentOptions() { ExtraData = eData };
                var dwg = DrawingFactory.CreateDrawing(_fixture.InventorHost, modelPath, xmlPath, compOptions);

                dwgPath = dwg.Render(new DrawingRenderOptions { CloseDrawing = false, Visible = true });
            }
            finally
            {
                _fixture.HostApplication.Visible = true;
                _fixture.HostApplication.UserInterfaceManager.UserInteractionDisabled = false;
            }

            Assert.True(System.IO.File.Exists(dwgPath));
        }

        [Fact(Skip = "Manual Testing")]
        public void RenderAndCreateMiscInstallationDrawingTest()
        {
            var xmlPath = TestHelper.GetSolutionItemFilePath("AutoCADBlockRefTest.xml", DrawingXMLFolderName);

            Assert.True(System.IO.File.Exists(xmlPath), "XML does not exist at " + xmlPath);

            var modelPath = TestHelper.CreateFromJson(_fixture.InventorHost, "SimpleStorageSystem");

            Assert.True(System.IO.File.Exists(modelPath), "Model does not exist at " + modelPath);

            ComponentFactory.RegisterComponent("AdvanceRefAutoCADBlock", (p, e, o) => new StorageConfigurator.Drawings.AdvanceRefAutoCADBlockComponent(p, e, o));
            //_fixture.HostApplication.Visible = false;
            _fixture.HostApplication.Documents.CloseAll();
            _fixture.HostApplication.OpenDocument(modelPath);
            string dwgPath = "";
            var eData = new Dictionary<string, object>()
            {
                {"name", "Name Proj"},
                {"drawingNumber", "123456"},
                {"advancedSalesName", "Mr. Sales Name"},
                {"advancedProjectManagerName", "Mr. PM"},
                {"jobsiteName", "Tank Connection Jobsite"},
                {"jobsiteAddress", "345 Knob Creek St"},
                {"jobsiteCity", "Jonesboro"},
                {"jobsiteState", "AR"},
                {"jobsiteZip", "72455"},
                {"customerName", "Yuengling"},
                {"customerAddress", "3 W Dr."},
                {"customerCity", "Pottsville"},
                {"customerState", "PA"},
                {"customerZip", "65412"},
                {"customerContactName", "Mr. Oscar Customer"},
                {"seismicEngName", "Quake, Shake, Bake"},
                {"seismicEngAddress", "5.9 Shake St."},
                {"seismicEngCity", "Earth"},
                {"seismicEngState", "FL"},
                {"seismicEngZip", "98745"},
                {"seismicEngContactName", "Mr. Quake"},
                {"seismicEngContactPhone", "879-124-3131"},
                {"seismicEngContactEmail", "support@qsb.com"}
            };
            try
            {
                var compOptions = new DrawingComponentOptions() { ExtraData = eData };
                var dwg = DrawingFactory.CreateDrawing(_fixture.InventorHost, modelPath, xmlPath, compOptions);

                dwgPath = dwg.Render(new DrawingRenderOptions { CloseDrawing = false, Visible = true });
            }
            finally
            {
                _fixture.HostApplication.Visible = true;
                _fixture.HostApplication.UserInterfaceManager.UserInteractionDisabled = false;
            }

            Assert.True(System.IO.File.Exists(dwgPath));
        }

        [Fact]
        public void CreateDwgTest()
        {
            var xmlPath = TestHelper.GetSolutionItemFilePath("StorageSystem.xml", DrawingXMLFolderName);

            Assert.True(System.IO.File.Exists(xmlPath), "XML does not exist at " + xmlPath);

            //var modelPath = @"C:\Projects\Advance\Inventor Models\Designs\Projects\0001_Simple After\0001.StorageSystem\0001.StorageSystem.iam";
            //var modelPath = @"C:\Projects\Advance\Inventor Models\Designs\Projects\0001_Simple B2B\0001.StorageSystem\0001.StorageSystem.iam";
            var modelPath = @"C:\Projects\Advance\Inventor Models\Designs\Projects\0001_After Frame Improv\0001.StorageSystem\0001.StorageSystem.iam";

            Assert.True(System.IO.File.Exists(modelPath), "Model does not exist at " + modelPath);

            ComponentFactory.RegisterComponent("AdvanceRefAutoCADBlock", (p, e, o) => new StorageConfigurator.Drawings.AdvanceRefAutoCADBlockComponent(p, e, o));
            _fixture.HostApplication.Visible = false;
            _fixture.HostApplication.Documents.CloseAll();
            _fixture.HostApplication.OpenDocument(modelPath);
            string dwgPath = "";
            var eData = new Dictionary<string, object>()
            {
                {"name", "Name Proj"},
                {"drawingNumber", "123456"},
                {"advancedSalesName", "Mr. Sales Name"},
                {"advancedProjectManagerName", "Mr. PM"},
                {"jobsiteName", "Tank Connection Jobsite"},
                {"jobsiteAddress", "345 Knob Creek St"},
                {"jobsiteCity", "Jonesboro"},
                {"jobsiteState", "AR"},
                {"jobsiteZip", "72455"},
                {"customerName", "Yuengling"},
                {"customerAddress", "3 W Dr."},
                {"customerCity", "Pottsville"},
                {"customerState", "PA"},
                {"customerZip", "65412"},
                {"customerContactName", "Mr. Oscar Customer"},
                {"seismicEngName", "Quake, Shake, Bake"},
                {"seismicEngAddress", "5.9 Shake St."},
                {"seismicEngCity", "Earth"},
                {"seismicEngState", "FL"},
                {"seismicEngZip", "98745"},
                {"seismicEngContactName", "Mr. Quake"},
                {"seismicEngContactPhone", "879-124-3131"},
                {"seismicEngContactEmail", "support@qsb.com"}
            };
            try
            {
                var compOptions = new DrawingComponentOptions() { ExtraData = eData };
                var dwg = DrawingFactory.CreateDrawing(_fixture.InventorHost, modelPath, xmlPath, compOptions);

                dwgPath = dwg.Render(new DrawingRenderOptions { CloseDrawing = false });
            }
            finally
            {
                _fixture.HostApplication.Visible = true;
                _fixture.HostApplication.UserInterfaceManager.UserInteractionDisabled = false;
            }

            Assert.True(System.IO.File.Exists(dwgPath));
        }

        #endregion

        #region Helper Methods

        private static IConfiguration GetConfiguration()
        {
            return new ConfigurationBuilder()
                .SetBasePath(System.Environment.CurrentDirectory)
                .AddJsonFile("appsettings.json", optional: true)
                .Build();
        }

        #endregion
    }
}
