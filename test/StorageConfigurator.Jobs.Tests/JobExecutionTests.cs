﻿using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Ruler.Jobs;
using Xunit;

namespace StorageConfigurator.Jobs.Tests
{
    public class JobExecutionTests
    {
        private readonly IConfiguration _configuration;

        public JobExecutionTests()
        {
            _configuration = TestHelper.GetConfiguration();
        }

        [Fact(Skip = "Manual Testing")]
        [Trait("Category", "ManualOnly")]
        public async Task CanExecuteJob()
        {
            var json = "{}";// TestDataHelper.GetTestData("occurrence.json");
            var job = new JobQueueJob { Description = "Test" };

            job.Parameters.Add(ParameterNames.DrawingNumber, "1234");
            job.Parameters.Add(ParameterNames.LineItemName, "My Line Item");
            job.Parameters.Add(ParameterNames.OccurrenceJson, json);

            var settings = _configuration.GetSection("ModelerJob").Get<ModelerJobSettings>();
            var handler = new ModelerJobHandler(settings, GetLogger<ModelerJobHandler>());
            var result = await handler.HandleJobAsync(job);

            Assert.Equal(JobStatuses.Success, result.Status);
        }

        private static ILogger<T> GetLogger<T>()
        {
            var serviceProvider = new ServiceCollection()
                .AddLogging(logging =>
                {
                    logging.AddDebug();
                })
                .BuildServiceProvider();

            return serviceProvider.GetService<ILogger<T>>();
        }
    }
}
