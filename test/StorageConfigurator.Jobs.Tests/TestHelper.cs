﻿using System;
using System.IO;
using System.Reflection;
using Microsoft.Extensions.Configuration;

namespace StorageConfigurator.Jobs.Tests
{
    public class TestHelper
    {
        public static string GetTestData(string filename)
        {
            string summary;
            var assembly = Assembly.GetExecutingAssembly();

            using (var reader = new StreamReader(assembly.GetManifestResourceStream($"StorageConfigurator.Jobs.Tests.TestData.{filename}")))
            {
                summary = reader.ReadToEnd().ToString();
            }

            return summary;
        }

        public static IConfiguration GetConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Environment.CurrentDirectory)
                .AddJsonFile("appsettings.json")
                .AddEnvironmentVariables();

            return builder.Build();
        }
    }
}
