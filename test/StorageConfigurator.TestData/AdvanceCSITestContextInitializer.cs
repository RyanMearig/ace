﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Ruler.Configurator.Extensibility;
using StorageConfigurator.Data;

namespace StorageConfigurator.TestData
{
    public class AdvanceCSITestContextInitializer : IDatabaseInitializer
    {
        #region Private Fields

        private readonly AdvanceCSIContext _csiContext;
        private readonly ILogger _logger;

        #endregion

        #region Constructors

        public AdvanceCSITestContextInitializer(AdvanceCSIContext csiContext, ILogger<AdvanceCSITestContextInitializer> logger)
        {
            _csiContext = csiContext;
            _logger = logger;
        }

        #endregion

        #region IDatabaseInitializer Implementation

        public bool AllowReset => false;

        public Task ClearDataAsync()
        {
            throw new NotImplementedException();
        }

        public Task MigrateAsync()
        {
            return Task.CompletedTask;
        }

        public async Task UpdateDataAsync()
        {
            _logger.LogInformation($"Ensuring {nameof(AdvanceCSIContext)} created");
            _csiContext.Database.EnsureCreated();
            if (await _csiContext.CustomerData.AnyAsync()) return;
            await RebuildSchemaAsync("StorageConfigurator.TestData.TestData.Sql.CSI_TestSchema.sql");
            _logger.LogInformation($"Adding data to {nameof(AdvanceCSIContext)}");
            await RunScriptAsync("StorageConfigurator.TestData.TestData.Sql.CSI_TestData1.sql");
            await RunScriptAsync("StorageConfigurator.TestData.TestData.Sql.CSI_TestData1b.sql");
            await RunScriptAsync("StorageConfigurator.TestData.TestData.Sql.CSI_TestData2.sql");
            await RunScriptAsync("StorageConfigurator.TestData.TestData.Sql.CSI_TestData2b.sql");
            await RunScriptAsync("StorageConfigurator.TestData.TestData.Sql.CSI_TestData3.sql");
            await RunScriptAsync("StorageConfigurator.TestData.TestData.Sql.CSI_TestData3b.sql");
            await RunScriptAsync("StorageConfigurator.TestData.TestData.Sql.CSI_TestData4.sql");
            await RunScriptAsync("StorageConfigurator.TestData.TestData.Sql.CSI_TestData4b.sql");
            await RunScriptAsync("StorageConfigurator.TestData.TestData.Sql.CSI_VendTestData.sql");
        }

        #endregion

        #region Private Methods

        private async Task RebuildSchemaAsync(string scriptPath)
        {
            _logger.LogInformation($"Droppping tables in {nameof(AdvanceCSIContext)}");
            await this.DropTablesAsync();
            _logger.LogInformation($"Rebuilding {nameof(AdvanceCSIContext)} types");
            await RunScriptAsync("StorageConfigurator.TestData.TestData.Sql.CSI_TestTypes.sql");
            _logger.LogInformation($"Rebuilding {nameof(AdvanceCSIContext)} schema");
            await RunScriptAsync(scriptPath);
        }

        private async Task RunScriptAsync(string scriptPath)
        {
            var assembly = Assembly.GetExecutingAssembly();
            var sqlStreamReader = new StreamReader(assembly.GetManifestResourceStream(scriptPath));
            var sql = sqlStreamReader.ReadToEnd();

            _csiContext.Database.SetCommandTimeout(300);
            await _csiContext.Database.ExecuteSqlRawAsync(sql);
        }

        private async Task DropTablesAsync()
        {
            await _csiContext.Database.ExecuteSqlRawAsync("DROP TABLE custaddr_mst");
            await _csiContext.Database.ExecuteSqlRawAsync("DROP TABLE customer_mst");
        }

        #endregion
    }
}
