/****** Object:  UserDefinedDataType [dbo].[AbcCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AbcCodeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[AbsenceHoursType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AbsenceHoursType] FROM [decimal](5, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[AbsenceReasonCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AbsenceReasonCodeType] FROM [nvarchar](3) NULL
/****** Object:  UserDefinedDataType [dbo].[ACAReceiptIDType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ACAReceiptIDType] FROM [nvarchar](24) NULL
/****** Object:  UserDefinedDataType [dbo].[ACARecordIDType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ACARecordIDType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[ACASubmissionIDType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ACASubmissionIDType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[AccessAsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AccessAsType] FROM [nvarchar](50) NULL
/****** Object:  UserDefinedDataType [dbo].[AccountNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AccountNameType] FROM [nvarchar](32) NULL
/****** Object:  UserDefinedDataType [dbo].[AcctClassType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AcctClassType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[AcctFieldsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AcctFieldsType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[AcctFmtType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AcctFmtType] FROM [nvarchar](12) NULL
/****** Object:  UserDefinedDataType [dbo].[AcctLenType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AcctLenType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[AcctOnlyType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AcctOnlyType] FROM [nvarchar](12) NULL
/****** Object:  UserDefinedDataType [dbo].[AcctPosType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AcctPosType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[AcctType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AcctType] FROM [nvarchar](12) NULL
/****** Object:  UserDefinedDataType [dbo].[AcctTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AcctTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[AchIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AchIdType] FROM [decimal](10, 0) NULL
/****** Object:  UserDefinedDataType [dbo].[AchNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AchNameType] FROM [nvarchar](23) NULL
/****** Object:  UserDefinedDataType [dbo].[AchOriginIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AchOriginIdType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[AchTransitReferenceType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AchTransitReferenceType] FROM [nvarchar](2) NULL
/****** Object:  UserDefinedDataType [dbo].[AchTransitType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AchTransitType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[AckNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AckNumType] FROM [nvarchar](7) NULL
/****** Object:  UserDefinedDataType [dbo].[AckSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AckSeqType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[AckStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AckStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ActivityCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ActivityCodeType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[ActivitySeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ActivitySeqType] FROM [decimal](10, 0) NULL
/****** Object:  UserDefinedDataType [dbo].[AddressLineType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AddressLineType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[AddressType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AddressType] FROM [nvarchar](50) NULL
/****** Object:  UserDefinedDataType [dbo].[AdpCompanyCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AdpCompanyCodeType] FROM [nvarchar](4) NULL
/****** Object:  UserDefinedDataType [dbo].[AdpFileIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AdpFileIdType] FROM [nvarchar](2) NULL
/****** Object:  UserDefinedDataType [dbo].[AdPublicationType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AdPublicationType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[AdpVersionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AdpVersionType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[AdResponsesType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AdResponsesType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[AdSectionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AdSectionType] FROM [nvarchar](25) NULL
/****** Object:  UserDefinedDataType [dbo].[AgeClassEffectivityType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AgeClassEffectivityType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[AgeDaysType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AgeDaysType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[AgeDescType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AgeDescType] FROM [nvarchar](12) NULL
/****** Object:  UserDefinedDataType [dbo].[AlienNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AlienNumType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[AllocationBasisRateType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AllocationBasisRateType] FROM [decimal](9, 3) NULL
/****** Object:  UserDefinedDataType [dbo].[AllocationBasisTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AllocationBasisTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[AlphaKeyType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AlphaKeyType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[AlphaPrefixType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AlphaPrefixType] FROM [nvarchar](7) NULL
/****** Object:  UserDefinedDataType [dbo].[AmountType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AmountType] FROM [decimal](23, 8) NULL
/****** Object:  UserDefinedDataType [dbo].[AmtTotType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AmtTotType] FROM [decimal](24, 8) NULL
/****** Object:  UserDefinedDataType [dbo].[AnnouncementIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AnnouncementIdType] FROM [decimal](11, 0) NULL
/****** Object:  UserDefinedDataType [dbo].[AnnualSalaryType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AnnualSalaryType] FROM [decimal](9, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[AnyRefLineType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AnyRefLineType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[AnyRefNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AnyRefNumType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[AnyRefReleaseType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AnyRefReleaseType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[AnyRefTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AnyRefTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ApAgeByType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApAgeByType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ApCheckFormTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApCheckFormTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ApCheckNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApCheckNumType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[ApCheckSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApCheckSeqType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[ApDiscType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApDiscType] FROM [decimal](6, 3) NULL
/****** Object:  UserDefinedDataType [dbo].[ApDistSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApDistSeqType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[ApdrafttStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApdrafttStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ApplicationDebugLogSourceTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApplicationDebugLogSourceTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ApplicationNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApplicationNameType] FROM [nvarchar](50) NULL
/****** Object:  UserDefinedDataType [dbo].[ApplyToInvNumCategoryType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApplyToInvNumCategoryType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[AppModuleType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AppModuleType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[AppmtdTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AppmtdTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[AppmtPayTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AppmtPayTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[AppNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AppNumType] FROM [nvarchar](7) NULL
/****** Object:  UserDefinedDataType [dbo].[ApsAgvfleetType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsAgvfleetType] FROM [nvarchar](8) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsAgvsysType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsAgvsysType] FROM [nvarchar](8) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsAltChangeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsAltChangeType] FROM [decimal](11, 0) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsAltNoType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsAltNoType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[ApsAttribType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsAttribType] FROM [nvarchar](30) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsAttvalType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsAttvalType] FROM [nvarchar](30) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsBatchNumberType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsBatchNumberType] FROM [int] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsBatchOverRlType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsBatchOverRlType] FROM [smallint] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsBatchQuanRlType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsBatchQuanRlType] FROM [smallint] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsBatchReportCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsBatchReportCodeType] FROM [nchar](1) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsBatchSepRlType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsBatchSepRlType] FROM [smallint] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[APSBatchStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[APSBatchStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ApsBatchType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsBatchType] FROM [nvarchar](30) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsBitFlagsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsBitFlagsType] FROM [int] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsBitmapType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsBitmapType] FROM [nvarchar](8) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsBomQuanCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsBomQuanCodeType] FROM [nchar](1) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsBreakRuleType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsBreakRuleType] FROM [smallint] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsBufferScaleType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsBufferScaleType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[ApsCalidType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsCalidType] FROM [nvarchar](30) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsCategoryType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsCategoryType] FROM [int] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsChangeTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsChangeTypeType] FROM [int] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsCodeType] FROM [nchar](1) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsConsegType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsConsegType] FROM [nvarchar](8) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsConsysType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsConsysType] FROM [nvarchar](8) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsCostType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsCostType] FROM [float] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsCostTypeCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsCostTypeCodeType] FROM [nchar](1) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsCustomerType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsCustomerType] FROM [nvarchar](8) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsDatabasenameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsDatabasenameType] FROM [nvarchar](30) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsDatasetNumberType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsDatasetNumberType] FROM [smallint] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsDayOrdinalType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsDayOrdinalType] FROM [smallint] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsDbMaxType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsDbMaxType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[ApsDbNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsDbNameType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[ApsDescriptType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsDescriptType] FROM [nvarchar](40) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsDiskSpaceType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsDiskSpaceType] FROM [int] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsDurationType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsDurationType] FROM [float] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsEffDateTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsEffDateTypeType] FROM [smallint] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsEffectType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsEffectType] FROM [nvarchar](39) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsEffvalType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsEffvalType] FROM [nvarchar](63) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsEmailType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsEmailType] FROM [nvarchar](80) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsErrFuncType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsErrFuncType] FROM [nvarchar](30) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsErrMsgType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsErrMsgType] FROM [nvarchar](512) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsErrProcType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsErrProcType] FROM [nvarchar](10) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsExprType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsExprType] FROM [nvarchar](250) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsFieldType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsFieldType] FROM [nvarchar](20) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsFilePrefixType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsFilePrefixType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[ApsFlagType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsFlagType] FROM [nchar](1) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsFleetType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsFleetType] FROM [nvarchar](8) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsFloatType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsFloatType] FROM [float] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsGanttThemeIDType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsGanttThemeIDType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ApsGatewayIntervalType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsGatewayIntervalType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[ApsGlbSeqRlType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsGlbSeqRlType] FROM [smallint] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsGntHighlightType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsGntHighlightType] FROM [nvarchar](30) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsGntSelectionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsGntSelectionType] FROM [nvarchar](30) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsHostnameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsHostnameType] FROM [nvarchar](30) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsHoursType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsHoursType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[ApsIconType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsIconType] FROM [nvarchar](8) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsIntType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsIntType] FROM [int] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsItemTagType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsItemTagType] FROM [int] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsIterationsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsIterationsType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[ApsJobstepType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsJobstepType] FROM [nvarchar](38) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsJs19CrsBrkRlType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsJs19CrsBrkRlType] FROM [smallint] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsJs19OlTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsJs19OlTypeType] FROM [smallint] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsJs19WhenRlType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsJs19WhenRlType] FROM [smallint] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsJsAllocRlType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsJsAllocRlType] FROM [smallint] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsJsBasedCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsJsBasedCodeType] FROM [nchar](1) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsJsResActnCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsJsResActnCodeType] FROM [nchar](1) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsJsSelRlType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsJsSelRlType] FROM [smallint] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsJsStepexpRlType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsJsStepexpRlType] FROM [smallint] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsJsSteptmRlType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsJsSteptmRlType] FROM [smallint] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsJstypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsJstypeType] FROM [nvarchar](30) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsLoadCompletionCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsLoadCompletionCodeType] FROM [nchar](1) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsLoadIDType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsLoadIDType] FROM [smallint] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsLoadSizeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsLoadSizeType] FROM [int] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsLocationType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsLocationType] FROM [nvarchar](30) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsLStatusCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsLStatusCodeType] FROM [nchar](1) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsLtableType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsLtableType] FROM [nvarchar](30) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsLtabvalType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsLtabvalType] FROM [nvarchar](30) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsLtypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsLtypeType] FROM [nchar](1) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsMaintType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsMaintType] FROM [nvarchar](30) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsMaterialType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsMaterialType] FROM [nvarchar](31) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsMatlDelvRuleType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsMatlDelvRuleType] FROM [smallint] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsMatlgrpType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsMatlgrpType] FROM [nvarchar](30) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsMatljsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsMatljsType] FROM [nvarchar](30) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsMatlSelRlType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsMatlSelRlType] FROM [smallint] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsMatlShipRlType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsMatlShipRlType] FROM [smallint] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsMaxIDType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsMaxIDType] FROM [nvarchar](80) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsMemoryType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsMemoryType] FROM [int] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsMhflsgType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsMhflsgType] FROM [nvarchar](8) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsModeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsModeType] FROM [nchar](1) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsMsgParmType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsMsgParmType] FROM [nvarchar](63) NULL
/****** Object:  UserDefinedDataType [dbo].[ApsMultiplierType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsMultiplierType] FROM [float] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsOperationEndCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsOperationEndCodeType] FROM [nchar](1) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsOperationStartCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsOperationStartCodeType] FROM [nchar](1) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsOperationStatusCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsOperationStatusCodeType] FROM [nchar](1) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsOperationTagType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsOperationTagType] FROM [int] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsOrderCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsOrderCodeType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[ApsOrderCompletionCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsOrderCompletionCodeType] FROM [nchar](1) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsOrderCostBasisType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsOrderCostBasisType] FROM [nchar](1) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsOrderExcessCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsOrderExcessCodeType] FROM [nchar](1) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsOrderRelRlType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsOrderRelRlType] FROM [smallint] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsOrderStartCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsOrderStartCodeType] FROM [nchar](1) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsOrderStatusCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsOrderStatusCodeType] FROM [nchar](1) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsOrderTagType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsOrderTagType] FROM [int] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsOrderType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsOrderType] FROM [nvarchar](63) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsOrderTypeCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsOrderTypeCodeType] FROM [nchar](1) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsOrderTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsOrderTypeType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[ApsOrdgrpType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsOrdgrpType] FROM [nvarchar](30) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsOrdTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsOrdTypeType] FROM [int] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsOvAllocRlType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsOvAllocRlType] FROM [smallint] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsPartType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsPartType] FROM [nvarchar](30) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsPathType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsPathType] FROM [nvarchar](250) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsPbomType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsPbomType] FROM [nvarchar](36) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsPDBDepthType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsPDBDepthType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[ApsPDBFieldType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsPDBFieldType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[ApsPercentageType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsPercentageType] FROM [float] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsPlanDebugLevelType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsPlanDebugLevelType] FROM [int] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsPlannerDBNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsPlannerDBNameType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[ApsPlannerDownCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsPlannerDownCodeType] FROM [nchar](1) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsPlannerLoadIDType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsPlannerLoadIDType] FROM [int] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsPlnConsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsPlnConsType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ApsPoolSelRlType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsPoolSelRlType] FROM [smallint] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsPoolSeqRlType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsPoolSeqRlType] FROM [smallint] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsPoolType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsPoolType] FROM [nvarchar](30) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsPooltypType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsPooltypType] FROM [nvarchar](30) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsPortType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsPortType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[ApsProcplanType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsProcplanType] FROM [nvarchar](36) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsQueueType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsQueueType] FROM [nvarchar](80) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsRefType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsRefType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[ApsReplicateNumberType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsReplicateNumberType] FROM [smallint] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsResgroupType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsResgroupType] FROM [nvarchar](30) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsResmhType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsResmhType] FROM [nvarchar](30) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsResourceEndCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsResourceEndCodeType] FROM [nchar](1) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsResourceStartCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsResourceStartCodeType] FROM [nchar](1) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsResourceStatusCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsResourceStatusCodeType] FROM [nchar](1) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsResourceType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsResourceType] FROM [nvarchar](30) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsResrcAllocCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsResrcAllocCodeType] FROM [nchar](1) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsResrcSelRlType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsResrcSelRlType] FROM [smallint] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsResrcSeqRlType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsResrcSeqRlType] FROM [smallint] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsResrefType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsResrefType] FROM [nvarchar](30) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsRestypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsRestypeType] FROM [nvarchar](30) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsRgrpAllocRlType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsRgrpAllocRlType] FROM [smallint] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsRgrpSLTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsRgrpSLTypeType] FROM [nchar](1) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsRulevalType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsRulevalType] FROM [nvarchar](30) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsSchedulerDownCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsSchedulerDownCodeType] FROM [nchar](1) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsSchedulerLoadIDType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsSchedulerLoadIDType] FROM [smallint] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsSelvalueType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsSelvalueType] FROM [float] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsSetupdelType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsSetupdelType] FROM [float] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsSetupRuleType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsSetupRuleType] FROM [smallint] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsShiftexType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsShiftexType] FROM [nvarchar](30) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsShiftexTypeCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsShiftexTypeCodeType] FROM [nchar](1) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsShiftType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsShiftType] FROM [nvarchar](30) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsSiteType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsSiteType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[ApsSmallIntType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsSmallIntType] FROM [smallint] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsSplitRuleType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsSplitRuleType] FROM [smallint] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsSplitsizeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsSplitsizeType] FROM [float] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsStrConstNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsStrConstNameType] FROM [nvarchar](30) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsStrDescriptionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsStrDescriptionType] FROM [nvarchar](2048) NULL
/****** Object:  UserDefinedDataType [dbo].[ApsStrHelpFileNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsStrHelpFileNameType] FROM [nvarchar](64) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsStrLanguageType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsStrLanguageType] FROM [nvarchar](50) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsStrLocalStringType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsStrLocalStringType] FROM [nvarchar](255) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsStrSublanguageType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsStrSublanguageType] FROM [nvarchar](50) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsTableType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsTableType] FROM [nvarchar](13) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsTimeOutType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsTimeOutType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[ApsTimeStrType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsTimeStrType] FROM [nvarchar](8) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsTimeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsTimeType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[ApsTimezoneType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsTimezoneType] FROM [nvarchar](50) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsTraceMessageType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsTraceMessageType] FROM [nvarchar](512) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsTraceMessageTypeCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsTraceMessageTypeCodeType] FROM [nchar](1) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsTrnfleetType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsTrnfleetType] FROM [nvarchar](8) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsTrnsysType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsTrnsysType] FROM [nvarchar](8) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsVarattidType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsVarattidType] FROM [nvarchar](8) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsWaitingTypeCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsWaitingTypeCodeType] FROM [nchar](1) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsWhenRuleType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsWhenRuleType] FROM [smallint] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsWhseType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsWhseType] FROM [nvarchar](4) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[ApsWorkCenterType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ApsWorkCenterType] FROM [nvarchar](6) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[AptrxpTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AptrxpTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[AptrxrTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AptrxrTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[AptrxtSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AptrxtSeqType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[AptrxTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AptrxTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ArAgeByType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ArAgeByType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ArCheckNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ArCheckNumType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[ArDistSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ArDistSeqType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[ArFinRateType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ArFinRateType] FROM [decimal](5, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[ARImportBatchIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ARImportBatchIdType] FROM [decimal](11, 0) NULL
/****** Object:  UserDefinedDataType [dbo].[ARImportFieldDateFmtType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ARImportFieldDateFmtType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[ARImportHeaderNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ARImportHeaderNumType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[ARImportLineNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ARImportLineNumType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[ARImportMappingDelimiterType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ARImportMappingDelimiterType] FROM [nvarchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ARImportMappingFieldNumberType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ARImportMappingFieldNumberType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[ARImportMappingFieldPositionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ARImportMappingFieldPositionType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[ARImportMappingIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ARImportMappingIdType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[ARImportMappingRecordIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ARImportMappingRecordIdType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[ARImportMappingTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ARImportMappingTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ARImportNumberOfImpliedDecimalsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ARImportNumberOfImpliedDecimalsType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[ARImportNumberOfRecordsToSkipType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ARImportNumberOfRecordsToSkipType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[ARImportPaymentStatType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ARImportPaymentStatType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ARImportStatType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ARImportStatType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ARImportValueConvIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ARImportValueConvIdType] FROM [nvarchar](60) NULL
/****** Object:  UserDefinedDataType [dbo].[ARImportValueType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ARImportValueType] FROM [nvarchar](255) NULL
/****** Object:  UserDefinedDataType [dbo].[ArInvSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ArInvSeqType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[arinvType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[arinvType] FROM [nvarchar](12) NULL
/****** Object:  UserDefinedDataType [dbo].[ArinvTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ArinvTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ArpmtTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ArpmtTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ArtranTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ArtranTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[AsnNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AsnNumType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[AssemblySeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AssemblySeqType] FROM [nvarchar](4) NULL
/****** Object:  UserDefinedDataType [dbo].[AttributeGroupClassType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AttributeGroupClassType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[AttributeGroupType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AttributeGroupType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[AttributeLabelType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AttributeLabelType] FROM [nvarchar](25) NULL
/****** Object:  UserDefinedDataType [dbo].[AU_AuthorizationType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AU_AuthorizationType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[AU_CheckDigitType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AU_CheckDigitType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[AU_ContainerSerialNumberType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AU_ContainerSerialNumberType] FROM [decimal](6, 0) NULL
/****** Object:  UserDefinedDataType [dbo].[AU_ContainerTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AU_ContainerTypeType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[AU_ContractIDType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AU_ContractIDType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[AU_ContractPrefixType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AU_ContractPrefixType] FROM [nvarchar](7) NULL
/****** Object:  UserDefinedDataType [dbo].[AU_ContractPriceMethodType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AU_ContractPriceMethodType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[AU_ContractStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AU_ContractStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[AU_EdiDateOffsetHoursType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AU_EdiDateOffsetHoursType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[AU_EquipmentCategoryIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AU_EquipmentCategoryIdType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[AU_FMEAActionTakenType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AU_FMEAActionTakenType] FROM [nvarchar](256) NULL
/****** Object:  UserDefinedDataType [dbo].[AU_FMEAClassificationType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AU_FMEAClassificationType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[AU_FMEAFailureModeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AU_FMEAFailureModeType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[AU_FMEAIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AU_FMEAIdType] FROM [decimal](11, 0) NULL
/****** Object:  UserDefinedDataType [dbo].[AU_FMEARatingType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AU_FMEARatingType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[AU_FMEARecommendedActionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AU_FMEARecommendedActionType] FROM [nvarchar](256) NULL
/****** Object:  UserDefinedDataType [dbo].[AU_ISOContainerGroupCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AU_ISOContainerGroupCodeType] FROM [nchar](2) NULL
/****** Object:  UserDefinedDataType [dbo].[AU_ISOContainerTypeCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AU_ISOContainerTypeCodeType] FROM [nchar](2) NULL
/****** Object:  UserDefinedDataType [dbo].[AU_ISOHeightWidthCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AU_ISOHeightWidthCodeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[AU_ISOLengthCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AU_ISOLengthCodeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[AU_OperatorType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AU_OperatorType] FROM [nvarchar](2) NULL
/****** Object:  UserDefinedDataType [dbo].[AU_OwnerCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AU_OwnerCodeType] FROM [nchar](3) NULL
/****** Object:  UserDefinedDataType [dbo].[AU_PaymentRefNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AU_PaymentRefNumType] FROM [nvarchar](50) NULL
/****** Object:  UserDefinedDataType [dbo].[AU_PaymentRefTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AU_PaymentRefTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[AU_QAProcessDurationType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AU_QAProcessDurationType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[AU_QAProcessIDType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AU_QAProcessIDType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[AU_QAProcessStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AU_QAProcessStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[AU_QAProcessType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AU_QAProcessType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[AU_RefNumIdTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AU_RefNumIdTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[AU_StackHeightType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AU_StackHeightType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[AuditActionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AuditActionType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[AuditCategoryType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AuditCategoryType] FROM [nchar](2) NULL
/****** Object:  UserDefinedDataType [dbo].[AuditDaysType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AuditDaysType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[AuditSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AuditSeqType] FROM [decimal](10, 0) NULL
/****** Object:  UserDefinedDataType [dbo].[AuthenticationModeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AuthenticationModeType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[AuthorizationObjectNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AuthorizationObjectNameType] FROM [nvarchar](50) NULL
/****** Object:  UserDefinedDataType [dbo].[AuthorizationObjectTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AuthorizationObjectTypeType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[AuthStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AuthStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[AutoVoucherMethodType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AutoVoucherMethodType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[AwardAmountType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AwardAmountType] FROM [decimal](8, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[AwardNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AwardNumType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[AwardTitleType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[AwardTitleType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[BalMethodType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BalMethodType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[BankAccountType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BankAccountType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[BankAuthorityPartyIDType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BankAuthorityPartyIDType] FROM [nvarchar](35) NULL
/****** Object:  UserDefinedDataType [dbo].[BankCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BankCodeType] FROM [nvarchar](3) NULL
/****** Object:  UserDefinedDataType [dbo].[BankNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BankNameType] FROM [nvarchar](35) NULL
/****** Object:  UserDefinedDataType [dbo].[BankNumberType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BankNumberType] FROM [nvarchar](15) NULL
/****** Object:  UserDefinedDataType [dbo].[BankNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BankNumType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[BankTransitNumMaskType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BankTransitNumMaskType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[BankTransitNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BankTransitNumType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[BatchID2Type]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BatchID2Type] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[BatchIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BatchIdType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[BatchmsgIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BatchmsgIdType] FROM [uniqueidentifier] NULL
/****** Object:  UserDefinedDataType [dbo].[BatchNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BatchNumType] FROM [decimal](24, 0) NULL
/****** Object:  UserDefinedDataType [dbo].[BatchrptFreqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BatchrptFreqType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[BatchrptStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BatchrptStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[BcoCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BcoCodeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[BflushTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BflushTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[BgQueueType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BgQueueType] FROM [nvarchar](12) NULL
/****** Object:  UserDefinedDataType [dbo].[BgRankType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BgRankType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[BGTaskExecutableType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BGTaskExecutableType] FROM [nvarchar](255) NULL
/****** Object:  UserDefinedDataType [dbo].[BGTaskNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BGTaskNameType] FROM [nvarchar](50) NULL
/****** Object:  UserDefinedDataType [dbo].[BGTaskParmsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BGTaskParmsType] FROM [nvarchar](1100) NULL
/****** Object:  UserDefinedDataType [dbo].[BGTaskReportTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BGTaskReportTypeType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[BICountType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BICountType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[BILargeDecimalType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BILargeDecimalType] FROM [decimal](38, 16) NULL
/****** Object:  UserDefinedDataType [dbo].[BILargeTextType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BILargeTextType] FROM [nvarchar](50) NULL
/****** Object:  UserDefinedDataType [dbo].[BillingTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BillingTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[BILongRowPointerType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BILongRowPointerType] FROM [varchar](72) NULL
/****** Object:  UserDefinedDataType [dbo].[BIMidRowPointerType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BIMidRowPointerType] FROM [varchar](42) NULL
/****** Object:  UserDefinedDataType [dbo].[BIObjectValueType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BIObjectValueType] FROM [nvarchar](200) NULL
/****** Object:  UserDefinedDataType [dbo].[BODTemplateSchemaType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BODTemplateSchemaType] FROM [nvarchar](max) NULL
/****** Object:  UserDefinedDataType [dbo].[BODTemplateStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BODTemplateStatusType] FROM [nvarchar](12) NULL
/****** Object:  UserDefinedDataType [dbo].[BODTemplateType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BODTemplateType] FROM [nvarchar](max) NULL
/****** Object:  UserDefinedDataType [dbo].[BODTemplateVersionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BODTemplateVersionType] FROM [nvarchar](12) NULL
/****** Object:  UserDefinedDataType [dbo].[BolNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BolNumType] FROM [nvarchar](7) NULL
/****** Object:  UserDefinedDataType [dbo].[BolPrefixType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BolPrefixType] FROM [nvarchar](7) NULL
/****** Object:  UserDefinedDataType [dbo].[BolStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BolStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[BolUnitsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BolUnitsType] FROM [nvarchar](15) NULL
/****** Object:  UserDefinedDataType [dbo].[BOMImportElementTagNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BOMImportElementTagNameType] FROM [nvarchar](4000) NULL
/****** Object:  UserDefinedDataType [dbo].[BOMImportValueConvIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BOMImportValueConvIdType] FROM [nvarchar](60) NULL
/****** Object:  UserDefinedDataType [dbo].[BOMImportValueType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BOMImportValueType] FROM [nvarchar](255) NULL
/****** Object:  UserDefinedDataType [dbo].[BonusCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BonusCodeType] FROM [nvarchar](3) NULL
/****** Object:  UserDefinedDataType [dbo].[BPProductType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BPProductType] FROM [nvarchar](50) NULL
/****** Object:  UserDefinedDataType [dbo].[BPVersionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BPVersionType] FROM [nvarchar](15) NULL
/****** Object:  UserDefinedDataType [dbo].[BranchCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BranchCodeType] FROM [nvarchar](15) NULL
/****** Object:  UserDefinedDataType [dbo].[BranchIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BranchIdType] FROM [nvarchar](3) NULL
/****** Object:  UserDefinedDataType [dbo].[BrandRecognitionLevelType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BrandRecognitionLevelType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[BubbleType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BubbleType] FROM [nvarchar](4) NULL
/****** Object:  UserDefinedDataType [dbo].[BucketsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BucketsType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[BufferHoursType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BufferHoursType] FROM [decimal](7, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[BuilderInvNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BuilderInvNumType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[BuilderInvoicePrefixType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BuilderInvoicePrefixType] FROM [nvarchar](7) NULL
/****** Object:  UserDefinedDataType [dbo].[BuilderPoNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BuilderPoNumType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[BuilderPoPrefixType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BuilderPoPrefixType] FROM [nvarchar](7) NULL
/****** Object:  UserDefinedDataType [dbo].[BuilderVoucherPrefixType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BuilderVoucherPrefixType] FROM [nvarchar](7) NULL
/****** Object:  UserDefinedDataType [dbo].[BuilderVoucherType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BuilderVoucherType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[BusExportCounterType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BusExportCounterType] FROM [decimal](24, 0) NULL
/****** Object:  UserDefinedDataType [dbo].[BusinessIdentificationNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BusinessIdentificationNumType] FROM [nvarchar](14) NULL
/****** Object:  UserDefinedDataType [dbo].[BusinessIdentifierCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BusinessIdentifierCodeType] FROM [nvarchar](11) NULL
/****** Object:  UserDefinedDataType [dbo].[BusinessRoleType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[BusinessRoleType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[CacheWindowsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CacheWindowsType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[CalendarType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CalendarType] FROM [nvarchar](6) NULL
/****** Object:  UserDefinedDataType [dbo].[CampaignIDType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CampaignIDType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[CampaignStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CampaignStatusType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[CampaignTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CampaignTypeType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[CapacityType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CapacityType] FROM [decimal](7, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[CapFactorType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CapFactorType] FROM [decimal](6, 3) NULL
/****** Object:  UserDefinedDataType [dbo].[CarrierAccountType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CarrierAccountType] FROM [nvarchar](25) NULL
/****** Object:  UserDefinedDataType [dbo].[CarrierBillToTransportationType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CarrierBillToTransportationType] FROM [nvarchar](5) NULL
/****** Object:  UserDefinedDataType [dbo].[CarrierCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CarrierCodeType] FROM [nvarchar](15) NULL
/****** Object:  UserDefinedDataType [dbo].[CarrierNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CarrierNumType] FROM [nvarchar](15) NULL
/****** Object:  UserDefinedDataType [dbo].[CarrierReferenceType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CarrierReferenceType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[CarrierServiceType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CarrierServiceType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[CarrierType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CarrierType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[CarrierUpchargePercentType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CarrierUpchargePercentType] FROM [decimal](4, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[CashRoundingFactorType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CashRoundingFactorType] FROM [decimal](6, 3) NULL
/****** Object:  UserDefinedDataType [dbo].[CategoryType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CategoryType] FROM [nvarchar](6) NULL
/****** Object:  UserDefinedDataType [dbo].[CCIAuthCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CCIAuthCodeType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[CCICardLast4DigitsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CCICardLast4DigitsType] FROM [nchar](4) NULL
/****** Object:  UserDefinedDataType [dbo].[CCICardNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CCICardNumType] FROM [nvarchar](16) NULL
/****** Object:  UserDefinedDataType [dbo].[CCICardSystemIDType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CCICardSystemIDType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[CCICardSystemType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CCICardSystemType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[CCICardTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CCICardTypeType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[CCICharAuthCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CCICharAuthCodeType] FROM [nvarchar](50) NULL
/****** Object:  UserDefinedDataType [dbo].[CCICVNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CCICVNumType] FROM [nvarchar](4) NULL
/****** Object:  UserDefinedDataType [dbo].[CCIExpDateType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CCIExpDateType] FROM [nchar](5) NULL
/****** Object:  UserDefinedDataType [dbo].[CCIGatewayLongTransNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CCIGatewayLongTransNumType] FROM [decimal](24, 0) NULL
/****** Object:  UserDefinedDataType [dbo].[CCIGatewayTransNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CCIGatewayTransNumType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[CCIGatewayVendIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CCIGatewayVendIdType] FROM [nvarchar](255) NULL
/****** Object:  UserDefinedDataType [dbo].[CCITransRefTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CCITransRefTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[CCITransTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CCITransTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[CellType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CellType] FROM [nvarchar](6) NULL
/****** Object:  UserDefinedDataType [dbo].[CertificateOfConformanceTextType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CertificateOfConformanceTextType] FROM [nvarchar](max) NULL
/****** Object:  UserDefinedDataType [dbo].[CertNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CertNumType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[CfpTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CfpTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ChargebackSequenceType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ChargebackSequenceType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[ChargebackStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ChargebackStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ChargebackTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ChargebackTypeType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[ChargeTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ChargeTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[CharLongDescType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CharLongDescType] FROM [nchar](255) NULL
/****** Object:  UserDefinedDataType [dbo].[CharMediumDescType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CharMediumDescType] FROM [nchar](80) NULL
/****** Object:  UserDefinedDataType [dbo].[CharShortDescType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CharShortDescType] FROM [nchar](35) NULL
/****** Object:  UserDefinedDataType [dbo].[ChartDPercentType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ChartDPercentType] FROM [decimal](6, 3) NULL
/****** Object:  UserDefinedDataType [dbo].[ChartDSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ChartDSeqType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[ChartTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ChartTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ChgNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ChgNumType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[ChildrenType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ChildrenType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[CityType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CityType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[ClassNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ClassNameType] FROM [nvarchar](32) NULL
/****** Object:  UserDefinedDataType [dbo].[CN_NotesType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CN_NotesType] FROM [nvarchar](100) NULL
/****** Object:  UserDefinedDataType [dbo].[CNVATStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CNVATStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[CoBlnStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CoBlnStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[CoConvertToType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CoConvertToType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[CoCustPoProjRmaTrnVendNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CoCustPoProjRmaTrnVendNumType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[CoitemStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CoitemStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[CoJobPrjType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CoJobPrjType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[CoJobProjTrnNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CoJobProjTrnNumType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[CoLineProjTaskPoRmaTrnLineInvNumVoucherType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CoLineProjTaskPoRmaTrnLineInvNumVoucherType] FROM [nvarchar](12) NULL
/****** Object:  UserDefinedDataType [dbo].[CoLineProjTaskTrnLineType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CoLineProjTaskTrnLineType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[CoLineSuffixPoLineProjTaskRmaTrnLineType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CoLineSuffixPoLineProjTaskRmaTrnLineType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[CoLineSuffixProjTaskTrnLineType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CoLineSuffixProjTaskTrnLineType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[CoLineSuffixProjTaskType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CoLineSuffixProjTaskType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[CoLineSuffixType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CoLineSuffixType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[CoLineType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CoLineType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[CollectionActionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CollectionActionType] FROM [nvarchar](16) NULL
/****** Object:  UserDefinedDataType [dbo].[CollectionFilterType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CollectionFilterType] FROM [nvarchar](max) NULL
/****** Object:  UserDefinedDataType [dbo].[CollectionNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CollectionNameType] FROM [nvarchar](80) NULL
/****** Object:  UserDefinedDataType [dbo].[CollectionPropertyDataTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CollectionPropertyDataTypeType] FROM [nvarchar](80) NULL
/****** Object:  UserDefinedDataType [dbo].[CollectionPropertyNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CollectionPropertyNameType] FROM [nvarchar](80) NULL
/****** Object:  UserDefinedDataType [dbo].[CollectionPropertyPathType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CollectionPropertyPathType] FROM [nvarchar](max) NULL
/****** Object:  UserDefinedDataType [dbo].[ColumnNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ColumnNameType] FROM [nvarchar](32) NULL
/****** Object:  UserDefinedDataType [dbo].[ColumnPrecisionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ColumnPrecisionType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[ColumnScaleType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ColumnScaleType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[ColumnType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ColumnType] FROM [nvarchar](32) NULL
/****** Object:  UserDefinedDataType [dbo].[ComboClassType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ComboClassType] FROM [nvarchar](35) NULL
/****** Object:  UserDefinedDataType [dbo].[ComboLabelType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ComboLabelType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[ComboValueType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ComboValueType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[ComboWidthType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ComboWidthType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[CommdueSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CommdueSeqType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[CommdueStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CommdueStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[CommLogNotesType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CommLogNotesType] FROM [nvarchar](max) NULL
/****** Object:  UserDefinedDataType [dbo].[CommLogStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CommLogStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[CommLogTopicType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CommLogTopicType] FROM [nvarchar](15) NULL
/****** Object:  UserDefinedDataType [dbo].[CommLogTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CommLogTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[CommodityCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CommodityCodeType] FROM [nvarchar](12) NULL
/****** Object:  UserDefinedDataType [dbo].[CommodityJurisdictionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CommodityJurisdictionType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[CommPercentType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CommPercentType] FROM [decimal](7, 3) NULL
/****** Object:  UserDefinedDataType [dbo].[CommtabField1Type]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CommtabField1Type] FROM [nvarchar](15) NULL
/****** Object:  UserDefinedDataType [dbo].[CommtabField2Type]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CommtabField2Type] FROM [nvarchar](15) NULL
/****** Object:  UserDefinedDataType [dbo].[CommtabValueType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CommtabValueType] FROM [decimal](7, 3) NULL
/****** Object:  UserDefinedDataType [dbo].[CompanyAddrBlockType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CompanyAddrBlockType] FROM [nvarchar](338) NULL
/****** Object:  UserDefinedDataType [dbo].[CompetitorIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CompetitorIdType] FROM [nvarchar](7) NULL
/****** Object:  UserDefinedDataType [dbo].[ComplianceProgramIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ComplianceProgramIdType] FROM [nvarchar](6) NULL
/****** Object:  UserDefinedDataType [dbo].[CompNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CompNumType] FROM [nvarchar](6) NULL
/****** Object:  UserDefinedDataType [dbo].[ConfigAttrNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ConfigAttrNameType] FROM [nvarchar](255) NULL
/****** Object:  UserDefinedDataType [dbo].[ConfigAttrTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ConfigAttrTypeType] FROM [nvarchar](15) NULL
/****** Object:  UserDefinedDataType [dbo].[ConfigAttrValueType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ConfigAttrValueType] FROM [nvarchar](max) NULL
/****** Object:  UserDefinedDataType [dbo].[ConfigAutoJobType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ConfigAutoJobType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ConfigAutoPostType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ConfigAutoPostType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ConfigCompIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ConfigCompIdType] FROM [nvarchar](12) NULL
/****** Object:  UserDefinedDataType [dbo].[ConfigCompNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ConfigCompNameType] FROM [nvarchar](255) NULL
/****** Object:  UserDefinedDataType [dbo].[ConfigCompNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ConfigCompNumType] FROM [nvarchar](255) NULL
/****** Object:  UserDefinedDataType [dbo].[ConfigCompTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ConfigCompTypeType] FROM [nvarchar](5) NULL
/****** Object:  UserDefinedDataType [dbo].[ConfigEntryPointType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ConfigEntryPointType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ConfigFormNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ConfigFormNameType] FROM [nvarchar](50) NULL
/****** Object:  UserDefinedDataType [dbo].[ConfigGID2Type]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ConfigGID2Type] FROM [nvarchar](36) NULL
/****** Object:  UserDefinedDataType [dbo].[ConfigGIDType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ConfigGIDType] FROM [nvarchar](32) NULL
/****** Object:  UserDefinedDataType [dbo].[ConfigIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ConfigIdType] FROM [nvarchar](32) NULL
/****** Object:  UserDefinedDataType [dbo].[ConfigModelType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ConfigModelType] FROM [nvarchar](255) NULL
/****** Object:  UserDefinedDataType [dbo].[ConfigNameSpaceType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ConfigNameSpaceType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[ConfigPostConfigType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ConfigPostConfigType] FROM [nvarchar](80) NULL
/****** Object:  UserDefinedDataType [dbo].[ConfigPrintFlagType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ConfigPrintFlagType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ConfigRefLineSufType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ConfigRefLineSufType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[ConfigRefNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ConfigRefNumType] FROM [nvarchar](32) NULL
/****** Object:  UserDefinedDataType [dbo].[ConfigRefTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ConfigRefTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ConfigRuleSetType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ConfigRuleSetType] FROM [nvarchar](60) NULL
/****** Object:  UserDefinedDataType [dbo].[ConfigSlFieldType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ConfigSlFieldType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[ConfigStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ConfigStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ConfigTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ConfigTypeType] FROM [nvarchar](24) NULL
/****** Object:  UserDefinedDataType [dbo].[ConfigurationNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ConfigurationNameType] FROM [nvarchar](50) NULL
/****** Object:  UserDefinedDataType [dbo].[ConfiguratorAuthKeyType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ConfiguratorAuthKeyType] FROM [nvarchar](255) NULL
/****** Object:  UserDefinedDataType [dbo].[ConfiguratorDefaultProfile]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ConfiguratorDefaultProfile] FROM [nvarchar](50) NULL
/****** Object:  UserDefinedDataType [dbo].[ConfiguratorServerIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ConfiguratorServerIdType] FROM [nvarchar](60) NULL
/****** Object:  UserDefinedDataType [dbo].[ConfiguratorType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ConfiguratorType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ConfiguratorUserInterfaceType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ConfiguratorUserInterfaceType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ConfigUserInterfaceType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ConfigUserInterfaceType] FROM [nvarchar](80) NULL
/****** Object:  UserDefinedDataType [dbo].[ConInvSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ConInvSeqType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[ConsignmentsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ConsignmentsType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[ConsignmentTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ConsignmentTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ContactIDType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ContactIDType] FROM [nvarchar](7) NULL
/****** Object:  UserDefinedDataType [dbo].[ContactMethodType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ContactMethodType] FROM [nvarchar](6) NULL
/****** Object:  UserDefinedDataType [dbo].[ContactType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ContactType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[ContainerNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ContainerNumType] FROM [nvarchar](15) NULL
/****** Object:  UserDefinedDataType [dbo].[ContainerPrefixType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ContainerPrefixType] FROM [nvarchar](15) NULL
/****** Object:  UserDefinedDataType [dbo].[ContainerType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ContainerType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[ContractType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ContractType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[CoNumJobType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CoNumJobType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[CoNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CoNumType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[CopiesType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CopiesType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[CoPoReleaseArInvSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CoPoReleaseArInvSeqType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[CoPoReleaseType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CoPoReleaseType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[CoPrefixType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CoPrefixType] FROM [nvarchar](7) NULL
/****** Object:  UserDefinedDataType [dbo].[CoProductMixType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CoProductMixType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[CoProjTaskTrnLineType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CoProjTaskTrnLineType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[CoProjTrnNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CoProjTrnNumType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[CoReleaseOperNumPoReleaseType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CoReleaseOperNumPoReleaseType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[CoReleaseOperNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CoReleaseOperNumType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[CoReleaseProjmatlSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CoReleaseProjmatlSeqType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[CoReleaseType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CoReleaseType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[CoShipApprovalLogSequenceType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CoShipApprovalLogSequenceType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[CoSlsCommSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CoSlsCommSeqType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[CoStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CoStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[CostCodeClassType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CostCodeClassType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[CostCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CostCodeType] FROM [nvarchar](3) NULL
/****** Object:  UserDefinedDataType [dbo].[CostCodeTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CostCodeTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[CostingAlternativeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CostingAlternativeType] FROM [nvarchar](12) NULL
/****** Object:  UserDefinedDataType [dbo].[CostMethodType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CostMethodType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[CostPrcNoNegType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CostPrcNoNegType] FROM [decimal](20, 8) NULL
/****** Object:  UserDefinedDataType [dbo].[CostPrcType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CostPrcType] FROM [decimal](20, 8) NULL
/****** Object:  UserDefinedDataType [dbo].[CostTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CostTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[CoTrnLineType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CoTrnLineType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[CoTrnNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CoTrnNumType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[CoTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CoTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[CountryCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CountryCodeType] FROM [nvarchar](2) NULL
/****** Object:  UserDefinedDataType [dbo].[CountryPackOptionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CountryPackOptionType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[CountryPackType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CountryPackType] FROM [nvarchar](15) NULL
/****** Object:  UserDefinedDataType [dbo].[CountrySpecificFieldNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CountrySpecificFieldNameType] FROM [nvarchar](255) NULL
/****** Object:  UserDefinedDataType [dbo].[CountrySpecificFieldValueType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CountrySpecificFieldValueType] FROM [nvarchar](100) NULL
/****** Object:  UserDefinedDataType [dbo].[CountryType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CountryType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[CountSequenceType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CountSequenceType] FROM [decimal](24, 0) NULL
/****** Object:  UserDefinedDataType [dbo].[CountyType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CountyType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[CourseNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CourseNumType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[CreateFeatType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CreateFeatType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[CrewSizeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CrewSizeType] FROM [decimal](5, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[CriterionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CriterionType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[CriterionTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CriterionTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[CtcLogSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CtcLogSeqType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[CurJobPrefixType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CurJobPrefixType] FROM [nvarchar](7) NULL
/****** Object:  UserDefinedDataType [dbo].[CurrCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CurrCodeType] FROM [nvarchar](3) NULL
/****** Object:  UserDefinedDataType [dbo].[CurrencyRateOverrideToleranceAmountType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CurrencyRateOverrideToleranceAmountType] FROM [decimal](12, 7) NULL
/****** Object:  UserDefinedDataType [dbo].[CurrentDateType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CurrentDateType] FROM [datetime] NULL
/****** Object:  UserDefinedDataType [dbo].[CurrSymbolType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CurrSymbolType] FROM [nvarchar](3) NULL
/****** Object:  UserDefinedDataType [dbo].[CurrTransMethodType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CurrTransMethodType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[CustDraftEscalationType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CustDraftEscalationType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[CustdrftStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CustdrftStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[CustEmpVendNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CustEmpVendNumType] FROM [nvarchar](7) NULL
/****** Object:  UserDefinedDataType [dbo].[CustItemRankType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CustItemRankType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[CustItemSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CustItemSeqType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[CustItemType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CustItemType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[CustLogRefNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CustLogRefNumType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[CustLogRefTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CustLogRefTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[CustNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CustNumType] FROM [nvarchar](7) NULL
/****** Object:  UserDefinedDataType [dbo].[CustomerAddrBlockType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CustomerAddrBlockType] FROM [nvarchar](374) NULL
/****** Object:  UserDefinedDataType [dbo].[CustPayTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CustPayTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[CustPoType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CustPoType] FROM [nvarchar](25) NULL
/****** Object:  UserDefinedDataType [dbo].[CustPrefixType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CustPrefixType] FROM [nvarchar](7) NULL
/****** Object:  UserDefinedDataType [dbo].[CustSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CustSeqType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[CustTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CustTypeType] FROM [nvarchar](6) NULL
/****** Object:  UserDefinedDataType [dbo].[CustVendRefNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CustVendRefNumType] FROM [nvarchar](7) NULL
/****** Object:  UserDefinedDataType [dbo].[CutoffDayType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CutoffDayType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[CycleFreqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CycleFreqType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[CycleStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CycleStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[CycleToleranceType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CycleToleranceType] FROM [decimal](9, 3) NULL
/****** Object:  UserDefinedDataType [dbo].[CycleTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[CycleTypeType] FROM [nvarchar](4) NULL
/****** Object:  UserDefinedDataType [dbo].[DatabaseTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DatabaseTypeType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[Date4Type]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[Date4Type] FROM [datetime] NULL
/****** Object:  UserDefinedDataType [dbo].[DateFormatType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DateFormatType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[DateKeyType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DateKeyType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[DateOffsetType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DateOffsetType] FROM [smallint] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[DateSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DateSeqType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[DateTime4Type]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DateTime4Type] FROM [datetime] NULL
/****** Object:  UserDefinedDataType [dbo].[DateTimeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DateTimeType] FROM [datetime] NULL
/****** Object:  UserDefinedDataType [dbo].[DateType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DateType] FROM [datetime] NULL
/****** Object:  UserDefinedDataType [dbo].[DaysCarriedForwardType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DaysCarriedForwardType] FROM [decimal](5, 3) NULL
/****** Object:  UserDefinedDataType [dbo].[DaysLeaveType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DaysLeaveType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[DaysLostType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DaysLostType] FROM [decimal](5, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[DaysMaxReimbursedType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DaysMaxReimbursedType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[DaysOffMaxType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DaysOffMaxType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[DaysOffType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DaysOffType] FROM [decimal](5, 3) NULL
/****** Object:  UserDefinedDataType [dbo].[DaysOSType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DaysOSType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[DaysOverType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DaysOverType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[DaysSupplyType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DaysSupplyType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[DcArchiveDaysType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DcArchiveDaysType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[DcDatabaseIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DcDatabaseIdType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[DcEscFlagType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DcEscFlagType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[DcHardwareType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DcHardwareType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[DcHostType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DcHostType] FROM [nvarchar](73) NULL
/****** Object:  UserDefinedDataType [dbo].[DcPortType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DcPortType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[DcPrinterTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DcPrinterTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[DcsfcTransTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DcsfcTransTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[DcStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DcStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[DcTermIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DcTermIdType] FROM [nvarchar](4) NULL
/****** Object:  UserDefinedDataType [dbo].[DcTransNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DcTransNumType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[DcTransTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DcTransTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[DecimalPlacesType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DecimalPlacesType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[DeCodeNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DeCodeNumType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[DeCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DeCodeType] FROM [nvarchar](3) NULL
/****** Object:  UserDefinedDataType [dbo].[DefaultCharType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DefaultCharType] FROM [nvarchar](80) NULL
/****** Object:  UserDefinedDataType [dbo].[DefaultIntType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DefaultIntType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[DefaultValueType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DefaultValueType] FROM [nvarchar](255) NULL
/****** Object:  UserDefinedDataType [dbo].[DefaultYesNoType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DefaultYesNoType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[DegreeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DegreeType] FROM [nvarchar](7) NULL
/****** Object:  UserDefinedDataType [dbo].[DeliverToType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DeliverToType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[DeltermType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DeltermType] FROM [nvarchar](4) NULL
/****** Object:  UserDefinedDataType [dbo].[DepDtlCreateSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DepDtlCreateSeqType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[DepDtlTransCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DepDtlTransCodeType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[DeprCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DeprCodeType] FROM [nvarchar](5) NULL
/****** Object:  UserDefinedDataType [dbo].[DeprMethodType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DeprMethodType] FROM [nvarchar](6) NULL
/****** Object:  UserDefinedDataType [dbo].[DeptType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DeptType] FROM [nvarchar](6) NULL
/****** Object:  UserDefinedDataType [dbo].[DeRateBasisType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DeRateBasisType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[DescriptionOfGoodsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DescriptionOfGoodsType] FROM [nvarchar](50) NULL
/****** Object:  UserDefinedDataType [dbo].[DescriptionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DescriptionType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[DestinationType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DestinationType] FROM [nvarchar](240) NULL
/****** Object:  UserDefinedDataType [dbo].[DeTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DeTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[DeWorkUnitDescType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DeWorkUnitDescType] FROM [nvarchar](15) NULL
/****** Object:  UserDefinedDataType [dbo].[DimensionAttributeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DimensionAttributeType] FROM [nvarchar](50) NULL
/****** Object:  UserDefinedDataType [dbo].[DimensionAttributeTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DimensionAttributeTypeType] FROM [nvarchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[DimensionDescriptionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DimensionDescriptionType] FROM [nvarchar](150) NULL
/****** Object:  UserDefinedDataType [dbo].[DimensionFunctionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DimensionFunctionType] FROM [nvarchar](255) NULL
/****** Object:  UserDefinedDataType [dbo].[DimensionFunctionTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DimensionFunctionTypeType] FROM [nvarchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[DimensionMaskingType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DimensionMaskingType] FROM [nvarchar](52) NULL
/****** Object:  UserDefinedDataType [dbo].[DimensionNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DimensionNameType] FROM [nvarchar](50) NULL
/****** Object:  UserDefinedDataType [dbo].[DimensionObjectNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DimensionObjectNameType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[DimensionObjectType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DimensionObjectType] FROM [nvarchar](50) NULL
/****** Object:  UserDefinedDataType [dbo].[DimensionObjectTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DimensionObjectTypeType] FROM [nvarchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[DimensionPrefixSymbolType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DimensionPrefixSymbolType] FROM [nvarchar](15) NULL
/****** Object:  UserDefinedDataType [dbo].[DimensionPrefixType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DimensionPrefixType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[DimensionToleranceTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DimensionToleranceTypeType] FROM [nvarchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[DimensionTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DimensionTypeType] FROM [nvarchar](15) NULL
/****** Object:  UserDefinedDataType [dbo].[DimensionUnitFactorType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DimensionUnitFactorType] FROM [nvarchar](150) NULL
/****** Object:  UserDefinedDataType [dbo].[DimensionUnitType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DimensionUnitType] FROM [nvarchar](50) NULL
/****** Object:  UserDefinedDataType [dbo].[DimensionValueType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DimensionValueType] FROM [nvarchar](max) NULL
/****** Object:  UserDefinedDataType [dbo].[DirDepPercentType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DirDepPercentType] FROM [decimal](4, 1) NULL
/****** Object:  UserDefinedDataType [dbo].[DirectDebitNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DirectDebitNumType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[DirectDebitStatType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DirectDebitStatType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[DiscDaysType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DiscDaysType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[DispCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DispCodeType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[DivNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DivNumType] FROM [nvarchar](6) NULL
/****** Object:  UserDefinedDataType [dbo].[DocCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DocCodeType] FROM [nvarchar](6) NULL
/****** Object:  UserDefinedDataType [dbo].[DocumentExtensionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DocumentExtensionType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[DocumentNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DocumentNameType] FROM [nvarchar](120) NULL
/****** Object:  UserDefinedDataType [dbo].[DocumentNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DocumentNumType] FROM [nvarchar](12) NULL
/****** Object:  UserDefinedDataType [dbo].[DocumentObjectType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DocumentObjectType] FROM [varbinary](max) NULL
/****** Object:  UserDefinedDataType [dbo].[DocumentStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DocumentStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[DocumentTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DocumentTypeType] FROM [nvarchar](50) NULL
/****** Object:  UserDefinedDataType [dbo].[DoInvoiceType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DoInvoiceType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[DoLengthType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DoLengthType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[DoLineType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DoLineType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[DoMarksType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DoMarksType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[DoMotorFreightCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DoMotorFreightCodeType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[DoNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DoNumType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[DoPackageTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DoPackageTypeType] FROM [nvarchar](6) NULL
/****** Object:  UserDefinedDataType [dbo].[DoPrefixType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DoPrefixType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[DoSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DoSeqType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[DoStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DoStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[DraftNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DraftNumType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[DrawingNbrType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DrawingNbrType] FROM [nvarchar](25) NULL
/****** Object:  UserDefinedDataType [dbo].[DropPrefixType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DropPrefixType] FROM [nvarchar](7) NULL
/****** Object:  UserDefinedDataType [dbo].[DropSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DropSeqType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[DropShipNoType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DropShipNoType] FROM [nvarchar](7) NULL
/****** Object:  UserDefinedDataType [dbo].[DropShipTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DropShipTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[DueDaysType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DueDaysType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[DuePeriodType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DuePeriodType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[DunsNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DunsNumType] FROM [nvarchar](15) NULL
/****** Object:  UserDefinedDataType [dbo].[DurationDataType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DurationDataType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[DynamicTableTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[DynamicTableTypeType] FROM [nvarchar](16) NULL
/****** Object:  UserDefinedDataType [dbo].[EarnedRebateIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EarnedRebateIdType] FROM [decimal](11, 0) NULL
/****** Object:  UserDefinedDataType [dbo].[EarnedRebateStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EarnedRebateStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ECCNUSMLValueType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ECCNUSMLValueType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[EcCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EcCodeType] FROM [nvarchar](2) NULL
/****** Object:  UserDefinedDataType [dbo].[EcConvFactorType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EcConvFactorType] FROM [decimal](9, 6) NULL
/****** Object:  UserDefinedDataType [dbo].[EcnBomSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EcnBomSeqType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[ECNChangeTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ECNChangeTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[EcnDistCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EcnDistCodeType] FROM [nvarchar](3) NULL
/****** Object:  UserDefinedDataType [dbo].[EcnGroupType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EcnGroupType] FROM [nvarchar](4) NULL
/****** Object:  UserDefinedDataType [dbo].[EcnItemStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EcnItemStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[EcnitemTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EcnitemTypeType] FROM [nchar](2) NULL
/****** Object:  UserDefinedDataType [dbo].[EcnJobTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EcnJobTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[EcnLineType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EcnLineType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[EcnModeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EcnModeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[EcnNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EcnNumType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[EcnPriorCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EcnPriorCodeType] FROM [nvarchar](3) NULL
/****** Object:  UserDefinedDataType [dbo].[EcnStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EcnStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[EdiAckCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EdiAckCodeType] FROM [nvarchar](2) NULL
/****** Object:  UserDefinedDataType [dbo].[EdiAckTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EdiAckTypeType] FROM [nvarchar](2) NULL
/****** Object:  UserDefinedDataType [dbo].[EdiAsnCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EdiAsnCodeType] FROM [nvarchar](2) NULL
/****** Object:  UserDefinedDataType [dbo].[EdiAutoPostType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EdiAutoPostType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[EdiBolItemRateType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EdiBolItemRateType] FROM [decimal](18, 8) NULL
/****** Object:  UserDefinedDataType [dbo].[EdiCoitemStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EdiCoitemStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[EdiCoStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EdiCoStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[EdiCoTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EdiCoTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[EdiDateOffsetType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EdiDateOffsetType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[EdiErrLineNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EdiErrLineNumType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[EdiErrMsgType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EdiErrMsgType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[EdiErrNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EdiErrNumType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[EdiErrSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EdiErrSeqType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[EdiExtRefType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EdiExtRefType] FROM [nvarchar](6) NULL
/****** Object:  UserDefinedDataType [dbo].[EdiInvCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EdiInvCodeType] FROM [nvarchar](2) NULL
/****** Object:  UserDefinedDataType [dbo].[EdiInvtypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EdiInvtypeType] FROM [nvarchar](2) NULL
/****** Object:  UserDefinedDataType [dbo].[EdiNonApplDataType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EdiNonApplDataType] FROM [nvarchar](75) NULL
/****** Object:  UserDefinedDataType [dbo].[EdiPoSeqNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EdiPoSeqNumType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[EdiReleaseActionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EdiReleaseActionType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[EdiSchedNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EdiSchedNumType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[EdiSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EdiSeqType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[EdiTpCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EdiTpCodeType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[EdiTrxCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EdiTrxCodeType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[EdiTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EdiTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[EdiUseDateType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EdiUseDateType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[EdiUsePriceType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EdiUsePriceType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[EducationYearsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EducationYearsType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[EEOClsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EEOClsType] FROM [nvarchar](2) NULL
/****** Object:  UserDefinedDataType [dbo].[EfficiencyType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EfficiencyType] FROM [decimal](4, 1) NULL
/****** Object:  UserDefinedDataType [dbo].[EFTAppFieldType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EFTAppFieldType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[EFTAppGroupType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EFTAppGroupType] FROM [nvarchar](60) NULL
/****** Object:  UserDefinedDataType [dbo].[EFTBankNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EFTBankNumType] FROM [nvarchar](6) NULL
/****** Object:  UserDefinedDataType [dbo].[EFTBatchGroupType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EFTBatchGroupType] FROM [nvarchar](60) NULL
/****** Object:  UserDefinedDataType [dbo].[EFTBatchIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EFTBatchIdType] FROM [decimal](11, 0) NULL
/****** Object:  UserDefinedDataType [dbo].[EFTCodeValueType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EFTCodeValueType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[EFTCodeValueTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EFTCodeValueTypeType] FROM [nvarchar](60) NULL
/****** Object:  UserDefinedDataType [dbo].[EFTDepositAcctTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EFTDepositAcctTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[EFTEndOfLineType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EFTEndOfLineType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[EFTFieldDateFmtType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EFTFieldDateFmtType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[EFTFieldLabelType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EFTFieldLabelType] FROM [nvarchar](255) NULL
/****** Object:  UserDefinedDataType [dbo].[EFTFieldNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EFTFieldNameType] FROM [nvarchar](255) NULL
/****** Object:  UserDefinedDataType [dbo].[EFTFileNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EFTFileNameType] FROM [nvarchar](60) NULL
/****** Object:  UserDefinedDataType [dbo].[EFTFileSizeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EFTFileSizeType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[EFTFileType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EFTFileType] FROM [nvarchar](15) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[EFTFormatType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EFTFormatType] FROM [nvarchar](2) NULL
/****** Object:  UserDefinedDataType [dbo].[EFTFormatTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EFTFormatTypeType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[EFTImportFieldNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EFTImportFieldNameType] FROM [nvarchar](60) NULL
/****** Object:  UserDefinedDataType [dbo].[EFTImportMethodType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EFTImportMethodType] FROM [nvarchar](80) NULL
/****** Object:  UserDefinedDataType [dbo].[EFTMappingDelimiterType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EFTMappingDelimiterType] FROM [nvarchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[EFTMappingFieldNumberType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EFTMappingFieldNumberType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[EFTMappingFieldPositionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EFTMappingFieldPositionType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[EFTMappingIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EFTMappingIdType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[EFTMappingRecordIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EFTMappingRecordIdType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[EFTMappingTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EFTMappingTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[EFTNumberOfImpliedDecimalsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EFTNumberOfImpliedDecimalsType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[EFTParseStringSizeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EFTParseStringSizeType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[EFTParseStringType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EFTParseStringType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[EFTRecordLengthType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EFTRecordLengthType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[EFTRegistrationNumberType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EFTRegistrationNumberType] FROM [nvarchar](14) NULL
/****** Object:  UserDefinedDataType [dbo].[EFTStatType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EFTStatType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[EFTTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EFTTypeType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[EFTUserNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EFTUserNameType] FROM [nvarchar](26) NULL
/****** Object:  UserDefinedDataType [dbo].[EFTUserNumberType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EFTUserNumberType] FROM [nvarchar](6) NULL
/****** Object:  UserDefinedDataType [dbo].[EFTValueConvIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EFTValueConvIdType] FROM [nvarchar](60) NULL
/****** Object:  UserDefinedDataType [dbo].[EFTValueType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EFTValueType] FROM [nvarchar](255) NULL
/****** Object:  UserDefinedDataType [dbo].[EICCreditsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EICCreditsType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[ElectronicSignatureColumnValueType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ElectronicSignatureColumnValueType] FROM [nvarchar](max) NULL
/****** Object:  UserDefinedDataType [dbo].[ElectronicSignatureTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ElectronicSignatureTypeType] FROM [nvarchar](15) NULL
/****** Object:  UserDefinedDataType [dbo].[EmailStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EmailStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[EmailType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EmailType] FROM [nvarchar](60) NULL
/****** Object:  UserDefinedDataType [dbo].[EmailTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EmailTypeType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[EmpAbsenceStatType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EmpAbsenceStatType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[EmpCategoryType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EmpCategoryType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[EmpContactPriorityType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EmpContactPriorityType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[EmpDepTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EmpDepTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[EmpJobCoPoRmaProjPsTrnNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EmpJobCoPoRmaProjPsTrnNumType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[EmpNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EmpNameType] FROM [nvarchar](50) NULL
/****** Object:  UserDefinedDataType [dbo].[EmpNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EmpNumType] FROM [nvarchar](7) NULL
/****** Object:  UserDefinedDataType [dbo].[EmpPrbankRankType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EmpPrbankRankType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[EmpStatTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EmpStatTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[EmpStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EmpStatusType] FROM [nvarchar](3) NULL
/****** Object:  UserDefinedDataType [dbo].[EmpTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EmpTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[EmpVendNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EmpVendNumType] FROM [nvarchar](7) NULL
/****** Object:  UserDefinedDataType [dbo].[EncryptedClientPasswordType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EncryptedClientPasswordType] FROM [nvarchar](150) NULL
/****** Object:  UserDefinedDataType [dbo].[EncryptedPasswordType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EncryptedPasswordType] FROM [nvarchar](70) NULL
/****** Object:  UserDefinedDataType [dbo].[EndUserType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EndUserType] FROM [nvarchar](6) NULL
/****** Object:  UserDefinedDataType [dbo].[EndUserTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EndUserTypeType] FROM [nvarchar](6) NULL
/****** Object:  UserDefinedDataType [dbo].[ErrLogTrxNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ErrLogTrxNumType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[ESBAccountingEntityIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ESBAccountingEntityIdType] FROM [nvarchar](250) NULL
/****** Object:  UserDefinedDataType [dbo].[ESBAgencyRoleType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ESBAgencyRoleType] FROM [nvarchar](250) NULL
/****** Object:  UserDefinedDataType [dbo].[ESBDrillbackLidType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ESBDrillbackLidType] FROM [nvarchar](250) NULL
/****** Object:  UserDefinedDataType [dbo].[ESBListYesNoType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ESBListYesNoType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[ESBLocationIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ESBLocationIdType] FROM [nvarchar](250) NULL
/****** Object:  UserDefinedDataType [dbo].[ESBLogicalIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ESBLogicalIdType] FROM [nvarchar](250) NULL
/****** Object:  UserDefinedDataType [dbo].[ESBMessageId2Type]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ESBMessageId2Type] FROM [nvarchar](250) NULL
/****** Object:  UserDefinedDataType [dbo].[ESBMessageIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ESBMessageIdType] FROM [bigint] NULL
/****** Object:  UserDefinedDataType [dbo].[ESBMessageXMLType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ESBMessageXMLType] FROM [varbinary](max) NULL
/****** Object:  UserDefinedDataType [dbo].[ESBNounIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ESBNounIdType] FROM [nvarchar](250) NULL
/****** Object:  UserDefinedDataType [dbo].[ESBNounNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ESBNounNameType] FROM [nvarchar](250) NULL
/****** Object:  UserDefinedDataType [dbo].[ESBPriorityType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ESBPriorityType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[ESBPropertyNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ESBPropertyNameType] FROM [nvarchar](250) NULL
/****** Object:  UserDefinedDataType [dbo].[ESBPropertyValueType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ESBPropertyValueType] FROM [nvarchar](250) NULL
/****** Object:  UserDefinedDataType [dbo].[ESBRevisionIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ESBRevisionIdType] FROM [nvarchar](250) NULL
/****** Object:  UserDefinedDataType [dbo].[ESBSystemIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ESBSystemIdType] FROM [nvarchar](250) NULL
/****** Object:  UserDefinedDataType [dbo].[ESBTenantIDType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ESBTenantIDType] FROM [nvarchar](250) NULL
/****** Object:  UserDefinedDataType [dbo].[ESBVariationIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ESBVariationIdType] FROM [nvarchar](250) NULL
/****** Object:  UserDefinedDataType [dbo].[EstJobPrefixType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EstJobPrefixType] FROM [nvarchar](7) NULL
/****** Object:  UserDefinedDataType [dbo].[EstNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EstNumType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[EstPrefixType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EstPrefixType] FROM [nvarchar](7) NULL
/****** Object:  UserDefinedDataType [dbo].[EthnicIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EthnicIdType] FROM [nvarchar](2) NULL
/****** Object:  UserDefinedDataType [dbo].[EvalCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EvalCodeType] FROM [nvarchar](3) NULL
/****** Object:  UserDefinedDataType [dbo].[EventActionParmList2Type]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EventActionParmList2Type] FROM [nvarchar](max) NULL
/****** Object:  UserDefinedDataType [dbo].[EventActionParmListType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EventActionParmListType] FROM [nvarchar](max) NULL
/****** Object:  UserDefinedDataType [dbo].[EventActionTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EventActionTypeType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[EventAttachmentNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EventAttachmentNameType] FROM [nvarchar](132) NULL
/****** Object:  UserDefinedDataType [dbo].[EventBigIntervalType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EventBigIntervalType] FROM [bigint] NULL
/****** Object:  UserDefinedDataType [dbo].[EventBigQuorumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EventBigQuorumType] FROM [bigint] NULL
/****** Object:  UserDefinedDataType [dbo].[EventChronoType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EventChronoType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[EventConditionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EventConditionType] FROM [nvarchar](3000) NULL
/****** Object:  UserDefinedDataType [dbo].[EventExpressionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EventExpressionType] FROM [nvarchar](max) NULL
/****** Object:  UserDefinedDataType [dbo].[EventGroupIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EventGroupIdType] FROM [uniqueidentifier] NULL
/****** Object:  UserDefinedDataType [dbo].[EventInitialStateNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EventInitialStateNameType] FROM [nvarchar](50) NULL
/****** Object:  UserDefinedDataType [dbo].[EventIntervalType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EventIntervalType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[EventNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EventNameType] FROM [nvarchar](50) NULL
/****** Object:  UserDefinedDataType [dbo].[EventObjectListType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EventObjectListType] FROM [nvarchar](255) NULL
/****** Object:  UserDefinedDataType [dbo].[EventObjectType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EventObjectType] FROM [nvarchar](50) NULL
/****** Object:  UserDefinedDataType [dbo].[EventOccurrencesType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EventOccurrencesType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[EventParmIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EventParmIdType] FROM [uniqueidentifier] NULL
/****** Object:  UserDefinedDataType [dbo].[EventQueuedObjectTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EventQueuedObjectTypeType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[EventQueueIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EventQueueIdType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[EventQuorumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EventQuorumType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[EventResultType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EventResultType] FROM [nvarchar](512) NULL
/****** Object:  UserDefinedDataType [dbo].[EventRevisionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EventRevisionType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[EventSequenceType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EventSequenceType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[EventStateIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EventStateIdType] FROM [uniqueidentifier] NULL
/****** Object:  UserDefinedDataType [dbo].[EventStateType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EventStateType] FROM [nvarchar](80) NULL
/****** Object:  UserDefinedDataType [dbo].[EventStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EventStatusType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[EventTitleType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EventTitleType] FROM [nvarchar](80) NULL
/****** Object:  UserDefinedDataType [dbo].[EventVariableAccessType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EventVariableAccessType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[EventVariableNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EventVariableNameType] FROM [nvarchar](100) NULL
/****** Object:  UserDefinedDataType [dbo].[EventVariableValueType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EventVariableValueType] FROM [nvarchar](max) NULL
/****** Object:  UserDefinedDataType [dbo].[EventVotingChoiceStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EventVotingChoiceStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[EventVotingMarginType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EventVotingMarginType] FROM [decimal](5, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[EventVotingMinimumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EventVotingMinimumType] FROM [decimal](6, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[EventVotingRuleType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EventVotingRuleType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[ExamScoreType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ExamScoreType] FROM [decimal](5, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[ExchangeNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ExchangeNameType] FROM [nvarchar](12) NULL
/****** Object:  UserDefinedDataType [dbo].[ExchRateType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ExchRateType] FROM [decimal](12, 7) NULL
/****** Object:  UserDefinedDataType [dbo].[ExciseTaxPercentType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ExciseTaxPercentType] FROM [decimal](4, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[ExcMessageType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ExcMessageType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[ExemptionsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ExemptionsType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[ExpCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ExpCodeType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[ExpectedNumberOfLeadsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ExpectedNumberOfLeadsType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[ExpectedReceiptCoLineJobSuffixType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ExpectedReceiptCoLineJobSuffixType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[ExpectedReceiptCoNumJobType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ExpectedReceiptCoNumJobType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[ExpectedReceiptCoReleaseType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ExpectedReceiptCoReleaseType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[ExpectedReceiptOJType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ExpectedReceiptOJType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ExpenseTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ExpenseTypeType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[ExportComplianceProgramType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ExportComplianceProgramType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[ExportDocIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ExportDocIdType] FROM [nvarchar](25) NULL
/****** Object:  UserDefinedDataType [dbo].[ExportDocumentTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ExportDocumentTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ExtDataStoreAccessKeyType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ExtDataStoreAccessKeyType] FROM [nvarchar](440) NULL
/****** Object:  UserDefinedDataType [dbo].[ExtDataStoreLifetimeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ExtDataStoreLifetimeType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[ExtDataStoreValueType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ExtDataStoreValueType] FROM [nvarchar](max) NULL
/****** Object:  UserDefinedDataType [dbo].[ExternalGUIDType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ExternalGUIDType] FROM [uniqueidentifier] NULL
/****** Object:  UserDefinedDataType [dbo].[ExternalMessageEntityType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ExternalMessageEntityType] FROM [nvarchar](80) NULL
/****** Object:  UserDefinedDataType [dbo].[ExternalPickUpSheetIDType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ExternalPickUpSheetIDType] FROM [nvarchar](25) NULL
/****** Object:  UserDefinedDataType [dbo].[ExternalReqNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ExternalReqNumType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[ExternalShipmentStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ExternalShipmentStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ExtFinExportCounterType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ExtFinExportCounterType] FROM [decimal](24, 0) NULL
/****** Object:  UserDefinedDataType [dbo].[ExtFinNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ExtFinNameType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[ExtPayPeriodNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ExtPayPeriodNameType] FROM [nvarchar](100) NULL
/****** Object:  UserDefinedDataType [dbo].[ExtSystemTransIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ExtSystemTransIdType] FROM [nvarchar](80) NULL
/****** Object:  UserDefinedDataType [dbo].[EZBCycleType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[EZBCycleType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FaClassType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FaClassType] FROM [nvarchar](5) NULL
/****** Object:  UserDefinedDataType [dbo].[FaCostSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FaCostSeqType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[FaCostTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FaCostTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FaDeprPercent2Type]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FaDeprPercent2Type] FROM [decimal](6, 3) NULL
/****** Object:  UserDefinedDataType [dbo].[FaDeprPercentType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FaDeprPercentType] FROM [decimal](5, 3) NULL
/****** Object:  UserDefinedDataType [dbo].[FaDeprPeriodType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FaDeprPeriodType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[FaDeprSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FaDeprSeqType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[FaDeptColumnType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FaDeptColumnType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[FaDistSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FaDistSeqType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[FaInvNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FaInvNumType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[FamilyCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FamilyCodeType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[FaModelType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FaModelType] FROM [nvarchar](15) NULL
/****** Object:  UserDefinedDataType [dbo].[FaNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FaNumType] FROM [nvarchar](12) NULL
/****** Object:  UserDefinedDataType [dbo].[FaPeriodsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FaPeriodsType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[FaSerialType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FaSerialType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[FaStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FaStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FaTagType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FaTagType] FROM [nvarchar](12) NULL
/****** Object:  UserDefinedDataType [dbo].[FaTranSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FaTranSeqType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[FaTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FaTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FaUnitsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FaUnitsType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[FaUsefulLifeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FaUsefulLifeType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[FaxHeaderType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FaxHeaderType] FROM [nvarchar](1000) NULL
/****** Object:  UserDefinedDataType [dbo].[FeatBlankType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FeatBlankType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FeatCodeLengthType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FeatCodeLengthType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[FeatCodeOffsetType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FeatCodeOffsetType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[FeatQualStringType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FeatQualStringType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[FeatRankType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FeatRankType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[FeatSelQtyType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FeatSelQtyType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[FeatStrType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FeatStrType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[FeatSuffixType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FeatSuffixType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[FeatTemplateType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FeatTemplateType] FROM [nvarchar](55) NULL
/****** Object:  UserDefinedDataType [dbo].[FeatureType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FeatureType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[FileServerTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FileServerTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FileSpecType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FileSpecType] FROM [nvarchar](255) NULL
/****** Object:  UserDefinedDataType [dbo].[FilterColumnValueType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FilterColumnValueType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[FilterSequenceType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FilterSequenceType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[FilterType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FilterType] FROM [nvarchar](750) NULL
/****** Object:  UserDefinedDataType [dbo].[FinGroupSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FinGroupSeqType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[FinPeriodType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FinPeriodType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[FinStmtColType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FinStmtColType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[FinStmtColWidthType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FinStmtColWidthType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[FinStmtGroupNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FinStmtGroupNumType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[FinStmtPosType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FinStmtPosType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[FinStmtSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FinStmtSeqType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[FIPSCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FIPSCodeType] FROM [nvarchar](2) NULL
/****** Object:  UserDefinedDataType [dbo].[FirmPlnTargetType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FirmPlnTargetType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FiscalReportingSystemType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FiscalReportingSystemType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[FiscalReportingSystemTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FiscalReportingSystemTypeType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[FiscalYearType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FiscalYearType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[FixedHoursType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FixedHoursType] FROM [decimal](8, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[Flag]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[Flag] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[FlagIeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FlagIeType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[FlagNyType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FlagNyType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[FldNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FldNameType] FROM [nvarchar](32) NULL
/****** Object:  UserDefinedDataType [dbo].[FlowTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FlowTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FOBType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FOBType] FROM [nvarchar](60) NULL
/****** Object:  UserDefinedDataType [dbo].[FolderNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FolderNameType] FROM [nvarchar](32) NULL
/****** Object:  UserDefinedDataType [dbo].[FolderTemplateType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FolderTemplateType] FROM [nvarchar](256) NULL
/****** Object:  UserDefinedDataType [dbo].[FontColorType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FontColorType] FROM [nchar](11) NULL
/****** Object:  UserDefinedDataType [dbo].[FontDescriptorType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FontDescriptorType] FROM [nvarchar](64) NULL
/****** Object:  UserDefinedDataType [dbo].[ForecastStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ForecastStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ForeignKeyPrefixType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ForeignKeyPrefixType] FROM [nvarchar](22) NULL
/****** Object:  UserDefinedDataType [dbo].[FormEditorType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FormEditorType] FROM [nvarchar](750) NULL
/****** Object:  UserDefinedDataType [dbo].[FormJobType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FormJobType] FROM [nvarchar](50) NULL
/****** Object:  UserDefinedDataType [dbo].[FormNameOrCaptionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FormNameOrCaptionType] FROM [nvarchar](50) NULL
/****** Object:  UserDefinedDataType [dbo].[FormNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FormNameType] FROM [nvarchar](50) NULL
/****** Object:  UserDefinedDataType [dbo].[FormPositionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FormPositionType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[FormSizeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FormSizeType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[FreightChargeMethodType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FreightChargeMethodType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[FSACMItemStatType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSACMItemStatType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSACMNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSACMNumType] FROM [nvarchar](19) NULL
/****** Object:  UserDefinedDataType [dbo].[FSACMStatType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSACMStatType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSACMTransDateFromType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSACMTransDateFromType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSAlertIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSAlertIdType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[FSApptStatType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSApptStatType] FROM [nvarchar](15) NULL
/****** Object:  UserDefinedDataType [dbo].[FSApptTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSApptTypeType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[FSAqmConnPwdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSAqmConnPwdType] FROM [nvarchar](max) NULL
/****** Object:  UserDefinedDataType [dbo].[FSAqmConnUserType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSAqmConnUserType] FROM [nvarchar](max) NULL
/****** Object:  UserDefinedDataType [dbo].[FSBChartOfAcctNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSBChartOfAcctNameType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[FSBNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSBNameType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[FSBPeriodNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSBPeriodNameType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[FSCalcLineTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSCalcLineTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSCgsRevLocType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSCgsRevLocType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSChangeBranchType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSChangeBranchType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[FSColorType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSColorType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[FSColumnWidthType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSColumnWidthType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[FSCommDuePayType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSCommDuePayType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSCompIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSCompIdType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[FSConfigUpdateMethod]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSConfigUpdateMethod] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSConfigUpdateMethodType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSConfigUpdateMethodType] FROM [nvarchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSContactNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSContactNumType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[FSContBillFreqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSContBillFreqType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSContBillTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSContBillTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSContLineType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSContLineType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[FSContMntStatType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSContMntStatType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSContPriceBasisType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSContPriceBasisType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSContractType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSContractType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[FSContServTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSContServTypeType] FROM [nvarchar](3) NULL
/****** Object:  UserDefinedDataType [dbo].[FSContStatType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSContStatType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[FSContSurchargeFreqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSContSurchargeFreqType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSContSurchargeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSContSurchargeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSContSurchargeTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSContSurchargeTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSContUnitOfRateType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSContUnitOfRateType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSCustomerDirectionsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSCustomerDirectionsType] FROM [nvarchar](max) NULL
/****** Object:  UserDefinedDataType [dbo].[FSDayOfMonthType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSDayOfMonthType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[FSDaysToMonthType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSDaysToMonthType] FROM [decimal](38, 10) NULL
/****** Object:  UserDefinedDataType [dbo].[FSDaysToWeekType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSDaysToWeekType] FROM [decimal](38, 10) NULL
/****** Object:  UserDefinedDataType [dbo].[FSDefaultFromType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSDefaultFromType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSDepAppTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSDepAppTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSDeptType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSDeptType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[FSDispatchEmailType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSDispatchEmailType] FROM [nvarchar](max) NULL
/****** Object:  UserDefinedDataType [dbo].[FSDispatchEventCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSDispatchEventCodeType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[FSDispCfgLevelsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSDispCfgLevelsType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[FSDragDropModeType ]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSDragDropModeType ] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[FSDriversLicenseType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSDriversLicenseType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[FSDropShipTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSDropShipTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSDrpLowLevelType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSDrpLowLevelType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[FSDurationType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSDurationType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[FSDurationTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSDurationTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSEmailCommandType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSEmailCommandType] FROM [nvarchar](55) NULL
/****** Object:  UserDefinedDataType [dbo].[FSEmailFlagType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSEmailFlagType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSEmailServerType ]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSEmailServerType ] FROM [nvarchar](100) NULL
/****** Object:  UserDefinedDataType [dbo].[FSEmailTypeType ]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSEmailTypeType ] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[FSEscalBasisType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSEscalBasisType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSEscalFreqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSEscalFreqType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSEventActivitiesType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSEventActivitiesType] FROM [nvarchar](max) NULL
/****** Object:  UserDefinedDataType [dbo].[FSEventCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSEventCodeType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[FSEventTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSEventTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSExchangeDaysType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSExchangeDaysType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[FSExchangeVersionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSExchangeVersionType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[FSFieldDescriptionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSFieldDescriptionType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[FSFieldIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSFieldIdType] FROM [nvarchar](25) NULL
/****** Object:  UserDefinedDataType [dbo].[FSFieldOrderType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSFieldOrderType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[FSFilterNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSFilterNameType] FROM [nvarchar](100) NULL
/****** Object:  UserDefinedDataType [dbo].[FSFilterType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSFilterType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[FSFontNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSFontNameType] FROM [nvarchar](100) NULL
/****** Object:  UserDefinedDataType [dbo].[FSFontSizeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSFontSizeType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[FSGPSLocType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSGPSLocType] FROM [decimal](21, 10) NULL
/****** Object:  UserDefinedDataType [dbo].[FSHighValueType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSHighValueType] FROM [decimal](38, 10) NULL
/****** Object:  UserDefinedDataType [dbo].[FSHrsToDayType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSHrsToDayType] FROM [decimal](38, 10) NULL
/****** Object:  UserDefinedDataType [dbo].[FSIncNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSIncNumType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[FSIncPriorCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSIncPriorCodeType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[FSInitialViewType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSInitialViewType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[FSInspectRefNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSInspectRefNumType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[FSInspectRefTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSInspectRefTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSInspectTaskType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSInspectTaskType] FROM [nvarchar](50) NULL
/****** Object:  UserDefinedDataType [dbo].[FSInspectTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSInspectTypeType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[FSInvTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSInvTypeType] FROM [nvarchar](4) NULL
/****** Object:  UserDefinedDataType [dbo].[FSKBGeneralType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSKBGeneralType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[FSKBKeywordType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSKBKeywordType] FROM [nchar](255) NULL
/****** Object:  UserDefinedDataType [dbo].[FSKBSpecificType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSKBSpecificType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[FSKBStatType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSKBStatType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSKBSummaryType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSKBSummaryType] FROM [nchar](1000) NULL
/****** Object:  UserDefinedDataType [dbo].[FSKBTextType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSKBTextType] FROM [nvarchar](max) NULL
/****** Object:  UserDefinedDataType [dbo].[FSKnowNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSKnowNumType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[FSLangCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSLangCodeType] FROM [nvarchar](6) NULL
/****** Object:  UserDefinedDataType [dbo].[FSLiCertType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSLiCertType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[FSListManAutoType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSListManAutoType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSLocalSplitterDistanceType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSLocalSplitterDistanceType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[FSLowValueType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSLowValueType] FROM [decimal](38, 10) NULL
/****** Object:  UserDefinedDataType [dbo].[FSMaintFreqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSMaintFreqType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSMeasResponseTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSMeasResponseTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSMeasTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSMeasTypeType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[FSMeterAmtType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSMeterAmtType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[FSMeterLabelType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSMeterLabelType] FROM [nvarchar](12) NULL
/****** Object:  UserDefinedDataType [dbo].[FSMiscCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSMiscCodeType] FROM [nvarchar](3) NULL
/****** Object:  UserDefinedDataType [dbo].[FSMonthType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSMonthType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[FSMultiDayMaxHrsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSMultiDayMaxHrsType] FROM [decimal](38, 10) NULL
/****** Object:  UserDefinedDataType [dbo].[FSMultiDayMethodType ]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSMultiDayMethodType ] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[FSMultiDayMinHrsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSMultiDayMinHrsType] FROM [decimal](38, 10) NULL
/****** Object:  UserDefinedDataType [dbo].[FSNotesType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSNotesType] FROM [nvarchar](max) NULL
/****** Object:  UserDefinedDataType [dbo].[FSOperCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSOperCodeType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[FSParmKeyType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSParmKeyType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[FSPartnerDispType ]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSPartnerDispType ] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[FSPartnerEndAddressType ]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSPartnerEndAddressType ] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[FSPartnerStartAddressType ]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSPartnerStartAddressType ] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[FSPartnerType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSPartnerType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[FSPartsrecEmailBodyType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSPartsrecEmailBodyType] FROM [nvarchar](max) NULL
/****** Object:  UserDefinedDataType [dbo].[FSPayTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSPayTypeType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[FSPctType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSPctType] FROM [decimal](4, 1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSPollIntervalType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSPollIntervalType] FROM [decimal](10, 3) NULL
/****** Object:  UserDefinedDataType [dbo].[FSPriorDateBasisType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSPriorDateBasisType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSReasonActivitiesType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSReasonActivitiesType] FROM [nvarchar](max) NULL
/****** Object:  UserDefinedDataType [dbo].[FSReasonNotesType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSReasonNotesType] FROM [nvarchar](max) NULL
/****** Object:  UserDefinedDataType [dbo].[FSReasonType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSReasonType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[FSReasonTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSReasonTypeType] FROM [nvarchar](15) NULL
/****** Object:  UserDefinedDataType [dbo].[FSReconBatchIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSReconBatchIdType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[FSRefLineType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSRefLineType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[FSRefNumChar150Type]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSRefNumChar150Type] FROM [nvarchar](150) NULL
/****** Object:  UserDefinedDataType [dbo].[FSRefNumChar200Type]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSRefNumChar200Type] FROM [nvarchar](200) NULL
/****** Object:  UserDefinedDataType [dbo].[FSRefNumChar30Type]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSRefNumChar30Type] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[FSRefNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSRefNumType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[FSRefReleaseType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSRefReleaseType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[FSRefreshIntervalType ]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSRefreshIntervalType ] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[FSRefSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSRefSeqType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[FSRefTypeCEVType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSRefTypeCEVType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSRefTypeChar100Type]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSRefTypeChar100Type] FROM [nvarchar](100) NULL
/****** Object:  UserDefinedDataType [dbo].[FSRefTypeChar10Type]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSRefTypeChar10Type] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[FSRefTypeCKOPSType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSRefTypeCKOPSType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSRefTypeIJKMNOPRType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSRefTypeIJKMNOPRType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSRefTypeIJKMOPRTType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSRefTypeIJKMOPRTType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSRefTypeIJPRT]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSRefTypeIJPRT] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSRefTypeIntType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSRefTypeIntType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[FSRefTypeJKMOPRSType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSRefTypeJKMOPRSType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSRefTypeJKNSType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSRefTypeJKNSType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSRefTypeKOType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSRefTypeKOType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSRefTypeNSType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSRefTypeNSType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSRefTypeNSUType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSRefTypeNSUType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSRefTypeOPSType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSRefTypeOPSType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSRefTypePRType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSRefTypePRType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSRegionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSRegionType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[FSReimbBatchIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSReimbBatchIdType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[FSReimbMethodType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSReimbMethodType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSReimbStatType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSReimbStatType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSRemoteIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSRemoteIdType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[FSResolutionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSResolutionType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[FSRowHeightType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSRowHeightType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[FSSchedBasisType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSSchedBasisType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSSchedDateFormatType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSSchedDateFormatType] FROM [nvarchar](50) NULL
/****** Object:  UserDefinedDataType [dbo].[FSSchedDaysBackwardType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSSchedDaysBackwardType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[FSSchedDaysForwardType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSSchedDaysForwardType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[FSSchedMultiMethodType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSSchedMultiMethodType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[FSSchedReminderType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSSchedReminderType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[FSScheduleIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSScheduleIdType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[FSSchedViewType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSSchedViewType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[FSSectionCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSSectionCodeType] FROM [nvarchar](50) NULL
/****** Object:  UserDefinedDataType [dbo].[FSSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSSeqType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[FSSkillType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSSkillType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[FSSROBillCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSSROBillCodeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSSROBillStatType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSSROBillStatType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSSROBillTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSSROBillTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSSROLineTransType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSSROLineTransType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSSROLineType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSSROLineType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[FSSROLineTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSSROLineTypeType] FROM [nvarchar](9) NULL
/****** Object:  UserDefinedDataType [dbo].[FSSROMatlTransTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSSROMatlTransTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSSROMatlUsageTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSSROMatlUsageTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSSRONumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSSRONumType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[FSSROOperStatType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSSROOperStatType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSSROOperType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSSROOperType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[FSSROStatType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSSROStatType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSSROTransNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSSROTransNumType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[FSSROTransTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSSROTransTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSSROTypeOfTransType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSSROTypeOfTransType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSSROTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSSROTypeType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[FSStatCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSStatCodeType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[FSSubTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSSubTypeType] FROM [nvarchar](150) NULL
/****** Object:  UserDefinedDataType [dbo].[FSTotalPeriodsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSTotalPeriodsType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[FSUfLRMDeliveryOrderNumberType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSUfLRMDeliveryOrderNumberType] FROM [nvarchar](15) NULL
/****** Object:  UserDefinedDataType [dbo].[FSUnitMfgType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSUnitMfgType] FROM [nvarchar](25) NULL
/****** Object:  UserDefinedDataType [dbo].[FSUnitStatCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSUnitStatCodeType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[FSUnitStatType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSUnitStatType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[FSUsernameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSUsernameType] FROM [nvarchar](100) NULL
/****** Object:  UserDefinedDataType [dbo].[FSUsrNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSUsrNumType] FROM [nvarchar](7) NULL
/****** Object:  UserDefinedDataType [dbo].[FSUsrSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSUsrSeqType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[FSValueType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSValueType] FROM [nvarchar](38) NULL
/****** Object:  UserDefinedDataType [dbo].[FSWarrCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSWarrCodeType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[FSWarrTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSWarrTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[FSWorkCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FSWorkCodeType] FROM [nvarchar](3) NULL
/****** Object:  UserDefinedDataType [dbo].[FullObjectPathType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FullObjectPathType] FROM [nvarchar](max) NULL
/****** Object:  UserDefinedDataType [dbo].[FunctIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[FunctIdType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[GarnishTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[GarnishTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[GenericCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[GenericCodeType] FROM [nvarchar](6) NULL
/****** Object:  UserDefinedDataType [dbo].[GenericDate]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[GenericDate] FROM [datetime] NULL
/****** Object:  UserDefinedDataType [dbo].[GenericDateType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[GenericDateType] FROM [datetime] NULL
/****** Object:  UserDefinedDataType [dbo].[GenericDecimalType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[GenericDecimalType] FROM [decimal](38, 10) NULL
/****** Object:  UserDefinedDataType [dbo].[GenericFloatType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[GenericFloatType] FROM [decimal](24, 10) NULL
/****** Object:  UserDefinedDataType [dbo].[GenericIntType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[GenericIntType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[GenericKeyType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[GenericKeyType] FROM [nvarchar](50) NULL
/****** Object:  UserDefinedDataType [dbo].[GenericMedCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[GenericMedCodeType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[GenericNoType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[GenericNoType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[GenericTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[GenericTypeType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[GivenNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[GivenNameType] FROM [nvarchar](15) NULL
/****** Object:  UserDefinedDataType [dbo].[GlbankRefTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[GlbankRefTypeType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[GlbankTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[GlbankTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[GlCheckNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[GlCheckNumType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[GlobalPlanningModeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[GlobalPlanningModeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[GlrpthcCalcTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[GlrpthcCalcTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[GlrpthcRangeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[GlrpthcRangeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[GlrpthcSourceType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[GlrpthcSourceType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[GlrptlAcctSortType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[GlrptlAcctSortType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[GlrptlBalTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[GlrptlBalTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[GlrptlcParenType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[GlrptlcParenType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[GlrptlcRangeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[GlrptlcRangeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[GlrptlPrintType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[GlrptlPrintType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[GlrptlTextAlignType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[GlrptlTextAlignType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[GlrptlTextPrintType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[GlrptlTextPrintType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[GlrptlTextTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[GlrptlTextTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[GlrptlTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[GlrptlTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[GPAType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[GPAType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[GracePeriodType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[GracePeriodType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[GraduationYearType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[GraduationYearType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[GrnLineType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[GrnLineType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[GrnNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[GrnNumType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[GrnPackNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[GrnPackNumType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[GrnStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[GrnStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[GroupnameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[GroupnameType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[HeatNumberType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[HeatNumberType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[HelpContextType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[HelpContextType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[HideParentGridColumnsOptionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[HideParentGridColumnsOptionType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[HierarchyType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[HierarchyType] FROM [nvarchar](255) NULL
/****** Object:  UserDefinedDataType [dbo].[HighLowCharType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[HighLowCharType] FROM [nvarchar](4000) NULL
/****** Object:  UserDefinedDataType [dbo].[HolidayOffsetMethodType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[HolidayOffsetMethodType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[HorizonWorkDaysType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[HorizonWorkDaysType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[HourAndMinuteType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[HourAndMinuteType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[HourlyRateType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[HourlyRateType] FROM [decimal](6, 3) NULL
/****** Object:  UserDefinedDataType [dbo].[HoursOffType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[HoursOffType] FROM [decimal](8, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[HTSCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[HTSCodeType] FROM [nvarchar](50) NULL
/****** Object:  UserDefinedDataType [dbo].[HugeTransNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[HugeTransNumType] FROM [decimal](11, 0) NULL
/****** Object:  UserDefinedDataType [dbo].[HugeWeightType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[HugeWeightType] FROM [decimal](13, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[I9AdmissionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[I9AdmissionType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[I9AuthorityType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[I9AuthorityType] FROM [nvarchar](35) NULL
/****** Object:  UserDefinedDataType [dbo].[I9BoxNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[I9BoxNumType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[I9DocIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[I9DocIdType] FROM [nvarchar](24) NULL
/****** Object:  UserDefinedDataType [dbo].[I9FormType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[I9FormType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[I9ListIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[I9ListIdType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ImageIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ImageIdType] FROM [nvarchar](60) NULL
/****** Object:  UserDefinedDataType [dbo].[ImageNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ImageNameType] FROM [nvarchar](250) NULL
/****** Object:  UserDefinedDataType [dbo].[ImageType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ImageType] FROM [varbinary](max) NULL
/****** Object:  UserDefinedDataType [dbo].[ImportDocIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ImportDocIdType] FROM [nvarchar](25) NULL
/****** Object:  UserDefinedDataType [dbo].[ImportedAmountType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ImportedAmountType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[ImportedPackagesType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ImportedPackagesType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[ImportedWeightType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ImportedWeightType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[ImportErrorNumberType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ImportErrorNumberType] FROM [decimal](11, 0) NULL
/****** Object:  UserDefinedDataType [dbo].[ImportSequenceType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ImportSequenceType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[IndCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[IndCodeType] FROM [nvarchar](3) NULL
/****** Object:  UserDefinedDataType [dbo].[IndexNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[IndexNameType] FROM [nvarchar](32) NULL
/****** Object:  UserDefinedDataType [dbo].[Infobar]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[Infobar] FROM [nvarchar](2800) NULL
/****** Object:  UserDefinedDataType [dbo].[InfobarType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[InfobarType] FROM [nvarchar](2800) NULL
/****** Object:  UserDefinedDataType [dbo].[InitialType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[InitialType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[InlineListType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[InlineListType] FROM [nvarchar](max) NULL
/****** Object:  UserDefinedDataType [dbo].[InputMaskType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[InputMaskType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[InsuranceAgeClassType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[InsuranceAgeClassType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[InsuranceAgeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[InsuranceAgeType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[InsuranceAmountType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[InsuranceAmountType] FROM [decimal](9, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[InsuranceCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[InsuranceCodeType] FROM [nvarchar](3) NULL
/****** Object:  UserDefinedDataType [dbo].[InsuranceCompContMonthlyType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[InsuranceCompContMonthlyType] FROM [decimal](6, 3) NULL
/****** Object:  UserDefinedDataType [dbo].[InsuranceCompContType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[InsuranceCompContType] FROM [decimal](8, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[InsuranceCostType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[InsuranceCostType] FROM [decimal](8, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[InsuranceCoverageClassType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[InsuranceCoverageClassType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[InsuranceMaxMinType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[InsuranceMaxMinType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[InsurancePolicyType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[InsurancePolicyType] FROM [nvarchar](12) NULL
/****** Object:  UserDefinedDataType [dbo].[InsurancePriceMonthlyType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[InsurancePriceMonthlyType] FROM [decimal](6, 3) NULL
/****** Object:  UserDefinedDataType [dbo].[InsuranceRoundingDivisorType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[InsuranceRoundingDivisorType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[InsuranceTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[InsuranceTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[InteractionIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[InteractionIdType] FROM [decimal](11, 0) NULL
/****** Object:  UserDefinedDataType [dbo].[InteractionRefTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[InteractionRefTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[InteractionTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[InteractionTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[InternationalBankAccountType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[InternationalBankAccountType] FROM [nvarchar](34) NULL
/****** Object:  UserDefinedDataType [dbo].[InternetMediaTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[InternetMediaTypeType] FROM [nvarchar](255) NULL
/****** Object:  UserDefinedDataType [dbo].[IntersitePostingMethodType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[IntersitePostingMethodType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[IntLineNoType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[IntLineNoType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[IntranetNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[IntranetNameType] FROM [nvarchar](60) NULL
/****** Object:  UserDefinedDataType [dbo].[InvCategoryType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[InvCategoryType] FROM [nvarchar](15) NULL
/****** Object:  UserDefinedDataType [dbo].[InventoryReservationProcessStepType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[InventoryReservationProcessStepType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[InvFreqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[InvFreqType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[InvGroupType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[InvGroupType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[InvLineType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[InvLineType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[InvNumLength]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[InvNumLength] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[InvNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[InvNumType] FROM [nvarchar](12) NULL
/****** Object:  UserDefinedDataType [dbo].[InvNumVoucherType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[InvNumVoucherType] FROM [nvarchar](12) NULL
/****** Object:  UserDefinedDataType [dbo].[InvoicesType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[InvoicesType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[InvoiceTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[InvoiceTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[InvProSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[InvProSeqType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[InvSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[InvSeqType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[IRSFedIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[IRSFedIdType] FROM [nvarchar](13) NULL
/****** Object:  UserDefinedDataType [dbo].[IRSPayerControlType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[IRSPayerControlType] FROM [nchar](4) NULL
/****** Object:  UserDefinedDataType [dbo].[IRSTransCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[IRSTransCodeType] FROM [nvarchar](5) NULL
/****** Object:  UserDefinedDataType [dbo].[ISOBankTranCodeDomainType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ISOBankTranCodeDomainType] FROM [nvarchar](4) NULL
/****** Object:  UserDefinedDataType [dbo].[ISOCountryCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ISOCountryCodeType] FROM [nvarchar](2) NULL
/****** Object:  UserDefinedDataType [dbo].[ISONumericCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ISONumericCodeType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[ItemCatalogIDType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ItemCatalogIDType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[ItemCatalogStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ItemCatalogStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ItemCategoryHierarchyRankType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ItemCategoryHierarchyRankType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[ItemCategoryRankType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ItemCategoryRankType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[ItemCategoryType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ItemCategoryType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[ItemContentFactorType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ItemContentFactorType] FROM [decimal](9, 5) NULL
/****** Object:  UserDefinedDataType [dbo].[ItemContentPriceBasisType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ItemContentPriceBasisType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ItemContentType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ItemContentType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[ItemlocRankType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ItemlocRankType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[ItemPictureSequenceType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ItemPictureSequenceType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[ItemPortalPriceStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ItemPortalPriceStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ItemPriceRequestIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ItemPriceRequestIdType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[ItemPriceRequestStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ItemPriceRequestStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ItemPriceResponseOperatorType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ItemPriceResponseOperatorType] FROM [nvarchar](2) NULL
/****** Object:  UserDefinedDataType [dbo].[ItemPriceResponseStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ItemPriceResponseStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ItemStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ItemStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ItemType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ItemType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[ItemvendpriceStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ItemvendpriceStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ItemvendRankType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ItemvendRankType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[ItemWeightType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ItemWeightType] FROM [decimal](11, 5) NULL
/****** Object:  UserDefinedDataType [dbo].[JobDetailType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[JobDetailType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[JobGradeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[JobGradeType] FROM [nvarchar](3) NULL
/****** Object:  UserDefinedDataType [dbo].[JobIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[JobIdType] FROM [nvarchar](7) NULL
/****** Object:  UserDefinedDataType [dbo].[JobLaborAllocType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[JobLaborAllocType] FROM [nvarchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[JobmatlProjmatlSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[JobmatlProjmatlSeqType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[JobmatlRankType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[JobmatlRankType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[JobmatlSequenceType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[JobmatlSequenceType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[JobmatlUnitsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[JobmatlUnitsType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[JobPoProjReqTrnNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[JobPoProjReqTrnNumType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[JobPoReqNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[JobPoReqNumType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[JobPrefixType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[JobPrefixType] FROM [nvarchar](7) NULL
/****** Object:  UserDefinedDataType [dbo].[JobRefSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[JobRefSeqType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[JobStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[JobStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[JobTitleType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[JobTitleType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[JobtranClassType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[JobtranClassType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[JobtranSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[JobtranSeqType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[JobtranTransSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[JobtranTransSeqType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[JobtranTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[JobtranTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[JobType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[JobType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[JobTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[JobTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[JoinTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[JoinTypeType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[JourControlPrefixType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[JourControlPrefixType] FROM [nvarchar](2) NULL
/****** Object:  UserDefinedDataType [dbo].[JournalCompLevelType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[JournalCompLevelType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[JournalIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[JournalIdType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[JournalPostStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[JournalPostStatusType] FROM [nvarchar](13) NULL
/****** Object:  UserDefinedDataType [dbo].[JournalPostTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[JournalPostTypeType] FROM [nvarchar](2) NULL
/****** Object:  UserDefinedDataType [dbo].[JournalSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[JournalSeqType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[JournalTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[JournalTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[JP_AccountHolderNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[JP_AccountHolderNameType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[JP_AccountOwnerNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[JP_AccountOwnerNameType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[JP_AccountReferenceType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[JP_AccountReferenceType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[JP_BankBranchNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[JP_BankBranchNameType] FROM [nvarchar](15) NULL
/****** Object:  UserDefinedDataType [dbo].[JP_ConsumptionTaxHeaderLineMethodType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[JP_ConsumptionTaxHeaderLineMethodType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[JP_ConsumptionTaxRoundMethodType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[JP_ConsumptionTaxRoundMethodType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[JP_EFTBankFeeTermsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[JP_EFTBankFeeTermsType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[JP_EFTBankFeeTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[JP_EFTBankFeeTypeType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[JP_EFTFileStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[JP_EFTFileStatusType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[JP_InvBatchIDType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[JP_InvBatchIDType] FROM [decimal](19, 0) NULL
/****** Object:  UserDefinedDataType [dbo].[JP_ZenginAcctNoType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[JP_ZenginAcctNoType] FROM [decimal](7, 0) NULL
/****** Object:  UserDefinedDataType [dbo].[JP_ZenginBankCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[JP_ZenginBankCodeType] FROM [decimal](4, 0) NULL
/****** Object:  UserDefinedDataType [dbo].[JP_ZenginBranchCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[JP_ZenginBranchCodeType] FROM [decimal](3, 0) NULL
/****** Object:  UserDefinedDataType [dbo].[JSONStringType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[JSONStringType] FROM [nvarchar](max) NULL
/****** Object:  UserDefinedDataType [dbo].[KeyLengthType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[KeyLengthType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[KeyListType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[KeyListType] FROM [nvarchar](max) NULL
/****** Object:  UserDefinedDataType [dbo].[KeySequenceType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[KeySequenceType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[KeyValueType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[KeyValueType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[LabelTemplateNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LabelTemplateNameType] FROM [nvarchar](25) NULL
/****** Object:  UserDefinedDataType [dbo].[LabelType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LabelType] FROM [nvarchar](32) NULL
/****** Object:  UserDefinedDataType [dbo].[LangCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LangCodeType] FROM [nvarchar](3) NULL
/****** Object:  UserDefinedDataType [dbo].[LangLabelType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LangLabelType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[LanguageIDType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LanguageIDType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[LargeIntType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LargeIntType] FROM [decimal](38, 0) NULL
/****** Object:  UserDefinedDataType [dbo].[LargeURLType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LargeURLType] FROM [nvarchar](500) NULL
/****** Object:  UserDefinedDataType [dbo].[LasttranDescType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LasttranDescType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[LasttranKeyType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LasttranKeyType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[LastTranType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LastTranType] FROM [decimal](10, 0) NULL
/****** Object:  UserDefinedDataType [dbo].[LcAllocMethodType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LcAllocMethodType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[LcAllocPercentType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LcAllocPercentType] FROM [decimal](8, 5) NULL
/****** Object:  UserDefinedDataType [dbo].[LcAllocTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LcAllocTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[LcnNoType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LcnNoType] FROM [nvarchar](7) NULL
/****** Object:  UserDefinedDataType [dbo].[LcrConfirmationType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LcrConfirmationType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[LcrNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LcrNumType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[LcrStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LcrStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[LcTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LcTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[LeadIDType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LeadIDType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[LeadQualityType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LeadQualityType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[LeadStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LeadStatusType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[LeadTimeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LeadTimeType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[LedgerConsolidatedType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LedgerConsolidatedType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[LedgerTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LedgerTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[LicenseCheckSumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LicenseCheckSumType] FROM [nvarchar](1000) NULL
/****** Object:  UserDefinedDataType [dbo].[LicenseDocumentType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LicenseDocumentType] FROM [ntext] NULL
/****** Object:  UserDefinedDataType [dbo].[LicenseModuleDescType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LicenseModuleDescType] FROM [nvarchar](60) NULL
/****** Object:  UserDefinedDataType [dbo].[LicenseModuleNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LicenseModuleNameType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[LicenseObjectNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LicenseObjectNameType] FROM [nvarchar](50) NULL
/****** Object:  UserDefinedDataType [dbo].[LicenseObjectTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LicenseObjectTypeType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[LicertType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LicertType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[LineDiscType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LineDiscType] FROM [decimal](11, 8) NULL
/****** Object:  UserDefinedDataType [dbo].[LinesPerDocType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LinesPerDocType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[LineWeightType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LineWeightType] FROM [decimal](10, 4) NULL
/****** Object:  UserDefinedDataType [dbo].[LinkByType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LinkByType] FROM [nvarchar](750) NULL
/****** Object:  UserDefinedDataType [dbo].[LinkDatabaseType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LinkDatabaseType] FROM [nvarchar](50) NULL
/****** Object:  UserDefinedDataType [dbo].[ListActiveInactiveType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListActiveInactiveType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ListActStdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListActStdType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ListAddCreateBothType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListAddCreateBothType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ListAddSubType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListAddSubType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[ListAlwaysPromptNeverType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListAlwaysPromptNeverType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ListAlwaysSometimesNeverType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListAlwaysSometimesNeverType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ListAmountPercentType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListAmountPercentType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ListAndOrType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListAndOrType] FROM [nvarchar](3) NULL
/****** Object:  UserDefinedDataType [dbo].[ListAppEmpType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListAppEmpType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ListAppendReplaceIgnoreType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListAppendReplaceIgnoreType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ListAppendReplaceType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListAppendReplaceType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ListAppOvwType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListAppOvwType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[ListAutoManualNoneType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListAutoManualNoneType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ListBlankAsteriskType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListBlankAsteriskType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[ListBuySellType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListBuySellType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[ListCharIntType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListCharIntType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[ListCustItemType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListCustItemType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ListCustVendNullType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListCustVendNullType] FROM [nvarchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ListDailyWeeklyType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListDailyWeeklyType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ListDebitMemoAdjustmentType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListDebitMemoAdjustmentType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ListDefaultAmountPercentType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListDefaultAmountPercentType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ListDepIndType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListDepIndType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ListDirectIndirectNonExportType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListDirectIndirectNonExportType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ListEnableDisableType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListEnableDisableType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ListExistingCreateBothType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListExistingCreateBothType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ListHistoryType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListHistoryType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ListHoursPiecesType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListHoursPiecesType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ListHrsMinType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListHrsMinType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[ListLeftRightType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListLeftRightType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[ListLocalRemoteType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListLocalRemoteType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ListLocLotType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListLocLotType] FROM [nchar](3) NULL
/****** Object:  UserDefinedDataType [dbo].[ListLotSerialType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListLotSerialType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ListManAutoType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListManAutoType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ListOnOffType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListOnOffType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[ListOpersJobsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListOpersJobsType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[ListOrderDueType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListOrderDueType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ListPendingApprovedRejectedType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListPendingApprovedRejectedType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ListPowersOf10Type]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListPowersOf10Type] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[ListPrepaidCollectType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListPrepaidCollectType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ListPullUpRuleType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListPullUpRuleType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[ListRegenerationNetChangeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListRegenerationNetChangeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ListReportColumnType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListReportColumnType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[ListSafetyStockRuleType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListSafetyStockRuleType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[ListSingleAllType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListSingleAllType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ListSiteEntityType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListSiteEntityType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ListTimeFenceRuleType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListTimeFenceRuleType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[ListUpDownType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListUpDownType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ListWcUserType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListWcUserType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ListXBlankType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListXBlankType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[ListYearPeriodType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListYearPeriodType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ListYesNoType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ListYesNoType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[LoadHoursType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LoadHoursType] FROM [decimal](6, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[LoadOffsetHoursType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LoadOffsetHoursType] FROM [decimal](5, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[LoadTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LoadTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[LockoutDurationType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LockoutDurationType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[LocType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LocType] FROM [nvarchar](15) NULL
/****** Object:  UserDefinedDataType [dbo].[LocTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LocTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[LongAddress]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LongAddress] FROM [nvarchar](4000) NULL
/****** Object:  UserDefinedDataType [dbo].[LongDescType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LongDescType] FROM [nvarchar](255) NULL
/****** Object:  UserDefinedDataType [dbo].[LongList]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LongList] FROM [nvarchar](4000) NULL
/****** Object:  UserDefinedDataType [dbo].[LongListType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LongListType] FROM [nvarchar](4000) NULL
/****** Object:  UserDefinedDataType [dbo].[LongRowPointerType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LongRowPointerType] FROM [nvarchar](450) NULL
/****** Object:  UserDefinedDataType [dbo].[LongStringType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LongStringType] FROM [nvarchar](255) NULL
/****** Object:  UserDefinedDataType [dbo].[LotLengthType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LotLengthType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[LotPrefixType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LotPrefixType] FROM [nvarchar](15) NULL
/****** Object:  UserDefinedDataType [dbo].[LotSizeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LotSizeType] FROM [decimal](9, 3) NULL
/****** Object:  UserDefinedDataType [dbo].[LotType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LotType] FROM [nvarchar](15) NULL
/****** Object:  UserDefinedDataType [dbo].[LowLevelType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[LowLevelType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[MachineDownType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MachineDownType] FROM [decimal](6, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[MachineHourType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MachineHourType] FROM [decimal](5, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[MachNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MachNumType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[MachType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MachType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[MajorType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MajorType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[ManufacturerIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ManufacturerIdType] FROM [nvarchar](7) NULL
/****** Object:  UserDefinedDataType [dbo].[ManufacturerItemRankType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ManufacturerItemRankType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[ManufacturerItemType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ManufacturerItemType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[MapAPIKeyType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MapAPIKeyType] FROM [nvarchar](255) NULL
/****** Object:  UserDefinedDataType [dbo].[MapDistanceUnitType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MapDistanceUnitType] FROM [nvarchar](2) NULL
/****** Object:  UserDefinedDataType [dbo].[MapProviderType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MapProviderType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[MaritalStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MaritalStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[MarkupAttributeNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MarkupAttributeNameType] FROM [nvarchar](80) NULL
/****** Object:  UserDefinedDataType [dbo].[MarkupDocumentDocumentationClassType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MarkupDocumentDocumentationClassType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[MarkupDocumentDocumentationType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MarkupDocumentDocumentationType] FROM [nvarchar](max) NULL
/****** Object:  UserDefinedDataType [dbo].[MarkupDocumentNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MarkupDocumentNameType] FROM [nvarchar](60) NULL
/****** Object:  UserDefinedDataType [dbo].[MarkupElementSequenceType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MarkupElementSequenceType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[MarkupElementTagNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MarkupElementTagNameType] FROM [nvarchar](max) NULL
/****** Object:  UserDefinedDataType [dbo].[MarkupElementValueType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MarkupElementValueType] FROM [nvarchar](max) NULL
/****** Object:  UserDefinedDataType [dbo].[MarkupElementValueTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MarkupElementValueTypeType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[MarkupType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MarkupType] FROM [decimal](8, 3) NULL
/****** Object:  UserDefinedDataType [dbo].[MatlTransNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MatlTransNumType] FROM [decimal](10, 0) NULL
/****** Object:  UserDefinedDataType [dbo].[MatlTransTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MatlTransTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[MatlTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MatlTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[MaxBatchSizeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MaxBatchSizeType] FROM [int] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[MaxConcurrentBGTasksType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MaxConcurrentBGTasksType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[MdayHoursType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MdayHoursType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[MdayNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MdayNumType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[MdaysType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MdaysType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[MediumDescType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MediumDescType] FROM [nvarchar](72) NULL
/****** Object:  UserDefinedDataType [dbo].[MemoNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MemoNumType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[MenuCategoryType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MenuCategoryType] FROM [nchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[MenuItemIDType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MenuItemIDType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[MenuItemRankType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MenuItemRankType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[MessageBusIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MessageBusIdType] FROM [nvarchar](256) NULL
/****** Object:  UserDefinedDataType [dbo].[MessageCategoryType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MessageCategoryType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[MessageLanguageType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MessageLanguageType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[MessageNoPrefixType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MessageNoPrefixType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[MessageNoType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MessageNoType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[MessageRecipientListType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MessageRecipientListType] FROM [nvarchar](512) NULL
/****** Object:  UserDefinedDataType [dbo].[MessageResponseChoicesType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MessageResponseChoicesType] FROM [nvarchar](80) NULL
/****** Object:  UserDefinedDataType [dbo].[MessageResponseChoiceType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MessageResponseChoiceType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[MessageSubjectType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MessageSubjectType] FROM [nvarchar](80) NULL
/****** Object:  UserDefinedDataType [dbo].[MessageType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MessageType] FROM [nvarchar](400) NULL
/****** Object:  UserDefinedDataType [dbo].[MethodCallSpecType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MethodCallSpecType] FROM [nvarchar](max) NULL
/****** Object:  UserDefinedDataType [dbo].[MethodNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MethodNameType] FROM [nvarchar](80) NULL
/****** Object:  UserDefinedDataType [dbo].[MGCurrentDateTimeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MGCurrentDateTimeType] FROM [datetime] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[MGGUIDRowPointerType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MGGUIDRowPointerType] FROM [uniqueidentifier] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[MGNoteExistsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MGNoteExistsType] FROM [tinyint] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[MGUserNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MGUserNameType] FROM [nvarchar](30) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[MileageRateType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MileageRateType] FROM [decimal](5, 4) NULL
/****** Object:  UserDefinedDataType [dbo].[MilitaryType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MilitaryType] FROM [nvarchar](4) NULL
/****** Object:  UserDefinedDataType [dbo].[Min1099Type]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[Min1099Type] FROM [decimal](9, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[MinorityCertificationNumberType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MinorityCertificationNumberType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[MinorityTypeDescriptionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MinorityTypeDescriptionType] FROM [nvarchar](60) NULL
/****** Object:  UserDefinedDataType [dbo].[MinorityTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MinorityTypeType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[MinutesGranularityType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MinutesGranularityType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[MinutesOffType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MinutesOffType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[MiscReportTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MiscReportTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[MiscTitleType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MiscTitleType] FROM [nvarchar](80) NULL
/****** Object:  UserDefinedDataType [dbo].[MMaintKeyType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MMaintKeyType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[MO_BOMAlternateType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MO_BOMAlternateType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[MO_CycleTimeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MO_CycleTimeType] FROM [decimal](8, 4) NULL
/****** Object:  UserDefinedDataType [dbo].[MO_CycleType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MO_CycleType] FROM [decimal](10, 0) NULL
/****** Object:  UserDefinedDataType [dbo].[MO_JobConfigTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MO_JobConfigTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[MO_LaborType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MO_LaborType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[MO_MaintenanceIDPrefixType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MO_MaintenanceIDPrefixType] FROM [nvarchar](6) NULL
/****** Object:  UserDefinedDataType [dbo].[MO_MaintenanceIDType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MO_MaintenanceIDType] FROM [nvarchar](25) NULL
/****** Object:  UserDefinedDataType [dbo].[MO_MaintenanceStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MO_MaintenanceStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[MO_MaintenanceType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MO_MaintenanceType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[MO_OperationWeightPercentType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MO_OperationWeightPercentType] FROM [decimal](6, 3) NULL
/****** Object:  UserDefinedDataType [dbo].[MO_ProductCycleType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MO_ProductCycleType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[MO_ScheduleFrequencyType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MO_ScheduleFrequencyType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[MO_ScheduleFrequencyTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MO_ScheduleFrequencyTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[MonitoredResourceStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MonitoredResourceStatusType] FROM [nvarchar](max) NULL
/****** Object:  UserDefinedDataType [dbo].[MonthlyCarAllowanceType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MonthlyCarAllowanceType] FROM [decimal](5, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[MonthType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MonthType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[MrpExceptCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MrpExceptCodeType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[MrpOrderLineType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MrpOrderLineType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[MrpOrderReleaseType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MrpOrderReleaseType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[MrpOrderType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MrpOrderType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[MrpOrderTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MrpOrderTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[MrpPriorityType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MrpPriorityType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[MrpReqSrcType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MrpReqSrcType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[MrpTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MrpTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[MrpWbTabType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MrpWbTabType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[MsgAnswerType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MsgAnswerType] FROM [nvarchar](12) NULL
/****** Object:  UserDefinedDataType [dbo].[MX_DIOTRateType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MX_DIOTRateType] FROM [nvarchar](5) NULL
/****** Object:  UserDefinedDataType [dbo].[MX_DIOTTransTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MX_DIOTTransTypeType] FROM [nvarchar](2) NULL
/****** Object:  UserDefinedDataType [dbo].[MX_DistSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MX_DistSeqType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[MX_IETUClassType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MX_IETUClassType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[MX_NationalityType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MX_NationalityType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[MX_TaxRegForeignType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MX_TaxRegForeignType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[MX_TaxRegNumTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MX_TaxRegNumTypeType] FROM [nvarchar](2) NULL
/****** Object:  UserDefinedDataType [dbo].[MX_VendorDIOTType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[MX_VendorDIOTType] FROM [nvarchar](2) NULL
/****** Object:  UserDefinedDataType [dbo].[NAFTACountryOfOriginType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[NAFTACountryOfOriginType] FROM [nchar](3) NULL
/****** Object:  UserDefinedDataType [dbo].[NAFTAPreferenceCriterionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[NAFTAPreferenceCriterionType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[NameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[NameType] FROM [nvarchar](60) NULL
/****** Object:  UserDefinedDataType [dbo].[NationalityType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[NationalityType] FROM [nvarchar](12) NULL
/****** Object:  UserDefinedDataType [dbo].[NetworkDomainNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[NetworkDomainNameType] FROM [nvarchar](256) NULL
/****** Object:  UserDefinedDataType [dbo].[NetworkEncryptedPasswordType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[NetworkEncryptedPasswordType] FROM [nvarchar](max) NULL
/****** Object:  UserDefinedDataType [dbo].[NetworkUsernameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[NetworkUsernameType] FROM [nvarchar](64) NULL
/****** Object:  UserDefinedDataType [dbo].[NextKeyLengthExprType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[NextKeyLengthExprType] FROM [nvarchar](65) NULL
/****** Object:  UserDefinedDataType [dbo].[NextKeyPrefixListType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[NextKeyPrefixListType] FROM [nvarchar](131) NULL
/****** Object:  UserDefinedDataType [dbo].[NoteType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[NoteType] FROM [nvarchar](max) NULL
/****** Object:  UserDefinedDataType [dbo].[NumberOfEmployeesType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[NumberOfEmployeesType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[NumberOfLinesType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[NumberOfLinesType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[NumberOfSalespersonsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[NumberOfSalespersonsType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[NumericFormatType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[NumericFormatType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[NumFlagType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[NumFlagType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[ObjectNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ObjectNameType] FROM [nvarchar](128) NULL
/****** Object:  UserDefinedDataType [dbo].[OfferOfCoverageCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[OfferOfCoverageCodeType] FROM [nvarchar](2) NULL
/****** Object:  UserDefinedDataType [dbo].[OldNewType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[OldNewType] FROM [nvarchar](3) NULL
/****** Object:  UserDefinedDataType [dbo].[OldTextKeyType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[OldTextKeyType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[OleObjectType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[OleObjectType] FROM [ntext] NULL
/****** Object:  UserDefinedDataType [dbo].[OperationCounterType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[OperationCounterType] FROM [decimal](24, 0) NULL
/****** Object:  UserDefinedDataType [dbo].[OperNumPoReleaseType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[OperNumPoReleaseType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[OperNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[OperNumType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[OPMDueDateMethodType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[OPMDueDateMethodType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[OPMPartialMethodType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[OPMPartialMethodType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[OppItemCommentsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[OppItemCommentsType] FROM [nvarchar](255) NULL
/****** Object:  UserDefinedDataType [dbo].[OpportunityClosePercentType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[OpportunityClosePercentType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[OpportunityIDType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[OpportunityIDType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[OpportunityPriorityType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[OpportunityPriorityType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[OpportunityRefNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[OpportunityRefNumType] FROM [nvarchar](15) NULL
/****** Object:  UserDefinedDataType [dbo].[OpportunitySourceType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[OpportunitySourceType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[OpportunityStage2Type]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[OpportunityStage2Type] FROM [nvarchar](15) NULL
/****** Object:  UserDefinedDataType [dbo].[OpportunityStageType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[OpportunityStageType] FROM [nvarchar](15) NULL
/****** Object:  UserDefinedDataType [dbo].[OpportunityStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[OpportunityStatusType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[OpportunityTaskNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[OpportunityTaskNumType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[OpportunityTaskStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[OpportunityTaskStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[OpportunityTaskTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[OpportunityTaskTypeType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[OptCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[OptCodeType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[OrderConfirmationRefType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[OrderConfirmationRefType] FROM [nvarchar](80) NULL
/****** Object:  UserDefinedDataType [dbo].[OrderDiscType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[OrderDiscType] FROM [decimal](11, 8) NULL
/****** Object:  UserDefinedDataType [dbo].[OrderLineReservationRefType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[OrderLineReservationRefType] FROM [nvarchar](80) NULL
/****** Object:  UserDefinedDataType [dbo].[OrderNoType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[OrderNoType] FROM [nvarchar](12) NULL
/****** Object:  UserDefinedDataType [dbo].[OrderPricePercentType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[OrderPricePercentType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[OrderSourceType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[OrderSourceType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[OSBaseNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[OSBaseNameType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[OSDomainLabelNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[OSDomainLabelNameType] FROM [nvarchar](63) NULL
/****** Object:  UserDefinedDataType [dbo].[OSExtensionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[OSExtensionType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[OSLocationType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[OSLocationType] FROM [nvarchar](255) NULL
/****** Object:  UserDefinedDataType [dbo].[OSMachineNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[OSMachineNameType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[OSResourceNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[OSResourceNameType] FROM [nvarchar](256) NULL
/****** Object:  UserDefinedDataType [dbo].[OSResourceTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[OSResourceTypeType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[OtherNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[OtherNameType] FROM [nvarchar](15) NULL
/****** Object:  UserDefinedDataType [dbo].[OutputMethodType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[OutputMethodType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[OverheadBasisType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[OverheadBasisType] FROM [nvarchar](3) NULL
/****** Object:  UserDefinedDataType [dbo].[OverheadRateType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[OverheadRateType] FROM [decimal](9, 3) NULL
/****** Object:  UserDefinedDataType [dbo].[OvertimeLimitType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[OvertimeLimitType] FROM [decimal](4, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[PackageIDType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PackageIDType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[PackagesType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PackagesType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[PackNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PackNumType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[ParmKeyType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ParmKeyType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[PartialDeprTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PartialDeprTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PartNoType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PartNoType] FROM [nvarchar](25) NULL
/****** Object:  UserDefinedDataType [dbo].[PasswordExpirationType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PasswordExpirationType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[PasswordHistoryCountType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PasswordHistoryCountType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[PasswordLengthType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PasswordLengthType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[PasswordRetriesType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PasswordRetriesType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[PasswordStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PasswordStatusType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[PasswordType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PasswordType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[PatchLevelType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PatchLevelType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[PayBasisType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PayBasisType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PaymentMethodType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PaymentMethodType] FROM [nvarchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PayRatePreciseType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PayRatePreciseType] FROM [decimal](11, 5) NULL
/****** Object:  UserDefinedDataType [dbo].[PayRateType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PayRateType] FROM [decimal](9, 3) NULL
/****** Object:  UserDefinedDataType [dbo].[PayrollConfigIDType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PayrollConfigIDType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[PayrollConfigNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PayrollConfigNameType] FROM [nvarchar](100) NULL
/****** Object:  UserDefinedDataType [dbo].[PcrReasonCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PcrReasonCodeType] FROM [nvarchar](3) NULL
/****** Object:  UserDefinedDataType [dbo].[PeriodsAveragedType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PeriodsAveragedType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[PersonalCarUsePctType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PersonalCarUsePctType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[PerSortTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PerSortTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PertotSortFieldType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PertotSortFieldType] FROM [nvarchar](12) NULL
/****** Object:  UserDefinedDataType [dbo].[PhoneExtType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PhoneExtType] FROM [nvarchar](13) NULL
/****** Object:  UserDefinedDataType [dbo].[PhoneType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PhoneType] FROM [nvarchar](25) NULL
/****** Object:  UserDefinedDataType [dbo].[PhyinvCntStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PhyinvCntStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PhyinvPostStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PhyinvPostStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PhyinvStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PhyinvStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PhyinvTransTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PhyinvTransTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PhyinvTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PhyinvTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PhyInvValCounterType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PhyInvValCounterType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PhytagsCountMethType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PhytagsCountMethType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PhytagsMatlTypesType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PhytagsMatlTypesType] FROM [nvarchar](4) NULL
/****** Object:  UserDefinedDataType [dbo].[PickGroupType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PickGroupType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[PickListIDType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PickListIDType] FROM [decimal](19, 0) NULL
/****** Object:  UserDefinedDataType [dbo].[PickListSequenceType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PickListSequenceType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[PickListStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PickListStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PieceCountType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PieceCountType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[PlanFenceType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PlanFenceType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[PlannerTraceLevelType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PlannerTraceLevelType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PlanNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PlanNumType] FROM [nvarchar](6) NULL
/****** Object:  UserDefinedDataType [dbo].[PlnSupplyProductCodeSourceTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PlnSupplyProductCodeSourceTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PlnSupplySourcePercentType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PlnSupplySourcePercentType] FROM [decimal](5, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[PlnSupplySourceRuleIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PlnSupplySourceRuleIdType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[PMTCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PMTCodeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PoChangeStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PoChangeStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PoChgSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PoChgSeqType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[PoitemStatType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PoitemStatType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PoitemStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PoitemStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PoLineItemTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PoLineItemTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PoLineType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PoLineType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[PollFreqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PollFreqType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[PoNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PoNumType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[PoPrefixType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PoPrefixType] FROM [nvarchar](7) NULL
/****** Object:  UserDefinedDataType [dbo].[PoPsTrnNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PoPsTrnNumType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[PopulationDepthType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PopulationDepthType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[PoReleaseType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PoReleaseType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[PortalAccountStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PortalAccountStatusType] FROM [nvarchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PortOfEntryType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PortOfEntryType] FROM [nvarchar](4) NULL
/****** Object:  UserDefinedDataType [dbo].[PosClassType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PosClassType] FROM [nvarchar](2) NULL
/****** Object:  UserDefinedDataType [dbo].[PosDetJobStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PosDetJobStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PosHoursType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PosHoursType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[PositivePayFieldDateFmtType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PositivePayFieldDateFmtType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[PositivePayFieldFillerType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PositivePayFieldFillerType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PositivePayFieldJustificationType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PositivePayFieldJustificationType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PositivePayFieldLengthType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PositivePayFieldLengthType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[PositivePayFieldTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PositivePayFieldTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PositivePayFieldValueType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PositivePayFieldValueType] FROM [nvarchar](50) NULL
/****** Object:  UserDefinedDataType [dbo].[PositivePayFieldVoidIndicatorType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PositivePayFieldVoidIndicatorType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[PositivePaySectionSequenceType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PositivePaySectionSequenceType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[POSMDrawerStatType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[POSMDrawerStatType] FROM [nvarchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[POSMDrawerType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[POSMDrawerType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[POSMDriversLicenseType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[POSMDriversLicenseType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[POSMNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[POSMNumType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[POSMOrdTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[POSMOrdTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[POSMPayRefType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[POSMPayRefType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[POSMPayTransTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[POSMPayTransTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[POSMPayTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[POSMPayTypeType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[POSMRefNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[POSMRefNumType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[POSMRefTypeCOSType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[POSMRefTypeCOSType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[POSMTransNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[POSMTransNumType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[PosReqTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PosReqTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PosRequirementType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PosRequirementType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[PostalCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PostalCodeType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[PoStatType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PoStatType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PoStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PoStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PoTrnNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PoTrnNumType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[PoTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PoTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PoVchTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PoVchTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PP_AreaConstantType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PP_AreaConstantType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[PP_CartonDimensionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PP_CartonDimensionType] FROM [decimal](20, 8) NULL
/****** Object:  UserDefinedDataType [dbo].[PP_FactorType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PP_FactorType] FROM [decimal](9, 5) NULL
/****** Object:  UserDefinedDataType [dbo].[PP_FormulaDescriptionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PP_FormulaDescriptionType] FROM [nvarchar](max) NULL
/****** Object:  UserDefinedDataType [dbo].[PP_FormulaType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PP_FormulaType] FROM [nvarchar](max) NULL
/****** Object:  UserDefinedDataType [dbo].[PP_FormulaTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PP_FormulaTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PP_GradeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PP_GradeType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[PP_MaterialClassType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PP_MaterialClassType] FROM [nvarchar](6) NULL
/****** Object:  UserDefinedDataType [dbo].[PP_MatlPaperSourceType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PP_MatlPaperSourceType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PP_NumberOfColorsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PP_NumberOfColorsType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[PP_NumberOfDimensionsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PP_NumberOfDimensionsType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[PP_NumberOfHolesType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PP_NumberOfHolesType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[PP_NumberOfJointsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PP_NumberOfJointsType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[PP_NumberOfManualHandlingStepsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PP_NumberOfManualHandlingStepsType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[PP_NumberOfSidesToPrintType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PP_NumberOfSidesToPrintType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[PP_NumberOfWordsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PP_NumberOfWordsType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[PP_OperationDimensionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PP_OperationDimensionType] FROM [decimal](20, 8) NULL
/****** Object:  UserDefinedDataType [dbo].[PP_OperationType2Type]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PP_OperationType2Type] FROM [nvarchar](3) NULL
/****** Object:  UserDefinedDataType [dbo].[PP_OperationTypeCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PP_OperationTypeCodeType] FROM [nvarchar](6) NULL
/****** Object:  UserDefinedDataType [dbo].[PP_OperationTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PP_OperationTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PP_OutNumberType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PP_OutNumberType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[PP_PaperCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PP_PaperCodeType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[PP_PaperConsumptionMinConstant]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PP_PaperConsumptionMinConstant] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[PP_PaperMassBasisType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PP_PaperMassBasisType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PP_PriceConstantType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PP_PriceConstantType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[PP_PrintModeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PP_PrintModeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PP_PrintSheetConstantType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PP_PrintSheetConstantType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[PP_QuoteLineType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PP_QuoteLineType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[PP_QuoteSectionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PP_QuoteSectionType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[PP_QuoteTemplateSectionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PP_QuoteTemplateSectionType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[PP_QuoteTemplateType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PP_QuoteTemplateType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[PP_SectionsCountType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PP_SectionsCountType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[PP_SheetCountType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PP_SheetCountType] FROM [decimal](19, 8) NULL
/****** Object:  UserDefinedDataType [dbo].[PP_StdConsumptionRateType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PP_StdConsumptionRateType] FROM [decimal](9, 5) NULL
/****** Object:  UserDefinedDataType [dbo].[PP_UpNumberType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PP_UpNumberType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[PrAmountType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PrAmountType] FROM [decimal](8, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[PrCheckNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PrCheckNumType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[PrCommCommSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PrCommCommSeqType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[PrCommSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PrCommSeqType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[PrDeductionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PrDeductionType] FROM [decimal](8, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[PrenoteType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PrenoteType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[PreqitemStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PreqitemStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PreqPrefixType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PreqPrefixType] FROM [nvarchar](7) NULL
/****** Object:  UserDefinedDataType [dbo].[PreqStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PreqStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PreRegisterType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PreRegisterType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[PrGenerateFromType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PrGenerateFromType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PrhrsHrTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PrhrsHrTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PriceBaseCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PriceBaseCodeType] FROM [nvarchar](2) NULL
/****** Object:  UserDefinedDataType [dbo].[PriceCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PriceCodeType] FROM [nvarchar](3) NULL
/****** Object:  UserDefinedDataType [dbo].[PriceFormulaType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PriceFormulaType] FROM [nvarchar](3) NULL
/****** Object:  UserDefinedDataType [dbo].[PriceMethodType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PriceMethodType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PricingDisplayRuleType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PricingDisplayRuleType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PricingPrecalculationRuleType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PricingPrecalculationRuleType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PrinterControlType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PrinterControlType] FROM [nvarchar](50) NULL
/****** Object:  UserDefinedDataType [dbo].[PrinterNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PrinterNameType] FROM [nvarchar](255) NULL
/****** Object:  UserDefinedDataType [dbo].[PrintModeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PrintModeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PriorityType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PriorityType] FROM [decimal](9, 3) NULL
/****** Object:  UserDefinedDataType [dbo].[PrivilegeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PrivilegeType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[PrlogHrTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PrlogHrTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PrlogPostedFromType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PrlogPostedFromType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PrLogSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PrLogSeqType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[ProbableType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProbableType] FROM [decimal](4, 3) NULL
/****** Object:  UserDefinedDataType [dbo].[ProbCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProbCodeType] FROM [nvarchar](3) NULL
/****** Object:  UserDefinedDataType [dbo].[ProcessFlowType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProcessFlowType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[ProcessIndType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProcessIndType] FROM [nvarchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ProcessManagerProcessIDType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProcessManagerProcessIDType] FROM [decimal](11, 0) NULL
/****** Object:  UserDefinedDataType [dbo].[ProcessManagerProcessStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProcessManagerProcessStatusType] FROM [char](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ProcessManagerProcessTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProcessManagerProcessTypeType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[ProcessManagerTaskDescriptionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProcessManagerTaskDescriptionType] FROM [nvarchar](4000) NULL
/****** Object:  UserDefinedDataType [dbo].[ProcessManagerTaskNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProcessManagerTaskNameType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[ProcessManagerTaskNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProcessManagerTaskNumType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[ProcessManagerTaskStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProcessManagerTaskStatusType] FROM [char](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ProcessManagerTaskTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProcessManagerTaskTypeType] FROM [char](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ProcessReqLineStatType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProcessReqLineStatType] FROM [nvarchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ProcessReqStatType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProcessReqStatType] FROM [nvarchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ProdMixPercentType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProdMixPercentType] FROM [decimal](6, 3) NULL
/****** Object:  UserDefinedDataType [dbo].[ProdMixPrefixType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProdMixPrefixType] FROM [nvarchar](7) NULL
/****** Object:  UserDefinedDataType [dbo].[ProdMixQuantityType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProdMixQuantityType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[ProdMixType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProdMixType] FROM [nvarchar](7) NULL
/****** Object:  UserDefinedDataType [dbo].[ProdTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProdTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ProductCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProductCodeType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[ProductOverviewType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProductOverviewType] FROM [nvarchar](max) NULL
/****** Object:  UserDefinedDataType [dbo].[ProductVersionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProductVersionType] FROM [nvarchar](160) NULL
/****** Object:  UserDefinedDataType [dbo].[ProfessionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProfessionType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[ProfileNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProfileNameType] FROM [nvarchar](50) NULL
/****** Object:  UserDefinedDataType [dbo].[ProgBillInvoiceFlagType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProgBillInvoiceFlagType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ProgBillSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProgBillSeqType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[ProjBillMethodType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProjBillMethodType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ProjBolStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProjBolStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ProjCalcAmountType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProjCalcAmountType] FROM [decimal](10, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[ProjCalcPercentType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProjCalcPercentType] FROM [decimal](4, 1) NULL
/****** Object:  UserDefinedDataType [dbo].[ProjchgSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProjchgSeqType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[ProjCostCalcMethodType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProjCostCalcMethodType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ProjcostDetailPeriodType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProjcostDetailPeriodType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[ProjCostTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProjCostTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ProjDistSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProjDistSeqType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[ProjDurationType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProjDurationType] FROM [decimal](7, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[ProjectChangeStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProjectChangeStatusType] FROM [nvarchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ProjectNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProjectNameType] FROM [nvarchar](80) NULL
/****** Object:  UserDefinedDataType [dbo].[ProjectResourceRefNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProjectResourceRefNameType] FROM [nvarchar](60) NULL
/****** Object:  UserDefinedDataType [dbo].[ProjectResourceTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProjectResourceTypeType] FROM [nvarchar](15) NULL
/****** Object:  UserDefinedDataType [dbo].[ProjFeeRateType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProjFeeRateType] FROM [decimal](7, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[ProjInvFmtType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProjInvFmtType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[ProjInvHdrTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProjInvHdrTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ProjInvMethodType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProjInvMethodType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ProjmatlSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProjmatlSeqType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[ProjMilestoneGroupType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProjMilestoneGroupType] FROM [nchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[ProjMilestoneTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProjMilestoneTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ProjMSNotificationTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProjMSNotificationTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ProjMsNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProjMsNumType] FROM [nvarchar](7) NULL
/****** Object:  UserDefinedDataType [dbo].[ProjMsSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProjMsSeqType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[ProjNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProjNumType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[ProjPercentCompleteType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProjPercentCompleteType] FROM [decimal](5, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[ProjPredecessorsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProjPredecessorsType] FROM [nvarchar](50) NULL
/****** Object:  UserDefinedDataType [dbo].[ProjPricingType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProjPricingType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ProjProjTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProjProjTypeType] FROM [nvarchar](3) NULL
/****** Object:  UserDefinedDataType [dbo].[ProjRateType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProjRateType] FROM [decimal](7, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[ProjRetPercentType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProjRetPercentType] FROM [decimal](6, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[ProjRevCalcMethodType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProjRevCalcMethodType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ProjRevSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProjRevSeqType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[ProjShipSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProjShipSeqType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[ProjStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProjStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ProjtaskDurUnitType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProjtaskDurUnitType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ProjTransNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProjTransNumType] FROM [decimal](11, 0) NULL
/****** Object:  UserDefinedDataType [dbo].[ProjTransSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProjTransSeqType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[ProjtranTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProjtranTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ProjTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProjTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ProjUnitsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProjUnitsType] FROM [decimal](9, 3) NULL
/****** Object:  UserDefinedDataType [dbo].[ProjVariancePriorityType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProjVariancePriorityType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[ProjWbsNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProjWbsNumType] FROM [nvarchar](15) NULL
/****** Object:  UserDefinedDataType [dbo].[ProjWipRelMethodType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProjWipRelMethodType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PromotionCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PromotionCodeType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[PromotionDiscountType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PromotionDiscountType] FROM [decimal](9, 3) NULL
/****** Object:  UserDefinedDataType [dbo].[PromotionTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PromotionTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PropertyClassType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PropertyClassType] FROM [nvarchar](80) NULL
/****** Object:  UserDefinedDataType [dbo].[ProspectIDType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProspectIDType] FROM [nvarchar](7) NULL
/****** Object:  UserDefinedDataType [dbo].[ProspectLogRefNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProspectLogRefNumType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[ProspectLogRefTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProspectLogRefTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ProspectType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProspectType] FROM [nvarchar](7) NULL
/****** Object:  UserDefinedDataType [dbo].[ProxCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProxCodeType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[ProxDayType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProxDayType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[ProxDiscDayType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProxDiscDayType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[ProxDiscMonthToForwardType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProxDiscMonthToForwardType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[ProxMonthToForwardType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ProxMonthToForwardType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[PrPayFreqsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PrPayFreqsType] FROM [nvarchar](6) NULL
/****** Object:  UserDefinedDataType [dbo].[PrPayFreqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PrPayFreqType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PrPercentType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PrPercentType] FROM [decimal](9, 6) NULL
/****** Object:  UserDefinedDataType [dbo].[PrTaxCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PrTaxCodeType] FROM [nvarchar](3) NULL
/****** Object:  UserDefinedDataType [dbo].[PrTaxRptIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PrTaxRptIdType] FROM [nvarchar](15) NULL
/****** Object:  UserDefinedDataType [dbo].[PrtaxtCalcBasisType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PrtaxtCalcBasisType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PrtaxtTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PrtaxtTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PrtrxdTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PrtrxdTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PrtrxSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PrtrxSeqType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[PrtrxTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PrtrxTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PrWithholdingTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PrWithholdingTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PrYearUnitsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PrYearUnitsType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[PrYtdAmountType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PrYtdAmountType] FROM [decimal](10, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[PsNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PsNumType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[PsPrefixType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PsPrefixType] FROM [nvarchar](7) NULL
/****** Object:  UserDefinedDataType [dbo].[PsRelPrefixType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PsRelPrefixType] FROM [nvarchar](16) NULL
/****** Object:  UserDefinedDataType [dbo].[PsStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PsStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[PublicationCategoryType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PublicationCategoryType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[PurchasesType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PurchasesType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[PurchOvhdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[PurchOvhdType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[QCAcceptRejectHoldStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QCAcceptRejectHoldStatusType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[QCActionCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QCActionCodeType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[QCCauseCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QCCauseCodeType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[QCCertificateOfConformanceNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QCCertificateOfConformanceNumType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[QCChangeMaterialRequestNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QCChangeMaterialRequestNumType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[QCCharType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QCCharType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[QCCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QCCodeType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[QCCorrectiveActionReportNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QCCorrectiveActionReportNumType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[QCCustomerComplaintReviewNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QCCustomerComplaintReviewNumType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[QCCustStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QCCustStatusType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[QCDaysType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QCDaysType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[QCDescriptionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QCDescriptionType] FROM [nvarchar](60) NULL
/****** Object:  UserDefinedDataType [dbo].[QCDispositionCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QCDispositionCodeType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[QCDocNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QCDocNumType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[QCDocType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QCDocType] FROM [nvarchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[QCEntityType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QCEntityType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[QCFailCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QCFailCodeType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[QCGageHistStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QCGageHistStatusType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[QCGageStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QCGageStatusType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[QCIntegerType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QCIntegerType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[QCLongCharType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QCLongCharType] FROM [nvarchar](1000) NULL
/****** Object:  UserDefinedDataType [dbo].[QCMaterialReviewReportNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QCMaterialReviewReportNumType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[QCMedCharType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QCMedCharType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[QCOpenClosedStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QCOpenClosedStatusType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[QCPointsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QCPointsType] FROM [decimal](8, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[QCPriorityType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QCPriorityType] FROM [nvarchar](12) NULL
/****** Object:  UserDefinedDataType [dbo].[QCProcessRefType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QCProcessRefType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[QCProcessRefTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QCProcessRefTypeType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[QCProcessTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QCProcessTypeType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[QCRcvrNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QCRcvrNumType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[QCReasonCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QCReasonCodeType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[QCRefLineType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QCRefLineType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[QCRefNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QCRefNumType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[QCRefReleaseType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QCRefReleaseType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[QCRefType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QCRefType] FROM [nvarchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[QCSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QCSeqType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[QCSmallCharType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QCSmallCharType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[QCStatusListingType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QCStatusListingType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[QCTestFamilyType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QCTestFamilyType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[QCTestSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QCTestSeqType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[QCTestTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QCTestTypeType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[QCTopicReviewReportNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QCTopicReviewReportNumType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[QCTopicStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QCTopicStatusType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[QCTransNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QCTransNumType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[QCTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QCTypeType] FROM [nvarchar](3) NULL
/****** Object:  UserDefinedDataType [dbo].[QCVendorRMANumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QCVendorRMANumType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[QCVendorStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QCVendorStatusType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[QCVRMAStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QCVRMAStatusType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[QtyCumuType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QtyCumuType] FROM [decimal](20, 8) NULL
/****** Object:  UserDefinedDataType [dbo].[QtyPerNoNegType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QtyPerNoNegType] FROM [decimal](18, 8) NULL
/****** Object:  UserDefinedDataType [dbo].[QtyPerType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QtyPerType] FROM [decimal](18, 8) NULL
/****** Object:  UserDefinedDataType [dbo].[QtyTotlNoNegType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QtyTotlNoNegType] FROM [decimal](20, 8) NULL
/****** Object:  UserDefinedDataType [dbo].[QtyTotlType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QtyTotlType] FROM [decimal](20, 8) NULL
/****** Object:  UserDefinedDataType [dbo].[QtyUnitNoNegType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QtyUnitNoNegType] FROM [decimal](19, 8) NULL
/****** Object:  UserDefinedDataType [dbo].[QtyUnitType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QtyUnitType] FROM [decimal](19, 8) NULL
/****** Object:  UserDefinedDataType [dbo].[QualCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QualCodeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[QuarterType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QuarterType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[QueryDisplaySeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QueryDisplaySeqType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[QueryExpressionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QueryExpressionType] FROM [nvarchar](720) NULL
/****** Object:  UserDefinedDataType [dbo].[QueryExprTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QueryExprTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[QueryFieldListType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QueryFieldListType] FROM [nvarchar](60) NULL
/****** Object:  UserDefinedDataType [dbo].[QueryJoinSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QueryJoinSeqType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[QueryNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QueryNameType] FROM [nvarchar](32) NULL
/****** Object:  UserDefinedDataType [dbo].[QueryOperatorType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QueryOperatorType] FROM [nvarchar](7) NULL
/****** Object:  UserDefinedDataType [dbo].[QueryRecordTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QueryRecordTypeType] FROM [nvarchar](5) NULL
/****** Object:  UserDefinedDataType [dbo].[QueryRetrieveTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QueryRetrieveTypeType] FROM [nvarchar](5) NULL
/****** Object:  UserDefinedDataType [dbo].[QuerySeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QuerySeqType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[QueueNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QueueNameType] FROM [nvarchar](256) NULL
/****** Object:  UserDefinedDataType [dbo].[QueueTransportType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[QueueTransportType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[RateCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RateCodeType] FROM [nvarchar](3) NULL
/****** Object:  UserDefinedDataType [dbo].[RateFactorType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RateFactorType] FROM [decimal](4, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[ReaderIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ReaderIdType] FROM [nvarchar](2) NULL
/****** Object:  UserDefinedDataType [dbo].[ReasonClassType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ReasonClassType] FROM [nvarchar](15) NULL
/****** Object:  UserDefinedDataType [dbo].[ReasonCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ReasonCodeType] FROM [nvarchar](3) NULL
/****** Object:  UserDefinedDataType [dbo].[RebateFairValueType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RebateFairValueType] FROM [decimal](9, 3) NULL
/****** Object:  UserDefinedDataType [dbo].[RebateNumberOfPeriodsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RebateNumberOfPeriodsType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[RebateRedemptionRateType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RebateRedemptionRateType] FROM [decimal](8, 5) NULL
/****** Object:  UserDefinedDataType [dbo].[RecordActionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RecordActionType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[RefDayOffsetDurationType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RefDayOffsetDurationType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[RefDayOffsetIntervalType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RefDayOffsetIntervalType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[RefDayOffsetMethodType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RefDayOffsetMethodType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[RefDayType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RefDayType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[RefDesignatorType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RefDesignatorType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[ReferenceObjectCaptionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ReferenceObjectCaptionType] FROM [nvarchar](128) NULL
/****** Object:  UserDefinedDataType [dbo].[ReferenceObjectType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ReferenceObjectType] FROM [nvarchar](128) NULL
/****** Object:  UserDefinedDataType [dbo].[ReferenceType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ReferenceType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[RefNumIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RefNumIdType] FROM [nvarchar](7) NULL
/****** Object:  UserDefinedDataType [dbo].[RefSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RefSeqType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[RefStrType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RefStrType] FROM [nvarchar](50) NULL
/****** Object:  UserDefinedDataType [dbo].[RefType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RefType] FROM [nvarchar](14) NULL
/****** Object:  UserDefinedDataType [dbo].[RefTypeCEIOPRVType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RefTypeCEIOPRVType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[RefTypeCGPVType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RefTypeCGPVType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[RefTypeIJKMNOTType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RefTypeIJKMNOTType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[RefTypeIJKO]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RefTypeIJKO] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[RefTypeIJKOPRSTWType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RefTypeIJKOPRSTWType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[RefTypeIJKOTType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RefTypeIJKOTType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[RefTypeIJKPRTType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RefTypeIJKPRTType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[RefTypeIJOPRSTType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RefTypeIJOPRSTType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[RefTypeIJOType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RefTypeIJOType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[RefTypeIJPRTType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RefTypeIJPRTType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[RefTypeIJPRType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RefTypeIJPRType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[RefTypeIKOTType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RefTypeIKOTType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[RefTypeJKOType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RefTypeJKOType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[RefTypeKOTType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RefTypeKOTType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[RefTypeMNTType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RefTypeMNTType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[RefTypeOTType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RefTypeOTType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[RefTypeOType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RefTypeOType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[RefTypePTType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RefTypePTType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ReimbursementMaxMinType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ReimbursementMaxMinType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[RelationshipType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RelationshipType] FROM [nvarchar](15) NULL
/****** Object:  UserDefinedDataType [dbo].[RepCategoryType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RepCategoryType] FROM [nvarchar](50) NULL
/****** Object:  UserDefinedDataType [dbo].[RepIntervalCountType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RepIntervalCountType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[RepIntervalTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RepIntervalTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ReplicationOperationType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ReplicationOperationType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[RepObjectType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RepObjectType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[ReportIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ReportIdType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[ReportLineType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ReportLineType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[ReportOrientationType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ReportOrientationType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ReportServerDeploymentModeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ReportServerDeploymentModeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ReportTemplateIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ReportTemplateIdType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[ReportTitleType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ReportTitleType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[ReportTxtType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ReportTxtType] FROM [nvarchar](70) NULL
/****** Object:  UserDefinedDataType [dbo].[ReqCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ReqCodeType] FROM [nvarchar](3) NULL
/****** Object:  UserDefinedDataType [dbo].[ReqLineType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ReqLineType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[ReqNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ReqNumType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[ResNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ResNumType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[ResourceSequenceType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ResourceSequenceType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[ResourcesType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ResourcesType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[ResourceType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ResourceType] FROM [nvarchar](6) NULL
/****** Object:  UserDefinedDataType [dbo].[RestockFeePercentType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RestockFeePercentType] FROM [decimal](5, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[RetCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RetCodeType] FROM [nvarchar](7) NULL
/****** Object:  UserDefinedDataType [dbo].[RetentionDaysType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RetentionDaysType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[RetireAmountType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RetireAmountType] FROM [decimal](8, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[RetireBasisType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RetireBasisType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[RetireEligibleType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RetireEligibleType] FROM [nvarchar](2) NULL
/****** Object:  UserDefinedDataType [dbo].[RetirePercentType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RetirePercentType] FROM [decimal](5, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[ReviewDaysType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ReviewDaysType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[ReviewMonthsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ReviewMonthsType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[ReviewTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ReviewTypeType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[RevisionIDType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RevisionIDType] FROM [nvarchar](22) NULL
/****** Object:  UserDefinedDataType [dbo].[RevisionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RevisionType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[RfBellDelayType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RfBellDelayType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[RfBrowserFieldSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RfBrowserFieldSeqType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[RfBrowserFieldType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RfBrowserFieldType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[RfBrowserNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RfBrowserNameType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[RfButtonLabelType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RfButtonLabelType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[RfClassSuffixType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RfClassSuffixType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[RfClassType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RfClassType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[RfCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RfCodeType] FROM [nvarchar](12) NULL
/****** Object:  UserDefinedDataType [dbo].[RfColumnType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RfColumnType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[RfDefaultType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RfDefaultType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[RfFieldFormatType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RfFieldFormatType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[RfFieldLabelType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RfFieldLabelType] FROM [nvarchar](25) NULL
/****** Object:  UserDefinedDataType [dbo].[RfFieldNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RfFieldNameType] FROM [nvarchar](25) NULL
/****** Object:  UserDefinedDataType [dbo].[RfFieldTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RfFieldTypeType] FROM [nvarchar](15) NULL
/****** Object:  UserDefinedDataType [dbo].[RfFieldWidthType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RfFieldWidthType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[RfHeightType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RfHeightType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[RfJumpFormatType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RfJumpFormatType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[RfKeyType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RfKeyType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[RfLabelFormatType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RfLabelFormatType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[RfLabelType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RfLabelType] FROM [nvarchar](15) NULL
/****** Object:  UserDefinedDataType [dbo].[RfMenuCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RfMenuCodeType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[RfMenuItemType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RfMenuItemType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[RfMenuKeyType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RfMenuKeyType] FROM [nvarchar](2) NULL
/****** Object:  UserDefinedDataType [dbo].[RfMenuSelLabelType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RfMenuSelLabelType] FROM [nvarchar](15) NULL
/****** Object:  UserDefinedDataType [dbo].[RfMenuTextType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RfMenuTextType] FROM [nvarchar](73) NULL
/****** Object:  UserDefinedDataType [dbo].[RfPagesType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RfPagesType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[RfPageType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RfPageType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[RFQDistMethodType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RFQDistMethodType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[RFQEmailTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RFQEmailTypeType] FROM [nchar](4) NULL
/****** Object:  UserDefinedDataType [dbo].[RFQFaxMethodType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RFQFaxMethodType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[RFQLineRespStatType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RFQLineRespStatType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[RFQLineType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RFQLineType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[RFQNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RFQNumType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[RFQProfileType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RFQProfileType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[RFQRefLineSufType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RFQRefLineSufType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[RFQRefNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RFQRefNumType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[RFQRefReleaseType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RFQRefReleaseType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[RFQRefTypeIJKRType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RFQRefTypeIJKRType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[RFQSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RFQSeqType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[RFQStatType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RFQStatType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[RfRelatedSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RfRelatedSeqType] FROM [nvarchar](80) NULL
/****** Object:  UserDefinedDataType [dbo].[RfRowType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RfRowType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[RfScreenNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RfScreenNameType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[RfSequenceType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RfSequenceType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[RfTmModeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RfTmModeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[RfWidthType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RfWidthType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[RmaItemStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RmaItemStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[RmaLineType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RmaLineType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[RmaNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RmaNumType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[RmaStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RmaStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[RMXDispCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RMXDispCodeType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[RMXPackLineType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RMXPackLineType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[RMXRefLineSufType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RMXRefLineSufType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[RMXRefNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RMXRefNumType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[RMXRefReleaseType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RMXRefReleaseType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[RMXRefTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RMXRefTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[RMXRtnTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RMXRtnTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[RMXSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RMXSeqType] FROM [decimal](18, 0) NULL
/****** Object:  UserDefinedDataType [dbo].[RMXTransNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RMXTransNumType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[RMXVndRtnTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RMXVndRtnTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[RouteType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RouteType] FROM [nvarchar](50) NULL
/****** Object:  UserDefinedDataType [dbo].[RowPointer]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RowPointer] FROM [uniqueidentifier] NULL
/****** Object:  UserDefinedDataType [dbo].[RowPointerType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RowPointerType] FROM [uniqueidentifier] NULL
/****** Object:  UserDefinedDataType [dbo].[RptDestTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RptDestTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[RptIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RptIdType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[RptLinesType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RptLinesType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[RptOptIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RptOptIdType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[RptOptValuesCompNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RptOptValuesCompNameType] FROM [nvarchar](50) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[RptOptValuesValueType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RptOptValuesValueType] FROM [nvarchar](120) NULL
/****** Object:  UserDefinedDataType [dbo].[RsvdNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RsvdNumType] FROM [decimal](10, 0) NULL
/****** Object:  UserDefinedDataType [dbo].[RunBasisLbrType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RunBasisLbrType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[RunBasisMchType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RunBasisMchType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[RunBasisType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RunBasisType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[RunHoursPiecesType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RunHoursPiecesType] FROM [decimal](15, 8) NULL
/****** Object:  UserDefinedDataType [dbo].[RunHoursType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RunHoursType] FROM [decimal](10, 5) NULL
/****** Object:  UserDefinedDataType [dbo].[RunRateType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RunRateType] FROM [decimal](10, 3) NULL
/****** Object:  UserDefinedDataType [dbo].[RunTicksType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[RunTicksType] FROM [decimal](19, 10) NULL
/****** Object:  UserDefinedDataType [dbo].[SafeHarborCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SafeHarborCodeType] FROM [nvarchar](2) NULL
/****** Object:  UserDefinedDataType [dbo].[SafetyStockMultiplierType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SafetyStockMultiplierType] FROM [decimal](10, 4) NULL
/****** Object:  UserDefinedDataType [dbo].[SafetyStockPercentType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SafetyStockPercentType] FROM [decimal](3, 1) NULL
/****** Object:  UserDefinedDataType [dbo].[SafetyStockRuleType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SafetyStockRuleType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[SalaryChangePercentType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SalaryChangePercentType] FROM [decimal](5, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[SalePricePercentType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SalePricePercentType] FROM [decimal](5, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[SalesForecastIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SalesForecastIdType] FROM [nvarchar](7) NULL
/****** Object:  UserDefinedDataType [dbo].[SalesForecastStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SalesForecastStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[SalesPeriodIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SalesPeriodIdType] FROM [nvarchar](7) NULL
/****** Object:  UserDefinedDataType [dbo].[SalesPeriodStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SalesPeriodStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[SalesTeamIDType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SalesTeamIDType] FROM [nvarchar](7) NULL
/****** Object:  UserDefinedDataType [dbo].[SaleSumBucketType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SaleSumBucketType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[SalPeriodType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SalPeriodType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ScalDurHoursType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ScalDurHoursType] FROM [decimal](4, 1) NULL
/****** Object:  UserDefinedDataType [dbo].[SchedBNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SchedBNumType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[SchedDriverType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SchedDriverType] FROM [nchar](1) NOT NULL
/****** Object:  UserDefinedDataType [dbo].[SchedHoursType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SchedHoursType] FROM [decimal](7, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[SchedMethodType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SchedMethodType] FROM [nvarchar](3) NULL
/****** Object:  UserDefinedDataType [dbo].[SchedSequenceType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SchedSequenceType] FROM [nvarchar](3) NULL
/****** Object:  UserDefinedDataType [dbo].[ScheduleDaysType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ScheduleDaysType] FROM [smallint] NOT NULL
/****** Object:  UserDefinedDataType [dbo].[SchToleranceType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SchToleranceType] FROM [decimal](5, 4) NULL
/****** Object:  UserDefinedDataType [dbo].[ScrapFactorType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ScrapFactorType] FROM [decimal](5, 4) NULL
/****** Object:  UserDefinedDataType [dbo].[SearchGroupType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SearchGroupType] FROM [nvarchar](80) NULL
/****** Object:  UserDefinedDataType [dbo].[SearchItemNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SearchItemNameType] FROM [nvarchar](80) NULL
/****** Object:  UserDefinedDataType [dbo].[SecurityFunctionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SecurityFunctionType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[SepaCreditorIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SepaCreditorIdType] FROM [nvarchar](35) NULL
/****** Object:  UserDefinedDataType [dbo].[SepaMandateRefType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SepaMandateRefType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[SequenceType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SequenceType] FROM [decimal](11, 0) NULL
/****** Object:  UserDefinedDataType [dbo].[SerialLengthType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SerialLengthType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[SerialPrefixType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SerialPrefixType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[SerialStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SerialStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[SerKeyType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SerKeyType] FROM [nvarchar](16) NULL
/****** Object:  UserDefinedDataType [dbo].[SerNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SerNumType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[ServiceDurationType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ServiceDurationType] FROM [decimal](7, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[ServiceDurationUnitType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ServiceDurationUnitType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[SessionTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SessionTypeType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[SetupGroupType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SetupGroupType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[SetupHoursType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SetupHoursType] FROM [decimal](6, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[SexType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SexType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[SfModeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SfModeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[SfOutputType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SfOutputType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[SfReaderSnType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SfReaderSnType] FROM [nvarchar](36) NULL
/****** Object:  UserDefinedDataType [dbo].[SheetTagIncrementType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SheetTagIncrementType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[SheetTagNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SheetTagNumType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[ShelfLifeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ShelfLifeType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[ShiftPremiumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ShiftPremiumType] FROM [decimal](5, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[ShiftPremiumTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ShiftPremiumTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ShiftTimeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ShiftTimeType] FROM [datetime] NULL
/****** Object:  UserDefinedDataType [dbo].[ShiftType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ShiftType] FROM [nvarchar](3) NULL
/****** Object:  UserDefinedDataType [dbo].[ShipCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ShipCodeType] FROM [nvarchar](4) NULL
/****** Object:  UserDefinedDataType [dbo].[ShipmentDocIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ShipmentDocIdType] FROM [nvarchar](35) NULL
/****** Object:  UserDefinedDataType [dbo].[ShipmentIDType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ShipmentIDType] FROM [decimal](19, 0) NULL
/****** Object:  UserDefinedDataType [dbo].[ShipmentLineIDType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ShipmentLineIDType] FROM [nvarchar](22) NULL
/****** Object:  UserDefinedDataType [dbo].[ShipmentLineType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ShipmentLineType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[ShipmentSequenceType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ShipmentSequenceType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[ShipmentStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ShipmentStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ShipMethodGroupType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ShipMethodGroupType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[ShipMethodType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ShipMethodType] FROM [nvarchar](4) NULL
/****** Object:  UserDefinedDataType [dbo].[ShipperNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ShipperNumType] FROM [nvarchar](15) NULL
/****** Object:  UserDefinedDataType [dbo].[ShipperType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ShipperType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[ShortDescType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ShortDescType] FROM [nvarchar](36) NULL
/****** Object:  UserDefinedDataType [dbo].[ShrinkFactorType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ShrinkFactorType] FROM [decimal](5, 4) NULL
/****** Object:  UserDefinedDataType [dbo].[SICCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SICCodeType] FROM [nvarchar](4) NULL
/****** Object:  UserDefinedDataType [dbo].[SingleFixBuildLevelType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SingleFixBuildLevelType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[SingleFixNumberType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SingleFixNumberType] FROM [nvarchar](80) NULL
/****** Object:  UserDefinedDataType [dbo].[SiteGroupType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SiteGroupType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[SiteListType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SiteListType] FROM [nvarchar](max) NULL
/****** Object:  UserDefinedDataType [dbo].[SiteShipmentIDType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SiteShipmentIDType] FROM [nvarchar](28) NULL
/****** Object:  UserDefinedDataType [dbo].[SiteType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SiteType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[SiteTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SiteTypeType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[SkillType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SkillType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[SlsclassType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SlsclassType] FROM [nvarchar](3) NULL
/****** Object:  UserDefinedDataType [dbo].[SlsmanType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SlsmanType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[SmallintType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SmallintType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[SmallYesNoType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SmallYesNoType] FROM [bit] NULL
/****** Object:  UserDefinedDataType [dbo].[SMTPDeliveryMethodType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SMTPDeliveryMethodType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[SortDirectionPlusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SortDirectionPlusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[SortFldPosType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SortFldPosType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[SortIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SortIdType] FROM [nvarchar](12) NULL
/****** Object:  UserDefinedDataType [dbo].[SortMethodType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SortMethodType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[SourceType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SourceType] FROM [nvarchar](4) NULL
/****** Object:  UserDefinedDataType [dbo].[SQLUserType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SQLUserType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[SSDInterchangeIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SSDInterchangeIdType] FROM [nvarchar](5) NULL
/****** Object:  UserDefinedDataType [dbo].[SsdRefTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SsdRefTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[SsnType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SsnType] FROM [nvarchar](11) NULL
/****** Object:  UserDefinedDataType [dbo].[StatementCycleType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[StatementCycleType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[StateSSDTaxIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[StateSSDTaxIdType] FROM [nvarchar](2) NULL
/****** Object:  UserDefinedDataType [dbo].[StateType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[StateType] FROM [nvarchar](5) NULL
/****** Object:  UserDefinedDataType [dbo].[StatusFreqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[StatusFreqType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[StaxSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[StaxSeqType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[SteelCutType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SteelCutType] FROM [nvarchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[SteelTagType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SteelTagType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[StepType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[StepType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[StorageMethodType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[StorageMethodType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[StringNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[StringNameType] FROM [nvarchar](50) NULL
/****** Object:  UserDefinedDataType [dbo].[SubAcctType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SubAcctType] FROM [nvarchar](12) NULL
/****** Object:  UserDefinedDataType [dbo].[SuffixNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SuffixNameType] FROM [nvarchar](3) NULL
/****** Object:  UserDefinedDataType [dbo].[SuffixPoLineProjTaskReqTrnLineType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SuffixPoLineProjTaskReqTrnLineType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[SuffixPoReqLineType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SuffixPoReqLineType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[SuffixPoReqTrnLineType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SuffixPoReqTrnLineType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[SuffixType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SuffixType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[SupplyToleranceHoursType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SupplyToleranceHoursType] FROM [decimal](4, 1) NULL
/****** Object:  UserDefinedDataType [dbo].[SupplyWebPoStatType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SupplyWebPoStatType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[SurchargeFactorType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SurchargeFactorType] FROM [decimal](5, 4) NULL
/****** Object:  UserDefinedDataType [dbo].[SurchargeSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SurchargeSeqType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[SurnameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SurnameType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[SymGroupType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SymGroupType] FROM [nvarchar](25) NULL
/****** Object:  UserDefinedDataType [dbo].[SymUserType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SymUserType] FROM [nvarchar](25) NULL
/****** Object:  UserDefinedDataType [dbo].[SystemTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[SystemTypeType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[TableGroupType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TableGroupType] FROM [nvarchar](32) NULL
/****** Object:  UserDefinedDataType [dbo].[TableIndexType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TableIndexType] FROM [nvarchar](7) NULL
/****** Object:  UserDefinedDataType [dbo].[TableNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TableNameType] FROM [nvarchar](32) NULL
/****** Object:  UserDefinedDataType [dbo].[TakenByType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TakenByType] FROM [nvarchar](15) NULL
/****** Object:  UserDefinedDataType [dbo].[TariffClassificationType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TariffClassificationType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[TaskNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TaskNumType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[TaxablePriceType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TaxablePriceType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[TaxCodeLabelType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TaxCodeLabelType] FROM [nvarchar](9) NULL
/****** Object:  UserDefinedDataType [dbo].[TaxCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TaxCodeType] FROM [nvarchar](6) NULL
/****** Object:  UserDefinedDataType [dbo].[TaxCodeTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TaxCodeTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[TaxDistrictType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TaxDistrictType] FROM [nvarchar](4) NULL
/****** Object:  UserDefinedDataType [dbo].[TaxFreeDaysType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TaxFreeDaysType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[TaxIdPromptType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TaxIdPromptType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[TaxJurType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TaxJurType] FROM [nvarchar](6) NULL
/****** Object:  UserDefinedDataType [dbo].[TaxModeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TaxModeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[TaxRateType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TaxRateType] FROM [decimal](8, 5) NULL
/****** Object:  UserDefinedDataType [dbo].[TaxRegNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TaxRegNumType] FROM [nvarchar](25) NULL
/****** Object:  UserDefinedDataType [dbo].[TaxSystemsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TaxSystemsType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[TaxSystemType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TaxSystemType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[TenureType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TenureType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[TermReasonCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TermReasonCodeType] FROM [nvarchar](3) NULL
/****** Object:  UserDefinedDataType [dbo].[TermsCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TermsCodeType] FROM [nvarchar](3) NULL
/****** Object:  UserDefinedDataType [dbo].[TermsPercentType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TermsPercentType] FROM [decimal](5, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[TerritoryCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TerritoryCodeType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[TerritoryType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TerritoryType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[TestCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TestCodeType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[TH_CartonPrefixType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TH_CartonPrefixType] FROM [nvarchar](2) NULL
/****** Object:  UserDefinedDataType [dbo].[TH_CartonSizeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TH_CartonSizeType] FROM [nvarchar](15) NULL
/****** Object:  UserDefinedDataType [dbo].[TH_ItemCategoryType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TH_ItemCategoryType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[TH_MeasurementType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TH_MeasurementType] FROM [decimal](6, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[TH_MonthType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TH_MonthType] FROM [nvarchar](2) NULL
/****** Object:  UserDefinedDataType [dbo].[TH_ShippingPortType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TH_ShippingPortType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[TH_TextType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TH_TextType] FROM [nvarchar](60) NULL
/****** Object:  UserDefinedDataType [dbo].[TH_VendInvNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TH_VendInvNumType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[TH_WHTSequenceType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TH_WHTSequenceType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[TH_WHTTaxType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TH_WHTTaxType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[TH_WHTTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TH_WHTTypeType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[TH_YearType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TH_YearType] FROM [nvarchar](4) NULL
/****** Object:  UserDefinedDataType [dbo].[TickDurationType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TickDurationType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[TickOffsetType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TickOffsetType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[TicksType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TicksType] FROM [decimal](10, 0) NULL
/****** Object:  UserDefinedDataType [dbo].[TimeCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TimeCodeType] FROM [nvarchar](15) NULL
/****** Object:  UserDefinedDataType [dbo].[TimeHoursType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TimeHoursType] FROM [decimal](4, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[TimeoutType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TimeoutType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[TimeSecondsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TimeSecondsType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[TimesPerYearType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TimesPerYearType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[TimeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TimeType] FROM [datetime] NULL
/****** Object:  UserDefinedDataType [dbo].[TimeZoneType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TimeZoneType] FROM [nvarchar](50) NULL
/****** Object:  UserDefinedDataType [dbo].[TipCreditRateType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TipCreditRateType] FROM [decimal](9, 6) NULL
/****** Object:  UserDefinedDataType [dbo].[TokenType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TokenType] FROM [decimal](11, 0) NULL
/****** Object:  UserDefinedDataType [dbo].[ToleranceDaysType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ToleranceDaysType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[TolerancePercentType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TolerancePercentType] FROM [decimal](7, 4) NULL
/****** Object:  UserDefinedDataType [dbo].[ToolTipType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ToolTipType] FROM [nvarchar](72) NULL
/****** Object:  UserDefinedDataType [dbo].[TopicType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TopicType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[TotalDaysOSType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TotalDaysOSType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[TotalHoursType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TotalHoursType] FROM [decimal](19, 8) NULL
/****** Object:  UserDefinedDataType [dbo].[TotalProdMixQuantityType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TotalProdMixQuantityType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[TotalWeightType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TotalWeightType] FROM [decimal](10, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[TPCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TPCodeType] FROM [nvarchar](17) NULL
/****** Object:  UserDefinedDataType [dbo].[TrackedOperTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TrackedOperTypeType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[TrackingNumberType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TrackingNumberType] FROM [nvarchar](34) NULL
/****** Object:  UserDefinedDataType [dbo].[TrackTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TrackTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[TrainingCostType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TrainingCostType] FROM [decimal](7, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[TrainingFeeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TrainingFeeType] FROM [decimal](7, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[TrainLocType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TrainLocType] FROM [nvarchar](35) NULL
/****** Object:  UserDefinedDataType [dbo].[TransferStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TransferStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[TransIndicatorType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TransIndicatorType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[TransitTimeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TransitTimeType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[TransNat2Type]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TransNat2Type] FROM [nvarchar](2) NULL
/****** Object:  UserDefinedDataType [dbo].[TransNatType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TransNatType] FROM [nvarchar](2) NULL
/****** Object:  UserDefinedDataType [dbo].[TransportType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TransportType] FROM [nvarchar](3) NULL
/****** Object:  UserDefinedDataType [dbo].[TRKFormName]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TRKFormName] FROM [nvarchar](50) NULL
/****** Object:  UserDefinedDataType [dbo].[TRKFormNameBase]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TRKFormNameBase] FROM [nvarchar](50) NULL
/****** Object:  UserDefinedDataType [dbo].[TrnLineType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TrnLineType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[TrnNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TrnNumType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[TrnPrefixType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TrnPrefixType] FROM [nvarchar](7) NULL
/****** Object:  UserDefinedDataType [dbo].[TrxFenceType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TrxFenceType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[TTVoucherType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TTVoucherType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[TypeOfNoteType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[TypeOfNoteType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[UdfaLevelTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[UdfaLevelTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[UetColorType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[UetColorType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[UetDataTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[UetDataTypeType] FROM [nvarchar](16) NULL
/****** Object:  UserDefinedDataType [dbo].[UetDefaultType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[UetDefaultType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[UetFldDtypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[UetFldDtypeType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[UetFormatType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[UetFormatType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[UetHeightType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[UetHeightType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[UetIndexSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[UetIndexSeqType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[UetInlineListType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[UetInlineListType] FROM [nvarchar](72) NULL
/****** Object:  UserDefinedDataType [dbo].[UetOrderType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[UetOrderType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[UetPrecisionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[UetPrecisionType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[UetScaleType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[UetScaleType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[UetSysApplyType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[UetSysApplyType] FROM [nvarchar](6) NULL
/****** Object:  UserDefinedDataType [dbo].[UetTicMarksType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[UetTicMarksType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[UetUtilTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[UetUtilTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[UetWidgetTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[UetWidgetTypeType] FROM [nvarchar](14) NULL
/****** Object:  UserDefinedDataType [dbo].[UetWidthType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[UetWidthType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[UMCategoryType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[UMCategoryType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[UMConvFactorType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[UMConvFactorType] FROM [float] NULL
/****** Object:  UserDefinedDataType [dbo].[UMConvTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[UMConvTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[UMType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[UMType] FROM [nvarchar](3) NULL
/****** Object:  UserDefinedDataType [dbo].[UnionDuesTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[UnionDuesTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[UnitCode1Type]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[UnitCode1Type] FROM [nvarchar](4) NULL
/****** Object:  UserDefinedDataType [dbo].[UnitCode2Type]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[UnitCode2Type] FROM [nvarchar](4) NULL
/****** Object:  UserDefinedDataType [dbo].[UnitCode3Type]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[UnitCode3Type] FROM [nvarchar](4) NULL
/****** Object:  UserDefinedDataType [dbo].[UnitCode4Type]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[UnitCode4Type] FROM [nvarchar](4) NULL
/****** Object:  UserDefinedDataType [dbo].[UnitCodeAccessType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[UnitCodeAccessType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[UnitWeightType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[UnitWeightType] FROM [decimal](11, 3) NULL
/****** Object:  UserDefinedDataType [dbo].[UnknownRefLineType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[UnknownRefLineType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[UnknownRefNumLastTranType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[UnknownRefNumLastTranType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[UnknownRefNumVoucherType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[UnknownRefNumVoucherType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[UnknownRefReleaseType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[UnknownRefReleaseType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[UnknownRefTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[UnknownRefTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[UnusedCharType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[UnusedCharType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[UnusedType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[UnusedType] FROM [nvarchar](32) NULL
/****** Object:  UserDefinedDataType [dbo].[UpdCurCostType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[UpdCurCostType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[URLType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[URLType] FROM [nvarchar](150) NULL
/****** Object:  UserDefinedDataType [dbo].[UserCharFldType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[UserCharFldType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[UserCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[UserCodeType] FROM [nvarchar](3) NULL
/****** Object:  UserDefinedDataType [dbo].[UserDateFldType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[UserDateFldType] FROM [datetime] NULL
/****** Object:  UserDefinedDataType [dbo].[UserDeciFldType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[UserDeciFldType] FROM [decimal](10, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[UserFlagType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[UserFlagType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[UserLogiFldType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[UserLogiFldType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[UsernameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[UsernameType] FROM [nvarchar](128) NULL
/****** Object:  UserDefinedDataType [dbo].[UtilizationType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[UtilizationType] FROM [decimal](4, 1) NULL
/****** Object:  UserDefinedDataType [dbo].[VacPeriodType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VacPeriodType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[ValueXMLTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[ValueXMLTypeType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[VariousBigRefLineType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VariousBigRefLineType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[VariousSmallRefLineType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VariousSmallRefLineType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[VarLeadTimeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VarLeadTimeType] FROM [decimal](10, 5) NULL
/****** Object:  UserDefinedDataType [dbo].[VATReportCategoryNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VATReportCategoryNameType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[VATReportLineNumberDescriptionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VATReportLineNumberDescriptionType] FROM [nvarchar](150) NULL
/****** Object:  UserDefinedDataType [dbo].[VATReportLineNumberType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VATReportLineNumberType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[VATReportNumberForTaxType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VATReportNumberForTaxType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[VATReportTaxTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VATReportTaxTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[VchDistSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VchDistSeqType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[VchHdrTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VchHdrTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[VchItemSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VchItemSeqType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[VchPrStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VchPrStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[VehicleNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VehicleNumType] FROM [nvarchar](15) NULL
/****** Object:  UserDefinedDataType [dbo].[VendInvNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VendInvNumType] FROM [nvarchar](22) NULL
/****** Object:  UserDefinedDataType [dbo].[VendInvoiceType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VendInvoiceType] FROM [nvarchar](22) NULL
/****** Object:  UserDefinedDataType [dbo].[VendItemType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VendItemType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[VendLcrNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VendLcrNumType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[VendLotType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VendLotType] FROM [nvarchar](15) NULL
/****** Object:  UserDefinedDataType [dbo].[VendNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VendNumType] FROM [nvarchar](7) NULL
/****** Object:  UserDefinedDataType [dbo].[VendOrderType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VendOrderType] FROM [nvarchar](22) NULL
/****** Object:  UserDefinedDataType [dbo].[VendorPayTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VendorPayTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[VendorStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VendorStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[VendTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VendTypeType] FROM [nvarchar](6) NULL
/****** Object:  UserDefinedDataType [dbo].[VeryLongListType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VeryLongListType] FROM [nvarchar](max) NULL
/****** Object:  UserDefinedDataType [dbo].[VotingRuleType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VotingRuleType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[VoucherType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VoucherType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[VouchSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VouchSeqType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[VSSItemType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VSSItemType] FROM [nvarchar](400) NULL
/****** Object:  UserDefinedDataType [dbo].[VSSVersionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VSSVersionType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[VTXAddressTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXAddressTypeType] FROM [nvarchar](2) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXAvataxDocCodetype]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXAvataxDocCodetype] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXAvataxDocDateType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXAvataxDocDateType] FROM [datetime] NULL
/****** Object:  UserDefinedDataType [dbo].[VTXAvataxDocTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXAvataxDocTypeType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[VTXCombRateType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXCombRateType] FROM [float] NULL
/****** Object:  UserDefinedDataType [dbo].[VTXCompCdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXCompCdType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXComponentCdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXComponentCdType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXCountryCanadaType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXCountryCanadaType] FROM [nvarchar](15) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXCountryUSType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXCountryUSType] FROM [nvarchar](15) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXCustCdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXCustCdType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXCustClassCdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXCustClassCdType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXCustExmtCertifNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXCustExmtCertifNumType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXCustExmtFlagType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXCustExmtFlagType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[VTXDatasourceType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXDatasourceType] FROM [nvarchar](70) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXDBTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXDBTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXDBVersionType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXDBVersionType] FROM [nvarchar](8) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXDebugMsgType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXDebugMsgType] FROM [nvarchar](3000) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXDebugSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXDebugSeqType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[VTXDivCdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXDivCdType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXDivCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXDivCodeType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXExemptCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXExemptCodeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXExemptLevelType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXExemptLevelType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXExemptProductType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXExemptProductType] FROM [nvarchar](50) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXExtdAmtType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXExtdAmtType] FROM [float] NULL
/****** Object:  UserDefinedDataType [dbo].[VTXGeoCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXGeoCodeType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[VTXGlAcctType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXGlAcctType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXHdrRef1Type]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXHdrRef1Type] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXHdrRef2Type]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXHdrRef2Type] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXHdrRef3Type]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXHdrRef3Type] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[VTXIndsysGeneralConnectStringType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXIndsysGeneralConnectStringType] FROM [nvarchar](75) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXIndsysMfgStatesConnStrType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXIndsysMfgStatesConnStrType] FROM [nvarchar](75) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXIndsysStatesConnectStringType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXIndsysStatesConnectStringType] FROM [nvarchar](75) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXIndsysZip4ConnectStringType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXIndsysZip4ConnectStringType] FROM [nvarchar](75) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXInputBufferType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXInputBufferType] FROM [nvarchar](3000) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXIntLineType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXIntLineType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[VTXInvCntlNoType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXInvCntlNoType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXInvDateType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXInvDateType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXInvGrossAmtType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXInvGrossAmtType] FROM [float] NULL
/****** Object:  UserDefinedDataType [dbo].[VTXInvNoType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXInvNoType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXInvTotalTaxType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXInvTotalTaxType] FROM [float] NULL
/****** Object:  UserDefinedDataType [dbo].[VTXLineNoType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXLineNoType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[VTXLineRef1Type]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXLineRef1Type] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXLineRef2Type]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXLineRef2Type] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[VTXLineRef3Type]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXLineRef3Type] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[VTXLineRefType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXLineRefType] FROM [nvarchar](4) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXLineType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXLineType] FROM [nvarchar](4) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXLineUserType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXLineUserType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXLogErrorsFileNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXLogErrorsFileNameType] FROM [nvarchar](70) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXLogErrorsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXLogErrorsType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXMsgLocType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXMsgLocType] FROM [nvarchar](50) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXMultiplierType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXMultiplierType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[VTXProdQtyType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXProdQtyType] FROM [float] NULL
/****** Object:  UserDefinedDataType [dbo].[VTXProdRptingCdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXProdRptingCdType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXProdSetCdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXProdSetCdType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXProductTaxingType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXProductTaxingType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXRefTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXRefTypeType] FROM [nvarchar](14) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXSalesUseType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXSalesUseType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXSendCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXSendCodeType] FROM [nvarchar](50) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXServerNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXServerNameType] FROM [nvarchar](70) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXSfAccessCdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXSfAccessCdType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[VTXSfGeoType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXSfGeoType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[VTXSfJurisInCityType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXSfJurisInCityType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[VTXSgeoType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXSgeoType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[VTXShortCompCdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXShortCompCdType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXShortInvNoType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXShortInvNoType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXStAccessCdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXStAccessCdType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[VTXSTEPUseType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXSTEPUseType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXStGeoType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXStGeoType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[VTXStJurisInCityType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXStJurisInCityType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[VTXStoreCdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXStoreCdType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXStoreStateType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXStoreStateType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXTaxbltyFlagType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXTaxbltyFlagType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[VTXTaxedGeoFlagType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXTaxedGeoFlagType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[VTXTaxingJurisType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXTaxingJurisType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[VTXTotalTaxType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXTotalTaxType] FROM [float] NULL
/****** Object:  UserDefinedDataType [dbo].[VTXTransCdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXTransCdType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[VTXTransDateType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXTransDateType] FROM [nvarchar](25) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXTransQtyType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXTransQtyType] FROM [float] NULL
/****** Object:  UserDefinedDataType [dbo].[VTXTransSubTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXTransSubTypeType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[VTXTransTypeRefnum]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXTransTypeRefnum] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXTransTypeRefType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXTransTypeRefType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXTransTypeTransType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXTransTypeTransType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXTransTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXTransTypeType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[VTXTWAuditType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXTWAuditType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXTWESrcType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXTWESrcType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXTweTrnDocIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXTweTrnDocIdType] FROM [bigint] NULL
/****** Object:  UserDefinedDataType [dbo].[VTXTWETrnIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXTWETrnIdType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXTWSystemType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXTWSystemType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXTXWGeoCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXTXWGeoCodeType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[VTXValidateCustomerType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[VTXValidateCustomerType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[W2BoxType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[W2BoxType] FROM [nvarchar](3) NULL
/****** Object:  UserDefinedDataType [dbo].[W2CodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[W2CodeType] FROM [nvarchar](2) NULL
/****** Object:  UserDefinedDataType [dbo].[W2EmployerTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[W2EmployerTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[W2NameCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[W2NameCodeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[WaitMonthsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WaitMonthsType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[WantAdDaysType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WantAdDaysType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[WaNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WaNumType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[WarningThresholdPercentType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WarningThresholdPercentType] FROM [decimal](7, 4) NULL
/****** Object:  UserDefinedDataType [dbo].[WaybillType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WaybillType] FROM [nvarchar](30) NULL
/****** Object:  UserDefinedDataType [dbo].[WBActionTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WBActionTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[WBAggregateNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WBAggregateNameType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[WBCategoryType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WBCategoryType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[WBColorType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WBColorType] FROM [nvarchar](11) NULL
/****** Object:  UserDefinedDataType [dbo].[WBDetailNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WBDetailNameType] FROM [nvarchar](500) NULL
/****** Object:  UserDefinedDataType [dbo].[WBDetailParmsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WBDetailParmsType] FROM [nvarchar](500) NULL
/****** Object:  UserDefinedDataType [dbo].[WBDetailTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WBDetailTypeType] FROM [nvarchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[WBDisplayTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WBDisplayTypeType] FROM [nchar](2) NULL
/****** Object:  UserDefinedDataType [dbo].[WBDrillColNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WBDrillColNameType] FROM [nchar](6) NULL
/****** Object:  UserDefinedDataType [dbo].[WBDrillNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WBDrillNumType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[WBEmailTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WBEmailTypeType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[WBFilterType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WBFilterType] FROM [nvarchar](max) NULL
/****** Object:  UserDefinedDataType [dbo].[WBFooterFormatType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WBFooterFormatType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[WBKPIGaugeTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WBKPIGaugeTypeType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[WBKPINumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WBKPINumType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[WBLinkType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WBLinkType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[WBOperatorType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WBOperatorType] FROM [nvarchar](2) NULL
/****** Object:  UserDefinedDataType [dbo].[WBProfileType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WBProfileType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[WBProgNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WBProgNameType] FROM [nvarchar](80) NULL
/****** Object:  UserDefinedDataType [dbo].[WBPropertyClassNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WBPropertyClassNameType] FROM [nvarchar](50) NULL
/****** Object:  UserDefinedDataType [dbo].[WBPropertyNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WBPropertyNameType] FROM [nvarchar](80) NULL
/****** Object:  UserDefinedDataType [dbo].[WBRecordCapType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WBRecordCapType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[WBScopeNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WBScopeNameType] FROM [nvarchar](50) NULL
/****** Object:  UserDefinedDataType [dbo].[WBScopeTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WBScopeTypeType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[WbsItemRefTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WbsItemRefTypeType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[WbsNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WbsNumType] FROM [nvarchar](15) NULL
/****** Object:  UserDefinedDataType [dbo].[WBSourceNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WBSourceNameType] FROM [nvarchar](500) NULL
/****** Object:  UserDefinedDataType [dbo].[WBSourceParmsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WBSourceParmsType] FROM [nvarchar](500) NULL
/****** Object:  UserDefinedDataType [dbo].[WBSourceTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WBSourceTypeType] FROM [nvarchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[WBSStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WBSStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[WBSymbolType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WBSymbolType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[WcOffsetType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WcOffsetType] FROM [decimal](5, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[WcsCollSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WcsCollSeqType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[WcsCollValueType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WcsCollValueType] FROM [nvarchar](40) NULL
/****** Object:  UserDefinedDataType [dbo].[WcsCriteriaFrozenZoneThruType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WcsCriteriaFrozenZoneThruType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[WcseqZoneType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WcseqZoneType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[WcsGroupIdType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WcsGroupIdType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[WcsortFieldType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WcsortFieldType] FROM [nvarchar](120) NULL
/****** Object:  UserDefinedDataType [dbo].[WcsSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WcsSeqType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[WcsSortSeqType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WcsSortSeqType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[WcType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WcType] FROM [nvarchar](6) NULL
/****** Object:  UserDefinedDataType [dbo].[WebCoPrefixType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WebCoPrefixType] FROM [nvarchar](7) NULL
/****** Object:  UserDefinedDataType [dbo].[WebCoStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WebCoStatusType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[WeekDayType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WeekDayType] FROM [tinyint] NULL
/****** Object:  UserDefinedDataType [dbo].[WeeksWorkedType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WeeksWorkedType] FROM [decimal](8, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[WeightType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WeightType] FROM [decimal](9, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[WeightUnitsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WeightUnitsType] FROM [nvarchar](3) NULL
/****** Object:  UserDefinedDataType [dbo].[WhseType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WhseType] FROM [nvarchar](4) NULL
/****** Object:  UserDefinedDataType [dbo].[WideTextType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WideTextType] FROM [nvarchar](132) NULL
/****** Object:  UserDefinedDataType [dbo].[WidgetEditorCharsType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WidgetEditorCharsType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[WidgetSliderValueType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WidgetSliderValueType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[WksBasisType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WksBasisType] FROM [nchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[WorkCountryType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WorkCountryType] FROM [nvarchar](2) NULL
/****** Object:  UserDefinedDataType [dbo].[WorkersCompensationAbbreviationType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WorkersCompensationAbbreviationType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[WorkersCompensationAgentIDType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WorkersCompensationAgentIDType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[WorkersCompensationAuthorityIDType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WorkersCompensationAuthorityIDType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[WorkersCompensationClaimNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WorkersCompensationClaimNumType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[WorkersCompensationClassCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WorkersCompensationClassCodeType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[WorkersCompensationDCOIDType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WorkersCompensationDCOIDType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[WorkersCompensationIndustryClassificationType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WorkersCompensationIndustryClassificationType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[WorkersCompensationInitialTreatmentIDType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WorkersCompensationInitialTreatmentIDType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[WorkersCompensationInjuryCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WorkersCompensationInjuryCodeType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[WorkersCompensationInjuryGroupIDType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WorkersCompensationInjuryGroupIDType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[WorkersCompensationInjuryGroupTypeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WorkersCompensationInjuryGroupTypeType] FROM [nvarchar](1) NULL
/****** Object:  UserDefinedDataType [dbo].[WorkersCompensationInsurerIDType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WorkersCompensationInsurerIDType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[WorkersCompensationJurisdictionClaimNumType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WorkersCompensationJurisdictionClaimNumType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[WorkersCompensationJurisdictionCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WorkersCompensationJurisdictionCodeType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[WorkersCompensationPolicyIDType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WorkersCompensationPolicyIDType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[WorkersCompensationStateCodeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WorkersCompensationStateCodeType] FROM [nvarchar](10) NULL
/****** Object:  UserDefinedDataType [dbo].[WorkflowNameType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WorkflowNameType] FROM [nvarchar](400) NULL
/****** Object:  UserDefinedDataType [dbo].[WorkStatusType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[WorkStatusType] FROM [nvarchar](20) NULL
/****** Object:  UserDefinedDataType [dbo].[XMLNodeIDType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[XMLNodeIDType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[XMLStringType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[XMLStringType] FROM [nvarchar](max) NULL
/****** Object:  UserDefinedDataType [dbo].[YearsInBusinessType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[YearsInBusinessType] FROM [int] NULL
/****** Object:  UserDefinedDataType [dbo].[YearType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[YearType] FROM [smallint] NULL
/****** Object:  UserDefinedDataType [dbo].[YieldType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[YieldType] FROM [decimal](5, 2) NULL
/****** Object:  UserDefinedDataType [dbo].[YtdLeadTimeType]    Script Date: 7/2/2020 9:23:11 AM ******/
CREATE TYPE [dbo].[YtdLeadTimeType] FROM [int] NULL
