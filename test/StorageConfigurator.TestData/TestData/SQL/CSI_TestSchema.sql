/****** Object:  Table [dbo].[custaddr_mst]    Script Date: 7/2/2020 9:24:00 AM ******/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE TABLE [dbo].[custaddr_mst](
	[site_ref] [dbo].[SiteType] NOT NULL,
	[cust_num] [dbo].[CustNumType] NOT NULL,
	[cust_seq] [dbo].[CustSeqType] NOT NULL,
	[name] [dbo].[NameType] NULL,
	[city] [dbo].[CityType] NULL,
	[state] [dbo].[StateType] NULL,
	[zip] [dbo].[PostalCodeType] NULL,
	[county] [dbo].[CountyType] NULL,
	[country] [dbo].[CountryType] NULL,
	[fax_num] [dbo].[PhoneType] NULL,
	[telex_num] [dbo].[PhoneType] NULL,
	[bal_method] [dbo].[BalMethodType] NULL,
	[addr##1] [dbo].[AddressType] NULL,
	[addr##2] [dbo].[AddressType] NULL,
	[addr##3] [dbo].[AddressType] NULL,
	[addr##4] [dbo].[AddressType] NULL,
	[credit_hold] [dbo].[ListYesNoType] NULL,
	[credit_hold_user] [dbo].[UserCodeType] NULL,
	[credit_hold_date] [dbo].[DateType] NULL,
	[credit_hold_reason] [dbo].[ReasonCodeType] NULL,
	[credit_limit] [dbo].[AmountType] NULL,
	[curr_code] [dbo].[CurrCodeType] NULL,
	[corp_cust] [dbo].[CustNumType] NULL,
	[corp_cred] [dbo].[ListYesNoType] NULL,
	[corp_address] [dbo].[ListYesNoType] NULL,
	[amt_over_inv_amt] [dbo].[AmountType] NULL,
	[days_over_inv_due_date] [dbo].[DaysOverType] NULL,
	[ship_to_email] [dbo].[EmailType] NULL,
	[bill_to_email] [dbo].[EmailType] NULL,
	[internet_url] [dbo].[URLType] NULL,
	[internal_email_addr] [dbo].[EmailType] NULL,
	[external_email_addr] [dbo].[EmailType] NULL,
	[NoteExistsFlag] [dbo].[FlagNyType] NOT NULL,
	[RecordDate] [dbo].[CurrentDateType] NOT NULL,
	[RowPointer] [dbo].[RowPointerType] NOT NULL,
	[CreatedBy] [dbo].[UsernameType] NOT NULL,
	[UpdatedBy] [dbo].[UsernameType] NOT NULL,
	[CreateDate] [dbo].[CurrentDateType] NOT NULL,
	[InWorkflow] [dbo].[FlagNyType] NOT NULL,
	[carrier_account] [dbo].[CarrierAccountType] NULL,
	[carrier_upcharge_pct] [dbo].[CarrierUpchargePercentType] NULL,
	[carrier_residential_indicator] [dbo].[ListYesNoType] NOT NULL,
	[carrier_bill_to_transportation] [dbo].[CarrierBillToTransportationType] NULL,
	[order_credit_limit] [dbo].[AmountType] NULL,
    /**
 CONSTRAINT [PK_custaddr_mst] PRIMARY KEY CLUSTERED 
(
	[cust_num] ASC,
	[cust_seq] ASC,
	[site_ref] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_custaddr_mst_RowPointer] UNIQUE NONCLUSTERED 
(
	[RowPointer] ASC,
	[site_ref] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_custaddr_mst_seq_cust] UNIQUE NONCLUSTERED 
(
	[cust_seq] ASC,
	[cust_num] ASC,
	[site_ref] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]**/
) ON [PRIMARY]
/****** Object:  Table [dbo].[customer_mst]    Script Date: 7/2/2020 9:24:00 AM ******/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE TABLE [dbo].[customer_mst](
	[site_ref] [dbo].[SiteType] NOT NULL,
	[cust_num] [dbo].[CustNumType] NOT NULL,
	[cust_seq] [dbo].[CustSeqType] NOT NULL,
	[contact##1] [dbo].[ContactType] NULL,
	[contact##2] [dbo].[ContactType] NULL,
	[contact##3] [dbo].[ContactType] NULL,
	[phone##1] [dbo].[PhoneType] NULL,
	[phone##2] [dbo].[PhoneType] NULL,
	[phone##3] [dbo].[PhoneType] NULL,
	[cust_type] [dbo].[CustTypeType] NULL,
	[terms_code] [dbo].[TermsCodeType] NULL,
	[ship_code] [dbo].[ShipCodeType] NULL,
	[slsman] [dbo].[SlsmanType] NULL,
	[state_cycle] [dbo].[StatementCycleType] NULL,
	[fin_chg] [dbo].[ListYesNoType] NULL,
	[last_inv] [dbo].[DateType] NULL,
	[last_paid] [dbo].[DateType] NULL,
	[sales_ytd] [dbo].[AmountType] NULL,
	[sales_lst_yr] [dbo].[AmountType] NULL,
	[disc_ytd] [dbo].[AmountType] NULL,
	[disc_lst_yr] [dbo].[AmountType] NULL,
	[last_fin_chg] [dbo].[DateType] NULL,
	[sales_ptd] [dbo].[AmountType] NULL,
	[calc_date] [dbo].[DateType] NULL,
	[num_periods] [dbo].[PeriodsAveragedType] NULL,
	[avg_days_os] [dbo].[DaysOSType] NULL,
	[num_invoices] [dbo].[InvoicesType] NULL,
	[hist_days_os] [dbo].[TotalDaysOSType] NULL,
	[larg_days_os] [dbo].[TotalDaysOSType] NULL,
	[last_days_os] [dbo].[DaysOSType] NULL,
	[avg_bal_os] [dbo].[AmountType] NULL,
	[large_bal_os] [dbo].[AmountType] NULL,
	[last_bal_os] [dbo].[AmountType] NULL,
	[whse] [dbo].[WhseType] NULL,
	[charfld1] [dbo].[UserCharFldType] NULL,
	[charfld2] [dbo].[UserCharFldType] NULL,
	[charfld3] [dbo].[UserCharFldType] NULL,
	[decifld1] [dbo].[UserDeciFldType] NULL,
	[decifld2] [dbo].[UserDeciFldType] NULL,
	[decifld3] [dbo].[UserDeciFldType] NULL,
	[logifld] [dbo].[UserLogiFldType] NULL,
	[datefld] [dbo].[UserDateFldType] NULL,
	[tax_reg_num1] [dbo].[TaxRegNumType] NULL,
	[bank_code] [dbo].[BankCodeType] NULL,
	[tax_reg_num2] [dbo].[TaxRegNumType] NULL,
	[pay_type] [dbo].[CustPayTypeType] NULL,
	[edi_cust] [dbo].[ListYesNoType] NULL,
	[branch_id] [dbo].[BranchIdType] NULL,
	[trans_nat] [dbo].[TransNatType] NULL,
	[delterm] [dbo].[DeltermType] NULL,
	[process_ind] [dbo].[ProcessIndType] NULL,
	[use_exch_rate] [dbo].[ListYesNoType] NULL,
	[tax_code1] [dbo].[TaxCodeType] NULL,
	[tax_code2] [dbo].[TaxCodeType] NULL,
	[pricecode] [dbo].[PriceCodeType] NULL,
	[ship_early] [dbo].[ListYesNoType] NULL,
	[ship_partial] [dbo].[ListYesNoType] NULL,
	[lang_code] [dbo].[LangCodeType] NULL,
	[end_user_type] [dbo].[EndUserTypeType] NULL,
	[ship_site] [dbo].[SiteType] NULL,
	[lcr_reqd] [dbo].[ListYesNoType] NULL,
	[cust_bank] [dbo].[BankCodeType] NULL,
	[draft_print_flag] [dbo].[ListYesNoType] NULL,
	[rcv_internal_email] [dbo].[ListYesNoType] NULL,
	[customer_email_addr] [dbo].[EmailType] NULL,
	[send_customer_email] [dbo].[ListYesNoType] NULL,
	[aps_pull_up] [dbo].[ListYesNoType] NULL,
	[do_invoice] [dbo].[DoInvoiceType] NULL,
	[consolidate] [dbo].[ListYesNoType] NULL,
	[inv_freq] [dbo].[InvFreqType] NULL,
	[summarize] [dbo].[ListYesNoType] NULL,
	[NoteExistsFlag] [dbo].[FlagNyType] NOT NULL,
	[RecordDate] [dbo].[CurrentDateType] NOT NULL,
	[RowPointer] [dbo].[RowPointerType] NOT NULL,
	[einvoice] [dbo].[ListYesNoType] NULL,
	[order_bal] [dbo].[AmountType] NULL,
	[posted_bal] [dbo].[AmountType] NULL,
	[CreatedBy] [dbo].[UsernameType] NOT NULL,
	[UpdatedBy] [dbo].[UsernameType] NOT NULL,
	[CreateDate] [dbo].[CurrentDateType] NOT NULL,
	[crm_guid] [dbo].[ExternalGUIDType] NULL,
	[InWorkflow] [dbo].[FlagNyType] NOT NULL,
	[print_pack_inv] [dbo].[ListYesNoType] NOT NULL,
	[one_pack_inv] [dbo].[ListYesNoType] NOT NULL,
	[inv_category] [dbo].[InvCategoryType] NOT NULL,
	[include_tax_in_price] [dbo].[ListYesNoType] NULL,
	[trans_nat_2] [dbo].[TransNat2Type] NULL,
	[use_revision_pay_days] [dbo].[ListYesNoType] NULL,
	[revision_day] [dbo].[WeekDayType] NULL,
	[revision_day_start_time##1] [dbo].[TimeType] NULL,
	[revision_day_start_time##2] [dbo].[TimeType] NULL,
	[revision_day_end_time##1] [dbo].[TimeType] NULL,
	[revision_day_end_time##2] [dbo].[TimeType] NULL,
	[pay_day] [dbo].[WeekDayType] NULL,
	[pay_day_start_time##1] [dbo].[TimeType] NULL,
	[pay_day_start_time##2] [dbo].[TimeType] NULL,
	[pay_day_end_time##1] [dbo].[TimeType] NULL,
	[pay_day_end_time##2] [dbo].[TimeType] NULL,
	[export_type] [dbo].[ListDirectIndirectNonExportType] NOT NULL,
	[active_for_data_integration] [dbo].[ListYesNoType] NOT NULL,
	[show_in_ship_to_drop_down_list] [dbo].[ListYesNoType] NOT NULL,
	[show_in_drop_down_list] [dbo].[ListYesNoType] NOT NULL,
	[sic_code] [dbo].[SICCodeType] NULL,
	[number_of_employees] [dbo].[NumberOfEmployeesType] NULL,
	[company_revenue] [dbo].[AmountType] NULL,
	[territory_code] [dbo].[TerritoryCodeType] NULL,
	[sales_team_id] [dbo].[SalesTeamIDType] NULL,
	[days_shipped_before_due_date_tolerance] [dbo].[ToleranceDaysType] NULL,
	[days_shipped_after_due_date_tolerance] [dbo].[ToleranceDaysType] NULL,
	[shipped_over_ordered_qty_tolerance] [dbo].[TolerancePercentType] NULL,
	[shipped_under_ordered_qty_tolerance] [dbo].[TolerancePercentType] NULL,
	[default_ship_to] [dbo].[CustSeqType] NOT NULL,
	[reseller_slsman] [dbo].[SlsmanType] NULL,
	[shipment_approval_required] [dbo].[ListYesNoType] NOT NULL,
	[ship_hold] [dbo].[ListYesNoType] NOT NULL,
	[ship_method_group] [dbo].[ShipMethodGroupType] NULL,
	[JP_consumption_tax_round_method] [dbo].[JP_ConsumptionTaxRoundMethodType] NOT NULL,
	[JP_consumption_tax_header_line_method] [dbo].[JP_ConsumptionTaxHeaderLineMethodType] NOT NULL,
	[vrtx_geocode] [dbo].[VTXTXWGeoCodeType] NULL,
	[include_orders_in_tax_rpt] [dbo].[ListYesNoType] NOT NULL,
	[constructive_sale_price_pct] [dbo].[SalePricePercentType] NULL,
	[tax_reg_num1_exp_date] [dbo].[DateType] NULL,
	[JP_inv_batch_cutoff_day] [dbo].[CutoffDayType] NOT NULL,
	[bank_acct_no] [dbo].[BankAccountType] NULL,
	[international_bank_account] [dbo].[InternationalBankAccountType] NULL,
	[account_name] [dbo].[AccountNameType] NULL,
	[picture] [dbo].[ImageType] NULL,
	[sepa_mandate_ref] [dbo].[SepaMandateRefType] NULL,
	[sepa_mandate_creation_date] [dbo].[DateType] NULL,
	[sepa_mandate_expiration_date] [dbo].[DateType] NULL,
	[sepa_mandate_last_used_date] [dbo].[DateType] NULL,
	[sepa_core_direct_debit] [dbo].[ListYesNoType] NOT NULL,
	[sepa_one_off_mandate] [dbo].[ListYesNoType] NOT NULL,
	[catalog_id] [dbo].[ItemCatalogIDType] NULL,
	[AU_contract_price_method] [dbo].[AU_ContractPriceMethodType] NOT NULL,
	[AU_price_by] [dbo].[ListOrderDueType] NOT NULL,
	[bank_authority_party_id] [dbo].[BankAuthorityPartyIDType] NULL,
	[Uf_ASP_CustAddr_Mail] [nvarchar](60) NULL,
	[Uf_ASP_Customers_ApprovedBy] [nvarchar](30) NULL,
	[Uf_ASP_Customers_ExpDate] [dbo].[DateTimeType] NULL,
	[asp_approved_date] [dbo].[DateType] NULL,
    /**
 CONSTRAINT [PK_customer_mst] PRIMARY KEY CLUSTERED 
(
	[cust_num] ASC,
	[cust_seq] ASC,
	[site_ref] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_customer_mst_RowPointer] UNIQUE NONCLUSTERED 
(
	[RowPointer] ASC,
	[site_ref] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]**/
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
/****** Object:  Table [dbo].[item_mst]    Script Date: 7/2/2020 9:24:01 AM ******/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
CREATE TABLE [dbo].[item_mst](
	[site_ref] [dbo].[SiteType] NOT NULL,
	[item] [dbo].[ItemType] NOT NULL,
	[description] [dbo].[DescriptionType] NULL,
	[qty_allocjob] [dbo].[QtyTotlType] NULL,
	[u_m] [dbo].[UMType] NULL,
	[lead_time] [dbo].[LeadTimeType] NOT NULL,
	[lot_size] [dbo].[QtyPerType] NULL,
	[qty_used_ytd] [dbo].[QtyCumuType] NULL,
	[qty_mfg_ytd] [dbo].[QtyCumuType] NULL,
	[abc_code] [dbo].[AbcCodeType] NULL,
	[drawing_nbr] [dbo].[DrawingNbrType] NULL,
	[product_code] [dbo].[ProductCodeType] NULL,
	[p_m_t_code] [dbo].[PMTCodeType] NULL,
	[cost_method] [dbo].[CostMethodType] NULL,
	[lst_lot_size] [dbo].[LotSizeType] NULL,
	[unit_cost] [dbo].[CostPrcType] NULL,
	[lst_u_cost] [dbo].[CostPrcType] NULL,
	[avg_u_cost] [dbo].[CostPrcType] NULL,
	[job] [dbo].[JobType] NULL,
	[suffix] [dbo].[SuffixType] NULL,
	[stocked] [dbo].[ListYesNoType] NULL,
	[matl_type] [dbo].[MatlTypeType] NULL,
	[family_code] [dbo].[FamilyCodeType] NULL,
	[low_level] [dbo].[LowLevelType] NULL,
	[last_inv] [dbo].[DateType] NULL,
	[days_supply] [dbo].[DaysSupplyType] NULL,
	[order_min] [dbo].[QtyUnitType] NULL,
	[order_mult] [dbo].[QtyUnitType] NULL,
	[plan_code] [dbo].[UserCodeType] NULL,
	[mps_flag] [dbo].[ListYesNoType] NULL,
	[accept_req] [dbo].[ListYesNoType] NULL,
	[change_date] [dbo].[DateType] NULL,
	[revision] [dbo].[RevisionType] NULL,
	[phantom_flag] [dbo].[ListYesNoType] NULL,
	[plan_flag] [dbo].[ListYesNoType] NULL,
	[paper_time] [dbo].[LeadTimeType] NOT NULL,
	[dock_time] [dbo].[LeadTimeType] NOT NULL,
	[asm_setup] [dbo].[CostPrcType] NULL,
	[asm_run] [dbo].[CostPrcType] NULL,
	[asm_matl] [dbo].[CostPrcType] NULL,
	[asm_tool] [dbo].[CostPrcType] NULL,
	[asm_fixture] [dbo].[CostPrcType] NULL,
	[asm_other] [dbo].[CostPrcType] NULL,
	[asm_fixed] [dbo].[CostPrcType] NULL,
	[asm_var] [dbo].[CostPrcType] NULL,
	[asm_outside] [dbo].[CostPrcType] NULL,
	[comp_setup] [dbo].[CostPrcType] NULL,
	[comp_run] [dbo].[CostPrcType] NULL,
	[comp_matl] [dbo].[CostPrcType] NULL,
	[comp_tool] [dbo].[CostPrcType] NULL,
	[comp_fixture] [dbo].[CostPrcType] NULL,
	[comp_other] [dbo].[CostPrcType] NULL,
	[comp_fixed] [dbo].[CostPrcType] NULL,
	[comp_var] [dbo].[CostPrcType] NULL,
	[comp_outside] [dbo].[CostPrcType] NULL,
	[sub_matl] [dbo].[CostPrcType] NULL,
	[shrink_fact] [dbo].[ScrapFactorType] NULL,
	[alt_item] [dbo].[ItemType] NULL,
	[unit_weight] [dbo].[ItemWeightType] NULL,
	[weight_units] [dbo].[WeightUnitsType] NULL,
	[charfld4] [dbo].[UserCharFldType] NULL,
	[cur_u_cost] [dbo].[CostPrcType] NULL,
	[feat_type] [dbo].[ListDepIndType] NULL,
	[var_lead] [dbo].[VarLeadTimeType] NULL,
	[feat_str] [dbo].[FeatStrType] NULL,
	[next_config] [dbo].[FeatSuffixType] NULL,
	[feat_templ] [dbo].[FeatTemplateType] NULL,
	[backflush] [dbo].[ListYesNoType] NULL,
	[charfld1] [dbo].[UserCharFldType] NULL,
	[charfld2] [dbo].[UserCharFldType] NULL,
	[charfld3] [dbo].[UserCharFldType] NULL,
	[decifld1] [dbo].[UserDeciFldType] NULL,
	[decifld2] [dbo].[UserDeciFldType] NULL,
	[decifld3] [dbo].[UserDeciFldType] NULL,
	[logifld] [dbo].[UserLogiFldType] NULL,
	[datefld] [dbo].[UserDateFldType] NULL,
	[track_ecn] [dbo].[ListYesNoType] NULL,
	[u_ws_price] [dbo].[CostPrcType] NULL,
	[comm_code] [dbo].[CommodityCodeType] NULL,
	[origin] [dbo].[EcCodeType] NULL,
	[unit_mat_cost] [dbo].[CostPrcType] NULL,
	[unit_duty_cost] [dbo].[CostPrcType] NULL,
	[unit_freight_cost] [dbo].[CostPrcType] NULL,
	[unit_brokerage_cost] [dbo].[CostPrcType] NULL,
	[cur_mat_cost] [dbo].[CostPrcType] NULL,
	[cur_duty_cost] [dbo].[CostPrcType] NULL,
	[cur_freight_cost] [dbo].[CostPrcType] NULL,
	[cur_brokerage_cost] [dbo].[CostPrcType] NULL,
	[tax_code1] [dbo].[TaxCodeType] NULL,
	[tax_code2] [dbo].[TaxCodeType] NULL,
	[bflush_loc] [dbo].[LocType] NULL,
	[reservable] [dbo].[ListYesNoType] NULL,
	[shelf_life] [dbo].[ShelfLifeType] NULL,
	[lot_prefix] [dbo].[LotPrefixType] NULL,
	[serial_prefix] [dbo].[SerialPrefixType] NULL,
	[serial_length] [dbo].[SerialLengthType] NULL,
	[issue_by] [dbo].[ListLocLotType] NULL,
	[serial_tracked] [dbo].[ListYesNoType] NULL,
	[lot_tracked] [dbo].[ListYesNoType] NULL,
	[cost_type] [dbo].[CostTypeType] NULL,
	[matl_cost] [dbo].[CostPrcType] NULL,
	[lbr_cost] [dbo].[CostPrcType] NULL,
	[fovhd_cost] [dbo].[CostPrcType] NULL,
	[vovhd_cost] [dbo].[CostPrcType] NULL,
	[out_cost] [dbo].[CostPrcType] NULL,
	[cur_matl_cost] [dbo].[CostPrcType] NULL,
	[cur_lbr_cost] [dbo].[CostPrcType] NULL,
	[cur_fovhd_cost] [dbo].[CostPrcType] NULL,
	[cur_vovhd_cost] [dbo].[CostPrcType] NULL,
	[cur_out_cost] [dbo].[CostPrcType] NULL,
	[avg_matl_cost] [dbo].[CostPrcType] NULL,
	[avg_lbr_cost] [dbo].[CostPrcType] NULL,
	[avg_fovhd_cost] [dbo].[CostPrcType] NULL,
	[avg_vovhd_cost] [dbo].[CostPrcType] NULL,
	[avg_out_cost] [dbo].[CostPrcType] NULL,
	[prod_type] [dbo].[ProdTypeType] NULL,
	[rate_per_day] [dbo].[RunRateType] NULL,
	[mps_plan_fence] [dbo].[PlanFenceType] NULL,
	[pass_req] [dbo].[ListYesNoType] NULL,
	[lot_gen_exp] [dbo].[ListYesNoType] NULL,
	[supply_site] [dbo].[SiteType] NULL,
	[prod_mix] [dbo].[ProdMixType] NULL,
	[stat] [dbo].[ItemStatusType] NULL,
	[status_chg_user_code] [dbo].[UserCodeType] NULL,
	[chg_date] [dbo].[DateType] NULL,
	[reason_code] [dbo].[ReasonCodeType] NULL,
	[supply_whse] [dbo].[WhseType] NULL,
	[due_period] [dbo].[DuePeriodType] NULL,
	[order_max] [dbo].[QtyUnitType] NULL,
	[mrp_part] [dbo].[ListYesNoType] NULL,
	[infinite_part] [dbo].[ListYesNoType] NULL,
	[NoteExistsFlag] [dbo].[FlagNyType] NOT NULL,
	[RecordDate] [dbo].[CurrentDateType] NOT NULL,
	[RowPointer] [dbo].[RowPointerType] NOT NULL,
	[supply_tolerance_hrs] [dbo].[SupplyToleranceHoursType] NULL,
	[exp_lead_time] [dbo].[LeadTimeType] NOT NULL,
	[var_exp_lead] [dbo].[VarLeadTimeType] NULL,
	[buyer] [dbo].[NameType] NULL,
	[order_configurable] [dbo].[ListYesNoType] NOT NULL,
	[job_configurable] [dbo].[ListYesNoType] NOT NULL,
	[cfg_model] [dbo].[ConfigModelType] NULL,
	[co_post_config] [dbo].[ConfigPostConfigType] NULL,
	[job_post_config] [dbo].[ConfigPostConfigType] NULL,
	[auto_job] [dbo].[ConfigAutoJobType] NOT NULL,
	[auto_post] [dbo].[ConfigAutoPostType] NOT NULL,
	[setupgroup] [dbo].[SetupGroupType] NULL,
	[CreatedBy] [dbo].[UsernameType] NOT NULL,
	[UpdatedBy] [dbo].[UsernameType] NOT NULL,
	[CreateDate] [dbo].[CurrentDateType] NOT NULL,
	[InWorkflow] [dbo].[FlagNyType] NOT NULL,
	[mfg_supply_switching_active] [dbo].[ListYesNoType] NULL,
	[time_fence_rule] [dbo].[ListTimeFenceRuleType] NULL,
	[time_fence_value] [dbo].[ApsFloatType] NULL,
	[earliest_planned_po_receipt] [dbo].[DateType] NULL,
	[use_reorder_point] [dbo].[ListYesNoType] NULL,
	[reorder_point] [dbo].[QtyUnitType] NULL,
	[fixed_order_qty] [dbo].[QtyUnitType] NULL,
	[unit_insurance_cost] [dbo].[CostPrcType] NULL,
	[unit_loc_frt_cost] [dbo].[CostPrcType] NULL,
	[cur_insurance_cost] [dbo].[CostPrcType] NULL,
	[cur_loc_frt_cost] [dbo].[CostPrcType] NULL,
	[tax_free_matl] [dbo].[ListYesNoType] NOT NULL,
	[tax_free_days] [dbo].[TaxFreeDaysType] NULL,
	[safety_stock_percent] [dbo].[SafetyStockPercentType] NOT NULL,
	[tariff_classification] [dbo].[TariffClassificationType] NULL,
	[lowdate]  AS (CONVERT([datetime],'1753-01-01',(121))),
	[rcpt_rqmt]  AS (' '),
	[active_for_data_integration] [dbo].[ListYesNoType] NOT NULL,
	[rcvd_over_po_qty_tolerance] [dbo].[TolerancePercentType] NULL,
	[rcvd_under_po_qty_tolerance] [dbo].[TolerancePercentType] NULL,
	[include_in_net_change_planning] [dbo].[ListYesNoType] NULL,
	[kit] [dbo].[ListYesNoType] NOT NULL,
	[print_kit_components] [dbo].[ListYesNoType] NOT NULL,
	[safety_stock_rule] [dbo].[SafetyStockRuleType] NULL,
	[show_in_drop_down_list] [dbo].[ListYesNoType] NOT NULL,
	[controlled_by_external_ics] [dbo].[ListYesNoType] NOT NULL,
	[inventory_ucl_tolerance] [dbo].[TolerancePercentType] NULL,
	[inventory_lcl_tolerance] [dbo].[TolerancePercentType] NULL,
	[separation_attribute] [dbo].[ApsAttribType] NULL,
	[batch_release_attribute1] [dbo].[ApsFloatType] NULL,
	[batch_release_attribute2] [dbo].[ApsFloatType] NULL,
	[batch_release_attribute3] [dbo].[ApsFloatType] NULL,
	[picture] [dbo].[ImageType] NULL,
	[active_for_customer_portal] [dbo].[ListYesNoType] NOT NULL,
	[featured] [dbo].[ListYesNoType] NOT NULL,
	[top_seller] [dbo].[ListYesNoType] NOT NULL,
	[overview] [dbo].[ProductOverviewType] NULL,
	[preassign_lots] [dbo].[ListYesNoType] NOT NULL,
	[preassign_serials] [dbo].[ListYesNoType] NOT NULL,
	[attr_group] [dbo].[AttributeGroupType] NULL,
	[dimension_group] [dbo].[AttributeGroupType] NULL,
	[lot_attr_group] [dbo].[AttributeGroupType] NULL,
	[track_pieces] [dbo].[ListYesNoType] NOT NULL,
	[bom_last_import_date] [dbo].[DateType] NULL,
	[save_current_rev_upon_bom_import] [dbo].[ListYesNoType] NOT NULL,
	[nafta_pref_crit] [dbo].[NAFTAPreferenceCriterionType] NULL,
	[subject_to_nafta_rvc] [dbo].[ListYesNoType] NOT NULL,
	[producer] [dbo].[ListYesNoType] NOT NULL,
	[nafta_country_of_origin] [dbo].[NAFTACountryOfOriginType] NULL,
	[must_use_future_rcpts_before_pln] [dbo].[ListYesNoType] NOT NULL,
	[portal_pricing_site] [dbo].[SiteType] NULL,
	[portal_pricing_enabled] [dbo].[ListYesNoType] NOT NULL,
	[subject_to_excise_tax] [dbo].[ListYesNoType] NOT NULL,
	[excise_tax_percent] [dbo].[ExciseTaxPercentType] NULL,
	[freight_amt] [dbo].[CostPrcType] NULL,
	[item_content] [dbo].[ListYesNoType] NOT NULL,
	[estimated_matl_use_up_date] [dbo].[DateType] NULL,
	[last_matl_use_up_report_date] [dbo].[DateType] NULL,
	[PP_length_linear_dimension] [dbo].[PP_OperationDimensionType] NULL,
	[PP_width_linear_dimension] [dbo].[PP_OperationDimensionType] NULL,
	[PP_height_linear_dimension] [dbo].[PP_OperationDimensionType] NULL,
	[PP_linear_dimension_u_m] [dbo].[UMType] NULL,
	[PP_density] [dbo].[AmountType] NULL,
	[PP_density_u_m] [dbo].[UMType] NULL,
	[PP_area] [dbo].[AmountType] NULL,
	[PP_area_u_m] [dbo].[UMType] NULL,
	[PP_bulk_mass] [dbo].[ItemWeightType] NULL,
	[PP_bulk_mass_u_m] [dbo].[UMType] NULL,
	[PP_ream_mass] [dbo].[ItemWeightType] NULL,
	[PP_ream_mass_u_m] [dbo].[UMType] NULL,
	[PP_grade] [dbo].[PP_GradeType] NULL,
	[PP_abnormal_size] [dbo].[ListYesNoType] NOT NULL,
	[PP_paper_mass_basis] [dbo].[PP_PaperMassBasisType] NOT NULL,
	[charge_item] [dbo].[ListYesNoType] NULL,
	[pull_up_safety_stock_rule] [dbo].[ListPullUpRuleType] NOT NULL,
	[pull_up_safety_stock_value] [dbo].[ApsFloatType] NOT NULL,
	[commodity_jurisdiction] [dbo].[CommodityJurisdictionType] NULL,
	[eccn_usml_value] [dbo].[ECCNUSMLValueType] NULL,
	[export_compliance_program] [dbo].[ExportComplianceProgramType] NULL,
	[sched_b_num] [dbo].[SchedBNumType] NULL,
	[hts_code] [dbo].[HTSCodeType] NULL,
	[country] [dbo].[CountryType] NULL,
	[fcst_bom_supply_flag] [dbo].[ListYesNoType] NOT NULL,
	[allow_on_pick_list] [dbo].[ListYesNoType] NOT NULL,
	[Uf_bay_preferred] [dbo].[SmallintType] NULL,
	[Uf_bundle_size] [dbo].[QtyUnitNoNegType] NULL,
	[Uf_prod_hours_per_unit] [dbo].[QtyUnitType] NULL,
	[Uf_sch_code] [nvarchar](25) NULL,
	[Uf_ASP_Items_RawMatlbundle] [dbo].[QtyUnitNoNegType] NULL,
	[Uf_standard_order_length] [dbo].[QtyUnitNoNegType] NULL,
	[Uf_ASP_Items_DrawingPath] [nvarchar](120) NULL,
	[Uf_ASP_Quoting_Summary] [int] NULL,
	[aps_length] [decimal](10, 2) NULL,
	[Uf_ASP_Items_ReportBundel] [dbo].[QtyUnitNoNegType] NULL,
	[Uf_Capacity_Qty] [dbo].[QtyTotlNoNegType] NULL,
    /**
 CONSTRAINT [PK_item_mst] PRIMARY KEY CLUSTERED 
(
	[item] ASC,
	[site_ref] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_item_mst_item_lowdate] UNIQUE NONCLUSTERED 
(
	[item] ASC,
	[lowdate] ASC,
	[rcpt_rqmt] ASC,
	[RowPointer] ASC,
	[site_ref] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_item_mst_level] UNIQUE NONCLUSTERED 
(
	[low_level] ASC,
	[item] ASC,
	[site_ref] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_item_mst_plan] UNIQUE NONCLUSTERED 
(
	[plan_code] ASC,
	[item] ASC,
	[site_ref] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_item_mst_product] UNIQUE NONCLUSTERED 
(
	[product_code] ASC,
	[item] ASC,
	[site_ref] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_item_mst_RowPointer] UNIQUE NONCLUSTERED 
(
	[RowPointer] ASC,
	[site_ref] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_item_mst_serial] UNIQUE NONCLUSTERED 
(
	[serial_tracked] ASC,
	[item] ASC,
	[site_ref] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_item_mst_use_reorder_point] UNIQUE NONCLUSTERED 
(
	[use_reorder_point] ASC,
	[item] ASC,
	[site_ref] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]**/
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
/****** Object:  Table [dbo].[itemvend_mst]    Script Date: 7/14/2020 9:35:49 AM ******/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE TABLE [dbo].[itemvendprice_mst](
	[site_ref] [dbo].[SiteType] NOT NULL,
	[item] [dbo].[ItemType] NOT NULL,
	[vend_num] [dbo].[VendNumType] NOT NULL,
	[effect_date] [dbo].[Date4Type] NOT NULL,
	[brk_qty##1] [dbo].[QtyUnitType] NULL,
	[brk_qty##2] [dbo].[QtyUnitType] NULL,
	[brk_qty##3] [dbo].[QtyUnitType] NULL,
	[brk_qty##4] [dbo].[QtyUnitType] NULL,
	[brk_qty##5] [dbo].[QtyUnitType] NULL,
	[brk_cost##1] [dbo].[CostPrcType] NULL,
	[brk_cost##2] [dbo].[CostPrcType] NULL,
	[brk_cost##3] [dbo].[CostPrcType] NULL,
	[brk_cost##4] [dbo].[CostPrcType] NULL,
	[brk_cost##5] [dbo].[CostPrcType] NULL,
	[unit_duty_cost] [dbo].[CostPrcType] NULL,
	[unit_freight_cost] [dbo].[CostPrcType] NULL,
	[unit_brokerage_cost] [dbo].[CostPrcType] NULL,
	[brk_qty_conv##1] [dbo].[QtyUnitType] NULL,
	[brk_qty_conv##2] [dbo].[QtyUnitType] NULL,
	[brk_qty_conv##3] [dbo].[QtyUnitType] NULL,
	[brk_qty_conv##4] [dbo].[QtyUnitType] NULL,
	[brk_qty_conv##5] [dbo].[QtyUnitType] NULL,
	[brk_cost_conv##1] [dbo].[CostPrcType] NULL,
	[brk_cost_conv##2] [dbo].[CostPrcType] NULL,
	[brk_cost_conv##3] [dbo].[CostPrcType] NULL,
	[brk_cost_conv##4] [dbo].[CostPrcType] NULL,
	[brk_cost_conv##5] [dbo].[CostPrcType] NULL,
	[NoteExistsFlag] [dbo].[FlagNyType] NOT NULL,
	[RecordDate] [dbo].[CurrentDateType] NOT NULL,
	[RowPointer] [dbo].[RowPointerType] NOT NULL,
	[CreatedBy] [dbo].[UsernameType] NOT NULL,
	[UpdatedBy] [dbo].[UsernameType] NOT NULL,
	[CreateDate] [dbo].[CurrentDateType] NOT NULL,
	[InWorkflow] [dbo].[FlagNyType] NOT NULL,
	[unit_insurance_cost] [dbo].[CostPrcType] NULL,
	[unit_loc_frt_cost] [dbo].[CostPrcType] NULL,
	[include_tax_in_cost] [dbo].[ListYesNoType] NULL,
	[stat] [dbo].[ItemvendpriceStatusType] NOT NULL,
)