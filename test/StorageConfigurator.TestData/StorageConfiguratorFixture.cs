﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Ruler.Configurator.Testing;
using StorageConfigurator.Plugin;
using StorageConfigurator.Data;
using Ruler.Configurator.Extensibility;

namespace StorageConfigurator.TestData
{
    public class StorageConfiguratorFixture : RulesTestFixture
    {
        public override string ConnectionStringName => "StorageConfigurator";

        protected override void ConfigureServices(IServiceCollection services)
        {
            var plugin = new StoragePlugin();
            plugin.ConfigureServices(services, Configuration);
            //TODO: Remove this after upgrading to ruler 1.3.0
            services.AddMemoryCache();
            services.AddSingleton<IConfiguration>(Configuration);

            //services.AddSingleton((s) => new AdvanceCSIConnectionFactory(Configuration));

            //var csiDbConnection = Configuration.GetConnectionString("CSIDbConnection");
            //services.AddDbContext<AdvanceCSIContext>(optionsBuilder => optionsBuilder.UseSqlServer(csiDbConnection), optionsLifetime: ServiceLifetime.Singleton);
            //services.AddSingleton<AdvanceCSIContextFactory>();
            //services.AddScoped<AdvanceCSIRepository>();
            services.AddScoped<IDatabaseInitializer, AdvanceCSITestContextInitializer>();

            //services.AddSingleton<AdvanceCSIItemService>();

        }

        public StorageConfiguratorContext CreateStorageConfiguratorContext()
        {
            var connectionString = this.Configuration.GetConnectionString("StorageConfigurator");
            var optionsBuilder = new DbContextOptionsBuilder<StorageConfiguratorContext>();
            optionsBuilder.UseSqlServer(connectionString);
            return new StorageConfiguratorContext(optionsBuilder.Options);
        }

    }
}
