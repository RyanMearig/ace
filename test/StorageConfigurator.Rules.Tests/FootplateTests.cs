﻿using Xunit;
using StorageConfigurator.TestData;

namespace StorageConfigurator.Rules.Tests
{
    [Collection(nameof(RulesCollection))]
    public class FootplateTests
    {
        #region Private Fields

        private readonly StorageConfiguratorFixture _fixture;

        #endregion

        public FootplateTests(StorageConfiguratorFixture fixture)
        {
            _fixture = fixture;
        }

        #region Tests

        [Theory]
        [InlineData("FP1K-A", false, "none", 2, 7)]
        [InlineData("FP1K-B", true, "none", 2, 7)]
        [InlineData("FP1K-C", false, "angle", 2, 7)]
        [InlineData("FP1K-D", true, "angle", 2, 7)]
        [InlineData("FP2K-A", false, "bullnose", 2, 7)]
        [InlineData("FP2K-B", true, "bullnose", 2, 8)]
        [InlineData("FP3-A", false, "none", 1, 7)]
        [InlineData("FP3-B", true, "none", 1, 7)]
        [InlineData("FP3-C", false, "angle", 1, 7)]
        [InlineData("FP3-D", true, "angle", 1, 7)]
        [InlineData("FP4-A", false, "bullnose", 1, 8)]
        [InlineData("FP4-B", true, "bullnose", 1, 9.5f)]
        public void CanGetFootplateValuesFromType(string type, bool hasDoubler, string postProtector, int anchorQty, float length)
        {
            var part = _fixture.CreateRootPart<Footplate>();
            part.BasePlateType.SetValue(type);

            Assert.True(hasDoubler == part.HasDoubler.Value);
            Assert.True(postProtector == part.PostProtector.Value);
            Assert.True(anchorQty == part.AnchorQty.Value);
            Assert.True(length == part.BasePlateLength.Value);
        }

        [Theory]
        [InlineData("FP1K-A", false, "none", 2, 7, "SquareFootplate")]
        [InlineData("FP1K-B", true, "none", 2, 7, "SquareFootplate")]
        [InlineData("FP1K-C", false, "angle", 2, 7, "SquareFootplate")]
        [InlineData("FP1K-D", true, "angle", 2, 7, "SquareFootplate")]
        [InlineData("FP2K-A", false, "bullnose", 2, 7, "FootplateWBullnose")]
        [InlineData("FP2K-B", true, "bullnose", 2, 8, "FootplateWBullnose")]
        [InlineData("FP3-A", false, "none", 1, 7, "SquareFootplate")]
        [InlineData("FP3-B", true, "none", 1, 7, "SquareFootplate")]
        [InlineData("FP3-C", false, "angle", 1, 7, "SquareFootplate")]
        [InlineData("FP3-D", true, "angle", 1, 7, "SquareFootplate")]
        [InlineData("FP4-A", false, "bullnose", 1, 8, "FootplateWBullnose")]
        [InlineData("FP4-B", true, "bullnose", 1, 9.5f, "FootplateWBullnose")]
        public void CanGetCorrectFootplateType(string type, bool hasDoubler, string postProtector, int anchorQty, float length, string factoryName)
        {
            var part = _fixture.CreateRootPart<FrameUpright>();
            part.HasFrontDoubler.SetValue(hasDoubler);
            part.FrontPostProtector.SetValue(postProtector);
            part.FrontAnchorQty.SetValue(anchorQty);

            Assert.True(type == part.FrontUprightColumnAssembly.Footplate.BasePlateType.Value);
            Assert.True(length == part.FrontUprightColumnAssembly.Footplate.BasePlateLength.Value);
            Assert.True(factoryName == part.FrontUprightColumnAssembly.Footplate.FactoryName.Value);
        }

        #endregion
    }
}
