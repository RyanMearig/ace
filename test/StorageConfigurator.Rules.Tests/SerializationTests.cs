﻿using Newtonsoft.Json;
using Xunit;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Reflection;
using Ruler.Rules;
using StorageConfigurator.Plugin;
using Ruler.Rules.Configuration;
using StorageConfigurator.TestData;

namespace StorageConfigurator.Rules.Tests
{
    [Collection(nameof(RulesCollection))]
    public class SerializationTests
    {
        #region Private Fields

        private readonly StorageConfiguratorFixture _fixture;

        #endregion

        public SerializationTests(StorageConfiguratorFixture fixture)
        {
            _fixture = fixture;
        }

        [Fact]
        public void CanSerializeStorageSystem_Web()
        {
            var part = _fixture.CreateRootPart<StorageSystem>();
            var settings = _fixture.CreatePartSerializerSettings();
            var json = JsonConvert.SerializeObject(part, settings.WebFullModel);
            Assert.NotNull(json);
        }

        [Fact]
        public void CanSerializeStorageSystem_Cad()
        {
            var part = _fixture.CreateRootPart<StorageSystem>();
            var settings = _fixture.CreatePartSerializerSettings();
            var json = JsonConvert.SerializeObject(part, settings.Cad);
            Assert.NotNull(json);
        }

        [Fact]
        public void CanSerializeSimpleStorageSystem_Cad()
        {
            var part = _fixture.CreateRootPart<StorageSystem>();
            var bay = part.Bays.AddChild<Bay>();

            var frameType = part.FrameTypes.AddChild<SelectiveFrameType>();
            var frameline = part.FrameLines.AddChild<SelectiveFrameLine>();

            var rowType = part.RowTypes.AddChild<Row>();
            var testRow = part.AddChild<RowProxy>();

            var settings = _fixture.CreatePartSerializerSettings();
            var json = JsonConvert.SerializeObject(part, settings.Cad);
            Assert.NotNull(json);
        }

        [Fact]
        public void CanSerializeStandardOptionStorageSystem_Cad()
        {
            var part = _fixture.CreateRootPart<StorageSystem>();

            var bay = part.Bays.AddChild<Bay>();
            var ddBay = part.Bays.AddChild<Bay>();
            ddBay.IsDoubleDeep.SetValue(true);
            var ddPBay = part.Bays.AddChild<Bay>();
            ddPBay.IsDoubleDeep.SetValue(true);
            ddPBay.DoubleDeepType.SetValue("post");

            var sFrameType = part.FrameTypes.AddChild<SelectiveFrameType>();
            var sFrameline = part.FrameLines.AddChild<SelectiveFrameLine>();

            var ddFrameType = part.FrameTypes.AddChild<DoubleDeepFrameType>();
            var ddFrameline = part.FrameLines.AddChild<SelectiveFrameLine>();
            ddFrameline.FrameName.SetValue(ddFrameType.Path);

            var ddpFrameType = part.FrameTypes.AddChild<DoubleDeepFrameType>();
            ddpFrameType.DoubleDeepType.SetValue("post");
            var ddpFrameline = part.FrameLines.AddChild<SelectiveFrameLine>();
            ddpFrameline.FrameName.SetValue(ddpFrameType.Path);

            var sRowType = part.RowTypes.AddChild<Row>();

            var ddRowType = part.RowTypes.AddChild<Row>();
            ddRowType.Frames.First().FrameName.SetValue(ddFrameline.Path);
            var ddPRowType = part.RowTypes.AddChild<Row>();
            ddPRowType.Frames.First().FrameName.SetValue(ddpFrameline.Path);
            var sTestRow = part.AddChild<RowProxy>();
            var ddTestRow = part.AddChild<RowProxy>();
            ddTestRow.RowTypeName.SetValue(ddRowType.Path);
            var ddpTestRow = part.AddChild<RowProxy>();
            ddpTestRow.RowTypeName.SetValue(ddPRowType.Path);

            var settings = _fixture.CreatePartSerializerSettings();
            var json = JsonConvert.SerializeObject(part, settings.Cad);
            Assert.NotNull(json);
        }

        [Fact]
        public void CanSerializeShippableParts_Web()
        {
            var part = _fixture.CreateRootPart<ShippableParts>();
            var settings = _fixture.CreatePartSerializerSettings();
            var json = JsonConvert.SerializeObject(part, settings.WebFullModel);
            Assert.NotNull(json);
        }

        [Fact]
        public void CanSearlizeStorageSystemTwoBay()
        {
            var part = _fixture.CreateRootPart<StorageSystem>();
            var bay = part.Bays.AddChild<Bay>();
            bay.LevelQty.SetValue(3);

            var frameType = part.FrameTypes.AddChild<SelectiveFrameType>();
            frameType.Height.SetValue(220);
            frameType.Frame.HasFrontDoubler.SetValue(true);
            part.FrameLines.AddChild<SelectiveFrameLine>();

            var testRow = part.RowTypes.AddChild<Row>();
            testRow.BayQty.SetValue(2);
            part.AddChild<RowProxy>();

            var settings = _fixture.CreatePartSerializerSettings();
            var json = JsonConvert.SerializeObject(part, settings.Cad);
            Assert.NotNull(json);
        }

        [Fact]
        public void CanSearlizeStorageSystemBackToBackFrames()
        {
            var part = _fixture.CreateRootPart<StorageSystem>();
            var bay = part.Bays.AddChild<Bay>();
            bay.LevelQty.SetValue(3);

            var frameType = part.FrameTypes.AddChild<SelectiveFrameType>();
            frameType.Height.SetValue(220);
            frameType.Frame.HasFrontDoubler.SetValue(true);
            var frameline = part.FrameLines.AddChild<SelectiveFrameLine>();
            frameline.IsBackToBack.SetValue(true);

            var testRow = part.RowTypes.AddChild<Row>();
            part.AddChild<RowProxy>();

            var settings = _fixture.CreatePartSerializerSettings();
            var json = JsonConvert.SerializeObject(part, settings.Cad);
            Assert.NotNull(json);
        }

        [Fact]
        public void CanSearlizeStorageSystemMultiRows()
        {
            var part = _fixture.CreateRootPart<StorageSystem>();
            var bay = part.Bays.AddChild<Bay>();
            bay.LevelQty.SetValue(3);

            var frameType = part.FrameTypes.AddChild<SelectiveFrameType>();
            frameType.Height.SetValue(220);
            frameType.Frame.HasFrontDoubler.SetValue(true);
            part.FrameLines.AddChild<SelectiveFrameLine>();

            var testRow = part.RowTypes.AddChild<Row>();
            testRow.BayQty.SetValue(2);
            part.AddChild<RowProxy>();

            var testRow2 = part.RowTypes.AddChild<Row>();
            var testRow2Prox = part.AddChild<RowProxy>();
            testRow2Prox.RowTypeName.SetValue(testRow2.RowTypeName.Value);

            var settings = _fixture.CreatePartSerializerSettings();
            var json = JsonConvert.SerializeObject(part, settings.Cad);
            Assert.NotNull(json);
        }

        [Fact]
        public void CanSerializeShippableParts_Cad()
        {
            var part = _fixture.CreateRootPart<ShippableParts>();
            //part.AddChild<FrameUpright>();
            //var frameUpright = part.AddChild<FrameUpright>();

            var settings = _fixture.CreatePartSerializerSettings();
            var json = JsonConvert.SerializeObject(part, settings.Cad);
            Assert.NotNull(json);
        }

        [Fact]
        public void CanSerializeFrameUpright_Web()
        {
            var part = _fixture.CreateRootPart<FrameUpright>();
            var settings = _fixture.CreatePartSerializerSettings();
            var json = JsonConvert.SerializeObject(part, settings.WebFullModel);
            Assert.NotNull(json);
        }

        [Fact]
        public void CanSerializeFrameUpright_Cad()
        {
            var part = _fixture.CreateRootPart<FrameUpright>();
            part.Height.SetValue(120);
            part.UprightColumnSize.SetValue("C4x4.5");
            part.HasFrontDoubler.SetValue(true);
            part.FrontDoublerHeight.SetValue(20);
            part.HasRearDoubler.SetValue(true);
            part.RearDoublerHeight.SetValue(84);

            var settings = _fixture.CreatePartSerializerSettings();
            var json = JsonConvert.SerializeObject(part, settings.Cad);
            Assert.NotNull(json);
        }

        [Fact]
        public void CanSerializeFootplate_Web()
        {
            var part = _fixture.CreateRootPart<Footplate>();
            var settings = _fixture.CreatePartSerializerSettings();
            var json = JsonConvert.SerializeObject(part, settings.WebFullModel);
            Assert.NotNull(json);
        }

        [Theory]
        [InlineData("FP1K-A")]
        [InlineData("FP1K-B")]
        [InlineData("FP1K-C")]
        [InlineData("FP1K-D")]
        [InlineData("FP2K-A")]
        [InlineData("FP2K-B")]
        [InlineData("FP3-A")]
        [InlineData("FP3-B")]
        [InlineData("FP3-C")]
        [InlineData("FP3-D")]
        [InlineData("FP4-A")]
        [InlineData("FP4-B")]
        public void CanSerializeFootplate_Cad(string type)
        {
            var part = _fixture.CreateRootPart<Footplate>();
            part.BasePlateType.SetValue(type);
            part.BeamType.SetValue("S");
            part.ChannelWidth.SetValue(3);

            var settings = _fixture.CreatePartSerializerSettings();
            var json = JsonConvert.SerializeObject(part, settings.Cad);
            Assert.NotNull(json);
        }

        [Fact]
        public void CanSerializeLoadBeam_Web()
        {
            var part = _fixture.CreateRootPart<ShelfLoadBeam>();
            var settings = _fixture.CreatePartSerializerSettings();
            var json = JsonConvert.SerializeObject(part, settings.WebFullModel);
            Assert.NotNull(json);
        }

        [Theory]
        [InlineData("8", "C3x3.5")]
        [InlineData("8infAdj", "C3x3.5")]
        [InlineData("9", "C3x3.5")]
        [InlineData("12", "C3x3.5")]
        [InlineData("13", "C3x3.5")]
        [InlineData("8", "C8x11.5")]
        [InlineData("8infAdj", "C8x11.5")]
        [InlineData("9", "C8x11.5")]
        [InlineData("12", "C8x11.5")]
        [InlineData("13", "C8x11.5")]
        public void CanSerializeLoadBeam_Cad(string bracketType, string beamSize)
        {
            var part = _fixture.CreateRootPart<ShelfLoadBeam>();
            part.ChannelSize.SetValue(beamSize);
            part.BracketType.SetValue(bracketType);

            var settings = _fixture.CreatePartSerializerSettings();
            var json = JsonConvert.SerializeObject(part, settings.Cad);
            Assert.NotNull(json);
        }

        #region Bay Tests
        [Fact]
        public void CanSerializeStdBay_Web()
        {
            var part = _fixture.CreateRootPart<Bay>();
            var settings = _fixture.CreatePartSerializerSettings();
            var json = JsonConvert.SerializeObject(part, settings.WebFullModel);
            Assert.NotNull(json);
        }

        [Fact]
        public void CanSerializeStdBay_Cad()
        {
            var part = _fixture.CreateRootPart<Bay>();
            var settings = _fixture.CreatePartSerializerSettings();
            var json = JsonConvert.SerializeObject(part, settings.Cad);
            Assert.NotNull(json);
        }

        [Fact]
        public void CanSerializeDoubleDeepFrameBay_Web()
        {
            var part = _fixture.CreateRootPart<Bay>();
            part.IsDoubleDeep.SetValue(true);
            var settings = _fixture.CreatePartSerializerSettings();
            var json = JsonConvert.SerializeObject(part, settings.WebFullModel);
            Assert.NotNull(json);
        }

        [Fact]
        public void CanSerializeDoubleDeepFrameBay_Cad()
        {
            var part = _fixture.CreateRootPart<Bay>();
            part.IsDoubleDeep.SetValue(true);
            var settings = _fixture.CreatePartSerializerSettings();
            var json = JsonConvert.SerializeObject(part, settings.Cad);
            Assert.NotNull(json);
        }

        [Fact]
        public void CanSerializeDoubleDeepPostBay_Web()
        {
            var part = _fixture.CreateRootPart<Bay>();
            part.IsDoubleDeep.SetValue(true);
            part.DoubleDeepType.SetValue("post");
            var settings = _fixture.CreatePartSerializerSettings();
            var json = JsonConvert.SerializeObject(part, settings.WebFullModel);
            Assert.NotNull(json);
        }

        [Fact]
        public void CanSerializeDoubleDeepPostBay_Cad()
        {
            var part = _fixture.CreateRootPart<Bay>();
            part.IsDoubleDeep.SetValue(true);
            part.DoubleDeepType.SetValue("post");
            var settings = _fixture.CreatePartSerializerSettings();
            var json = JsonConvert.SerializeObject(part, settings.Cad);
            Assert.NotNull(json);
        }

        #endregion

        #region FrameLine Tests

        [Fact]
        public void CanSerializeSelectiveFrameLine_Web()
        {
            var part = _fixture.CreateRootPart<SelectiveFrameType>();
            var settings = _fixture.CreatePartSerializerSettings();
            var json = JsonConvert.SerializeObject(part, settings.WebFullModel);
            Assert.NotNull(json);
        }

        [Fact]
        public void CanSerializeSelectiveFrameLine_Cad()
        {
            var part = _fixture.CreateRootPart<SelectiveFrameType>();
            var settings = _fixture.CreatePartSerializerSettings();
            var json = JsonConvert.SerializeObject(part, settings.Cad);
            Assert.NotNull(json);
        }

        [Fact]
        public void CanSerializeSelectiveHeavyFrameLine_Web()
        {
            var part = _fixture.CreateRootPart<SelectiveFrameType>();
            part.UprightType.SetValue("H");
            var settings = _fixture.CreatePartSerializerSettings();
            var json = JsonConvert.SerializeObject(part, settings.WebFullModel);
            Assert.NotNull(json);
        }

        [Fact]
        public void CanSerializeSelectiveHeavyFrameLine_Cad()
        {
            var part = _fixture.CreateRootPart<SelectiveFrameType>();
            part.UprightType.SetValue("H");
            var settings = _fixture.CreatePartSerializerSettings();
            var json = JsonConvert.SerializeObject(part, settings.Cad);
            Assert.NotNull(json);
        }

        [Fact]
        public void CanSerializeDDFFFrameLine_Web()
        {
            var part = _fixture.CreateRootPart<DoubleDeepFrameType>();
            var settings = _fixture.CreatePartSerializerSettings();
            var json = JsonConvert.SerializeObject(part, settings.WebFullModel);
            Assert.NotNull(json);
        }

        [Fact]
        public void CanSerializeDDFFFrameLine_Cad()
        {
            var part = _fixture.CreateRootPart<DoubleDeepFrameType>();
            var settings = _fixture.CreatePartSerializerSettings();
            var json = JsonConvert.SerializeObject(part, settings.Cad);
            Assert.NotNull(json);
        }
        [Fact]
        public void CanSerializeDDPostFrameLine_Web()
        {
            var part = _fixture.CreateRootPart<DoubleDeepFrameType>();
            part.DoubleDeepType.SetValue("post");
            var settings = _fixture.CreatePartSerializerSettings();
            var json = JsonConvert.SerializeObject(part, settings.WebFullModel);
            Assert.NotNull(json);
        }

        [Fact]
        public void CanSerializeDDPostFrameLine_Cad()
        {
            var part = _fixture.CreateRootPart<DoubleDeepFrameType>();
            part.DoubleDeepType.SetValue("post");
            var settings = _fixture.CreatePartSerializerSettings();
            var json = JsonConvert.SerializeObject(part, settings.Cad);
            Assert.NotNull(json);
        }

        #endregion

        #region End Aisle Guard Tests

        [Fact]
        public void CanSerializeEndAisleGuard_Web()
        {
            var part = _fixture.CreateRootPart<EndAisleGuard>();
            var settings = _fixture.CreatePartSerializerSettings();
            var json = JsonConvert.SerializeObject(part, settings.WebFullModel);
            Assert.NotNull(json);
        }

        [Fact]
        public void CanSerializeEndAisleGuard_CAD()
        {
            var part = _fixture.CreateRootPart<EndAisleGuard>();
            part.SpaceQty.SetValue(2);
            part.Space1.SetValue(10);

            var settings = _fixture.CreatePartSerializerSettings();
            var json = JsonConvert.SerializeObject(part, settings.Cad);
            Assert.NotNull(json);
        }

        #endregion

        [Fact]
        public void Testing()
        {
            float slantElev = 36;
            float slantInset = 12;

            //use Pythagorean Theorem
            float a = (slantInset);
            float b = (slantElev);
            float hyp = (float)Math.Sqrt((Math.Pow(a, 2) + Math.Pow(b, 2)));

            double angleInRad = Math.Asin(a / hyp);

            var rotationInDegrees = Helper.RadToDeg((float)angleInRad);

            //List<float> frameList = new List<float>() { 1, 2, 3 };

            //List<List<float>> retList = frameList.Select((value, index) => new { IncNum = index / 4, value })
            //                                        .GroupBy(pair => pair.IncNum)
            //                                        .Select(grp => grp.Select(g => g.value).ToList())
            //                                        .ToList();

            //List<float> frameList = new List<float>() { 1, 2, 3, 4 };
            //List<float> bayList = new List<float>() { 5, 6, 7 };

            //var result = frameList.SelectMany((f, i) =>
            //{
            //    if (i+1 > bayList.Count)
            //        return new List<float> { f };
            //    else
            //        return new List<float> { f, bayList[i] };
            //}).ToList();

            //List<float> testList = new List<float>() { 6, 28, 70, 110, 132, 150, 170, 190, 220, 280, 330, 350, 360 };

            //List<float> retList = RetList(testList);

            Assert.True(true);
            //float val = 43.1234f;
            //float expected = 44;
            //var temp = System.Convert.ToInt32("FFA500", 16);
            //float result = Helper.RoundToNearest(val, nearest: 4);
            //Assert.True(result == expected);

            //float val = 1.09375f;
            //float expected = 1.0625f;

            //float result = Helper.RoundDownToNearest(val, increment: 0.0625f);
            //Assert.True(result == expected);
        }

        [Fact]
        public async void CanGetSharedJsonRuleDefinitions()
        {
            var assy = Assembly.GetExecutingAssembly();
            var singleDataPath = "StorageConfigurator.Rules.Tests.TestData.TestRulesSeedData";
            var sharedDataPath = "StorageConfigurator.Rules.Tests.TestData.TestSharedRulesSeedData";

            var resourceMap = new Dictionary<string, IEnumerable<string>>()
            {
                { "UprightColumn", new List<string>{ "BeamMaterialPartNumber" } },
                { "HeavyHorizontalBrace", new List<string>{ "BeamMaterialPartNumber", "TestRules" } }
            };


            var provider = new JsonResourceSharedRulesSeedDataProvider(assy, singleDataPath, sharedDataPath, resourceMap);

            var hostDefinitions = new List<RuleHostDefinition>();
            await foreach(var hostDef in provider.GetRuleHostDefinitionsAsync())
            {
                hostDefinitions.Add(hostDef);
            }

            Assert.Equal(3, hostDefinitions.Count);
            var uprightDef = hostDefinitions.FirstOrDefault(h => h.Name == "UprightColumn");
            var uprightMaterialPnDef = uprightDef.Rules.FirstOrDefault(r => r.Name == "MaterialPartNumber");
            var uprighTestDef = uprightDef.Rules.FirstOrDefault(r => r.Name == "Foo");
            Assert.NotNull(uprightMaterialPnDef);
            Assert.Null(uprighTestDef);
            Assert.True(uprightDef.Rules.Count > 1);

            var heavyHorizHostDef = hostDefinitions.FirstOrDefault(h => h.Name == "HeavyHorizontalBrace");
            var heavyHorizMaterialPnDef = heavyHorizHostDef.Rules.FirstOrDefault(r => r.Name == "MaterialPartNumber");
            var heavyHorizTestDef = heavyHorizHostDef.Rules.FirstOrDefault(r => r.Name == "Foo");
            Assert.NotNull(heavyHorizMaterialPnDef);
            Assert.NotNull(heavyHorizTestDef);
            Assert.True(heavyHorizHostDef.Rules.Count > 1);

        }


        public List<float> RetList(List<float> locs)
        {
            List<float> retVal = new List<float>();
            List<float> HorizBraceLocsTopDown = locs.Reverse<float>().ToList();

            retVal = RecurseGetLocs(HorizBraceLocsTopDown, retVal, HorizBraceLocsTopDown.FirstOrDefault());

            return retVal;
        }

        public List<float> RecurseGetLocs(List<float> searchList, List<float> locList, float searchVal)
        {
            List<float> retVal = locList;
            float closestLoc = GetClosestLocation(searchList, searchVal);
            retVal.Add(closestLoc);

            float newSearchVal = closestLoc - 120;
            if (newSearchVal > searchList.Last())
                retVal = RecurseGetLocs(searchList, retVal, newSearchVal);

            return retVal;
        }

        public float GetClosestLocation(List<float> searchList, float searchVal)
        {
            return searchList.Where(x => x >= searchVal).Last();
        }
    }
}
