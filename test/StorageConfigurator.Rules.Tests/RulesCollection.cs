﻿using Xunit;
using StorageConfigurator.TestData;

namespace StorageConfigurator.Rules.Tests
{
    [CollectionDefinition(nameof(RulesCollection))]
    public class RulesCollection : ICollectionFixture<StorageConfiguratorFixture>
    {
    }
}
