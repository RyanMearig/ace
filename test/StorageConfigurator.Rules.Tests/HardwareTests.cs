﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;
using Newtonsoft.Json;
using Xunit;
using System.Linq;
using StorageConfigurator.TestData;

namespace StorageConfigurator.Rules.Tests
{
    [Collection(nameof(RulesCollection))]
    public class HardwareTests
    {

        #region Private Fields

        private readonly StorageConfiguratorFixture _fixture;

        #endregion

        #region Constructors

        public HardwareTests(StorageConfiguratorFixture fixture)
        {
            _fixture = fixture;
        }

        #endregion

        #region Tests

        [Theory]
        [InlineData("xb", "0.375", "round", 0.4375f, 0.375f, 1, BoltTypes.Hex, 1, 2)]
        [InlineData("xb", "0.5", "round", 0.5625f, 0.5, 1, BoltTypes.Hex, 1, 2)]
        [InlineData("xbw", "0.375", "round", 0.4375f, 0.375f, 1, BoltTypes.Hex, 1, 2)]
        [InlineData("xbw", "0.5", "round", 0.5625f, 0.5, 1, BoltTypes.Hex, 1, 2)]
        [InlineData("xbw-dd1", "0.375", "round", 0.4375f, 0.375f, 1, BoltTypes.Hex, 1, 3)]
        [InlineData("xbw-dd1", "0.5", "round", 0.5625f, 0.5, 1, BoltTypes.Hex, 1, 3)]
        [InlineData("xbw-dd2", "0.375", "round", 0.4375f, 0.375f, 1, BoltTypes.Hex, 1, 4)]
        [InlineData("xbw-dd2", "0.5", "round", 0.5625f, 0.5, 1, BoltTypes.Hex, 1, 4)]
        [InlineData("xbwr1", "0.375", "round", 0.4375f, 0.375f, 1, BoltTypes.Hex, 1, 2)]
        [InlineData("xbwr1", "0.5", "round", 0.5625f, 0.5, 1, BoltTypes.Hex, 1, 2)]
        [InlineData("xbwr2", "0.5", "square", 0.53125f, 0.5f, 1, BoltTypes.Hex, 1, 2)]
        public void CanGetCrossbarHardware(string type, string hardwareSize, string holeType, float expectedHoleSize, float expectedBoltSize, float expectedBoltLength,
                                            string expectedBoltType, int expectedKitQty, int expectedTotalQty)
        {
            var part = _fixture.CreateRootPart<ShelfBeamCrossbar>();
            part.Type.SetValue(type);
            part.FrontBeamHardwareSize.SetValue(hardwareSize);
            part.FrontInteriorBeamHardwareSize.SetValue(hardwareSize);
            part.RearInteriorBeamHardwareSize.SetValue(hardwareSize);
            part.RearBeamHardwareSize.SetValue(hardwareSize);
            part.HoleType.SetValue(holeType);

            var hardwareKits = part.Hardwares;

            Assert.Equal(expectedHoleSize, part.HoleSize.Value);
            Assert.Equal(expectedBoltSize, hardwareKits.First().BoltSize.Value);
            Assert.Equal(expectedBoltLength, hardwareKits.First().BoltLength.Value);
            Assert.Equal(expectedBoltType, hardwareKits.First().BoltType.Value);
            Assert.Equal(expectedKitQty, hardwareKits.Count());
            Assert.Equal(expectedTotalQty, hardwareKits.Sum(d => d.Qty.Value));
        }

        [Theory]
        [InlineData("rs", false, false, "C3x3.5", 0.5f, 1, 0, BoltTypes.Hex, 1, 2)]
        [InlineData("rs", false, false, "C5x6.1", 0.5f, 1.25f, 0, BoltTypes.Hex, 1, 2)]
        [InlineData("rs", true, false, "C3x3.5", 0.5f, 4, 1, BoltTypes.Hex, 2, 2)]
        [InlineData("rs", false, true, "C5x6.1", 0.5f, 1.25f, 4.5f, BoltTypes.Hex, 2, 2)]
        [InlineData("rs", true, true, "C3x3.5", 0.5f, 4, 0, BoltTypes.Hex, 1, 2)]
        [InlineData("rs", true, true, "C5x6.1", 0.5f, 4.5, 0, BoltTypes.Hex, 1, 2)]
        [InlineData("rfsw", false, false, "C3x3.5", 0.5f, 1, 0, BoltTypes.Hex, 1, 4)]
        [InlineData("rfsw", false, false, "C5x6.1", 0.5f, 1.25f, 0, BoltTypes.Hex, 1, 4)]
        [InlineData("rfsw", true, false, "C3x3.5", 0.5f, 4, 1, BoltTypes.Hex, 2, 4)]
        [InlineData("rfsw", false, true, "C5x6.1", 0.5f, 1.25f, 4.5f, BoltTypes.Hex, 2, 4)]
        [InlineData("rfsw", true, true, "C3x3.5", 0.5f, 4, 0, BoltTypes.Hex, 1, 4)]
        [InlineData("rfsw", true, true, "C5x6.1", 0.5f, 4.5, 0, BoltTypes.Hex, 1, 4)]
        [InlineData("rfswhd", false, false, "C3x3.5", 0.5f, 1, 0, BoltTypes.Hex, 1, 4)]
        [InlineData("rfswhd", false, false, "C5x6.1", 0.5f, 1.25f, 0, BoltTypes.Hex, 1, 4)]
        [InlineData("rfswhd", true, false, "C3x3.5", 0.5f, 4, 1, BoltTypes.Hex, 2, 4)]
        [InlineData("rfswhd", false, true, "C5x6.1", 0.5f, 1.25f, 4.5f, BoltTypes.Hex, 2, 4)]
        [InlineData("rfswhd", true, true, "C3x3.5", 0.5f, 4, 0, BoltTypes.Hex, 1, 4)]
        [InlineData("rfswhd", true, true, "C5x6.1", 0.5f, 4.5, 0, BoltTypes.Hex, 1, 4)]
        [InlineData("rrx", false, false, "C3x3.5", 0.5f, 1, 0, BoltTypes.Hex, 1, 4)]
        [InlineData("rrx", false, false, "C5x6.1", 0.5f, 1.25f, 0, BoltTypes.Hex, 1, 4)]
        [InlineData("rrx", true, false, "C3x3.5", 0.5f, 4, 1, BoltTypes.Hex, 2, 4)]
        [InlineData("rrx", false, true, "C5x6.1", 0.5f, 1.25f, 4.5f, BoltTypes.Hex, 2, 4)]
        [InlineData("rrx", true, true, "C3x3.5", 0.5f, 4, 0, BoltTypes.Hex, 1, 4)]
        [InlineData("rrx", true, true, "C5x6.1", 0.5f, 4.5, 0, BoltTypes.Hex, 1, 4)]
        [InlineData("rrxI", false, false, "C3x3.5", 0.5f, 1, 0, BoltTypes.Hex, 1, 4)]
        [InlineData("rrxI", false, false, "C5x6.1", 0.5f, 1.25f, 0, BoltTypes.Hex, 1, 4)]
        [InlineData("rrxI", true, false, "C3x3.5", 0.5f, 4, 1, BoltTypes.Hex, 2, 4)]
        [InlineData("rrxI", false, true, "C5x6.1", 0.5f, 1.25f, 4.5f, BoltTypes.Hex, 2, 4)]
        [InlineData("rrxI", true, true, "C3x3.5", 0.5f, 4, 0, BoltTypes.Hex, 1, 4)]
        [InlineData("rrxI", true, true, "C5x6.1", 0.5f, 4.5, 0, BoltTypes.Hex, 1, 4)]
        [InlineData("rrxID", false, false, "C3x3.5", 0.5f, 1, 0, BoltTypes.Hex, 1, 4)]
        [InlineData("rrxID", false, false, "C5x6.1", 0.5f, 1.25f, 0, BoltTypes.Hex, 1, 4)]
        [InlineData("rrxID", true, false, "C3x3.5", 0.5f, 4, 1, BoltTypes.Hex, 2, 4)]
        [InlineData("rrxID", false, true, "C5x6.1", 0.5f, 1.25f, 4.5f, BoltTypes.Hex, 2, 4)]
        [InlineData("rrxID", true, true, "C3x3.5", 0.5f, 4, 0, BoltTypes.Hex, 1, 4)]
        [InlineData("rrxID", true, true, "C5x6.1", 0.5f, 4.5, 0, BoltTypes.Hex, 1, 4)]
        [InlineData("rrxd", false, false, "C3x3.5", 0.5f, 1, 0, BoltTypes.Hex, 1, 4)]
        [InlineData("rrxd", false, false, "C5x6.1", 0.5f, 1.25f, 0, BoltTypes.Hex, 1, 4)]
        [InlineData("rrxd", true, false, "C3x3.5", 0.5f, 4, 1, BoltTypes.Hex, 2, 4)]
        [InlineData("rrxd", false, true, "C5x6.1", 0.5f, 1.25f, 4.5f, BoltTypes.Hex, 2, 4)]
        [InlineData("rrxd", true, true, "C3x3.5", 0.5f, 4, 0, BoltTypes.Hex, 1, 4)]
        [InlineData("rrxd", true, true, "C5x6.1", 0.5f, 4.5, 0, BoltTypes.Hex, 1, 4)]
        public void CanGetFrameTieHardware(string type, bool frontDoubler, bool rearDoubler, string columnSize, 
                                            float expectedBoltSize, float expectedBoltLength1, float expectedBoltLength2,
                                            string expectedBoltType, int expectedKitQty, int expectedTotalQty)
        {
            var part = _fixture.CreateRootPart<FrameTie>();
            part.Type.SetValue(type);
            part.FrontDoubleratConnection.SetValue(frontDoubler);
            part.RearDoubleratConnection.SetValue(rearDoubler);
            part.ColumnChannelSize.SetValue(columnSize);

            var hardwareKits = part.Hardwares;

            Assert.Equal(expectedBoltSize, hardwareKits.First().BoltSize.Value);
            Assert.Equal(expectedBoltLength1, hardwareKits.First().BoltLength.Value);
            if (hardwareKits.Count() > 1)
            {
                Assert.Equal(expectedBoltLength2, hardwareKits.Last().BoltLength.Value);
            }

            Assert.Equal(expectedBoltType, hardwareKits.First().BoltType.Value);
            Assert.Equal(expectedKitQty, hardwareKits.Count());
            Assert.Equal(expectedTotalQty, hardwareKits.Sum(d => d.Qty.Value));
        }

        [Theory]
        [InlineData(0.4375f, 0.375f, 1.75f, BoltTypes.TapHex)]
        [InlineData(0.5625f, 0.5, 1.75f, BoltTypes.TapHex)]
        public void CanGetPalletStopHardware(float holeSize, float expectedBoltSize, float expectedBoltLength,
                                        string expectedBoltType)
        {
            var part = _fixture.CreateRootPart<ShelfPalletStop>();
            part.BeamHoleSize.SetValue(holeSize);

            Assert.Equal(expectedBoltSize, part.Hardware.BoltSize.Value);
            Assert.Equal(expectedBoltLength, part.Hardware.BoltLength.Value);
            Assert.Equal(expectedBoltType, part.Hardware.BoltType.Value);
        }

        [Theory]
        [InlineData("S", false, 0.5f, 1, BoltTypes.Hex, 2, true)]
        [InlineData("H", false, 0.5f, 1, BoltTypes.Hex, 2, true)]
        [InlineData("BH", true, 0.5f, 1, BoltTypes.Hex, 2, true)]
        public void CanGetFrameUprightHardware(string uprightType, bool hasHardware, float expectedBoltSize, 
                                        float expectedBoltLength, string expectedBoltType, int expectedQty, bool hasWashers)
        {
            var part = _fixture.CreateRootPart<FrameUpright>();
            part.UprightType.SetValue(uprightType);

            Assert.Equal(hasHardware, part.Hardware.IsActive.Value);

            if (part.Hardware.IsActive.Value)
            {
                Assert.Equal(expectedBoltSize, part.Hardware.BoltSize.Value);
                Assert.Equal(expectedBoltLength, part.Hardware.BoltLength.Value);
                Assert.Equal(expectedBoltType, part.Hardware.BoltType.Value);
                Assert.Equal(expectedQty, part.Hardware.Qty.Value);
                Assert.Equal(hasWashers, part.Hardware.HasWashers.Value);
            }
        }

        [Theory]
        [InlineData(2, "C3x3.5", "8", 4, false, 1, false, 2, 8)]
        [InlineData(2, "C5x6.1", "8", 4.5f, false, 1.25, true, 2, 8)]
        [InlineData(2, "C3x3.5", "12", 4, false, 1, false, 2, 12)]
        [InlineData(2, "C5x6.1", "12", 4.5f, false, 1.25, true, 2, 12)]
        [InlineData(3, "C3x3.5", "8", 4, false, 1, false, 2, 16)]
        [InlineData(3, "C5x6.1", "8", 4.5f, false, 1.25, true, 2, 16)]
        [InlineData(3, "C3x3.5", "12", 4, false, 1, false, 2, 24)]
        [InlineData(3, "C5x6.1", "12", 4.5f, false, 1.25, true, 2, 24)]
        public void CanGetSelectiveBeamsHardware(int levelQty, string columnSize, string loadBeamBracketType,
                                                float expectedBoltLength1, bool expectedHasWashers1, float expectedBoltLength2, 
                                                bool expectedHasWashers2, int expectedKitCount, int expectedTotalQty)
        {
            var part = _fixture.CreateRootPart<StorageSystem>();
            var bay = part.Bays.AddChild<Bay>();
            bay.LevelQty.SetValue(levelQty);
            bay.FirstActiveLevel.Value.FrontBeamBracketType.SetValue(loadBeamBracketType);
            bay.FirstActiveLevel.Value.RearBeamBracketType.SetValue(loadBeamBracketType);

            var frameType = part.FrameTypes.AddChild<SelectiveFrameType>();
            frameType.Height.SetValue(220);
            frameType.UprightColumnSize.SetValue(columnSize);
            frameType.Frame.Frame.HasFrontDoubler.SetValue(true);
            part.FrameLines.AddChild<SelectiveFrameLine>();

            var testRow = part.RowTypes.AddChild<Row>();
            testRow.BayQty.SetValue(1);
            part.AddChild<RowProxy>();

            var rowBay = testRow.Bays.First();
            var hardwareKits = rowBay.Hardwares;

            Assert.Equal(0.5f, hardwareKits.First().BoltSize.Value);
            Assert.Equal(BoltTypes.Hex, hardwareKits.First().BoltType.Value);
            Assert.Equal(expectedBoltLength1, hardwareKits.First().BoltLength.Value);
            Assert.Equal(expectedHasWashers1, hardwareKits.First().HasWashers.Value);
            Assert.Equal(expectedBoltLength2, hardwareKits.Last().BoltLength.Value);
            Assert.Equal(expectedHasWashers2, hardwareKits.Last().HasWashers.Value);
            Assert.Equal(expectedKitCount, hardwareKits.Count());
            Assert.Equal(expectedTotalQty, hardwareKits.Sum(d => d.Qty.Value));
        }

        [Theory]
        [InlineData(1, 24)]
        [InlineData(2, 48)]
        [InlineData(3, 72)]
        public void CanGetTotalHardwareSelective(int bayQty, int expectedTotalHardware)
        {
            var part = _fixture.CreateRootPart<StorageSystem>();
            var bay = part.Bays.AddChild<Bay>();
            bay.LevelQty.SetValue(3);

            var frameType = part.FrameTypes.AddChild<SelectiveFrameType>();
            frameType.Height.SetValue(220);
            frameType.Frame.Frame.HasFrontDoubler.SetValue(true);
            part.FrameLines.AddChild<SelectiveFrameLine>();

            var testRow = part.RowTypes.AddChild<Row>();
            testRow.RowQty.SetValue(1);
            testRow.BayQty.SetValue(bayQty);
            part.AddChild<RowProxy>();

            var hardwareKits = part.HardwareKits.Value;
            //var results = hardwareKits.Select(k => (k.BoltType.Value, k.BoltSize.Value.ToString(), k.Length.Value, k.Qty.Value.ToString())).ToList();
            var uniqueHardwareKits = part.UniqueHardwareKits.Value;
            //var res = uniqueHardwareKits.Select(k => (k.BoltType.Value, k.BoltSize.Value.ToString(), k.Length.Value, k.Qty.Value.ToString())).ToList();
            Assert.Equal(2, uniqueHardwareKits.Count);

            //test total qty
            Assert.Equal(expectedTotalHardware, part.UniqueHardwareKits.Value.Sum(d => d.Qty));
        }

        [Theory]
        [InlineData(1, 24, 26)]
        [InlineData(2, 48, 51)]
        [InlineData(3, 72, 75)]
        public void CanGetTotalBOMHardwareSelective(int bayQty, int expectedTotalHardware, int expectedTotalBOMHardware)
        {
            var part = _fixture.CreateRootPart<StorageSystem>();
            var bay = part.Bays.AddChild<Bay>();
            bay.LevelQty.SetValue(3);

            var frameType = part.FrameTypes.AddChild<SelectiveFrameType>();
            frameType.Height.SetValue(220);
            frameType.Frame.Frame.HasFrontDoubler.SetValue(true);
            part.FrameLines.AddChild<SelectiveFrameLine>();

            var testRow = part.RowTypes.AddChild<Row>();
            testRow.RowQty.SetValue(1);
            testRow.BayQty.SetValue(bayQty);
            part.AddChild<RowProxy>();

            var hardwareKits = part.HardwareKits.Value;
            //var results = hardwareKits.Select(k => (k.BoltType.Value, k.BoltSize.Value.ToString(), k.Length.Value, k.Qty.Value.ToString())).ToList();
            var uniqueHardwareKits = part.UniqueHardwareKits.Value;
            //var res = uniqueHardwareKits.Select(k => (k.BoltType.Value, k.BoltSize.Value.ToString(), k.Length.Value, k.Qty.Value.ToString())).ToList();
            Assert.Equal(2, uniqueHardwareKits.Count);

            //test total qty
            Assert.Equal(expectedTotalHardware, part.HardwareContainer.StorageSystemHardwareKits.Sum(d => d.Qty.Value));
            Assert.Equal(expectedTotalBOMHardware, part.HardwareContainer.StorageSystemHardwareKits.Sum(d => d.BOMQty.Value));
        }

        [Theory]
        [InlineData(1, 72)]
        [InlineData(2, 144)]
        [InlineData(3, 216)]
        public void CanGetTotalHardwareBackToBack(int bayQty, int expectedTotalHardware)
        {
            var part = _fixture.CreateRootPart<StorageSystem>();
            var bay = part.Bays.AddChild<Bay>();

            var frameType = part.FrameTypes.AddChild<SelectiveFrameType>();
            frameType.Height.SetValue(220);
            frameType.Frame.Frame.HasFrontDoubler.SetValue(true);
            var frameline = part.FrameLines.AddChild<SelectiveFrameLine>();
            frameline.FrontFrameTypeName.SetValue(frameType.Path);
            frameline.IsBackToBack.SetValue(true);

            var testRow = part.RowTypes.AddChild<Row>();
            testRow.BayQty.SetValue(bayQty);
            part.AddChild<RowProxy>();

            var uniqueHardwareKits = part.UniqueHardwareKits.Value;
            Assert.Equal(2, uniqueHardwareKits.Count);

            //test total qty
            Assert.Equal(expectedTotalHardware, part.UniqueHardwareKits.Value.Sum(d => d.Qty));
        }

        [Theory]
        [InlineData(1, 2, 2)]
        [InlineData(2, 2, 2)]
        [InlineData(3, 2, 2)]
        [InlineData(1, 1, 2)]
        [InlineData(2, 1, 2)]
        [InlineData(3, 1, 2)]
        [InlineData(1, 1, 4)]
        [InlineData(2, 1, 4)]
        [InlineData(3, 1, 4)]
        public void CanGetTotalAnchorsSelective(int bayQty, int frontAnchorsPerFootplate, 
                                                int rearAnchorsPerFootplate)
        {
            var part = _fixture.CreateRootPart<StorageSystem>();
            var bay = part.Bays.AddChild<Bay>();
            bay.LevelQty.SetValue(3);

            var frameType = part.FrameTypes.AddChild<SelectiveFrameType>();
            frameType.Height.SetValue(220);
            frameType.Frame.FrontAnchorQty.SetValue(frontAnchorsPerFootplate);
            frameType.Frame.RearAnchorQty.SetValue(rearAnchorsPerFootplate);
            part.FrameLines.AddChild<SelectiveFrameLine>();

            var testRow = part.RowTypes.AddChild<Row>();
            testRow.RowQty.SetValue(1);
            testRow.BayQty.SetValue(bayQty);
            part.AddChild<RowProxy>();

            var expectedTotalAnchors = (frontAnchorsPerFootplate + rearAnchorsPerFootplate) * (bayQty + 1);

            Assert.Equal(expectedTotalAnchors, part.UniqueAnchorDefinitions.Value.Sum(d => d.Qty));
            Assert.Equal(expectedTotalAnchors, part.HardwareContainer.StorageSystemAnchors.Sum(c => c.Qty.Value));
        }

        [Fact]
        public void CanGetTotalAnchorsSelectiveMultiRow()
        {
            var part = _fixture.CreateRootPart<StorageSystem>();
            var bay = part.Bays.AddChild<Bay>();
            bay.LevelQty.SetValue(3);

            var frameType = part.FrameTypes.AddChild<SelectiveFrameType>();
            frameType.Height.SetValue(220);
            frameType.Frame.FrontAnchorQty.SetValue(2);
            frameType.Frame.RearAnchorQty.SetValue(2);
            part.FrameLines.AddChild<SelectiveFrameLine>();

            Assert.Equal(0, part.UniqueAnchorDefinitions.Value.Sum(d => d.Qty));
            Assert.Equal(0, part.HardwareContainer.StorageSystemAnchors.Sum(c => c.Qty.Value));

            var testRow = part.RowTypes.AddChild<Row>();
            testRow.RowQty.SetValue(1);
            testRow.BayQty.SetValue(1);
            part.AddChild<RowProxy>();

            Assert.Equal(8, part.UniqueAnchorDefinitions.Value.Sum(d => d.Qty));
            Assert.Equal(8, part.HardwareContainer.StorageSystemAnchors.Sum(c => c.Qty.Value));

            var testRow2 = part.RowTypes.AddChild<Row>();
            testRow2.RowQty.SetValue(1);
            testRow2.BayQty.SetValue(2);
            var testRow2Prox = part.AddChild<RowProxy>();
            testRow2Prox.RowTypeName.SetValue(testRow2.RowTypeName.Value);

            Assert.Equal(20, part.UniqueAnchorDefinitions.Value.Sum(d => d.Qty));
            Assert.Equal(20, part.HardwareContainer.StorageSystemAnchors.Sum(c => c.Qty.Value));

            var testRow3 = part.RowTypes.AddChild<Row>();
            testRow3.RowQty.SetValue(1);
            testRow3.BayQty.SetValue(4);
            var testRow3Prox = part.AddChild<RowProxy>();
            testRow3Prox.RowTypeName.SetValue(testRow3.RowTypeName.Value);

            Assert.Equal(40, part.UniqueAnchorDefinitions.Value.Sum(d => d.Qty));
            Assert.Equal(40, part.HardwareContainer.StorageSystemAnchors.Sum(c => c.Qty.Value));
        }

        #region Washer Tests

        [Theory]
        [InlineData("rs", false, false, "C3x3.5", 0.5f, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 0)]
        [InlineData("rs", false, false, "C5x6.1", 0.5f, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 2)]
        [InlineData("rs", true, false, "C3x3.5", 0.5f, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 0)]
        [InlineData("rs", false, true, "C5x6.1", 0.5f, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 1)]
        [InlineData("rs", true, true, "C3x3.5", 0.5f, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 0)]
        [InlineData("rs", true, true, "C5x6.1", 0.5f, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 0)]
        [InlineData("rfsw", false, false, "C3x3.5", 0.5f, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 0)]
        [InlineData("rfsw", false, false, "C5x6.1", 0.5f, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 4)]
        [InlineData("rfsw", true, false, "C3x3.5", 0.5f, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 0)]
        [InlineData("rfsw", false, true, "C5x6.1", 0.5f, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 2)]
        [InlineData("rfsw", true, true, "C3x3.5", 0.5f, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 0)]
        [InlineData("rfsw", true, true, "C5x6.1", 0.5f, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 0)]
        [InlineData("rfswhd", false, false, "C3x3.5", 0.5f, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 0)]
        [InlineData("rfswhd", false, false, "C5x6.1", 0.5f, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 4)]
        [InlineData("rfswhd", true, false, "C3x3.5", 0.5f, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 0)]
        [InlineData("rfswhd", false, true, "C5x6.1", 0.5f, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 2)]
        [InlineData("rfswhd", true, true, "C3x3.5", 0.5f, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 0)]
        [InlineData("rfswhd", true, true, "C5x6.1", 0.5f, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 0)]
        [InlineData("rrx", false, false, "C3x3.5", 0.5f, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 0)]
        [InlineData("rrx", false, false, "C5x6.1", 0.5f, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 4)]
        [InlineData("rrx", true, false, "C3x3.5", 0.5f, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 0)]
        [InlineData("rrx", false, true, "C5x6.1", 0.5f, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 2)]
        [InlineData("rrx", true, true, "C3x3.5", 0.5f, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 0)]
        [InlineData("rrx", true, true, "C5x6.1", 0.5f, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 0)]
        [InlineData("rrxI", false, false, "C3x3.5", 0.5f, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 0)]
        [InlineData("rrxI", false, false, "C5x6.1", 0.5f, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 4)]
        [InlineData("rrxI", true, false, "C3x3.5", 0.5f, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 0)]
        [InlineData("rrxI", false, true, "C5x6.1", 0.5f, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 2)]
        [InlineData("rrxI", true, true, "C3x3.5", 0.5f, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 0)]
        [InlineData("rrxI", true, true, "C5x6.1", 0.5f, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 0)]
        [InlineData("rrxID", false, false, "C3x3.5", 0.5f, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 0)]
        [InlineData("rrxID", false, false, "C5x6.1", 0.5f, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 4)]
        [InlineData("rrxID", true, false, "C3x3.5", 0.5f, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 0)]
        [InlineData("rrxID", false, true, "C5x6.1", 0.5f, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 2)]
        [InlineData("rrxID", true, true, "C3x3.5", 0.5f, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 0)]
        [InlineData("rrxID", true, true, "C5x6.1", 0.5f, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 0)]
        [InlineData("rrxd", false, false, "C3x3.5", 0.5f, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 0)]
        [InlineData("rrxd", false, false, "C5x6.1", 0.5f, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 4)]
        [InlineData("rrxd", true, false, "C3x3.5", 0.5f, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 0)]
        [InlineData("rrxd", false, true, "C5x6.1", 0.5f, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 2)]
        [InlineData("rrxd", true, true, "C3x3.5", 0.5f, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 0)]
        [InlineData("rrxd", true, true, "C5x6.1", 0.5f, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 0)]
        public void CanGetFrameTieWashers(string type, bool frontDoubler, bool rearDoubler, string columnSize,
                                        float expectedBoltSize, string expectedWasherType, 
                                        string expectedWasherMat, int expectedTotalWasherQty)
        {
            var part = _fixture.CreateRootPart<FrameTie>();
            part.Type.SetValue(type);
            part.FrontDoubleratConnection.SetValue(frontDoubler);
            part.RearDoubleratConnection.SetValue(rearDoubler);
            part.ColumnChannelSize.SetValue(columnSize);

            var hardwareKits = part.Hardwares;

            Assert.Equal(expectedBoltSize, hardwareKits.First().BoltSize.Value);
            
            Assert.Equal(expectedWasherType, hardwareKits.First().WasherType.Value);
            Assert.Equal(expectedTotalWasherQty, hardwareKits.Sum(d => d.WasherQty.Value));

            if (expectedTotalWasherQty > 0)
            {
                Assert.Equal(expectedWasherMat, hardwareKits.Where(k => k.HasWashers.Value).First().WasherMaterial.Value);
            }
        }

        [Theory]
        [InlineData("S", false, 0.5f, WasherTypes.Flat, HardwareMaterial.Grade2, 0)]
        [InlineData("H", false, 0.5f, WasherTypes.Flat, HardwareMaterial.Grade2, 0)]
        [InlineData("BH", true, 0.5f, WasherTypes.Flat, HardwareMaterial.Grade2, 2)]
        public void CanGetFrameUprightWashers(string uprightType, bool hasHardware, float expectedBoltSize,
                                        string expectedWasherType, string expectedWasherMat, int expectedWasherQty)
        {
            var part = _fixture.CreateRootPart<FrameUpright>();
            part.UprightType.SetValue(uprightType);

            Assert.Equal(hasHardware, part.Hardware.IsActive.Value);

            if (part.Hardware.IsActive.Value)
            {
                Assert.Equal(expectedBoltSize, part.Hardware.BoltSize.Value);
                Assert.Equal(expectedWasherType, part.Hardware.WasherType.Value);
                Assert.Equal(expectedWasherMat, part.Hardware.WasherMaterial.Value);
                Assert.Equal(expectedWasherQty, part.Hardware.WasherQty.Value);
            }
        }

        [Theory]
        [InlineData(2, "C3x3.5", "8", false, false, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 0)]
        [InlineData(2, "C5x6.1", "8", false, true, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 4)]
        [InlineData(2, "C3x3.5", "12", false, false, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 0)]
        [InlineData(2, "C5x6.1", "12", false, true, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 6)]
        [InlineData(3, "C3x3.5", "8", false, false, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 0)]
        [InlineData(3, "C5x6.1", "8", false, true, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 12)]
        [InlineData(3, "C3x3.5", "12", false, false, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 0)]
        [InlineData(3, "C5x6.1", "12", false, true, WasherTypes.Clipped, HardwareMaterial.ZincPlated, 18)]
        public void CanGetSelectiveBeamsWashers(int levelQty, string columnSize, string loadBeamBracketType,
                                                bool expectedHasWashers1, bool expectedHasWashers2, 
                                                string expectedWasherType, string expectedWasherMat, int expectedTotalWasherQty)
        {
            var part = _fixture.CreateRootPart<StorageSystem>();
            var bay = part.Bays.AddChild<Bay>();
            bay.LevelQty.SetValue(levelQty);
            bay.FirstActiveLevel.Value.FrontBeamBracketType.SetValue(loadBeamBracketType);
            bay.FirstActiveLevel.Value.RearBeamBracketType.SetValue(loadBeamBracketType);

            var frameType = part.FrameTypes.AddChild<SelectiveFrameType>();
            frameType.Height.SetValue(220);
            frameType.UprightColumnSize.SetValue(columnSize);
            frameType.Frame.Frame.HasFrontDoubler.SetValue(true);
            part.FrameLines.AddChild<SelectiveFrameLine>();

            var testRow = part.AddChild<Row>();
            testRow.BayQty.SetValue(1);
            part.AddChild<RowProxy>();

            var rowBay = testRow.Bays.First();
            var hardwareKits = rowBay.Hardwares;

            Assert.Equal(0.5f, hardwareKits.First().BoltSize.Value);
            Assert.Equal(BoltTypes.Hex, hardwareKits.First().BoltType.Value);
            Assert.Equal(expectedHasWashers1, hardwareKits.First().HasWashers.Value);
            Assert.Equal(expectedHasWashers2, hardwareKits.Last().HasWashers.Value);
            Assert.Equal(expectedWasherType, hardwareKits.First().WasherType.Value);
            Assert.Equal(expectedWasherMat, hardwareKits.First().WasherMaterial.Value);
            Assert.Equal(expectedTotalWasherQty, hardwareKits.Sum(d => d.WasherQty.Value));
        }

        [Theory]
        [InlineData(1, 16)]
        [InlineData(2, 30)]
        [InlineData(3, 44)]
        public void CanGetTotalWashersSelective(int bayQty, int expectedTotalWashers)
        {
            var part = _fixture.CreateRootPart<StorageSystem>();
            var bay = part.Bays.AddChild<Bay>();
            bay.LevelQty.SetValue(3);

            var frameType = part.FrameTypes.AddChild<SelectiveFrameType>();
            frameType.Height.SetValue(220);
            frameType.UprightType.SetValue("BH");
            frameType.UprightColumnSize.SetValue("C5x6.1");
            frameType.Frame.Frame.HasFrontDoubler.SetValue(true);
            part.FrameLines.AddChild<SelectiveFrameLine>();

            var testRow = part.RowTypes.AddChild<Row>();
            testRow.RowQty.SetValue(1);
            testRow.BayQty.SetValue(bayQty);
            part.AddChild<RowProxy>();

            var washers = part.WasherDefinitions.Value;
            //var results = washers.Select(k => (k.BoltSize.Value.ToString(), k.WasherType.Value, k.Qty.Value.ToString())).ToList();
            var uniqueWashers = part.UniqueWashers.Value;
            //var res = uniqueWashers.Select(k => (k.BoltSize.Value.ToString(), k.WasherType.Value, k.Qty.Value.ToString())).ToList();
            Assert.Equal(2, uniqueWashers.Count);

            //test total qty
            Assert.Equal(expectedTotalWashers, part.UniqueWashers.Value.Sum(d => d.Qty));
        }

        [Theory]
        [InlineData(1, 16, 18)]
        [InlineData(2, 30, 32)]
        [InlineData(3, 44, 47)]
        public void CanGetTotalWashersBOMQtySelective(int bayQty, int expectedTotalWashers, int expectedTotalBOMWashers)
        {
            var part = _fixture.CreateRootPart<StorageSystem>();
            var bay = part.Bays.AddChild<Bay>();
            bay.LevelQty.SetValue(3);

            var frameType = part.FrameTypes.AddChild<SelectiveFrameType>();
            frameType.Height.SetValue(220);
            frameType.UprightType.SetValue("BH");
            frameType.UprightColumnSize.SetValue("C5x6.1");
            frameType.Frame.Frame.HasFrontDoubler.SetValue(true);
            part.FrameLines.AddChild<SelectiveFrameLine>();

            var testRow = part.RowTypes.AddChild<Row>();
            testRow.RowQty.SetValue(1);
            testRow.BayQty.SetValue(bayQty);
            part.AddChild<RowProxy>();

            var washers = part.WasherDefinitions.Value;
            //var results = washers.Select(k => (k.BoltSize.Value.ToString(), k.WasherType.Value, k.Qty.Value.ToString())).ToList();
            var uniqueWashers = part.UniqueWashers.Value;
            //var res = uniqueWashers.Select(k => (k.BoltSize.Value.ToString(), k.WasherType.Value, k.Qty.Value.ToString())).ToList();
            Assert.Equal(2, uniqueWashers.Count);

            //test total qty
            Assert.Equal(expectedTotalWashers, part.HardwareContainer.StorageSystemWashers.Sum(p => p.Qty.Value));
            Assert.Equal(expectedTotalBOMWashers, part.HardwareContainer.StorageSystemWashers.Sum(p => p.BOMQty.Value));
        }

        #endregion

        [Fact]
        public void Testing()
        {
            var temp = Helper.fractionString(0.5, feetToo: false);

            Assert.True(true);
        }

        #endregion

    }
}
