﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ruler.Cpq;
using Ruler.Rules;
using Xunit;
using StorageConfigurator.Plugin;
using StorageConfigurator.TestData;

namespace StorageConfigurator.Rules.Tests
{
    [Collection(nameof(RulesCollection))]
    public class RulerCPQTests
    {

        #region Private Fields

        private readonly StorageConfiguratorFixture _fixture;

        #endregion

        #region Constructors

        public RulerCPQTests(StorageConfiguratorFixture fixture)
        {
            _fixture = fixture;
        }

        #endregion

        #region Tests

        [Fact]
        public void CanAddCustomLaborItems()
        {
            var part = _fixture.CreateRootPart<CustomPart>();
            var laborItemOne = part.AddChild(new Ruler.Rules.ChildOptions<CustomLaborItem>());
            laborItemOne.Hours.SetValue(50);
            var laborItemTwo = part.AddChild(new Ruler.Rules.ChildOptions<CustomLaborItem>());
            laborItemTwo.Hours.SetValue(40);

            Assert.True(part.Bom.ItemLaborHours.Value == 90);
        }

        #region Labor Hour Tests

        [Fact]
        public void VerifyPartLaborHours()
        {
            var part = CreateCustomPartWithLabor();

            Assert.Equal(0.75, part.Bom.ItemLaborHours.Value, 4);

            var bomSummarizer = new StorageBomSummarizer();
            var bomSummary = bomSummarizer.GetBomSummary(part.Bom);

            Assert.Equal(0.75, bomSummary.ItemLaborHours, 4);
            Assert.Equal(0.75, bomSummary.TotalLaborHours, 4);
        }

        [Fact]
        public void VerifyPartLaborHoursWithManualQty()
        {
            var part = CreateCustomPartWithLabor();
            part.Bom.ItemQty.SetValue(2);

            Assert.Equal(1.5, part.Bom.TotalItemLaborHours.Value, 4);

            var bomSummarizer = new StorageBomSummarizer();
            var bomSummary = bomSummarizer.GetBomSummary(part.Bom, part.Bom.ItemQty.Value);

            Assert.Equal(1.5, bomSummary.TotalLaborHours, 4);
        }

        [Fact]
        public void VerifySmallAssemblyLaborHours()
        {
            var part = CreateSmallCustomAssembly();

            Assert.Equal(0, part.Bom.ItemLaborHours.Value, 4);

            var bomSummarizer = new StorageBomSummarizer();
            var bomSummary = bomSummarizer.GetBomSummary(part.Bom);

            Assert.Equal(0, bomSummary.ItemLaborHours, 4);
            Assert.Equal(1.5, bomSummary.TotalLaborHours, 4);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        public void CanGetRowAndStandardBomSummary(int rowQty)
        {
            var part = _fixture.CreateRootPart<StorageSystem>();
            var bay = part.Bays.AddChild<Bay>();

            var frameType = part.FrameTypes.AddChild<SelectiveFrameType>();
            var frameline = part.FrameLines.AddChild<SelectiveFrameLine>();
            //frameline.FrontFrameTypeName.SetValue(frameType.Path);

            var testRow = part.RowTypes.AddChild<Row>();
            for (int i = 0; i < rowQty; i++)
            {
                var testRowProx = part.AddChild<RowProxy>();
                testRowProx.RowTypeName.SetValue(testRow.Path);
            }

            var bomSummarizer = new StorageBomSummarizer();
            var bomSummary = bomSummarizer.GetBomSummary(part.Bom);
            var rowSummary = bomSummarizer.GetBomSummary(part.RowBom);

            var bomSummaryTotal = bomSummary.TotalCost;
            var rowSummaryTotal = rowSummary.TotalCost;
            
            if (rowQty == 1)
            {
                Assert.Equal(bomSummaryTotal, rowSummaryTotal, 2);
            }
            else
            {
                Assert.True(bomSummaryTotal < rowSummaryTotal);
            }
        }

        [Fact]
        public void VerifySmallAssemblyLaborHoursWithManualQty()
        {
            var part = CreateSmallCustomAssembly();
            part.Bom.ItemQty.SetValue(2);

            Assert.Equal(0, part.Bom.TotalItemLaborHours.Value, 4);

            var bomSummarizer = new StorageBomSummarizer();
            var bomSummary = bomSummarizer.GetBomSummary(part.Bom, part.Bom.ItemQty.Value);

            Assert.Equal(3, bomSummary.TotalLaborHours, 4);
        }

        #endregion

        #region Weight Tests

        [Fact]
        public void VerifySmallAssemblyWeight()
        {
            var part = CreateSmallCustomAssembly();

            Assert.Equal(5.5, part.Bom.UnitWeight.Value, 4);
            Assert.Equal(5.5, part.Bom.TotalWeight.Value, 4);

            var bomSummarizer = new StorageBomSummarizer();
            var bomSummary = bomSummarizer.GetBomSummary(part.Bom);

            Assert.Equal(5.5, bomSummary.TotalWeight, 4);
        }

        [Fact]
        public void VerifySmallAssemblyWeightWManualQty()
        {
            var part = CreateSmallCustomAssembly();
            var subComp3 = part.AddChild(new ChildOptions<CustomPart>());
            subComp3 = CreateCustomPartWithLabor(part: subComp3, manualQty: 2);

            Assert.Equal(9.5, part.Bom.UnitWeight.Value, 4);
            Assert.Equal(9.5, part.Bom.TotalWeight.Value, 4);

            var bomSummarizer = new StorageBomSummarizer();
            var bomSummary = bomSummarizer.GetBomSummary(part.Bom);

            Assert.Equal(9.5, bomSummary.TotalWeight, 4);
        }

        #endregion

        #region Cost Tests

        [Fact]
        public void VerifyPartCosts()
        {
            var part = CreateCustomPartWithLabor();

            Assert.Equal(5, part.Bom.ItemCostNoLabor.Value, 4);
            Assert.Equal(75, part.Bom.ItemLaborCost.Value, 4);
            Assert.Equal(80, part.Bom.ItemCost.Value, 4);

            Assert.Equal(5, part.Bom.UnitCostNoLabor.Value, 4);
            Assert.Equal(75, part.Bom.UnitLaborCost.Value, 4);
            Assert.Equal(80, part.Bom.UnitCost.Value, 4);

            Assert.Equal(80, part.Bom.TotalCost.Value, 4);

            var bomSummarizer = new StorageBomSummarizer();
            var bomSummary = bomSummarizer.GetBomSummary(part.Bom);

            Assert.Equal(80, bomSummary.ItemCost, 4);
            Assert.Equal(5, bomSummary.ItemCostNoLabor, 4);
            Assert.Equal(5, bomSummary.TotalCostNoLabor, 4);
            Assert.Equal(75, bomSummary.TotalLaborCost, 4);
            Assert.Equal(80, bomSummary.TotalCost, 4);
        }

        [Fact]
        public void VerifyPartCostsWithManualQty()
        {
            var part = CreateCustomPartWithLabor();
            part.Bom.ItemQty.SetValue(2);

            Assert.Equal(5, part.Bom.ItemCostNoLabor.Value, 4);
            Assert.Equal(75, part.Bom.ItemLaborCost.Value, 4);
            Assert.Equal(80, part.Bom.ItemCost.Value, 4);

            Assert.Equal(5, part.Bom.UnitCostNoLabor.Value, 4);
            Assert.Equal(75, part.Bom.UnitLaborCost.Value, 4);
            Assert.Equal(80, part.Bom.UnitCost.Value, 4);

            Assert.Equal(160, part.Bom.TotalCost.Value, 4);

            var bomSummarizer = new StorageBomSummarizer();
            var bomSummary = bomSummarizer.GetBomSummary(part.Bom, part.Bom.ItemQty.Value);

            Assert.Equal(80, bomSummary.ItemCost, 4);
            Assert.Equal(5, bomSummary.ItemCostNoLabor, 4);
            Assert.Equal(10, bomSummary.TotalCostNoLabor, 4);
            Assert.Equal(150, bomSummary.TotalLaborCost, 4);
            Assert.Equal(160, bomSummary.TotalCost, 4);
        }

        [Fact]
        public void VerifySmallAssemblyUnitCosts()
        {
            var part = CreateSmallCustomAssembly();

            Assert.Equal(0, part.Bom.ItemCostNoLabor.Value, 4);
            Assert.Equal(0, part.Bom.ItemLaborCost.Value, 4);
            Assert.Equal(0, part.Bom.ItemCost.Value, 4);

            Assert.Equal(12, part.Bom.UnitCostNoLabor.Value, 4);
            Assert.Equal(150, part.Bom.UnitLaborCost.Value, 4);
            Assert.Equal(162, part.Bom.UnitCost.Value, 4);

            Assert.Equal(162, part.Bom.TotalCost.Value, 4);

            var bomSummarizer = new StorageBomSummarizer();
            var bomSummary = bomSummarizer.GetBomSummary(part.Bom);

            Assert.Equal(0, bomSummary.ItemCost, 4);
            Assert.Equal(0, bomSummary.ItemCostNoLabor, 4);
            Assert.Equal(12, bomSummary.TotalCostNoLabor, 4);
            Assert.Equal(150, bomSummary.TotalLaborCost, 4);
            Assert.Equal(162, bomSummary.TotalCost, 4);
        }

        [Fact]
        public void VerifySmallAssemblyCostsWManualQtyChild()
        {
            var part = CreateSmallCustomAssembly();
            var subComp3 = part.AddChild(new ChildOptions<CustomPart>());
            subComp3 = CreateCustomPartWithLabor(part: subComp3, manualQty: 2);

            Assert.Equal(0, part.Bom.ItemCostNoLabor.Value, 4);
            Assert.Equal(0, part.Bom.ItemLaborCost.Value, 4);
            Assert.Equal(0, part.Bom.ItemCost.Value, 4);

            Assert.Equal(22, part.Bom.UnitCostNoLabor.Value, 4);
            Assert.Equal(300, part.Bom.UnitLaborCost.Value, 4);
            Assert.Equal(322, part.Bom.UnitCost.Value, 4);

            Assert.Equal(322, part.Bom.TotalCost.Value, 4);

            var bomSummarizer = new StorageBomSummarizer();
            var bomSummary = bomSummarizer.GetBomSummary(part.Bom);

            Assert.Equal(0, bomSummary.ItemCost, 4);
            Assert.Equal(0, bomSummary.ItemCostNoLabor, 4);
            Assert.Equal(22, bomSummary.TotalCostNoLabor, 4);
            Assert.Equal(300, bomSummary.TotalLaborCost, 4);
            Assert.Equal(322, bomSummary.TotalCost, 4);
        }

        [Fact]
        public void VerifySmallAssemblyUnitCostsWDuplicateParts()
        {
            var part = CreateSmallCustomAssemblyWDuplicateParts();
            var nonWeldLaborItem = part.AddChild(new ChildOptions<CustomLaborItem>());
            nonWeldLaborItem = SetCustomLaborItemInfo(nonWeldLaborItem, 0.5);

            Assert.Equal(0, part.Bom.ItemCostNoLabor.Value, 4);
            Assert.Equal(50, part.Bom.ItemLaborCost.Value, 4);
            Assert.Equal(50, part.Bom.ItemCost.Value, 4);

            Assert.Equal(12, part.Bom.UnitCostNoLabor.Value, 4);
            Assert.Equal(200, part.Bom.UnitLaborCost.Value, 4);
            Assert.Equal(212, part.Bom.UnitCost.Value, 4);

            Assert.Equal(212, part.Bom.TotalCost.Value, 4);

            var bomSummarizer = new StorageBomSummarizer();
            var bomSummary = bomSummarizer.GetBomSummary(part.Bom);

            Assert.Equal(50, bomSummary.ItemCost, 4);
            Assert.Equal(0, bomSummary.ItemCostNoLabor, 4);
            Assert.Equal(12, bomSummary.TotalCostNoLabor, 4);
            Assert.Equal(200, bomSummary.TotalLaborCost, 4);
            Assert.Equal(212, bomSummary.TotalCost, 4);
        }

        #endregion

        #region Price and Margin Tests

        [Theory]
        [InlineData(1, 20, 0, 6.25, 93.75, 100)]
        [InlineData(2, 20, 0, 6.25, 93.75, 100)]
        [InlineData(1, 0, 20, 5, 75, 80)]
        [InlineData(2, 0, 20, 5, 75, 80)]
        public void VerifyPartPricesWSettingMargins(int manualQty, double itemMarginPercent, double margin,
                                                double expItemPriceNoLabor, double expItemLaborPrice, double expItemPrice)
        {
            var part = CreateCustomPartWithLabor(itemMarginPercent: itemMarginPercent, margin: margin);
            part.Bom.ItemQty.SetValue(manualQty);

            Assert.Equal(expItemPriceNoLabor, part.Bom.ItemPriceNoLabor.Value, 4);
            Assert.Equal(expItemLaborPrice, part.Bom.ItemLaborPrice.Value, 4);
            Assert.Equal(expItemPrice, part.Bom.ItemPrice.Value, 4);

            Assert.Equal(100, part.Bom.UnitPrice.Value, 4);

            Assert.Equal((100 * manualQty), part.Bom.TotalPrice.Value, 4);

            var bomSummarizer = new StorageBomSummarizer();
            var bomSummary = bomSummarizer.GetBomSummary(part.Bom, part.Bom.ItemQty.Value);

            Assert.Equal((100 * manualQty), bomSummary.TotalPrice, 4);
        }

        [Theory]
        [InlineData(0, 0, 10.989, 182)]
        [InlineData(20, 0, 10.989, 182)]
        [InlineData(0, 20, 20, 202.5)]
        public void VerifySmallAssemblyUnitPrices(double itemMarginPercent, double margin, 
                                                  double expMargin, double expUnitPrice)
        {
            var part = CreateSmallCustomAssembly(itemMarginPercent: itemMarginPercent, margin: margin);

            Assert.Equal(0, part.Bom.ItemPriceNoLabor.Value, 4);
            Assert.Equal(0, part.Bom.ItemLaborPrice.Value, 4);
            Assert.Equal(0, part.Bom.ItemPrice.Value, 4);

            Assert.Equal(expMargin, part.Bom.Margin.Value, 4);
            Assert.Equal(expUnitPrice, part.Bom.UnitPrice.Value, 2);

            Assert.Equal(expUnitPrice, part.Bom.TotalPrice.Value, 2);

            var bomSummarizer = new StorageBomSummarizer();
            var bomSummary = bomSummarizer.GetBomSummary(part.Bom);

            Assert.Equal(expUnitPrice, bomSummary.TotalPrice, 2);
        }

        [Theory]
        [InlineData(20, 50, 100, 242, 484)]
        [InlineData(50, 20, 160, 242, 302.50)]
        [InlineData(20, 20, 100, 242, 302.50)]
        public void VerifySmallAssemblyUnitPricesWChildandRootMargins(double childMargin, double rootMargin,
                                                              double expChildUnitPrice, double expUnitCost, 
                                                              double expUnitPrice)
        {
            var part = CreateSmallCustomAssembly(margin: rootMargin);
            var subComp3 = part.AddChild(new ChildOptions<CustomPart>());
            subComp3 = CreateCustomPartWithLabor("PT-098", part: subComp3, margin: childMargin);

            Assert.Equal(expChildUnitPrice, subComp3.Bom.UnitPrice.Value);

            Assert.Equal(expUnitCost, part.Bom.UnitCost.Value, 2);
            Assert.Equal(expUnitPrice, part.Bom.UnitPrice.Value, 2);
            Assert.Equal(expUnitPrice, part.Bom.TotalPrice.Value, 2);

            var bomSummarizer = new StorageBomSummarizer();
            var bomSummary = bomSummarizer.GetBomSummary(part.Bom);

            Assert.Equal(expUnitPrice, bomSummary.TotalPrice, 2);
        }

        [Theory]
        [InlineData(1, 0, 0, 7.6336, 242, 262)]
        [InlineData(2, 0, 0, 5.848, 322, 342)]
        [InlineData(1, 20, 0, 14.1844, 242, 282)]
        [InlineData(2, 20, 0, 15.7068, 322, 382)]
        [InlineData(1, 0, 20, 20, 242, 302.50)]
        [InlineData(2, 0, 20, 20, 322, 402.50)]
        public void VerifySmallAssemblyPricesWManualQtyChild(int manualQty, double itemMarginPercent, double margin, 
                                                    double expMargin, double expUnitCost, double expUnitPrice)
        {
            var part = CreateSmallCustomAssembly(margin: margin);
            var subComp3 = part.AddChild(new ChildOptions<CustomPart>());
            subComp3 = CreateCustomPartWithLabor("PT-098", part: subComp3, 
                                                itemMarginPercent: itemMarginPercent, manualQty: manualQty);

            Assert.Equal(0, part.Bom.ItemPriceNoLabor.Value, 4);
            Assert.Equal(0, part.Bom.ItemLaborPrice.Value, 4);
            Assert.Equal(0, part.Bom.ItemPrice.Value, 4);

            Assert.Equal(expUnitCost, part.Bom.UnitCost.Value, 2);
            Assert.Equal(expMargin, part.Bom.Margin.Value, 4);
            Assert.Equal(expUnitPrice, part.Bom.UnitPrice.Value, 2);

            Assert.Equal(expUnitPrice, part.Bom.TotalPrice.Value, 2);

            var bomSummarizer = new StorageBomSummarizer();
            var bomSummary = bomSummarizer.GetBomSummary(part.Bom);

            Assert.Equal(expUnitPrice, bomSummary.TotalPrice, 2);
        }


        [Theory]
        [InlineData(0, 0, 0, 212, 50, 212)]
        [InlineData(20, 0, 5.5679, 212, 62.5, 224.5)]
        [InlineData(0, 20, 20, 212, 50, 265)]
        [InlineData(20, 20, 20, 212, 62.5, 265)]
        public void VerifySmallAssemblyUnitPricesWDuplicateParts(double itemMarginPercent, double margin,
                                                    double expMargin, double expUnitCost,
                                                    double expItemPrice, double expUnitPrice)
        {
            var part = CreateSmallCustomAssemblyWDuplicateParts();
            part.Bom.ItemMarginPercent.SetValue(itemMarginPercent);
            if (margin > 0) part.Bom.Margin.SetValue(margin);
            var nonWeldLaborItem = part.AddChild(new ChildOptions<CustomLaborItem>());
            nonWeldLaborItem = SetCustomLaborItemInfo(nonWeldLaborItem, 0.5);

            Assert.Equal(0, part.Bom.ItemPriceNoLabor.Value, 4);
            Assert.Equal(expItemPrice, part.Bom.ItemLaborPrice.Value, 4);
            Assert.Equal(expItemPrice, part.Bom.ItemPrice.Value, 4);

            Assert.Equal(expUnitCost, part.Bom.UnitCost.Value, 2);
            Assert.Equal(expMargin, part.Bom.Margin.Value, 4);
            Assert.Equal(expUnitPrice, part.Bom.UnitPrice.Value, 2);

            Assert.Equal(expUnitPrice, part.Bom.TotalPrice.Value, 2);

            var bomSummarizer = new StorageBomSummarizer();
            var bomSummary = bomSummarizer.GetBomSummary(part.Bom);

            Assert.Equal(expUnitPrice, bomSummary.TotalPrice, 2);
        }

        [Fact]
        public void CanCalculateMargin()
        {
            var part = _fixture.CreateRootPart<GenericBomPart>();
            part.Bom.IsPurchased.SetValue(true);
            part.Bom.ItemMarginPercent.SetValue(25);
            part.Bom.ItemPurchasedCost.SetValue(100);

            Assert.Equal(133.33, part.Bom.TotalPrice.Value, 2);
        }

        [Theory]
        [InlineData(100, 0, 242, 282, 14.1844)]
        [InlineData(100, 300, 242, 300, 19.3333)]
        [InlineData(0, 300, 242, 300, 19.3333)]
        public void VerifySmallAssemblySettingManualPrice(double childUnitPrice, double unitPriceOverride, 
                                                          double expUnitCost, double expUnitPrice, 
                                                          double expMargin)
        {
            var assy = CreateSmallCustomAssembly();
            var subComp3 = assy.AddChild(new ChildOptions<CustomPart>());
            subComp3 = CreateCustomPartWithLabor("PT-098", part: subComp3);
            if (childUnitPrice > 0)
            {
                subComp3.Bom.LockedPrice.SetValue(false);
                subComp3.Bom.UnitPrice.SetValue(childUnitPrice);
            }

            if (unitPriceOverride > 0)
            {
                assy.Bom.LockedPrice.SetValue(false);
                assy.Bom.UnitPrice.SetValue(unitPriceOverride);
            }

            Assert.Equal(expMargin, assy.Bom.Margin.Value, 4);
            Assert.Equal(expUnitCost, assy.Bom.UnitCost.Value, 2);
            Assert.Equal(expUnitPrice, assy.Bom.UnitPrice.Value, 2);

            Assert.Equal(expUnitPrice, assy.Bom.TotalPrice.Value, 2);

            var bomSummarizer = new StorageBomSummarizer();
            var bomSummary = bomSummarizer.GetBomSummary(assy.Bom);

            Assert.Equal(expUnitPrice, bomSummary.TotalPrice, 2);
        }


        #endregion

        #endregion

        #region Methods

        public CustomPart CreateSmallCustomAssemblyWDuplicateParts(string partNumber = "A-135", CustomPart part = null)
        {
            if (part == null) part = _fixture.CreateRootPart<CustomPart>();
            part.PartNumber.SetValue(partNumber);
            var subComp = part.AddChild(new ChildOptions<CustomPart>());
            subComp = CreateCustomPartWithLabor("PT-123", subComp);
            var subComp2 = part.AddChild(new ChildOptions<CustomPart>());
            subComp2 = CreateCustomPartWithLabor("PT-123", subComp2);
            var subBuyoutComp = part.AddChild(new ChildOptions<BuyoutPart>());
            subBuyoutComp = CreateBuyoutPart(part: subBuyoutComp);

            return part;
        }

        public CustomPart CreateSmallCustomAssembly(string partNumber = "A-123", CustomPart part = null,
                                                    int manualQty = 1, double itemMarginPercent = 0, double margin = 0)
        {
            if (part == null) part = _fixture.CreateRootPart<CustomPart>();
            part.PartNumber.SetValue(partNumber);
            part.Bom.ItemQty.SetValue(manualQty);
            if (itemMarginPercent > 0) part.Bom.ItemMarginPercent.SetValue(itemMarginPercent);
            if (margin > 0) part.Bom.Margin.SetValue(margin);
            var subComp = part.AddChild(new ChildOptions<CustomPart>()); 
            subComp = CreateCustomPartWithLabor(part: subComp, itemMarginPercent: 20);
            var subComp2 = part.AddChild(new ChildOptions<CustomPart>()); 
            subComp2 = CreateCustomPartWithLabor("PT-123", subComp2);
            var subBuyoutComp = part.AddChild(new ChildOptions<BuyoutPart>()); 
            subBuyoutComp = CreateBuyoutPart(part: subBuyoutComp);

            return part;
        }

        public BuyoutPart CreateBuyoutPart(string partNumber = "PP-555", BuyoutPart part = null)
        {
            if (part == null) part = _fixture.CreateRootPart<BuyoutPart>();
            part.PartNumber.SetValue(partNumber);
            part.Bom.IsPurchased.SetValue(true);
            part.Bom.ItemPurchasedCost.SetValue(2);
            part.Bom.ItemPurchasedWeight.SetValue(1.5);

            return part;
        }

        public CustomPart CreateCustomPartWithLabor(string partNumber = "PT-999", CustomPart part = null, 
                                                    int manualQty = 1, double itemMarginPercent = 0, double margin = 0)
        {
            if (part == null) part = _fixture.CreateRootPart<CustomPart>();
            part.PartNumber.SetValue(partNumber);
            part.Bom.ItemQty.SetValue(manualQty);
            part.Bom.ItemMaterialWeight.SetValue(2);
            part.Bom.ItemMaterialCost.SetValue(5);
            if (itemMarginPercent > 0) part.Bom.ItemMarginPercent.SetValue(itemMarginPercent);
            if (margin > 0) part.Bom.Margin.SetValue(margin);

            var weldLaborItem = part.AddChild(new ChildOptions<CustomLaborItem>());
            weldLaborItem = SetCustomLaborItemInfo(weldLaborItem, 0.5, "Weld");
            var nonWeldLaborItem = part.AddChild(new ChildOptions<CustomLaborItem>());
            nonWeldLaborItem = SetCustomLaborItemInfo(nonWeldLaborItem, 0.25);

            return part;
        }

        public CustomLaborItem SetCustomLaborItemInfo(CustomLaborItem inLabor, double hours, string category = "Non-Weld", double costPerHour = 100)
        {
            inLabor.Hours.SetValue(hours);
            inLabor.Category.SetValue(category);
            inLabor.CostPerHour.SetValue(costPerHour);

            return inLabor;
        }

        #endregion

    }
}
