﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using StorageConfigurator.TestData;

namespace StorageConfigurator.Rules.Tests
{
    [Collection(nameof(RulesCollection))]
    public class CsiIntegrationTests
    {
        #region Private Fields

        private readonly StorageConfiguratorFixture _fixture;

        #endregion

        #region Constructors

        public CsiIntegrationTests(StorageConfiguratorFixture fixture)
        {
            _fixture = fixture;
        }

        #endregion

        #region Tests

        [Fact]
        public void CanGetMaterialCostFromCsi()
        {
            var part = _fixture.CreateRootPart<XBCrossbar>();
            part.AngleType.SetValue("1-1/2 x 1-1/2 x 1/8");
            part.Bom.PlantCode.SetValue("UT_SLC");

            Assert.Equal("S.A.Z1616", part.XBCrossbarAngle.MaterialPartNumber.Value);

            var matUnitCost = part.XBCrossbarAngle.Bom.MaterialUnitCost.Value;

            Assert.Equal(0.3853325, matUnitCost, 7); //0.0366056 per inch, 0.3571274 per lb

        }

        [Fact]
        public void CanGetBundleQtyFromCsi()
        {
            var part = _fixture.CreateRootPart<WeldedCrossbar>();
            part.AngleType.SetValue("1-1/2 x 1-1/2 x 1/8");
            part.Bom.PlantCode.SetValue("UT_SLC");

            Assert.Equal("S.A.Z1616", part.Angle.MaterialPartNumber.Value);
            Assert.Equal(182, part.Angle.BundleQty.Value, 2);

        }

        #endregion
    }
}
