﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Ruler.Rules;
using Ruler.Rules.Graphics;
using StorageConfigurator.TestData;
using Xunit;

namespace StorageConfigurator.Rules.Tests
{
    [Collection(nameof(RulesCollection))]
    public class PartNumberTests
    {
        #region Private Fields

        private readonly StorageConfiguratorFixture _fixture;

        #endregion

        #region Constructors

        public PartNumberTests(StorageConfiguratorFixture fixture)
        {
            _fixture = fixture;
        }

        #endregion

        #region Tests

        #region Upright Tests

        [Theory()]
        [InlineData("C3x3.5", "S", 360f, true, false, "USA8F-")]
        [InlineData("C3x3.5", "S", 360f, true, true, "USA8FR-")]
        [InlineData("C3x3.5", "S", 360f, false, false, "USA8-")]
        [InlineData("C3x3.5", "S", 360f, false, true, "USA8R-")]
        [InlineData("C4x5.4", "S", 360f, false, true, "USD8R-")]
        [InlineData("C4x5.4", "S", 180f, false, false, "USD4-")]
        [InlineData("C4x5.4", "S", 224f, false, false, "USD5-")]
        [InlineData("C3x4.1", "H", 360f, true, false, "USB8F-")]
        [InlineData("C3x4.1", "B", 360f, true, false, "UBLB8F-")]
        [InlineData("C3x4.1", "BH", 360f, true, false, "USB8F-")]
        [InlineData("C3x4.1", "BTH", 360f, true, false, "UHB8F-")]
        [InlineData("C3x4.1", "D", 360f, true, false, "UDB8F-")]
        [InlineData("C3x4.1", "T", 360f, true, false, "UTB8F-")]
        [InlineData("C3x4.1", "LS", 360f, true, false, "ULSB8F-")]
        [InlineData("C3x4.1", "L", 360f, true, false, "ULB8F-")]
        [InlineData("C4x4.5", "S", 360f, true, false, "USC8F-")]
        [InlineData("C5x6.1", "S", 360f, true, false, "USEL8F-")]
        [InlineData("C5x6.7", "S", 360f, true, false, "USE8F-")]
        public void CanGetUprightPartNumber(string columnSize, string uprightType, float height, bool frontDoubler, bool rearDoubler, string expPartNumber)
        {
            var part = _fixture.CreateRootPart<FrameUpright>();
            part.UprightColumnSize.SetValue(columnSize);
            part.UprightType.SetValue(uprightType);
            part.Height.SetValue(height);
            part.HasFrontDoubler.SetValue(frontDoubler);
            part.HasRearDoubler.SetValue(rearDoubler);

            Assert.Equal(expPartNumber, part.PartNumberPrefix.Value);
        }

        [Fact()]
        public void CanIncrementUprightPartNumber()
        {
            var part1 = _fixture.CreateRootPart<FrameUpright>();
            var part2 = _fixture.CreateRootPart<FrameUpright>();
            part2.Height.SetValue(part1.Height.Value + 4);

            var pn1 = part1.PartNumber.Value;
            var pn2 = part2.PartNumber.Value;

            Assert.True(GetIncrement(pn1) != GetIncrement(pn2));

        }

        #endregion
                
        [Fact]
        public void CanUpdatePartNumberAfterFactoryRevision()
      {
            var modelContext = _fixture.CreateModelContext();
            var part = modelContext.CreateRootPart<FrameUpright>();

            var pnContext = _fixture.CreateStorageConfiguratorContext();
            var initialPartNumber = part.PartNumber.Value;

            var factoryRevs = pnContext.FactoryRevisions;
            factoryRevs.Add(new StorageConfigurator.Data.FactoryRevision() { FactoryName = "FrameUpright", PrimaryRevision = "B" });
            pnContext.FactoryRevisions = factoryRevs;
            pnContext.SaveChanges();
            Thread.Sleep(TimeSpan.FromSeconds(25));
            var newPart = modelContext.CreateRootPart<FrameUpright>();
            var newPn = newPart.PartNumber.Value;

            Assert.NotEqual(initialPartNumber, newPn);

            //Remove record created by test
            factoryRevs.Remove(factoryRevs.FirstOrDefault(rev => rev.FactoryName == "FrameUpright"));
            pnContext.FactoryRevisions = factoryRevs;
            pnContext.SaveChanges();
        }

        #region Frame Tie Tests

        [Fact]
        public void CanGetRSFrameTiePartNumber()
        {
            var part = _fixture.CreateRootPart<RSFrameTie>();
            part.Type.SetValue("rs");

            Assert.Equal("RS-", part.PartNumberPrefix.Value);
        }

        [Fact]
        public void CanIncrementRSFrameTiePartNumber()
        {
            var part1 = _fixture.CreateRootPart<RSFrameTie>();
            part1.Type.SetValue("rs");
            var part2 = _fixture.CreateRootPart<RSFrameTie>();
            part2.Type.SetValue("rs");
            part2.Length.SetValue(120);

            var pn1 = part1.PartNumber.Value;
            var pn2 = part2.PartNumber.Value;

            Assert.True(GetIncrement(pn1) != GetIncrement(pn2));
        }

        #region Welded

        [Fact]
        public void CanGetWeldedFrameTiePartNumber()
        {
            var part = _fixture.CreateRootPart<WeldedFrameTie>();
            part.Type.SetValue("rfsw");

            Assert.Equal("RFSW-", part.PartNumberPrefix.Value);
        }

        [Fact]
        public void CanIncrementWeldedFrameTiePartNumber()
        {
            var part1 = _fixture.CreateRootPart<WeldedFrameTie>();
            part1.Type.SetValue("rfsw");
            var part2 = _fixture.CreateRootPart<WeldedFrameTie>();
            part2.Type.SetValue("rfsw");
            part2.OverallLength.SetValue(120);

            var pn1 = part1.PartNumber.Value;
            var pn2 = part2.PartNumber.Value;

            Assert.True(GetIncrement(pn1) != GetIncrement(pn2));
        }

        #region Frame Tie Angle

        [Theory]
        [InlineData("1-1/2 x 1-1/2 x 1/8", "P40015-")]
        [InlineData("2 x 2 x 1/8", "P40017-")]
        [InlineData("2 x 2 x 3/16", "P40019-")]
        public void CanGetWeldedFrameTieAnglePartNumberPrefix(string angleType, string expPrefix)
        {
            var part = _fixture.CreateRootPart<FrameTieAngle>();
            part.AngleType.SetValue(angleType);

            Assert.Equal(expPrefix, part.PartNumberPrefix.Value);
        }

        [Fact]
        public void CanIncrementWeldedFrameTieAnglePartNumber()
        {
            var part1 = _fixture.CreateRootPart<FrameTieAngle>();
            part1.AngleType.SetValue("1-1/2 x 1-1/2 x 1/8");
            var part2 = _fixture.CreateRootPart<FrameTieAngle>();
            part2.AngleType.SetValue("1-1/2 x 1-1/2 x 1/8");
            part2.Length.SetValue(36);

            var pn1 = part1.PartNumber.Value;
            var pn2 = part2.PartNumber.Value;

            Assert.True(GetIncrement(pn1) != GetIncrement(pn2));
        }

        #endregion

        #region Frame Tie Four Hole End Plate

        [Theory]
        [InlineData("rfsw", "P40801-")]
        [InlineData("rfswhd", "P40802HD-")]
        [InlineData("rrx", "P40802-")]
        [InlineData("rrxI", "P40802-")]
        public void CanGetFrameTieFourHoleEndPlatePartNumberPrefix(string frameTieType, string expPrefix)
        {
            var part = _fixture.CreateRootPart<FrameTieFourHoleEndPlate>();
            part.FrameTieType.SetValue(frameTieType);

            Assert.Equal(expPrefix, part.PartNumberPrefix.Value);
        }

        [Fact]
        public void CanIncrementFrameTieFourHoleEndPlatePartNumber()
        {
            var part1 = _fixture.CreateRootPart<FrameTieFourHoleEndPlate>();
            part1.FrameTieType.SetValue("rfsw");
            var part2 = _fixture.CreateRootPart<FrameTieFourHoleEndPlate>();
            part2.FrameTieType.SetValue("rfsw");
            part2.Height.SetValue(12);

            var pn1 = part1.PartNumber.Value;
            var pn2 = part2.PartNumber.Value;

            Assert.True(GetIncrement(pn1) != GetIncrement(pn2));
        }

        #endregion

        #region Frame Tie Channel

        [Theory]
        [InlineData("rfswhd", "C3x3.5", "PC31100HD-")]
        [InlineData("rfswhd", "C3x4.1", "PC31101HD-")]
        [InlineData("rfswhd", "C4x4.5", "PC31102HD-")]
        [InlineData("rfswhd", "C4x5.4", "PC31103HD-")]
        [InlineData("rfswhd", "C5x6.1", "PC31104LHD-")]
        [InlineData("rfswhd", "C5x6.7", "PC31104HD-")]
        [InlineData("rrx", "C3x3.5", "PC31100-")]
        [InlineData("rrx", "C3x4.1", "PC31101-")]
        [InlineData("rrx", "C4x4.5", "PC31102-")]
        [InlineData("rrx", "C4x5.4", "PC31103-")]
        [InlineData("rrx", "C5x6.1", "PC31104L-")]
        [InlineData("rrx", "C5x6.7", "PC31104-")]
        [InlineData("rrxI", "C3x3.5", "PC31100-")]
        [InlineData("rrxI", "C3x4.1", "PC31101-")]
        [InlineData("rrxI", "C4x4.5", "PC31102-")]
        [InlineData("rrxI", "C4x5.4", "PC31103-")]
        [InlineData("rrxI", "C5x6.1", "PC31104L-")]
        [InlineData("rrxI", "C5x6.7", "PC31104-")]
        [InlineData("rrxID", "C3x3.5", "PC31100-")]
        [InlineData("rrxID", "C3x4.1", "PC31101-")]
        [InlineData("rrxID", "C4x4.5", "PC31102-")]
        [InlineData("rrxID", "C4x5.4", "PC31103-")]
        [InlineData("rrxID", "C5x6.1", "PC31104L-")]
        [InlineData("rrxID", "C5x6.7", "PC31104-")]
        [InlineData("rrxd", "C3x3.5", "PC31100-")]
        [InlineData("rrxd", "C3x4.1", "PC31101-")]
        [InlineData("rrxd", "C4x4.5", "PC31102-")]
        [InlineData("rrxd", "C4x5.4", "PC31103-")]
        [InlineData("rrxd", "C5x6.1", "PC31104L-")]
        [InlineData("rrxd", "C5x6.7", "PC31104-")]
        public void CanGetFrameTieChannelPartNumberPrefix(string frameTieType, string channelSize, string expPrefix)
        {
            var part = _fixture.CreateRootPart<FrameTieChannel>();
            part.FrameTieType.SetValue(frameTieType);
            part.ChannelSize.SetValue(channelSize);

            Assert.Equal(expPrefix, part.PartNumberPrefix.Value);
        }

        [Fact]
        public void CanIncrementFrameTieChannelPartNumber()
        {
            var part1 = _fixture.CreateRootPart<FrameTieChannel>();
            part1.FrameTieType.SetValue("rfsw");
            part1.ChannelSize.SetValue("C3x3.5");
            var part2 = _fixture.CreateRootPart<FrameTieChannel>();
            part2.FrameTieType.SetValue("rfsw");
            part2.Length.SetValue(21);

            var pn1 = part1.PartNumber.Value;
            var pn2 = part2.PartNumber.Value;

            Assert.True(GetIncrement(pn1) != GetIncrement(pn2));
        }

        #endregion

        #region Doubled Rub Rail

        [Fact]
        public void CanGetDoubledRubRailPlatePartNumberPrefix()
        {
            var part = _fixture.CreateRootPart<DoubledRubRailPlate>();

            Assert.Equal("P40834-", part.PartNumberPrefix.Value);
        }

        [Fact]
        public void CanIncrementDoubledRubRailPlatePartNumber()
        {
            var part1 = _fixture.CreateRootPart<DoubledRubRailPlate>();
            var part2 = _fixture.CreateRootPart<DoubledRubRailPlate>();
            part2.Width.SetValue(3);
 
            var pn1 = part1.PartNumber.Value;
            var pn2 = part2.PartNumber.Value;

            Assert.True(GetIncrement(pn1) != GetIncrement(pn2));
        }

        #endregion 

        #endregion

        #region Welded HD

        [Fact]
        public void CanGetWeldedHDFrameTiePartNumber()
        {
            var part = _fixture.CreateRootPart<WeldedHDFrameTie>();
            part.Type.SetValue("rfswhd");

            Assert.Equal("RFSWHD-", part.PartNumberPrefix.Value);
        }

        [Fact]
        public void CanIncrementWeldedHDFrameTiePartNumber()
        {
            var part1 = _fixture.CreateRootPart<WeldedHDFrameTie>();
            part1.Type.SetValue("rfswhd");
            var part2 = _fixture.CreateRootPart<WeldedHDFrameTie>();
            part2.Type.SetValue("rfswhd");
            part2.ChannelLength.SetValue(120);

            var pn1 = part1.PartNumber.Value;
            var pn2 = part2.PartNumber.Value;

            Assert.True(GetIncrement(pn1) != GetIncrement(pn2));
        }

        #endregion

        #region Welded Rub Rail

        [Theory]
        [InlineData("rrx", "C3x3.5", "RRA-")]
        [InlineData("rrx", "C3x4.1", "RRB-")]
        [InlineData("rrx", "C4x4.5", "RRC-")]
        [InlineData("rrx", "C4x5.4", "RRD-")]
        [InlineData("rrx", "C5x6.1", "RREL-")]
        [InlineData("rrx", "C5x6.7", "RRE-")]
        [InlineData("rrxI", "C3x3.5", "RRAI-")]
        [InlineData("rrxI", "C3x4.1", "RRBI-")]
        [InlineData("rrxI", "C4x4.5", "RRCI-")]
        [InlineData("rrxI", "C4x5.4", "RRDI-")]
        [InlineData("rrxI", "C5x6.1", "RRELI-")]
        [InlineData("rrxI", "C5x6.7", "RREI-")]
        [InlineData("rrxID", "C3x3.5", "RRAID-")]
        [InlineData("rrxID", "C3x4.1", "RRBID-")]
        [InlineData("rrxID", "C4x4.5", "RRCID-")]
        [InlineData("rrxID", "C4x5.4", "RRDID-")]
        [InlineData("rrxID", "C5x6.1", "RRELID-")]
        [InlineData("rrxID", "C5x6.7", "RREID-")]
        public void CanGetWeldedRubRailPartNumberPrefix(string type, string channelSize, string expPartNumber)
        {
            var part = _fixture.CreateRootPart<WeldedRubRail>();
            part.Type.SetValue(type);
            part.ChannelSize.SetValue(channelSize);

            Assert.Equal(expPartNumber, part.PartNumberPrefix.Value);
        }

        [Theory]
        [InlineData("rrx")]
        [InlineData("rrxI")]
        [InlineData("rrxID")]
        public void CanIncrementWeldedRubRailPartNumber(string type)
        {
            var part1 = _fixture.CreateRootPart<WeldedRubRail>();
            part1.Type.SetValue(type);
            var part2 = _fixture.CreateRootPart<WeldedRubRail>();
            part2.Type.SetValue(type);
            part2.OverallLength.SetValue(120);

            var pn1 = part1.PartNumber.Value;
            var pn2 = part2.PartNumber.Value;

            Assert.True(GetIncrement(pn1) != GetIncrement(pn2));
        }

        #endregion

        #region Double Rub Rail

        [Theory]
        [InlineData("C3x3.5", "RRAD-")]
        [InlineData("C3x4.1", "RRBD-")]
        [InlineData("C4x4.5", "RRCD-")]
        [InlineData("C4x5.4", "RRDD-")]
        [InlineData("C5x6.1", "RRELD-")]
        [InlineData("C5x6.7", "RRED-")]
        public void CanGetDoubledRubRailPartNumberPrefix(string channelSize, string expPartNumber)
        {
            var part = _fixture.CreateRootPart<DoubledRubRail>();
            part.Type.SetValue("rrxd");
            part.ChannelSize.SetValue(channelSize);

            Assert.Equal(expPartNumber, part.PartNumberPrefix.Value);
        }

        [Fact]
        public void CanIncrementDoubledRubRailPartNumber()
        {
            var part1 = _fixture.CreateRootPart<DoubledRubRail>();
            part1.Type.SetValue("rrxd");
            var part2 = _fixture.CreateRootPart<DoubledRubRail>();
            part2.Type.SetValue("rrxd");
            part2.OverallLength.SetValue(120);

            var pn1 = part1.PartNumber.Value;
            var pn2 = part2.PartNumber.Value;

            Assert.True(GetIncrement(pn1) != GetIncrement(pn2));
        }

        #endregion

        #region Welded Rub Rail Angle

        [Fact]
        public void CanGetRubRailAnglePartNumber()
        {
            var part = _fixture.CreateRootPart<RubRailAngle>();

            Assert.Equal("P40010", part.PartNumber.Value);
        }

        #endregion

        #endregion

        #region Cross Bar Tests

        [Theory]
        [InlineData("round", "XB-")]
        [InlineData("square", "XB-")]
        public void CanGetXBCrossbarPartNumberPrefix(string holeType, string expPartNumber)
        {
            var part = _fixture.CreateRootPart<XBCrossbar>();
            part.HoleType.SetValue(holeType);

            Assert.Equal(expPartNumber, part.PartNumberPrefix.Value);
        }

        #region Welded Cross Bar

        [Fact]
        public void CanIncrementXBCrossbarPartNumber()
        {
            var part1 = _fixture.CreateRootPart<XBCrossbar>();
            var part2 = _fixture.CreateRootPart<XBCrossbar>();
            part2.Length.SetValue(120);

            var pn1 = part1.PartNumber.Value;
            var pn2 = part2.PartNumber.Value;

            Assert.True(GetIncrement(pn1) != GetIncrement(pn2));
        }

        [Theory]
        [InlineData("xbw", "std", "std", "XBW-")]
        [InlineData("xbw-dd1", "std", "std", "XBWSS-DD1-")]
        [InlineData("xbw-dd1", "std", "rev", "XBWSR-DD1-")]
        [InlineData("xbw-dd1", "rev", "std", "XBWRS-DD1-")]
        [InlineData("xbw-dd1", "rev", "rev", "XBWRR-DD1-")]
        [InlineData("xbw-dd2", "std", "std", "XBWSS-DD2-")]
        [InlineData("xbw-dd2", "std", "rev", "XBWSR-DD2-")]
        [InlineData("xbw-dd2", "rev", "std", "XBWRS-DD2-")]
        [InlineData("xbw-dd2", "rev", "rev", "XBWRR-DD2-")]
        [InlineData("xbwr1", "std", "std", "XBWR1-")]
        [InlineData("xbwr2", "std", "std", "XBWR2-")]
        public void CanGetWeldedCrossbarPartNumberPrefix(string type, string frontOrientation, string rearOrientation, string expPartNumber)
        {
            var part = _fixture.CreateRootPart<WeldedCrossbar>();
            part.Type.SetValue(type);
            part.FrontBeamOrientation.SetValue(frontOrientation);
            part.RearBeamOrientation.SetValue(rearOrientation);

            Assert.Equal(expPartNumber, part.PartNumberPrefix.Value);            
        }
                
        [Fact]
        public void CanIncrementWeldedCrossbarPartNumber()
        {
            var part1 = _fixture.CreateRootPart<WeldedCrossbar>();
            var part2 = _fixture.CreateRootPart<WeldedCrossbar>();
            part2.OverallLength.SetValue(120);

            var pn1 = part1.PartNumber.Value;
            var pn2 = part2.PartNumber.Value;

            Assert.True(GetIncrement(pn1) != GetIncrement(pn2));
        }

        #endregion

        #region Crossbar Angle

        [Theory]
        [InlineData("xbw", "1-1/2 x 1-1/2 x 1/8", "PB21611-")]
        [InlineData("xbw", "2 x 2 x 1/8", "PB23678-")]
        [InlineData("xbw", "3 x 2 x 3/16", "PB23647-")]
        [InlineData("xbw", "2 x 2 x 3/16", "PB23657-")]
        [InlineData("xbw", "2-1/2 x 2-1/2 x 3/16", "PB23653-")]
        [InlineData("xbw", "3 x 2 x 1/4", "PB23654-")]
        [InlineData("xbwr1", "1-1/2 x 1-1/2 x 1/8", "PB21611-")]
        [InlineData("xbwr1", "2 x 2 x 1/8", "PB23678-")]
        [InlineData("xbwr1", "3 x 2 x 3/16", "PB23647-")]
        [InlineData("xbwr1", "2 x 2 x 3/16", "PB23657-")]
        [InlineData("xbwr1", "2-1/2 x 2-1/2 x 3/16", "PB23653-")]
        [InlineData("xbwr2", "1-1/2 x 1-1/2 x 1/8", "PB21611-")]
        [InlineData("xbwr2", "2 x 2 x 1/8", "PB23678-")]
        [InlineData("xbwr2", "3 x 2 x 3/16", "PB23647-")]
        [InlineData("xbwr2", "2 x 2 x 3/16", "PB23657-")]
        [InlineData("xbwr2", "2-1/2 x 2-1/2 x 3/16", "PB23653-")]
        [InlineData("xbw-dd1", "1-1/2 x 1-1/2 x 1/8", "PC31088-")]
        [InlineData("xbw-dd1", "2 x 2 x 1/8", "PC31088-")]
        [InlineData("xbw-dd1", "3 x 2 x 3/16", "PC31088-")]
        [InlineData("xbw-dd1", "2 x 2 x 3/16", "PC31088-")]
        [InlineData("xbw-dd1", "2-1/2 x 2-1/2 x 3/16", "PC31088-")]
        [InlineData("xbw-dd1", "3 x 2 x 1/4", "PC31088-")]
        [InlineData("xbw-dd2", "1-1/2 x 1-1/2 x 1/8", "PC31089-")]
        [InlineData("xbw-dd2", "2 x 2 x 1/8", "PC31089-")]
        [InlineData("xbw-dd2", "3 x 2 x 3/16", "PC31089-")]
        [InlineData("xbw-dd2", "2 x 2 x 3/16", "PC31089-")]
        [InlineData("xbw-dd2", "2-1/2 x 2-1/2 x 3/16", "PC31089-")]
        [InlineData("xbw-dd2", "3 x 2 x 1/4", "PC31089-")]
        public void CanGetCrossbarAnglePartNumberPrefix(string crossBarType, string angleType, string expPrefix)
        {
            var part = _fixture.CreateRootPart<CrossbarAngle>();
            part.CrossbarType.SetValue(crossBarType);
            part.AngleType.SetValue(angleType);

            Assert.Equal(expPrefix, part.PartNumberPrefix.Value);
        }

        [Fact]
        public void CanIncrementCrossbarAnglePartNumber()
        {
            var part1 = _fixture.CreateRootPart<CrossbarAngle>();
            part1.CrossbarType.SetValue("xbw");
            part1.AngleType.SetValue("1-1/2 x 1-1/2 x 1/8");
            part1.Length.SetValue(24);
            var part2 = _fixture.CreateRootPart<CrossbarAngle>();
            part2.CrossbarType.SetValue("xbw");
            part2.AngleType.SetValue("1-1/2 x 1-1/2 x 1/8");
            part2.Length.SetValue(48);

            var pn1 = part1.PartNumber.Value;
            var pn2 = part2.PartNumber.Value;

            Assert.True(GetIncrement(pn1) != GetIncrement(pn2));
        }

        #endregion

        #region Cross Bar End Angle

        [Fact]
        public void CanGetCrossbarEndAnglePartNumberPrefix()
        {
            var part = _fixture.CreateRootPart<WeldedCrossbar>();

            var angle = part.FrontEndAngle;
            angle.HoleType.SetValue("round");
            angle.HoleSize.SetValue(.5625f);
            Assert.Equal("PC31042-", angle.PartNumberPrefix.Value);
        }

        [Fact]
        public void CanIncrementCrossbarEndAnglePartNumber()
        {
            var part1 = _fixture.CreateRootPart<WeldedCrossbar>();
            var angle1 = part1.FrontEndAngle;
            angle1.HoleType.SetValue("round");
            var part2 = _fixture.CreateRootPart<WeldedCrossbar>();
            var angle2 = part2.FrontEndAngle;
            part2.HoleType.SetValue("square");

            var pn1 = angle1.PartNumber.Value;
            var pn2 = angle2.PartNumber.Value;

            Assert.True(GetIncrement(pn1) != GetIncrement(pn2));
        }

        #endregion

        #region Cross Bar End Plate

        [Fact]
        public void CanGetCrossbarEndPlatePartNumberPrefix()
        {
            var part = _fixture.CreateRootPart<CrossbarEndPlate>();

            Assert.Equal("PC31059", part.PartNumber.Value);
        }

        #endregion

        #region Cross Bar Notch Angle

        [Fact]
        public void CanGetCrossbarNotchAnglePartNumberPrefix()
        {
            var part = _fixture.CreateRootPart<WeldedCrossbar>();

            var angle = part.NotchOneAngle;
            Assert.Equal("PC31127", angle.PartNumber.Value);
        }

        #endregion

        #region Cross Bar Notch Plate

        [Theory]
        [InlineData("xbw-dd1", 1.5f, "PC31173-")]
        [InlineData("xbw-dd1", 2, "PC31173-")]
        [InlineData("xbw-dd1", 2.5f, "PC31172-")]
        [InlineData("xbw-dd2", 1.5f, "PC31173-")]
        [InlineData("xbw-dd2", 2, "PC31173-")]
        [InlineData("xbw-dd2", 2.5f, "PC31172-")]
        public void CanGetCrossbarNotchPlatePartNumber(string crossBarType, float width, string expPartNumber)
        {
            var xBar = _fixture.CreateRootPart<WeldedCrossbar>();
            xBar.Type.SetValue(crossBarType);
            xBar.AngleWidth.SetValue(width);

            var plate = xBar.NotchPlate;

            Assert.Equal(expPartNumber, plate.PartNumberPrefix.Value);
        }

        [Fact]
        public void CanIncrementCrossbarNotchPlatePartNumber()
        {
            var part1 = _fixture.CreateRootPart<WeldedCrossbar>();
            part1.Type.SetValue("xbw-dd1");

            var plate1 = part1.NotchPlate;

            var part2 = _fixture.CreateRootPart<WeldedCrossbar>();
            part2.Type.SetValue("xbw-dd2");
            var plate2 = part2.NotchPlate;

            var pn1 = plate1.PartNumber.Value;
            var pn2 = plate2.PartNumber.Value;

            Assert.True(GetIncrement(pn1) != GetIncrement(pn2));
        }

        #endregion


        #endregion

        #region Upright Column Assembly Tests

        [Theory]
        [InlineData("C3x3.5", false, "POSTA-")]
        [InlineData("C3x4.1", false, "POSTB-")]
        [InlineData("C4x4.5", false, "POSTC-")]
        [InlineData("C4x5.4", false, "POSTD-")]
        [InlineData("C5x6.1", false, "POSTEL-")]
        [InlineData("C5x6.7", false, "POSTE-")]
        [InlineData("C3x3.5", true, "POSTAD-")]
        [InlineData("C3x4.1", true, "POSTBD-")]
        [InlineData("C4x4.5", true, "POSTCD-")]
        [InlineData("C4x5.4", true, "POSTDD-")]
        [InlineData("C5x6.1", true, "POSTELD-")]
        [InlineData("C5x6.7", true, "POSTED-")]
        public void CanGetUprightColumnAssemblyPartNumber(string channelSize, bool hasDoubler, string expPartNumber)
        {
            var part = _fixture.CreateRootPart<UprightColumnAssembly>();
            part.UprightColumnSize.SetValue(channelSize);
            part.HasDoubler.SetValue(hasDoubler);

            Assert.Equal(expPartNumber, part.PartNumberPrefix.Value);
        }

        [Fact]
        public void CanIncrementUprightPostPartNumber()
        {
            var part1 = _fixture.CreateRootPart<UprightColumnAssembly>();
            var part2 = _fixture.CreateRootPart<UprightColumnAssembly>();
            part2.ColumnHeight.SetValue(200);

            var pn1 = part1.PartNumber.Value;
            var pn2 = part2.PartNumber.Value;

            Assert.True(GetIncrement(pn1) != GetIncrement(pn2));
        }

        [Theory]
        [InlineData("C3x3.5", true, false, false, 2, "P40032-F-")]
        [InlineData("C3x4.1", true, false, false, 2, "P40132-F-")]
        [InlineData("C4x4.5", true, false, false, 2, "P40042-F-")]
        [InlineData("C4x5.4", true, false, false, 2, "P40142-F-")]
        [InlineData("C5x6.7", true, false, false, 2, "P40052-F-")]
        [InlineData("C3x3.5", true, true, false, 2, "P40032BL-")]
        [InlineData("C3x4.1", true, true, false, 2, "P40132BL-")]
        [InlineData("C4x4.5", true, true, false, 2, "P40042BL-")]
        [InlineData("C4x5.4", true, true, false, 2, "P40142BL-")]
        [InlineData("C5x6.7", true, true, false, 2, "P40052BL-")]
        [InlineData("C3x3.5", false, false, false, 2, "P40032-R-")]
        [InlineData("C3x4.1", false, false, false, 2, "P40132-R-")]
        [InlineData("C4x4.5", false, false, false, 2, "P40042-R-")]
        [InlineData("C4x5.4", false, false, false, 2, "P40142-R-")]
        [InlineData("C5x6.7", false, false, false, 2, "P40052-R-")]
        [InlineData("C3x3.5", true, false, false, 4, "P40034-F-")]
        [InlineData("C3x4.1", true, false, false, 4, "P40134-F-")]
        [InlineData("C4x4.5", true, false, false, 4, "P40044-F-")]
        [InlineData("C4x5.4", true, false, false, 4, "P40144-F-")]
        [InlineData("C5x6.7", true, false, false, 4, "P40054-F-")]
        [InlineData("C3x3.5", true, true, false, 4, "P40034BL-")]
        [InlineData("C3x4.1", true, true, false, 4, "P40134BL-")]
        [InlineData("C4x4.5", true, true, false, 4, "P40044BL-")]
        [InlineData("C4x5.4", true, true, false, 4, "P40144BL-")]
        [InlineData("C5x6.7", true, true, false, 4, "P40054BL-")]
        [InlineData("C3x3.5", false, false, false, 4, "P40034-R-")]
        [InlineData("C3x4.1", false, false, false, 4, "P40134-R-")]
        [InlineData("C4x4.5", false, false, false, 4, "P40044-R-")]
        [InlineData("C4x5.4", false, false, false, 4, "P40144-R-")]
        [InlineData("C5x6.7", false, false, false, 4, "P40054-R-")]
        [InlineData("C3x3.5", true, false, true, 2, "P40232-F-")]
        [InlineData("C3x4.1", true, false, true, 2, "P40332-F-")]
        [InlineData("C4x4.5", true, false, true, 2, "P40242-F-")]
        [InlineData("C4x5.4", true, false, true, 2, "P40442-F-")]
        [InlineData("C5x6.7", true, false, true, 2, "P40532-F-")]
        [InlineData("C3x3.5", true, true, true, 2, "P40232BL-")]
        [InlineData("C3x4.1", true, true, true, 2, "P40332BL-")]
        [InlineData("C4x4.5", true, true, true, 2, "P40242BL-")]
        [InlineData("C4x5.4", true, true, true, 2, "P40442BL-")]
        [InlineData("C5x6.7", true, true, true, 2, "P40532BL-")]
        [InlineData("C3x3.5", false, false, true, 2, "P40232-R-")]
        [InlineData("C3x4.1", false, false, true, 2, "P40332-R-")]
        [InlineData("C4x4.5", false, false, true, 2, "P40242-R-")]
        [InlineData("C4x5.4", false, false, true, 2, "P40442-R-")]
        [InlineData("C5x6.7", false, false, true, 2, "P40532-R-")]
        [InlineData("C3x3.5", true, false, true, 4, "P40234-F-")]
        [InlineData("C3x4.1", true, false, true, 4, "P40334-F-")]
        [InlineData("C4x4.5", true, false, true, 4, "P40244-F-")]
        [InlineData("C4x5.4", true, false, true, 4, "P40444-F-")]
        [InlineData("C5x6.7", true, false, true, 4, "P40534-F-")]
        [InlineData("C3x3.5", true, true, true, 4, "P40234BL-")]
        [InlineData("C3x4.1", true, true, true, 4, "P40334BL-")]
        [InlineData("C4x4.5", true, true, true, 4, "P40244BL-")]
        [InlineData("C4x5.4", true, true, true, 4, "P40444BL-")]
        [InlineData("C5x6.7", true, true, true, 4, "P40534BL-")]
        [InlineData("C3x3.5", false, false, true, 4, "P40234-R-")]
        [InlineData("C3x4.1", false, false, true, 4, "P40334-R-")]
        [InlineData("C4x4.5", false, false, true, 4, "P40244-R-")]
        [InlineData("C4x5.4", false, false, true, 4, "P40444-R-")]
        [InlineData("C5x6.7", false, false, true, 4, "P40534-R-")]
        public void CanGetUprightColumnPartNumberPrefix(string channelSize, bool isFrontColumn, bool isBentLeg, bool isDoubler, float holeSpacing, string expPartNumber)
        {
            var part = _fixture.CreateRootPart<UprightColumn>();
            part.ChannelSize.SetValue(channelSize);
            part.IsFrontColumn.SetValue(isFrontColumn);
            part.IsBentLegColumn.SetValue(isBentLeg);
            part.IsDoubler.SetValue(isDoubler);
            part.HoleSpacing.SetValue(holeSpacing);

            Assert.Equal(expPartNumber, part.PartNumberPrefix.Value);
        }

        [Fact]
        public void CanIncrementUprightColumnPartNumber()
        {
            var part1 = _fixture.CreateRootPart<UprightColumn>();
            part1.IsFrontColumn.SetValue(true);
            part1.Length.SetValue(100);
            var part2 = _fixture.CreateRootPart<UprightColumn>();
            part2.IsFrontColumn.SetValue(true);
            part2.Length.SetValue(200);

            var pn1 = part1.PartNumber.Value;
            var pn2 = part2.PartNumber.Value;

            Assert.True(GetIncrement(pn1) != GetIncrement(pn2));
        }

        [Fact]
        public void CanGetUprightColumnPartNumberFR()
        {
            var part1 = _fixture.CreateRootPart<UprightColumn>();
            var part2 = _fixture.CreateRootPart<UprightColumn>();
            part1.IsFrontColumn.SetValue(true);
            part2.IsFrontColumn.SetValue(false); 

            var pn1 = part1.PartNumber.Value;
            var pn2 = part2.PartNumber.Value;

            var ph1 = part1.CadHash.Value;
            var ph2 = part2.CadHash.Value;

            Assert.True(ph1 != ph2);
        }

        [Fact]
        public void CanGetUprightColumn2PartNumberPrefix()
        {
            var part = _fixture.CreateRootPart<FrameUpright>();
            part.UprightColumnSize.SetValue("C3x3.5");

            Assert.Equal("P40034-F-", part.FrontUprightColumnAssembly.UprightColumnChannel.PartNumberPrefix.Value);
            Assert.Equal("P40034-R-", part.RearUprightColumnAssembly.UprightColumnChannel.PartNumberPrefix.Value);
            Assert.NotEqual(part.FrontUprightColumnAssembly.PartNumber.Value, part.RearUprightColumnAssembly.PartNumber.Value);
        }

        #endregion

        #region Load Beam Tests

        [Theory]
        [InlineData("std", "C3x3.5", 1, "BA1S-")]
        [InlineData("std", "C3x4.1", 1, "BB1S-")]
        [InlineData("std", "C4x4.5", 1, "BC1S-")]
        [InlineData("std", "C4x5.4", 1, "BD1S-")]
        [InlineData("std", "C5x6.1", 1, "BEL1S-")]
        [InlineData("std", "C5x6.7", 1, "BE1S-")]
        [InlineData("rev", "C3x3.5", 1, "RBA1S-")]
        [InlineData("rev", "C3x4.1", 1, "RBB1S-")]
        [InlineData("rev", "C4x4.5", 1, "RBC1S-")]
        [InlineData("rev", "C4x5.4", 1, "RBD1S-")]
        [InlineData("rev", "C5x6.1", 1, "RBEL1S-")]
        [InlineData("rev", "C5x6.7", 1, "RBE1S-")]
        [InlineData("std", "C3x3.5", 2, "BA2S-")]
        [InlineData("std", "C3x4.1", 2, "BB2S-")]
        [InlineData("std", "C4x4.5", 2, "BC2S-")]
        [InlineData("std", "C4x5.4", 2, "BD2S-")]
        [InlineData("std", "C5x6.1", 2, "BEL2S-")]
        [InlineData("std", "C5x6.7", 2, "BE2S-")]
        [InlineData("rev", "C3x3.5", 2, "RBA2S-")]
        [InlineData("rev", "C3x4.1", 2, "RBB2S-")]
        [InlineData("rev", "C4x4.5", 2, "RBC2S-")]
        [InlineData("rev", "C4x5.4", 2, "RBD2S-")]
        [InlineData("rev", "C5x6.1", 2, "RBEL2S-")]
        [InlineData("rev", "C5x6.7", 2, "RBE2S-")]
        [InlineData("std", "C3x3.5", 3, "BA3S-")]
        [InlineData("std", "C3x4.1", 3, "BB3S-")]
        [InlineData("std", "C4x4.5", 3, "BC3S-")]
        [InlineData("std", "C4x5.4", 3, "BD3S-")]
        [InlineData("std", "C5x6.1", 3, "BEL3S-")]
        [InlineData("std", "C5x6.7", 3, "BE3S-")]
        [InlineData("rev", "C3x3.5", 3, "RBA3S-")]
        [InlineData("rev", "C3x4.1", 3, "RBB3S-")]
        [InlineData("rev", "C4x4.5", 3, "RBC3S-")]
        [InlineData("rev", "C4x5.4", 3, "RBD3S-")]
        [InlineData("rev", "C5x6.1", 3, "RBEL3S-")]
        [InlineData("rev", "C5x6.7", 3, "RBE3S-")]
        [InlineData("std", "C3x3.5", 4, "BA4S-")]
        [InlineData("std", "C3x4.1", 4, "BB4S-")]
        [InlineData("std", "C4x4.5", 4, "BC4S-")]
        [InlineData("std", "C4x5.4", 4, "BD4S-")]
        [InlineData("std", "C5x6.1", 4, "BEL4S-")]
        [InlineData("std", "C5x6.7", 4, "BE4S-")]
        [InlineData("rev", "C3x3.5", 4, "RBA4S-")]
        [InlineData("rev", "C3x4.1", 4, "RBB4S-")]
        [InlineData("rev", "C4x4.5", 4, "RBC4S-")]
        [InlineData("rev", "C4x5.4", 4, "RBD4S-")]
        [InlineData("rev", "C5x6.1", 4, "RBEL4S-")]
        [InlineData("rev", "C5x6.7", 4, "RBE4S-")]
        public void CanGetLoadBeamPartNumber(string orientation, string channelSize, int loadsWide, string expPartNumber)
        {
            var part = _fixture.CreateRootPart<ShelfLoadBeam>();
            part.Orientation.SetValue(orientation);
            part.ChannelSize.SetValue(channelSize);
            part.LoadsWide.SetValue(loadsWide);

            Assert.Equal(expPartNumber, part.PartNumberPrefix.Value);
        }

        [Fact]
        public void CanIncrementLoadBeamPartNumber()
        {
            var part1 = _fixture.CreateRootPart<ShelfLoadBeam>();
            var part2 = _fixture.CreateRootPart<ShelfLoadBeam>();
            part2.Width.SetValue(60);

            var pn1 = part1.PartNumber.Value;
            var pn2 = part2.PartNumber.Value;

            Assert.True(GetIncrement(pn1) != GetIncrement(pn2));
        }

        [Theory]
        [InlineData("8", "PC31049")]
        [InlineData("8infAdj", "PC31076")]
        [InlineData("9", "PC31048")]
        [InlineData("12", "PC31050")]
        [InlineData("13", "PC31047")]
        public void CanGetShelfBeamEndBracketPartNumber(string type, string expectedPartNumber)
        {
            var beamEndBracket = _fixture.CreateRootPart<ShelfBeamEndBracket>();

            beamEndBracket.Type.SetValue(type);

            Assert.Equal(expectedPartNumber, beamEndBracket.PartNumber.Value);
        }

        #endregion

        #region Bolt On Heavy Horizontal

        [Theory]
        [InlineData("C3x3.5", "BOHEAVYA-")]
        [InlineData("C3x4.1", "BOHEAVYB-")]
        [InlineData("C4x4.5", "BOHEAVYC-")]
        [InlineData("C4x5.4", "BOHEAVYD-")]
        [InlineData("C5x6.1", "BOHEAVYEL-")]
        [InlineData("C5x6.7", "BOHEAVYE-")]
        public void CanGetBoltOnHeavyHorizontalPartNumberPrefix(string channelSize, string expPartNumber)
        {
            var part = _fixture.CreateRootPart<BoltOnHorizontalBrace>();
            part.ChannelSize.SetValue(channelSize);
            
            Assert.Equal(expPartNumber, part.PartNumberPrefix.Value); //TODO: PartNumberPrefix is in the Channel, not BoltOnHorizontalBrace
        }

        [Fact]
        public void CanIncrementBoltOnHeavyHorizontalPartNumber()
        {
            var part1 = _fixture.CreateRootPart<BoltOnHorizontalBrace>();
            var part2 = _fixture.CreateRootPart<BoltOnHorizontalBrace>();
            part2.Length.SetValue(120);

            var pn1 = part1.PartNumber.Value;
            var pn2 = part2.PartNumber.Value;

            Assert.True(GetIncrement(pn1) != GetIncrement(pn2));
        }

        #endregion

        #region Pallet Stop Tests

        [Theory]
        [InlineData("PS-R-X", "C3x3.5", "PSR3-")]
        [InlineData("PS-R-X", "C3x4.1", "PSR3-")]
        [InlineData("PS-R-X", "C4x4.5", "PSR4-")]
        [InlineData("PS-R-X", "C4x5.4", "PSR4-")]
        [InlineData("PS-R-X", "C5x6.1", "PSR5-")]
        [InlineData("PS-R-X", "C5x6.7", "PSR5-")]
        [InlineData("PS-R-X", "C6x8.2", "PSR6-")]
        [InlineData("PS-R-X", "C7x9.8", "PSR7-")]
        [InlineData("PS-R-X", "C8x11.5", "PSR8-")]
        [InlineData("PS-R-X", "C10x15.3", "PSR10-")]
        [InlineData("PS-L-DD-X", "C3x3.5", "PSLDD3-")]
        [InlineData("PS-L-DD-X", "C3x4.1", "PSLDD3-")]
        [InlineData("PS-L-DD-X", "C4x4.5", "PSLDD4-")]
        [InlineData("PS-L-DD-X", "C4x5.4", "PSLDD4-")]
        [InlineData("PS-L-DD-X", "C5x6.1", "PSLDD5-")]
        [InlineData("PS-L-DD-X", "C5x6.7", "PSLDD5-")]
        [InlineData("PS-L-DD-X", "C6x8.2", "PSLDD6-")]
        [InlineData("PS-L-DD-X", "C7x9.8", "PSLDD7-")]
        [InlineData("PS-L-DD-X", "C8x11.5", "PSLDD8-")]
        [InlineData("PS-L-DD-X", "C10x15.3", "PSLDD10-")]
        public void CanGetPalletStopPartNumberPrefix(string style, string beamSize, string expPartNumber)
        {
            var part = _fixture.CreateRootPart<ShelfPalletStop>();
            part.Style.SetValue(style);
            part.BeamSize.SetValue(beamSize);

            Assert.Equal(expPartNumber, part.PartNumberPrefix.Value); 
        }

        [Fact]
        public void CanIncrementPalletStopPartNumber()
        {
            var part1 = _fixture.CreateRootPart<ShelfPalletStop>();
            var part2 = _fixture.CreateRootPart<ShelfPalletStop>();
            part2.Width.SetValue(5);

            var pn1 = part1.PartNumber.Value;
            var pn2 = part2.PartNumber.Value;

            Assert.True(GetIncrement(pn1) != GetIncrement(pn2));
        }

        #endregion

        #region Doubler Cap Tests

        [Theory]
        [InlineData("C3x3.5", "UT", "PC31034")]
        [InlineData("C4x5.4", "UT", "PC31034-4")]
        [InlineData("C5x6.1", "UT", "PC31034-5")]
        [InlineData("C3x3.5", "GA", "PC31034-MS")]
        [InlineData("C4x5.4", "GA", "PC31034-4-MS")]
        [InlineData("C5x6.1", "GA", "PC31034-5-MS")]
        public void CanGetDoublerCapPartNumber(string columnSize, string location, string expPartNumber)
        {
            var part = _fixture.CreateRootPart<FrameUpright>();
            part.UprightColumnSize.SetValue(columnSize);
            part.HasFrontDoubler.SetValue(true);

            var doublerCap = part.FrontUprightColumnAssembly.DoublerCap;
            doublerCap.Location.SetValue(location);

            Assert.Equal(expPartNumber, doublerCap.PartNumber.Value);
        }

        #endregion

        #region Angle Post Protector Tests

        [Theory]
        [InlineData(@"2 x 2 x 1/4", "P40900-")]
        [InlineData(@"2-1/2 x 2-1/2 x 1/4", "P40900-4-")]
        [InlineData(@"3 x 3 x 1/4", "P40900-5-")]
        public void CanGetAnglePostProtectorPartNumberPrefix(string angleType, string expPartNumber)
        {
            var part = _fixture.CreateRootPart<AnglePostProtector>();
            part.AngleType.SetValue(angleType);

            Assert.Equal(expPartNumber, part.PartNumberPrefix.Value);
        }

        [Fact]
        public void CanIncrementAnglePostProtectorPartNumber()
        {
            var part1 = _fixture.CreateRootPart<AnglePostProtector>();
            var part2 = _fixture.CreateRootPart<AnglePostProtector>();
            part2.Length.SetValue(120);

            var pn1 = part1.PartNumber.Value;
            var pn2 = part2.PartNumber.Value;

            Assert.True(GetIncrement(pn1) != GetIncrement(pn2));
        }

        #endregion

        #region Slant Leg Column Tube Tests

        [Theory]
        [InlineData("L", 3, "P40277-MS-")]
        [InlineData("L", 4, "P40276-MS-")]
        [InlineData("LS", 3, "P40279-")]
        [InlineData("LS", 4, "P40282-")]
        public void CanGetSlantLegColumnTubePartNumberPrefix(string uprightType, float depth, string expPartNumber)
        {
            var part = _fixture.CreateRootPart<SlantLegColumnTube>();
            part.UprightType.SetValue(uprightType);
            part.Depth.SetValue(depth);

            Assert.Equal(expPartNumber, part.PartNumberPrefix.Value);
        }

        [Fact]
        public void CanIncrementSlantLegColumnTubePartNumber()
        {
            var part1 = _fixture.CreateRootPart<SlantLegColumnTube>();
            var part2 = _fixture.CreateRootPart<SlantLegColumnTube>();
            part2.Length.SetValue(120);

            var pn1 = part1.PartNumber.Value;
            var pn2 = part2.PartNumber.Value;

            Assert.True(GetIncrement(pn1) != GetIncrement(pn2));
        }

        [Theory]
        [InlineData(true, "P40288")]
        [InlineData(false, "P40289")]
        public void CanGetSlantLegTubeCapPartNumber(bool hasDoubler, string expPartNumber)
        {
            var part = _fixture.CreateRootPart<TubeCapPlate>();
            part.HasDoubler.SetValue(hasDoubler);

            Assert.Equal(expPartNumber, part.PartNumber.Value);
        }

        #endregion

        #region Bullnose Tests

        [Theory]
        [InlineData(7.75, 3, "P40901-8P3")]
        [InlineData(7.75, 4, "P40901-8P4")]
        [InlineData(7.75, 5, "P40901-8P5")]
        [InlineData(9.75, 3, "P40901-10P3")]
        [InlineData(9.75, 4, "P40901-10P4")]
        [InlineData(9.75, 5, "P40901-10P5")]
        [InlineData(11.75, 3, "P40901-12P3")]
        [InlineData(11.75, 4, "P40901-12P4")]
        [InlineData(11.75, 5, "P40901-12P5")]
        public void CanGetBullnosePartNumber(float height, float columnWidth, string expPartNumber)
        {
            var part = _fixture.CreateRootPart<Bullnose>();
            part.ColumnWidth.SetValue(columnWidth);
            part.Height.SetValue(height);

            Assert.Equal(expPartNumber, part.PartNumber.Value);
        }

        #endregion

        #region Bolt-on Heavy Horizontal Angle Tests

        [Theory]
        [InlineData(2, "P40810")]
        [InlineData(3, "P40810-4")]
        [InlineData(4, "P40810-5")]
        public void CanGetBoltonHeavyHorizontalAnglePartNumber(float length, string expPartNumber)
        {
            var part = _fixture.CreateRootPart<BoltonHeavyHorizontalAngle>();
            part.Length.SetValue(length);

            Assert.Equal(expPartNumber, part.PartNumber.Value);
        }

        #endregion

        #region Diagonal Brace Tests

        [Fact]
        public void CanGetDiagonalBracePartNumberPrefix()
        {
            var part = _fixture.CreateRootPart<DiagonalBrace>();

            Assert.Equal("P40016-", part.PartNumberPrefix.Value);
        }

        [Fact]
        public void CanIncrementDiagonalBracePartNumber()
        {
            var part1 = _fixture.CreateRootPart<DiagonalBrace>();
            part1.Length.SetValue(12);
            var part2 = _fixture.CreateRootPart<DiagonalBrace>();
            part2.Length.SetValue(36);

            var pn1 = part1.PartNumber.Value;
            var pn2 = part2.PartNumber.Value;

            Assert.True(GetIncrement(pn1) != GetIncrement(pn2));
        }

        #endregion

        #region Horizontal Brace Tests
        
        [Theory]
        [InlineData(0, "P40015-")]
        [InlineData(.5f, "P40015M-")]
        [InlineData(5.5f, "P40015M-")]
        public void CanGetHorizontalBracePartNumberPrefix(float miterAngle, string expPrefix)
        {
            var part = _fixture.CreateRootPart<HorizontalBrace>();
            part.RearMiterAngle.SetValue(miterAngle);

            Assert.Equal(expPrefix, part.PartNumberPrefix.Value);
        }

        [Fact]
        public void CanIncrementHorizontalBracePartNumber()
        {
            var part1 = _fixture.CreateRootPart<HorizontalBrace>();
            part1.Length.SetValue(12);
            var part2 = _fixture.CreateRootPart<HorizontalBrace>();
            part2.Length.SetValue(36);

            var pn1 = part1.PartNumber.Value;
            var pn2 = part2.PartNumber.Value;

            Assert.True(GetIncrement(pn1) != GetIncrement(pn2));
        }

        #endregion

        #region Heavy Horizontal Brace Tests

        [Theory]
        [InlineData("C3x3.5", "H", "P40630-")]
        [InlineData("C4x4.5", "H", "P40640-")]
        [InlineData("C5x6.1", "H", "P40650-")]
        [InlineData("C3x4.1", "H", "P40730-")]
        [InlineData("C4x5.4", "H", "P40740-")]
        [InlineData("C5x6.7", "H", "P40750-")]
        [InlineData("C3x3.5", "L", "P40630M-")]
        [InlineData("C4x4.5", "L", "P40640M-")]
        [InlineData("C5x6.1", "L", "P40650M-")]
        [InlineData("C3x4.1", "L", "P40730M-")]
        [InlineData("C4x5.4", "L", "P40740M-")]
        [InlineData("C5x6.7", "L", "P40750M-")]
        [InlineData("C3x3.5", "LS", "P40630M-")]
        [InlineData("C4x4.5", "LS", "P40640M-")]
        [InlineData("C5x6.1", "LS", "P40650M-")]
        [InlineData("C3x4.1", "LS", "P40730M-")]
        [InlineData("C4x5.4", "LS", "P40740M-")]
        [InlineData("C5x6.7", "LS", "P40750M-")]
        [InlineData("C3x3.5", "BL", "P40630M-")]
        [InlineData("C4x4.5", "BL", "P40640M-")]
        [InlineData("C5x6.1", "BL", "P40650M-")]
        [InlineData("C3x4.1", "BL", "P40730M-")]
        [InlineData("C4x5.4", "BL", "P40740M-")]
        [InlineData("C5x6.7", "BL", "P40750M-")]
        [InlineData("C4x4.5", "D", "P40645-")]
        public void CanGetHeavyHorizontalBracePartNumberPrefix(string channelSize, string uprightType, string expPartNumber)
        {
            var part = _fixture.CreateRootPart<HeavyHorizontalBrace>();
            part.ChannelSize.SetValue(channelSize);
            part.UprightType.SetValue(uprightType);

            Assert.Equal(expPartNumber, part.PartNumberPrefix.Value);
        }

        [Fact]
        public void CanIncrementHeavyHorizontalBracePartNumber()
        {
            var part1 = _fixture.CreateRootPart<HeavyHorizontalBrace>();
            part1.UprightType.SetValue("H");
            var part2 = _fixture.CreateRootPart<HeavyHorizontalBrace>();
            part2.UprightType.SetValue("H");
            part2.Length.SetValue(60);

            var pn1 = part1.PartNumber.Value;
            var pn2 = part2.PartNumber.Value;

            Assert.True(GetIncrement(pn1) != GetIncrement(pn2));
        }

        #endregion

        #region Heavy Horizontal Doubler Tests

        [Fact]
        public void CanGetHeavyHorizontalDoublerPartNumberPrefix()
        {
            var part = _fixture.CreateRootPart<HeavyHorizontalDoubler>();
            part.ChannelSize.SetValue("C4x4.5");

            Assert.Equal("P40645-1H-", part.PartNumberPrefix.Value);
        }

        [Fact]
        public void CanIncrementHeavyHorizontalDoublerPartNumber()
        {
            var part1 = _fixture.CreateRootPart<HeavyHorizontalDoubler>();
            part1.ChannelSize.SetValue("C4x4.5");
            var part2 = _fixture.CreateRootPart<HeavyHorizontalDoubler>();
            part2.ChannelSize.SetValue("C4x4.5");
            part2.Length.SetValue(60);

            var pn1 = part1.PartNumber.Value;
            var pn2 = part2.PartNumber.Value;

            Assert.True(GetIncrement(pn1) != GetIncrement(pn2));
        }

        #endregion

        #region Heavy Horizontal Weld Clip Tests

        [Fact]
        public void CanGetHeavyHorizontalWeldClipPartNumber()
        {
            var part = _fixture.CreateRootPart<HeavyHorizontalWeldClip>();

            Assert.Equal("P40103", part.PartNumber.Value);
        }

        #endregion

        #region Heavy Horizontal Weld Plate Tests

        [Theory]
        [InlineData(3, "P40104-3")]
        [InlineData(4, "P40104-4")]
        [InlineData(5, "P40104-5")]
        public void CanGetHeavyHorizontalWeldPlatePartNumber(float channelWidth, string expPartNumber)
        {
            var part = _fixture.CreateRootPart<HeavyHorizontalWeldPlate>();
            part.ChannelWidth.SetValue(channelWidth);

            Assert.Equal(expPartNumber, part.PartNumber.Value);
        }

        #endregion

        #region Top and Bottom Tippmann Horizontal Channels Tests

        [Theory]
        [InlineData(3, true, "P40632-")]
        [InlineData(3, false, "P40633-")]
        [InlineData(4, true, "P40642-")]
        [InlineData(4, false, "P40643-")]
        [InlineData(5, true, "P40752-")]
        [InlineData(5, false, "P40753-")]
        public void CanGetTippmannHorizontalPartNumberPrefix(float channelWidth, bool isTop, string expPartNumber)
        {
            var part = _fixture.CreateRootPart<TippmannHorizontal>();
            part.ChannelWidth.SetValue(channelWidth);
            part.IsTopChannel.SetValue(isTop);

            Assert.Equal(expPartNumber, part.PartNumberPrefix.Value);
        }

        [Fact]
        public void CanIncrementTippmanHorizontalPartNumber()
        {
            var part1 = _fixture.CreateRootPart<TippmannHorizontal>();
            var part2 = _fixture.CreateRootPart<TippmannHorizontal>();
            part2.Length.SetValue(60);

            var pn1 = part1.PartNumber.Value;
            var pn2 = part2.PartNumber.Value;

            Assert.True(GetIncrement(pn1) != GetIncrement(pn2));
        }

        #endregion

        #region Footplate Tests

        [Theory]
        [InlineData(.375, "FP1K-A", "S", "C3x3.5", ".5", true, "P40304-FP1K-A-3-4-5-F")]
        [InlineData(.375, "FP1K-B", "S", "C3x3.5", ".5", true, "P40304-FP1K-B-3-4-5-F")]
        [InlineData(.375, "FP1K-C", "S", "C3x3.5", ".5", true, "P40304-FP1K-C-3-4-5-F")]
        [InlineData(.375, "FP1K-D", "S", "C3x3.5", ".5", true, "P40304-FP1K-D-3-4-5-F")]
        [InlineData(.375, "FP2K-A", "S", "C3x3.5", ".5", true, "P40304-FP2K-A-3-4-5-F")]
        [InlineData(.375, "FP2K-B", "S", "C3x3.5", ".5", true, "P40304-FP2K-B-3-4-5-F")]
        [InlineData(.375, "FP3K-A", "S", "C3x3.5", ".5", true, "P40304-FP3K-A-3-4-5-F")]
        [InlineData(.375, "FP3K-B", "S", "C3x3.5", ".5", true, "P40304-FP3K-B-3-4-5-F")]
        [InlineData(.375, "FP3K-C", "S", "C3x3.5", ".5", true, "P40304-FP3K-C-3-4-5-F")]
        [InlineData(.375, "FP3K-D", "S", "C3x3.5", ".5", true, "P40304-FP3K-D-3-4-5-F")]
        [InlineData(.375, "FP4K-A", "S", "C3x3.5", ".5", true, "P40304-FP4K-A-3-4-5-F")]
        [InlineData(.375, "FP4K-B", "S", "C3x3.5", ".5", true, "P40304-FP4K-B-3-4-5-F")]
        [InlineData(.375, "FP5K-A", "S", "C3x3.5", ".5", true, "P40304-FP5K-A-3-4-5-F")]
        [InlineData(.375, "FP5K-B", "S", "C3x3.5", ".5", true, "P40304-FP5K-B-3-4-5-F")]
        [InlineData(.375, "FP5K-C", "S", "C3x3.5", ".5", true, "P40304-FP5K-C-3-4-5-F")]
        [InlineData(.375, "FP5K-D", "S", "C3x3.5", ".5", true, "P40304-FP5K-D-3-4-5-F")]
        [InlineData(.375, "FP6K-A", "S", "C3x3.5", ".5", true, "P40304-FP6K-A-3-4-5-F")]
        [InlineData(.375, "FP6K-B", "S", "C3x3.5", ".5", true, "P40304-FP6K-B-3-4-5-F")]
        [InlineData(.375, "FP1K-A", "S", "C4x4.5", ".5", true, "P40304-FP1K-A-4-5-5-F")]
        [InlineData(.375, "FP1K-B", "S", "C4x4.5", ".5", true, "P40304-FP1K-B-4-5-5-F")]
        [InlineData(.375, "FP1K-C", "S", "C4x4.5", ".5", true, "P40304-FP1K-C-4-5-5-F")]
        [InlineData(.375, "FP1K-D", "S", "C4x4.5", ".5", true, "P40304-FP1K-D-4-5-5-F")]
        [InlineData(.375, "FP2K-A", "S", "C4x4.5", ".5", true, "P40304-FP2K-A-4-5-5-F")]
        [InlineData(.375, "FP2K-B", "S", "C4x4.5", ".5", true, "P40304-FP2K-B-4-5-5-F")]
        [InlineData(.375, "FP3K-A", "S", "C4x4.5", ".5", true, "P40304-FP3K-A-4-5-5-F")]
        [InlineData(.375, "FP3K-B", "S", "C4x4.5", ".5", true, "P40304-FP3K-B-4-5-5-F")]
        [InlineData(.375, "FP3K-C", "S", "C4x4.5", ".5", true, "P40304-FP3K-C-4-5-5-F")]
        [InlineData(.375, "FP3K-D", "S", "C4x4.5", ".5", true, "P40304-FP3K-D-4-5-5-F")]
        [InlineData(.375, "FP4K-A", "S", "C4x4.5", ".5", true, "P40304-FP4K-A-4-5-5-F")]
        [InlineData(.375, "FP4K-B", "S", "C4x4.5", ".5", true, "P40304-FP4K-B-4-5-5-F")]
        [InlineData(.375, "FP5K-A", "S", "C4x4.5", ".5", true, "P40304-FP5K-A-4-5-5-F")]
        [InlineData(.375, "FP5K-B", "S", "C4x4.5", ".5", true, "P40304-FP5K-B-4-5-5-F")]
        [InlineData(.375, "FP5K-C", "S", "C4x4.5", ".5", true, "P40304-FP5K-C-4-5-5-F")]
        [InlineData(.375, "FP5K-D", "S", "C4x4.5", ".5", true, "P40304-FP5K-D-4-5-5-F")]
        [InlineData(.375, "FP6K-A", "S", "C4x4.5", ".5", true, "P40304-FP6K-A-4-5-5-F")]
        [InlineData(.375, "FP6K-B", "S", "C4x4.5", ".5", true, "P40304-FP6K-B-4-5-5-F")]
        [InlineData(.375, "FP1K-A", "S", "C5x6.7", ".5", true, "P40304-FP1K-A-5-6-5-F")]
        [InlineData(.375, "FP1K-B", "S", "C5x6.7", ".5", true, "P40304-FP1K-B-5-6-5-F")]
        [InlineData(.375, "FP1K-C", "S", "C5x6.7", ".5", true, "P40304-FP1K-C-5-6-5-F")]
        [InlineData(.375, "FP1K-D", "S", "C5x6.7", ".5", true, "P40304-FP1K-D-5-6-5-F")]
        [InlineData(.375, "FP2K-A", "S", "C5x6.7", ".5", true, "P40304-FP2K-A-5-6-5-F")]
        [InlineData(.375, "FP2K-B", "S", "C5x6.7", ".5", true, "P40304-FP2K-B-5-6-5-F")]
        [InlineData(.375, "FP3K-A", "S", "C5x6.7", ".5", true, "P40304-FP3K-A-5-6-5-F")]
        [InlineData(.375, "FP3K-B", "S", "C5x6.7", ".5", true, "P40304-FP3K-B-5-6-5-F")]
        [InlineData(.375, "FP3K-C", "S", "C5x6.7", ".5", true, "P40304-FP3K-C-5-6-5-F")]
        [InlineData(.375, "FP3K-D", "S", "C5x6.7", ".5", true, "P40304-FP3K-D-5-6-5-F")]
        [InlineData(.375, "FP4K-A", "S", "C5x6.7", ".5", true, "P40304-FP4K-A-5-6-5-F")]
        [InlineData(.375, "FP4K-B", "S", "C5x6.7", ".5", true, "P40304-FP4K-B-5-6-5-F")]
        [InlineData(.375, "FP5K-A", "S", "C5x6.7", ".5", true, "P40304-FP5K-A-5-6-5-F")]
        [InlineData(.375, "FP5K-B", "S", "C5x6.7", ".5", true, "P40304-FP5K-B-5-6-5-F")]
        [InlineData(.375, "FP5K-C", "S", "C5x6.7", ".5", true, "P40304-FP5K-C-5-6-5-F")]
        [InlineData(.375, "FP5K-D", "S", "C5x6.7", ".5", true, "P40304-FP5K-D-5-6-5-F")]
        [InlineData(.375, "FP6K-A", "S", "C5x6.7", ".5", true, "P40304-FP6K-A-5-6-5-F")]
        [InlineData(.375, "FP6K-B", "S", "C5x6.7", ".5", true, "P40304-FP6K-B-5-6-5-F")]
        [InlineData(.375, "FP1K-A", "S", "C3x3.5", ".625", true, "P40304-FP1K-A-3-4-6-F")]
        [InlineData(.375, "FP1K-B", "S", "C3x3.5", ".625", true, "P40304-FP1K-B-3-4-6-F")]
        [InlineData(.375, "FP1K-C", "S", "C3x3.5", ".625", true, "P40304-FP1K-C-3-4-6-F")]
        [InlineData(.375, "FP1K-D", "S", "C3x3.5", ".625", true, "P40304-FP1K-D-3-4-6-F")]
        [InlineData(.375, "FP2K-A", "S", "C3x3.5", ".625", true, "P40304-FP2K-A-3-4-6-F")]
        [InlineData(.375, "FP2K-B", "S", "C3x3.5", ".625", true, "P40304-FP2K-B-3-4-6-F")]
        [InlineData(.375, "FP3K-A", "S", "C3x3.5", ".625", true, "P40304-FP3K-A-3-4-6-F")]
        [InlineData(.375, "FP3K-B", "S", "C3x3.5", ".625", true, "P40304-FP3K-B-3-4-6-F")]
        [InlineData(.375, "FP3K-C", "S", "C3x3.5", ".625", true, "P40304-FP3K-C-3-4-6-F")]
        [InlineData(.375, "FP3K-D", "S", "C3x3.5", ".625", true, "P40304-FP3K-D-3-4-6-F")]
        [InlineData(.375, "FP4K-A", "S", "C3x3.5", ".625", true, "P40304-FP4K-A-3-4-6-F")]
        [InlineData(.375, "FP4K-B", "S", "C3x3.5", ".625", true, "P40304-FP4K-B-3-4-6-F")]
        [InlineData(.375, "FP5K-A", "S", "C3x3.5", ".625", true, "P40304-FP5K-A-3-4-6-F")]
        [InlineData(.375, "FP5K-B", "S", "C3x3.5", ".625", true, "P40304-FP5K-B-3-4-6-F")]
        [InlineData(.375, "FP5K-C", "S", "C3x3.5", ".625", true, "P40304-FP5K-C-3-4-6-F")]
        [InlineData(.375, "FP5K-D", "S", "C3x3.5", ".625", true, "P40304-FP5K-D-3-4-6-F")]
        [InlineData(.375, "FP6K-A", "S", "C3x3.5", ".625", true, "P40304-FP6K-A-3-4-6-F")]
        [InlineData(.375, "FP6K-B", "S", "C3x3.5", ".625", true, "P40304-FP6K-B-3-4-6-F")]
        [InlineData(.375, "FP1K-A", "S", "C4x4.5", ".625", true, "P40304-FP1K-A-4-5-6-F")]
        [InlineData(.375, "FP1K-B", "S", "C4x4.5", ".625", true, "P40304-FP1K-B-4-5-6-F")]
        [InlineData(.375, "FP1K-C", "S", "C4x4.5", ".625", true, "P40304-FP1K-C-4-5-6-F")]
        [InlineData(.375, "FP1K-D", "S", "C4x4.5", ".625", true, "P40304-FP1K-D-4-5-6-F")]
        [InlineData(.375, "FP2K-A", "S", "C4x4.5", ".625", true, "P40304-FP2K-A-4-5-6-F")]
        [InlineData(.375, "FP2K-B", "S", "C4x4.5", ".625", true, "P40304-FP2K-B-4-5-6-F")]
        [InlineData(.375, "FP3K-A", "S", "C4x4.5", ".625", true, "P40304-FP3K-A-4-5-6-F")]
        [InlineData(.375, "FP3K-B", "S", "C4x4.5", ".625", true, "P40304-FP3K-B-4-5-6-F")]
        [InlineData(.375, "FP3K-C", "S", "C4x4.5", ".625", true, "P40304-FP3K-C-4-5-6-F")]
        [InlineData(.375, "FP3K-D", "S", "C4x4.5", ".625", true, "P40304-FP3K-D-4-5-6-F")]
        [InlineData(.375, "FP4K-A", "S", "C4x4.5", ".625", true, "P40304-FP4K-A-4-5-6-F")]
        [InlineData(.375, "FP4K-B", "S", "C4x4.5", ".625", true, "P40304-FP4K-B-4-5-6-F")]
        [InlineData(.375, "FP5K-A", "S", "C4x4.5", ".625", true, "P40304-FP5K-A-4-5-6-F")]
        [InlineData(.375, "FP5K-B", "S", "C4x4.5", ".625", true, "P40304-FP5K-B-4-5-6-F")]
        [InlineData(.375, "FP5K-C", "S", "C4x4.5", ".625", true, "P40304-FP5K-C-4-5-6-F")]
        [InlineData(.375, "FP5K-D", "S", "C4x4.5", ".625", true, "P40304-FP5K-D-4-5-6-F")]
        [InlineData(.375, "FP6K-A", "S", "C4x4.5", ".625", true, "P40304-FP6K-A-4-5-6-F")]
        [InlineData(.375, "FP6K-B", "S", "C4x4.5", ".625", true, "P40304-FP6K-B-4-5-6-F")]
        [InlineData(.375, "FP1K-A", "S", "C5x6.7", ".625", true, "P40304-FP1K-A-5-6-6-F")]
        [InlineData(.375, "FP1K-B", "S", "C5x6.7", ".625", true, "P40304-FP1K-B-5-6-6-F")]
        [InlineData(.375, "FP1K-C", "S", "C5x6.7", ".625", true, "P40304-FP1K-C-5-6-6-F")]
        [InlineData(.375, "FP1K-D", "S", "C5x6.7", ".625", true, "P40304-FP1K-D-5-6-6-F")]
        [InlineData(.375, "FP2K-A", "S", "C5x6.7", ".625", true, "P40304-FP2K-A-5-6-6-F")]
        [InlineData(.375, "FP2K-B", "S", "C5x6.7", ".625", true, "P40304-FP2K-B-5-6-6-F")]
        [InlineData(.375, "FP3K-A", "S", "C5x6.7", ".625", true, "P40304-FP3K-A-5-6-6-F")]
        [InlineData(.375, "FP3K-B", "S", "C5x6.7", ".625", true, "P40304-FP3K-B-5-6-6-F")]
        [InlineData(.375, "FP3K-C", "S", "C5x6.7", ".625", true, "P40304-FP3K-C-5-6-6-F")]
        [InlineData(.375, "FP3K-D", "S", "C5x6.7", ".625", true, "P40304-FP3K-D-5-6-6-F")]
        [InlineData(.375, "FP4K-A", "S", "C5x6.7", ".625", true, "P40304-FP4K-A-5-6-6-F")]
        [InlineData(.375, "FP4K-B", "S", "C5x6.7", ".625", true, "P40304-FP4K-B-5-6-6-F")]
        [InlineData(.375, "FP5K-A", "S", "C5x6.7", ".625", true, "P40304-FP5K-A-5-6-6-F")]
        [InlineData(.375, "FP5K-B", "S", "C5x6.7", ".625", true, "P40304-FP5K-B-5-6-6-F")]
        [InlineData(.375, "FP5K-C", "S", "C5x6.7", ".625", true, "P40304-FP5K-C-5-6-6-F")]
        [InlineData(.375, "FP5K-D", "S", "C5x6.7", ".625", true, "P40304-FP5K-D-5-6-6-F")]
        [InlineData(.375, "FP6K-A", "S", "C5x6.7", ".625", true, "P40304-FP6K-A-5-6-6-F")]
        [InlineData(.375, "FP6K-B", "S", "C5x6.7", ".625", true, "P40304-FP6K-B-5-6-6-F")]
        [InlineData(.375, "FP1K-A", "S", "C3x3.5", ".75", true, "P40304-FP1K-A-3-4-7-F")]
        [InlineData(.375, "FP1K-B", "S", "C3x3.5", ".75", true, "P40304-FP1K-B-3-4-7-F")]
        [InlineData(.375, "FP1K-C", "S", "C3x3.5", ".75", true, "P40304-FP1K-C-3-4-7-F")]
        [InlineData(.375, "FP1K-D", "S", "C3x3.5", ".75", true, "P40304-FP1K-D-3-4-7-F")]
        [InlineData(.375, "FP2K-A", "S", "C3x3.5", ".75", true, "P40304-FP2K-A-3-4-7-F")]
        [InlineData(.375, "FP2K-B", "S", "C3x3.5", ".75", true, "P40304-FP2K-B-3-4-7-F")]
        [InlineData(.375, "FP3K-A", "S", "C3x3.5", ".75", true, "P40304-FP3K-A-3-4-7-F")]
        [InlineData(.375, "FP3K-B", "S", "C3x3.5", ".75", true, "P40304-FP3K-B-3-4-7-F")]
        [InlineData(.375, "FP3K-C", "S", "C3x3.5", ".75", true, "P40304-FP3K-C-3-4-7-F")]
        [InlineData(.375, "FP3K-D", "S", "C3x3.5", ".75", true, "P40304-FP3K-D-3-4-7-F")]
        [InlineData(.375, "FP4K-A", "S", "C3x3.5", ".75", true, "P40304-FP4K-A-3-4-7-F")]
        [InlineData(.375, "FP4K-B", "S", "C3x3.5", ".75", true, "P40304-FP4K-B-3-4-7-F")]
        [InlineData(.375, "FP5K-A", "S", "C3x3.5", ".75", true, "P40304-FP5K-A-3-4-7-F")]
        [InlineData(.375, "FP5K-B", "S", "C3x3.5", ".75", true, "P40304-FP5K-B-3-4-7-F")]
        [InlineData(.375, "FP5K-C", "S", "C3x3.5", ".75", true, "P40304-FP5K-C-3-4-7-F")]
        [InlineData(.375, "FP5K-D", "S", "C3x3.5", ".75", true, "P40304-FP5K-D-3-4-7-F")]
        [InlineData(.375, "FP6K-A", "S", "C3x3.5", ".75", true, "P40304-FP6K-A-3-4-7-F")]
        [InlineData(.375, "FP6K-B", "S", "C3x3.5", ".75", true, "P40304-FP6K-B-3-4-7-F")]                                                                                   
        [InlineData(.375, "FP1K-A", "S", "C4x4.5", ".75", true, "P40304-FP1K-A-4-5-7-F")]
        [InlineData(.375, "FP1K-B", "S", "C4x4.5", ".75", true, "P40304-FP1K-B-4-5-7-F")]
        [InlineData(.375, "FP1K-C", "S", "C4x4.5", ".75", true, "P40304-FP1K-C-4-5-7-F")]
        [InlineData(.375, "FP1K-D", "S", "C4x4.5", ".75", true, "P40304-FP1K-D-4-5-7-F")]
        [InlineData(.375, "FP2K-A", "S", "C4x4.5", ".75", true, "P40304-FP2K-A-4-5-7-F")]
        [InlineData(.375, "FP2K-B", "S", "C4x4.5", ".75", true, "P40304-FP2K-B-4-5-7-F")]
        [InlineData(.375, "FP3K-A", "S", "C4x4.5", ".75", true, "P40304-FP3K-A-4-5-7-F")]
        [InlineData(.375, "FP3K-B", "S", "C4x4.5", ".75", true, "P40304-FP3K-B-4-5-7-F")]
        [InlineData(.375, "FP3K-C", "S", "C4x4.5", ".75", true, "P40304-FP3K-C-4-5-7-F")]
        [InlineData(.375, "FP3K-D", "S", "C4x4.5", ".75", true, "P40304-FP3K-D-4-5-7-F")]
        [InlineData(.375, "FP4K-A", "S", "C4x4.5", ".75", true, "P40304-FP4K-A-4-5-7-F")]
        [InlineData(.375, "FP4K-B", "S", "C4x4.5", ".75", true, "P40304-FP4K-B-4-5-7-F")]
        [InlineData(.375, "FP5K-A", "S", "C4x4.5", ".75", true, "P40304-FP5K-A-4-5-7-F")]
        [InlineData(.375, "FP5K-B", "S", "C4x4.5", ".75", true, "P40304-FP5K-B-4-5-7-F")]
        [InlineData(.375, "FP5K-C", "S", "C4x4.5", ".75", true, "P40304-FP5K-C-4-5-7-F")]
        [InlineData(.375, "FP5K-D", "S", "C4x4.5", ".75", true, "P40304-FP5K-D-4-5-7-F")]
        [InlineData(.375, "FP6K-A", "S", "C4x4.5", ".75", true, "P40304-FP6K-A-4-5-7-F")]
        [InlineData(.375, "FP6K-B", "S", "C4x4.5", ".75", true, "P40304-FP6K-B-4-5-7-F")]                                                                                   
        [InlineData(.375, "FP1K-A", "S", "C5x6.7", ".75", true, "P40304-FP1K-A-5-6-7-F")]
        [InlineData(.375, "FP1K-B", "S", "C5x6.7", ".75", true, "P40304-FP1K-B-5-6-7-F")]
        [InlineData(.375, "FP1K-C", "S", "C5x6.7", ".75", true, "P40304-FP1K-C-5-6-7-F")]
        [InlineData(.375, "FP1K-D", "S", "C5x6.7", ".75", true, "P40304-FP1K-D-5-6-7-F")]
        [InlineData(.375, "FP2K-A", "S", "C5x6.7", ".75", true, "P40304-FP2K-A-5-6-7-F")]
        [InlineData(.375, "FP2K-B", "S", "C5x6.7", ".75", true, "P40304-FP2K-B-5-6-7-F")]
        [InlineData(.375, "FP3K-A", "S", "C5x6.7", ".75", true, "P40304-FP3K-A-5-6-7-F")]
        [InlineData(.375, "FP3K-B", "S", "C5x6.7", ".75", true, "P40304-FP3K-B-5-6-7-F")]
        [InlineData(.375, "FP3K-C", "S", "C5x6.7", ".75", true, "P40304-FP3K-C-5-6-7-F")]
        [InlineData(.375, "FP3K-D", "S", "C5x6.7", ".75", true, "P40304-FP3K-D-5-6-7-F")]
        [InlineData(.375, "FP4K-A", "S", "C5x6.7", ".75", true, "P40304-FP4K-A-5-6-7-F")]
        [InlineData(.375, "FP4K-B", "S", "C5x6.7", ".75", true, "P40304-FP4K-B-5-6-7-F")]
        [InlineData(.375, "FP5K-A", "S", "C5x6.7", ".75", true, "P40304-FP5K-A-5-6-7-F")]
        [InlineData(.375, "FP5K-B", "S", "C5x6.7", ".75", true, "P40304-FP5K-B-5-6-7-F")]
        [InlineData(.375, "FP5K-C", "S", "C5x6.7", ".75", true, "P40304-FP5K-C-5-6-7-F")]
        [InlineData(.375, "FP5K-D", "S", "C5x6.7", ".75", true, "P40304-FP5K-D-5-6-7-F")]
        [InlineData(.375, "FP6K-A", "S", "C5x6.7", ".75", true, "P40304-FP6K-A-5-6-7-F")]
        [InlineData(.375, "FP6K-B", "S", "C5x6.7", ".75", true, "P40304-FP6K-B-5-6-7-F")]
        //1/2in plate thickness
        [InlineData(.5, "FP1K-A", "S", "C3x3.5", ".5", true, "P40305-FP1K-A-3-4-5-F")]
        [InlineData(.5, "FP1K-B", "S", "C3x3.5", ".5", true, "P40305-FP1K-B-3-4-5-F")]
        [InlineData(.5, "FP1K-C", "S", "C3x3.5", ".5", true, "P40305-FP1K-C-3-4-5-F")]
        [InlineData(.5, "FP1K-D", "S", "C3x3.5", ".5", true, "P40305-FP1K-D-3-4-5-F")]
        [InlineData(.5, "FP2K-A", "S", "C3x3.5", ".5", true, "P40305-FP2K-A-3-4-5-F")]
        [InlineData(.5, "FP2K-B", "S", "C3x3.5", ".5", true, "P40305-FP2K-B-3-4-5-F")]
        [InlineData(.5, "FP3K-A", "S", "C3x3.5", ".5", true, "P40305-FP3K-A-3-4-5-F")]
        [InlineData(.5, "FP3K-B", "S", "C3x3.5", ".5", true, "P40305-FP3K-B-3-4-5-F")]
        [InlineData(.5, "FP3K-C", "S", "C3x3.5", ".5", true, "P40305-FP3K-C-3-4-5-F")]
        [InlineData(.5, "FP3K-D", "S", "C3x3.5", ".5", true, "P40305-FP3K-D-3-4-5-F")]
        [InlineData(.5, "FP4K-A", "S", "C3x3.5", ".5", true, "P40305-FP4K-A-3-4-5-F")]
        [InlineData(.5, "FP4K-B", "S", "C3x3.5", ".5", true, "P40305-FP4K-B-3-4-5-F")]
        [InlineData(.5, "FP5K-A", "S", "C3x3.5", ".5", true, "P40305-FP5K-A-3-4-5-F")]
        [InlineData(.5, "FP5K-B", "S", "C3x3.5", ".5", true, "P40305-FP5K-B-3-4-5-F")]
        [InlineData(.5, "FP5K-C", "S", "C3x3.5", ".5", true, "P40305-FP5K-C-3-4-5-F")]
        [InlineData(.5, "FP5K-D", "S", "C3x3.5", ".5", true, "P40305-FP5K-D-3-4-5-F")]
        [InlineData(.5, "FP6K-A", "S", "C3x3.5", ".5", true, "P40305-FP6K-A-3-4-5-F")]
        [InlineData(.5, "FP6K-B", "S", "C3x3.5", ".5", true, "P40305-FP6K-B-3-4-5-F")]
        [InlineData(.5, "FP1K-A", "S", "C4x4.5", ".5", true, "P40305-FP1K-A-4-5-5-F")]
        [InlineData(.5, "FP1K-B", "S", "C4x4.5", ".5", true, "P40305-FP1K-B-4-5-5-F")]
        [InlineData(.5, "FP1K-C", "S", "C4x4.5", ".5", true, "P40305-FP1K-C-4-5-5-F")]
        [InlineData(.5, "FP1K-D", "S", "C4x4.5", ".5", true, "P40305-FP1K-D-4-5-5-F")]
        [InlineData(.5, "FP2K-A", "S", "C4x4.5", ".5", true, "P40305-FP2K-A-4-5-5-F")]
        [InlineData(.5, "FP2K-B", "S", "C4x4.5", ".5", true, "P40305-FP2K-B-4-5-5-F")]
        [InlineData(.5, "FP3K-A", "S", "C4x4.5", ".5", true, "P40305-FP3K-A-4-5-5-F")]
        [InlineData(.5, "FP3K-B", "S", "C4x4.5", ".5", true, "P40305-FP3K-B-4-5-5-F")]
        [InlineData(.5, "FP3K-C", "S", "C4x4.5", ".5", true, "P40305-FP3K-C-4-5-5-F")]
        [InlineData(.5, "FP3K-D", "S", "C4x4.5", ".5", true, "P40305-FP3K-D-4-5-5-F")]
        [InlineData(.5, "FP4K-A", "S", "C4x4.5", ".5", true, "P40305-FP4K-A-4-5-5-F")]
        [InlineData(.5, "FP4K-B", "S", "C4x4.5", ".5", true, "P40305-FP4K-B-4-5-5-F")]
        [InlineData(.5, "FP5K-A", "S", "C4x4.5", ".5", true, "P40305-FP5K-A-4-5-5-F")]
        [InlineData(.5, "FP5K-B", "S", "C4x4.5", ".5", true, "P40305-FP5K-B-4-5-5-F")]
        [InlineData(.5, "FP5K-C", "S", "C4x4.5", ".5", true, "P40305-FP5K-C-4-5-5-F")]
        [InlineData(.5, "FP5K-D", "S", "C4x4.5", ".5", true, "P40305-FP5K-D-4-5-5-F")]
        [InlineData(.5, "FP6K-A", "S", "C4x4.5", ".5", true, "P40305-FP6K-A-4-5-5-F")]
        [InlineData(.5, "FP6K-B", "S", "C4x4.5", ".5", true, "P40305-FP6K-B-4-5-5-F")]
        [InlineData(.5, "FP1K-A", "S", "C5x6.7", ".5", true, "P40305-FP1K-A-5-6-5-F")]
        [InlineData(.5, "FP1K-B", "S", "C5x6.7", ".5", true, "P40305-FP1K-B-5-6-5-F")]
        [InlineData(.5, "FP1K-C", "S", "C5x6.7", ".5", true, "P40305-FP1K-C-5-6-5-F")]
        [InlineData(.5, "FP1K-D", "S", "C5x6.7", ".5", true, "P40305-FP1K-D-5-6-5-F")]
        [InlineData(.5, "FP2K-A", "S", "C5x6.7", ".5", true, "P40305-FP2K-A-5-6-5-F")]
        [InlineData(.5, "FP2K-B", "S", "C5x6.7", ".5", true, "P40305-FP2K-B-5-6-5-F")]
        [InlineData(.5, "FP3K-A", "S", "C5x6.7", ".5", true, "P40305-FP3K-A-5-6-5-F")]
        [InlineData(.5, "FP3K-B", "S", "C5x6.7", ".5", true, "P40305-FP3K-B-5-6-5-F")]
        [InlineData(.5, "FP3K-C", "S", "C5x6.7", ".5", true, "P40305-FP3K-C-5-6-5-F")]
        [InlineData(.5, "FP3K-D", "S", "C5x6.7", ".5", true, "P40305-FP3K-D-5-6-5-F")]
        [InlineData(.5, "FP4K-A", "S", "C5x6.7", ".5", true, "P40305-FP4K-A-5-6-5-F")]
        [InlineData(.5, "FP4K-B", "S", "C5x6.7", ".5", true, "P40305-FP4K-B-5-6-5-F")]
        [InlineData(.5, "FP5K-A", "S", "C5x6.7", ".5", true, "P40305-FP5K-A-5-6-5-F")]
        [InlineData(.5, "FP5K-B", "S", "C5x6.7", ".5", true, "P40305-FP5K-B-5-6-5-F")]
        [InlineData(.5, "FP5K-C", "S", "C5x6.7", ".5", true, "P40305-FP5K-C-5-6-5-F")]
        [InlineData(.5, "FP5K-D", "S", "C5x6.7", ".5", true, "P40305-FP5K-D-5-6-5-F")]
        [InlineData(.5, "FP6K-A", "S", "C5x6.7", ".5", true, "P40305-FP6K-A-5-6-5-F")]
        [InlineData(.5, "FP6K-B", "S", "C5x6.7", ".5", true, "P40305-FP6K-B-5-6-5-F")]
        [InlineData(.5, "FP1K-A", "S", "C3x3.5", ".625", true, "P40305-FP1K-A-3-4-6-F")]
        [InlineData(.5, "FP1K-B", "S", "C3x3.5", ".625", true, "P40305-FP1K-B-3-4-6-F")]
        [InlineData(.5, "FP1K-C", "S", "C3x3.5", ".625", true, "P40305-FP1K-C-3-4-6-F")]
        [InlineData(.5, "FP1K-D", "S", "C3x3.5", ".625", true, "P40305-FP1K-D-3-4-6-F")]
        [InlineData(.5, "FP2K-A", "S", "C3x3.5", ".625", true, "P40305-FP2K-A-3-4-6-F")]
        [InlineData(.5, "FP2K-B", "S", "C3x3.5", ".625", true, "P40305-FP2K-B-3-4-6-F")]
        [InlineData(.5, "FP3K-A", "S", "C3x3.5", ".625", true, "P40305-FP3K-A-3-4-6-F")]
        [InlineData(.5, "FP3K-B", "S", "C3x3.5", ".625", true, "P40305-FP3K-B-3-4-6-F")]
        [InlineData(.5, "FP3K-C", "S", "C3x3.5", ".625", true, "P40305-FP3K-C-3-4-6-F")]
        [InlineData(.5, "FP3K-D", "S", "C3x3.5", ".625", true, "P40305-FP3K-D-3-4-6-F")]
        [InlineData(.5, "FP4K-A", "S", "C3x3.5", ".625", true, "P40305-FP4K-A-3-4-6-F")]
        [InlineData(.5, "FP4K-B", "S", "C3x3.5", ".625", true, "P40305-FP4K-B-3-4-6-F")]
        [InlineData(.5, "FP5K-A", "S", "C3x3.5", ".625", true, "P40305-FP5K-A-3-4-6-F")]
        [InlineData(.5, "FP5K-B", "S", "C3x3.5", ".625", true, "P40305-FP5K-B-3-4-6-F")]
        [InlineData(.5, "FP5K-C", "S", "C3x3.5", ".625", true, "P40305-FP5K-C-3-4-6-F")]
        [InlineData(.5, "FP5K-D", "S", "C3x3.5", ".625", true, "P40305-FP5K-D-3-4-6-F")]
        [InlineData(.5, "FP6K-A", "S", "C3x3.5", ".625", true, "P40305-FP6K-A-3-4-6-F")]
        [InlineData(.5, "FP6K-B", "S", "C3x3.5", ".625", true, "P40305-FP6K-B-3-4-6-F")]
        [InlineData(.5, "FP1K-A", "S", "C4x4.5", ".625", true, "P40305-FP1K-A-4-5-6-F")]
        [InlineData(.5, "FP1K-B", "S", "C4x4.5", ".625", true, "P40305-FP1K-B-4-5-6-F")]
        [InlineData(.5, "FP1K-C", "S", "C4x4.5", ".625", true, "P40305-FP1K-C-4-5-6-F")]
        [InlineData(.5, "FP1K-D", "S", "C4x4.5", ".625", true, "P40305-FP1K-D-4-5-6-F")]
        [InlineData(.5, "FP2K-A", "S", "C4x4.5", ".625", true, "P40305-FP2K-A-4-5-6-F")]
        [InlineData(.5, "FP2K-B", "S", "C4x4.5", ".625", true, "P40305-FP2K-B-4-5-6-F")]
        [InlineData(.5, "FP3K-A", "S", "C4x4.5", ".625", true, "P40305-FP3K-A-4-5-6-F")]
        [InlineData(.5, "FP3K-B", "S", "C4x4.5", ".625", true, "P40305-FP3K-B-4-5-6-F")]
        [InlineData(.5, "FP3K-C", "S", "C4x4.5", ".625", true, "P40305-FP3K-C-4-5-6-F")]
        [InlineData(.5, "FP3K-D", "S", "C4x4.5", ".625", true, "P40305-FP3K-D-4-5-6-F")]
        [InlineData(.5, "FP4K-A", "S", "C4x4.5", ".625", true, "P40305-FP4K-A-4-5-6-F")]
        [InlineData(.5, "FP4K-B", "S", "C4x4.5", ".625", true, "P40305-FP4K-B-4-5-6-F")]
        [InlineData(.5, "FP5K-A", "S", "C4x4.5", ".625", true, "P40305-FP5K-A-4-5-6-F")]
        [InlineData(.5, "FP5K-B", "S", "C4x4.5", ".625", true, "P40305-FP5K-B-4-5-6-F")]
        [InlineData(.5, "FP5K-C", "S", "C4x4.5", ".625", true, "P40305-FP5K-C-4-5-6-F")]
        [InlineData(.5, "FP5K-D", "S", "C4x4.5", ".625", true, "P40305-FP5K-D-4-5-6-F")]
        [InlineData(.5, "FP6K-A", "S", "C4x4.5", ".625", true, "P40305-FP6K-A-4-5-6-F")]
        [InlineData(.5, "FP6K-B", "S", "C4x4.5", ".625", true, "P40305-FP6K-B-4-5-6-F")]
        [InlineData(.5, "FP1K-A", "S", "C5x6.7", ".625", true, "P40305-FP1K-A-5-6-6-F")]
        [InlineData(.5, "FP1K-B", "S", "C5x6.7", ".625", true, "P40305-FP1K-B-5-6-6-F")]
        [InlineData(.5, "FP1K-C", "S", "C5x6.7", ".625", true, "P40305-FP1K-C-5-6-6-F")]
        [InlineData(.5, "FP1K-D", "S", "C5x6.7", ".625", true, "P40305-FP1K-D-5-6-6-F")]
        [InlineData(.5, "FP2K-A", "S", "C5x6.7", ".625", true, "P40305-FP2K-A-5-6-6-F")]
        [InlineData(.5, "FP2K-B", "S", "C5x6.7", ".625", true, "P40305-FP2K-B-5-6-6-F")]
        [InlineData(.5, "FP3K-A", "S", "C5x6.7", ".625", true, "P40305-FP3K-A-5-6-6-F")]
        [InlineData(.5, "FP3K-B", "S", "C5x6.7", ".625", true, "P40305-FP3K-B-5-6-6-F")]
        [InlineData(.5, "FP3K-C", "S", "C5x6.7", ".625", true, "P40305-FP3K-C-5-6-6-F")]
        [InlineData(.5, "FP3K-D", "S", "C5x6.7", ".625", true, "P40305-FP3K-D-5-6-6-F")]
        [InlineData(.5, "FP4K-A", "S", "C5x6.7", ".625", true, "P40305-FP4K-A-5-6-6-F")]
        [InlineData(.5, "FP4K-B", "S", "C5x6.7", ".625", true, "P40305-FP4K-B-5-6-6-F")]
        [InlineData(.5, "FP5K-A", "S", "C5x6.7", ".625", true, "P40305-FP5K-A-5-6-6-F")]
        [InlineData(.5, "FP5K-B", "S", "C5x6.7", ".625", true, "P40305-FP5K-B-5-6-6-F")]
        [InlineData(.5, "FP5K-C", "S", "C5x6.7", ".625", true, "P40305-FP5K-C-5-6-6-F")]
        [InlineData(.5, "FP5K-D", "S", "C5x6.7", ".625", true, "P40305-FP5K-D-5-6-6-F")]
        [InlineData(.5, "FP6K-A", "S", "C5x6.7", ".625", true, "P40305-FP6K-A-5-6-6-F")]
        [InlineData(.5, "FP6K-B", "S", "C5x6.7", ".625", true, "P40305-FP6K-B-5-6-6-F")]
        [InlineData(.5, "FP1K-A", "S", "C3x3.5", ".75", true, "P40305-FP1K-A-3-4-7-F")]
        [InlineData(.5, "FP1K-B", "S", "C3x3.5", ".75", true, "P40305-FP1K-B-3-4-7-F")]
        [InlineData(.5, "FP1K-C", "S", "C3x3.5", ".75", true, "P40305-FP1K-C-3-4-7-F")]
        [InlineData(.5, "FP1K-D", "S", "C3x3.5", ".75", true, "P40305-FP1K-D-3-4-7-F")]
        [InlineData(.5, "FP2K-A", "S", "C3x3.5", ".75", true, "P40305-FP2K-A-3-4-7-F")]
        [InlineData(.5, "FP2K-B", "S", "C3x3.5", ".75", true, "P40305-FP2K-B-3-4-7-F")]
        [InlineData(.5, "FP3K-A", "S", "C3x3.5", ".75", true, "P40305-FP3K-A-3-4-7-F")]
        [InlineData(.5, "FP3K-B", "S", "C3x3.5", ".75", true, "P40305-FP3K-B-3-4-7-F")]
        [InlineData(.5, "FP3K-C", "S", "C3x3.5", ".75", true, "P40305-FP3K-C-3-4-7-F")]
        [InlineData(.5, "FP3K-D", "S", "C3x3.5", ".75", true, "P40305-FP3K-D-3-4-7-F")]
        [InlineData(.5, "FP4K-A", "S", "C3x3.5", ".75", true, "P40305-FP4K-A-3-4-7-F")]
        [InlineData(.5, "FP4K-B", "S", "C3x3.5", ".75", true, "P40305-FP4K-B-3-4-7-F")]
        [InlineData(.5, "FP5K-A", "S", "C3x3.5", ".75", true, "P40305-FP5K-A-3-4-7-F")]
        [InlineData(.5, "FP5K-B", "S", "C3x3.5", ".75", true, "P40305-FP5K-B-3-4-7-F")]
        [InlineData(.5, "FP5K-C", "S", "C3x3.5", ".75", true, "P40305-FP5K-C-3-4-7-F")]
        [InlineData(.5, "FP5K-D", "S", "C3x3.5", ".75", true, "P40305-FP5K-D-3-4-7-F")]
        [InlineData(.5, "FP6K-A", "S", "C3x3.5", ".75", true, "P40305-FP6K-A-3-4-7-F")]
        [InlineData(.5, "FP6K-B", "S", "C3x3.5", ".75", true, "P40305-FP6K-B-3-4-7-F")]
        [InlineData(.5, "FP1K-A", "S", "C4x4.5", ".75", true, "P40305-FP1K-A-4-5-7-F")]
        [InlineData(.5, "FP1K-B", "S", "C4x4.5", ".75", true, "P40305-FP1K-B-4-5-7-F")]
        [InlineData(.5, "FP1K-C", "S", "C4x4.5", ".75", true, "P40305-FP1K-C-4-5-7-F")]
        [InlineData(.5, "FP1K-D", "S", "C4x4.5", ".75", true, "P40305-FP1K-D-4-5-7-F")]
        [InlineData(.5, "FP2K-A", "S", "C4x4.5", ".75", true, "P40305-FP2K-A-4-5-7-F")]
        [InlineData(.5, "FP2K-B", "S", "C4x4.5", ".75", true, "P40305-FP2K-B-4-5-7-F")]
        [InlineData(.5, "FP3K-A", "S", "C4x4.5", ".75", true, "P40305-FP3K-A-4-5-7-F")]
        [InlineData(.5, "FP3K-B", "S", "C4x4.5", ".75", true, "P40305-FP3K-B-4-5-7-F")]
        [InlineData(.5, "FP3K-C", "S", "C4x4.5", ".75", true, "P40305-FP3K-C-4-5-7-F")]
        [InlineData(.5, "FP3K-D", "S", "C4x4.5", ".75", true, "P40305-FP3K-D-4-5-7-F")]
        [InlineData(.5, "FP4K-A", "S", "C4x4.5", ".75", true, "P40305-FP4K-A-4-5-7-F")]
        [InlineData(.5, "FP4K-B", "S", "C4x4.5", ".75", true, "P40305-FP4K-B-4-5-7-F")]
        [InlineData(.5, "FP5K-A", "S", "C4x4.5", ".75", true, "P40305-FP5K-A-4-5-7-F")]
        [InlineData(.5, "FP5K-B", "S", "C4x4.5", ".75", true, "P40305-FP5K-B-4-5-7-F")]
        [InlineData(.5, "FP5K-C", "S", "C4x4.5", ".75", true, "P40305-FP5K-C-4-5-7-F")]
        [InlineData(.5, "FP5K-D", "S", "C4x4.5", ".75", true, "P40305-FP5K-D-4-5-7-F")]
        [InlineData(.5, "FP6K-A", "S", "C4x4.5", ".75", true, "P40305-FP6K-A-4-5-7-F")]
        [InlineData(.5, "FP6K-B", "S", "C4x4.5", ".75", true, "P40305-FP6K-B-4-5-7-F")]
        [InlineData(.5, "FP1K-A", "S", "C5x6.7", ".75", true, "P40305-FP1K-A-5-6-7-F")]
        [InlineData(.5, "FP1K-B", "S", "C5x6.7", ".75", true, "P40305-FP1K-B-5-6-7-F")]
        [InlineData(.5, "FP1K-C", "S", "C5x6.7", ".75", true, "P40305-FP1K-C-5-6-7-F")]
        [InlineData(.5, "FP1K-D", "S", "C5x6.7", ".75", true, "P40305-FP1K-D-5-6-7-F")]
        [InlineData(.5, "FP2K-A", "S", "C5x6.7", ".75", true, "P40305-FP2K-A-5-6-7-F")]
        [InlineData(.5, "FP2K-B", "S", "C5x6.7", ".75", true, "P40305-FP2K-B-5-6-7-F")]
        [InlineData(.5, "FP3K-A", "S", "C5x6.7", ".75", true, "P40305-FP3K-A-5-6-7-F")]
        [InlineData(.5, "FP3K-B", "S", "C5x6.7", ".75", true, "P40305-FP3K-B-5-6-7-F")]
        [InlineData(.5, "FP3K-C", "S", "C5x6.7", ".75", true, "P40305-FP3K-C-5-6-7-F")]
        [InlineData(.5, "FP3K-D", "S", "C5x6.7", ".75", true, "P40305-FP3K-D-5-6-7-F")]
        [InlineData(.5, "FP4K-A", "S", "C5x6.7", ".75", true, "P40305-FP4K-A-5-6-7-F")]
        [InlineData(.5, "FP4K-B", "S", "C5x6.7", ".75", true, "P40305-FP4K-B-5-6-7-F")]
        [InlineData(.5, "FP5K-A", "S", "C5x6.7", ".75", true, "P40305-FP5K-A-5-6-7-F")]
        [InlineData(.5, "FP5K-B", "S", "C5x6.7", ".75", true, "P40305-FP5K-B-5-6-7-F")]
        [InlineData(.5, "FP5K-C", "S", "C5x6.7", ".75", true, "P40305-FP5K-C-5-6-7-F")]
        [InlineData(.5, "FP5K-D", "S", "C5x6.7", ".75", true, "P40305-FP5K-D-5-6-7-F")]
        [InlineData(.5, "FP6K-A", "S", "C5x6.7", ".75", true, "P40305-FP6K-A-5-6-7-F")]
        [InlineData(.5, "FP6K-B", "S", "C5x6.7", ".75", true, "P40305-FP6K-B-5-6-7-F")]
        //5/8in plate thickness
        [InlineData(.625, "FP1K-A", "S", "C3x3.5", ".5", true, "P40306-FP1K-A-3-4-5-F")]
        [InlineData(.625, "FP1K-B", "S", "C3x3.5", ".5", true, "P40306-FP1K-B-3-4-5-F")]
        [InlineData(.625, "FP1K-C", "S", "C3x3.5", ".5", true, "P40306-FP1K-C-3-4-5-F")]
        [InlineData(.625, "FP1K-D", "S", "C3x3.5", ".5", true, "P40306-FP1K-D-3-4-5-F")]
        [InlineData(.625, "FP2K-A", "S", "C3x3.5", ".5", true, "P40306-FP2K-A-3-4-5-F")]
        [InlineData(.625, "FP2K-B", "S", "C3x3.5", ".5", true, "P40306-FP2K-B-3-4-5-F")]
        [InlineData(.625, "FP3K-A", "S", "C3x3.5", ".5", true, "P40306-FP3K-A-3-4-5-F")]
        [InlineData(.625, "FP3K-B", "S", "C3x3.5", ".5", true, "P40306-FP3K-B-3-4-5-F")]
        [InlineData(.625, "FP3K-C", "S", "C3x3.5", ".5", true, "P40306-FP3K-C-3-4-5-F")]
        [InlineData(.625, "FP3K-D", "S", "C3x3.5", ".5", true, "P40306-FP3K-D-3-4-5-F")]
        [InlineData(.625, "FP4K-A", "S", "C3x3.5", ".5", true, "P40306-FP4K-A-3-4-5-F")]
        [InlineData(.625, "FP4K-B", "S", "C3x3.5", ".5", true, "P40306-FP4K-B-3-4-5-F")]
        [InlineData(.625, "FP5K-A", "S", "C3x3.5", ".5", true, "P40306-FP5K-A-3-4-5-F")]
        [InlineData(.625, "FP5K-B", "S", "C3x3.5", ".5", true, "P40306-FP5K-B-3-4-5-F")]
        [InlineData(.625, "FP5K-C", "S", "C3x3.5", ".5", true, "P40306-FP5K-C-3-4-5-F")]
        [InlineData(.625, "FP5K-D", "S", "C3x3.5", ".5", true, "P40306-FP5K-D-3-4-5-F")]
        [InlineData(.625, "FP6K-A", "S", "C3x3.5", ".5", true, "P40306-FP6K-A-3-4-5-F")]
        [InlineData(.625, "FP6K-B", "S", "C3x3.5", ".5", true, "P40306-FP6K-B-3-4-5-F")]
        [InlineData(.625, "FP1K-A", "S", "C4x4.5", ".5", true, "P40306-FP1K-A-4-5-5-F")]
        [InlineData(.625, "FP1K-B", "S", "C4x4.5", ".5", true, "P40306-FP1K-B-4-5-5-F")]
        [InlineData(.625, "FP1K-C", "S", "C4x4.5", ".5", true, "P40306-FP1K-C-4-5-5-F")]
        [InlineData(.625, "FP1K-D", "S", "C4x4.5", ".5", true, "P40306-FP1K-D-4-5-5-F")]
        [InlineData(.625, "FP2K-A", "S", "C4x4.5", ".5", true, "P40306-FP2K-A-4-5-5-F")]
        [InlineData(.625, "FP2K-B", "S", "C4x4.5", ".5", true, "P40306-FP2K-B-4-5-5-F")]
        [InlineData(.625, "FP3K-A", "S", "C4x4.5", ".5", true, "P40306-FP3K-A-4-5-5-F")]
        [InlineData(.625, "FP3K-B", "S", "C4x4.5", ".5", true, "P40306-FP3K-B-4-5-5-F")]
        [InlineData(.625, "FP3K-C", "S", "C4x4.5", ".5", true, "P40306-FP3K-C-4-5-5-F")]
        [InlineData(.625, "FP3K-D", "S", "C4x4.5", ".5", true, "P40306-FP3K-D-4-5-5-F")]
        [InlineData(.625, "FP4K-A", "S", "C4x4.5", ".5", true, "P40306-FP4K-A-4-5-5-F")]
        [InlineData(.625, "FP4K-B", "S", "C4x4.5", ".5", true, "P40306-FP4K-B-4-5-5-F")]
        [InlineData(.625, "FP5K-A", "S", "C4x4.5", ".5", true, "P40306-FP5K-A-4-5-5-F")]
        [InlineData(.625, "FP5K-B", "S", "C4x4.5", ".5", true, "P40306-FP5K-B-4-5-5-F")]
        [InlineData(.625, "FP5K-C", "S", "C4x4.5", ".5", true, "P40306-FP5K-C-4-5-5-F")]
        [InlineData(.625, "FP5K-D", "S", "C4x4.5", ".5", true, "P40306-FP5K-D-4-5-5-F")]
        [InlineData(.625, "FP6K-A", "S", "C4x4.5", ".5", true, "P40306-FP6K-A-4-5-5-F")]
        [InlineData(.625, "FP6K-B", "S", "C4x4.5", ".5", true, "P40306-FP6K-B-4-5-5-F")]
        [InlineData(.625, "FP1K-A", "S", "C5x6.7", ".5", true, "P40306-FP1K-A-5-6-5-F")]
        [InlineData(.625, "FP1K-B", "S", "C5x6.7", ".5", true, "P40306-FP1K-B-5-6-5-F")]
        [InlineData(.625, "FP1K-C", "S", "C5x6.7", ".5", true, "P40306-FP1K-C-5-6-5-F")]
        [InlineData(.625, "FP1K-D", "S", "C5x6.7", ".5", true, "P40306-FP1K-D-5-6-5-F")]
        [InlineData(.625, "FP2K-A", "S", "C5x6.7", ".5", true, "P40306-FP2K-A-5-6-5-F")]
        [InlineData(.625, "FP2K-B", "S", "C5x6.7", ".5", true, "P40306-FP2K-B-5-6-5-F")]
        [InlineData(.625, "FP3K-A", "S", "C5x6.7", ".5", true, "P40306-FP3K-A-5-6-5-F")]
        [InlineData(.625, "FP3K-B", "S", "C5x6.7", ".5", true, "P40306-FP3K-B-5-6-5-F")]
        [InlineData(.625, "FP3K-C", "S", "C5x6.7", ".5", true, "P40306-FP3K-C-5-6-5-F")]
        [InlineData(.625, "FP3K-D", "S", "C5x6.7", ".5", true, "P40306-FP3K-D-5-6-5-F")]
        [InlineData(.625, "FP4K-A", "S", "C5x6.7", ".5", true, "P40306-FP4K-A-5-6-5-F")]
        [InlineData(.625, "FP4K-B", "S", "C5x6.7", ".5", true, "P40306-FP4K-B-5-6-5-F")]
        [InlineData(.625, "FP5K-A", "S", "C5x6.7", ".5", true, "P40306-FP5K-A-5-6-5-F")]
        [InlineData(.625, "FP5K-B", "S", "C5x6.7", ".5", true, "P40306-FP5K-B-5-6-5-F")]
        [InlineData(.625, "FP5K-C", "S", "C5x6.7", ".5", true, "P40306-FP5K-C-5-6-5-F")]
        [InlineData(.625, "FP5K-D", "S", "C5x6.7", ".5", true, "P40306-FP5K-D-5-6-5-F")]
        [InlineData(.625, "FP6K-A", "S", "C5x6.7", ".5", true, "P40306-FP6K-A-5-6-5-F")]
        [InlineData(.625, "FP6K-B", "S", "C5x6.7", ".5", true, "P40306-FP6K-B-5-6-5-F")]
        [InlineData(.625, "FP1K-A", "S", "C3x3.5", ".625", true, "P40306-FP1K-A-3-4-6-F")]
        [InlineData(.625, "FP1K-B", "S", "C3x3.5", ".625", true, "P40306-FP1K-B-3-4-6-F")]
        [InlineData(.625, "FP1K-C", "S", "C3x3.5", ".625", true, "P40306-FP1K-C-3-4-6-F")]
        [InlineData(.625, "FP1K-D", "S", "C3x3.5", ".625", true, "P40306-FP1K-D-3-4-6-F")]
        [InlineData(.625, "FP2K-A", "S", "C3x3.5", ".625", true, "P40306-FP2K-A-3-4-6-F")]
        [InlineData(.625, "FP2K-B", "S", "C3x3.5", ".625", true, "P40306-FP2K-B-3-4-6-F")]
        [InlineData(.625, "FP3K-A", "S", "C3x3.5", ".625", true, "P40306-FP3K-A-3-4-6-F")]
        [InlineData(.625, "FP3K-B", "S", "C3x3.5", ".625", true, "P40306-FP3K-B-3-4-6-F")]
        [InlineData(.625, "FP3K-C", "S", "C3x3.5", ".625", true, "P40306-FP3K-C-3-4-6-F")]
        [InlineData(.625, "FP3K-D", "S", "C3x3.5", ".625", true, "P40306-FP3K-D-3-4-6-F")]
        [InlineData(.625, "FP4K-A", "S", "C3x3.5", ".625", true, "P40306-FP4K-A-3-4-6-F")]
        [InlineData(.625, "FP4K-B", "S", "C3x3.5", ".625", true, "P40306-FP4K-B-3-4-6-F")]
        [InlineData(.625, "FP5K-A", "S", "C3x3.5", ".625", true, "P40306-FP5K-A-3-4-6-F")]
        [InlineData(.625, "FP5K-B", "S", "C3x3.5", ".625", true, "P40306-FP5K-B-3-4-6-F")]
        [InlineData(.625, "FP5K-C", "S", "C3x3.5", ".625", true, "P40306-FP5K-C-3-4-6-F")]
        [InlineData(.625, "FP5K-D", "S", "C3x3.5", ".625", true, "P40306-FP5K-D-3-4-6-F")]
        [InlineData(.625, "FP6K-A", "S", "C3x3.5", ".625", true, "P40306-FP6K-A-3-4-6-F")]
        [InlineData(.625, "FP6K-B", "S", "C3x3.5", ".625", true, "P40306-FP6K-B-3-4-6-F")]
        [InlineData(.625, "FP1K-A", "S", "C4x4.5", ".625", true, "P40306-FP1K-A-4-5-6-F")]
        [InlineData(.625, "FP1K-B", "S", "C4x4.5", ".625", true, "P40306-FP1K-B-4-5-6-F")]
        [InlineData(.625, "FP1K-C", "S", "C4x4.5", ".625", true, "P40306-FP1K-C-4-5-6-F")]
        [InlineData(.625, "FP1K-D", "S", "C4x4.5", ".625", true, "P40306-FP1K-D-4-5-6-F")]
        [InlineData(.625, "FP2K-A", "S", "C4x4.5", ".625", true, "P40306-FP2K-A-4-5-6-F")]
        [InlineData(.625, "FP2K-B", "S", "C4x4.5", ".625", true, "P40306-FP2K-B-4-5-6-F")]
        [InlineData(.625, "FP3K-A", "S", "C4x4.5", ".625", true, "P40306-FP3K-A-4-5-6-F")]
        [InlineData(.625, "FP3K-B", "S", "C4x4.5", ".625", true, "P40306-FP3K-B-4-5-6-F")]
        [InlineData(.625, "FP3K-C", "S", "C4x4.5", ".625", true, "P40306-FP3K-C-4-5-6-F")]
        [InlineData(.625, "FP3K-D", "S", "C4x4.5", ".625", true, "P40306-FP3K-D-4-5-6-F")]
        [InlineData(.625, "FP4K-A", "S", "C4x4.5", ".625", true, "P40306-FP4K-A-4-5-6-F")]
        [InlineData(.625, "FP4K-B", "S", "C4x4.5", ".625", true, "P40306-FP4K-B-4-5-6-F")]
        [InlineData(.625, "FP5K-A", "S", "C4x4.5", ".625", true, "P40306-FP5K-A-4-5-6-F")]
        [InlineData(.625, "FP5K-B", "S", "C4x4.5", ".625", true, "P40306-FP5K-B-4-5-6-F")]
        [InlineData(.625, "FP5K-C", "S", "C4x4.5", ".625", true, "P40306-FP5K-C-4-5-6-F")]
        [InlineData(.625, "FP5K-D", "S", "C4x4.5", ".625", true, "P40306-FP5K-D-4-5-6-F")]
        [InlineData(.625, "FP6K-A", "S", "C4x4.5", ".625", true, "P40306-FP6K-A-4-5-6-F")]
        [InlineData(.625, "FP6K-B", "S", "C4x4.5", ".625", true, "P40306-FP6K-B-4-5-6-F")]
        [InlineData(.625, "FP1K-A", "S", "C5x6.7", ".625", true, "P40306-FP1K-A-5-6-6-F")]
        [InlineData(.625, "FP1K-B", "S", "C5x6.7", ".625", true, "P40306-FP1K-B-5-6-6-F")]
        [InlineData(.625, "FP1K-C", "S", "C5x6.7", ".625", true, "P40306-FP1K-C-5-6-6-F")]
        [InlineData(.625, "FP1K-D", "S", "C5x6.7", ".625", true, "P40306-FP1K-D-5-6-6-F")]
        [InlineData(.625, "FP2K-A", "S", "C5x6.7", ".625", true, "P40306-FP2K-A-5-6-6-F")]
        [InlineData(.625, "FP2K-B", "S", "C5x6.7", ".625", true, "P40306-FP2K-B-5-6-6-F")]
        [InlineData(.625, "FP3K-A", "S", "C5x6.7", ".625", true, "P40306-FP3K-A-5-6-6-F")]
        [InlineData(.625, "FP3K-B", "S", "C5x6.7", ".625", true, "P40306-FP3K-B-5-6-6-F")]
        [InlineData(.625, "FP3K-C", "S", "C5x6.7", ".625", true, "P40306-FP3K-C-5-6-6-F")]
        [InlineData(.625, "FP3K-D", "S", "C5x6.7", ".625", true, "P40306-FP3K-D-5-6-6-F")]
        [InlineData(.625, "FP4K-A", "S", "C5x6.7", ".625", true, "P40306-FP4K-A-5-6-6-F")]
        [InlineData(.625, "FP4K-B", "S", "C5x6.7", ".625", true, "P40306-FP4K-B-5-6-6-F")]
        [InlineData(.625, "FP5K-A", "S", "C5x6.7", ".625", true, "P40306-FP5K-A-5-6-6-F")]
        [InlineData(.625, "FP5K-B", "S", "C5x6.7", ".625", true, "P40306-FP5K-B-5-6-6-F")]
        [InlineData(.625, "FP5K-C", "S", "C5x6.7", ".625", true, "P40306-FP5K-C-5-6-6-F")]
        [InlineData(.625, "FP5K-D", "S", "C5x6.7", ".625", true, "P40306-FP5K-D-5-6-6-F")]
        [InlineData(.625, "FP6K-A", "S", "C5x6.7", ".625", true, "P40306-FP6K-A-5-6-6-F")]
        [InlineData(.625, "FP6K-B", "S", "C5x6.7", ".625", true, "P40306-FP6K-B-5-6-6-F")]
        [InlineData(.625, "FP1K-A", "S", "C3x3.5", ".75", true, "P40306-FP1K-A-3-4-7-F")]
        [InlineData(.625, "FP1K-B", "S", "C3x3.5", ".75", true, "P40306-FP1K-B-3-4-7-F")]
        [InlineData(.625, "FP1K-C", "S", "C3x3.5", ".75", true, "P40306-FP1K-C-3-4-7-F")]
        [InlineData(.625, "FP1K-D", "S", "C3x3.5", ".75", true, "P40306-FP1K-D-3-4-7-F")]
        [InlineData(.625, "FP2K-A", "S", "C3x3.5", ".75", true, "P40306-FP2K-A-3-4-7-F")]
        [InlineData(.625, "FP2K-B", "S", "C3x3.5", ".75", true, "P40306-FP2K-B-3-4-7-F")]
        [InlineData(.625, "FP3K-A", "S", "C3x3.5", ".75", true, "P40306-FP3K-A-3-4-7-F")]
        [InlineData(.625, "FP3K-B", "S", "C3x3.5", ".75", true, "P40306-FP3K-B-3-4-7-F")]
        [InlineData(.625, "FP3K-C", "S", "C3x3.5", ".75", true, "P40306-FP3K-C-3-4-7-F")]
        [InlineData(.625, "FP3K-D", "S", "C3x3.5", ".75", true, "P40306-FP3K-D-3-4-7-F")]
        [InlineData(.625, "FP4K-A", "S", "C3x3.5", ".75", true, "P40306-FP4K-A-3-4-7-F")]
        [InlineData(.625, "FP4K-B", "S", "C3x3.5", ".75", true, "P40306-FP4K-B-3-4-7-F")]
        [InlineData(.625, "FP5K-A", "S", "C3x3.5", ".75", true, "P40306-FP5K-A-3-4-7-F")]
        [InlineData(.625, "FP5K-B", "S", "C3x3.5", ".75", true, "P40306-FP5K-B-3-4-7-F")]
        [InlineData(.625, "FP5K-C", "S", "C3x3.5", ".75", true, "P40306-FP5K-C-3-4-7-F")]
        [InlineData(.625, "FP5K-D", "S", "C3x3.5", ".75", true, "P40306-FP5K-D-3-4-7-F")]
        [InlineData(.625, "FP6K-A", "S", "C3x3.5", ".75", true, "P40306-FP6K-A-3-4-7-F")]
        [InlineData(.625, "FP6K-B", "S", "C3x3.5", ".75", true, "P40306-FP6K-B-3-4-7-F")]
        [InlineData(.625, "FP1K-A", "S", "C4x4.5", ".75", true, "P40306-FP1K-A-4-5-7-F")]
        [InlineData(.625, "FP1K-B", "S", "C4x4.5", ".75", true, "P40306-FP1K-B-4-5-7-F")]
        [InlineData(.625, "FP1K-C", "S", "C4x4.5", ".75", true, "P40306-FP1K-C-4-5-7-F")]
        [InlineData(.625, "FP1K-D", "S", "C4x4.5", ".75", true, "P40306-FP1K-D-4-5-7-F")]
        [InlineData(.625, "FP2K-A", "S", "C4x4.5", ".75", true, "P40306-FP2K-A-4-5-7-F")]
        [InlineData(.625, "FP2K-B", "S", "C4x4.5", ".75", true, "P40306-FP2K-B-4-5-7-F")]
        [InlineData(.625, "FP3K-A", "S", "C4x4.5", ".75", true, "P40306-FP3K-A-4-5-7-F")]
        [InlineData(.625, "FP3K-B", "S", "C4x4.5", ".75", true, "P40306-FP3K-B-4-5-7-F")]
        [InlineData(.625, "FP3K-C", "S", "C4x4.5", ".75", true, "P40306-FP3K-C-4-5-7-F")]
        [InlineData(.625, "FP3K-D", "S", "C4x4.5", ".75", true, "P40306-FP3K-D-4-5-7-F")]
        [InlineData(.625, "FP4K-A", "S", "C4x4.5", ".75", true, "P40306-FP4K-A-4-5-7-F")]
        [InlineData(.625, "FP4K-B", "S", "C4x4.5", ".75", true, "P40306-FP4K-B-4-5-7-F")]
        [InlineData(.625, "FP5K-A", "S", "C4x4.5", ".75", true, "P40306-FP5K-A-4-5-7-F")]
        [InlineData(.625, "FP5K-B", "S", "C4x4.5", ".75", true, "P40306-FP5K-B-4-5-7-F")]
        [InlineData(.625, "FP5K-C", "S", "C4x4.5", ".75", true, "P40306-FP5K-C-4-5-7-F")]
        [InlineData(.625, "FP5K-D", "S", "C4x4.5", ".75", true, "P40306-FP5K-D-4-5-7-F")]
        [InlineData(.625, "FP6K-A", "S", "C4x4.5", ".75", true, "P40306-FP6K-A-4-5-7-F")]
        [InlineData(.625, "FP6K-B", "S", "C4x4.5", ".75", true, "P40306-FP6K-B-4-5-7-F")]
        [InlineData(.625, "FP1K-A", "S", "C5x6.7", ".75", true, "P40306-FP1K-A-5-6-7-F")]
        [InlineData(.625, "FP1K-B", "S", "C5x6.7", ".75", true, "P40306-FP1K-B-5-6-7-F")]
        [InlineData(.625, "FP1K-C", "S", "C5x6.7", ".75", true, "P40306-FP1K-C-5-6-7-F")]
        [InlineData(.625, "FP1K-D", "S", "C5x6.7", ".75", true, "P40306-FP1K-D-5-6-7-F")]
        [InlineData(.625, "FP2K-A", "S", "C5x6.7", ".75", true, "P40306-FP2K-A-5-6-7-F")]
        [InlineData(.625, "FP2K-B", "S", "C5x6.7", ".75", true, "P40306-FP2K-B-5-6-7-F")]
        [InlineData(.625, "FP3K-A", "S", "C5x6.7", ".75", true, "P40306-FP3K-A-5-6-7-F")]
        [InlineData(.625, "FP3K-B", "S", "C5x6.7", ".75", true, "P40306-FP3K-B-5-6-7-F")]
        [InlineData(.625, "FP3K-C", "S", "C5x6.7", ".75", true, "P40306-FP3K-C-5-6-7-F")]
        [InlineData(.625, "FP3K-D", "S", "C5x6.7", ".75", true, "P40306-FP3K-D-5-6-7-F")]
        [InlineData(.625, "FP4K-A", "S", "C5x6.7", ".75", true, "P40306-FP4K-A-5-6-7-F")]
        [InlineData(.625, "FP4K-B", "S", "C5x6.7", ".75", true, "P40306-FP4K-B-5-6-7-F")]
        [InlineData(.625, "FP5K-A", "S", "C5x6.7", ".75", true, "P40306-FP5K-A-5-6-7-F")]
        [InlineData(.625, "FP5K-B", "S", "C5x6.7", ".75", true, "P40306-FP5K-B-5-6-7-F")]
        [InlineData(.625, "FP5K-C", "S", "C5x6.7", ".75", true, "P40306-FP5K-C-5-6-7-F")]
        [InlineData(.625, "FP5K-D", "S", "C5x6.7", ".75", true, "P40306-FP5K-D-5-6-7-F")]
        [InlineData(.625, "FP6K-A", "S", "C5x6.7", ".75", true, "P40306-FP6K-A-5-6-7-F")]
        [InlineData(.625, "FP6K-B", "S", "C5x6.7", ".75", true, "P40306-FP6K-B-5-6-7-F")]
        //change upright type
        [InlineData(.375, "FP1K-A", "SL", "C3x3.5", ".5", true, "P40304-FP1K-A-3-5-5-F")]
        [InlineData(.375, "FP1K-B", "SL", "C3x3.5", ".5", true, "P40304-FP1K-B-3-5-5-F")]
        [InlineData(.375, "FP1K-C", "SL", "C3x3.5", ".5", true, "P40304-FP1K-C-3-5-5-F")]
        [InlineData(.375, "FP1K-D", "SL", "C3x3.5", ".5", true, "P40304-FP1K-D-3-5-5-F")]
        [InlineData(.375, "FP2K-A", "SL", "C3x3.5", ".5", true, "P40304-FP2K-A-3-5-5-F")]
        [InlineData(.375, "FP2K-B", "SL", "C3x3.5", ".5", true, "P40304-FP2K-B-3-5-5-F")]
        [InlineData(.375, "FP3K-A", "SL", "C3x3.5", ".5", true, "P40304-FP3K-A-3-5-5-F")]
        [InlineData(.375, "FP3K-B", "SL", "C3x3.5", ".5", true, "P40304-FP3K-B-3-5-5-F")]
        [InlineData(.375, "FP3K-C", "SL", "C3x3.5", ".5", true, "P40304-FP3K-C-3-5-5-F")]
        [InlineData(.375, "FP3K-D", "SL", "C3x3.5", ".5", true, "P40304-FP3K-D-3-5-5-F")]
        [InlineData(.375, "FP4K-A", "SL", "C3x3.5", ".5", true, "P40304-FP4K-A-3-5-5-F")]
        [InlineData(.375, "FP4K-B", "SL", "C3x3.5", ".5", true, "P40304-FP4K-B-3-5-5-F")]
        [InlineData(.375, "FP5K-A", "SL", "C3x3.5", ".5", true, "P40304-FP5K-A-3-5-5-F")]
        [InlineData(.375, "FP5K-B", "SL", "C3x3.5", ".5", true, "P40304-FP5K-B-3-5-5-F")]
        [InlineData(.375, "FP5K-C", "SL", "C3x3.5", ".5", true, "P40304-FP5K-C-3-5-5-F")]
        [InlineData(.375, "FP5K-D", "SL", "C3x3.5", ".5", true, "P40304-FP5K-D-3-5-5-F")]
        [InlineData(.375, "FP6K-A", "SL", "C3x3.5", ".5", true, "P40304-FP6K-A-3-5-5-F")]
        [InlineData(.375, "FP6K-B", "SL", "C3x3.5", ".5", true, "P40304-FP6K-B-3-5-5-F")]
        [InlineData(.375, "FP1K-A", "SL", "C4x4.5", ".5", true, "P40304-FP1K-A-4-6-5-F")]
        [InlineData(.375, "FP1K-B", "SL", "C4x4.5", ".5", true, "P40304-FP1K-B-4-6-5-F")]
        [InlineData(.375, "FP1K-C", "SL", "C4x4.5", ".5", true, "P40304-FP1K-C-4-6-5-F")]
        [InlineData(.375, "FP1K-D", "SL", "C4x4.5", ".5", true, "P40304-FP1K-D-4-6-5-F")]
        [InlineData(.375, "FP2K-A", "SL", "C4x4.5", ".5", true, "P40304-FP2K-A-4-6-5-F")]
        [InlineData(.375, "FP2K-B", "SL", "C4x4.5", ".5", true, "P40304-FP2K-B-4-6-5-F")]
        [InlineData(.375, "FP3K-A", "SL", "C4x4.5", ".5", true, "P40304-FP3K-A-4-6-5-F")]
        [InlineData(.375, "FP3K-B", "SL", "C4x4.5", ".5", true, "P40304-FP3K-B-4-6-5-F")]
        [InlineData(.375, "FP3K-C", "SL", "C4x4.5", ".5", true, "P40304-FP3K-C-4-6-5-F")]
        [InlineData(.375, "FP3K-D", "SL", "C4x4.5", ".5", true, "P40304-FP3K-D-4-6-5-F")]
        [InlineData(.375, "FP4K-A", "SL", "C4x4.5", ".5", true, "P40304-FP4K-A-4-6-5-F")]
        [InlineData(.375, "FP4K-B", "SL", "C4x4.5", ".5", true, "P40304-FP4K-B-4-6-5-F")]
        [InlineData(.375, "FP5K-A", "SL", "C4x4.5", ".5", true, "P40304-FP5K-A-4-6-5-F")]
        [InlineData(.375, "FP5K-B", "SL", "C4x4.5", ".5", true, "P40304-FP5K-B-4-6-5-F")]
        [InlineData(.375, "FP5K-C", "SL", "C4x4.5", ".5", true, "P40304-FP5K-C-4-6-5-F")]
        [InlineData(.375, "FP5K-D", "SL", "C4x4.5", ".5", true, "P40304-FP5K-D-4-6-5-F")]
        [InlineData(.375, "FP6K-A", "SL", "C4x4.5", ".5", true, "P40304-FP6K-A-4-6-5-F")]
        [InlineData(.375, "FP6K-B", "SL", "C4x4.5", ".5", true, "P40304-FP6K-B-4-6-5-F")]
        [InlineData(.375, "FP1K-A", "SL", "C5x6.7", ".5", true, "P40304-FP1K-A-5-7-5-F")]
        [InlineData(.375, "FP1K-B", "SL", "C5x6.7", ".5", true, "P40304-FP1K-B-5-7-5-F")]
        [InlineData(.375, "FP1K-C", "SL", "C5x6.7", ".5", true, "P40304-FP1K-C-5-7-5-F")]
        [InlineData(.375, "FP1K-D", "SL", "C5x6.7", ".5", true, "P40304-FP1K-D-5-7-5-F")]
        [InlineData(.375, "FP2K-A", "SL", "C5x6.7", ".5", true, "P40304-FP2K-A-5-7-5-F")]
        [InlineData(.375, "FP2K-B", "SL", "C5x6.7", ".5", true, "P40304-FP2K-B-5-7-5-F")]
        [InlineData(.375, "FP3K-A", "SL", "C5x6.7", ".5", true, "P40304-FP3K-A-5-7-5-F")]
        [InlineData(.375, "FP3K-B", "SL", "C5x6.7", ".5", true, "P40304-FP3K-B-5-7-5-F")]
        [InlineData(.375, "FP3K-C", "SL", "C5x6.7", ".5", true, "P40304-FP3K-C-5-7-5-F")]
        [InlineData(.375, "FP3K-D", "SL", "C5x6.7", ".5", true, "P40304-FP3K-D-5-7-5-F")]
        [InlineData(.375, "FP4K-A", "SL", "C5x6.7", ".5", true, "P40304-FP4K-A-5-7-5-F")]
        [InlineData(.375, "FP4K-B", "SL", "C5x6.7", ".5", true, "P40304-FP4K-B-5-7-5-F")]
        [InlineData(.375, "FP5K-A", "SL", "C5x6.7", ".5", true, "P40304-FP5K-A-5-7-5-F")]
        [InlineData(.375, "FP5K-B", "SL", "C5x6.7", ".5", true, "P40304-FP5K-B-5-7-5-F")]
        [InlineData(.375, "FP5K-C", "SL", "C5x6.7", ".5", true, "P40304-FP5K-C-5-7-5-F")]
        [InlineData(.375, "FP5K-D", "SL", "C5x6.7", ".5", true, "P40304-FP5K-D-5-7-5-F")]
        [InlineData(.375, "FP6K-A", "SL", "C5x6.7", ".5", true, "P40304-FP6K-A-5-7-5-F")]
        [InlineData(.375, "FP6K-B", "SL", "C5x6.7", ".5", true, "P40304-FP6K-B-5-7-5-F")]
        [InlineData(.375, "FP1K-A", "SL", "C3x3.5", ".625", true, "P40304-FP1K-A-3-5-6-F")]
        [InlineData(.375, "FP1K-B", "SL", "C3x3.5", ".625", true, "P40304-FP1K-B-3-5-6-F")]
        [InlineData(.375, "FP1K-C", "SL", "C3x3.5", ".625", true, "P40304-FP1K-C-3-5-6-F")]
        [InlineData(.375, "FP1K-D", "SL", "C3x3.5", ".625", true, "P40304-FP1K-D-3-5-6-F")]
        [InlineData(.375, "FP2K-A", "SL", "C3x3.5", ".625", true, "P40304-FP2K-A-3-5-6-F")]
        [InlineData(.375, "FP2K-B", "SL", "C3x3.5", ".625", true, "P40304-FP2K-B-3-5-6-F")]
        [InlineData(.375, "FP3K-A", "SL", "C3x3.5", ".625", true, "P40304-FP3K-A-3-5-6-F")]
        [InlineData(.375, "FP3K-B", "SL", "C3x3.5", ".625", true, "P40304-FP3K-B-3-5-6-F")]
        [InlineData(.375, "FP3K-C", "SL", "C3x3.5", ".625", true, "P40304-FP3K-C-3-5-6-F")]
        [InlineData(.375, "FP3K-D", "SL", "C3x3.5", ".625", true, "P40304-FP3K-D-3-5-6-F")]
        [InlineData(.375, "FP4K-A", "SL", "C3x3.5", ".625", true, "P40304-FP4K-A-3-5-6-F")]
        [InlineData(.375, "FP4K-B", "SL", "C3x3.5", ".625", true, "P40304-FP4K-B-3-5-6-F")]
        [InlineData(.375, "FP5K-A", "SL", "C3x3.5", ".625", true, "P40304-FP5K-A-3-5-6-F")]
        [InlineData(.375, "FP5K-B", "SL", "C3x3.5", ".625", true, "P40304-FP5K-B-3-5-6-F")]
        [InlineData(.375, "FP5K-C", "SL", "C3x3.5", ".625", true, "P40304-FP5K-C-3-5-6-F")]
        [InlineData(.375, "FP5K-D", "SL", "C3x3.5", ".625", true, "P40304-FP5K-D-3-5-6-F")]
        [InlineData(.375, "FP6K-A", "SL", "C3x3.5", ".625", true, "P40304-FP6K-A-3-5-6-F")]
        [InlineData(.375, "FP6K-B", "SL", "C3x3.5", ".625", true, "P40304-FP6K-B-3-5-6-F")]
        [InlineData(.375, "FP1K-A", "SL", "C4x4.5", ".625", true, "P40304-FP1K-A-4-6-6-F")]
        [InlineData(.375, "FP1K-B", "SL", "C4x4.5", ".625", true, "P40304-FP1K-B-4-6-6-F")]
        [InlineData(.375, "FP1K-C", "SL", "C4x4.5", ".625", true, "P40304-FP1K-C-4-6-6-F")]
        [InlineData(.375, "FP1K-D", "SL", "C4x4.5", ".625", true, "P40304-FP1K-D-4-6-6-F")]
        [InlineData(.375, "FP2K-A", "SL", "C4x4.5", ".625", true, "P40304-FP2K-A-4-6-6-F")]
        [InlineData(.375, "FP2K-B", "SL", "C4x4.5", ".625", true, "P40304-FP2K-B-4-6-6-F")]
        [InlineData(.375, "FP3K-A", "SL", "C4x4.5", ".625", true, "P40304-FP3K-A-4-6-6-F")]
        [InlineData(.375, "FP3K-B", "SL", "C4x4.5", ".625", true, "P40304-FP3K-B-4-6-6-F")]
        [InlineData(.375, "FP3K-C", "SL", "C4x4.5", ".625", true, "P40304-FP3K-C-4-6-6-F")]
        [InlineData(.375, "FP3K-D", "SL", "C4x4.5", ".625", true, "P40304-FP3K-D-4-6-6-F")]
        [InlineData(.375, "FP4K-A", "SL", "C4x4.5", ".625", true, "P40304-FP4K-A-4-6-6-F")]
        [InlineData(.375, "FP4K-B", "SL", "C4x4.5", ".625", true, "P40304-FP4K-B-4-6-6-F")]
        [InlineData(.375, "FP5K-A", "SL", "C4x4.5", ".625", true, "P40304-FP5K-A-4-6-6-F")]
        [InlineData(.375, "FP5K-B", "SL", "C4x4.5", ".625", true, "P40304-FP5K-B-4-6-6-F")]
        [InlineData(.375, "FP5K-C", "SL", "C4x4.5", ".625", true, "P40304-FP5K-C-4-6-6-F")]
        [InlineData(.375, "FP5K-D", "SL", "C4x4.5", ".625", true, "P40304-FP5K-D-4-6-6-F")]
        [InlineData(.375, "FP6K-A", "SL", "C4x4.5", ".625", true, "P40304-FP6K-A-4-6-6-F")]
        [InlineData(.375, "FP6K-B", "SL", "C4x4.5", ".625", true, "P40304-FP6K-B-4-6-6-F")]
        [InlineData(.375, "FP1K-A", "SL", "C5x6.7", ".625", true, "P40304-FP1K-A-5-7-6-F")]
        [InlineData(.375, "FP1K-B", "SL", "C5x6.7", ".625", true, "P40304-FP1K-B-5-7-6-F")]
        [InlineData(.375, "FP1K-C", "SL", "C5x6.7", ".625", true, "P40304-FP1K-C-5-7-6-F")]
        [InlineData(.375, "FP1K-D", "SL", "C5x6.7", ".625", true, "P40304-FP1K-D-5-7-6-F")]
        [InlineData(.375, "FP2K-A", "SL", "C5x6.7", ".625", true, "P40304-FP2K-A-5-7-6-F")]
        [InlineData(.375, "FP2K-B", "SL", "C5x6.7", ".625", true, "P40304-FP2K-B-5-7-6-F")]
        [InlineData(.375, "FP3K-A", "SL", "C5x6.7", ".625", true, "P40304-FP3K-A-5-7-6-F")]
        [InlineData(.375, "FP3K-B", "SL", "C5x6.7", ".625", true, "P40304-FP3K-B-5-7-6-F")]
        [InlineData(.375, "FP3K-C", "SL", "C5x6.7", ".625", true, "P40304-FP3K-C-5-7-6-F")]
        [InlineData(.375, "FP3K-D", "SL", "C5x6.7", ".625", true, "P40304-FP3K-D-5-7-6-F")]
        [InlineData(.375, "FP4K-A", "SL", "C5x6.7", ".625", true, "P40304-FP4K-A-5-7-6-F")]
        [InlineData(.375, "FP4K-B", "SL", "C5x6.7", ".625", true, "P40304-FP4K-B-5-7-6-F")]
        [InlineData(.375, "FP5K-A", "SL", "C5x6.7", ".625", true, "P40304-FP5K-A-5-7-6-F")]
        [InlineData(.375, "FP5K-B", "SL", "C5x6.7", ".625", true, "P40304-FP5K-B-5-7-6-F")]
        [InlineData(.375, "FP5K-C", "SL", "C5x6.7", ".625", true, "P40304-FP5K-C-5-7-6-F")]
        [InlineData(.375, "FP5K-D", "SL", "C5x6.7", ".625", true, "P40304-FP5K-D-5-7-6-F")]
        [InlineData(.375, "FP6K-A", "SL", "C5x6.7", ".625", true, "P40304-FP6K-A-5-7-6-F")]
        [InlineData(.375, "FP6K-B", "SL", "C5x6.7", ".625", true, "P40304-FP6K-B-5-7-6-F")]
        [InlineData(.375, "FP1K-A", "SL", "C3x3.5", ".75", true, "P40304-FP1K-A-3-5-7-F")]
        [InlineData(.375, "FP1K-B", "SL", "C3x3.5", ".75", true, "P40304-FP1K-B-3-5-7-F")]
        [InlineData(.375, "FP1K-C", "SL", "C3x3.5", ".75", true, "P40304-FP1K-C-3-5-7-F")]
        [InlineData(.375, "FP1K-D", "SL", "C3x3.5", ".75", true, "P40304-FP1K-D-3-5-7-F")]
        [InlineData(.375, "FP2K-A", "SL", "C3x3.5", ".75", true, "P40304-FP2K-A-3-5-7-F")]
        [InlineData(.375, "FP2K-B", "SL", "C3x3.5", ".75", true, "P40304-FP2K-B-3-5-7-F")]
        [InlineData(.375, "FP3K-A", "SL", "C3x3.5", ".75", true, "P40304-FP3K-A-3-5-7-F")]
        [InlineData(.375, "FP3K-B", "SL", "C3x3.5", ".75", true, "P40304-FP3K-B-3-5-7-F")]
        [InlineData(.375, "FP3K-C", "SL", "C3x3.5", ".75", true, "P40304-FP3K-C-3-5-7-F")]
        [InlineData(.375, "FP3K-D", "SL", "C3x3.5", ".75", true, "P40304-FP3K-D-3-5-7-F")]
        [InlineData(.375, "FP4K-A", "SL", "C3x3.5", ".75", true, "P40304-FP4K-A-3-5-7-F")]
        [InlineData(.375, "FP4K-B", "SL", "C3x3.5", ".75", true, "P40304-FP4K-B-3-5-7-F")]
        [InlineData(.375, "FP5K-A", "SL", "C3x3.5", ".75", true, "P40304-FP5K-A-3-5-7-F")]
        [InlineData(.375, "FP5K-B", "SL", "C3x3.5", ".75", true, "P40304-FP5K-B-3-5-7-F")]
        [InlineData(.375, "FP5K-C", "SL", "C3x3.5", ".75", true, "P40304-FP5K-C-3-5-7-F")]
        [InlineData(.375, "FP5K-D", "SL", "C3x3.5", ".75", true, "P40304-FP5K-D-3-5-7-F")]
        [InlineData(.375, "FP6K-A", "SL", "C3x3.5", ".75", true, "P40304-FP6K-A-3-5-7-F")]
        [InlineData(.375, "FP6K-B", "SL", "C3x3.5", ".75", true, "P40304-FP6K-B-3-5-7-F")]
        [InlineData(.375, "FP1K-A", "SL", "C4x4.5", ".75", true, "P40304-FP1K-A-4-6-7-F")]
        [InlineData(.375, "FP1K-B", "SL", "C4x4.5", ".75", true, "P40304-FP1K-B-4-6-7-F")]
        [InlineData(.375, "FP1K-C", "SL", "C4x4.5", ".75", true, "P40304-FP1K-C-4-6-7-F")]
        [InlineData(.375, "FP1K-D", "SL", "C4x4.5", ".75", true, "P40304-FP1K-D-4-6-7-F")]
        [InlineData(.375, "FP2K-A", "SL", "C4x4.5", ".75", true, "P40304-FP2K-A-4-6-7-F")]
        [InlineData(.375, "FP2K-B", "SL", "C4x4.5", ".75", true, "P40304-FP2K-B-4-6-7-F")]
        [InlineData(.375, "FP3K-A", "SL", "C4x4.5", ".75", true, "P40304-FP3K-A-4-6-7-F")]
        [InlineData(.375, "FP3K-B", "SL", "C4x4.5", ".75", true, "P40304-FP3K-B-4-6-7-F")]
        [InlineData(.375, "FP3K-C", "SL", "C4x4.5", ".75", true, "P40304-FP3K-C-4-6-7-F")]
        [InlineData(.375, "FP3K-D", "SL", "C4x4.5", ".75", true, "P40304-FP3K-D-4-6-7-F")]
        [InlineData(.375, "FP4K-A", "SL", "C4x4.5", ".75", true, "P40304-FP4K-A-4-6-7-F")]
        [InlineData(.375, "FP4K-B", "SL", "C4x4.5", ".75", true, "P40304-FP4K-B-4-6-7-F")]
        [InlineData(.375, "FP5K-A", "SL", "C4x4.5", ".75", true, "P40304-FP5K-A-4-6-7-F")]
        [InlineData(.375, "FP5K-B", "SL", "C4x4.5", ".75", true, "P40304-FP5K-B-4-6-7-F")]
        [InlineData(.375, "FP5K-C", "SL", "C4x4.5", ".75", true, "P40304-FP5K-C-4-6-7-F")]
        [InlineData(.375, "FP5K-D", "SL", "C4x4.5", ".75", true, "P40304-FP5K-D-4-6-7-F")]
        [InlineData(.375, "FP6K-A", "SL", "C4x4.5", ".75", true, "P40304-FP6K-A-4-6-7-F")]
        [InlineData(.375, "FP6K-B", "SL", "C4x4.5", ".75", true, "P40304-FP6K-B-4-6-7-F")]
        [InlineData(.375, "FP1K-A", "SL", "C5x6.7", ".75", true, "P40304-FP1K-A-5-7-7-F")]
        [InlineData(.375, "FP1K-B", "SL", "C5x6.7", ".75", true, "P40304-FP1K-B-5-7-7-F")]
        [InlineData(.375, "FP1K-C", "SL", "C5x6.7", ".75", true, "P40304-FP1K-C-5-7-7-F")]
        [InlineData(.375, "FP1K-D", "SL", "C5x6.7", ".75", true, "P40304-FP1K-D-5-7-7-F")]
        [InlineData(.375, "FP2K-A", "SL", "C5x6.7", ".75", true, "P40304-FP2K-A-5-7-7-F")]
        [InlineData(.375, "FP2K-B", "SL", "C5x6.7", ".75", true, "P40304-FP2K-B-5-7-7-F")]
        [InlineData(.375, "FP3K-A", "SL", "C5x6.7", ".75", true, "P40304-FP3K-A-5-7-7-F")]
        [InlineData(.375, "FP3K-B", "SL", "C5x6.7", ".75", true, "P40304-FP3K-B-5-7-7-F")]
        [InlineData(.375, "FP3K-C", "SL", "C5x6.7", ".75", true, "P40304-FP3K-C-5-7-7-F")]
        [InlineData(.375, "FP3K-D", "SL", "C5x6.7", ".75", true, "P40304-FP3K-D-5-7-7-F")]
        [InlineData(.375, "FP4K-A", "SL", "C5x6.7", ".75", true, "P40304-FP4K-A-5-7-7-F")]
        [InlineData(.375, "FP4K-B", "SL", "C5x6.7", ".75", true, "P40304-FP4K-B-5-7-7-F")]
        [InlineData(.375, "FP5K-A", "SL", "C5x6.7", ".75", true, "P40304-FP5K-A-5-7-7-F")]
        [InlineData(.375, "FP5K-B", "SL", "C5x6.7", ".75", true, "P40304-FP5K-B-5-7-7-F")]
        [InlineData(.375, "FP5K-C", "SL", "C5x6.7", ".75", true, "P40304-FP5K-C-5-7-7-F")]
        [InlineData(.375, "FP5K-D", "SL", "C5x6.7", ".75", true, "P40304-FP5K-D-5-7-7-F")]
        [InlineData(.375, "FP6K-A", "SL", "C5x6.7", ".75", true, "P40304-FP6K-A-5-7-7-F")]
        [InlineData(.375, "FP6K-B", "SL", "C5x6.7", ".75", true, "P40304-FP6K-B-5-7-7-F")]
        //1/2in plate thickness
        [InlineData(.5, "FP1K-A", "SL", "C3x3.5", ".5", true, "P40305-FP1K-A-3-5-5-F")]
        [InlineData(.5, "FP1K-B", "SL", "C3x3.5", ".5", true, "P40305-FP1K-B-3-5-5-F")]
        [InlineData(.5, "FP1K-C", "SL", "C3x3.5", ".5", true, "P40305-FP1K-C-3-5-5-F")]
        [InlineData(.5, "FP1K-D", "SL", "C3x3.5", ".5", true, "P40305-FP1K-D-3-5-5-F")]
        [InlineData(.5, "FP2K-A", "SL", "C3x3.5", ".5", true, "P40305-FP2K-A-3-5-5-F")]
        [InlineData(.5, "FP2K-B", "SL", "C3x3.5", ".5", true, "P40305-FP2K-B-3-5-5-F")]
        [InlineData(.5, "FP3K-A", "SL", "C3x3.5", ".5", true, "P40305-FP3K-A-3-5-5-F")]
        [InlineData(.5, "FP3K-B", "SL", "C3x3.5", ".5", true, "P40305-FP3K-B-3-5-5-F")]
        [InlineData(.5, "FP3K-C", "SL", "C3x3.5", ".5", true, "P40305-FP3K-C-3-5-5-F")]
        [InlineData(.5, "FP3K-D", "SL", "C3x3.5", ".5", true, "P40305-FP3K-D-3-5-5-F")]
        [InlineData(.5, "FP4K-A", "SL", "C3x3.5", ".5", true, "P40305-FP4K-A-3-5-5-F")]
        [InlineData(.5, "FP4K-B", "SL", "C3x3.5", ".5", true, "P40305-FP4K-B-3-5-5-F")]
        [InlineData(.5, "FP5K-A", "SL", "C3x3.5", ".5", true, "P40305-FP5K-A-3-5-5-F")]
        [InlineData(.5, "FP5K-B", "SL", "C3x3.5", ".5", true, "P40305-FP5K-B-3-5-5-F")]
        [InlineData(.5, "FP5K-C", "SL", "C3x3.5", ".5", true, "P40305-FP5K-C-3-5-5-F")]
        [InlineData(.5, "FP5K-D", "SL", "C3x3.5", ".5", true, "P40305-FP5K-D-3-5-5-F")]
        [InlineData(.5, "FP6K-A", "SL", "C3x3.5", ".5", true, "P40305-FP6K-A-3-5-5-F")]
        [InlineData(.5, "FP6K-B", "SL", "C3x3.5", ".5", true, "P40305-FP6K-B-3-5-5-F")]
        [InlineData(.5, "FP1K-A", "SL", "C4x4.5", ".5", true, "P40305-FP1K-A-4-6-5-F")]
        [InlineData(.5, "FP1K-B", "SL", "C4x4.5", ".5", true, "P40305-FP1K-B-4-6-5-F")]
        [InlineData(.5, "FP1K-C", "SL", "C4x4.5", ".5", true, "P40305-FP1K-C-4-6-5-F")]
        [InlineData(.5, "FP1K-D", "SL", "C4x4.5", ".5", true, "P40305-FP1K-D-4-6-5-F")]
        [InlineData(.5, "FP2K-A", "SL", "C4x4.5", ".5", true, "P40305-FP2K-A-4-6-5-F")]
        [InlineData(.5, "FP2K-B", "SL", "C4x4.5", ".5", true, "P40305-FP2K-B-4-6-5-F")]
        [InlineData(.5, "FP3K-A", "SL", "C4x4.5", ".5", true, "P40305-FP3K-A-4-6-5-F")]
        [InlineData(.5, "FP3K-B", "SL", "C4x4.5", ".5", true, "P40305-FP3K-B-4-6-5-F")]
        [InlineData(.5, "FP3K-C", "SL", "C4x4.5", ".5", true, "P40305-FP3K-C-4-6-5-F")]
        [InlineData(.5, "FP3K-D", "SL", "C4x4.5", ".5", true, "P40305-FP3K-D-4-6-5-F")]
        [InlineData(.5, "FP4K-A", "SL", "C4x4.5", ".5", true, "P40305-FP4K-A-4-6-5-F")]
        [InlineData(.5, "FP4K-B", "SL", "C4x4.5", ".5", true, "P40305-FP4K-B-4-6-5-F")]
        [InlineData(.5, "FP5K-A", "SL", "C4x4.5", ".5", true, "P40305-FP5K-A-4-6-5-F")]
        [InlineData(.5, "FP5K-B", "SL", "C4x4.5", ".5", true, "P40305-FP5K-B-4-6-5-F")]
        [InlineData(.5, "FP5K-C", "SL", "C4x4.5", ".5", true, "P40305-FP5K-C-4-6-5-F")]
        [InlineData(.5, "FP5K-D", "SL", "C4x4.5", ".5", true, "P40305-FP5K-D-4-6-5-F")]
        [InlineData(.5, "FP6K-A", "SL", "C4x4.5", ".5", true, "P40305-FP6K-A-4-6-5-F")]
        [InlineData(.5, "FP6K-B", "SL", "C4x4.5", ".5", true, "P40305-FP6K-B-4-6-5-F")]
        [InlineData(.5, "FP1K-A", "SL", "C5x6.7", ".5", true, "P40305-FP1K-A-5-7-5-F")]
        [InlineData(.5, "FP1K-B", "SL", "C5x6.7", ".5", true, "P40305-FP1K-B-5-7-5-F")]
        [InlineData(.5, "FP1K-C", "SL", "C5x6.7", ".5", true, "P40305-FP1K-C-5-7-5-F")]
        [InlineData(.5, "FP1K-D", "SL", "C5x6.7", ".5", true, "P40305-FP1K-D-5-7-5-F")]
        [InlineData(.5, "FP2K-A", "SL", "C5x6.7", ".5", true, "P40305-FP2K-A-5-7-5-F")]
        [InlineData(.5, "FP2K-B", "SL", "C5x6.7", ".5", true, "P40305-FP2K-B-5-7-5-F")]
        [InlineData(.5, "FP3K-A", "SL", "C5x6.7", ".5", true, "P40305-FP3K-A-5-7-5-F")]
        [InlineData(.5, "FP3K-B", "SL", "C5x6.7", ".5", true, "P40305-FP3K-B-5-7-5-F")]
        [InlineData(.5, "FP3K-C", "SL", "C5x6.7", ".5", true, "P40305-FP3K-C-5-7-5-F")]
        [InlineData(.5, "FP3K-D", "SL", "C5x6.7", ".5", true, "P40305-FP3K-D-5-7-5-F")]
        [InlineData(.5, "FP4K-A", "SL", "C5x6.7", ".5", true, "P40305-FP4K-A-5-7-5-F")]
        [InlineData(.5, "FP4K-B", "SL", "C5x6.7", ".5", true, "P40305-FP4K-B-5-7-5-F")]
        [InlineData(.5, "FP5K-A", "SL", "C5x6.7", ".5", true, "P40305-FP5K-A-5-7-5-F")]
        [InlineData(.5, "FP5K-B", "SL", "C5x6.7", ".5", true, "P40305-FP5K-B-5-7-5-F")]
        [InlineData(.5, "FP5K-C", "SL", "C5x6.7", ".5", true, "P40305-FP5K-C-5-7-5-F")]
        [InlineData(.5, "FP5K-D", "SL", "C5x6.7", ".5", true, "P40305-FP5K-D-5-7-5-F")]
        [InlineData(.5, "FP6K-A", "SL", "C5x6.7", ".5", true, "P40305-FP6K-A-5-7-5-F")]
        [InlineData(.5, "FP6K-B", "SL", "C5x6.7", ".5", true, "P40305-FP6K-B-5-7-5-F")]
        [InlineData(.5, "FP1K-A", "SL", "C3x3.5", ".625", true, "P40305-FP1K-A-3-5-6-F")]
        [InlineData(.5, "FP1K-B", "SL", "C3x3.5", ".625", true, "P40305-FP1K-B-3-5-6-F")]
        [InlineData(.5, "FP1K-C", "SL", "C3x3.5", ".625", true, "P40305-FP1K-C-3-5-6-F")]
        [InlineData(.5, "FP1K-D", "SL", "C3x3.5", ".625", true, "P40305-FP1K-D-3-5-6-F")]
        [InlineData(.5, "FP2K-A", "SL", "C3x3.5", ".625", true, "P40305-FP2K-A-3-5-6-F")]
        [InlineData(.5, "FP2K-B", "SL", "C3x3.5", ".625", true, "P40305-FP2K-B-3-5-6-F")]
        [InlineData(.5, "FP3K-A", "SL", "C3x3.5", ".625", true, "P40305-FP3K-A-3-5-6-F")]
        [InlineData(.5, "FP3K-B", "SL", "C3x3.5", ".625", true, "P40305-FP3K-B-3-5-6-F")]
        [InlineData(.5, "FP3K-C", "SL", "C3x3.5", ".625", true, "P40305-FP3K-C-3-5-6-F")]
        [InlineData(.5, "FP3K-D", "SL", "C3x3.5", ".625", true, "P40305-FP3K-D-3-5-6-F")]
        [InlineData(.5, "FP4K-A", "SL", "C3x3.5", ".625", true, "P40305-FP4K-A-3-5-6-F")]
        [InlineData(.5, "FP4K-B", "SL", "C3x3.5", ".625", true, "P40305-FP4K-B-3-5-6-F")]
        [InlineData(.5, "FP5K-A", "SL", "C3x3.5", ".625", true, "P40305-FP5K-A-3-5-6-F")]
        [InlineData(.5, "FP5K-B", "SL", "C3x3.5", ".625", true, "P40305-FP5K-B-3-5-6-F")]
        [InlineData(.5, "FP5K-C", "SL", "C3x3.5", ".625", true, "P40305-FP5K-C-3-5-6-F")]
        [InlineData(.5, "FP5K-D", "SL", "C3x3.5", ".625", true, "P40305-FP5K-D-3-5-6-F")]
        [InlineData(.5, "FP6K-A", "SL", "C3x3.5", ".625", true, "P40305-FP6K-A-3-5-6-F")]
        [InlineData(.5, "FP6K-B", "SL", "C3x3.5", ".625", true, "P40305-FP6K-B-3-5-6-F")]
        [InlineData(.5, "FP1K-A", "SL", "C4x4.5", ".625", true, "P40305-FP1K-A-4-6-6-F")]
        [InlineData(.5, "FP1K-B", "SL", "C4x4.5", ".625", true, "P40305-FP1K-B-4-6-6-F")]
        [InlineData(.5, "FP1K-C", "SL", "C4x4.5", ".625", true, "P40305-FP1K-C-4-6-6-F")]
        [InlineData(.5, "FP1K-D", "SL", "C4x4.5", ".625", true, "P40305-FP1K-D-4-6-6-F")]
        [InlineData(.5, "FP2K-A", "SL", "C4x4.5", ".625", true, "P40305-FP2K-A-4-6-6-F")]
        [InlineData(.5, "FP2K-B", "SL", "C4x4.5", ".625", true, "P40305-FP2K-B-4-6-6-F")]
        [InlineData(.5, "FP3K-A", "SL", "C4x4.5", ".625", true, "P40305-FP3K-A-4-6-6-F")]
        [InlineData(.5, "FP3K-B", "SL", "C4x4.5", ".625", true, "P40305-FP3K-B-4-6-6-F")]
        [InlineData(.5, "FP3K-C", "SL", "C4x4.5", ".625", true, "P40305-FP3K-C-4-6-6-F")]
        [InlineData(.5, "FP3K-D", "SL", "C4x4.5", ".625", true, "P40305-FP3K-D-4-6-6-F")]
        [InlineData(.5, "FP4K-A", "SL", "C4x4.5", ".625", true, "P40305-FP4K-A-4-6-6-F")]
        [InlineData(.5, "FP4K-B", "SL", "C4x4.5", ".625", true, "P40305-FP4K-B-4-6-6-F")]
        [InlineData(.5, "FP5K-A", "SL", "C4x4.5", ".625", true, "P40305-FP5K-A-4-6-6-F")]
        [InlineData(.5, "FP5K-B", "SL", "C4x4.5", ".625", true, "P40305-FP5K-B-4-6-6-F")]
        [InlineData(.5, "FP5K-C", "SL", "C4x4.5", ".625", true, "P40305-FP5K-C-4-6-6-F")]
        [InlineData(.5, "FP5K-D", "SL", "C4x4.5", ".625", true, "P40305-FP5K-D-4-6-6-F")]
        [InlineData(.5, "FP6K-A", "SL", "C4x4.5", ".625", true, "P40305-FP6K-A-4-6-6-F")]
        [InlineData(.5, "FP6K-B", "SL", "C4x4.5", ".625", true, "P40305-FP6K-B-4-6-6-F")]
        [InlineData(.5, "FP1K-A", "SL", "C5x6.7", ".625", true, "P40305-FP1K-A-5-7-6-F")]
        [InlineData(.5, "FP1K-B", "SL", "C5x6.7", ".625", true, "P40305-FP1K-B-5-7-6-F")]
        [InlineData(.5, "FP1K-C", "SL", "C5x6.7", ".625", true, "P40305-FP1K-C-5-7-6-F")]
        [InlineData(.5, "FP1K-D", "SL", "C5x6.7", ".625", true, "P40305-FP1K-D-5-7-6-F")]
        [InlineData(.5, "FP2K-A", "SL", "C5x6.7", ".625", true, "P40305-FP2K-A-5-7-6-F")]
        [InlineData(.5, "FP2K-B", "SL", "C5x6.7", ".625", true, "P40305-FP2K-B-5-7-6-F")]
        [InlineData(.5, "FP3K-A", "SL", "C5x6.7", ".625", true, "P40305-FP3K-A-5-7-6-F")]
        [InlineData(.5, "FP3K-B", "SL", "C5x6.7", ".625", true, "P40305-FP3K-B-5-7-6-F")]
        [InlineData(.5, "FP3K-C", "SL", "C5x6.7", ".625", true, "P40305-FP3K-C-5-7-6-F")]
        [InlineData(.5, "FP3K-D", "SL", "C5x6.7", ".625", true, "P40305-FP3K-D-5-7-6-F")]
        [InlineData(.5, "FP4K-A", "SL", "C5x6.7", ".625", true, "P40305-FP4K-A-5-7-6-F")]
        [InlineData(.5, "FP4K-B", "SL", "C5x6.7", ".625", true, "P40305-FP4K-B-5-7-6-F")]
        [InlineData(.5, "FP5K-A", "SL", "C5x6.7", ".625", true, "P40305-FP5K-A-5-7-6-F")]
        [InlineData(.5, "FP5K-B", "SL", "C5x6.7", ".625", true, "P40305-FP5K-B-5-7-6-F")]
        [InlineData(.5, "FP5K-C", "SL", "C5x6.7", ".625", true, "P40305-FP5K-C-5-7-6-F")]
        [InlineData(.5, "FP5K-D", "SL", "C5x6.7", ".625", true, "P40305-FP5K-D-5-7-6-F")]
        [InlineData(.5, "FP6K-A", "SL", "C5x6.7", ".625", true, "P40305-FP6K-A-5-7-6-F")]
        [InlineData(.5, "FP6K-B", "SL", "C5x6.7", ".625", true, "P40305-FP6K-B-5-7-6-F")]
        [InlineData(.5, "FP1K-A", "SL", "C3x3.5", ".75", true, "P40305-FP1K-A-3-5-7-F")]
        [InlineData(.5, "FP1K-B", "SL", "C3x3.5", ".75", true, "P40305-FP1K-B-3-5-7-F")]
        [InlineData(.5, "FP1K-C", "SL", "C3x3.5", ".75", true, "P40305-FP1K-C-3-5-7-F")]
        [InlineData(.5, "FP1K-D", "SL", "C3x3.5", ".75", true, "P40305-FP1K-D-3-5-7-F")]
        [InlineData(.5, "FP2K-A", "SL", "C3x3.5", ".75", true, "P40305-FP2K-A-3-5-7-F")]
        [InlineData(.5, "FP2K-B", "SL", "C3x3.5", ".75", true, "P40305-FP2K-B-3-5-7-F")]
        [InlineData(.5, "FP3K-A", "SL", "C3x3.5", ".75", true, "P40305-FP3K-A-3-5-7-F")]
        [InlineData(.5, "FP3K-B", "SL", "C3x3.5", ".75", true, "P40305-FP3K-B-3-5-7-F")]
        [InlineData(.5, "FP3K-C", "SL", "C3x3.5", ".75", true, "P40305-FP3K-C-3-5-7-F")]
        [InlineData(.5, "FP3K-D", "SL", "C3x3.5", ".75", true, "P40305-FP3K-D-3-5-7-F")]
        [InlineData(.5, "FP4K-A", "SL", "C3x3.5", ".75", true, "P40305-FP4K-A-3-5-7-F")]
        [InlineData(.5, "FP4K-B", "SL", "C3x3.5", ".75", true, "P40305-FP4K-B-3-5-7-F")]
        [InlineData(.5, "FP5K-A", "SL", "C3x3.5", ".75", true, "P40305-FP5K-A-3-5-7-F")]
        [InlineData(.5, "FP5K-B", "SL", "C3x3.5", ".75", true, "P40305-FP5K-B-3-5-7-F")]
        [InlineData(.5, "FP5K-C", "SL", "C3x3.5", ".75", true, "P40305-FP5K-C-3-5-7-F")]
        [InlineData(.5, "FP5K-D", "SL", "C3x3.5", ".75", true, "P40305-FP5K-D-3-5-7-F")]
        [InlineData(.5, "FP6K-A", "SL", "C3x3.5", ".75", true, "P40305-FP6K-A-3-5-7-F")]
        [InlineData(.5, "FP6K-B", "SL", "C3x3.5", ".75", true, "P40305-FP6K-B-3-5-7-F")]
        [InlineData(.5, "FP1K-A", "SL", "C4x4.5", ".75", true, "P40305-FP1K-A-4-6-7-F")]
        [InlineData(.5, "FP1K-B", "SL", "C4x4.5", ".75", true, "P40305-FP1K-B-4-6-7-F")]
        [InlineData(.5, "FP1K-C", "SL", "C4x4.5", ".75", true, "P40305-FP1K-C-4-6-7-F")]
        [InlineData(.5, "FP1K-D", "SL", "C4x4.5", ".75", true, "P40305-FP1K-D-4-6-7-F")]
        [InlineData(.5, "FP2K-A", "SL", "C4x4.5", ".75", true, "P40305-FP2K-A-4-6-7-F")]
        [InlineData(.5, "FP2K-B", "SL", "C4x4.5", ".75", true, "P40305-FP2K-B-4-6-7-F")]
        [InlineData(.5, "FP3K-A", "SL", "C4x4.5", ".75", true, "P40305-FP3K-A-4-6-7-F")]
        [InlineData(.5, "FP3K-B", "SL", "C4x4.5", ".75", true, "P40305-FP3K-B-4-6-7-F")]
        [InlineData(.5, "FP3K-C", "SL", "C4x4.5", ".75", true, "P40305-FP3K-C-4-6-7-F")]
        [InlineData(.5, "FP3K-D", "SL", "C4x4.5", ".75", true, "P40305-FP3K-D-4-6-7-F")]
        [InlineData(.5, "FP4K-A", "SL", "C4x4.5", ".75", true, "P40305-FP4K-A-4-6-7-F")]
        [InlineData(.5, "FP4K-B", "SL", "C4x4.5", ".75", true, "P40305-FP4K-B-4-6-7-F")]
        [InlineData(.5, "FP5K-A", "SL", "C4x4.5", ".75", true, "P40305-FP5K-A-4-6-7-F")]
        [InlineData(.5, "FP5K-B", "SL", "C4x4.5", ".75", true, "P40305-FP5K-B-4-6-7-F")]
        [InlineData(.5, "FP5K-C", "SL", "C4x4.5", ".75", true, "P40305-FP5K-C-4-6-7-F")]
        [InlineData(.5, "FP5K-D", "SL", "C4x4.5", ".75", true, "P40305-FP5K-D-4-6-7-F")]
        [InlineData(.5, "FP6K-A", "SL", "C4x4.5", ".75", true, "P40305-FP6K-A-4-6-7-F")]
        [InlineData(.5, "FP6K-B", "SL", "C4x4.5", ".75", true, "P40305-FP6K-B-4-6-7-F")]
        [InlineData(.5, "FP1K-A", "SL", "C5x6.7", ".75", true, "P40305-FP1K-A-5-7-7-F")]
        [InlineData(.5, "FP1K-B", "SL", "C5x6.7", ".75", true, "P40305-FP1K-B-5-7-7-F")]
        [InlineData(.5, "FP1K-C", "SL", "C5x6.7", ".75", true, "P40305-FP1K-C-5-7-7-F")]
        [InlineData(.5, "FP1K-D", "SL", "C5x6.7", ".75", true, "P40305-FP1K-D-5-7-7-F")]
        [InlineData(.5, "FP2K-A", "SL", "C5x6.7", ".75", true, "P40305-FP2K-A-5-7-7-F")]
        [InlineData(.5, "FP2K-B", "SL", "C5x6.7", ".75", true, "P40305-FP2K-B-5-7-7-F")]
        [InlineData(.5, "FP3K-A", "SL", "C5x6.7", ".75", true, "P40305-FP3K-A-5-7-7-F")]
        [InlineData(.5, "FP3K-B", "SL", "C5x6.7", ".75", true, "P40305-FP3K-B-5-7-7-F")]
        [InlineData(.5, "FP3K-C", "SL", "C5x6.7", ".75", true, "P40305-FP3K-C-5-7-7-F")]
        [InlineData(.5, "FP3K-D", "SL", "C5x6.7", ".75", true, "P40305-FP3K-D-5-7-7-F")]
        [InlineData(.5, "FP4K-A", "SL", "C5x6.7", ".75", true, "P40305-FP4K-A-5-7-7-F")]
        [InlineData(.5, "FP4K-B", "SL", "C5x6.7", ".75", true, "P40305-FP4K-B-5-7-7-F")]
        [InlineData(.5, "FP5K-A", "SL", "C5x6.7", ".75", true, "P40305-FP5K-A-5-7-7-F")]
        [InlineData(.5, "FP5K-B", "SL", "C5x6.7", ".75", true, "P40305-FP5K-B-5-7-7-F")]
        [InlineData(.5, "FP5K-C", "SL", "C5x6.7", ".75", true, "P40305-FP5K-C-5-7-7-F")]
        [InlineData(.5, "FP5K-D", "SL", "C5x6.7", ".75", true, "P40305-FP5K-D-5-7-7-F")]
        [InlineData(.5, "FP6K-A", "SL", "C5x6.7", ".75", true, "P40305-FP6K-A-5-7-7-F")]
        [InlineData(.5, "FP6K-B", "SL", "C5x6.7", ".75", true, "P40305-FP6K-B-5-7-7-F")]
        //5/8in plate thickness
        [InlineData(.625, "FP1K-A", "SL", "C3x3.5", ".5", true, "P40306-FP1K-A-3-5-5-F")]
        [InlineData(.625, "FP1K-B", "SL", "C3x3.5", ".5", true, "P40306-FP1K-B-3-5-5-F")]
        [InlineData(.625, "FP1K-C", "SL", "C3x3.5", ".5", true, "P40306-FP1K-C-3-5-5-F")]
        [InlineData(.625, "FP1K-D", "SL", "C3x3.5", ".5", true, "P40306-FP1K-D-3-5-5-F")]
        [InlineData(.625, "FP2K-A", "SL", "C3x3.5", ".5", true, "P40306-FP2K-A-3-5-5-F")]
        [InlineData(.625, "FP2K-B", "SL", "C3x3.5", ".5", true, "P40306-FP2K-B-3-5-5-F")]
        [InlineData(.625, "FP3K-A", "SL", "C3x3.5", ".5", true, "P40306-FP3K-A-3-5-5-F")]
        [InlineData(.625, "FP3K-B", "SL", "C3x3.5", ".5", true, "P40306-FP3K-B-3-5-5-F")]
        [InlineData(.625, "FP3K-C", "SL", "C3x3.5", ".5", true, "P40306-FP3K-C-3-5-5-F")]
        [InlineData(.625, "FP3K-D", "SL", "C3x3.5", ".5", true, "P40306-FP3K-D-3-5-5-F")]
        [InlineData(.625, "FP4K-A", "SL", "C3x3.5", ".5", true, "P40306-FP4K-A-3-5-5-F")]
        [InlineData(.625, "FP4K-B", "SL", "C3x3.5", ".5", true, "P40306-FP4K-B-3-5-5-F")]
        [InlineData(.625, "FP5K-A", "SL", "C3x3.5", ".5", true, "P40306-FP5K-A-3-5-5-F")]
        [InlineData(.625, "FP5K-B", "SL", "C3x3.5", ".5", true, "P40306-FP5K-B-3-5-5-F")]
        [InlineData(.625, "FP5K-C", "SL", "C3x3.5", ".5", true, "P40306-FP5K-C-3-5-5-F")]
        [InlineData(.625, "FP5K-D", "SL", "C3x3.5", ".5", true, "P40306-FP5K-D-3-5-5-F")]
        [InlineData(.625, "FP6K-A", "SL", "C3x3.5", ".5", true, "P40306-FP6K-A-3-5-5-F")]
        [InlineData(.625, "FP6K-B", "SL", "C3x3.5", ".5", true, "P40306-FP6K-B-3-5-5-F")]
        [InlineData(.625, "FP1K-A", "SL", "C4x4.5", ".5", true, "P40306-FP1K-A-4-6-5-F")]
        [InlineData(.625, "FP1K-B", "SL", "C4x4.5", ".5", true, "P40306-FP1K-B-4-6-5-F")]
        [InlineData(.625, "FP1K-C", "SL", "C4x4.5", ".5", true, "P40306-FP1K-C-4-6-5-F")]
        [InlineData(.625, "FP1K-D", "SL", "C4x4.5", ".5", true, "P40306-FP1K-D-4-6-5-F")]
        [InlineData(.625, "FP2K-A", "SL", "C4x4.5", ".5", true, "P40306-FP2K-A-4-6-5-F")]
        [InlineData(.625, "FP2K-B", "SL", "C4x4.5", ".5", true, "P40306-FP2K-B-4-6-5-F")]
        [InlineData(.625, "FP3K-A", "SL", "C4x4.5", ".5", true, "P40306-FP3K-A-4-6-5-F")]
        [InlineData(.625, "FP3K-B", "SL", "C4x4.5", ".5", true, "P40306-FP3K-B-4-6-5-F")]
        [InlineData(.625, "FP3K-C", "SL", "C4x4.5", ".5", true, "P40306-FP3K-C-4-6-5-F")]
        [InlineData(.625, "FP3K-D", "SL", "C4x4.5", ".5", true, "P40306-FP3K-D-4-6-5-F")]
        [InlineData(.625, "FP4K-A", "SL", "C4x4.5", ".5", true, "P40306-FP4K-A-4-6-5-F")]
        [InlineData(.625, "FP4K-B", "SL", "C4x4.5", ".5", true, "P40306-FP4K-B-4-6-5-F")]
        [InlineData(.625, "FP5K-A", "SL", "C4x4.5", ".5", true, "P40306-FP5K-A-4-6-5-F")]
        [InlineData(.625, "FP5K-B", "SL", "C4x4.5", ".5", true, "P40306-FP5K-B-4-6-5-F")]
        [InlineData(.625, "FP5K-C", "SL", "C4x4.5", ".5", true, "P40306-FP5K-C-4-6-5-F")]
        [InlineData(.625, "FP5K-D", "SL", "C4x4.5", ".5", true, "P40306-FP5K-D-4-6-5-F")]
        [InlineData(.625, "FP6K-A", "SL", "C4x4.5", ".5", true, "P40306-FP6K-A-4-6-5-F")]
        [InlineData(.625, "FP6K-B", "SL", "C4x4.5", ".5", true, "P40306-FP6K-B-4-6-5-F")]
        [InlineData(.625, "FP1K-A", "SL", "C5x6.7", ".5", true, "P40306-FP1K-A-5-7-5-F")]
        [InlineData(.625, "FP1K-B", "SL", "C5x6.7", ".5", true, "P40306-FP1K-B-5-7-5-F")]
        [InlineData(.625, "FP1K-C", "SL", "C5x6.7", ".5", true, "P40306-FP1K-C-5-7-5-F")]
        [InlineData(.625, "FP1K-D", "SL", "C5x6.7", ".5", true, "P40306-FP1K-D-5-7-5-F")]
        [InlineData(.625, "FP2K-A", "SL", "C5x6.7", ".5", true, "P40306-FP2K-A-5-7-5-F")]
        [InlineData(.625, "FP2K-B", "SL", "C5x6.7", ".5", true, "P40306-FP2K-B-5-7-5-F")]
        [InlineData(.625, "FP3K-A", "SL", "C5x6.7", ".5", true, "P40306-FP3K-A-5-7-5-F")]
        [InlineData(.625, "FP3K-B", "SL", "C5x6.7", ".5", true, "P40306-FP3K-B-5-7-5-F")]
        [InlineData(.625, "FP3K-C", "SL", "C5x6.7", ".5", true, "P40306-FP3K-C-5-7-5-F")]
        [InlineData(.625, "FP3K-D", "SL", "C5x6.7", ".5", true, "P40306-FP3K-D-5-7-5-F")]
        [InlineData(.625, "FP4K-A", "SL", "C5x6.7", ".5", true, "P40306-FP4K-A-5-7-5-F")]
        [InlineData(.625, "FP4K-B", "SL", "C5x6.7", ".5", true, "P40306-FP4K-B-5-7-5-F")]
        [InlineData(.625, "FP5K-A", "SL", "C5x6.7", ".5", true, "P40306-FP5K-A-5-7-5-F")]
        [InlineData(.625, "FP5K-B", "SL", "C5x6.7", ".5", true, "P40306-FP5K-B-5-7-5-F")]
        [InlineData(.625, "FP5K-C", "SL", "C5x6.7", ".5", true, "P40306-FP5K-C-5-7-5-F")]
        [InlineData(.625, "FP5K-D", "SL", "C5x6.7", ".5", true, "P40306-FP5K-D-5-7-5-F")]
        [InlineData(.625, "FP6K-A", "SL", "C5x6.7", ".5", true, "P40306-FP6K-A-5-7-5-F")]
        [InlineData(.625, "FP6K-B", "SL", "C5x6.7", ".5", true, "P40306-FP6K-B-5-7-5-F")]
        [InlineData(.625, "FP1K-A", "SL", "C3x3.5", ".625", true, "P40306-FP1K-A-3-5-6-F")]
        [InlineData(.625, "FP1K-B", "SL", "C3x3.5", ".625", true, "P40306-FP1K-B-3-5-6-F")]
        [InlineData(.625, "FP1K-C", "SL", "C3x3.5", ".625", true, "P40306-FP1K-C-3-5-6-F")]
        [InlineData(.625, "FP1K-D", "SL", "C3x3.5", ".625", true, "P40306-FP1K-D-3-5-6-F")]
        [InlineData(.625, "FP2K-A", "SL", "C3x3.5", ".625", true, "P40306-FP2K-A-3-5-6-F")]
        [InlineData(.625, "FP2K-B", "SL", "C3x3.5", ".625", true, "P40306-FP2K-B-3-5-6-F")]
        [InlineData(.625, "FP3K-A", "SL", "C3x3.5", ".625", true, "P40306-FP3K-A-3-5-6-F")]
        [InlineData(.625, "FP3K-B", "SL", "C3x3.5", ".625", true, "P40306-FP3K-B-3-5-6-F")]
        [InlineData(.625, "FP3K-C", "SL", "C3x3.5", ".625", true, "P40306-FP3K-C-3-5-6-F")]
        [InlineData(.625, "FP3K-D", "SL", "C3x3.5", ".625", true, "P40306-FP3K-D-3-5-6-F")]
        [InlineData(.625, "FP4K-A", "SL", "C3x3.5", ".625", true, "P40306-FP4K-A-3-5-6-F")]
        [InlineData(.625, "FP4K-B", "SL", "C3x3.5", ".625", true, "P40306-FP4K-B-3-5-6-F")]
        [InlineData(.625, "FP5K-A", "SL", "C3x3.5", ".625", true, "P40306-FP5K-A-3-5-6-F")]
        [InlineData(.625, "FP5K-B", "SL", "C3x3.5", ".625", true, "P40306-FP5K-B-3-5-6-F")]
        [InlineData(.625, "FP5K-C", "SL", "C3x3.5", ".625", true, "P40306-FP5K-C-3-5-6-F")]
        [InlineData(.625, "FP5K-D", "SL", "C3x3.5", ".625", true, "P40306-FP5K-D-3-5-6-F")]
        [InlineData(.625, "FP6K-A", "SL", "C3x3.5", ".625", true, "P40306-FP6K-A-3-5-6-F")]
        [InlineData(.625, "FP6K-B", "SL", "C3x3.5", ".625", true, "P40306-FP6K-B-3-5-6-F")]
        [InlineData(.625, "FP1K-A", "SL", "C4x4.5", ".625", true, "P40306-FP1K-A-4-6-6-F")]
        [InlineData(.625, "FP1K-B", "SL", "C4x4.5", ".625", true, "P40306-FP1K-B-4-6-6-F")]
        [InlineData(.625, "FP1K-C", "SL", "C4x4.5", ".625", true, "P40306-FP1K-C-4-6-6-F")]
        [InlineData(.625, "FP1K-D", "SL", "C4x4.5", ".625", true, "P40306-FP1K-D-4-6-6-F")]
        [InlineData(.625, "FP2K-A", "SL", "C4x4.5", ".625", true, "P40306-FP2K-A-4-6-6-F")]
        [InlineData(.625, "FP2K-B", "SL", "C4x4.5", ".625", true, "P40306-FP2K-B-4-6-6-F")]
        [InlineData(.625, "FP3K-A", "SL", "C4x4.5", ".625", true, "P40306-FP3K-A-4-6-6-F")]
        [InlineData(.625, "FP3K-B", "SL", "C4x4.5", ".625", true, "P40306-FP3K-B-4-6-6-F")]
        [InlineData(.625, "FP3K-C", "SL", "C4x4.5", ".625", true, "P40306-FP3K-C-4-6-6-F")]
        [InlineData(.625, "FP3K-D", "SL", "C4x4.5", ".625", true, "P40306-FP3K-D-4-6-6-F")]
        [InlineData(.625, "FP4K-A", "SL", "C4x4.5", ".625", true, "P40306-FP4K-A-4-6-6-F")]
        [InlineData(.625, "FP4K-B", "SL", "C4x4.5", ".625", true, "P40306-FP4K-B-4-6-6-F")]
        [InlineData(.625, "FP5K-A", "SL", "C4x4.5", ".625", true, "P40306-FP5K-A-4-6-6-F")]
        [InlineData(.625, "FP5K-B", "SL", "C4x4.5", ".625", true, "P40306-FP5K-B-4-6-6-F")]
        [InlineData(.625, "FP5K-C", "SL", "C4x4.5", ".625", true, "P40306-FP5K-C-4-6-6-F")]
        [InlineData(.625, "FP5K-D", "SL", "C4x4.5", ".625", true, "P40306-FP5K-D-4-6-6-F")]
        [InlineData(.625, "FP6K-A", "SL", "C4x4.5", ".625", true, "P40306-FP6K-A-4-6-6-F")]
        [InlineData(.625, "FP6K-B", "SL", "C4x4.5", ".625", true, "P40306-FP6K-B-4-6-6-F")]
        [InlineData(.625, "FP1K-A", "SL", "C5x6.7", ".625", true, "P40306-FP1K-A-5-7-6-F")]
        [InlineData(.625, "FP1K-B", "SL", "C5x6.7", ".625", true, "P40306-FP1K-B-5-7-6-F")]
        [InlineData(.625, "FP1K-C", "SL", "C5x6.7", ".625", true, "P40306-FP1K-C-5-7-6-F")]
        [InlineData(.625, "FP1K-D", "SL", "C5x6.7", ".625", true, "P40306-FP1K-D-5-7-6-F")]
        [InlineData(.625, "FP2K-A", "SL", "C5x6.7", ".625", true, "P40306-FP2K-A-5-7-6-F")]
        [InlineData(.625, "FP2K-B", "SL", "C5x6.7", ".625", true, "P40306-FP2K-B-5-7-6-F")]
        [InlineData(.625, "FP3K-A", "SL", "C5x6.7", ".625", true, "P40306-FP3K-A-5-7-6-F")]
        [InlineData(.625, "FP3K-B", "SL", "C5x6.7", ".625", true, "P40306-FP3K-B-5-7-6-F")]
        [InlineData(.625, "FP3K-C", "SL", "C5x6.7", ".625", true, "P40306-FP3K-C-5-7-6-F")]
        [InlineData(.625, "FP3K-D", "SL", "C5x6.7", ".625", true, "P40306-FP3K-D-5-7-6-F")]
        [InlineData(.625, "FP4K-A", "SL", "C5x6.7", ".625", true, "P40306-FP4K-A-5-7-6-F")]
        [InlineData(.625, "FP4K-B", "SL", "C5x6.7", ".625", true, "P40306-FP4K-B-5-7-6-F")]
        [InlineData(.625, "FP5K-A", "SL", "C5x6.7", ".625", true, "P40306-FP5K-A-5-7-6-F")]
        [InlineData(.625, "FP5K-B", "SL", "C5x6.7", ".625", true, "P40306-FP5K-B-5-7-6-F")]
        [InlineData(.625, "FP5K-C", "SL", "C5x6.7", ".625", true, "P40306-FP5K-C-5-7-6-F")]
        [InlineData(.625, "FP5K-D", "SL", "C5x6.7", ".625", true, "P40306-FP5K-D-5-7-6-F")]
        [InlineData(.625, "FP6K-A", "SL", "C5x6.7", ".625", true, "P40306-FP6K-A-5-7-6-F")]
        [InlineData(.625, "FP6K-B", "SL", "C5x6.7", ".625", true, "P40306-FP6K-B-5-7-6-F")]
        [InlineData(.625, "FP1K-A", "SL", "C3x3.5", ".75", true, "P40306-FP1K-A-3-5-7-F")]
        [InlineData(.625, "FP1K-B", "SL", "C3x3.5", ".75", true, "P40306-FP1K-B-3-5-7-F")]
        [InlineData(.625, "FP1K-C", "SL", "C3x3.5", ".75", true, "P40306-FP1K-C-3-5-7-F")]
        [InlineData(.625, "FP1K-D", "SL", "C3x3.5", ".75", true, "P40306-FP1K-D-3-5-7-F")]
        [InlineData(.625, "FP2K-A", "SL", "C3x3.5", ".75", true, "P40306-FP2K-A-3-5-7-F")]
        [InlineData(.625, "FP2K-B", "SL", "C3x3.5", ".75", true, "P40306-FP2K-B-3-5-7-F")]
        [InlineData(.625, "FP3K-A", "SL", "C3x3.5", ".75", true, "P40306-FP3K-A-3-5-7-F")]
        [InlineData(.625, "FP3K-B", "SL", "C3x3.5", ".75", true, "P40306-FP3K-B-3-5-7-F")]
        [InlineData(.625, "FP3K-C", "SL", "C3x3.5", ".75", true, "P40306-FP3K-C-3-5-7-F")]
        [InlineData(.625, "FP3K-D", "SL", "C3x3.5", ".75", true, "P40306-FP3K-D-3-5-7-F")]
        [InlineData(.625, "FP4K-A", "SL", "C3x3.5", ".75", true, "P40306-FP4K-A-3-5-7-F")]
        [InlineData(.625, "FP4K-B", "SL", "C3x3.5", ".75", true, "P40306-FP4K-B-3-5-7-F")]
        [InlineData(.625, "FP5K-A", "SL", "C3x3.5", ".75", true, "P40306-FP5K-A-3-5-7-F")]
        [InlineData(.625, "FP5K-B", "SL", "C3x3.5", ".75", true, "P40306-FP5K-B-3-5-7-F")]
        [InlineData(.625, "FP5K-C", "SL", "C3x3.5", ".75", true, "P40306-FP5K-C-3-5-7-F")]
        [InlineData(.625, "FP5K-D", "SL", "C3x3.5", ".75", true, "P40306-FP5K-D-3-5-7-F")]
        [InlineData(.625, "FP6K-A", "SL", "C3x3.5", ".75", true, "P40306-FP6K-A-3-5-7-F")]
        [InlineData(.625, "FP6K-B", "SL", "C3x3.5", ".75", true, "P40306-FP6K-B-3-5-7-F")]
        [InlineData(.625, "FP1K-A", "SL", "C4x4.5", ".75", true, "P40306-FP1K-A-4-6-7-F")]
        [InlineData(.625, "FP1K-B", "SL", "C4x4.5", ".75", true, "P40306-FP1K-B-4-6-7-F")]
        [InlineData(.625, "FP1K-C", "SL", "C4x4.5", ".75", true, "P40306-FP1K-C-4-6-7-F")]
        [InlineData(.625, "FP1K-D", "SL", "C4x4.5", ".75", true, "P40306-FP1K-D-4-6-7-F")]
        [InlineData(.625, "FP2K-A", "SL", "C4x4.5", ".75", true, "P40306-FP2K-A-4-6-7-F")]
        [InlineData(.625, "FP2K-B", "SL", "C4x4.5", ".75", true, "P40306-FP2K-B-4-6-7-F")]
        [InlineData(.625, "FP3K-A", "SL", "C4x4.5", ".75", true, "P40306-FP3K-A-4-6-7-F")]
        [InlineData(.625, "FP3K-B", "SL", "C4x4.5", ".75", true, "P40306-FP3K-B-4-6-7-F")]
        [InlineData(.625, "FP3K-C", "SL", "C4x4.5", ".75", true, "P40306-FP3K-C-4-6-7-F")]
        [InlineData(.625, "FP3K-D", "SL", "C4x4.5", ".75", true, "P40306-FP3K-D-4-6-7-F")]
        [InlineData(.625, "FP4K-A", "SL", "C4x4.5", ".75", true, "P40306-FP4K-A-4-6-7-F")]
        [InlineData(.625, "FP4K-B", "SL", "C4x4.5", ".75", true, "P40306-FP4K-B-4-6-7-F")]
        [InlineData(.625, "FP5K-A", "SL", "C4x4.5", ".75", true, "P40306-FP5K-A-4-6-7-F")]
        [InlineData(.625, "FP5K-B", "SL", "C4x4.5", ".75", true, "P40306-FP5K-B-4-6-7-F")]
        [InlineData(.625, "FP5K-C", "SL", "C4x4.5", ".75", true, "P40306-FP5K-C-4-6-7-F")]
        [InlineData(.625, "FP5K-D", "SL", "C4x4.5", ".75", true, "P40306-FP5K-D-4-6-7-F")]
        [InlineData(.625, "FP6K-A", "SL", "C4x4.5", ".75", true, "P40306-FP6K-A-4-6-7-F")]
        [InlineData(.625, "FP6K-B", "SL", "C4x4.5", ".75", true, "P40306-FP6K-B-4-6-7-F")]
        [InlineData(.625, "FP1K-A", "SL", "C5x6.7", ".75", true, "P40306-FP1K-A-5-7-7-F")]
        [InlineData(.625, "FP1K-B", "SL", "C5x6.7", ".75", true, "P40306-FP1K-B-5-7-7-F")]
        [InlineData(.625, "FP1K-C", "SL", "C5x6.7", ".75", true, "P40306-FP1K-C-5-7-7-F")]
        [InlineData(.625, "FP1K-D", "SL", "C5x6.7", ".75", true, "P40306-FP1K-D-5-7-7-F")]
        [InlineData(.625, "FP2K-A", "SL", "C5x6.7", ".75", true, "P40306-FP2K-A-5-7-7-F")]
        [InlineData(.625, "FP2K-B", "SL", "C5x6.7", ".75", true, "P40306-FP2K-B-5-7-7-F")]
        [InlineData(.625, "FP3K-A", "SL", "C5x6.7", ".75", true, "P40306-FP3K-A-5-7-7-F")]
        [InlineData(.625, "FP3K-B", "SL", "C5x6.7", ".75", true, "P40306-FP3K-B-5-7-7-F")]
        [InlineData(.625, "FP3K-C", "SL", "C5x6.7", ".75", true, "P40306-FP3K-C-5-7-7-F")]
        [InlineData(.625, "FP3K-D", "SL", "C5x6.7", ".75", true, "P40306-FP3K-D-5-7-7-F")]
        [InlineData(.625, "FP4K-A", "SL", "C5x6.7", ".75", true, "P40306-FP4K-A-5-7-7-F")]
        [InlineData(.625, "FP4K-B", "SL", "C5x6.7", ".75", true, "P40306-FP4K-B-5-7-7-F")]
        [InlineData(.625, "FP5K-A", "SL", "C5x6.7", ".75", true, "P40306-FP5K-A-5-7-7-F")]
        [InlineData(.625, "FP5K-B", "SL", "C5x6.7", ".75", true, "P40306-FP5K-B-5-7-7-F")]
        [InlineData(.625, "FP5K-C", "SL", "C5x6.7", ".75", true, "P40306-FP5K-C-5-7-7-F")]
        [InlineData(.625, "FP5K-D", "SL", "C5x6.7", ".75", true, "P40306-FP5K-D-5-7-7-F")]
        [InlineData(.625, "FP6K-A", "SL", "C5x6.7", ".75", true, "P40306-FP6K-A-5-7-7-F")]
        [InlineData(.625, "FP6K-B", "SL", "C5x6.7", ".75", true, "P40306-FP6K-B-5-7-7-F")]

        //rEAR
        [InlineData(.375, "FP1K-A", "S", "C3x3.5", ".5", false, "P40304-FP1K-A-3-4-5-R")]
        [InlineData(.375, "FP1K-B", "S", "C3x3.5", ".5", false, "P40304-FP1K-B-3-4-5-R")]
        [InlineData(.375, "FP1K-C", "S", "C3x3.5", ".5", false, "P40304-FP1K-C-3-4-5-R")]
        [InlineData(.375, "FP1K-D", "S", "C3x3.5", ".5", false, "P40304-FP1K-D-3-4-5-R")]
        [InlineData(.375, "FP2K-A", "S", "C3x3.5", ".5", false, "P40304-FP2K-A-3-4-5-R")]
        [InlineData(.375, "FP2K-B", "S", "C3x3.5", ".5", false, "P40304-FP2K-B-3-4-5-R")]
        [InlineData(.375, "FP3K-A", "S", "C3x3.5", ".5", false, "P40304-FP3K-A-3-4-5-R")]
        [InlineData(.375, "FP3K-B", "S", "C3x3.5", ".5", false, "P40304-FP3K-B-3-4-5-R")]
        [InlineData(.375, "FP3K-C", "S", "C3x3.5", ".5", false, "P40304-FP3K-C-3-4-5-R")]
        [InlineData(.375, "FP3K-D", "S", "C3x3.5", ".5", false, "P40304-FP3K-D-3-4-5-R")]
        [InlineData(.375, "FP4K-A", "S", "C3x3.5", ".5", false, "P40304-FP4K-A-3-4-5-R")]
        [InlineData(.375, "FP4K-B", "S", "C3x3.5", ".5", false, "P40304-FP4K-B-3-4-5-R")]
        [InlineData(.375, "FP5K-A", "S", "C3x3.5", ".5", false, "P40304-FP5K-A-3-4-5-R")]
        [InlineData(.375, "FP5K-B", "S", "C3x3.5", ".5", false, "P40304-FP5K-B-3-4-5-R")]
        [InlineData(.375, "FP5K-C", "S", "C3x3.5", ".5", false, "P40304-FP5K-C-3-4-5-R")]
        [InlineData(.375, "FP5K-D", "S", "C3x3.5", ".5", false, "P40304-FP5K-D-3-4-5-R")]
        [InlineData(.375, "FP6K-A", "S", "C3x3.5", ".5", false, "P40304-FP6K-A-3-4-5-R")]
        [InlineData(.375, "FP6K-B", "S", "C3x3.5", ".5", false, "P40304-FP6K-B-3-4-5-R")]
        [InlineData(.375, "FP1K-A", "S", "C4x4.5", ".5", false, "P40304-FP1K-A-4-5-5-R")]
        [InlineData(.375, "FP1K-B", "S", "C4x4.5", ".5", false, "P40304-FP1K-B-4-5-5-R")]
        [InlineData(.375, "FP1K-C", "S", "C4x4.5", ".5", false, "P40304-FP1K-C-4-5-5-R")]
        [InlineData(.375, "FP1K-D", "S", "C4x4.5", ".5", false, "P40304-FP1K-D-4-5-5-R")]
        [InlineData(.375, "FP2K-A", "S", "C4x4.5", ".5", false, "P40304-FP2K-A-4-5-5-R")]
        [InlineData(.375, "FP2K-B", "S", "C4x4.5", ".5", false, "P40304-FP2K-B-4-5-5-R")]
        [InlineData(.375, "FP3K-A", "S", "C4x4.5", ".5", false, "P40304-FP3K-A-4-5-5-R")]
        [InlineData(.375, "FP3K-B", "S", "C4x4.5", ".5", false, "P40304-FP3K-B-4-5-5-R")]
        [InlineData(.375, "FP3K-C", "S", "C4x4.5", ".5", false, "P40304-FP3K-C-4-5-5-R")]
        [InlineData(.375, "FP3K-D", "S", "C4x4.5", ".5", false, "P40304-FP3K-D-4-5-5-R")]
        [InlineData(.375, "FP4K-A", "S", "C4x4.5", ".5", false, "P40304-FP4K-A-4-5-5-R")]
        [InlineData(.375, "FP4K-B", "S", "C4x4.5", ".5", false, "P40304-FP4K-B-4-5-5-R")]
        [InlineData(.375, "FP5K-A", "S", "C4x4.5", ".5", false, "P40304-FP5K-A-4-5-5-R")]
        [InlineData(.375, "FP5K-B", "S", "C4x4.5", ".5", false, "P40304-FP5K-B-4-5-5-R")]
        [InlineData(.375, "FP5K-C", "S", "C4x4.5", ".5", false, "P40304-FP5K-C-4-5-5-R")]
        [InlineData(.375, "FP5K-D", "S", "C4x4.5", ".5", false, "P40304-FP5K-D-4-5-5-R")]
        [InlineData(.375, "FP6K-A", "S", "C4x4.5", ".5", false, "P40304-FP6K-A-4-5-5-R")]
        [InlineData(.375, "FP6K-B", "S", "C4x4.5", ".5", false, "P40304-FP6K-B-4-5-5-R")]
        [InlineData(.375, "FP1K-A", "S", "C5x6.7", ".5", false, "P40304-FP1K-A-5-6-5-R")]
        [InlineData(.375, "FP1K-B", "S", "C5x6.7", ".5", false, "P40304-FP1K-B-5-6-5-R")]
        [InlineData(.375, "FP1K-C", "S", "C5x6.7", ".5", false, "P40304-FP1K-C-5-6-5-R")]
        [InlineData(.375, "FP1K-D", "S", "C5x6.7", ".5", false, "P40304-FP1K-D-5-6-5-R")]
        [InlineData(.375, "FP2K-A", "S", "C5x6.7", ".5", false, "P40304-FP2K-A-5-6-5-R")]
        [InlineData(.375, "FP2K-B", "S", "C5x6.7", ".5", false, "P40304-FP2K-B-5-6-5-R")]
        [InlineData(.375, "FP3K-A", "S", "C5x6.7", ".5", false, "P40304-FP3K-A-5-6-5-R")]
        [InlineData(.375, "FP3K-B", "S", "C5x6.7", ".5", false, "P40304-FP3K-B-5-6-5-R")]
        [InlineData(.375, "FP3K-C", "S", "C5x6.7", ".5", false, "P40304-FP3K-C-5-6-5-R")]
        [InlineData(.375, "FP3K-D", "S", "C5x6.7", ".5", false, "P40304-FP3K-D-5-6-5-R")]
        [InlineData(.375, "FP4K-A", "S", "C5x6.7", ".5", false, "P40304-FP4K-A-5-6-5-R")]
        [InlineData(.375, "FP4K-B", "S", "C5x6.7", ".5", false, "P40304-FP4K-B-5-6-5-R")]
        [InlineData(.375, "FP5K-A", "S", "C5x6.7", ".5", false, "P40304-FP5K-A-5-6-5-R")]
        [InlineData(.375, "FP5K-B", "S", "C5x6.7", ".5", false, "P40304-FP5K-B-5-6-5-R")]
        [InlineData(.375, "FP5K-C", "S", "C5x6.7", ".5", false, "P40304-FP5K-C-5-6-5-R")]
        [InlineData(.375, "FP5K-D", "S", "C5x6.7", ".5", false, "P40304-FP5K-D-5-6-5-R")]
        [InlineData(.375, "FP6K-A", "S", "C5x6.7", ".5", false, "P40304-FP6K-A-5-6-5-R")]
        [InlineData(.375, "FP6K-B", "S", "C5x6.7", ".5", false, "P40304-FP6K-B-5-6-5-R")]
        [InlineData(.375, "FP1K-A", "S", "C3x3.5", ".625", false, "P40304-FP1K-A-3-4-6-R")]
        [InlineData(.375, "FP1K-B", "S", "C3x3.5", ".625", false, "P40304-FP1K-B-3-4-6-R")]
        [InlineData(.375, "FP1K-C", "S", "C3x3.5", ".625", false, "P40304-FP1K-C-3-4-6-R")]
        [InlineData(.375, "FP1K-D", "S", "C3x3.5", ".625", false, "P40304-FP1K-D-3-4-6-R")]
        [InlineData(.375, "FP2K-A", "S", "C3x3.5", ".625", false, "P40304-FP2K-A-3-4-6-R")]
        [InlineData(.375, "FP2K-B", "S", "C3x3.5", ".625", false, "P40304-FP2K-B-3-4-6-R")]
        [InlineData(.375, "FP3K-A", "S", "C3x3.5", ".625", false, "P40304-FP3K-A-3-4-6-R")]
        [InlineData(.375, "FP3K-B", "S", "C3x3.5", ".625", false, "P40304-FP3K-B-3-4-6-R")]
        [InlineData(.375, "FP3K-C", "S", "C3x3.5", ".625", false, "P40304-FP3K-C-3-4-6-R")]
        [InlineData(.375, "FP3K-D", "S", "C3x3.5", ".625", false, "P40304-FP3K-D-3-4-6-R")]
        [InlineData(.375, "FP4K-A", "S", "C3x3.5", ".625", false, "P40304-FP4K-A-3-4-6-R")]
        [InlineData(.375, "FP4K-B", "S", "C3x3.5", ".625", false, "P40304-FP4K-B-3-4-6-R")]
        [InlineData(.375, "FP5K-A", "S", "C3x3.5", ".625", false, "P40304-FP5K-A-3-4-6-R")]
        [InlineData(.375, "FP5K-B", "S", "C3x3.5", ".625", false, "P40304-FP5K-B-3-4-6-R")]
        [InlineData(.375, "FP5K-C", "S", "C3x3.5", ".625", false, "P40304-FP5K-C-3-4-6-R")]
        [InlineData(.375, "FP5K-D", "S", "C3x3.5", ".625", false, "P40304-FP5K-D-3-4-6-R")]
        [InlineData(.375, "FP6K-A", "S", "C3x3.5", ".625", false, "P40304-FP6K-A-3-4-6-R")]
        [InlineData(.375, "FP6K-B", "S", "C3x3.5", ".625", false, "P40304-FP6K-B-3-4-6-R")]
        [InlineData(.375, "FP1K-A", "S", "C4x4.5", ".625", false, "P40304-FP1K-A-4-5-6-R")]
        [InlineData(.375, "FP1K-B", "S", "C4x4.5", ".625", false, "P40304-FP1K-B-4-5-6-R")]
        [InlineData(.375, "FP1K-C", "S", "C4x4.5", ".625", false, "P40304-FP1K-C-4-5-6-R")]
        [InlineData(.375, "FP1K-D", "S", "C4x4.5", ".625", false, "P40304-FP1K-D-4-5-6-R")]
        [InlineData(.375, "FP2K-A", "S", "C4x4.5", ".625", false, "P40304-FP2K-A-4-5-6-R")]
        [InlineData(.375, "FP2K-B", "S", "C4x4.5", ".625", false, "P40304-FP2K-B-4-5-6-R")]
        [InlineData(.375, "FP3K-A", "S", "C4x4.5", ".625", false, "P40304-FP3K-A-4-5-6-R")]
        [InlineData(.375, "FP3K-B", "S", "C4x4.5", ".625", false, "P40304-FP3K-B-4-5-6-R")]
        [InlineData(.375, "FP3K-C", "S", "C4x4.5", ".625", false, "P40304-FP3K-C-4-5-6-R")]
        [InlineData(.375, "FP3K-D", "S", "C4x4.5", ".625", false, "P40304-FP3K-D-4-5-6-R")]
        [InlineData(.375, "FP4K-A", "S", "C4x4.5", ".625", false, "P40304-FP4K-A-4-5-6-R")]
        [InlineData(.375, "FP4K-B", "S", "C4x4.5", ".625", false, "P40304-FP4K-B-4-5-6-R")]
        [InlineData(.375, "FP5K-A", "S", "C4x4.5", ".625", false, "P40304-FP5K-A-4-5-6-R")]
        [InlineData(.375, "FP5K-B", "S", "C4x4.5", ".625", false, "P40304-FP5K-B-4-5-6-R")]
        [InlineData(.375, "FP5K-C", "S", "C4x4.5", ".625", false, "P40304-FP5K-C-4-5-6-R")]
        [InlineData(.375, "FP5K-D", "S", "C4x4.5", ".625", false, "P40304-FP5K-D-4-5-6-R")]
        [InlineData(.375, "FP6K-A", "S", "C4x4.5", ".625", false, "P40304-FP6K-A-4-5-6-R")]
        [InlineData(.375, "FP6K-B", "S", "C4x4.5", ".625", false, "P40304-FP6K-B-4-5-6-R")]
        [InlineData(.375, "FP1K-A", "S", "C5x6.7", ".625", false, "P40304-FP1K-A-5-6-6-R")]
        [InlineData(.375, "FP1K-B", "S", "C5x6.7", ".625", false, "P40304-FP1K-B-5-6-6-R")]
        [InlineData(.375, "FP1K-C", "S", "C5x6.7", ".625", false, "P40304-FP1K-C-5-6-6-R")]
        [InlineData(.375, "FP1K-D", "S", "C5x6.7", ".625", false, "P40304-FP1K-D-5-6-6-R")]
        [InlineData(.375, "FP2K-A", "S", "C5x6.7", ".625", false, "P40304-FP2K-A-5-6-6-R")]
        [InlineData(.375, "FP2K-B", "S", "C5x6.7", ".625", false, "P40304-FP2K-B-5-6-6-R")]
        [InlineData(.375, "FP3K-A", "S", "C5x6.7", ".625", false, "P40304-FP3K-A-5-6-6-R")]
        [InlineData(.375, "FP3K-B", "S", "C5x6.7", ".625", false, "P40304-FP3K-B-5-6-6-R")]
        [InlineData(.375, "FP3K-C", "S", "C5x6.7", ".625", false, "P40304-FP3K-C-5-6-6-R")]
        [InlineData(.375, "FP3K-D", "S", "C5x6.7", ".625", false, "P40304-FP3K-D-5-6-6-R")]
        [InlineData(.375, "FP4K-A", "S", "C5x6.7", ".625", false, "P40304-FP4K-A-5-6-6-R")]
        [InlineData(.375, "FP4K-B", "S", "C5x6.7", ".625", false, "P40304-FP4K-B-5-6-6-R")]
        [InlineData(.375, "FP5K-A", "S", "C5x6.7", ".625", false, "P40304-FP5K-A-5-6-6-R")]
        [InlineData(.375, "FP5K-B", "S", "C5x6.7", ".625", false, "P40304-FP5K-B-5-6-6-R")]
        [InlineData(.375, "FP5K-C", "S", "C5x6.7", ".625", false, "P40304-FP5K-C-5-6-6-R")]
        [InlineData(.375, "FP5K-D", "S", "C5x6.7", ".625", false, "P40304-FP5K-D-5-6-6-R")]
        [InlineData(.375, "FP6K-A", "S", "C5x6.7", ".625", false, "P40304-FP6K-A-5-6-6-R")]
        [InlineData(.375, "FP6K-B", "S", "C5x6.7", ".625", false, "P40304-FP6K-B-5-6-6-R")]
        [InlineData(.375, "FP1K-A", "S", "C3x3.5", ".75", false, "P40304-FP1K-A-3-4-7-R")]
        [InlineData(.375, "FP1K-B", "S", "C3x3.5", ".75", false, "P40304-FP1K-B-3-4-7-R")]
        [InlineData(.375, "FP1K-C", "S", "C3x3.5", ".75", false, "P40304-FP1K-C-3-4-7-R")]
        [InlineData(.375, "FP1K-D", "S", "C3x3.5", ".75", false, "P40304-FP1K-D-3-4-7-R")]
        [InlineData(.375, "FP2K-A", "S", "C3x3.5", ".75", false, "P40304-FP2K-A-3-4-7-R")]
        [InlineData(.375, "FP2K-B", "S", "C3x3.5", ".75", false, "P40304-FP2K-B-3-4-7-R")]
        [InlineData(.375, "FP3K-A", "S", "C3x3.5", ".75", false, "P40304-FP3K-A-3-4-7-R")]
        [InlineData(.375, "FP3K-B", "S", "C3x3.5", ".75", false, "P40304-FP3K-B-3-4-7-R")]
        [InlineData(.375, "FP3K-C", "S", "C3x3.5", ".75", false, "P40304-FP3K-C-3-4-7-R")]
        [InlineData(.375, "FP3K-D", "S", "C3x3.5", ".75", false, "P40304-FP3K-D-3-4-7-R")]
        [InlineData(.375, "FP4K-A", "S", "C3x3.5", ".75", false, "P40304-FP4K-A-3-4-7-R")]
        [InlineData(.375, "FP4K-B", "S", "C3x3.5", ".75", false, "P40304-FP4K-B-3-4-7-R")]
        [InlineData(.375, "FP5K-A", "S", "C3x3.5", ".75", false, "P40304-FP5K-A-3-4-7-R")]
        [InlineData(.375, "FP5K-B", "S", "C3x3.5", ".75", false, "P40304-FP5K-B-3-4-7-R")]
        [InlineData(.375, "FP5K-C", "S", "C3x3.5", ".75", false, "P40304-FP5K-C-3-4-7-R")]
        [InlineData(.375, "FP5K-D", "S", "C3x3.5", ".75", false, "P40304-FP5K-D-3-4-7-R")]
        [InlineData(.375, "FP6K-A", "S", "C3x3.5", ".75", false, "P40304-FP6K-A-3-4-7-R")]
        [InlineData(.375, "FP6K-B", "S", "C3x3.5", ".75", false, "P40304-FP6K-B-3-4-7-R")]
        [InlineData(.375, "FP1K-A", "S", "C4x4.5", ".75", false, "P40304-FP1K-A-4-5-7-R")]
        [InlineData(.375, "FP1K-B", "S", "C4x4.5", ".75", false, "P40304-FP1K-B-4-5-7-R")]
        [InlineData(.375, "FP1K-C", "S", "C4x4.5", ".75", false, "P40304-FP1K-C-4-5-7-R")]
        [InlineData(.375, "FP1K-D", "S", "C4x4.5", ".75", false, "P40304-FP1K-D-4-5-7-R")]
        [InlineData(.375, "FP2K-A", "S", "C4x4.5", ".75", false, "P40304-FP2K-A-4-5-7-R")]
        [InlineData(.375, "FP2K-B", "S", "C4x4.5", ".75", false, "P40304-FP2K-B-4-5-7-R")]
        [InlineData(.375, "FP3K-A", "S", "C4x4.5", ".75", false, "P40304-FP3K-A-4-5-7-R")]
        [InlineData(.375, "FP3K-B", "S", "C4x4.5", ".75", false, "P40304-FP3K-B-4-5-7-R")]
        [InlineData(.375, "FP3K-C", "S", "C4x4.5", ".75", false, "P40304-FP3K-C-4-5-7-R")]
        [InlineData(.375, "FP3K-D", "S", "C4x4.5", ".75", false, "P40304-FP3K-D-4-5-7-R")]
        [InlineData(.375, "FP4K-A", "S", "C4x4.5", ".75", false, "P40304-FP4K-A-4-5-7-R")]
        [InlineData(.375, "FP4K-B", "S", "C4x4.5", ".75", false, "P40304-FP4K-B-4-5-7-R")]
        [InlineData(.375, "FP5K-A", "S", "C4x4.5", ".75", false, "P40304-FP5K-A-4-5-7-R")]
        [InlineData(.375, "FP5K-B", "S", "C4x4.5", ".75", false, "P40304-FP5K-B-4-5-7-R")]
        [InlineData(.375, "FP5K-C", "S", "C4x4.5", ".75", false, "P40304-FP5K-C-4-5-7-R")]
        [InlineData(.375, "FP5K-D", "S", "C4x4.5", ".75", false, "P40304-FP5K-D-4-5-7-R")]
        [InlineData(.375, "FP6K-A", "S", "C4x4.5", ".75", false, "P40304-FP6K-A-4-5-7-R")]
        [InlineData(.375, "FP6K-B", "S", "C4x4.5", ".75", false, "P40304-FP6K-B-4-5-7-R")]
        [InlineData(.375, "FP1K-A", "S", "C5x6.7", ".75", false, "P40304-FP1K-A-5-6-7-R")]
        [InlineData(.375, "FP1K-B", "S", "C5x6.7", ".75", false, "P40304-FP1K-B-5-6-7-R")]
        [InlineData(.375, "FP1K-C", "S", "C5x6.7", ".75", false, "P40304-FP1K-C-5-6-7-R")]
        [InlineData(.375, "FP1K-D", "S", "C5x6.7", ".75", false, "P40304-FP1K-D-5-6-7-R")]
        [InlineData(.375, "FP2K-A", "S", "C5x6.7", ".75", false, "P40304-FP2K-A-5-6-7-R")]
        [InlineData(.375, "FP2K-B", "S", "C5x6.7", ".75", false, "P40304-FP2K-B-5-6-7-R")]
        [InlineData(.375, "FP3K-A", "S", "C5x6.7", ".75", false, "P40304-FP3K-A-5-6-7-R")]
        [InlineData(.375, "FP3K-B", "S", "C5x6.7", ".75", false, "P40304-FP3K-B-5-6-7-R")]
        [InlineData(.375, "FP3K-C", "S", "C5x6.7", ".75", false, "P40304-FP3K-C-5-6-7-R")]
        [InlineData(.375, "FP3K-D", "S", "C5x6.7", ".75", false, "P40304-FP3K-D-5-6-7-R")]
        [InlineData(.375, "FP4K-A", "S", "C5x6.7", ".75", false, "P40304-FP4K-A-5-6-7-R")]
        [InlineData(.375, "FP4K-B", "S", "C5x6.7", ".75", false, "P40304-FP4K-B-5-6-7-R")]
        [InlineData(.375, "FP5K-A", "S", "C5x6.7", ".75", false, "P40304-FP5K-A-5-6-7-R")]
        [InlineData(.375, "FP5K-B", "S", "C5x6.7", ".75", false, "P40304-FP5K-B-5-6-7-R")]
        [InlineData(.375, "FP5K-C", "S", "C5x6.7", ".75", false, "P40304-FP5K-C-5-6-7-R")]
        [InlineData(.375, "FP5K-D", "S", "C5x6.7", ".75", false, "P40304-FP5K-D-5-6-7-R")]
        [InlineData(.375, "FP6K-A", "S", "C5x6.7", ".75", false, "P40304-FP6K-A-5-6-7-R")]
        [InlineData(.375, "FP6K-B", "S", "C5x6.7", ".75", false, "P40304-FP6K-B-5-6-7-R")]
        //1/2in plate thickness
        [InlineData(.5, "FP1K-A", "S", "C3x3.5", ".5", false, "P40305-FP1K-A-3-4-5-R")]
        [InlineData(.5, "FP1K-B", "S", "C3x3.5", ".5", false, "P40305-FP1K-B-3-4-5-R")]
        [InlineData(.5, "FP1K-C", "S", "C3x3.5", ".5", false, "P40305-FP1K-C-3-4-5-R")]
        [InlineData(.5, "FP1K-D", "S", "C3x3.5", ".5", false, "P40305-FP1K-D-3-4-5-R")]
        [InlineData(.5, "FP2K-A", "S", "C3x3.5", ".5", false, "P40305-FP2K-A-3-4-5-R")]
        [InlineData(.5, "FP2K-B", "S", "C3x3.5", ".5", false, "P40305-FP2K-B-3-4-5-R")]
        [InlineData(.5, "FP3K-A", "S", "C3x3.5", ".5", false, "P40305-FP3K-A-3-4-5-R")]
        [InlineData(.5, "FP3K-B", "S", "C3x3.5", ".5", false, "P40305-FP3K-B-3-4-5-R")]
        [InlineData(.5, "FP3K-C", "S", "C3x3.5", ".5", false, "P40305-FP3K-C-3-4-5-R")]
        [InlineData(.5, "FP3K-D", "S", "C3x3.5", ".5", false, "P40305-FP3K-D-3-4-5-R")]
        [InlineData(.5, "FP4K-A", "S", "C3x3.5", ".5", false, "P40305-FP4K-A-3-4-5-R")]
        [InlineData(.5, "FP4K-B", "S", "C3x3.5", ".5", false, "P40305-FP4K-B-3-4-5-R")]
        [InlineData(.5, "FP5K-A", "S", "C3x3.5", ".5", false, "P40305-FP5K-A-3-4-5-R")]
        [InlineData(.5, "FP5K-B", "S", "C3x3.5", ".5", false, "P40305-FP5K-B-3-4-5-R")]
        [InlineData(.5, "FP5K-C", "S", "C3x3.5", ".5", false, "P40305-FP5K-C-3-4-5-R")]
        [InlineData(.5, "FP5K-D", "S", "C3x3.5", ".5", false, "P40305-FP5K-D-3-4-5-R")]
        [InlineData(.5, "FP6K-A", "S", "C3x3.5", ".5", false, "P40305-FP6K-A-3-4-5-R")]
        [InlineData(.5, "FP6K-B", "S", "C3x3.5", ".5", false, "P40305-FP6K-B-3-4-5-R")]
        [InlineData(.5, "FP1K-A", "S", "C4x4.5", ".5", false, "P40305-FP1K-A-4-5-5-R")]
        [InlineData(.5, "FP1K-B", "S", "C4x4.5", ".5", false, "P40305-FP1K-B-4-5-5-R")]
        [InlineData(.5, "FP1K-C", "S", "C4x4.5", ".5", false, "P40305-FP1K-C-4-5-5-R")]
        [InlineData(.5, "FP1K-D", "S", "C4x4.5", ".5", false, "P40305-FP1K-D-4-5-5-R")]
        [InlineData(.5, "FP2K-A", "S", "C4x4.5", ".5", false, "P40305-FP2K-A-4-5-5-R")]
        [InlineData(.5, "FP2K-B", "S", "C4x4.5", ".5", false, "P40305-FP2K-B-4-5-5-R")]
        [InlineData(.5, "FP3K-A", "S", "C4x4.5", ".5", false, "P40305-FP3K-A-4-5-5-R")]
        [InlineData(.5, "FP3K-B", "S", "C4x4.5", ".5", false, "P40305-FP3K-B-4-5-5-R")]
        [InlineData(.5, "FP3K-C", "S", "C4x4.5", ".5", false, "P40305-FP3K-C-4-5-5-R")]
        [InlineData(.5, "FP3K-D", "S", "C4x4.5", ".5", false, "P40305-FP3K-D-4-5-5-R")]
        [InlineData(.5, "FP4K-A", "S", "C4x4.5", ".5", false, "P40305-FP4K-A-4-5-5-R")]
        [InlineData(.5, "FP4K-B", "S", "C4x4.5", ".5", false, "P40305-FP4K-B-4-5-5-R")]
        [InlineData(.5, "FP5K-A", "S", "C4x4.5", ".5", false, "P40305-FP5K-A-4-5-5-R")]
        [InlineData(.5, "FP5K-B", "S", "C4x4.5", ".5", false, "P40305-FP5K-B-4-5-5-R")]
        [InlineData(.5, "FP5K-C", "S", "C4x4.5", ".5", false, "P40305-FP5K-C-4-5-5-R")]
        [InlineData(.5, "FP5K-D", "S", "C4x4.5", ".5", false, "P40305-FP5K-D-4-5-5-R")]
        [InlineData(.5, "FP6K-A", "S", "C4x4.5", ".5", false, "P40305-FP6K-A-4-5-5-R")]
        [InlineData(.5, "FP6K-B", "S", "C4x4.5", ".5", false, "P40305-FP6K-B-4-5-5-R")]
        [InlineData(.5, "FP1K-A", "S", "C5x6.7", ".5", false, "P40305-FP1K-A-5-6-5-R")]
        [InlineData(.5, "FP1K-B", "S", "C5x6.7", ".5", false, "P40305-FP1K-B-5-6-5-R")]
        [InlineData(.5, "FP1K-C", "S", "C5x6.7", ".5", false, "P40305-FP1K-C-5-6-5-R")]
        [InlineData(.5, "FP1K-D", "S", "C5x6.7", ".5", false, "P40305-FP1K-D-5-6-5-R")]
        [InlineData(.5, "FP2K-A", "S", "C5x6.7", ".5", false, "P40305-FP2K-A-5-6-5-R")]
        [InlineData(.5, "FP2K-B", "S", "C5x6.7", ".5", false, "P40305-FP2K-B-5-6-5-R")]
        [InlineData(.5, "FP3K-A", "S", "C5x6.7", ".5", false, "P40305-FP3K-A-5-6-5-R")]
        [InlineData(.5, "FP3K-B", "S", "C5x6.7", ".5", false, "P40305-FP3K-B-5-6-5-R")]
        [InlineData(.5, "FP3K-C", "S", "C5x6.7", ".5", false, "P40305-FP3K-C-5-6-5-R")]
        [InlineData(.5, "FP3K-D", "S", "C5x6.7", ".5", false, "P40305-FP3K-D-5-6-5-R")]
        [InlineData(.5, "FP4K-A", "S", "C5x6.7", ".5", false, "P40305-FP4K-A-5-6-5-R")]
        [InlineData(.5, "FP4K-B", "S", "C5x6.7", ".5", false, "P40305-FP4K-B-5-6-5-R")]
        [InlineData(.5, "FP5K-A", "S", "C5x6.7", ".5", false, "P40305-FP5K-A-5-6-5-R")]
        [InlineData(.5, "FP5K-B", "S", "C5x6.7", ".5", false, "P40305-FP5K-B-5-6-5-R")]
        [InlineData(.5, "FP5K-C", "S", "C5x6.7", ".5", false, "P40305-FP5K-C-5-6-5-R")]
        [InlineData(.5, "FP5K-D", "S", "C5x6.7", ".5", false, "P40305-FP5K-D-5-6-5-R")]
        [InlineData(.5, "FP6K-A", "S", "C5x6.7", ".5", false, "P40305-FP6K-A-5-6-5-R")]
        [InlineData(.5, "FP6K-B", "S", "C5x6.7", ".5", false, "P40305-FP6K-B-5-6-5-R")]
        [InlineData(.5, "FP1K-A", "S", "C3x3.5", ".625", false, "P40305-FP1K-A-3-4-6-R")]
        [InlineData(.5, "FP1K-B", "S", "C3x3.5", ".625", false, "P40305-FP1K-B-3-4-6-R")]
        [InlineData(.5, "FP1K-C", "S", "C3x3.5", ".625", false, "P40305-FP1K-C-3-4-6-R")]
        [InlineData(.5, "FP1K-D", "S", "C3x3.5", ".625", false, "P40305-FP1K-D-3-4-6-R")]
        [InlineData(.5, "FP2K-A", "S", "C3x3.5", ".625", false, "P40305-FP2K-A-3-4-6-R")]
        [InlineData(.5, "FP2K-B", "S", "C3x3.5", ".625", false, "P40305-FP2K-B-3-4-6-R")]
        [InlineData(.5, "FP3K-A", "S", "C3x3.5", ".625", false, "P40305-FP3K-A-3-4-6-R")]
        [InlineData(.5, "FP3K-B", "S", "C3x3.5", ".625", false, "P40305-FP3K-B-3-4-6-R")]
        [InlineData(.5, "FP3K-C", "S", "C3x3.5", ".625", false, "P40305-FP3K-C-3-4-6-R")]
        [InlineData(.5, "FP3K-D", "S", "C3x3.5", ".625", false, "P40305-FP3K-D-3-4-6-R")]
        [InlineData(.5, "FP4K-A", "S", "C3x3.5", ".625", false, "P40305-FP4K-A-3-4-6-R")]
        [InlineData(.5, "FP4K-B", "S", "C3x3.5", ".625", false, "P40305-FP4K-B-3-4-6-R")]
        [InlineData(.5, "FP5K-A", "S", "C3x3.5", ".625", false, "P40305-FP5K-A-3-4-6-R")]
        [InlineData(.5, "FP5K-B", "S", "C3x3.5", ".625", false, "P40305-FP5K-B-3-4-6-R")]
        [InlineData(.5, "FP5K-C", "S", "C3x3.5", ".625", false, "P40305-FP5K-C-3-4-6-R")]
        [InlineData(.5, "FP5K-D", "S", "C3x3.5", ".625", false, "P40305-FP5K-D-3-4-6-R")]
        [InlineData(.5, "FP6K-A", "S", "C3x3.5", ".625", false, "P40305-FP6K-A-3-4-6-R")]
        [InlineData(.5, "FP6K-B", "S", "C3x3.5", ".625", false, "P40305-FP6K-B-3-4-6-R")]
        [InlineData(.5, "FP1K-A", "S", "C4x4.5", ".625", false, "P40305-FP1K-A-4-5-6-R")]
        [InlineData(.5, "FP1K-B", "S", "C4x4.5", ".625", false, "P40305-FP1K-B-4-5-6-R")]
        [InlineData(.5, "FP1K-C", "S", "C4x4.5", ".625", false, "P40305-FP1K-C-4-5-6-R")]
        [InlineData(.5, "FP1K-D", "S", "C4x4.5", ".625", false, "P40305-FP1K-D-4-5-6-R")]
        [InlineData(.5, "FP2K-A", "S", "C4x4.5", ".625", false, "P40305-FP2K-A-4-5-6-R")]
        [InlineData(.5, "FP2K-B", "S", "C4x4.5", ".625", false, "P40305-FP2K-B-4-5-6-R")]
        [InlineData(.5, "FP3K-A", "S", "C4x4.5", ".625", false, "P40305-FP3K-A-4-5-6-R")]
        [InlineData(.5, "FP3K-B", "S", "C4x4.5", ".625", false, "P40305-FP3K-B-4-5-6-R")]
        [InlineData(.5, "FP3K-C", "S", "C4x4.5", ".625", false, "P40305-FP3K-C-4-5-6-R")]
        [InlineData(.5, "FP3K-D", "S", "C4x4.5", ".625", false, "P40305-FP3K-D-4-5-6-R")]
        [InlineData(.5, "FP4K-A", "S", "C4x4.5", ".625", false, "P40305-FP4K-A-4-5-6-R")]
        [InlineData(.5, "FP4K-B", "S", "C4x4.5", ".625", false, "P40305-FP4K-B-4-5-6-R")]
        [InlineData(.5, "FP5K-A", "S", "C4x4.5", ".625", false, "P40305-FP5K-A-4-5-6-R")]
        [InlineData(.5, "FP5K-B", "S", "C4x4.5", ".625", false, "P40305-FP5K-B-4-5-6-R")]
        [InlineData(.5, "FP5K-C", "S", "C4x4.5", ".625", false, "P40305-FP5K-C-4-5-6-R")]
        [InlineData(.5, "FP5K-D", "S", "C4x4.5", ".625", false, "P40305-FP5K-D-4-5-6-R")]
        [InlineData(.5, "FP6K-A", "S", "C4x4.5", ".625", false, "P40305-FP6K-A-4-5-6-R")]
        [InlineData(.5, "FP6K-B", "S", "C4x4.5", ".625", false, "P40305-FP6K-B-4-5-6-R")]
        [InlineData(.5, "FP1K-A", "S", "C5x6.7", ".625", false, "P40305-FP1K-A-5-6-6-R")]
        [InlineData(.5, "FP1K-B", "S", "C5x6.7", ".625", false, "P40305-FP1K-B-5-6-6-R")]
        [InlineData(.5, "FP1K-C", "S", "C5x6.7", ".625", false, "P40305-FP1K-C-5-6-6-R")]
        [InlineData(.5, "FP1K-D", "S", "C5x6.7", ".625", false, "P40305-FP1K-D-5-6-6-R")]
        [InlineData(.5, "FP2K-A", "S", "C5x6.7", ".625", false, "P40305-FP2K-A-5-6-6-R")]
        [InlineData(.5, "FP2K-B", "S", "C5x6.7", ".625", false, "P40305-FP2K-B-5-6-6-R")]
        [InlineData(.5, "FP3K-A", "S", "C5x6.7", ".625", false, "P40305-FP3K-A-5-6-6-R")]
        [InlineData(.5, "FP3K-B", "S", "C5x6.7", ".625", false, "P40305-FP3K-B-5-6-6-R")]
        [InlineData(.5, "FP3K-C", "S", "C5x6.7", ".625", false, "P40305-FP3K-C-5-6-6-R")]
        [InlineData(.5, "FP3K-D", "S", "C5x6.7", ".625", false, "P40305-FP3K-D-5-6-6-R")]
        [InlineData(.5, "FP4K-A", "S", "C5x6.7", ".625", false, "P40305-FP4K-A-5-6-6-R")]
        [InlineData(.5, "FP4K-B", "S", "C5x6.7", ".625", false, "P40305-FP4K-B-5-6-6-R")]
        [InlineData(.5, "FP5K-A", "S", "C5x6.7", ".625", false, "P40305-FP5K-A-5-6-6-R")]
        [InlineData(.5, "FP5K-B", "S", "C5x6.7", ".625", false, "P40305-FP5K-B-5-6-6-R")]
        [InlineData(.5, "FP5K-C", "S", "C5x6.7", ".625", false, "P40305-FP5K-C-5-6-6-R")]
        [InlineData(.5, "FP5K-D", "S", "C5x6.7", ".625", false, "P40305-FP5K-D-5-6-6-R")]
        [InlineData(.5, "FP6K-A", "S", "C5x6.7", ".625", false, "P40305-FP6K-A-5-6-6-R")]
        [InlineData(.5, "FP6K-B", "S", "C5x6.7", ".625", false, "P40305-FP6K-B-5-6-6-R")]
        [InlineData(.5, "FP1K-A", "S", "C3x3.5", ".75", false, "P40305-FP1K-A-3-4-7-R")]
        [InlineData(.5, "FP1K-B", "S", "C3x3.5", ".75", false, "P40305-FP1K-B-3-4-7-R")]
        [InlineData(.5, "FP1K-C", "S", "C3x3.5", ".75", false, "P40305-FP1K-C-3-4-7-R")]
        [InlineData(.5, "FP1K-D", "S", "C3x3.5", ".75", false, "P40305-FP1K-D-3-4-7-R")]
        [InlineData(.5, "FP2K-A", "S", "C3x3.5", ".75", false, "P40305-FP2K-A-3-4-7-R")]
        [InlineData(.5, "FP2K-B", "S", "C3x3.5", ".75", false, "P40305-FP2K-B-3-4-7-R")]
        [InlineData(.5, "FP3K-A", "S", "C3x3.5", ".75", false, "P40305-FP3K-A-3-4-7-R")]
        [InlineData(.5, "FP3K-B", "S", "C3x3.5", ".75", false, "P40305-FP3K-B-3-4-7-R")]
        [InlineData(.5, "FP3K-C", "S", "C3x3.5", ".75", false, "P40305-FP3K-C-3-4-7-R")]
        [InlineData(.5, "FP3K-D", "S", "C3x3.5", ".75", false, "P40305-FP3K-D-3-4-7-R")]
        [InlineData(.5, "FP4K-A", "S", "C3x3.5", ".75", false, "P40305-FP4K-A-3-4-7-R")]
        [InlineData(.5, "FP4K-B", "S", "C3x3.5", ".75", false, "P40305-FP4K-B-3-4-7-R")]
        [InlineData(.5, "FP5K-A", "S", "C3x3.5", ".75", false, "P40305-FP5K-A-3-4-7-R")]
        [InlineData(.5, "FP5K-B", "S", "C3x3.5", ".75", false, "P40305-FP5K-B-3-4-7-R")]
        [InlineData(.5, "FP5K-C", "S", "C3x3.5", ".75", false, "P40305-FP5K-C-3-4-7-R")]
        [InlineData(.5, "FP5K-D", "S", "C3x3.5", ".75", false, "P40305-FP5K-D-3-4-7-R")]
        [InlineData(.5, "FP6K-A", "S", "C3x3.5", ".75", false, "P40305-FP6K-A-3-4-7-R")]
        [InlineData(.5, "FP6K-B", "S", "C3x3.5", ".75", false, "P40305-FP6K-B-3-4-7-R")]
        [InlineData(.5, "FP1K-A", "S", "C4x4.5", ".75", false, "P40305-FP1K-A-4-5-7-R")]
        [InlineData(.5, "FP1K-B", "S", "C4x4.5", ".75", false, "P40305-FP1K-B-4-5-7-R")]
        [InlineData(.5, "FP1K-C", "S", "C4x4.5", ".75", false, "P40305-FP1K-C-4-5-7-R")]
        [InlineData(.5, "FP1K-D", "S", "C4x4.5", ".75", false, "P40305-FP1K-D-4-5-7-R")]
        [InlineData(.5, "FP2K-A", "S", "C4x4.5", ".75", false, "P40305-FP2K-A-4-5-7-R")]
        [InlineData(.5, "FP2K-B", "S", "C4x4.5", ".75", false, "P40305-FP2K-B-4-5-7-R")]
        [InlineData(.5, "FP3K-A", "S", "C4x4.5", ".75", false, "P40305-FP3K-A-4-5-7-R")]
        [InlineData(.5, "FP3K-B", "S", "C4x4.5", ".75", false, "P40305-FP3K-B-4-5-7-R")]
        [InlineData(.5, "FP3K-C", "S", "C4x4.5", ".75", false, "P40305-FP3K-C-4-5-7-R")]
        [InlineData(.5, "FP3K-D", "S", "C4x4.5", ".75", false, "P40305-FP3K-D-4-5-7-R")]
        [InlineData(.5, "FP4K-A", "S", "C4x4.5", ".75", false, "P40305-FP4K-A-4-5-7-R")]
        [InlineData(.5, "FP4K-B", "S", "C4x4.5", ".75", false, "P40305-FP4K-B-4-5-7-R")]
        [InlineData(.5, "FP5K-A", "S", "C4x4.5", ".75", false, "P40305-FP5K-A-4-5-7-R")]
        [InlineData(.5, "FP5K-B", "S", "C4x4.5", ".75", false, "P40305-FP5K-B-4-5-7-R")]
        [InlineData(.5, "FP5K-C", "S", "C4x4.5", ".75", false, "P40305-FP5K-C-4-5-7-R")]
        [InlineData(.5, "FP5K-D", "S", "C4x4.5", ".75", false, "P40305-FP5K-D-4-5-7-R")]
        [InlineData(.5, "FP6K-A", "S", "C4x4.5", ".75", false, "P40305-FP6K-A-4-5-7-R")]
        [InlineData(.5, "FP6K-B", "S", "C4x4.5", ".75", false, "P40305-FP6K-B-4-5-7-R")]
        [InlineData(.5, "FP1K-A", "S", "C5x6.7", ".75", false, "P40305-FP1K-A-5-6-7-R")]
        [InlineData(.5, "FP1K-B", "S", "C5x6.7", ".75", false, "P40305-FP1K-B-5-6-7-R")]
        [InlineData(.5, "FP1K-C", "S", "C5x6.7", ".75", false, "P40305-FP1K-C-5-6-7-R")]
        [InlineData(.5, "FP1K-D", "S", "C5x6.7", ".75", false, "P40305-FP1K-D-5-6-7-R")]
        [InlineData(.5, "FP2K-A", "S", "C5x6.7", ".75", false, "P40305-FP2K-A-5-6-7-R")]
        [InlineData(.5, "FP2K-B", "S", "C5x6.7", ".75", false, "P40305-FP2K-B-5-6-7-R")]
        [InlineData(.5, "FP3K-A", "S", "C5x6.7", ".75", false, "P40305-FP3K-A-5-6-7-R")]
        [InlineData(.5, "FP3K-B", "S", "C5x6.7", ".75", false, "P40305-FP3K-B-5-6-7-R")]
        [InlineData(.5, "FP3K-C", "S", "C5x6.7", ".75", false, "P40305-FP3K-C-5-6-7-R")]
        [InlineData(.5, "FP3K-D", "S", "C5x6.7", ".75", false, "P40305-FP3K-D-5-6-7-R")]
        [InlineData(.5, "FP4K-A", "S", "C5x6.7", ".75", false, "P40305-FP4K-A-5-6-7-R")]
        [InlineData(.5, "FP4K-B", "S", "C5x6.7", ".75", false, "P40305-FP4K-B-5-6-7-R")]
        [InlineData(.5, "FP5K-A", "S", "C5x6.7", ".75", false, "P40305-FP5K-A-5-6-7-R")]
        [InlineData(.5, "FP5K-B", "S", "C5x6.7", ".75", false, "P40305-FP5K-B-5-6-7-R")]
        [InlineData(.5, "FP5K-C", "S", "C5x6.7", ".75", false, "P40305-FP5K-C-5-6-7-R")]
        [InlineData(.5, "FP5K-D", "S", "C5x6.7", ".75", false, "P40305-FP5K-D-5-6-7-R")]
        [InlineData(.5, "FP6K-A", "S", "C5x6.7", ".75", false, "P40305-FP6K-A-5-6-7-R")]
        [InlineData(.5, "FP6K-B", "S", "C5x6.7", ".75", false, "P40305-FP6K-B-5-6-7-R")]
        //5/8in plate thickness
        [InlineData(.625, "FP1K-A", "S", "C3x3.5", ".5", false, "P40306-FP1K-A-3-4-5-R")]
        [InlineData(.625, "FP1K-B", "S", "C3x3.5", ".5", false, "P40306-FP1K-B-3-4-5-R")]
        [InlineData(.625, "FP1K-C", "S", "C3x3.5", ".5", false, "P40306-FP1K-C-3-4-5-R")]
        [InlineData(.625, "FP1K-D", "S", "C3x3.5", ".5", false, "P40306-FP1K-D-3-4-5-R")]
        [InlineData(.625, "FP2K-A", "S", "C3x3.5", ".5", false, "P40306-FP2K-A-3-4-5-R")]
        [InlineData(.625, "FP2K-B", "S", "C3x3.5", ".5", false, "P40306-FP2K-B-3-4-5-R")]
        [InlineData(.625, "FP3K-A", "S", "C3x3.5", ".5", false, "P40306-FP3K-A-3-4-5-R")]
        [InlineData(.625, "FP3K-B", "S", "C3x3.5", ".5", false, "P40306-FP3K-B-3-4-5-R")]
        [InlineData(.625, "FP3K-C", "S", "C3x3.5", ".5", false, "P40306-FP3K-C-3-4-5-R")]
        [InlineData(.625, "FP3K-D", "S", "C3x3.5", ".5", false, "P40306-FP3K-D-3-4-5-R")]
        [InlineData(.625, "FP4K-A", "S", "C3x3.5", ".5", false, "P40306-FP4K-A-3-4-5-R")]
        [InlineData(.625, "FP4K-B", "S", "C3x3.5", ".5", false, "P40306-FP4K-B-3-4-5-R")]
        [InlineData(.625, "FP5K-A", "S", "C3x3.5", ".5", false, "P40306-FP5K-A-3-4-5-R")]
        [InlineData(.625, "FP5K-B", "S", "C3x3.5", ".5", false, "P40306-FP5K-B-3-4-5-R")]
        [InlineData(.625, "FP5K-C", "S", "C3x3.5", ".5", false, "P40306-FP5K-C-3-4-5-R")]
        [InlineData(.625, "FP5K-D", "S", "C3x3.5", ".5", false, "P40306-FP5K-D-3-4-5-R")]
        [InlineData(.625, "FP6K-A", "S", "C3x3.5", ".5", false, "P40306-FP6K-A-3-4-5-R")]
        [InlineData(.625, "FP6K-B", "S", "C3x3.5", ".5", false, "P40306-FP6K-B-3-4-5-R")]
        [InlineData(.625, "FP1K-A", "S", "C4x4.5", ".5", false, "P40306-FP1K-A-4-5-5-R")]
        [InlineData(.625, "FP1K-B", "S", "C4x4.5", ".5", false, "P40306-FP1K-B-4-5-5-R")]
        [InlineData(.625, "FP1K-C", "S", "C4x4.5", ".5", false, "P40306-FP1K-C-4-5-5-R")]
        [InlineData(.625, "FP1K-D", "S", "C4x4.5", ".5", false, "P40306-FP1K-D-4-5-5-R")]
        [InlineData(.625, "FP2K-A", "S", "C4x4.5", ".5", false, "P40306-FP2K-A-4-5-5-R")]
        [InlineData(.625, "FP2K-B", "S", "C4x4.5", ".5", false, "P40306-FP2K-B-4-5-5-R")]
        [InlineData(.625, "FP3K-A", "S", "C4x4.5", ".5", false, "P40306-FP3K-A-4-5-5-R")]
        [InlineData(.625, "FP3K-B", "S", "C4x4.5", ".5", false, "P40306-FP3K-B-4-5-5-R")]
        [InlineData(.625, "FP3K-C", "S", "C4x4.5", ".5", false, "P40306-FP3K-C-4-5-5-R")]
        [InlineData(.625, "FP3K-D", "S", "C4x4.5", ".5", false, "P40306-FP3K-D-4-5-5-R")]
        [InlineData(.625, "FP4K-A", "S", "C4x4.5", ".5", false, "P40306-FP4K-A-4-5-5-R")]
        [InlineData(.625, "FP4K-B", "S", "C4x4.5", ".5", false, "P40306-FP4K-B-4-5-5-R")]
        [InlineData(.625, "FP5K-A", "S", "C4x4.5", ".5", false, "P40306-FP5K-A-4-5-5-R")]
        [InlineData(.625, "FP5K-B", "S", "C4x4.5", ".5", false, "P40306-FP5K-B-4-5-5-R")]
        [InlineData(.625, "FP5K-C", "S", "C4x4.5", ".5", false, "P40306-FP5K-C-4-5-5-R")]
        [InlineData(.625, "FP5K-D", "S", "C4x4.5", ".5", false, "P40306-FP5K-D-4-5-5-R")]
        [InlineData(.625, "FP6K-A", "S", "C4x4.5", ".5", false, "P40306-FP6K-A-4-5-5-R")]
        [InlineData(.625, "FP6K-B", "S", "C4x4.5", ".5", false, "P40306-FP6K-B-4-5-5-R")]
        [InlineData(.625, "FP1K-A", "S", "C5x6.7", ".5", false, "P40306-FP1K-A-5-6-5-R")]
        [InlineData(.625, "FP1K-B", "S", "C5x6.7", ".5", false, "P40306-FP1K-B-5-6-5-R")]
        [InlineData(.625, "FP1K-C", "S", "C5x6.7", ".5", false, "P40306-FP1K-C-5-6-5-R")]
        [InlineData(.625, "FP1K-D", "S", "C5x6.7", ".5", false, "P40306-FP1K-D-5-6-5-R")]
        [InlineData(.625, "FP2K-A", "S", "C5x6.7", ".5", false, "P40306-FP2K-A-5-6-5-R")]
        [InlineData(.625, "FP2K-B", "S", "C5x6.7", ".5", false, "P40306-FP2K-B-5-6-5-R")]
        [InlineData(.625, "FP3K-A", "S", "C5x6.7", ".5", false, "P40306-FP3K-A-5-6-5-R")]
        [InlineData(.625, "FP3K-B", "S", "C5x6.7", ".5", false, "P40306-FP3K-B-5-6-5-R")]
        [InlineData(.625, "FP3K-C", "S", "C5x6.7", ".5", false, "P40306-FP3K-C-5-6-5-R")]
        [InlineData(.625, "FP3K-D", "S", "C5x6.7", ".5", false, "P40306-FP3K-D-5-6-5-R")]
        [InlineData(.625, "FP4K-A", "S", "C5x6.7", ".5", false, "P40306-FP4K-A-5-6-5-R")]
        [InlineData(.625, "FP4K-B", "S", "C5x6.7", ".5", false, "P40306-FP4K-B-5-6-5-R")]
        [InlineData(.625, "FP5K-A", "S", "C5x6.7", ".5", false, "P40306-FP5K-A-5-6-5-R")]
        [InlineData(.625, "FP5K-B", "S", "C5x6.7", ".5", false, "P40306-FP5K-B-5-6-5-R")]
        [InlineData(.625, "FP5K-C", "S", "C5x6.7", ".5", false, "P40306-FP5K-C-5-6-5-R")]
        [InlineData(.625, "FP5K-D", "S", "C5x6.7", ".5", false, "P40306-FP5K-D-5-6-5-R")]
        [InlineData(.625, "FP6K-A", "S", "C5x6.7", ".5", false, "P40306-FP6K-A-5-6-5-R")]
        [InlineData(.625, "FP6K-B", "S", "C5x6.7", ".5", false, "P40306-FP6K-B-5-6-5-R")]
        [InlineData(.625, "FP1K-A", "S", "C3x3.5", ".625", false, "P40306-FP1K-A-3-4-6-R")]
        [InlineData(.625, "FP1K-B", "S", "C3x3.5", ".625", false, "P40306-FP1K-B-3-4-6-R")]
        [InlineData(.625, "FP1K-C", "S", "C3x3.5", ".625", false, "P40306-FP1K-C-3-4-6-R")]
        [InlineData(.625, "FP1K-D", "S", "C3x3.5", ".625", false, "P40306-FP1K-D-3-4-6-R")]
        [InlineData(.625, "FP2K-A", "S", "C3x3.5", ".625", false, "P40306-FP2K-A-3-4-6-R")]
        [InlineData(.625, "FP2K-B", "S", "C3x3.5", ".625", false, "P40306-FP2K-B-3-4-6-R")]
        [InlineData(.625, "FP3K-A", "S", "C3x3.5", ".625", false, "P40306-FP3K-A-3-4-6-R")]
        [InlineData(.625, "FP3K-B", "S", "C3x3.5", ".625", false, "P40306-FP3K-B-3-4-6-R")]
        [InlineData(.625, "FP3K-C", "S", "C3x3.5", ".625", false, "P40306-FP3K-C-3-4-6-R")]
        [InlineData(.625, "FP3K-D", "S", "C3x3.5", ".625", false, "P40306-FP3K-D-3-4-6-R")]
        [InlineData(.625, "FP4K-A", "S", "C3x3.5", ".625", false, "P40306-FP4K-A-3-4-6-R")]
        [InlineData(.625, "FP4K-B", "S", "C3x3.5", ".625", false, "P40306-FP4K-B-3-4-6-R")]
        [InlineData(.625, "FP5K-A", "S", "C3x3.5", ".625", false, "P40306-FP5K-A-3-4-6-R")]
        [InlineData(.625, "FP5K-B", "S", "C3x3.5", ".625", false, "P40306-FP5K-B-3-4-6-R")]
        [InlineData(.625, "FP5K-C", "S", "C3x3.5", ".625", false, "P40306-FP5K-C-3-4-6-R")]
        [InlineData(.625, "FP5K-D", "S", "C3x3.5", ".625", false, "P40306-FP5K-D-3-4-6-R")]
        [InlineData(.625, "FP6K-A", "S", "C3x3.5", ".625", false, "P40306-FP6K-A-3-4-6-R")]
        [InlineData(.625, "FP6K-B", "S", "C3x3.5", ".625", false, "P40306-FP6K-B-3-4-6-R")]
        [InlineData(.625, "FP1K-A", "S", "C4x4.5", ".625", false, "P40306-FP1K-A-4-5-6-R")]
        [InlineData(.625, "FP1K-B", "S", "C4x4.5", ".625", false, "P40306-FP1K-B-4-5-6-R")]
        [InlineData(.625, "FP1K-C", "S", "C4x4.5", ".625", false, "P40306-FP1K-C-4-5-6-R")]
        [InlineData(.625, "FP1K-D", "S", "C4x4.5", ".625", false, "P40306-FP1K-D-4-5-6-R")]
        [InlineData(.625, "FP2K-A", "S", "C4x4.5", ".625", false, "P40306-FP2K-A-4-5-6-R")]
        [InlineData(.625, "FP2K-B", "S", "C4x4.5", ".625", false, "P40306-FP2K-B-4-5-6-R")]
        [InlineData(.625, "FP3K-A", "S", "C4x4.5", ".625", false, "P40306-FP3K-A-4-5-6-R")]
        [InlineData(.625, "FP3K-B", "S", "C4x4.5", ".625", false, "P40306-FP3K-B-4-5-6-R")]
        [InlineData(.625, "FP3K-C", "S", "C4x4.5", ".625", false, "P40306-FP3K-C-4-5-6-R")]
        [InlineData(.625, "FP3K-D", "S", "C4x4.5", ".625", false, "P40306-FP3K-D-4-5-6-R")]
        [InlineData(.625, "FP4K-A", "S", "C4x4.5", ".625", false, "P40306-FP4K-A-4-5-6-R")]
        [InlineData(.625, "FP4K-B", "S", "C4x4.5", ".625", false, "P40306-FP4K-B-4-5-6-R")]
        [InlineData(.625, "FP5K-A", "S", "C4x4.5", ".625", false, "P40306-FP5K-A-4-5-6-R")]
        [InlineData(.625, "FP5K-B", "S", "C4x4.5", ".625", false, "P40306-FP5K-B-4-5-6-R")]
        [InlineData(.625, "FP5K-C", "S", "C4x4.5", ".625", false, "P40306-FP5K-C-4-5-6-R")]
        [InlineData(.625, "FP5K-D", "S", "C4x4.5", ".625", false, "P40306-FP5K-D-4-5-6-R")]
        [InlineData(.625, "FP6K-A", "S", "C4x4.5", ".625", false, "P40306-FP6K-A-4-5-6-R")]
        [InlineData(.625, "FP6K-B", "S", "C4x4.5", ".625", false, "P40306-FP6K-B-4-5-6-R")]
        [InlineData(.625, "FP1K-A", "S", "C5x6.7", ".625", false, "P40306-FP1K-A-5-6-6-R")]
        [InlineData(.625, "FP1K-B", "S", "C5x6.7", ".625", false, "P40306-FP1K-B-5-6-6-R")]
        [InlineData(.625, "FP1K-C", "S", "C5x6.7", ".625", false, "P40306-FP1K-C-5-6-6-R")]
        [InlineData(.625, "FP1K-D", "S", "C5x6.7", ".625", false, "P40306-FP1K-D-5-6-6-R")]
        [InlineData(.625, "FP2K-A", "S", "C5x6.7", ".625", false, "P40306-FP2K-A-5-6-6-R")]
        [InlineData(.625, "FP2K-B", "S", "C5x6.7", ".625", false, "P40306-FP2K-B-5-6-6-R")]
        [InlineData(.625, "FP3K-A", "S", "C5x6.7", ".625", false, "P40306-FP3K-A-5-6-6-R")]
        [InlineData(.625, "FP3K-B", "S", "C5x6.7", ".625", false, "P40306-FP3K-B-5-6-6-R")]
        [InlineData(.625, "FP3K-C", "S", "C5x6.7", ".625", false, "P40306-FP3K-C-5-6-6-R")]
        [InlineData(.625, "FP3K-D", "S", "C5x6.7", ".625", false, "P40306-FP3K-D-5-6-6-R")]
        [InlineData(.625, "FP4K-A", "S", "C5x6.7", ".625", false, "P40306-FP4K-A-5-6-6-R")]
        [InlineData(.625, "FP4K-B", "S", "C5x6.7", ".625", false, "P40306-FP4K-B-5-6-6-R")]
        [InlineData(.625, "FP5K-A", "S", "C5x6.7", ".625", false, "P40306-FP5K-A-5-6-6-R")]
        [InlineData(.625, "FP5K-B", "S", "C5x6.7", ".625", false, "P40306-FP5K-B-5-6-6-R")]
        [InlineData(.625, "FP5K-C", "S", "C5x6.7", ".625", false, "P40306-FP5K-C-5-6-6-R")]
        [InlineData(.625, "FP5K-D", "S", "C5x6.7", ".625", false, "P40306-FP5K-D-5-6-6-R")]
        [InlineData(.625, "FP6K-A", "S", "C5x6.7", ".625", false, "P40306-FP6K-A-5-6-6-R")]
        [InlineData(.625, "FP6K-B", "S", "C5x6.7", ".625", false, "P40306-FP6K-B-5-6-6-R")]
        [InlineData(.625, "FP1K-A", "S", "C3x3.5", ".75", false, "P40306-FP1K-A-3-4-7-R")]
        [InlineData(.625, "FP1K-B", "S", "C3x3.5", ".75", false, "P40306-FP1K-B-3-4-7-R")]
        [InlineData(.625, "FP1K-C", "S", "C3x3.5", ".75", false, "P40306-FP1K-C-3-4-7-R")]
        [InlineData(.625, "FP1K-D", "S", "C3x3.5", ".75", false, "P40306-FP1K-D-3-4-7-R")]
        [InlineData(.625, "FP2K-A", "S", "C3x3.5", ".75", false, "P40306-FP2K-A-3-4-7-R")]
        [InlineData(.625, "FP2K-B", "S", "C3x3.5", ".75", false, "P40306-FP2K-B-3-4-7-R")]
        [InlineData(.625, "FP3K-A", "S", "C3x3.5", ".75", false, "P40306-FP3K-A-3-4-7-R")]
        [InlineData(.625, "FP3K-B", "S", "C3x3.5", ".75", false, "P40306-FP3K-B-3-4-7-R")]
        [InlineData(.625, "FP3K-C", "S", "C3x3.5", ".75", false, "P40306-FP3K-C-3-4-7-R")]
        [InlineData(.625, "FP3K-D", "S", "C3x3.5", ".75", false, "P40306-FP3K-D-3-4-7-R")]
        [InlineData(.625, "FP4K-A", "S", "C3x3.5", ".75", false, "P40306-FP4K-A-3-4-7-R")]
        [InlineData(.625, "FP4K-B", "S", "C3x3.5", ".75", false, "P40306-FP4K-B-3-4-7-R")]
        [InlineData(.625, "FP5K-A", "S", "C3x3.5", ".75", false, "P40306-FP5K-A-3-4-7-R")]
        [InlineData(.625, "FP5K-B", "S", "C3x3.5", ".75", false, "P40306-FP5K-B-3-4-7-R")]
        [InlineData(.625, "FP5K-C", "S", "C3x3.5", ".75", false, "P40306-FP5K-C-3-4-7-R")]
        [InlineData(.625, "FP5K-D", "S", "C3x3.5", ".75", false, "P40306-FP5K-D-3-4-7-R")]
        [InlineData(.625, "FP6K-A", "S", "C3x3.5", ".75", false, "P40306-FP6K-A-3-4-7-R")]
        [InlineData(.625, "FP6K-B", "S", "C3x3.5", ".75", false, "P40306-FP6K-B-3-4-7-R")]
        [InlineData(.625, "FP1K-A", "S", "C4x4.5", ".75", false, "P40306-FP1K-A-4-5-7-R")]
        [InlineData(.625, "FP1K-B", "S", "C4x4.5", ".75", false, "P40306-FP1K-B-4-5-7-R")]
        [InlineData(.625, "FP1K-C", "S", "C4x4.5", ".75", false, "P40306-FP1K-C-4-5-7-R")]
        [InlineData(.625, "FP1K-D", "S", "C4x4.5", ".75", false, "P40306-FP1K-D-4-5-7-R")]
        [InlineData(.625, "FP2K-A", "S", "C4x4.5", ".75", false, "P40306-FP2K-A-4-5-7-R")]
        [InlineData(.625, "FP2K-B", "S", "C4x4.5", ".75", false, "P40306-FP2K-B-4-5-7-R")]
        [InlineData(.625, "FP3K-A", "S", "C4x4.5", ".75", false, "P40306-FP3K-A-4-5-7-R")]
        [InlineData(.625, "FP3K-B", "S", "C4x4.5", ".75", false, "P40306-FP3K-B-4-5-7-R")]
        [InlineData(.625, "FP3K-C", "S", "C4x4.5", ".75", false, "P40306-FP3K-C-4-5-7-R")]
        [InlineData(.625, "FP3K-D", "S", "C4x4.5", ".75", false, "P40306-FP3K-D-4-5-7-R")]
        [InlineData(.625, "FP4K-A", "S", "C4x4.5", ".75", false, "P40306-FP4K-A-4-5-7-R")]
        [InlineData(.625, "FP4K-B", "S", "C4x4.5", ".75", false, "P40306-FP4K-B-4-5-7-R")]
        [InlineData(.625, "FP5K-A", "S", "C4x4.5", ".75", false, "P40306-FP5K-A-4-5-7-R")]
        [InlineData(.625, "FP5K-B", "S", "C4x4.5", ".75", false, "P40306-FP5K-B-4-5-7-R")]
        [InlineData(.625, "FP5K-C", "S", "C4x4.5", ".75", false, "P40306-FP5K-C-4-5-7-R")]
        [InlineData(.625, "FP5K-D", "S", "C4x4.5", ".75", false, "P40306-FP5K-D-4-5-7-R")]
        [InlineData(.625, "FP6K-A", "S", "C4x4.5", ".75", false, "P40306-FP6K-A-4-5-7-R")]
        [InlineData(.625, "FP6K-B", "S", "C4x4.5", ".75", false, "P40306-FP6K-B-4-5-7-R")]
        [InlineData(.625, "FP1K-A", "S", "C5x6.7", ".75", false, "P40306-FP1K-A-5-6-7-R")]
        [InlineData(.625, "FP1K-B", "S", "C5x6.7", ".75", false, "P40306-FP1K-B-5-6-7-R")]
        [InlineData(.625, "FP1K-C", "S", "C5x6.7", ".75", false, "P40306-FP1K-C-5-6-7-R")]
        [InlineData(.625, "FP1K-D", "S", "C5x6.7", ".75", false, "P40306-FP1K-D-5-6-7-R")]
        [InlineData(.625, "FP2K-A", "S", "C5x6.7", ".75", false, "P40306-FP2K-A-5-6-7-R")]
        [InlineData(.625, "FP2K-B", "S", "C5x6.7", ".75", false, "P40306-FP2K-B-5-6-7-R")]
        [InlineData(.625, "FP3K-A", "S", "C5x6.7", ".75", false, "P40306-FP3K-A-5-6-7-R")]
        [InlineData(.625, "FP3K-B", "S", "C5x6.7", ".75", false, "P40306-FP3K-B-5-6-7-R")]
        [InlineData(.625, "FP3K-C", "S", "C5x6.7", ".75", false, "P40306-FP3K-C-5-6-7-R")]
        [InlineData(.625, "FP3K-D", "S", "C5x6.7", ".75", false, "P40306-FP3K-D-5-6-7-R")]
        [InlineData(.625, "FP4K-A", "S", "C5x6.7", ".75", false, "P40306-FP4K-A-5-6-7-R")]
        [InlineData(.625, "FP4K-B", "S", "C5x6.7", ".75", false, "P40306-FP4K-B-5-6-7-R")]
        [InlineData(.625, "FP5K-A", "S", "C5x6.7", ".75", false, "P40306-FP5K-A-5-6-7-R")]
        [InlineData(.625, "FP5K-B", "S", "C5x6.7", ".75", false, "P40306-FP5K-B-5-6-7-R")]
        [InlineData(.625, "FP5K-C", "S", "C5x6.7", ".75", false, "P40306-FP5K-C-5-6-7-R")]
        [InlineData(.625, "FP5K-D", "S", "C5x6.7", ".75", false, "P40306-FP5K-D-5-6-7-R")]
        [InlineData(.625, "FP6K-A", "S", "C5x6.7", ".75", false, "P40306-FP6K-A-5-6-7-R")]
        [InlineData(.625, "FP6K-B", "S", "C5x6.7", ".75", false, "P40306-FP6K-B-5-6-7-R")]



        // 
        [InlineData(.375, "FP1K-A", "SL", "C3x3.5", ".5", false, "P40304-FP1K-A-3-5-5-R")]
        [InlineData(.375, "FP1K-B", "SL", "C3x3.5", ".5", false, "P40304-FP1K-B-3-5-5-R")]
        [InlineData(.375, "FP1K-C", "SL", "C3x3.5", ".5", false, "P40304-FP1K-C-3-5-5-R")]
        [InlineData(.375, "FP1K-D", "SL", "C3x3.5", ".5", false, "P40304-FP1K-D-3-5-5-R")]
        [InlineData(.375, "FP2K-A", "SL", "C3x3.5", ".5", false, "P40304-FP2K-A-3-5-5-R")]
        [InlineData(.375, "FP2K-B", "SL", "C3x3.5", ".5", false, "P40304-FP2K-B-3-5-5-R")]
        [InlineData(.375, "FP3K-A", "SL", "C3x3.5", ".5", false, "P40304-FP3K-A-3-5-5-R")]
        [InlineData(.375, "FP3K-B", "SL", "C3x3.5", ".5", false, "P40304-FP3K-B-3-5-5-R")]
        [InlineData(.375, "FP3K-C", "SL", "C3x3.5", ".5", false, "P40304-FP3K-C-3-5-5-R")]
        [InlineData(.375, "FP3K-D", "SL", "C3x3.5", ".5", false, "P40304-FP3K-D-3-5-5-R")]
        [InlineData(.375, "FP4K-A", "SL", "C3x3.5", ".5", false, "P40304-FP4K-A-3-5-5-R")]
        [InlineData(.375, "FP4K-B", "SL", "C3x3.5", ".5", false, "P40304-FP4K-B-3-5-5-R")]
        [InlineData(.375, "FP5K-A", "SL", "C3x3.5", ".5", false, "P40304-FP5K-A-3-5-5-R")]
        [InlineData(.375, "FP5K-B", "SL", "C3x3.5", ".5", false, "P40304-FP5K-B-3-5-5-R")]
        [InlineData(.375, "FP5K-C", "SL", "C3x3.5", ".5", false, "P40304-FP5K-C-3-5-5-R")]
        [InlineData(.375, "FP5K-D", "SL", "C3x3.5", ".5", false, "P40304-FP5K-D-3-5-5-R")]
        [InlineData(.375, "FP6K-A", "SL", "C3x3.5", ".5", false, "P40304-FP6K-A-3-5-5-R")]
        [InlineData(.375, "FP6K-B", "SL", "C3x3.5", ".5", false, "P40304-FP6K-B-3-5-5-R")]
        [InlineData(.375, "FP1K-A", "SL", "C4x4.5", ".5", false, "P40304-FP1K-A-4-6-5-R")]
        [InlineData(.375, "FP1K-B", "SL", "C4x4.5", ".5", false, "P40304-FP1K-B-4-6-5-R")]
        [InlineData(.375, "FP1K-C", "SL", "C4x4.5", ".5", false, "P40304-FP1K-C-4-6-5-R")]
        [InlineData(.375, "FP1K-D", "SL", "C4x4.5", ".5", false, "P40304-FP1K-D-4-6-5-R")]
        [InlineData(.375, "FP2K-A", "SL", "C4x4.5", ".5", false, "P40304-FP2K-A-4-6-5-R")]
        [InlineData(.375, "FP2K-B", "SL", "C4x4.5", ".5", false, "P40304-FP2K-B-4-6-5-R")]
        [InlineData(.375, "FP3K-A", "SL", "C4x4.5", ".5", false, "P40304-FP3K-A-4-6-5-R")]
        [InlineData(.375, "FP3K-B", "SL", "C4x4.5", ".5", false, "P40304-FP3K-B-4-6-5-R")]
        [InlineData(.375, "FP3K-C", "SL", "C4x4.5", ".5", false, "P40304-FP3K-C-4-6-5-R")]
        [InlineData(.375, "FP3K-D", "SL", "C4x4.5", ".5", false, "P40304-FP3K-D-4-6-5-R")]
        [InlineData(.375, "FP4K-A", "SL", "C4x4.5", ".5", false, "P40304-FP4K-A-4-6-5-R")]
        [InlineData(.375, "FP4K-B", "SL", "C4x4.5", ".5", false, "P40304-FP4K-B-4-6-5-R")]
        [InlineData(.375, "FP5K-A", "SL", "C4x4.5", ".5", false, "P40304-FP5K-A-4-6-5-R")]
        [InlineData(.375, "FP5K-B", "SL", "C4x4.5", ".5", false, "P40304-FP5K-B-4-6-5-R")]
        [InlineData(.375, "FP5K-C", "SL", "C4x4.5", ".5", false, "P40304-FP5K-C-4-6-5-R")]
        [InlineData(.375, "FP5K-D", "SL", "C4x4.5", ".5", false, "P40304-FP5K-D-4-6-5-R")]
        [InlineData(.375, "FP6K-A", "SL", "C4x4.5", ".5", false, "P40304-FP6K-A-4-6-5-R")]
        [InlineData(.375, "FP6K-B", "SL", "C4x4.5", ".5", false, "P40304-FP6K-B-4-6-5-R")]
        [InlineData(.375, "FP1K-A", "SL", "C5x6.7", ".5", false, "P40304-FP1K-A-5-7-5-R")]
        [InlineData(.375, "FP1K-B", "SL", "C5x6.7", ".5", false, "P40304-FP1K-B-5-7-5-R")]
        [InlineData(.375, "FP1K-C", "SL", "C5x6.7", ".5", false, "P40304-FP1K-C-5-7-5-R")]
        [InlineData(.375, "FP1K-D", "SL", "C5x6.7", ".5", false, "P40304-FP1K-D-5-7-5-R")]
        [InlineData(.375, "FP2K-A", "SL", "C5x6.7", ".5", false, "P40304-FP2K-A-5-7-5-R")]
        [InlineData(.375, "FP2K-B", "SL", "C5x6.7", ".5", false, "P40304-FP2K-B-5-7-5-R")]
        [InlineData(.375, "FP3K-A", "SL", "C5x6.7", ".5", false, "P40304-FP3K-A-5-7-5-R")]
        [InlineData(.375, "FP3K-B", "SL", "C5x6.7", ".5", false, "P40304-FP3K-B-5-7-5-R")]
        [InlineData(.375, "FP3K-C", "SL", "C5x6.7", ".5", false, "P40304-FP3K-C-5-7-5-R")]
        [InlineData(.375, "FP3K-D", "SL", "C5x6.7", ".5", false, "P40304-FP3K-D-5-7-5-R")]
        [InlineData(.375, "FP4K-A", "SL", "C5x6.7", ".5", false, "P40304-FP4K-A-5-7-5-R")]
        [InlineData(.375, "FP4K-B", "SL", "C5x6.7", ".5", false, "P40304-FP4K-B-5-7-5-R")]
        [InlineData(.375, "FP5K-A", "SL", "C5x6.7", ".5", false, "P40304-FP5K-A-5-7-5-R")]
        [InlineData(.375, "FP5K-B", "SL", "C5x6.7", ".5", false, "P40304-FP5K-B-5-7-5-R")]
        [InlineData(.375, "FP5K-C", "SL", "C5x6.7", ".5", false, "P40304-FP5K-C-5-7-5-R")]
        [InlineData(.375, "FP5K-D", "SL", "C5x6.7", ".5", false, "P40304-FP5K-D-5-7-5-R")]
        [InlineData(.375, "FP6K-A", "SL", "C5x6.7", ".5", false, "P40304-FP6K-A-5-7-5-R")]
        [InlineData(.375, "FP6K-B", "SL", "C5x6.7", ".5", false, "P40304-FP6K-B-5-7-5-R")]
        [InlineData(.375, "FP1K-A", "SL", "C3x3.5", ".625", false, "P40304-FP1K-A-3-5-6-R")]
        [InlineData(.375, "FP1K-B", "SL", "C3x3.5", ".625", false, "P40304-FP1K-B-3-5-6-R")]
        [InlineData(.375, "FP1K-C", "SL", "C3x3.5", ".625", false, "P40304-FP1K-C-3-5-6-R")]
        [InlineData(.375, "FP1K-D", "SL", "C3x3.5", ".625", false, "P40304-FP1K-D-3-5-6-R")]
        [InlineData(.375, "FP2K-A", "SL", "C3x3.5", ".625", false, "P40304-FP2K-A-3-5-6-R")]
        [InlineData(.375, "FP2K-B", "SL", "C3x3.5", ".625", false, "P40304-FP2K-B-3-5-6-R")]
        [InlineData(.375, "FP3K-A", "SL", "C3x3.5", ".625", false, "P40304-FP3K-A-3-5-6-R")]
        [InlineData(.375, "FP3K-B", "SL", "C3x3.5", ".625", false, "P40304-FP3K-B-3-5-6-R")]
        [InlineData(.375, "FP3K-C", "SL", "C3x3.5", ".625", false, "P40304-FP3K-C-3-5-6-R")]
        [InlineData(.375, "FP3K-D", "SL", "C3x3.5", ".625", false, "P40304-FP3K-D-3-5-6-R")]
        [InlineData(.375, "FP4K-A", "SL", "C3x3.5", ".625", false, "P40304-FP4K-A-3-5-6-R")]
        [InlineData(.375, "FP4K-B", "SL", "C3x3.5", ".625", false, "P40304-FP4K-B-3-5-6-R")]
        [InlineData(.375, "FP5K-A", "SL", "C3x3.5", ".625", false, "P40304-FP5K-A-3-5-6-R")]
        [InlineData(.375, "FP5K-B", "SL", "C3x3.5", ".625", false, "P40304-FP5K-B-3-5-6-R")]
        [InlineData(.375, "FP5K-C", "SL", "C3x3.5", ".625", false, "P40304-FP5K-C-3-5-6-R")]
        [InlineData(.375, "FP5K-D", "SL", "C3x3.5", ".625", false, "P40304-FP5K-D-3-5-6-R")]
        [InlineData(.375, "FP6K-A", "SL", "C3x3.5", ".625", false, "P40304-FP6K-A-3-5-6-R")]
        [InlineData(.375, "FP6K-B", "SL", "C3x3.5", ".625", false, "P40304-FP6K-B-3-5-6-R")]
        [InlineData(.375, "FP1K-A", "SL", "C4x4.5", ".625", false, "P40304-FP1K-A-4-6-6-R")]
        [InlineData(.375, "FP1K-B", "SL", "C4x4.5", ".625", false, "P40304-FP1K-B-4-6-6-R")]
        [InlineData(.375, "FP1K-C", "SL", "C4x4.5", ".625", false, "P40304-FP1K-C-4-6-6-R")]
        [InlineData(.375, "FP1K-D", "SL", "C4x4.5", ".625", false, "P40304-FP1K-D-4-6-6-R")]
        [InlineData(.375, "FP2K-A", "SL", "C4x4.5", ".625", false, "P40304-FP2K-A-4-6-6-R")]
        [InlineData(.375, "FP2K-B", "SL", "C4x4.5", ".625", false, "P40304-FP2K-B-4-6-6-R")]
        [InlineData(.375, "FP3K-A", "SL", "C4x4.5", ".625", false, "P40304-FP3K-A-4-6-6-R")]
        [InlineData(.375, "FP3K-B", "SL", "C4x4.5", ".625", false, "P40304-FP3K-B-4-6-6-R")]
        [InlineData(.375, "FP3K-C", "SL", "C4x4.5", ".625", false, "P40304-FP3K-C-4-6-6-R")]
        [InlineData(.375, "FP3K-D", "SL", "C4x4.5", ".625", false, "P40304-FP3K-D-4-6-6-R")]
        [InlineData(.375, "FP4K-A", "SL", "C4x4.5", ".625", false, "P40304-FP4K-A-4-6-6-R")]
        [InlineData(.375, "FP4K-B", "SL", "C4x4.5", ".625", false, "P40304-FP4K-B-4-6-6-R")]
        [InlineData(.375, "FP5K-A", "SL", "C4x4.5", ".625", false, "P40304-FP5K-A-4-6-6-R")]
        [InlineData(.375, "FP5K-B", "SL", "C4x4.5", ".625", false, "P40304-FP5K-B-4-6-6-R")]
        [InlineData(.375, "FP5K-C", "SL", "C4x4.5", ".625", false, "P40304-FP5K-C-4-6-6-R")]
        [InlineData(.375, "FP5K-D", "SL", "C4x4.5", ".625", false, "P40304-FP5K-D-4-6-6-R")]
        [InlineData(.375, "FP6K-A", "SL", "C4x4.5", ".625", false, "P40304-FP6K-A-4-6-6-R")]
        [InlineData(.375, "FP6K-B", "SL", "C4x4.5", ".625", false, "P40304-FP6K-B-4-6-6-R")]
        [InlineData(.375, "FP1K-A", "SL", "C5x6.7", ".625", false, "P40304-FP1K-A-5-7-6-R")]
        [InlineData(.375, "FP1K-B", "SL", "C5x6.7", ".625", false, "P40304-FP1K-B-5-7-6-R")]
        [InlineData(.375, "FP1K-C", "SL", "C5x6.7", ".625", false, "P40304-FP1K-C-5-7-6-R")]
        [InlineData(.375, "FP1K-D", "SL", "C5x6.7", ".625", false, "P40304-FP1K-D-5-7-6-R")]
        [InlineData(.375, "FP2K-A", "SL", "C5x6.7", ".625", false, "P40304-FP2K-A-5-7-6-R")]
        [InlineData(.375, "FP2K-B", "SL", "C5x6.7", ".625", false, "P40304-FP2K-B-5-7-6-R")]
        [InlineData(.375, "FP3K-A", "SL", "C5x6.7", ".625", false, "P40304-FP3K-A-5-7-6-R")]
        [InlineData(.375, "FP3K-B", "SL", "C5x6.7", ".625", false, "P40304-FP3K-B-5-7-6-R")]
        [InlineData(.375, "FP3K-C", "SL", "C5x6.7", ".625", false, "P40304-FP3K-C-5-7-6-R")]
        [InlineData(.375, "FP3K-D", "SL", "C5x6.7", ".625", false, "P40304-FP3K-D-5-7-6-R")]
        [InlineData(.375, "FP4K-A", "SL", "C5x6.7", ".625", false, "P40304-FP4K-A-5-7-6-R")]
        [InlineData(.375, "FP4K-B", "SL", "C5x6.7", ".625", false, "P40304-FP4K-B-5-7-6-R")]
        [InlineData(.375, "FP5K-A", "SL", "C5x6.7", ".625", false, "P40304-FP5K-A-5-7-6-R")]
        [InlineData(.375, "FP5K-B", "SL", "C5x6.7", ".625", false, "P40304-FP5K-B-5-7-6-R")]
        [InlineData(.375, "FP5K-C", "SL", "C5x6.7", ".625", false, "P40304-FP5K-C-5-7-6-R")]
        [InlineData(.375, "FP5K-D", "SL", "C5x6.7", ".625", false, "P40304-FP5K-D-5-7-6-R")]
        [InlineData(.375, "FP6K-A", "SL", "C5x6.7", ".625", false, "P40304-FP6K-A-5-7-6-R")]
        [InlineData(.375, "FP6K-B", "SL", "C5x6.7", ".625", false, "P40304-FP6K-B-5-7-6-R")]
        [InlineData(.375, "FP1K-A", "SL", "C3x3.5", ".75", false, "P40304-FP1K-A-3-5-7-R")]
        [InlineData(.375, "FP1K-B", "SL", "C3x3.5", ".75", false, "P40304-FP1K-B-3-5-7-R")]
        [InlineData(.375, "FP1K-C", "SL", "C3x3.5", ".75", false, "P40304-FP1K-C-3-5-7-R")]
        [InlineData(.375, "FP1K-D", "SL", "C3x3.5", ".75", false, "P40304-FP1K-D-3-5-7-R")]
        [InlineData(.375, "FP2K-A", "SL", "C3x3.5", ".75", false, "P40304-FP2K-A-3-5-7-R")]
        [InlineData(.375, "FP2K-B", "SL", "C3x3.5", ".75", false, "P40304-FP2K-B-3-5-7-R")]
        [InlineData(.375, "FP3K-A", "SL", "C3x3.5", ".75", false, "P40304-FP3K-A-3-5-7-R")]
        [InlineData(.375, "FP3K-B", "SL", "C3x3.5", ".75", false, "P40304-FP3K-B-3-5-7-R")]
        [InlineData(.375, "FP3K-C", "SL", "C3x3.5", ".75", false, "P40304-FP3K-C-3-5-7-R")]
        [InlineData(.375, "FP3K-D", "SL", "C3x3.5", ".75", false, "P40304-FP3K-D-3-5-7-R")]
        [InlineData(.375, "FP4K-A", "SL", "C3x3.5", ".75", false, "P40304-FP4K-A-3-5-7-R")]
        [InlineData(.375, "FP4K-B", "SL", "C3x3.5", ".75", false, "P40304-FP4K-B-3-5-7-R")]
        [InlineData(.375, "FP5K-A", "SL", "C3x3.5", ".75", false, "P40304-FP5K-A-3-5-7-R")]
        [InlineData(.375, "FP5K-B", "SL", "C3x3.5", ".75", false, "P40304-FP5K-B-3-5-7-R")]
        [InlineData(.375, "FP5K-C", "SL", "C3x3.5", ".75", false, "P40304-FP5K-C-3-5-7-R")]
        [InlineData(.375, "FP5K-D", "SL", "C3x3.5", ".75", false, "P40304-FP5K-D-3-5-7-R")]
        [InlineData(.375, "FP6K-A", "SL", "C3x3.5", ".75", false, "P40304-FP6K-A-3-5-7-R")]
        [InlineData(.375, "FP6K-B", "SL", "C3x3.5", ".75", false, "P40304-FP6K-B-3-5-7-R")]
        [InlineData(.375, "FP1K-A", "SL", "C4x4.5", ".75", false, "P40304-FP1K-A-4-6-7-R")]
        [InlineData(.375, "FP1K-B", "SL", "C4x4.5", ".75", false, "P40304-FP1K-B-4-6-7-R")]
        [InlineData(.375, "FP1K-C", "SL", "C4x4.5", ".75", false, "P40304-FP1K-C-4-6-7-R")]
        [InlineData(.375, "FP1K-D", "SL", "C4x4.5", ".75", false, "P40304-FP1K-D-4-6-7-R")]
        [InlineData(.375, "FP2K-A", "SL", "C4x4.5", ".75", false, "P40304-FP2K-A-4-6-7-R")]
        [InlineData(.375, "FP2K-B", "SL", "C4x4.5", ".75", false, "P40304-FP2K-B-4-6-7-R")]
        [InlineData(.375, "FP3K-A", "SL", "C4x4.5", ".75", false, "P40304-FP3K-A-4-6-7-R")]
        [InlineData(.375, "FP3K-B", "SL", "C4x4.5", ".75", false, "P40304-FP3K-B-4-6-7-R")]
        [InlineData(.375, "FP3K-C", "SL", "C4x4.5", ".75", false, "P40304-FP3K-C-4-6-7-R")]
        [InlineData(.375, "FP3K-D", "SL", "C4x4.5", ".75", false, "P40304-FP3K-D-4-6-7-R")]
        [InlineData(.375, "FP4K-A", "SL", "C4x4.5", ".75", false, "P40304-FP4K-A-4-6-7-R")]
        [InlineData(.375, "FP4K-B", "SL", "C4x4.5", ".75", false, "P40304-FP4K-B-4-6-7-R")]
        [InlineData(.375, "FP5K-A", "SL", "C4x4.5", ".75", false, "P40304-FP5K-A-4-6-7-R")]
        [InlineData(.375, "FP5K-B", "SL", "C4x4.5", ".75", false, "P40304-FP5K-B-4-6-7-R")]
        [InlineData(.375, "FP5K-C", "SL", "C4x4.5", ".75", false, "P40304-FP5K-C-4-6-7-R")]
        [InlineData(.375, "FP5K-D", "SL", "C4x4.5", ".75", false, "P40304-FP5K-D-4-6-7-R")]
        [InlineData(.375, "FP6K-A", "SL", "C4x4.5", ".75", false, "P40304-FP6K-A-4-6-7-R")]
        [InlineData(.375, "FP6K-B", "SL", "C4x4.5", ".75", false, "P40304-FP6K-B-4-6-7-R")]
        [InlineData(.375, "FP1K-A", "SL", "C5x6.7", ".75", false, "P40304-FP1K-A-5-7-7-R")]
        [InlineData(.375, "FP1K-B", "SL", "C5x6.7", ".75", false, "P40304-FP1K-B-5-7-7-R")]
        [InlineData(.375, "FP1K-C", "SL", "C5x6.7", ".75", false, "P40304-FP1K-C-5-7-7-R")]
        [InlineData(.375, "FP1K-D", "SL", "C5x6.7", ".75", false, "P40304-FP1K-D-5-7-7-R")]
        [InlineData(.375, "FP2K-A", "SL", "C5x6.7", ".75", false, "P40304-FP2K-A-5-7-7-R")]
        [InlineData(.375, "FP2K-B", "SL", "C5x6.7", ".75", false, "P40304-FP2K-B-5-7-7-R")]
        [InlineData(.375, "FP3K-A", "SL", "C5x6.7", ".75", false, "P40304-FP3K-A-5-7-7-R")]
        [InlineData(.375, "FP3K-B", "SL", "C5x6.7", ".75", false, "P40304-FP3K-B-5-7-7-R")]
        [InlineData(.375, "FP3K-C", "SL", "C5x6.7", ".75", false, "P40304-FP3K-C-5-7-7-R")]
        [InlineData(.375, "FP3K-D", "SL", "C5x6.7", ".75", false, "P40304-FP3K-D-5-7-7-R")]
        [InlineData(.375, "FP4K-A", "SL", "C5x6.7", ".75", false, "P40304-FP4K-A-5-7-7-R")]
        [InlineData(.375, "FP4K-B", "SL", "C5x6.7", ".75", false, "P40304-FP4K-B-5-7-7-R")]
        [InlineData(.375, "FP5K-A", "SL", "C5x6.7", ".75", false, "P40304-FP5K-A-5-7-7-R")]
        [InlineData(.375, "FP5K-B", "SL", "C5x6.7", ".75", false, "P40304-FP5K-B-5-7-7-R")]
        [InlineData(.375, "FP5K-C", "SL", "C5x6.7", ".75", false, "P40304-FP5K-C-5-7-7-R")]
        [InlineData(.375, "FP5K-D", "SL", "C5x6.7", ".75", false, "P40304-FP5K-D-5-7-7-R")]
        [InlineData(.375, "FP6K-A", "SL", "C5x6.7", ".75", false, "P40304-FP6K-A-5-7-7-R")]
        [InlineData(.375, "FP6K-B", "SL", "C5x6.7", ".75", false, "P40304-FP6K-B-5-7-7-R")]
        //1/2in plate thickness
        [InlineData(.5, "FP1K-A", "SL", "C3x3.5", ".5", false, "P40305-FP1K-A-3-5-5-R")]
        [InlineData(.5, "FP1K-B", "SL", "C3x3.5", ".5", false, "P40305-FP1K-B-3-5-5-R")]
        [InlineData(.5, "FP1K-C", "SL", "C3x3.5", ".5", false, "P40305-FP1K-C-3-5-5-R")]
        [InlineData(.5, "FP1K-D", "SL", "C3x3.5", ".5", false, "P40305-FP1K-D-3-5-5-R")]
        [InlineData(.5, "FP2K-A", "SL", "C3x3.5", ".5", false, "P40305-FP2K-A-3-5-5-R")]
        [InlineData(.5, "FP2K-B", "SL", "C3x3.5", ".5", false, "P40305-FP2K-B-3-5-5-R")]
        [InlineData(.5, "FP3K-A", "SL", "C3x3.5", ".5", false, "P40305-FP3K-A-3-5-5-R")]
        [InlineData(.5, "FP3K-B", "SL", "C3x3.5", ".5", false, "P40305-FP3K-B-3-5-5-R")]
        [InlineData(.5, "FP3K-C", "SL", "C3x3.5", ".5", false, "P40305-FP3K-C-3-5-5-R")]
        [InlineData(.5, "FP3K-D", "SL", "C3x3.5", ".5", false, "P40305-FP3K-D-3-5-5-R")]
        [InlineData(.5, "FP4K-A", "SL", "C3x3.5", ".5", false, "P40305-FP4K-A-3-5-5-R")]
        [InlineData(.5, "FP4K-B", "SL", "C3x3.5", ".5", false, "P40305-FP4K-B-3-5-5-R")]
        [InlineData(.5, "FP5K-A", "SL", "C3x3.5", ".5", false, "P40305-FP5K-A-3-5-5-R")]
        [InlineData(.5, "FP5K-B", "SL", "C3x3.5", ".5", false, "P40305-FP5K-B-3-5-5-R")]
        [InlineData(.5, "FP5K-C", "SL", "C3x3.5", ".5", false, "P40305-FP5K-C-3-5-5-R")]
        [InlineData(.5, "FP5K-D", "SL", "C3x3.5", ".5", false, "P40305-FP5K-D-3-5-5-R")]
        [InlineData(.5, "FP6K-A", "SL", "C3x3.5", ".5", false, "P40305-FP6K-A-3-5-5-R")]
        [InlineData(.5, "FP6K-B", "SL", "C3x3.5", ".5", false, "P40305-FP6K-B-3-5-5-R")]
        [InlineData(.5, "FP1K-A", "SL", "C4x4.5", ".5", false, "P40305-FP1K-A-4-6-5-R")]
        [InlineData(.5, "FP1K-B", "SL", "C4x4.5", ".5", false, "P40305-FP1K-B-4-6-5-R")]
        [InlineData(.5, "FP1K-C", "SL", "C4x4.5", ".5", false, "P40305-FP1K-C-4-6-5-R")]
        [InlineData(.5, "FP1K-D", "SL", "C4x4.5", ".5", false, "P40305-FP1K-D-4-6-5-R")]
        [InlineData(.5, "FP2K-A", "SL", "C4x4.5", ".5", false, "P40305-FP2K-A-4-6-5-R")]
        [InlineData(.5, "FP2K-B", "SL", "C4x4.5", ".5", false, "P40305-FP2K-B-4-6-5-R")]
        [InlineData(.5, "FP3K-A", "SL", "C4x4.5", ".5", false, "P40305-FP3K-A-4-6-5-R")]
        [InlineData(.5, "FP3K-B", "SL", "C4x4.5", ".5", false, "P40305-FP3K-B-4-6-5-R")]
        [InlineData(.5, "FP3K-C", "SL", "C4x4.5", ".5", false, "P40305-FP3K-C-4-6-5-R")]
        [InlineData(.5, "FP3K-D", "SL", "C4x4.5", ".5", false, "P40305-FP3K-D-4-6-5-R")]
        [InlineData(.5, "FP4K-A", "SL", "C4x4.5", ".5", false, "P40305-FP4K-A-4-6-5-R")]
        [InlineData(.5, "FP4K-B", "SL", "C4x4.5", ".5", false, "P40305-FP4K-B-4-6-5-R")]
        [InlineData(.5, "FP5K-A", "SL", "C4x4.5", ".5", false, "P40305-FP5K-A-4-6-5-R")]
        [InlineData(.5, "FP5K-B", "SL", "C4x4.5", ".5", false, "P40305-FP5K-B-4-6-5-R")]
        [InlineData(.5, "FP5K-C", "SL", "C4x4.5", ".5", false, "P40305-FP5K-C-4-6-5-R")]
        [InlineData(.5, "FP5K-D", "SL", "C4x4.5", ".5", false, "P40305-FP5K-D-4-6-5-R")]
        [InlineData(.5, "FP6K-A", "SL", "C4x4.5", ".5", false, "P40305-FP6K-A-4-6-5-R")]
        [InlineData(.5, "FP6K-B", "SL", "C4x4.5", ".5", false, "P40305-FP6K-B-4-6-5-R")]
        [InlineData(.5, "FP1K-A", "SL", "C5x6.7", ".5", false, "P40305-FP1K-A-5-7-5-R")]
        [InlineData(.5, "FP1K-B", "SL", "C5x6.7", ".5", false, "P40305-FP1K-B-5-7-5-R")]
        [InlineData(.5, "FP1K-C", "SL", "C5x6.7", ".5", false, "P40305-FP1K-C-5-7-5-R")]
        [InlineData(.5, "FP1K-D", "SL", "C5x6.7", ".5", false, "P40305-FP1K-D-5-7-5-R")]
        [InlineData(.5, "FP2K-A", "SL", "C5x6.7", ".5", false, "P40305-FP2K-A-5-7-5-R")]
        [InlineData(.5, "FP2K-B", "SL", "C5x6.7", ".5", false, "P40305-FP2K-B-5-7-5-R")]
        [InlineData(.5, "FP3K-A", "SL", "C5x6.7", ".5", false, "P40305-FP3K-A-5-7-5-R")]
        [InlineData(.5, "FP3K-B", "SL", "C5x6.7", ".5", false, "P40305-FP3K-B-5-7-5-R")]
        [InlineData(.5, "FP3K-C", "SL", "C5x6.7", ".5", false, "P40305-FP3K-C-5-7-5-R")]
        [InlineData(.5, "FP3K-D", "SL", "C5x6.7", ".5", false, "P40305-FP3K-D-5-7-5-R")]
        [InlineData(.5, "FP4K-A", "SL", "C5x6.7", ".5", false, "P40305-FP4K-A-5-7-5-R")]
        [InlineData(.5, "FP4K-B", "SL", "C5x6.7", ".5", false, "P40305-FP4K-B-5-7-5-R")]
        [InlineData(.5, "FP5K-A", "SL", "C5x6.7", ".5", false, "P40305-FP5K-A-5-7-5-R")]
        [InlineData(.5, "FP5K-B", "SL", "C5x6.7", ".5", false, "P40305-FP5K-B-5-7-5-R")]
        [InlineData(.5, "FP5K-C", "SL", "C5x6.7", ".5", false, "P40305-FP5K-C-5-7-5-R")]
        [InlineData(.5, "FP5K-D", "SL", "C5x6.7", ".5", false, "P40305-FP5K-D-5-7-5-R")]
        [InlineData(.5, "FP6K-A", "SL", "C5x6.7", ".5", false, "P40305-FP6K-A-5-7-5-R")]
        [InlineData(.5, "FP6K-B", "SL", "C5x6.7", ".5", false, "P40305-FP6K-B-5-7-5-R")]
        [InlineData(.5, "FP1K-A", "SL", "C3x3.5", ".625", false, "P40305-FP1K-A-3-5-6-R")]
        [InlineData(.5, "FP1K-B", "SL", "C3x3.5", ".625", false, "P40305-FP1K-B-3-5-6-R")]
        [InlineData(.5, "FP1K-C", "SL", "C3x3.5", ".625", false, "P40305-FP1K-C-3-5-6-R")]
        [InlineData(.5, "FP1K-D", "SL", "C3x3.5", ".625", false, "P40305-FP1K-D-3-5-6-R")]
        [InlineData(.5, "FP2K-A", "SL", "C3x3.5", ".625", false, "P40305-FP2K-A-3-5-6-R")]
        [InlineData(.5, "FP2K-B", "SL", "C3x3.5", ".625", false, "P40305-FP2K-B-3-5-6-R")]
        [InlineData(.5, "FP3K-A", "SL", "C3x3.5", ".625", false, "P40305-FP3K-A-3-5-6-R")]
        [InlineData(.5, "FP3K-B", "SL", "C3x3.5", ".625", false, "P40305-FP3K-B-3-5-6-R")]
        [InlineData(.5, "FP3K-C", "SL", "C3x3.5", ".625", false, "P40305-FP3K-C-3-5-6-R")]
        [InlineData(.5, "FP3K-D", "SL", "C3x3.5", ".625", false, "P40305-FP3K-D-3-5-6-R")]
        [InlineData(.5, "FP4K-A", "SL", "C3x3.5", ".625", false, "P40305-FP4K-A-3-5-6-R")]
        [InlineData(.5, "FP4K-B", "SL", "C3x3.5", ".625", false, "P40305-FP4K-B-3-5-6-R")]
        [InlineData(.5, "FP5K-A", "SL", "C3x3.5", ".625", false, "P40305-FP5K-A-3-5-6-R")]
        [InlineData(.5, "FP5K-B", "SL", "C3x3.5", ".625", false, "P40305-FP5K-B-3-5-6-R")]
        [InlineData(.5, "FP5K-C", "SL", "C3x3.5", ".625", false, "P40305-FP5K-C-3-5-6-R")]
        [InlineData(.5, "FP5K-D", "SL", "C3x3.5", ".625", false, "P40305-FP5K-D-3-5-6-R")]
        [InlineData(.5, "FP6K-A", "SL", "C3x3.5", ".625", false, "P40305-FP6K-A-3-5-6-R")]
        [InlineData(.5, "FP6K-B", "SL", "C3x3.5", ".625", false, "P40305-FP6K-B-3-5-6-R")]
        [InlineData(.5, "FP1K-A", "SL", "C4x4.5", ".625", false, "P40305-FP1K-A-4-6-6-R")]
        [InlineData(.5, "FP1K-B", "SL", "C4x4.5", ".625", false, "P40305-FP1K-B-4-6-6-R")]
        [InlineData(.5, "FP1K-C", "SL", "C4x4.5", ".625", false, "P40305-FP1K-C-4-6-6-R")]
        [InlineData(.5, "FP1K-D", "SL", "C4x4.5", ".625", false, "P40305-FP1K-D-4-6-6-R")]
        [InlineData(.5, "FP2K-A", "SL", "C4x4.5", ".625", false, "P40305-FP2K-A-4-6-6-R")]
        [InlineData(.5, "FP2K-B", "SL", "C4x4.5", ".625", false, "P40305-FP2K-B-4-6-6-R")]
        [InlineData(.5, "FP3K-A", "SL", "C4x4.5", ".625", false, "P40305-FP3K-A-4-6-6-R")]
        [InlineData(.5, "FP3K-B", "SL", "C4x4.5", ".625", false, "P40305-FP3K-B-4-6-6-R")]
        [InlineData(.5, "FP3K-C", "SL", "C4x4.5", ".625", false, "P40305-FP3K-C-4-6-6-R")]
        [InlineData(.5, "FP3K-D", "SL", "C4x4.5", ".625", false, "P40305-FP3K-D-4-6-6-R")]
        [InlineData(.5, "FP4K-A", "SL", "C4x4.5", ".625", false, "P40305-FP4K-A-4-6-6-R")]
        [InlineData(.5, "FP4K-B", "SL", "C4x4.5", ".625", false, "P40305-FP4K-B-4-6-6-R")]
        [InlineData(.5, "FP5K-A", "SL", "C4x4.5", ".625", false, "P40305-FP5K-A-4-6-6-R")]
        [InlineData(.5, "FP5K-B", "SL", "C4x4.5", ".625", false, "P40305-FP5K-B-4-6-6-R")]
        [InlineData(.5, "FP5K-C", "SL", "C4x4.5", ".625", false, "P40305-FP5K-C-4-6-6-R")]
        [InlineData(.5, "FP5K-D", "SL", "C4x4.5", ".625", false, "P40305-FP5K-D-4-6-6-R")]
        [InlineData(.5, "FP6K-A", "SL", "C4x4.5", ".625", false, "P40305-FP6K-A-4-6-6-R")]
        [InlineData(.5, "FP6K-B", "SL", "C4x4.5", ".625", false, "P40305-FP6K-B-4-6-6-R")]
        [InlineData(.5, "FP1K-A", "SL", "C5x6.7", ".625", false, "P40305-FP1K-A-5-7-6-R")]
        [InlineData(.5, "FP1K-B", "SL", "C5x6.7", ".625", false, "P40305-FP1K-B-5-7-6-R")]
        [InlineData(.5, "FP1K-C", "SL", "C5x6.7", ".625", false, "P40305-FP1K-C-5-7-6-R")]
        [InlineData(.5, "FP1K-D", "SL", "C5x6.7", ".625", false, "P40305-FP1K-D-5-7-6-R")]
        [InlineData(.5, "FP2K-A", "SL", "C5x6.7", ".625", false, "P40305-FP2K-A-5-7-6-R")]
        [InlineData(.5, "FP2K-B", "SL", "C5x6.7", ".625", false, "P40305-FP2K-B-5-7-6-R")]
        [InlineData(.5, "FP3K-A", "SL", "C5x6.7", ".625", false, "P40305-FP3K-A-5-7-6-R")]
        [InlineData(.5, "FP3K-B", "SL", "C5x6.7", ".625", false, "P40305-FP3K-B-5-7-6-R")]
        [InlineData(.5, "FP3K-C", "SL", "C5x6.7", ".625", false, "P40305-FP3K-C-5-7-6-R")]
        [InlineData(.5, "FP3K-D", "SL", "C5x6.7", ".625", false, "P40305-FP3K-D-5-7-6-R")]
        [InlineData(.5, "FP4K-A", "SL", "C5x6.7", ".625", false, "P40305-FP4K-A-5-7-6-R")]
        [InlineData(.5, "FP4K-B", "SL", "C5x6.7", ".625", false, "P40305-FP4K-B-5-7-6-R")]
        [InlineData(.5, "FP5K-A", "SL", "C5x6.7", ".625", false, "P40305-FP5K-A-5-7-6-R")]
        [InlineData(.5, "FP5K-B", "SL", "C5x6.7", ".625", false, "P40305-FP5K-B-5-7-6-R")]
        [InlineData(.5, "FP5K-C", "SL", "C5x6.7", ".625", false, "P40305-FP5K-C-5-7-6-R")]
        [InlineData(.5, "FP5K-D", "SL", "C5x6.7", ".625", false, "P40305-FP5K-D-5-7-6-R")]
        [InlineData(.5, "FP6K-A", "SL", "C5x6.7", ".625", false, "P40305-FP6K-A-5-7-6-R")]
        [InlineData(.5, "FP6K-B", "SL", "C5x6.7", ".625", false, "P40305-FP6K-B-5-7-6-R")]
        [InlineData(.5, "FP1K-A", "SL", "C3x3.5", ".75", false, "P40305-FP1K-A-3-5-7-R")]
        [InlineData(.5, "FP1K-B", "SL", "C3x3.5", ".75", false, "P40305-FP1K-B-3-5-7-R")]
        [InlineData(.5, "FP1K-C", "SL", "C3x3.5", ".75", false, "P40305-FP1K-C-3-5-7-R")]
        [InlineData(.5, "FP1K-D", "SL", "C3x3.5", ".75", false, "P40305-FP1K-D-3-5-7-R")]
        [InlineData(.5, "FP2K-A", "SL", "C3x3.5", ".75", false, "P40305-FP2K-A-3-5-7-R")]
        [InlineData(.5, "FP2K-B", "SL", "C3x3.5", ".75", false, "P40305-FP2K-B-3-5-7-R")]
        [InlineData(.5, "FP3K-A", "SL", "C3x3.5", ".75", false, "P40305-FP3K-A-3-5-7-R")]
        [InlineData(.5, "FP3K-B", "SL", "C3x3.5", ".75", false, "P40305-FP3K-B-3-5-7-R")]
        [InlineData(.5, "FP3K-C", "SL", "C3x3.5", ".75", false, "P40305-FP3K-C-3-5-7-R")]
        [InlineData(.5, "FP3K-D", "SL", "C3x3.5", ".75", false, "P40305-FP3K-D-3-5-7-R")]
        [InlineData(.5, "FP4K-A", "SL", "C3x3.5", ".75", false, "P40305-FP4K-A-3-5-7-R")]
        [InlineData(.5, "FP4K-B", "SL", "C3x3.5", ".75", false, "P40305-FP4K-B-3-5-7-R")]
        [InlineData(.5, "FP5K-A", "SL", "C3x3.5", ".75", false, "P40305-FP5K-A-3-5-7-R")]
        [InlineData(.5, "FP5K-B", "SL", "C3x3.5", ".75", false, "P40305-FP5K-B-3-5-7-R")]
        [InlineData(.5, "FP5K-C", "SL", "C3x3.5", ".75", false, "P40305-FP5K-C-3-5-7-R")]
        [InlineData(.5, "FP5K-D", "SL", "C3x3.5", ".75", false, "P40305-FP5K-D-3-5-7-R")]
        [InlineData(.5, "FP6K-A", "SL", "C3x3.5", ".75", false, "P40305-FP6K-A-3-5-7-R")]
        [InlineData(.5, "FP6K-B", "SL", "C3x3.5", ".75", false, "P40305-FP6K-B-3-5-7-R")]
        [InlineData(.5, "FP1K-A", "SL", "C4x4.5", ".75", false, "P40305-FP1K-A-4-6-7-R")]
        [InlineData(.5, "FP1K-B", "SL", "C4x4.5", ".75", false, "P40305-FP1K-B-4-6-7-R")]
        [InlineData(.5, "FP1K-C", "SL", "C4x4.5", ".75", false, "P40305-FP1K-C-4-6-7-R")]
        [InlineData(.5, "FP1K-D", "SL", "C4x4.5", ".75", false, "P40305-FP1K-D-4-6-7-R")]
        [InlineData(.5, "FP2K-A", "SL", "C4x4.5", ".75", false, "P40305-FP2K-A-4-6-7-R")]
        [InlineData(.5, "FP2K-B", "SL", "C4x4.5", ".75", false, "P40305-FP2K-B-4-6-7-R")]
        [InlineData(.5, "FP3K-A", "SL", "C4x4.5", ".75", false, "P40305-FP3K-A-4-6-7-R")]
        [InlineData(.5, "FP3K-B", "SL", "C4x4.5", ".75", false, "P40305-FP3K-B-4-6-7-R")]
        [InlineData(.5, "FP3K-C", "SL", "C4x4.5", ".75", false, "P40305-FP3K-C-4-6-7-R")]
        [InlineData(.5, "FP3K-D", "SL", "C4x4.5", ".75", false, "P40305-FP3K-D-4-6-7-R")]
        [InlineData(.5, "FP4K-A", "SL", "C4x4.5", ".75", false, "P40305-FP4K-A-4-6-7-R")]
        [InlineData(.5, "FP4K-B", "SL", "C4x4.5", ".75", false, "P40305-FP4K-B-4-6-7-R")]
        [InlineData(.5, "FP5K-A", "SL", "C4x4.5", ".75", false, "P40305-FP5K-A-4-6-7-R")]
        [InlineData(.5, "FP5K-B", "SL", "C4x4.5", ".75", false, "P40305-FP5K-B-4-6-7-R")]
        [InlineData(.5, "FP5K-C", "SL", "C4x4.5", ".75", false, "P40305-FP5K-C-4-6-7-R")]
        [InlineData(.5, "FP5K-D", "SL", "C4x4.5", ".75", false, "P40305-FP5K-D-4-6-7-R")]
        [InlineData(.5, "FP6K-A", "SL", "C4x4.5", ".75", false, "P40305-FP6K-A-4-6-7-R")]
        [InlineData(.5, "FP6K-B", "SL", "C4x4.5", ".75", false, "P40305-FP6K-B-4-6-7-R")]
        [InlineData(.5, "FP1K-A", "SL", "C5x6.7", ".75", false, "P40305-FP1K-A-5-7-7-R")]
        [InlineData(.5, "FP1K-B", "SL", "C5x6.7", ".75", false, "P40305-FP1K-B-5-7-7-R")]
        [InlineData(.5, "FP1K-C", "SL", "C5x6.7", ".75", false, "P40305-FP1K-C-5-7-7-R")]
        [InlineData(.5, "FP1K-D", "SL", "C5x6.7", ".75", false, "P40305-FP1K-D-5-7-7-R")]
        [InlineData(.5, "FP2K-A", "SL", "C5x6.7", ".75", false, "P40305-FP2K-A-5-7-7-R")]
        [InlineData(.5, "FP2K-B", "SL", "C5x6.7", ".75", false, "P40305-FP2K-B-5-7-7-R")]
        [InlineData(.5, "FP3K-A", "SL", "C5x6.7", ".75", false, "P40305-FP3K-A-5-7-7-R")]
        [InlineData(.5, "FP3K-B", "SL", "C5x6.7", ".75", false, "P40305-FP3K-B-5-7-7-R")]
        [InlineData(.5, "FP3K-C", "SL", "C5x6.7", ".75", false, "P40305-FP3K-C-5-7-7-R")]
        [InlineData(.5, "FP3K-D", "SL", "C5x6.7", ".75", false, "P40305-FP3K-D-5-7-7-R")]
        [InlineData(.5, "FP4K-A", "SL", "C5x6.7", ".75", false, "P40305-FP4K-A-5-7-7-R")]
        [InlineData(.5, "FP4K-B", "SL", "C5x6.7", ".75", false, "P40305-FP4K-B-5-7-7-R")]
        [InlineData(.5, "FP5K-A", "SL", "C5x6.7", ".75", false, "P40305-FP5K-A-5-7-7-R")]
        [InlineData(.5, "FP5K-B", "SL", "C5x6.7", ".75", false, "P40305-FP5K-B-5-7-7-R")]
        [InlineData(.5, "FP5K-C", "SL", "C5x6.7", ".75", false, "P40305-FP5K-C-5-7-7-R")]
        [InlineData(.5, "FP5K-D", "SL", "C5x6.7", ".75", false, "P40305-FP5K-D-5-7-7-R")]
        [InlineData(.5, "FP6K-A", "SL", "C5x6.7", ".75", false, "P40305-FP6K-A-5-7-7-R")]
        [InlineData(.5, "FP6K-B", "SL", "C5x6.7", ".75", false, "P40305-FP6K-B-5-7-7-R")]
        //5/8in plate thickness
        [InlineData(.625, "FP1K-A", "SL", "C3x3.5", ".5", false, "P40306-FP1K-A-3-5-5-R")]
        [InlineData(.625, "FP1K-B", "SL", "C3x3.5", ".5", false, "P40306-FP1K-B-3-5-5-R")]
        [InlineData(.625, "FP1K-C", "SL", "C3x3.5", ".5", false, "P40306-FP1K-C-3-5-5-R")]
        [InlineData(.625, "FP1K-D", "SL", "C3x3.5", ".5", false, "P40306-FP1K-D-3-5-5-R")]
        [InlineData(.625, "FP2K-A", "SL", "C3x3.5", ".5", false, "P40306-FP2K-A-3-5-5-R")]
        [InlineData(.625, "FP2K-B", "SL", "C3x3.5", ".5", false, "P40306-FP2K-B-3-5-5-R")]
        [InlineData(.625, "FP3K-A", "SL", "C3x3.5", ".5", false, "P40306-FP3K-A-3-5-5-R")]
        [InlineData(.625, "FP3K-B", "SL", "C3x3.5", ".5", false, "P40306-FP3K-B-3-5-5-R")]
        [InlineData(.625, "FP3K-C", "SL", "C3x3.5", ".5", false, "P40306-FP3K-C-3-5-5-R")]
        [InlineData(.625, "FP3K-D", "SL", "C3x3.5", ".5", false, "P40306-FP3K-D-3-5-5-R")]
        [InlineData(.625, "FP4K-A", "SL", "C3x3.5", ".5", false, "P40306-FP4K-A-3-5-5-R")]
        [InlineData(.625, "FP4K-B", "SL", "C3x3.5", ".5", false, "P40306-FP4K-B-3-5-5-R")]
        [InlineData(.625, "FP5K-A", "SL", "C3x3.5", ".5", false, "P40306-FP5K-A-3-5-5-R")]
        [InlineData(.625, "FP5K-B", "SL", "C3x3.5", ".5", false, "P40306-FP5K-B-3-5-5-R")]
        [InlineData(.625, "FP5K-C", "SL", "C3x3.5", ".5", false, "P40306-FP5K-C-3-5-5-R")]
        [InlineData(.625, "FP5K-D", "SL", "C3x3.5", ".5", false, "P40306-FP5K-D-3-5-5-R")]
        [InlineData(.625, "FP6K-A", "SL", "C3x3.5", ".5", false, "P40306-FP6K-A-3-5-5-R")]
        [InlineData(.625, "FP6K-B", "SL", "C3x3.5", ".5", false, "P40306-FP6K-B-3-5-5-R")]
        [InlineData(.625, "FP1K-A", "SL", "C4x4.5", ".5", false, "P40306-FP1K-A-4-6-5-R")]
        [InlineData(.625, "FP1K-B", "SL", "C4x4.5", ".5", false, "P40306-FP1K-B-4-6-5-R")]
        [InlineData(.625, "FP1K-C", "SL", "C4x4.5", ".5", false, "P40306-FP1K-C-4-6-5-R")]
        [InlineData(.625, "FP1K-D", "SL", "C4x4.5", ".5", false, "P40306-FP1K-D-4-6-5-R")]
        [InlineData(.625, "FP2K-A", "SL", "C4x4.5", ".5", false, "P40306-FP2K-A-4-6-5-R")]
        [InlineData(.625, "FP2K-B", "SL", "C4x4.5", ".5", false, "P40306-FP2K-B-4-6-5-R")]
        [InlineData(.625, "FP3K-A", "SL", "C4x4.5", ".5", false, "P40306-FP3K-A-4-6-5-R")]
        [InlineData(.625, "FP3K-B", "SL", "C4x4.5", ".5", false, "P40306-FP3K-B-4-6-5-R")]
        [InlineData(.625, "FP3K-C", "SL", "C4x4.5", ".5", false, "P40306-FP3K-C-4-6-5-R")]
        [InlineData(.625, "FP3K-D", "SL", "C4x4.5", ".5", false, "P40306-FP3K-D-4-6-5-R")]
        [InlineData(.625, "FP4K-A", "SL", "C4x4.5", ".5", false, "P40306-FP4K-A-4-6-5-R")]
        [InlineData(.625, "FP4K-B", "SL", "C4x4.5", ".5", false, "P40306-FP4K-B-4-6-5-R")]
        [InlineData(.625, "FP5K-A", "SL", "C4x4.5", ".5", false, "P40306-FP5K-A-4-6-5-R")]
        [InlineData(.625, "FP5K-B", "SL", "C4x4.5", ".5", false, "P40306-FP5K-B-4-6-5-R")]
        [InlineData(.625, "FP5K-C", "SL", "C4x4.5", ".5", false, "P40306-FP5K-C-4-6-5-R")]
        [InlineData(.625, "FP5K-D", "SL", "C4x4.5", ".5", false, "P40306-FP5K-D-4-6-5-R")]
        [InlineData(.625, "FP6K-A", "SL", "C4x4.5", ".5", false, "P40306-FP6K-A-4-6-5-R")]
        [InlineData(.625, "FP6K-B", "SL", "C4x4.5", ".5", false, "P40306-FP6K-B-4-6-5-R")]
        [InlineData(.625, "FP1K-A", "SL", "C5x6.7", ".5", false, "P40306-FP1K-A-5-7-5-R")]
        [InlineData(.625, "FP1K-B", "SL", "C5x6.7", ".5", false, "P40306-FP1K-B-5-7-5-R")]
        [InlineData(.625, "FP1K-C", "SL", "C5x6.7", ".5", false, "P40306-FP1K-C-5-7-5-R")]
        [InlineData(.625, "FP1K-D", "SL", "C5x6.7", ".5", false, "P40306-FP1K-D-5-7-5-R")]
        [InlineData(.625, "FP2K-A", "SL", "C5x6.7", ".5", false, "P40306-FP2K-A-5-7-5-R")]
        [InlineData(.625, "FP2K-B", "SL", "C5x6.7", ".5", false, "P40306-FP2K-B-5-7-5-R")]
        [InlineData(.625, "FP3K-A", "SL", "C5x6.7", ".5", false, "P40306-FP3K-A-5-7-5-R")]
        [InlineData(.625, "FP3K-B", "SL", "C5x6.7", ".5", false, "P40306-FP3K-B-5-7-5-R")]
        [InlineData(.625, "FP3K-C", "SL", "C5x6.7", ".5", false, "P40306-FP3K-C-5-7-5-R")]
        [InlineData(.625, "FP3K-D", "SL", "C5x6.7", ".5", false, "P40306-FP3K-D-5-7-5-R")]
        [InlineData(.625, "FP4K-A", "SL", "C5x6.7", ".5", false, "P40306-FP4K-A-5-7-5-R")]
        [InlineData(.625, "FP4K-B", "SL", "C5x6.7", ".5", false, "P40306-FP4K-B-5-7-5-R")]
        [InlineData(.625, "FP5K-A", "SL", "C5x6.7", ".5", false, "P40306-FP5K-A-5-7-5-R")]
        [InlineData(.625, "FP5K-B", "SL", "C5x6.7", ".5", false, "P40306-FP5K-B-5-7-5-R")]
        [InlineData(.625, "FP5K-C", "SL", "C5x6.7", ".5", false, "P40306-FP5K-C-5-7-5-R")]
        [InlineData(.625, "FP5K-D", "SL", "C5x6.7", ".5", false, "P40306-FP5K-D-5-7-5-R")]
        [InlineData(.625, "FP6K-A", "SL", "C5x6.7", ".5", false, "P40306-FP6K-A-5-7-5-R")]
        [InlineData(.625, "FP6K-B", "SL", "C5x6.7", ".5", false, "P40306-FP6K-B-5-7-5-R")]
        [InlineData(.625, "FP1K-A", "SL", "C3x3.5", ".625", false, "P40306-FP1K-A-3-5-6-R")]
        [InlineData(.625, "FP1K-B", "SL", "C3x3.5", ".625", false, "P40306-FP1K-B-3-5-6-R")]
        [InlineData(.625, "FP1K-C", "SL", "C3x3.5", ".625", false, "P40306-FP1K-C-3-5-6-R")]
        [InlineData(.625, "FP1K-D", "SL", "C3x3.5", ".625", false, "P40306-FP1K-D-3-5-6-R")]
        [InlineData(.625, "FP2K-A", "SL", "C3x3.5", ".625", false, "P40306-FP2K-A-3-5-6-R")]
        [InlineData(.625, "FP2K-B", "SL", "C3x3.5", ".625", false, "P40306-FP2K-B-3-5-6-R")]
        [InlineData(.625, "FP3K-A", "SL", "C3x3.5", ".625", false, "P40306-FP3K-A-3-5-6-R")]
        [InlineData(.625, "FP3K-B", "SL", "C3x3.5", ".625", false, "P40306-FP3K-B-3-5-6-R")]
        [InlineData(.625, "FP3K-C", "SL", "C3x3.5", ".625", false, "P40306-FP3K-C-3-5-6-R")]
        [InlineData(.625, "FP3K-D", "SL", "C3x3.5", ".625", false, "P40306-FP3K-D-3-5-6-R")]
        [InlineData(.625, "FP4K-A", "SL", "C3x3.5", ".625", false, "P40306-FP4K-A-3-5-6-R")]
        [InlineData(.625, "FP4K-B", "SL", "C3x3.5", ".625", false, "P40306-FP4K-B-3-5-6-R")]
        [InlineData(.625, "FP5K-A", "SL", "C3x3.5", ".625", false, "P40306-FP5K-A-3-5-6-R")]
        [InlineData(.625, "FP5K-B", "SL", "C3x3.5", ".625", false, "P40306-FP5K-B-3-5-6-R")]
        [InlineData(.625, "FP5K-C", "SL", "C3x3.5", ".625", false, "P40306-FP5K-C-3-5-6-R")]
        [InlineData(.625, "FP5K-D", "SL", "C3x3.5", ".625", false, "P40306-FP5K-D-3-5-6-R")]
        [InlineData(.625, "FP6K-A", "SL", "C3x3.5", ".625", false, "P40306-FP6K-A-3-5-6-R")]
        [InlineData(.625, "FP6K-B", "SL", "C3x3.5", ".625", false, "P40306-FP6K-B-3-5-6-R")]
        [InlineData(.625, "FP1K-A", "SL", "C4x4.5", ".625", false, "P40306-FP1K-A-4-6-6-R")]
        [InlineData(.625, "FP1K-B", "SL", "C4x4.5", ".625", false, "P40306-FP1K-B-4-6-6-R")]
        [InlineData(.625, "FP1K-C", "SL", "C4x4.5", ".625", false, "P40306-FP1K-C-4-6-6-R")]
        [InlineData(.625, "FP1K-D", "SL", "C4x4.5", ".625", false, "P40306-FP1K-D-4-6-6-R")]
        [InlineData(.625, "FP2K-A", "SL", "C4x4.5", ".625", false, "P40306-FP2K-A-4-6-6-R")]
        [InlineData(.625, "FP2K-B", "SL", "C4x4.5", ".625", false, "P40306-FP2K-B-4-6-6-R")]
        [InlineData(.625, "FP3K-A", "SL", "C4x4.5", ".625", false, "P40306-FP3K-A-4-6-6-R")]
        [InlineData(.625, "FP3K-B", "SL", "C4x4.5", ".625", false, "P40306-FP3K-B-4-6-6-R")]
        [InlineData(.625, "FP3K-C", "SL", "C4x4.5", ".625", false, "P40306-FP3K-C-4-6-6-R")]
        [InlineData(.625, "FP3K-D", "SL", "C4x4.5", ".625", false, "P40306-FP3K-D-4-6-6-R")]
        [InlineData(.625, "FP4K-A", "SL", "C4x4.5", ".625", false, "P40306-FP4K-A-4-6-6-R")]
        [InlineData(.625, "FP4K-B", "SL", "C4x4.5", ".625", false, "P40306-FP4K-B-4-6-6-R")]
        [InlineData(.625, "FP5K-A", "SL", "C4x4.5", ".625", false, "P40306-FP5K-A-4-6-6-R")]
        [InlineData(.625, "FP5K-B", "SL", "C4x4.5", ".625", false, "P40306-FP5K-B-4-6-6-R")]
        [InlineData(.625, "FP5K-C", "SL", "C4x4.5", ".625", false, "P40306-FP5K-C-4-6-6-R")]
        [InlineData(.625, "FP5K-D", "SL", "C4x4.5", ".625", false, "P40306-FP5K-D-4-6-6-R")]
        [InlineData(.625, "FP6K-A", "SL", "C4x4.5", ".625", false, "P40306-FP6K-A-4-6-6-R")]
        [InlineData(.625, "FP6K-B", "SL", "C4x4.5", ".625", false, "P40306-FP6K-B-4-6-6-R")]
        [InlineData(.625, "FP1K-A", "SL", "C5x6.7", ".625", false, "P40306-FP1K-A-5-7-6-R")]
        [InlineData(.625, "FP1K-B", "SL", "C5x6.7", ".625", false, "P40306-FP1K-B-5-7-6-R")]
        [InlineData(.625, "FP1K-C", "SL", "C5x6.7", ".625", false, "P40306-FP1K-C-5-7-6-R")]
        [InlineData(.625, "FP1K-D", "SL", "C5x6.7", ".625", false, "P40306-FP1K-D-5-7-6-R")]
        [InlineData(.625, "FP2K-A", "SL", "C5x6.7", ".625", false, "P40306-FP2K-A-5-7-6-R")]
        [InlineData(.625, "FP2K-B", "SL", "C5x6.7", ".625", false, "P40306-FP2K-B-5-7-6-R")]
        [InlineData(.625, "FP3K-A", "SL", "C5x6.7", ".625", false, "P40306-FP3K-A-5-7-6-R")]
        [InlineData(.625, "FP3K-B", "SL", "C5x6.7", ".625", false, "P40306-FP3K-B-5-7-6-R")]
        [InlineData(.625, "FP3K-C", "SL", "C5x6.7", ".625", false, "P40306-FP3K-C-5-7-6-R")]
        [InlineData(.625, "FP3K-D", "SL", "C5x6.7", ".625", false, "P40306-FP3K-D-5-7-6-R")]
        [InlineData(.625, "FP4K-A", "SL", "C5x6.7", ".625", false, "P40306-FP4K-A-5-7-6-R")]
        [InlineData(.625, "FP4K-B", "SL", "C5x6.7", ".625", false, "P40306-FP4K-B-5-7-6-R")]
        [InlineData(.625, "FP5K-A", "SL", "C5x6.7", ".625", false, "P40306-FP5K-A-5-7-6-R")]
        [InlineData(.625, "FP5K-B", "SL", "C5x6.7", ".625", false, "P40306-FP5K-B-5-7-6-R")]
        [InlineData(.625, "FP5K-C", "SL", "C5x6.7", ".625", false, "P40306-FP5K-C-5-7-6-R")]
        [InlineData(.625, "FP5K-D", "SL", "C5x6.7", ".625", false, "P40306-FP5K-D-5-7-6-R")]
        [InlineData(.625, "FP6K-A", "SL", "C5x6.7", ".625", false, "P40306-FP6K-A-5-7-6-R")]
        [InlineData(.625, "FP6K-B", "SL", "C5x6.7", ".625", false, "P40306-FP6K-B-5-7-6-R")]
        [InlineData(.625, "FP1K-A", "SL", "C3x3.5", ".75", false, "P40306-FP1K-A-3-5-7-R")]
        [InlineData(.625, "FP1K-B", "SL", "C3x3.5", ".75", false, "P40306-FP1K-B-3-5-7-R")]
        [InlineData(.625, "FP1K-C", "SL", "C3x3.5", ".75", false, "P40306-FP1K-C-3-5-7-R")]
        [InlineData(.625, "FP1K-D", "SL", "C3x3.5", ".75", false, "P40306-FP1K-D-3-5-7-R")]
        [InlineData(.625, "FP2K-A", "SL", "C3x3.5", ".75", false, "P40306-FP2K-A-3-5-7-R")]
        [InlineData(.625, "FP2K-B", "SL", "C3x3.5", ".75", false, "P40306-FP2K-B-3-5-7-R")]
        [InlineData(.625, "FP3K-A", "SL", "C3x3.5", ".75", false, "P40306-FP3K-A-3-5-7-R")]
        [InlineData(.625, "FP3K-B", "SL", "C3x3.5", ".75", false, "P40306-FP3K-B-3-5-7-R")]
        [InlineData(.625, "FP3K-C", "SL", "C3x3.5", ".75", false, "P40306-FP3K-C-3-5-7-R")]
        [InlineData(.625, "FP3K-D", "SL", "C3x3.5", ".75", false, "P40306-FP3K-D-3-5-7-R")]
        [InlineData(.625, "FP4K-A", "SL", "C3x3.5", ".75", false, "P40306-FP4K-A-3-5-7-R")]
        [InlineData(.625, "FP4K-B", "SL", "C3x3.5", ".75", false, "P40306-FP4K-B-3-5-7-R")]
        [InlineData(.625, "FP5K-A", "SL", "C3x3.5", ".75", false, "P40306-FP5K-A-3-5-7-R")]
        [InlineData(.625, "FP5K-B", "SL", "C3x3.5", ".75", false, "P40306-FP5K-B-3-5-7-R")]
        [InlineData(.625, "FP5K-C", "SL", "C3x3.5", ".75", false, "P40306-FP5K-C-3-5-7-R")]
        [InlineData(.625, "FP5K-D", "SL", "C3x3.5", ".75", false, "P40306-FP5K-D-3-5-7-R")]
        [InlineData(.625, "FP6K-A", "SL", "C3x3.5", ".75", false, "P40306-FP6K-A-3-5-7-R")]
        [InlineData(.625, "FP6K-B", "SL", "C3x3.5", ".75", false, "P40306-FP6K-B-3-5-7-R")]
        [InlineData(.625, "FP1K-A", "SL", "C4x4.5", ".75", false, "P40306-FP1K-A-4-6-7-R")]
        [InlineData(.625, "FP1K-B", "SL", "C4x4.5", ".75", false, "P40306-FP1K-B-4-6-7-R")]
        [InlineData(.625, "FP1K-C", "SL", "C4x4.5", ".75", false, "P40306-FP1K-C-4-6-7-R")]
        [InlineData(.625, "FP1K-D", "SL", "C4x4.5", ".75", false, "P40306-FP1K-D-4-6-7-R")]
        [InlineData(.625, "FP2K-A", "SL", "C4x4.5", ".75", false, "P40306-FP2K-A-4-6-7-R")]
        [InlineData(.625, "FP2K-B", "SL", "C4x4.5", ".75", false, "P40306-FP2K-B-4-6-7-R")]
        [InlineData(.625, "FP3K-A", "SL", "C4x4.5", ".75", false, "P40306-FP3K-A-4-6-7-R")]
        [InlineData(.625, "FP3K-B", "SL", "C4x4.5", ".75", false, "P40306-FP3K-B-4-6-7-R")]
        [InlineData(.625, "FP3K-C", "SL", "C4x4.5", ".75", false, "P40306-FP3K-C-4-6-7-R")]
        [InlineData(.625, "FP3K-D", "SL", "C4x4.5", ".75", false, "P40306-FP3K-D-4-6-7-R")]
        [InlineData(.625, "FP4K-A", "SL", "C4x4.5", ".75", false, "P40306-FP4K-A-4-6-7-R")]
        [InlineData(.625, "FP4K-B", "SL", "C4x4.5", ".75", false, "P40306-FP4K-B-4-6-7-R")]
        [InlineData(.625, "FP5K-A", "SL", "C4x4.5", ".75", false, "P40306-FP5K-A-4-6-7-R")]
        [InlineData(.625, "FP5K-B", "SL", "C4x4.5", ".75", false, "P40306-FP5K-B-4-6-7-R")]
        [InlineData(.625, "FP5K-C", "SL", "C4x4.5", ".75", false, "P40306-FP5K-C-4-6-7-R")]
        [InlineData(.625, "FP5K-D", "SL", "C4x4.5", ".75", false, "P40306-FP5K-D-4-6-7-R")]
        [InlineData(.625, "FP6K-A", "SL", "C4x4.5", ".75", false, "P40306-FP6K-A-4-6-7-R")]
        [InlineData(.625, "FP6K-B", "SL", "C4x4.5", ".75", false, "P40306-FP6K-B-4-6-7-R")]
        [InlineData(.625, "FP1K-A", "SL", "C5x6.7", ".75", false, "P40306-FP1K-A-5-7-7-R")]
        [InlineData(.625, "FP1K-B", "SL", "C5x6.7", ".75", false, "P40306-FP1K-B-5-7-7-R")]
        [InlineData(.625, "FP1K-C", "SL", "C5x6.7", ".75", false, "P40306-FP1K-C-5-7-7-R")]
        [InlineData(.625, "FP1K-D", "SL", "C5x6.7", ".75", false, "P40306-FP1K-D-5-7-7-R")]
        [InlineData(.625, "FP2K-A", "SL", "C5x6.7", ".75", false, "P40306-FP2K-A-5-7-7-R")]
        [InlineData(.625, "FP2K-B", "SL", "C5x6.7", ".75", false, "P40306-FP2K-B-5-7-7-R")]
        [InlineData(.625, "FP3K-A", "SL", "C5x6.7", ".75", false, "P40306-FP3K-A-5-7-7-R")]
        [InlineData(.625, "FP3K-B", "SL", "C5x6.7", ".75", false, "P40306-FP3K-B-5-7-7-R")]
        [InlineData(.625, "FP3K-C", "SL", "C5x6.7", ".75", false, "P40306-FP3K-C-5-7-7-R")]
        [InlineData(.625, "FP3K-D", "SL", "C5x6.7", ".75", false, "P40306-FP3K-D-5-7-7-R")]
        [InlineData(.625, "FP4K-A", "SL", "C5x6.7", ".75", false, "P40306-FP4K-A-5-7-7-R")]
        [InlineData(.625, "FP4K-B", "SL", "C5x6.7", ".75", false, "P40306-FP4K-B-5-7-7-R")]
        [InlineData(.625, "FP5K-A", "SL", "C5x6.7", ".75", false, "P40306-FP5K-A-5-7-7-R")]
        [InlineData(.625, "FP5K-B", "SL", "C5x6.7", ".75", false, "P40306-FP5K-B-5-7-7-R")]
        [InlineData(.625, "FP5K-C", "SL", "C5x6.7", ".75", false, "P40306-FP5K-C-5-7-7-R")]
        [InlineData(.625, "FP5K-D", "SL", "C5x6.7", ".75", false, "P40306-FP5K-D-5-7-7-R")]
        [InlineData(.625, "FP6K-A", "SL", "C5x6.7", ".75", false, "P40306-FP6K-A-5-7-7-R")]
        [InlineData(.625, "FP6K-B", "SL", "C5x6.7", ".75", false, "P40306-FP6K-B-5-7-7-R")]
        public void CanGetFootplatePartNumber(float plateThickness, string footPlateType, string uprightType, string uprightColSize, string anchorSize, bool isFront, string expPartNumber)
        {
            var colAssy = _fixture.CreateRootPart<UprightColumnAssembly>();
            colAssy.UprightColumnSize.SetValue(uprightColSize);
            colAssy.FootplateThickness.SetValue(plateThickness);
            colAssy.FootPlateType.SetValue(footPlateType);
            colAssy.UprightType.SetValue(uprightType);
            
            colAssy.AnchorSize.SetValue(anchorSize);
            colAssy.IsFrontUprightColumn.SetValue(isFront);

            var part = colAssy.Footplate;

            Assert.Equal(expPartNumber, part.PartNumber.Value);
        }

        #endregion

        

        #region Hardware Tests

        [Fact]
        public void CanGetHardwareKitPartNumberPrefix()
        {
            var part = _fixture.CreateRootPart<HardwareKit>();

            Assert.Equal("_HDWKIT-", part.PartNumberPrefix.Value);
        }

        [Fact]
        public void CanIncrementHardwareKitPartNumber()
        {
            var part1 = _fixture.CreateRootPart<HardwareKit>();
            var part2 = _fixture.CreateRootPart<HardwareKit>();
            part2.Length.SetValue(3);

            var pn1 = part1.PartNumber.Value;
            var pn2 = part2.PartNumber.Value;

            Assert.True(GetIncrement(pn1) != GetIncrement(pn2));
        }

        [Theory]
        [InlineData(0.5f, 1, BoltTypes.Hex, HardwareMaterial.Grade5, "HW.BLT.TPA61AG5")]
        [InlineData(0.5f, 1.25, BoltTypes.Hex, HardwareMaterial.Grade5, "HW.BLT.TPA614G5")]
        [InlineData(0.5f, 4, BoltTypes.Hex, HardwareMaterial.Grade5, "HW.BLT.TPA64AG5")]
        [InlineData(0.5f, 1, BoltTypes.Carriage, HardwareMaterial.Grade5, "HW.BLT.CRA625G5")]
        [InlineData(0.5f, 1, BoltTypes.ButtonHead, HardwareMaterial.Grade5, "HW.BTN.CPA61AZIN")]
        [InlineData(0.5f, 2, BoltTypes.CapScrew, HardwareMaterial.A325, "HW.BLT.CPA62AA32")]
        public void CanGetBoltPartNumbers(float diameter, float length, string type, string material, string expectedPartNumber)
        {
            var bolt = _fixture.CreateRootPart<Bolt>();
            var hardwareKit = _fixture.CreateRootPart<HardwareKit>();

            hardwareKit.BoltSize.SetValue(diameter);
            hardwareKit.Length.SetValue(length);
            hardwareKit.BoltType.SetValue(type);
            hardwareKit.BoltMaterial.SetValue(material);

            bolt.BoltDiameter.SetValue(diameter);
            bolt.Length.SetValue(length);
            bolt.BoltType.SetValue(type);
            bolt.BoltMaterial.SetValue(material);

            Assert.Equal(expectedPartNumber, bolt.PartNumber.Value);
            Assert.Equal(expectedPartNumber, hardwareKit.Bolt.PartNumber.Value);
        }

        [Theory]
        [InlineData(0.5f, NutTypes.Hex, HardwareMaterial.Grade5, "HW.NUT.HXA613G5")]
        [InlineData(0.5f, NutTypes.Flange, HardwareMaterial.Grade5, "HW.NUT.FLA613G5")]
        [InlineData(0.375f, NutTypes.Flange, HardwareMaterial.ZincPlated, "HW.NUT.FLA116")]
        public void CanGetNutPartNumbers(float diameter, string type, string material, string expectedPartNumber)
        {
            var nut = _fixture.CreateRootPart<Nut>();
            var hardwareKit = _fixture.CreateRootPart<HardwareKit>();

            hardwareKit.BoltSize.SetValue(diameter);
            hardwareKit.NutType.SetValue(type);
            hardwareKit.NutMaterial.SetValue(material);

            nut.BoltDiameter.SetValue(diameter);
            nut.NutType.SetValue(type);
            nut.NutMaterial.SetValue(material);

            Assert.Equal(expectedPartNumber, nut.PartNumber.Value);
            Assert.Equal(expectedPartNumber, hardwareKit.Nut.PartNumber.Value);
        }

        [Theory]
        [InlineData(0.5f, WasherTypes.Clipped, HardwareMaterial.ZincPlated, "HW.WSH.CL6")]
        [InlineData(0.5f, WasherTypes.Flat, HardwareMaterial.Grade2, "HW.WSH.FL6")]
        [InlineData(0.375f, WasherTypes.Flat, HardwareMaterial.Grade2, "HW.WSH.FL1")]
        public void CanGetWashersPartNumbers(float diameter, string type, string material, string expectedPartNumber)
        {
            var washers = _fixture.CreateRootPart<Washers>();

            washers.BoltDiameter.SetValue(diameter);
            washers.WasherType.SetValue(type);
            washers.WasherMaterial.SetValue(material);

            Assert.Equal(expectedPartNumber, washers.PartNumber.Value);
        }

        #endregion

        #endregion


        #region Helper Functions

        string GetIncrement(string partNumber)
        {
            return partNumber.Split("-").LastOrDefault();
        }

        #endregion

    }
}
