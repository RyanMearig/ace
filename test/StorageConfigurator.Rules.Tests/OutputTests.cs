﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using Ruler.Cpq;
using StorageConfigurator.Outputs;
using Xunit;
using StorageConfigurator.TestData;

namespace StorageConfigurator.Rules.Tests
{
    [Collection(nameof(RulesCollection))]
    public class OutputTests
    {

        #region Private Fields

        private readonly StorageConfiguratorFixture _fixture;

        #endregion

        public OutputTests(StorageConfiguratorFixture fixture)
        {
            _fixture = fixture;
        }

        //[Fact]
        //public async void CanGenerateBOMDetailDoc()
        //{
        //    var testFile = "TestProject.json";
        //    var projectJson = GetTestData(testFile);
        //    var reportGen = new ASReportGenerator();
        //    var reports = await reportGen.GetReports(projectJson, new List<BomSummaryItem>());

        //    //var assembly = typeof(OutputTests).Assembly;
        //    //var uri = new UriBuilder(assembly.CodeBase);
            
        //    Assert.All(reports, r => System.IO.File.Exists(r.TempPath));
        //}

        //private string GetTestData(string filename)
        //{
        //    string summary;
        //    var assembly = Assembly.GetExecutingAssembly();

        //    using (var reader = new StreamReader(assembly.GetManifestResourceStream($"StorageConfigurator.Rules.Tests.TestData.{filename}")))
        //    {
        //        summary = reader.ReadToEnd().ToString();
        //    }

        //    return summary;
        //}

    }
}
