﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ruler.Cpq;
using Ruler.Rules;
using Xunit;
using StorageConfigurator.Plugin;
using StorageConfigurator.TestData;

namespace StorageConfigurator.Rules.Tests
{
    [Collection(nameof(RulesCollection))]
    public class CPQTests
    {

        #region Private Fields

        private readonly StorageConfiguratorFixture _fixture;

        #endregion

        public CPQTests(StorageConfiguratorFixture fixture)
        {
            _fixture = fixture;
        }

        #region Tests

        [Theory]
        [InlineData(1, 4.7749)]
        [InlineData(2, 6.0498)]
        [InlineData(3, 7.3247)]
        public void CanSetRowQtyAndGetBomValues(int rowQty, double expHrs)
        {
            var part = _fixture.CreateRootPart<StorageSystem>();
            var bay = part.Bays.AddChild<Bay>();
            part.FrameTypes.AddChild<SelectiveFrameType>();
            part.FrameLines.AddChild<SelectiveFrameLine>();

            var testRowType = part.RowTypes.AddChild<Row>();
            testRowType.RowQty.SetValue(rowQty);
            part.AddChild<RowProxy>();

            var bomSummarizer = new StorageBomSummarizer();
            var bomSummary = bomSummarizer.GetBomSummary(part.Bom);

            Assert.Equal(expHrs, GetTotalLaborByCategory(bomSummary, "weld"), 4);
            Assert.True(bomSummary.TotalCost > 0);
            Assert.True(bomSummary.TotalPrice > 0);
        }

        [Theory]
        [InlineData("GA_C", 2.16)]
        [InlineData("UT_SLC", 2.35)]
        public void CanGetPlantPricing(string plantCode, double expCost)
        {
            var part = _fixture.CreateRootPart<ShelfLoadBeam>();
            part.Bom.PlantCode.SetValue(plantCode);
            var labor = part.Bom.LaborItems.Value.FirstOrDefault(li => li.OperationName.ToLower().Contains("weld time"));
            var laborCost = labor.Cost;
            Assert.Equal(expCost, laborCost, 2);
        }

        [Theory]
        [InlineData("GA_C")]
        [InlineData("UT_SLC")]
        public void CanGetPlantCodePropagation(string plantCode)
        {
            var part = _fixture.CreateRootPart<StorageSystem>();
            part.Bom.PlantCode.SetValue(plantCode);

            var bay = part.Bays.AddChild<Bay>();
            var frameType = part.FrameTypes.AddChild<SelectiveFrameType>();
            part.FrameLines.AddChild<SelectiveFrameLine>();
            var testRow = part.AddChild<Row>();
            testRow.RowQty.SetValue(1);

            Assert.Equal(part.Bom.PlantCode.Value, frameType.Frame.Frame.Bom.PlantCode.Value);
        }


        #region Shelf Load Beam Labor Tests

        [Theory]
        [InlineData("C3x3.5", "8", 48, 0.0026)]
        [InlineData("C3x3.5", "8", 100, 0.0038)]
        [InlineData("C3x3.5", "8", 120, 0.0077)]
        [InlineData("C3x3.5", "12", 120, 0.0154)]
        [InlineData("C3x4.1", "8", 120, 0.0077)]
        [InlineData("C3x4.1", "12", 120, 0.0154)]
        [InlineData("C4x4.5", "8", 120, 0.0077)]
        [InlineData("C4x4.5", "12", 120, 0.0154)]
        [InlineData("C4x5.4", "8", 120, 0.0077)]
        [InlineData("C4x5.4", "12", 120, 0.0154)]
        [InlineData("C5x6.1", "8", 120, 0.0154)]
        [InlineData("C5x6.1", "12", 120, 0.0308)]
        [InlineData("C5x6.7", "8", 120, 0.0154)]
        [InlineData("C5x6.7", "12", 120, 0.0308)]
        [InlineData("C6x8.2", "8", 120, 0.0192)]
        [InlineData("C6x8.2", "12", 120, 0.0385)]
        [InlineData("C7x9.8", "8", 120, 0.0192)]
        [InlineData("C7x9.8", "12", 120, 0.0385)]
        [InlineData("C8x11.5", "8", 120, 0.0256)]
        [InlineData("C8x11.5", "12", 120, 0.0513)]
        [InlineData("C10x15.3", "8", 120, 0.0385)]
        [InlineData("C10x15.3", "12", 120, 0.0769)]
        public void CanGetShelfLoadBeamPaintLabor(string channelSize, string bracketType, float width, float expHrs)
        {
            var part = _fixture.CreateRootPart<ShelfLoadBeam>();
            //part.Bom.LaborHourRounding.SetValue(0.01f);
            part.ChannelSize.SetValue(channelSize);
            part.BracketType.SetValue(bracketType);
            part.Width.SetValue(width);

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(expHrs, labor.First(l => l.OperationName.ToLower().Contains("paint")).Hours, 4);
        }

        [Theory]
        [InlineData("C3x3.5", 100, 0.0333)]
        [InlineData("C3x3.5", 120, 0.04)]
        [InlineData("C3x3.5", 140, 0.0467)]
        [InlineData("C3x3.5", 160, 0.0533)]
        [InlineData("C3x3.5", 180, 0.0667)]
        [InlineData("C3x4.1", 120, 0.04)]
        [InlineData("C4x4.5", 120, 0.0444)]
        [InlineData("C4x5.4", 120, 0.0444)]
        [InlineData("C5x6.1", 120, 0.0545)]
        [InlineData("C5x6.7", 120, 0.0545)]
        [InlineData("C6x8.2", 120, 0.0667)]
        [InlineData("C7x9.8", 120, 0.1412)]
        [InlineData("C8x11.5", 120, 0.1714)]
        [InlineData("C10x15.3", 120, 0.2182)]
        public void CanGetShelfLoadBeamWeldLabor(string channelSize, float width, float expHrs)
        {
            var part = _fixture.CreateRootPart<ShelfLoadBeam>();
            //part.Bom.LaborHourRounding.SetValue(0.01f);
            part.ChannelSize.SetValue(channelSize);
            part.Width.SetValue(width);

            var labor = part.Bom.LaborItems.Value;
            //Assert.Equal(expCost, labor.Sum(i => i.Cost), 2);
            Assert.Equal(expHrs, labor.First(l => l.OperationName.ToLower().Contains("weld time")).Hours, 4);
            //Assert.Equal(expHrs, labor.Sum(i => i.Hours), 2);
        }

        [Theory]
        [InlineData("C3x3.5", "8", 48, 0.0108)]
        [InlineData("C3x3.5", "8", 100, 0.0125)]
        [InlineData("C3x3.5", "8", 120, 0.025)]
        [InlineData("C3x3.5", "12", 120, 0.05)]
        [InlineData("C3x4.1", "8", 80, 0.0125)]
        [InlineData("C3x4.1", "12", 80, 0.025)]
        [InlineData("C4x4.5", "8", 80, 0.0125)]
        [InlineData("C4x4.5", "12", 80, 0.025)]
        [InlineData("C4x5.4", "8", 80, 0.0125)]
        [InlineData("C4x5.4", "12", 80, 0.025)]
        [InlineData("C5x6.1", "8", 80, 0.025)]
        [InlineData("C5x6.1", "12", 80, 0.05)]
        [InlineData("C5x6.7", "8", 80, 0.025)]
        [InlineData("C5x6.7", "12", 80, 0.05)]
        [InlineData("C6x8.2", "8", 80, 0.0313)]
        [InlineData("C6x8.2", "12", 80, 0.0625)]
        [InlineData("C7x9.8", "8", 80, 0.0313)]
        [InlineData("C7x9.8", "12", 80, 0.0625)]
        [InlineData("C8x11.5", "8", 80, 0.0417)]
        [InlineData("C8x11.5", "12", 80, 0.125)]
        [InlineData("C10x15.3", "8", 80, 0.0938)]
        [InlineData("C10x15.3", "12", 80, 0.1875)]
        public void CanGetShelfLoadBeamUnloadLabor(string channelSize, string bracketType, float width, float expHrs)
        {
            var part = _fixture.CreateRootPart<ShelfLoadBeam>();
            //part.Bom.LaborHourRounding.SetValue(0.01f);
            part.ChannelSize.SetValue(channelSize);
            part.BracketType.SetValue(bracketType);
            part.Width.SetValue(width);

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(expHrs, labor.First(l => l.OperationName.ToLower().Contains("unload")).Hours, 4);
        }

        [Fact]
        public void CanGetShelfLoadBeamChildrenLabor()
        {
            var part = _fixture.CreateRootPart<ShelfLoadBeam>();
            //part.Bom.LaborHourRounding.SetValue(0.01f);
            part.ChannelSize.SetValue("C3x3.5");
            part.BracketType.SetValue("8");
            part.Width.SetValue(80);

            var labor = part.Bom.LaborItems.Value;
            var childLabor = part.Bom.BomChildren.Value.SelectMany(c => c.LaborItems.Value);
            var childLaborHours = childLabor.Sum(b => b.Hours);
            Assert.True(childLaborHours > 0);
        }

        [Fact]
        public void CanGetChildSetupLaborBasedOnQty()
        {
            var part = _fixture.CreateRootPart<ShelfLoadBeam>();
            //part.Bom.LaborHourRounding.SetValue(0.01f);
            part.ChannelSize.SetValue("C3x3.5");
            part.BracketType.SetValue("8");
            part.Width.SetValue(80);

            var bomSummarizer = new StorageBomSummarizer();
            var bomSummary = bomSummarizer.GetBomSummary(part.Bom, 10);

            var lBeam = bomSummary.Children.FirstOrDefault(c => c.Path.Contains("Beam"));
            var beamSetup = lBeam.Labor.FirstOrDefault(li => li.Name.Contains("(Setup)"));

            Assert.Equal(0.05, beamSetup.Hours, 2);
        }

        [Fact]
        public void CanGetShelfLoadBeamBomSummary()
        {
            var part = _fixture.CreateRootPart<ShippableParts>();
            var shelfLoadBeam = part.AddChild(new Ruler.Rules.ChildOptions<ShelfLoadBeam>());
            //part.Bom.LaborHourRounding.SetValue(0.01f);
            shelfLoadBeam.ChannelSize.SetValue("C3x3.5");
            shelfLoadBeam.BracketType.SetValue("8");
            shelfLoadBeam.Width.SetValue(80);

            var bomSummarizer = new StorageBomSummarizer();
            var bomSummary = bomSummarizer.GetBomSummary(part.Bom);

            Assert.True(bomSummary.TotalLaborHours > 0);
        }

        #endregion

        #region Shelf Beam Labor Tests

        [Theory]
        [InlineData("C3x3.5", 48, 0.0159)]
        [InlineData("C3x3.5", 80, 0.0212)]
        [InlineData("C3x3.5", 120, 0.0307)]
        [InlineData("C3x3.5", 160, 0.0615)]
        [InlineData("C3x4.1", 120, 0.0307)]
        [InlineData("C4x4.5", 120, 0.0307)]
        [InlineData("C4x5.4", 120, 0.0307)]
        [InlineData("C5x6.1", 120, 0.0357)]
        [InlineData("C5x6.7", 120, 0.0357)]
        [InlineData("C6x8.2", 120, 0.0406)]
        [InlineData("C7x9.8", 120, 0)]
        [InlineData("C8x11.5", 120, 0)]
        [InlineData("C10x15.3", 120, 0)]
        public void CanGetShelfBeamLBeamLaborHours(string channelSize, float length, float expHrs)
        {
            var part = _fixture.CreateRootPart<ShelfBeam>();
            //part.Bom.LaborHourRounding.SetValue(0.01f);
            part.ChannelSize.SetValue(channelSize);
            part.Length.SetValue(length);

            var labor = part.Bom.LaborItems.Value;
            var lBeamLaborItem = labor.FirstOrDefault(l => l.OperationName.ToLower().Contains("lbeam"));

            if (expHrs == 0)
            {
                Assert.True(lBeamLaborItem == null);
            }
            else
            {
                Assert.Equal(expHrs, lBeamLaborItem.Hours, 4);
            }
            //Assert.Equal(expHrs, labor.Sum(i => i.Hours), 2);
        }

        [Theory]
        [InlineData("C3x3.5", 120, 0)]
        [InlineData("C3x4.1", 120, 0)]
        [InlineData("C4x4.5", 120, 0)]
        [InlineData("C4x5.4", 120, 0)]
        [InlineData("C5x6.1", 120, 0)]
        [InlineData("C5x6.7", 120, 0)]
        [InlineData("C6x8.2", 120, 0)]
        [InlineData("C7x9.8", 48, 0.0242)]
        [InlineData("C7x9.8", 80, 0.0322)]
        [InlineData("C7x9.8", 120, 0.0467)]
        [InlineData("C7x9.8", 160, 0.0934)]
        [InlineData("C8x11.5", 120, 0.0467)]
        [InlineData("C10x15.3", 120, 0.6148)]
        public void CanGetShelfBeamAccupressLaborHours(string channelSize, float length, float expHrs)
        {
            var part = _fixture.CreateRootPart<ShelfBeam>();
            part.ChannelSize.SetValue(channelSize);
            part.Length.SetValue(length);

            var labor = part.Bom.LaborItems.Value;
            var lBeamLaborItem = labor.FirstOrDefault(l => l.OperationName.ToLower().Contains("accupress"));

            if (expHrs == 0)
            {
                Assert.True(lBeamLaborItem == null);
            }
            else
            {
                Assert.Equal(expHrs, lBeamLaborItem.Hours, 4);
            }
        }

        [Theory]
        [InlineData("C3x3.5", 48, 14, 5.1968)]
        [InlineData("C3x3.5", 80, 23.3333, 8.6613)]
        [InlineData("C3x3.5", 120, 35, 12.992)]
        [InlineData("C3x3.5", 160, 46.6667, 17.3226)]
        [InlineData("C3x4.1", 120, 41, 15.2192)]//Cost updated from 30.2334 after csi integration
        [InlineData("C4x4.5", 120, 45, 16.704)]
        [InlineData("C4x5.4", 120, 54, 20.0448)]
        [InlineData("C5x6.1", 120, 61, 23.2532)]
        [InlineData("C5x6.7", 120, 67, 25.5404)]
        [InlineData("C6x8.2", 120, 82, 31.2584)] //Cost updated from 30.2334 after csi integration
        [InlineData("C7x9.8", 120, 98, 40.0525)]//Cost updated from 30.2334 after csi integration
        [InlineData("C8x11.5", 120, 115, 41.2504)]//Cost updated from 30.2334 after csi integration
        public void CanGetShelfBeamCostAndWeight(string channelSize, float length, double expWt, double expCost)
        {
            var part = _fixture.CreateRootPart<ShelfBeam>();
            part.ChannelSize.SetValue(channelSize);
            part.Length.SetValue(length);

            Assert.Equal(expCost, part.Bom.ItemMaterialCost.Value, 4);
            Assert.Equal(expWt, part.Bom.ItemWeight.Value, 4);
        }

        #endregion

        #region Slant Leg Column Tube Tests

        [Theory]
        [InlineData(3, "S.T.AAX.3A3A")]
        [InlineData(4, "S.T.AAX.4A3A")]
        public void CanGetSlantLegColumnTubeMaterialNumber(float depth, string expPNum)
        {
            var part = _fixture.CreateRootPart<SlantLegColumnTube>();
            part.Depth.SetValue(depth);

            Assert.Equal(expPNum, part.Bom.MaterialPartNumber.Value);
        }

        [Fact]
        public void CanGetSlantLegColumnTubeMaterialPrice()
        {
            var part1 = _fixture.CreateRootPart<SlantLegColumnTube>();
            part1.Depth.SetValue(4);
            part1.Length.SetValue(36);
            var part2 = _fixture.CreateRootPart<SlantLegColumnTube>();
            part2.Depth.SetValue(4);
            part2.Length.SetValue(48);

            Assert.True(part1.Bom.ItemMaterialPrice.Value > 0);
            Assert.True(part2.Bom.ItemMaterialPrice.Value > 0);
            Assert.True(part2.Bom.ItemMaterialPrice.Value > part1.Bom.ItemMaterialPrice.Value);
        }

        [Fact]
        public void CanGetSlantLegColumnTubeSawLabor()
        {
            var part = _fixture.CreateRootPart<SlantLegColumnTube>();

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(.0222, labor.First(l => l.OperationName.ToLower().Contains("saw")).Hours, 4);
        }

        [Fact]
        public void CanGetSlantLegColumnTubeSawSetup()
        {
            var part = _fixture.CreateRootPart<SlantLegColumnTube>();

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(.5, labor.First(l => l.OperationName.ToLower().Contains("saw")).SetupHours, 4);
        }

        #endregion

        #region Shelf Load Beam Bracket Tests

        [Theory]
        [InlineData("8", 0.836, 1.26)]
        [InlineData("8infAdj", 1.271, 1.91)]
        [InlineData("9", 1.049, 1.26)]
        [InlineData("12", 1.459, 2.21)]
        [InlineData("13", 1.929, 2.21)]
        public void CanGetShelfBeamEndBracketCostAndWeight(string type, double expCost, double expWt)
        {
            var beamEndBracket = _fixture.CreateRootPart<ShelfBeamEndBracket>();

            beamEndBracket.Type.SetValue(type);

            Assert.Equal(expCost, beamEndBracket.Bom.ItemCost.Value);
            Assert.Equal(expWt, beamEndBracket.Bom.ItemWeight.Value);
        }

        #endregion

        #region XB Crossbar Tests

        [Theory]
        [InlineData(@"1-1/2 x 1-1/2 x 1/8", 20, 0.0017)]
        [InlineData(@"2 x 2 x 1/8", 18, 0.0017)]
        public void CanGetXBCrossbarPaintLabor(string angleSize, float length, float expHrs)
        {
            var part = _fixture.CreateRootPart<XBCrossbar>();
            part.AngleType.SetValue(angleSize);
            part.OverallLength.SetValue(length);
            
            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(expHrs, labor.First(l => l.OperationName.ToLower().Contains("paint")).Hours, 4);
        }

        [Theory]
        [InlineData(@"1-1/2 x 1-1/2 x 1/8", 20, 0.005)]
        [InlineData(@"2 x 2 x 1/8", 18, 0.005)]
        public void CanGetXBCrossbarUnloadLabor(string angleSize, float length, float expHrs)
        {
            var part = _fixture.CreateRootPart<XBCrossbar>();
            part.AngleType.SetValue(angleSize);
            part.OverallLength.SetValue(length);

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(expHrs, labor.First(l => l.OperationName.ToLower().Contains("unload")).Hours, 4);
        }

        [Theory]
        [InlineData(@"1-1/2 x 1-1/2 x 1/8", 8.9375, 1.23, 1.2521)]
        [InlineData(@"1-1/2 x 1-1/2 x 1/8", 20.9375, 2.46, 2.5041)]
        [InlineData(@"1-1/2 x 1-1/2 x 1/8", 46.9375, 5.125, 5.2169)]
        [InlineData(@"2 x 2 x 1/8", 8.9375, 1.65, 1.6796)]
        [InlineData(@"2 x 2 x 1/8", 20.9375, 3.3, 3.3592)]
        [InlineData(@"2 x 2 x 1/8", 46.9375, 6.875, 6.9983)]
        public void CanGetXBCrossbarWeight(string angleSize, float length, float expMatWeight, float expWeightWPaint)
        {
            var part = _fixture.CreateRootPart<XBCrossbar>();
            part.AngleType.SetValue(angleSize);
            part.OverallLength.SetValue(length);

            Assert.Equal(expMatWeight, part.XBCrossbarAngle.Bom.ItemMaterialWeight.Value, 4);
            Assert.Equal(expWeightWPaint, part.Bom.UnitWeight.Value, 4);
        }

        [Theory]
        [InlineData(@"1-1/2 x 1-1/2 x 1/8", 41.3125, 4.51, 4.5909)]
        public void CanGetShelfBeamCrossbarWeight(string angleSize, float length, float expMatWeight, float expWeight)
        {
            var part = _fixture.CreateRootPart<ShelfBeamCrossbar>();
            part.AngleType.SetValue(angleSize);
            part.RackDepth.SetValue(length);

            Assert.Equal(expMatWeight, part.XBCrossbar.XBCrossbarAngle.Bom.ItemMaterialWeight.Value, 4);
            Assert.Equal(expWeight, part.XBCrossbar.Bom.UnitWeight.Value, 4);
        }

        [Fact]
        public void CanGetXBCrossbarBomSummary()
        {
            var part = _fixture.CreateRootPart<ShippableParts>();
            var beamCrossbar = part.AddChild(new Ruler.Rules.ChildOptions<ShelfBeamCrossbar>());
            beamCrossbar.AngleType.SetValue(@"1-1/2 x 1-1/2 x 1/8");
            beamCrossbar.RackDepth.SetValue(41.3125f);
            beamCrossbar.Qty.SetValue(76);

            var bomSummarizer = new StorageBomSummarizer();
            var bomSummary = bomSummarizer.GetBomSummary(part.Bom);

            Assert.Equal(2.8567, bomSummary.TotalLaborHours, 4);
        }

        #endregion

        #region Crossbar End Angle Tests

        [Fact]
        public void CanGetCrossbarEndAngleCNCLabor()
        {
            var part = _fixture.CreateRootPart<CrossbarEndAngle>();

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(.00588, labor.First(l => l.OperationName.ToLower().Contains("cnc")).Hours, 4);
        }

        [Fact]
        public void CanGetCrossbarEndAngleCNCSetup()
        {
            var part = _fixture.CreateRootPart<CrossbarEndAngle>();

            Assert.Equal(0.5, part.Bom.ItemSetupHours.Value, 4);
        }

        #endregion

        #region Crossbar End Plate Tests

        [Fact]
        public void CanGetCrossbarEndPlateFederalLabor()
        {
            var part = _fixture.CreateRootPart<CrossbarEndPlate>();

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(.002857, labor.First(l => l.OperationName.ToLower().Contains("federal")).Hours, 4);
        }

        [Fact]
        public void CanGetCrossbarEndPlateFederalSetup()
        {
            var part = _fixture.CreateRootPart<CrossbarEndPlate>();

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(0.25, labor.First(l => l.OperationName.ToLower().Contains("federal")).SetupHours, 4);
        }
        
        [Fact]
        public void CanGetCrossbarEndPlateAccurpressLabor()
        {
            var part = _fixture.CreateRootPart<CrossbarEndPlate>();

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(.003508, labor.First(l => l.OperationName.ToLower().Contains("accurpress")).Hours, 4);
        }

        [Fact]
        public void CanGetCrossbarEndPlateAccurpressSetup()
        {
            var part = _fixture.CreateRootPart<CrossbarEndPlate>();

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(1.0, labor.First(l => l.OperationName.ToLower().Contains("accurpress")).SetupHours, 4);
        }

        #endregion

        #region Crossbar Angle Tests

        [Fact]
        public void CanGetCrossbarAngleSawLabor()
        {
            var part = _fixture.CreateRootPart<CrossbarAngle>();

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(.001374, labor.First(l => l.OperationName.ToLower().Contains("saw")).Hours, 4);
        }

        [Theory]
        [InlineData("XBW-DD1", .005556)]
        [InlineData("XBW-DD2", .011112)]
        public void CanGetCrossbarAngleIronworkerLabor(string crossbarType, double expHrs)
        {
            var part = _fixture.CreateRootPart<CrossbarAngle>();
            part.CrossbarType.SetValue(crossbarType);

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(expHrs, labor.First(l => l.OperationName.ToLower().Contains("ironworker")).Hours, 4);
        }

        [Theory]
        [InlineData("XBW-DD1", .25)]
        [InlineData("XBW-DD2", .5)]
        public void CanGetCrossbarAngleIronworkerSetup(string crossbarType, double expHrs)
        {
            var part = _fixture.CreateRootPart<CrossbarAngle>();
            part.CrossbarType.SetValue(crossbarType);

            Assert.Equal(expHrs, part.Bom.ItemSetupHours.Value, 4);
        }

        #endregion

        #region WeldedCrossbar Notch Plate Tests

        [Fact]
        public void CanGetFlatPlateIronworkerLabor()
        {
            var part = _fixture.CreateRootPart<WeldedCrossbarNotchPlate>();

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(.002777, labor.First(l => l.OperationName.ToLower().Contains("ironworker")).Hours, 4);
        }

        [Fact]
        public void CanGetFlatPlateIronworkerSetup()
        {
            var part = _fixture.CreateRootPart<WeldedCrossbarNotchPlate>();

            Assert.Equal(0.25, part.Bom.ItemSetupHours.Value, 4);
        }

        #endregion

        #region Frame Upright Tests

        [Theory]
        [InlineData("S", 200, 44, 0.0446)]
        [InlineData("S", 408, 48, 0.0446)]
        [InlineData("H", 200, 44, 0.0446)]
        [InlineData("H", 408, 48, 0.0446)]
        [InlineData("BH", 200, 44, 0.0446)]
        [InlineData("BH", 408, 48, 0.0446)]
        [InlineData("BTH", 200, 44, 0.0446)]
        [InlineData("BTH", 408, 48, 0.0446)]
        [InlineData("D", 200, 44, 0.0446)]
        [InlineData("D", 408, 48, 0.0446)]
        [InlineData("T", 200, 44, 0.0446)]
        [InlineData("T", 408, 48, 0.0446)]
        [InlineData("LS", 200, 44, 0.0446)]
        [InlineData("LS", 408, 48, 0.0446)]
        [InlineData("L", 200, 44, 0.0446)]
        [InlineData("L", 408, 48, 0.0446)]
        [InlineData("B", 200, 44, 0.0446)]
        [InlineData("B", 408, 48, 0.0446)]
        public void CanGetFrameUprightPaintLabor(string type, float height, float depth, float expHrs)
        {
            var part = _fixture.CreateRootPart<FrameUpright>();
            part.UprightType.SetValue(type);
            part.Height.SetValue(height);
            part.Depth.SetValue(depth);

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(expHrs, labor.First(l => l.OperationName.ToLower().Contains("paint")).Hours, 4);
        }

        [Theory]
        [InlineData("S", 200, 44, 0.1563)]
        [InlineData("S", 408, 48, 0.1563)]
        [InlineData("H", 200, 44, 0.1563)]
        [InlineData("H", 408, 48, 0.1563)]
        [InlineData("BH", 200, 44, 0.1563)]
        [InlineData("BH", 408, 48, 0.1563)]
        [InlineData("BTH", 200, 44, 0.1563)]
        [InlineData("BTH", 408, 48, 0.1563)]
        [InlineData("D", 200, 44, 0.1563)]
        [InlineData("D", 408, 48, 0.1563)]
        [InlineData("T", 200, 44, 0.1563)]
        [InlineData("T", 408, 48, 0.1563)]
        [InlineData("LS", 200, 44, 0.1563)]
        [InlineData("LS", 408, 48, 0.1563)]
        [InlineData("L", 200, 44, 0.1563)]
        [InlineData("L", 408, 48, 0.1563)]
        [InlineData("B", 200, 44, 0.1563)]
        [InlineData("B", 408, 48, 0.1563)]
        public void CanGetFrameUprightUnloadLabor(string type, float height, float depth, float expHrs)
        {
            var part = _fixture.CreateRootPart<FrameUpright>();
            part.UprightType.SetValue(type);
            part.Height.SetValue(height);
            part.Depth.SetValue(depth);

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(expHrs, labor.First(l => l.OperationName.ToLower().Contains("unload")).Hours, 4);
        }

        [Theory]
        [InlineData("C3x3.5", 2, false, 224, false, 224, "none", 6, "none", 6, "S", 0, 0, 0.12048)]
        [InlineData("C3x3.5", 2, true, 224, true, 224, "angle", 6, "angle", 6, "S", 0, 0, 0.529188)]
        [InlineData("C3x3.5", 2, true, 224, true, 224, "bullnose", 5.75, "bullnose", 5.75, "S", 0, 0, 0.5381)]
        [InlineData("C3x3.5", 2, false, 224, false, 224, "angle", 6, "angle", 6, "H", 1, 1, 0.258088)]
        [InlineData("C3x3.5", 2, true, 224, true, 224, "angle", 6, "angle", 6, "H", 0, 0, 0.563408)]
        [InlineData("C3x3.5", 2, false, 224, false, 224, "angle", 6, "angle", 6, "BTH", 2, 2, 0.332628)]
        [InlineData("C3x3.5", 2, false, 224, false, 224, "angle", 6, "angle", 6, "BH", 0, 0, 0.205268)]
        public void CanGetFrameUprightWeldLabor(string colSize, int panelQty, bool hasFrontDoubler, float frontDoublerHeight, bool hasRearDoubler, float rearDoublerHeight,
                        string frontPostProtectorType, float frontPostProtectorHeight, string rearPostProtectorType, float rearPostProtectorHeight, string uprightType, int frontWeldPlateQty, int rearWeldPlateQty, float expHrs)
        {
            var part = _fixture.CreateRootPart<FrameUpright>();
            part.UprightType.SetValue(uprightType);
            part.UprightColumnSize.SetValue(colSize);
            part.PanelQty.SetValue(panelQty);
            part.Height.SetValue(360);
            part.HasFrontDoubler.SetValue(hasFrontDoubler);
            part.FrontDoublerHeight.SetValue(frontDoublerHeight);
            part.HasRearDoubler.SetValue(hasRearDoubler);
            part.RearDoublerHeight.SetValue(rearDoublerHeight);
            part.FrontPostProtector.SetValue(frontPostProtectorType);
            part.FrontPostProtectorHeight.SetValue(frontPostProtectorHeight);
            part.RearPostProtector.SetValue(rearPostProtectorType);
            part.RearPostProtectorHeight.SetValue(rearPostProtectorHeight);
            part.HeavyHorizontalFrontWeldPlateQty.SetValue(frontWeldPlateQty);
            part.HeavyHorizontalRearWeldPlateQty.SetValue(rearWeldPlateQty);

            var labor = part.Bom.LaborItems.Value;

            Assert.Equal(expHrs, labor.First(l => l.OperationName.ToLower().Contains("weld")).Hours, 4);
        }

        [Theory]
        [InlineData("C3x3.5", 1, 0.08130)]
        [InlineData("C3x3.5", 2, 0.12048)]
        [InlineData("C3x3.5", 3, 0.15873)]
        [InlineData("C3x3.5", 4, 0.20408)]
        [InlineData("C3x3.5", 5, 0.28571)]
        [InlineData("C3x3.5", 6, 0.34483)]
        [InlineData("C3x3.5", 7, 0.43478)]
        [InlineData("C3x3.5", 8, 0.52632)]
        [InlineData("C3x3.5", 9, 0.66667)]
        [InlineData("C3x3.5", 10, 0.90909)]
        [InlineData("C3x3.5", 11, 1.23967)]
        [InlineData("C3x3.5", 12, 1.69046)]
        [InlineData("C3x4.1", 1, 0.08130)]
        [InlineData("C3x4.1", 2, 0.12048)]
        [InlineData("C3x4.1", 3, 0.15873)]
        [InlineData("C3x4.1", 4, 0.20408)]
        [InlineData("C3x4.1", 5, 0.28571)]
        [InlineData("C3x4.1", 6, 0.34483)]
        [InlineData("C3x4.1", 7, 0.43478)]
        [InlineData("C3x4.1", 8, 0.52632)]
        [InlineData("C3x4.1", 9, 0.66667)]
        [InlineData("C3x4.1", 10, 0.90909)]
        [InlineData("C3x4.1", 11, 1.23967)]
        [InlineData("C3x4.1", 12, 1.69046)]
        [InlineData("C4x4.5", 1, 0.09259)]
        [InlineData("C4x4.5", 2, 0.13514)]
        [InlineData("C4x4.5", 3, 0.17857)]
        [InlineData("C4x4.5", 4, 0.22727)]
        [InlineData("C4x4.5", 5, 0.32258)]
        [InlineData("C4x4.5", 6, 0.37037)]
        [InlineData("C4x4.5", 7, 0.47619)]
        [InlineData("C4x4.5", 8, 0.58824)]
        [InlineData("C4x4.5", 9, 0.71429)]
        [InlineData("C4x4.5", 10, 1.0)]
        [InlineData("C4x4.5", 11, 1.4)]
        [InlineData("C4x4.5", 12, 1.96)]
        [InlineData("C4x5.4", 1, 0.09259)]
        [InlineData("C4x5.4", 2, 0.13514)]
        [InlineData("C4x5.4", 3, 0.17857)]
        [InlineData("C4x5.4", 4, 0.22727)]
        [InlineData("C4x5.4", 5, 0.32258)]
        [InlineData("C4x5.4", 6, 0.37037)]
        [InlineData("C4x5.4", 7, 0.47619)]
        [InlineData("C4x5.4", 8, 0.58824)]
        [InlineData("C4x5.4", 9, 0.71429)]
        [InlineData("C4x5.4", 10, 1.0)]
        [InlineData("C4x5.4", 11, 1.4)]
        [InlineData("C4x5.4", 12, 1.96)]
        [InlineData("C5x6.1", 1, 0.2)]
        [InlineData("C5x6.1", 2, 0.24390)]
        [InlineData("C5x6.1", 3, 0.28571)]
        [InlineData("C5x6.1", 4, 0.33333)]
        [InlineData("C5x6.1", 5, 0.37037)]
        [InlineData("C5x6.1", 6, 0.43478)]
        [InlineData("C5x6.1", 7, 0.5)]
        [InlineData("C5x6.1", 8, 0.625)]
        [InlineData("C5x6.1", 9, 0.76923)]
        [InlineData("C5x6.1", 10, 1.0)]
        [InlineData("C5x6.1", 11, 1.3)]
        [InlineData("C5x6.1", 12, 1.69)]
        [InlineData("C5x6.7", 1, 0.2)]
        [InlineData("C5x6.7", 2, 0.24390)]
        [InlineData("C5x6.7", 3, 0.28571)]
        [InlineData("C5x6.7", 4, 0.33333)]
        [InlineData("C5x6.7", 5, 0.37037)]
        [InlineData("C5x6.7", 6, 0.43478)]
        [InlineData("C5x6.7", 7, 0.5)]
        [InlineData("C5x6.7", 8, 0.625)]
        [InlineData("C5x6.7", 9, 0.76923)]
        [InlineData("C5x6.7", 10, 1.0)]
        [InlineData("C5x6.7", 11, 1.3)]
        [InlineData("C5x6.7", 12, 1.69)]
        public void CanGetFrameUprightWeldBaseLabor(string colSize, int panelQty, float expHrs)
        {
            var part = _fixture.CreateRootPart<FrameUpright>();
            part.UprightColumnSize.SetValue(colSize);
            part.PanelQty.SetValue(panelQty);

            //var labor = part.Bom.LaborItems.Value;

            //Assert.Equal(expHrs, labor.First(l => l.OperationName.ToLower().Contains("weld")).Hours, 4);
            Assert.Equal(expHrs, part.WeldBaseLabor.Value, 4);
        }

        [Theory]
        [InlineData(false, 84, 84, 0)]
        [InlineData(true, 100, 100, 0.06216f)]
        [InlineData(true, 112, 116, 0.06888f)]
        [InlineData(true, 224, 348, 0.16632f)]
        [InlineData(true, 332, 680, 0.30576f)]
        public void CanGetFrameUprightFrontDoublerWeldLaborAddon(bool hasDoubler, float doubleHeight, float expEDH, float expHrs)
        {
            var part = _fixture.CreateRootPart<FrameUpright>();
            part.Height.SetValue(360);
            part.HasFrontDoubler.SetValue(hasDoubler);
            part.FrontDoublerHeight.SetValue(doubleHeight);

            Assert.Equal(expEDH, part.FrontEDH.Value);
            Assert.Equal(expHrs, part.FrontDoublerWeldAddon.Value, 4);
        }

        [Theory]
        [InlineData(false, 84, 84, 0)]
        [InlineData(true, 100, 100, 0.06216f)]
        [InlineData(true, 112, 116, 0.06888f)]
        [InlineData(true, 224, 348, 0.16632f)]
        [InlineData(true, 332, 680, 0.30576f)]
        public void CanGetFrameUprightRearDoublerWeldLaborAddon(bool hasDoubler, float doubleHeight, float expEDH, float expHrs)
        {
            var part = _fixture.CreateRootPart<FrameUpright>();
            part.Height.SetValue(360);
            part.HasRearDoubler.SetValue(hasDoubler);
            part.RearDoublerHeight.SetValue(doubleHeight);

            Assert.Equal(expEDH, part.RearEDH.Value);
            Assert.Equal(expHrs, part.RearDoublerWeldAddon.Value, 4);
        }

        [Theory]
        [InlineData(false, 0)]
        [InlineData(true, 0.0065)]
        public void CanGetFrameUprightFrontDoublerCapWeldLaborAddon(bool hasDoubler, float expHrs)
        {
            var part = _fixture.CreateRootPart<FrameUpright>();
            part.HasFrontDoubler.SetValue(hasDoubler);

            Assert.Equal(expHrs, part.FrontDoublerCapWeldAddon.Value, 4);
        }

        [Theory]
        [InlineData(false, 0)]
        [InlineData(true, 0.0065)]
        public void CanGetFrameUprightRearDoublerCapWeldLaborAddon(bool hasDoubler, float expHrs)
        {
            var part = _fixture.CreateRootPart<FrameUpright>();
            part.HasRearDoubler.SetValue(hasDoubler);

            Assert.Equal(expHrs, part.RearDoublerCapWeldAddon.Value, 4);
        }

        [Theory]
        [InlineData("none", 0, 0)]
        [InlineData("angle", 6, 0.031534)]
        [InlineData("angle", 14, 0.139054)]
        public void CanGetFrameUprightFrontAngleProtectorWeldLaborAddon(string type, float angleHeight, float expHrs)
        {
            var part = _fixture.CreateRootPart<FrameUpright>();
            part.FrontPostProtector.SetValue(type);
            part.FrontPostProtectorHeight.SetValue(angleHeight);

            Assert.Equal(expHrs, part.FrontAngleProtectorWeldAddon.Value, 4);
        }

        [Theory]
        [InlineData("none", 0, 0)]
        [InlineData("angle", 6, 0.031534)]
        [InlineData("angle", 14, 0.139054)]
        public void CanGetFrameUprightRearAngleProtectorWeldLaborAddon(string type, float angleHeight, float expHrs)
        {
            var part = _fixture.CreateRootPart<FrameUpright>();
            part.RearPostProtector.SetValue(type);
            part.RearPostProtectorHeight.SetValue(angleHeight);

            Assert.Equal(expHrs, part.RearAngleProtectorWeldAddon.Value, 4);
        }

        [Theory]
        [InlineData("none", 0, 0)]
        [InlineData("bullnose", 2.75, 0.03179)]
        [InlineData("bullnose", 3.75, 0.03179)]
        [InlineData("bullnose", 4.75, 0.03599)]
        [InlineData("bullnose", 5.75, 0.03599)]
        [InlineData("bullnose", 7.75, 0.03809)]
        public void CanGetFrameUprightFrontBullnoseWeldLaborAddon(string type, float height, float expHrs)
        {
            var part = _fixture.CreateRootPart<FrameUpright>();
            part.FrontPostProtector.SetValue(type);
            part.FrontPostProtectorHeight.SetValue(height);

            Assert.Equal(expHrs, part.FrontBullnoseWeldAddon.Value, 4);
        }

        [Theory]
        [InlineData("none", 0, 0)]
        [InlineData("bullnose", 2.75, 0.03179)]
        [InlineData("bullnose", 3.75, 0.03179)]
        [InlineData("bullnose", 4.75, 0.03599)]
        [InlineData("bullnose", 5.75, 0.03599)]
        [InlineData("bullnose", 7.75, 0.03809)]
        public void CanGetFrameUprightRearBullnoseWeldLaborAddon(string type, float height, float expHrs)
        {
            var part = _fixture.CreateRootPart<FrameUpright>();
            part.RearPostProtector.SetValue(type);
            part.RearPostProtectorHeight.SetValue(height);

            Assert.Equal(expHrs, part.RearBullnoseWeldAddon.Value, 4);
        }

        [Theory]
        [InlineData("S", 0, 0, 0)]
        [InlineData("H", 0, 0, 0.03422)]
        [InlineData("H", 1, 0, 0.05438)]
        [InlineData("H", 1, 1, 0.07454)]
        [InlineData("BTH", 2, 2, 0.14908)]
        public void CanGetFrameUprightHeavyHorizontalWeldLaborAddon(string uprightType, int frontPlateQty, int rearPlateQty, float expHrs)
        {
            var part = _fixture.CreateRootPart<FrameUpright>();
            part.UprightType.SetValue(uprightType);
            part.HeavyHorizontalFrontWeldPlateQty.SetValue(frontPlateQty);
            part.HeavyHorizontalRearWeldPlateQty.SetValue(rearPlateQty);

            Assert.Equal(expHrs, part.HeavyHorizontalWeldAddon.Value, 4);
        }

        [Theory]
        [InlineData("S", 0)]
        [InlineData("BH", 0.02172)]
        public void CanGetFrameUprightBoltOnHeavyHorizontalWeldLaborAddon(string uprightType, float expHrs)
        {
            var part = _fixture.CreateRootPart<FrameUpright>();
            part.UprightType.SetValue(uprightType);

            Assert.Equal(expHrs, part.BoltOnHeavyHorizontalAngleMountsWeld.Value, 4);
        }

        [Theory]
        [InlineData("S", 0)]
        [InlineData("L", .12)]
        [InlineData("LS", .12)]
        public void CanGetFrameUprightSlantLegWeldLaborAddon(string uprightType, float expHrs)
        {
            var part = _fixture.CreateRootPart<FrameUpright>();
            part.UprightType.SetValue(uprightType);

            Assert.Equal(expHrs, part.SlantLegWeldAddon.Value, 4);
        }

        [Theory]
        [InlineData("S", 0)]
        [InlineData("L", 1)]
        [InlineData("LS", 1)]
        public void CanGetFrameUprightSlantLegWeldSetupAddon(string uprightType, float expHrs)
        {
            var part = _fixture.CreateRootPart<FrameUpright>();
            part.UprightType.SetValue(uprightType);

            Assert.Equal(expHrs, part.SlantLegWeldSetup.Value, 4);
        }

        [Theory]
        [InlineData("S", 0)]
        [InlineData("B", .3452)]
        public void CanGetFrameUprightBentLegWeldLaborAddon(string uprightType, float expHrs)
        {
            var part = _fixture.CreateRootPart<FrameUpright>();
            part.UprightType.SetValue(uprightType);

            Assert.Equal(expHrs, part.BentLegWeldAddon.Value, 4);
        }

        [Theory]
        [InlineData("S", 0)]
        [InlineData("T", .116)]
        public void CanGetFrameUprightTippmanWeldLaborAddon(string uprightType, float expHrs)
        {
            var part = _fixture.CreateRootPart<FrameUpright>();
            part.UprightType.SetValue(uprightType);

            Assert.Equal(expHrs, part.TippmanWeldAddon.Value, 4);
        }

        [Theory]
        [InlineData("S", 1)]
        [InlineData("T", 1.25)]
        public void CanGetFrameUprightTippmanWeldMultiplierAddon(string uprightType, float expHrs)
        {
            var part = _fixture.CreateRootPart<FrameUpright>();
            part.UprightType.SetValue(uprightType);

            Assert.Equal(expHrs, part.TippmanWeldMultiplier.Value, 4);
        }

        [Theory]
        [InlineData("S", 1)]
        [InlineData("T", 3)]
        public void CanGetFrameUprightTippmanWeldSetupMultiplierAddon(string uprightType, float expHrs)
        {
            var part = _fixture.CreateRootPart<FrameUpright>();
            part.UprightType.SetValue(uprightType);

            Assert.Equal(expHrs, part.TippmanWeldSetupMultiplier.Value, 4);
        }

        #endregion

        #region Upright Column Tests

        [Fact]
        public void CanGetUprightColumnSawLabor()
        {
            var part = _fixture.CreateRootPart<UprightColumn>();
            part.BundleQty.SetValue(64);

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(0.00391, labor.First(l => l.OperationName.ToLower().Contains("saw")).Hours, 4);
        }

        [Theory]
        [InlineData(180, 4, 0.0214286)]
        [InlineData(180, 2, 0.0214286)]
        [InlineData(240, 4, 0.0285714)]
        [InlineData(240, 2, 0.0285714)]
        public void CanGetUprightColumnPunchLabor(float length, float holeSpacing, double expLaborHrs)
        {
            var part = _fixture.CreateRootPart<UprightColumn>();
            part.Length.SetValue(length);
            part.HoleSpacing.SetValue(holeSpacing);

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(expLaborHrs, labor.First(l => l.OperationName.ToLower().Contains("punch")).Hours, 4);
        }

        [Theory]
        [InlineData(2.0f, 3.5)]
        [InlineData(4.0f, 0.5)]
        public void CanGetUprightColumnPunchSetup(float holeSpacing, double setupHrs)
        {
            var part = _fixture.CreateRootPart<UprightColumn>();
            part.Length.SetValue(240);
            part.HoleSpacing.SetValue(holeSpacing);

            Assert.Equal(setupHrs, part.Bom.ItemSetupHours.Value, 4);
        }

        [Theory]
        [InlineData("C3x4.1", "S.C.34.1")]
        [InlineData("C5x6.7", "S.C.56.7")]
        [InlineData("C7x9.8", "S.C.79.8")]
        public void CanGetUprightColumnMaterialPartNumber(string size, string expPnum)
        {
            var part = _fixture.CreateRootPart<UprightColumn>();
            part.ChannelSize.SetValue(size);

            Assert.Equal(expPnum, part.MaterialPartNumber.Value);
        }

        [Theory]
        [InlineData("C3x4.1", 1.5219, 4.1)] //cost changed from 1.47067 after csi integration
        [InlineData("C5x6.7", 2.554, 6.7)]
        [InlineData("C7x9.8", 4.0053, 9.8)] //cost changed from 3.88276 after csi integration
        public void CanGetUprightColumnMaterialCostAndWeight(string size, double expCost, double expWeight)
        {
            var part1 = _fixture.CreateRootPart<UprightColumn>();
            part1.ChannelSize.SetValue(size);
            part1.Length.SetValue(12);

            Assert.Equal(expWeight, part1.Bom.ItemWeight.Value, 4);
            Assert.Equal(expCost, part1.Bom.ItemMaterialPrice.Value, 4);
        }

        [Fact]
        public void CanGetUprightColumnMaterialPrice()
        {
            var part1 = _fixture.CreateRootPart<UprightColumn>();
            part1.ChannelSize.SetValue("C3x4.1");
            part1.Length.SetValue(36);
            var part2 = _fixture.CreateRootPart<UprightColumn>();
            part2.ChannelSize.SetValue("C3x4.1");
            part2.Length.SetValue(48);

            Assert.True(part1.Bom.ItemMaterialPrice.Value > 0);
            Assert.True(part2.Bom.ItemMaterialPrice.Value > 0);
            Assert.True(part2.Bom.ItemMaterialPrice.Value > part1.Bom.ItemMaterialPrice.Value);
        }

        [Theory]
        [InlineData("C3x4.1", "S.C.34.1")]
        [InlineData("C5x6.7", "S.C.56.7")]
        [InlineData("C7x9.8", "S.C.79.8")]
        public void CanGetHeavyHorizontalColumnMaterialPartNumber(string size, string expPnum)
        {
            var part = _fixture.CreateRootPart<HeavyHorizontalBrace>();
            part.ChannelSize.SetValue(size);

            Assert.Equal(expPnum, part.MaterialPartNumber.Value);
        }

        [Fact]
        public void CanGetHeavyHorizontalMaterialPrice()
        {
            var part1 = _fixture.CreateRootPart<HeavyHorizontalBrace>();
            part1.ChannelSize.SetValue("C3x4.1");
            part1.Length.SetValue(36);
            var part2 = _fixture.CreateRootPart<HeavyHorizontalBrace>();
            part2.ChannelSize.SetValue("C3x4.1");
            part2.Length.SetValue(48);

            Assert.True(part1.Bom.ItemMaterialPrice.Value > 0);
            Assert.True(part2.Bom.ItemMaterialPrice.Value > 0);
            Assert.True(part2.Bom.ItemMaterialPrice.Value > part1.Bom.ItemMaterialPrice.Value);
        }

        #endregion

        #region Doubler Cap Tests

        [Fact]
        public void CanGetDoublerCapBurntableLabor()
        {
            var part = _fixture.CreateRootPart<DoublerCap>();

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(.0033, labor.First(l => l.OperationName.ToLower().Contains("burn")).Hours, 4);
        }

        [Fact]
        public void CanGetDoublerCapBurntableSetup()
        {
            var part = _fixture.CreateRootPart<DoublerCap>();

            Assert.Equal(0.5, part.Bom.ItemSetupHours.Value, 4);
        }

        [Theory]
        [InlineData("C3x4.1", "S.C.34.1")]
        [InlineData("C5x6.7", "S.C.56.7")]
        [InlineData("C7x9.8", "S.C.79.8")]
        public void CanGetDoublerMaterialPartNumber(string size, string expPnum)
        {
            var part = _fixture.CreateRootPart<HeavyHorizontalBrace>();
            part.ChannelSize.SetValue(size);

            Assert.Equal(expPnum, part.MaterialPartNumber.Value);
        }

        [Theory]
        [InlineData("C3x3.5", "GA", "S.S.14GA.A60120")]
        [InlineData("C4x5.4", "GA", "S.S.14GA.A60120")]
        [InlineData("C5x6.1", "GA", "S.S.14GA.A60120")]
        public void CanGetDoublerCapMaterialPartNumber(string columnSize, string location, string expPartNumber)
        {
            var part = _fixture.CreateRootPart<FrameUpright>();
            part.UprightColumnSize.SetValue(columnSize);
            part.HasFrontDoubler.SetValue(true);

            var doublerCap = part.FrontUprightColumnAssembly.DoublerCap;
            doublerCap.Location.SetValue(location);

            Assert.Equal(expPartNumber, doublerCap.MaterialPartNumber.Value);
        }

        #endregion

        #region Footplate Tests

        [Fact]
        public void CanGetFootplateBurntableLabor()
        {
            var part = _fixture.CreateRootPart<Footplate>();

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(.01, labor.First(l => l.OperationName.ToLower().Contains("burn")).Hours, 4);
        }

        [Fact]
        public void CanGetFootplateBurntableSetup()
        {
            var part = _fixture.CreateRootPart<Footplate>();

            Assert.Equal(0.5, part.Bom.ItemSetupHours.Value, 4);
        }

        [Theory]
        [InlineData(.375, "S.S.AA1.A72120")]
        [InlineData(.5, "S.S.AA6.A60120")]
        [InlineData(.625, "S.S.AA2.A48A96")]
        public void CanGetFootplateMaterialPartNumber(float thickness, string expPnum)
        {
            var part = _fixture.CreateRootPart<Footplate>();
            part.Thickness.SetValue(thickness);

            Assert.Equal(expPnum, part.Bom.MaterialPartNumber.Value);
        }

        [Theory]
        [InlineData(.375, 0.78638, 2.23125)]
        [InlineData(.5, 1.30156, 2.975)]
        [InlineData(.625, 1.8001, 2.97792)]
        public void CanGetFootplateMaterialCostWeight(float thickness, double expCost, double expWeight)
        {
            var part = _fixture.CreateRootPart<Footplate>();
            part.Thickness.SetValue(thickness);
            part.Width.SetValue(3);
            part.BasePlateLength.SetValue(7);

            Assert.Equal(expWeight, part.Bom.ItemWeight.Value, 4);
            Assert.Equal(expCost, part.Bom.ItemMaterialCost.Value, 4);
        }

        [Fact]
        public void CanGetFootplateMaterialCostIncrease()
        {
            var part1 = _fixture.CreateRootPart<Footplate>();
            part1.Thickness.SetValue(.5f);
            part1.BasePlateLength.SetValue(7);
            var part2 = _fixture.CreateRootPart<Footplate>();
            part2.Thickness.SetValue(.5f);
            part2.BasePlateLength.SetValue(8);

            Assert.True(part1.Bom.ItemMaterialCost.Value > 0);
            Assert.True(part2.Bom.ItemMaterialCost.Value > 0);
            Assert.True(part2.Bom.ItemMaterialCost.Value >  part1.Bom.ItemMaterialCost.Value);
        }

        #endregion

        #region Horizontal Brace Tests

        [Fact]
        public void CanGetHorizontalBraceIronworkerLabor()
        {
            var part = _fixture.CreateRootPart<HorizontalBrace>();

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(.00416, labor.First(l => l.OperationName.ToLower().Contains("ironworker")).Hours, 4);
        }

        [Fact]
        public void CanGetHorizontalBraceIronworkerSetup()
        {
            var part = _fixture.CreateRootPart<HorizontalBrace>();

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(0.25, labor.First(l => l.OperationName.ToLower().Contains("ironworker")).SetupHours, 4);
        }

        [Fact]
        public void CanGetHorizontalBraceSlantCutLabor()
        {
            var part = _fixture.CreateRootPart<HorizontalBrace>();
            part.RearMiterAngle.SetValue(5);

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(.0043, labor.First(l => l.OperationName.ToLower().Contains("slant cut")).Hours, 4);
        }

        [Fact]
        public void CanGetHorizontalBraceSlantCutSetup()
        {
            var part = _fixture.CreateRootPart<HorizontalBrace>();
            part.RearMiterAngle.SetValue(5);

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(0.25, labor.First(l => l.OperationName.ToLower().Contains("slant cut")).SetupHours, 4);
        }

        [Theory]
        [InlineData("1-1/2 x 1-1/2 x 1/8", "S.A.Z1616")]
        [InlineData("2 x 2 x 3/16", "S.A.S2A2A")]
        [InlineData("3 x 3 x 1/4", "S.A.43A3A")]
        public void CanGetHorizontalBraceMaterialPartNumber(string type, string expPnum)
        {
            var part = _fixture.CreateRootPart<HorizontalBrace>();
            part.AngleType.SetValue(type);

            Assert.Equal(expPnum, part.MaterialPartNumber.Value);
        }

        [Fact]
        public void CanGetHorizontalBraceMaterialPrice()
        {
            var part1 = _fixture.CreateRootPart<HorizontalBrace>();
            part1.AngleType.SetValue("3 x 3 x 1/4");
            part1.Length.SetValue(36);
            var part2 = _fixture.CreateRootPart<HorizontalBrace>();
            part2.AngleType.SetValue("3 x 3 x 1/4");
            part2.Length.SetValue(48);

            Assert.True(part1.Bom.ItemMaterialPrice.Value > 0);
            Assert.True(part2.Bom.ItemMaterialPrice.Value > 0);
            Assert.True(part2.Bom.ItemMaterialPrice.Value > part1.Bom.ItemMaterialPrice.Value);
        }

        [Theory]
        [InlineData("1-1/2 x 1-1/2 x 1/8", .474, 1.23)] //cost updated from .446736 after csi integration
        [InlineData("2 x 2 x 3/16", 0.8996, 2.44)]
        [InlineData("3 x 3 x 1/4", 1.765, 4.9)] //cost updated from 1.73313 after csi integration
        public void CanGetHorizontalBraceCostAndWeight(string type, double expCost, double expWeight)
        {
            var part1 = _fixture.CreateRootPart<HorizontalBrace>();
            part1.AngleType.SetValue(type);
            part1.Length.SetValue(12);

            Assert.Equal(expWeight, part1.Bom.ItemWeight.Value, 4);
            Assert.Equal(expCost, part1.Bom.ItemMaterialPrice.Value, 4);
        }

        #endregion

        #region Diagonal Brace Tests

        [Fact]
        public void CanGetDiagonalBraceSawLabor()
        {
            var part = _fixture.CreateRootPart<DiagonalBrace>();

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(.00137, labor.First(l => l.OperationName.ToLower().Contains("saw")).Hours, 4);
        }

        #endregion

        #region Heavy Horizontal Tests

        [Fact]
        public void CanGetHeavyHorizontalBraceSawLabor()
        {
            var part = _fixture.CreateRootPart<HeavyHorizontalBrace>();

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(.0045, labor.First(l => l.OperationName.ToLower().Contains("horizontal saw")).Hours, 4);
        }

        [Theory]
        [InlineData(0, 0)]
        [InlineData(5, 0.01667)]
        public void CanGetHeavyHorizontalBraceColdSawLabor(float miterAngle, double expLaborHours)
        {
            var part = _fixture.CreateRootPart<HeavyHorizontalBrace>();
            part.RearMiterAngle.SetValue(miterAngle);

            var labor = part.Bom.LaborItems.Value;

            if (expLaborHours == 0)
            {
                Assert.True(labor.FirstOrDefault(l => l.OperationName.ToLower().Contains("cold saw")) == null);
            }
            else
            {
                Assert.Equal(expLaborHours, labor.First(l => l.OperationName.ToLower().Contains("cold saw")).Hours, 4);
            }
        }

        [Theory]
        [InlineData(true, false, 0.00833)]
        [InlineData(false, true, 0.00833)]
        [InlineData(true, true, 0.01667)]
        public void CanGetHeavyHorizontalBraceIronworkerLabor(bool frontHole, bool rearHole, double expHrs)
        {
            var part = _fixture.CreateRootPart<HeavyHorizontalBrace>();
            part.HasFrontHole.SetValue(frontHole);
            part.HasRearHole.SetValue(rearHole);

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(expHrs, labor.First(l => l.OperationName.ToLower().Contains("ironworker")).Hours, 4);
        }

        [Theory]
        [InlineData(false, false, 0)]
        [InlineData(true, false, 0.5)]
        [InlineData(false, true, 0.5)]
        [InlineData(true, true, 1.0)]
        public void CanGetHeavyHorizontalBraceIronworkerSetup(bool frontHole, bool rearHole, double expHrs)
        {
            var part = _fixture.CreateRootPart<HeavyHorizontalBrace>();
            part.HasFrontHole.SetValue(frontHole);
            part.HasRearHole.SetValue(rearHole);

            Assert.Equal(expHrs, part.Bom.ItemSetupHours.Value, 4);
        }

        #endregion

        #region Tippmann Horizontal Tests

        [Fact]
        public void CanGetTippmannHorizontalBraceSawLabor()
        {
            var part = _fixture.CreateRootPart<TippmannHorizontal>();

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(.0045, labor.First(l => l.OperationName.ToLower().Contains("saw")).Hours, 4);
        }

        [Fact]
        public void CanGetTippmanHorizontalBraceIronworkerLabor()
        {
            var part = _fixture.CreateRootPart<TippmannHorizontal>();

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(0.04, labor.First(l => l.OperationName.ToLower().Contains("ironworker")).Hours, 4);
        }

        [Fact]
        public void CanGetTippmannHorizontalBraceIronworkerSetup()
        {
            var part = _fixture.CreateRootPart<TippmannHorizontal>();

            Assert.Equal(0.75, part.Bom.ItemSetupHours.Value, 4);
        }

        #endregion

        #region Heavy Horizontal Weld Clip Tests

        [Fact]
        public void CanGetHeavyHorizontalWeldClipSawLabor()
        {
            var part = _fixture.CreateRootPart<HeavyHorizontalWeldClip>();

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(.003378, labor.First(l => l.OperationName.ToLower().Contains("saw")).Hours, 4);
        }

        #endregion

        #region Heavy Horizontal Weld Plate Tests

        [Fact]
        public void CanGetHeavyHorizontalWeldPlateBurntableLabor()
        {
            var part = _fixture.CreateRootPart<HeavyHorizontalWeldPlate>();

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(.0025, labor.First(l => l.OperationName.ToLower().Contains("burn")).Hours, 4);
        }

        [Fact]
        public void CanGetHeavyHorizontalWeldPlateBurntableSetup()
        {
            var part = _fixture.CreateRootPart<HeavyHorizontalWeldPlate>();

            Assert.Equal(0.5, part.Bom.ItemSetupHours.Value, 4);
        }

        #endregion

        #region Bullnose Tests

        [Theory]
        [InlineData(2.75, 3, 1.637)]
        [InlineData(2.75, 4, 1.878)]
        [InlineData(2.75, 5, 2.8)]
        [InlineData(3.75, 3, 1.768)]
        [InlineData(3.75, 4, 2.95)]
        [InlineData(3.75, 5, 2.759)]
        [InlineData(4.75, 3, 2.25)]
        [InlineData(4.75, 4, 2.85)]
        [InlineData(4.75, 5, 3)]
        [InlineData(5.75, 3, 2.765)]
        [InlineData(5.75, 4, 2.821)]
        [InlineData(5.75, 5, 3.2)]
        [InlineData(7.75, 3, 4.62)]
        [InlineData(7.75, 4, 4.397)]
        public void CanGetBullnoseCost(float height, float columnWidth, double expCost)
        {
            var part = _fixture.CreateRootPart<Bullnose>();
            part.ColumnWidth.SetValue(columnWidth);
            part.Height.SetValue(height);

            Assert.Equal(expCost, part.Bom.ItemCost.Value, 4);
        }

        #endregion

        #region Angle Post Protector Tests

        [Fact]
        public void CanGetAnglePostProtectorIronworkerLabor()
        {
            var part = _fixture.CreateRootPart<AnglePostProtector>();

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(.00222, labor.First(l => l.OperationName.ToLower().Contains("ironworker")).Hours, 4);
        }

        [Fact]
        public void CanGetAnglePostProtectorIronworkerSetup()
        {
            var part = _fixture.CreateRootPart<AnglePostProtector>();

            Assert.Equal(0.25, part.Bom.ItemSetupHours.Value, 4);
        }

        #endregion

        #region Bolt On Horizontal Brace Tests

        [Fact]
        public void CanGetBoltOnHorizontalBraceSawLabor()
        {
            var part = _fixture.CreateRootPart<BoltOnHorizontalBrace>();

            var labor = part.BoltOnChannel.Bom.LaborItems.Value;
            Assert.Equal(.0045, labor.First(l => l.OperationName.ToLower().Contains("saw")).Hours, 4);
        }
        
        [Fact]
        public void CanGetBoltOnHorizontalBraceIronworkerLabor()
        {
            var part = _fixture.CreateRootPart<BoltOnHorizontalBrace>();

            var labor = part.BoltOnChannel.Bom.LaborItems.Value;
            Assert.Equal(0.01538, labor.First(l => l.OperationName.ToLower().Contains("ironworker")).Hours, 4);
        }

        [Fact]
        public void CanGetBoltOnHorizontalBraceIronworkerSetup()
        {
            var part = _fixture.CreateRootPart<BoltOnHorizontalBrace>();

            Assert.Equal(0.25, part.BoltOnChannel.Bom.ItemSetupHours.Value, 4);
        }

        #endregion

        #region Bolt On Horizontal Angle Tests

        [Fact]
        public void CanGetBoltOnHorizontalAngleSawLabor()
        {
            var part = _fixture.CreateRootPart<BoltonHeavyHorizontalAngle>();

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(.003378, labor.First(l => l.OperationName.ToLower().Contains("saw")).Hours, 4);
        }

        [Fact]
        public void CanGetBoltOnHorizontalAngleIronworkerLabor()
        {
            var part = _fixture.CreateRootPart<BoltonHeavyHorizontalAngle>();

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(0.01111, labor.First(l => l.OperationName.ToLower().Contains("ironworker")).Hours, 4);
        }

        [Fact]
        public void CanGetBoltOnHorizontalAngleIronworkerSetup()
        {
            var part = _fixture.CreateRootPart<BoltonHeavyHorizontalAngle>();

            Assert.Equal(0.5, part.Bom.ItemSetupHours.Value, 4);
        }

        #endregion

        #region Shelf Pallet Stop Tests

        [Fact]
        public void CanGetShelfPalletStopPaintLabor()
        {
            var part = _fixture.CreateRootPart<ShelfPalletStop>();

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(.001666, labor.First(l => l.OperationName.ToLower().Contains("paint")).Hours, 4);
        }

        [Fact]
        public void CanGetShelfPalletStopUnloadLabor()
        {
            var part = _fixture.CreateRootPart<ShelfPalletStop>();

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(.008, labor.First(l => l.OperationName.ToLower().Contains("unload")).Hours, 4);
        }

        [Fact]
        public void CanGetShelfPalletStopIronworkerCutLabor()
        {
            var part = _fixture.CreateRootPart<ShelfPalletStop>();

            var labor = part.ShelfPalletStopMaterial.Bom.LaborItems.Value;
            Assert.Equal(.003333, labor.First(l => l.OperationName.ToLower().Contains("ironworker cut")).Hours, 4);
        }

        [Fact]
        public void CanGetShelfPalletStopIronworkerCutSetup()
        {
            var part = _fixture.CreateRootPart<ShelfPalletStop>();

            var labor = part.ShelfPalletStopMaterial.Bom.LaborItems.Value;
            Assert.Equal(.25, labor.First(l => l.OperationName.ToLower().Contains("ironworker cut")).SetupHours, 4);
        }

        [Fact]
        public void CanGetShelfPalletStopIronworkerPunchLabor()
        {
            var part = _fixture.CreateRootPart<ShelfPalletStop>();

            var labor = part.ShelfPalletStopMaterial.Bom.LaborItems.Value;
            Assert.Equal(.006667, labor.First(l => l.OperationName.ToLower().Contains("ironworker punch")).Hours, 4);
        }

        [Fact]
        public void CanGetShelfPalletStopIronworkerPunchSetup()
        {
            var part = _fixture.CreateRootPart<ShelfPalletStop>();

            var labor = part.ShelfPalletStopMaterial.Bom.LaborItems.Value;
            Assert.Equal(.25, labor.First(l => l.OperationName.ToLower().Contains("ironworker punch")).SetupHours, 4);
        }

        [Fact]
        public void CanGetShelfPalletStopAccurpressForm1Labor()
        {
            var part = _fixture.CreateRootPart<ShelfPalletStop>();
            part.Style.SetValue("PS-L-DD-X");

            var labor = part.ShelfPalletStopMaterial.Bom.LaborItems.Value;
            Assert.Equal(.01111, labor.First(l => l.OperationName.ToLower().Contains("accurpress form1")).Hours, 4);
        }

        [Fact]
        public void CanGetShelfPalletStopAccurpressForm1Setup()
        {
            var part = _fixture.CreateRootPart<ShelfPalletStop>();
            part.Style.SetValue("PS-L-DD-X");

            var labor = part.ShelfPalletStopMaterial.Bom.LaborItems.Value;
            Assert.Equal(1, labor.First(l => l.OperationName.ToLower().Contains("accurpress form1")).SetupHours, 4);
        }

        [Fact]
        public void CanGetShelfPalletStopAccurpressForm2Labor()
        {
            var part = _fixture.CreateRootPart<ShelfPalletStop>();
            part.Style.SetValue("PS-R-X");

            var labor = part.ShelfPalletStopMaterial.Bom.LaborItems.Value;
            Assert.Equal(.016667, labor.First(l => l.OperationName.ToLower().Contains("accurpress form2")).Hours, 4);
        }

        [Fact]
        public void CanGetShelfPalletStopAccurpressForm2Setup()
        {
            var part = _fixture.CreateRootPart<ShelfPalletStop>();
            part.Style.SetValue("PS-R-X");

            var labor = part.ShelfPalletStopMaterial.Bom.LaborItems.Value;
            Assert.Equal(1, labor.First(l => l.OperationName.ToLower().Contains("accurpress form2")).SetupHours, 4);
        }

        #endregion

        #region Welded Crossbar Tests

        [Fact]
        public void CanGetWeldedCrossbarWeldLabor()
        {
            var part = _fixture.CreateRootPart<WeldedCrossbar>();

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(.017857, labor.First(l => l.OperationName.ToLower().Contains("crossbar weld")).Hours, 4);
        }

        [Fact]
        public void CanGetWeldedCrossbarWeldSetup()
        {
            var part = _fixture.CreateRootPart<WeldedCrossbar>();

            Assert.Equal(0.5, part.Bom.ItemSetupHours.Value, 4);
        }

        [Theory]
        [InlineData(48, 0.001667)]
        [InlineData(72, 0.003333)]
        public void CanGetWeldedCrossbarPaintLabor(float length, double expHrs)
        {
            var part = _fixture.CreateRootPart<WeldedCrossbar>();
            part.AngleLength.SetValue(length);

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(expHrs, labor.First(l => l.OperationName.ToLower().Contains("crossbar paint")).Hours, 4);
        }

        [Theory]
        [InlineData(48, 0.004)]
        [InlineData(72, 0.008)]
        public void CanGetWeldedCrossbarUnloadLabor(float length, double expHrs)
        {
            var part = _fixture.CreateRootPart<WeldedCrossbar>();
            part.AngleLength.SetValue(length);

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(expHrs, labor.First(l => l.OperationName.ToLower().Contains("unload")).Hours, 4);
        }

        #endregion

        #region Welded Rubrail Tests

        [Theory]
        [InlineData(3, .03257)]
        [InlineData(4, .03558)]
        [InlineData(5, .04347)]
        public void CanGetWeldedRubRailWeldLabor(float beamWidth, double expHrs)
        {
            var part = _fixture.CreateRootPart<WeldedRubRail>();
            part.BeamWidth.SetValue(beamWidth);

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(expHrs, labor.First(l => l.OperationName.ToLower().Contains("rubrail weld")).Hours, 4);
        }

        [Fact]
        public void CanGetWeldedRubRailWeldSetup()
        {
            var part = _fixture.CreateRootPart<WeldedRubRail>();

            Assert.Equal(0.5, part.Bom.ItemSetupHours.Value, 4);
        }

        [Theory]
        [InlineData(48, 0.003333)]
        [InlineData(72, 0.006667)]
        public void CanGetWeldedRubRailPaintLabor(float length, double expHrs)
        {
            var part = _fixture.CreateRootPart<WeldedRubRail>();
            part.OverallLength.SetValue(length);

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(expHrs, labor.First(l => l.OperationName.ToLower().Contains("rubrail paint")).Hours, 4);
        }

        [Theory]
        [InlineData(48, 0.008)]
        [InlineData(72, 0.016)]
        public void CanGetWeldedRubRailUnloadLabor(float length, double expHrs)
        {
            var part = _fixture.CreateRootPart<WeldedRubRail>();
            part.OverallLength.SetValue(length);

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(expHrs, labor.First(l => l.OperationName.ToLower().Contains("rubrail unload")).Hours, 4);
        }

        #endregion

        #region Doubled Rubrail Tests

        [Theory]
        [InlineData("C3x4.1", .11111)]
        [InlineData("C4x4.5", .125)]
        [InlineData("C5x6.1", .142857)]
        public void CanGetDoubledRubRailWeldLabor(string channelSize, double expHrs)
        {
            var part = _fixture.CreateRootPart<DoubledRubRail>();
            part.ChannelSize.SetValue(channelSize);

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(expHrs, labor.First(l => l.OperationName.ToLower().Contains("weld")).Hours, 4);
        }

        [Fact]
        public void CanGetDoubledRubRailWeldSetup()
        {
            var part = _fixture.CreateRootPart<DoubledRubRail>();

            Assert.Equal(0.5, part.Bom.ItemSetupHours.Value, 4);
        }

        [Theory]
        [InlineData(48, 0.003333)]
        [InlineData(72, 0.006667)]
        public void CanGetDoubledRubRailPaintLabor(float length, double expHrs)
        {
            var part = _fixture.CreateRootPart<DoubledRubRail>();
            part.OverallLength.SetValue(length);

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(expHrs, labor.First(l => l.OperationName.ToLower().Contains("paint")).Hours, 4);
        }

        [Theory]
        [InlineData(48, 0.008)]
        [InlineData(72, 0.016)]
        public void CanGetDoubledRubRailUnloadLabor(float length, double expHrs)
        {
            var part = _fixture.CreateRootPart<DoubledRubRail>();
            part.OverallLength.SetValue(length);

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(expHrs, labor.First(l => l.OperationName.ToLower().Contains("unload")).Hours, 4);
        }

        #endregion

        #region Doubled Rub Rail Plate Tests

        [Fact]
        public void CanGetDoubledRubrailPlateIronworkerLabor()
        {
            var part = _fixture.CreateRootPart<DoubledRubRailPlate>();

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(.0025, labor.First(l => l.OperationName.ToLower().Contains("ironworker")).Hours, 4);
        }

        [Fact]
        public void CanGetDoubledRubrailPlateIronworkerSetup()
        {
            var part = _fixture.CreateRootPart<DoubledRubRailPlate>();

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(0.25, labor.First(l => l.OperationName.ToLower().Contains("ironworker")).SetupHours, 4);
        }

        [Fact]
        public void CanGetDoubledRubrailPlateAccurpressLabor()
        {
            var part = _fixture.CreateRootPart<DoubledRubRailPlate>();

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(.006667, labor.First(l => l.OperationName.ToLower().Contains("accurpress")).Hours, 4);
        }

        [Fact]
        public void CanGetDoubledRubrailPlateAccurpressSetup()
        {
            var part = _fixture.CreateRootPart<DoubledRubRailPlate>();

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(2, labor.First(l => l.OperationName.ToLower().Contains("accurpress")).SetupHours, 4);
        }

        #endregion

        #region Frame Tie Four Hole End Plate Tests

        [Theory]
        [InlineData("rfswhd", 0.002232)]
        [InlineData("rrx", 0.002232)]
        [InlineData("rrxI", 0.002232)]
        [InlineData("rfsw", 0.002222)]
        public void CanGetFrameTieEndPlateIronworkerLabor(string type, double expHrs)
        {
            var part = _fixture.CreateRootPart<FrameTieFourHoleEndPlate>();
            part.FrameTieType.SetValue(type);

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(expHrs, labor.First(l => l.OperationName.ToLower().Contains("ironworker")).Hours, 4);
        }

        [Fact]
        public void CanGetFrameTieEndPlateIronworkerSetup()
        {
            var part = _fixture.CreateRootPart<FrameTieFourHoleEndPlate>();

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(0.25, labor.First(l => l.OperationName.ToLower().Contains("ironworker")).SetupHours, 4);
        }

        [Theory]
        [InlineData("rfswhd", 0.003389)]
        [InlineData("rrx", 0.003389)]
        [InlineData("rrxI", 0.003389)]
        [InlineData("rfsw", 0.003571)]        
        public void CanGetFrameTieEndPlateAccurpressLabor(string type, double expHrs)
        {
            var part = _fixture.CreateRootPart<FrameTieFourHoleEndPlate>();
            part.FrameTieType.SetValue(type);

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(expHrs, labor.First(l => l.OperationName.ToLower().Contains("accurpress")).Hours, 4);
        }

        [Fact]
        public void CanGetFrameTieEndPlateAccurpressSetup()
        {
            var part = _fixture.CreateRootPart<FrameTieFourHoleEndPlate>();

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(1, labor.First(l => l.OperationName.ToLower().Contains("accurpress")).SetupHours, 4);
        }

        [Theory]
        [InlineData(2, "S.F.AS02A")]
        [InlineData(3, "S.F.AS03A")]
        [InlineData(4, "S.F.AS04A")]
        [InlineData(5, "S.F.AS05A")]
        public void CanGetFrameTieEndPlateMaterialPartNumber(float width, string expPnum)
        {
            var part = _fixture.CreateRootPart<FrameTieFourHoleEndPlate>();
            part.Thickness.SetValue(.1875f);
            part.Width.SetValue(width);

            Assert.Equal(expPnum, part.Bom.MaterialPartNumber.Value);
        }

        [Theory]
        [InlineData(.375, 3, 0.8472, 2.23417)]
        [InlineData(.5, 4, 1.4724, 3.966667)]
        [InlineData(.625, 3, 1.4261, 3.721667)]
        public void CanGetFrameTieEndPlateMaterialCostWeight(float thickness, float width, double expCost, double expWeight)
        {
            var part = _fixture.CreateRootPart<FrameTieFourHoleEndPlate>();
            part.Thickness.SetValue(thickness);
            part.Width.SetValue(width);
            part.Height.SetValue(7);

            Assert.Equal(expWeight, part.Bom.ItemWeight.Value, 4);
            Assert.Equal(expCost, part.Bom.ItemMaterialCost.Value, 4);
        }

        #endregion

        #region Rubrail Angle Tests

        [Fact]
        public void CanGetRubrailAngleIronworkerCutLabor()
        {
            var part = _fixture.CreateRootPart<RubRailAngle>();

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(0.002778, labor.First(l => l.OperationName.ToLower().Contains("ironworker cut")).Hours, 4);
        }

        [Fact]
        public void CanGetRubRailAngleIronworkerCutSetup()
        {
            var part = _fixture.CreateRootPart<RubRailAngle>();

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(0.25, labor.First(l => l.OperationName.ToLower().Contains("ironworker cut")).SetupHours, 4);
        }

        [Fact]
        public void CanGetRubrailAngleIronworkerPunch1Labor()
        {
            var part = _fixture.CreateRootPart<RubRailAngle>();

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(0.002778, labor.First(l => l.OperationName.ToLower().Contains("ironworker punch1")).Hours, 4);
        }

        [Fact]
        public void CanGetRubRailAngleIronworkerPunch1Setup()
        {
            var part = _fixture.CreateRootPart<RubRailAngle>();

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(0.25, labor.First(l => l.OperationName.ToLower().Contains("ironworker punch1")).SetupHours, 4);
        }

        [Fact]
        public void CanGetRubrailAngleIronworkerPunch2Labor()
        {
            var part = _fixture.CreateRootPart<RubRailAngle>();

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(0.002778, labor.First(l => l.OperationName.ToLower().Contains("ironworker punch2")).Hours, 4);
        }

        [Fact]
        public void CanGetRubRailAngleIronworkerPunch2Setup()
        {
            var part = _fixture.CreateRootPart<RubRailAngle>();

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(0.25, labor.First(l => l.OperationName.ToLower().Contains("ironworker punch2")).SetupHours, 4);
        }

        #endregion

        #region Frame Tie Four Hole End Plate Tests

        [Fact]
        public void CanGetFrameTieChannelSawLabor()
        {
            var part = _fixture.CreateRootPart<FrameTieChannel>();

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(0.0045, labor.First(l => l.OperationName.ToLower().Contains("saw")).Hours, 4);
        }

        #endregion

        #region Welded HD Frame Tie Tests

        [Theory]
        [InlineData(3, .0625)]
        [InlineData(4, .067568)]
        [InlineData(5, .080645)]
        public void CanGetWeldedHDFrameTieWeldLabor(float beamWidth, double expHrs)
        {
            var part = _fixture.CreateRootPart<WeldedHDFrameTie>();
            part.BeamWidth.SetValue(beamWidth);

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(expHrs, labor.First(l => l.OperationName.ToLower().Contains("welded hd frame tie weld")).Hours, 4);
        }

        [Fact]
        public void CanGetWeldedHDFrameTieWeldSetup()
        {
            var part = _fixture.CreateRootPart<WeldedHDFrameTie>();

            Assert.Equal(0.5, part.Bom.ItemSetupHours.Value, 4);
        }

        [Theory]
        [InlineData(48, 0.003333)]
        [InlineData(72, 0.006667)]
        public void CanGetWeldedHDFrameTiePaintLabor(float length, double expHrs)
        {
            var part = _fixture.CreateRootPart<WeldedHDFrameTie>();
            part.OverallLength.SetValue(length);

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(expHrs, labor.First(l => l.OperationName.ToLower().Contains("welded hd frame tie paint")).Hours, 4);
        }

        [Theory]
        [InlineData(48, 0.008)]
        [InlineData(72, 0.016)]
        public void CanGetWeldedHDFrameTieUnloadLabor(float length, double expHrs)
        {
            var part = _fixture.CreateRootPart<WeldedHDFrameTie>();
            part.OverallLength.SetValue(length);

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(expHrs, labor.First(l => l.OperationName.ToLower().Contains("welded hd frame tie unload")).Hours, 4);
        }

        #endregion

        #region Welded Frame Tie Tests

        [Theory]
        [InlineData(1.5, .017857)]
        [InlineData(2, .02)]
        public void CanGetWeldedFrameTieWeldLabor(float angleWidth, double expHrs)
        {
            var part = _fixture.CreateRootPart<WeldedFrameTie>();
            part.AngleWidth.SetValue(angleWidth);

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(expHrs, labor.First(l => l.OperationName.ToLower().Contains("welded frame tie weld")).Hours, 4);
        }

        [Fact]
        public void CanGetWeldedFrameTieWeldSetup()
        {
            var part = _fixture.CreateRootPart<WeldedFrameTie>();

            Assert.Equal(0.5, part.Bom.ItemSetupHours.Value, 4);
        }

        [Theory]
        [InlineData(48, 0.001667)]
        [InlineData(72, 0.003333)]
        public void CanGetWeldedFrameTiePaintLabor(float length, double expHrs)
        {
            var part = _fixture.CreateRootPart<WeldedFrameTie>();
            part.OverallLength.SetValue(length);

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(expHrs, labor.First(l => l.OperationName.ToLower().Contains("welded frame tie paint")).Hours, 4);
        }

        [Theory]
        [InlineData(48, 0.004)]
        [InlineData(72, 0.008)]
        public void CanGetWeldedFrameTieUnloadLabor(float length, double expHrs)
        {
            var part = _fixture.CreateRootPart<WeldedFrameTie>();
            part.OverallLength.SetValue(length);

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(expHrs, labor.First(l => l.OperationName.ToLower().Contains("welded frame tie unload")).Hours, 4);
        }

        #endregion

        #region Frame Tie Angle Tests

        [Fact]
        public void CanGetFrameTieAngleSawLabor()
        {
            var part = _fixture.CreateRootPart<FrameTieAngle>();

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(0.0014, labor.First(l => l.OperationName.ToLower().Contains("saw")).Hours, 4);
        }

        #endregion

        #region RS Frame Tie Tests

        [Fact]
        public void CanGetRSFrameTiePaintLabor()
        {
            var part = _fixture.CreateRootPart<RSFrameTie>();

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(0.0016667, labor.First(l => l.OperationName.ToLower().Contains("paint")).Hours, 4);
        }

        [Fact]
        public void CanGetRSFrameTieUnloadLabor()
        {
            var part = _fixture.CreateRootPart<RSFrameTie>();

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(0.005, labor.First(l => l.OperationName.ToLower().Contains("unload")).Hours, 4);
        }

        [Fact]
        public void CanGetRSFrameTieFicepLabor()
        {
            var part = _fixture.CreateRootPart<RSFrameTie>();

            var labor = part.RSFrameTieAngle.Bom.LaborItems.Value;
            Assert.Equal(0.003333, labor.First(l => l.OperationName.ToLower().Contains("ficep")).Hours, 4);
        }

        [Fact]
        public void CanGetRSFrameTieFicepSetup()
        {
            var part = _fixture.CreateRootPart<RSFrameTie>();

            var labor = part.RSFrameTieAngle.Bom.LaborItems.Value;
            Assert.Equal(0.25, labor.First(l => l.OperationName.ToLower().Contains("ficep")).SetupHours, 4);
        }

        [Fact]
        public void CanGetRSFrameTieAccurpressForm1Labor()
        {
            var part = _fixture.CreateRootPart<RSFrameTie>();

            var labor = part.RSFrameTieAngle.Bom.LaborItems.Value;
            Assert.Equal(0.011111, labor.First(l => l.OperationName.ToLower().Contains("accurpress form1")).Hours, 4);
        }

        [Fact]
        public void CanGetRSFrameTieAccurpressForm1Setup()
        {
            var part = _fixture.CreateRootPart<RSFrameTie>();

            var labor = part.RSFrameTieAngle.Bom.LaborItems.Value;
            Assert.Equal(1.0, labor.First(l => l.OperationName.ToLower().Contains("accurpress form1")).SetupHours, 4);
        }

        [Fact]
        public void CanGetRSFrameTieAccurpressForm2Labor()
        {
            var part = _fixture.CreateRootPart<RSFrameTie>();
            part.Length.SetValue(6);

            var labor = part.RSFrameTieAngle.Bom.LaborItems.Value;
            Assert.Equal(0.011111, labor.First(l => l.OperationName.ToLower().Contains("accurpress form2")).Hours, 4);
        }

        #endregion

        #region Upright Column Assembly Tests

        [Theory]
        [InlineData("none", 0, .08796)]
        [InlineData("angle", 6, .119494)]
        [InlineData("bullnose", 3, .11975)]
        public void CanGetUprightColumnAssemblyWeldLabor(string protectorType, float protectorHeight, double expHrs)
        {
            var part = _fixture.CreateRootPart<UprightColumnAssembly>();
            part.OverallWidth.SetValue(3);
            part.ColumnHeight.SetValue(120);
            part.IsFramePostColumn.SetValue(true);
            part.HasDoubler.SetValue(true);
            part.DoublerHeight.SetValue(100);
            part.PostProtectorType.SetValue(protectorType);
            part.PostProtectorHeight.SetValue(protectorHeight);

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(expHrs, labor.First(l => l.OperationName.ToLower().Contains("weld")).Hours, 4);
        }

        [Fact]
        public void CanGetUprightColumnAssemblyWeldSetup()
        {
            var part = _fixture.CreateRootPart<UprightColumnAssembly>();
            part.IsFramePostColumn.SetValue(true);

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(0.5, labor.First(l => l.OperationName.ToLower().Contains("weld")).SetupHours, 4);
        }

        [Theory]
        [InlineData(3, 120, 0.0193)]
        [InlineData(3, 160, 0.0293)]
        [InlineData(3, 260, 0.0433)]
        [InlineData(3, 372, 0.0633)]
        [InlineData(4, 120, 0.0213)]
        [InlineData(4, 160, 0.0313)]
        [InlineData(4, 260, 0.0453)]
        [InlineData(4, 372, 0.0653)]
        [InlineData(5, 120, 0.0223)]
        [InlineData(5, 160, 0.0323)]
        [InlineData(5, 260, 0.0463)]
        [InlineData(5, 372, 0.0663)]
        public void CanGetUprightColumnAssemblyWeldBaseLabor(float channelWidth, float height, double expHrs)
        {
            var part = _fixture.CreateRootPart<UprightColumnAssembly>();
            part.OverallWidth.SetValue(channelWidth);
            part.ColumnHeight.SetValue(height);
            part.IsFramePostColumn.SetValue(true);

            Assert.Equal(expHrs, part.WeldBaseLabor.Value, 4);
        }

        [Theory]
        [InlineData(false, 84, 84, 0)]
        [InlineData(true, 100, 100, 0.06216f)]
        [InlineData(true, 112, 116, 0.06888f)]
        [InlineData(true, 224, 348, 0.16632f)]
        [InlineData(true, 332, 680, 0.30576f)]
        public void CanGetUprightColumnAssemblynDoublerWeldLaborAddon(bool hasDoubler, float doubleHeight, float expEDH, float expHrs)
        {
            var part = _fixture.CreateRootPart<UprightColumnAssembly>();
            part.ColumnHeight.SetValue(360);
            part.HasDoubler.SetValue(hasDoubler);
            part.DoublerHeight.SetValue(doubleHeight);

            Assert.Equal(expEDH, part.EDH.Value);
            Assert.Equal(expHrs, part.DoublerWeldAddon.Value, 4);
        }

        [Theory]
        [InlineData(false, 0)]
        [InlineData(true, 0.0065)]
        public void CanGetUprightColumnAssemblynDoublerCapWeldLaborAddon(bool hasDoubler, float expHrs)
        {
            var part = _fixture.CreateRootPart<UprightColumnAssembly>();
            part.HasDoubler.SetValue(hasDoubler);

            Assert.Equal(expHrs, part.DoublerCapWeldAddon.Value, 4);
        }

        [Theory]
        [InlineData("none", 0, 0)]
        [InlineData("angle", 6, 0.031534)]
        [InlineData("angle", 14, 0.139054)]
        public void CanGetUprightColumnAssemblyAngleProtectorWeldLaborAddon(string type, float angleHeight, float expHrs)
        {
            var part = _fixture.CreateRootPart<UprightColumnAssembly>();
            part.PostProtectorType.SetValue(type);
            part.PostProtectorHeight.SetValue(angleHeight);

            Assert.Equal(expHrs, part.AngleProtectorWeldAddon.Value, 4);
        }

        [Theory]
        [InlineData("none", 0, 0)]
        [InlineData("bullnose", 3, 0.03179)]
        [InlineData("bullnose", 4, 0.03179)]
        [InlineData("bullnose", 5, 0.03599)]
        [InlineData("bullnose", 6, 0.03599)]
        [InlineData("bullnose", 8, 0.03809)]
        public void CanGetUprightColumnAssemblyBullnoseWeldLaborAddon(string type, float height, float expHrs)
        {
            var part = _fixture.CreateRootPart<UprightColumnAssembly>();
            part.PostProtectorType.SetValue(type);
            part.PostProtectorHeight.SetValue(height);

            Assert.Equal(expHrs, part.BullnoseWeldAddon.Value, 4);
        }

        [Theory]
        [InlineData(100, .016667)]
        [InlineData(250, .033333)]
        public void CanGetUprightColumnAssemblyPaintLabor(float weight, double expHrs)
        {
            var part = _fixture.CreateRootPart<UprightColumnAssembly>();
            part.IsFramePostColumn.SetValue(true);
            part.Weight.SetValue(weight);
            //part.HasDoubler.SetValue(true);
            //part.DoublerHeight.SetValue(part.ColumnHeight.Value / 2);

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(expHrs, labor.First(l => l.OperationName.ToLower().Contains("paint")).Hours, 4);
        }

        [Fact]
        public void CanGetUprightColumnAssemblyPaintSetup()
        {
            var part = _fixture.CreateRootPart<UprightColumnAssembly>();
            part.IsFramePostColumn.SetValue(true);

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(0.1, labor.First(l => l.OperationName.ToLower().Contains("paint")).SetupHours, 4);
        }

        [Theory]
        [InlineData(100, .05)]
        [InlineData(250, .1)]
        public void CanGetUprightColumnAssemblyUnloadLabor(float weight, double expHrs)
        {
            var part = _fixture.CreateRootPart<UprightColumnAssembly>();
            part.IsFramePostColumn.SetValue(true);
            part.Weight.SetValue(weight);

            var labor = part.Bom.LaborItems.Value;
            Assert.Equal(expHrs, labor.First(l => l.OperationName.ToLower().Contains("unload")).Hours, 4);
        }

        #endregion

        #region Paint Tests

        [Theory]
        [InlineData("C3x3.5", "8", 48, "OT.PNT.I.SRED", 0.5308)]
        [InlineData("C3x3.5", "8", 48, "OT.PNT.I.SORG", 0.6351)]
        [InlineData("C10x15.3", "12", 12, "OT.PNT.I.CUSTOM", 0.9717)]
        [InlineData("C10x15.3", "12", 200, "OT.PNT.I.CUSTOM", 13.4361)]

        public void CanGetShelfLoadBeamPaintCost(string channelSize, string bracketType, float width, 
                                                string colorItem, float expCost)
        {
            var part = _fixture.CreateRootPart<ShelfLoadBeam>();
            part.ChannelSize.SetValue(channelSize);
            part.BracketType.SetValue(bracketType);
            part.Width.SetValue(width);
            part.Color.SetValue(colorItem);
            var partPaint = GetPaintChild(part.ActiveChildParts.Value);

            Assert.Equal(expCost, part.PaintCost.Value, 4);
            Assert.Equal(expCost, partPaint.Bom.TotalCost.Value, 4);
        }

        [Theory]
        [InlineData("OT.PNT.I.SORG", .01224)]
        [InlineData("OT.PNT.I.SRED", 0.0102)]
        [InlineData("OT.PNT.I.RBLU", .00979)]
        public void CanGetXBCrossbarPaintCost(string color, float expCost)
        {
            var part = _fixture.CreateRootPart<XBCrossbar>();
            part.Color.SetValue(color);
            var partPaint = GetPaintChild(part.ActiveChildParts.Value);

            Assert.Equal(expCost, part.PaintCost.Value, 3);
            Assert.Equal(expCost, partPaint.Bom.TotalCost.Value, 3);
        }

        [Fact]
        public void CanGetXBCrossbarPaintCostIncrease()
        {
            var part1 = _fixture.CreateRootPart<XBCrossbar>();
            part1.Color.SetValue("OT.PNT.I.RBLU");
            part1.Length.SetValue(24);
            var part1Paint = GetPaintChild(part1.ActiveChildParts.Value);

            var part2 = _fixture.CreateRootPart<XBCrossbar>();
            part2.Color.SetValue("OT.PNT.I.RBLU");
            part2.Length.SetValue(36);
            var part2Paint = GetPaintChild(part2.ActiveChildParts.Value);

            Assert.True(part1.PaintCost.Value < part2.PaintCost.Value);
            Assert.True(part1Paint.Bom.TotalCost.Value < part2Paint.Bom.TotalCost.Value);
        }

        [Theory]
        [InlineData("OT.PNT.I.SORG", .02943)]
        [InlineData("OT.PNT.I.SRED", .02482)]
        [InlineData("OT.PNT.I.RBLU", .02355)]
        public void CanGetWeldedCrossbarPaintCost(string color, float expCost)
        {
            var part = _fixture.CreateRootPart<WeldedCrossbar>();
            part.Color.SetValue(color);
            var partPaint = GetPaintChild(part.ActiveChildParts.Value);

            Assert.Equal(expCost, part.PaintCost.Value, 3);
            Assert.Equal(expCost, partPaint.Bom.TotalCost.Value, 3);
        }

        [Theory]
        [InlineData("OT.PNT.I.SORG", 11.86646)]
        [InlineData("OT.PNT.I.SRED", 9.919)]
        [InlineData("OT.PNT.I.RBLU", 9.493)]
        public void CanGetFrameUprightPaintCost(string color, float expCost)
        {
            var part = _fixture.CreateRootPart<FrameUpright>();
            part.Color.SetValue(color);
            var partPaint = GetPaintChild(part.ActiveChildParts.Value);

            Assert.Equal(expCost, part.PaintCost.Value, 3);
            Assert.Equal(expCost, partPaint.Bom.TotalCost.Value, 3);
        }

        [Theory]
        [InlineData("OT.PNT.I.SORG", 0.06)]
        [InlineData("OT.PNT.I.SRED", 0.05)]
        [InlineData("OT.PNT.I.RBLU", 0.0481)]
        public void CanGetRSFrameTiePaintCost(string color, float expCost)
        {
            var part = _fixture.CreateRootPart<FrameTie>();
            part.Type.SetValue("rs");

            var part2 = part.RSFrameTie;
            part2.Color.SetValue(color);
            var part2Paint = GetPaintChild(part2.ActiveChildParts.Value);

            Assert.Equal(expCost, part2.PaintCost.Value, 3);
            Assert.Equal(expCost, part2Paint.Bom.TotalCost.Value, 3);
        }

        [Theory]
        [InlineData("OT.PNT.I.SORG", .096408)]
        [InlineData("OT.PNT.I.SRED", 0.080572)]
        [InlineData("OT.PNT.I.RBLU", .077155)]
        public void CanGetWeldedFrameTiePaintCost(string color, float expCost)
        {
            var part = _fixture.CreateRootPart<FrameTie>();
            part.Type.SetValue("rfsw");

            var part2 = part.RSFWFrameTie;
            part2.Color.SetValue(color);
            var part2Paint = GetPaintChild(part2.ActiveChildParts.Value);

            Assert.Equal(expCost, part2.PaintCost.Value, 3);
            Assert.Equal(expCost, part2Paint.Bom.TotalCost.Value, 3);
        }

        [Theory]
        [InlineData("OT.PNT.I.SORG", .256386)]
        [InlineData("OT.PNT.I.SRED", 0.214)]
        [InlineData("OT.PNT.I.RBLU", 0.205)]
        public void CanGetWeldedHDFrameTiePaintCost(string color, float expCost)
        {
            var part = _fixture.CreateRootPart<FrameTie>();
            part.Type.SetValue("rfswhd");

            var part2 = part.RFSWHDFrameTie;
            part2.Color.SetValue(color);
            var part2Paint = GetPaintChild(part2.ActiveChildParts.Value);

            Assert.Equal(expCost, part2.PaintCost.Value, 3);
            Assert.Equal(expCost, part2Paint.Bom.TotalCost.Value, 3);
        }

        [Theory]
        [InlineData("OT.PNT.I.SORG", .206739)]
        [InlineData("OT.PNT.I.SRED", 0.173)]
        [InlineData("OT.PNT.I.RBLU", 0.165)]
        public void CanGetWeldedRubRailTiePaintCost(string color, float expCost)
        {
            var part = _fixture.CreateRootPart<FrameTie>();
            part.Type.SetValue("rrx");

            var part2 = part.WeldedRubRail;
            part2.Color.SetValue(color);
            var part2Paint = GetPaintChild(part2.ActiveChildParts.Value);

            Assert.Equal(expCost, part2.PaintCost.Value, 3);
            Assert.Equal(expCost, part2Paint.Bom.TotalCost.Value, 3);
        }

        [Theory]
        [InlineData("OT.PNT.I.SORG", .338949)]
        [InlineData("OT.PNT.I.SRED", 0.2833)]
        [InlineData("OT.PNT.I.RBLU", 0.27116)]
        public void CanGetDoubledRubRailTiePaintCost(string color, float expCost)
        {
            var part = _fixture.CreateRootPart<FrameTie>();
            part.Type.SetValue("rrxd");

            var part2 = part.RRXDRubRail;
            part2.Color.SetValue(color);
            var part2Paint = GetPaintChild(part2.ActiveChildParts.Value);

            Assert.Equal(expCost, part2.PaintCost.Value, 3);
            Assert.Equal(expCost, part2Paint.Bom.TotalCost.Value, 3);
        }

        [Theory]
        [InlineData("OT.PNT.I.SORG", 0.183)]
        [InlineData("OT.PNT.I.SRED", 0.153)]
        [InlineData("OT.PNT.I.RBLU", 0.147)]
        public void CanGetShelfPalletStopPaintCost(string color, float expCost)
        {
            var part = _fixture.CreateRootPart<ShelfPalletStop>();
            part.Color.SetValue(color);
            var partPaint = GetPaintChild(part.ActiveChildParts.Value);

            Assert.Equal(expCost, part.PaintCost.Value, 3);
            Assert.Equal(expCost, partPaint.Bom.TotalCost.Value, 3);
        }

        [Theory]
        [InlineData(true, "OT.PNT.I.SORG", 4.2110)]
        [InlineData(true, "OT.PNT.I.SRED", 3.5199)]
        [InlineData(true, "OT.PNT.I.RBLU", 3.3688)]
        [InlineData(false, "OT.PNT.I.RBLU", 0)]
        public void CanGetColumnPostPaintCost(bool isColumnPost, string color, float expCost)
        {
            var part = _fixture.CreateRootPart<UprightColumnAssembly>();
            part.IsFramePostColumn.SetValue(isColumnPost);
            part.ColumnHeight.SetValue(360);
            part.Color.SetValue(color);
            var partPaint = GetPaintChild(part.ActiveChildParts.Value);

            Assert.Equal(expCost, part.PaintCost.Value, 3);
            if (expCost > 0)
            {
                Assert.Equal(expCost, partPaint.Bom.TotalCost.Value, 3);
            }
        }

        [Theory]
        [InlineData("OT.PNT.I.SORG", .78)]
        [InlineData("OT.PNT.I.SRED", 0.6520)]
        [InlineData("OT.PNT.I.RBLU", 0.6240)]
        public void CanGetCustomPartPaintCost(string color, float expCost)
        {
            var part = _fixture.CreateRootPart<CustomPart>();            
            part.PartWeight.SetValue(20);
            part.Color.SetValue(color);

            Assert.Equal(expCost, part.PaintCost.Value, 3);
            Assert.Equal(expCost, part.Bom.ItemAddedCost.Value, 3);
            Assert.Equal(expCost, part.Bom.ItemMaterialFinalCost.Value - part.Bom.ItemMaterialCost.Value, 3);
        }

        [Theory]
        [InlineData("OT.PNT.I.SORG", .78)]
        [InlineData("OT.PNT.I.SRED", 0.6520)]
        [InlineData("OT.PNT.I.RBLU", 0.624)]
        public void CanGetBuyoutPartPaintCost(string color, float expCost)
        {
            var part = _fixture.CreateRootPart<BuyoutPart>();
            part.PartWeight.SetValue(20);
            part.Color.SetValue(color);

            Assert.Equal(expCost, part.PaintCost.Value, 3);
            Assert.Equal(expCost, part.Bom.ItemAddedCost.Value, 3);
            Assert.Equal(expCost, part.Bom.ItemMaterialFinalCost.Value - part.Bom.ItemMaterialCost.Value, 3);
        }

        #endregion

        #region Material Tests

        [Fact]
        public void CheckUprightColumnMaterialNumberandCost()
        {
            var part = _fixture.CreateRootPart<UprightColumn>();
            part.Length.SetValue(120);

            Assert.NotEmpty(part.MaterialPartNumber.Value);
            Assert.True(part.Bom.ItemMaterialCost.Value > 0);
        }

        [Fact]
        public void CheckHeavyHorizontalBraceMaterialNumberandCost()
        {
            var part = _fixture.CreateRootPart<HeavyHorizontalBrace>();
            part.Length.SetValue(36);

            Assert.NotEmpty(part.MaterialPartNumber.Value);
            Assert.True(part.Bom.ItemMaterialCost.Value > 0);
        }

        [Fact]
        public void CheckShelfBeamMaterialNumberandCost()
        {
            var part = _fixture.CreateRootPart<ShelfBeam>();
            part.Length.SetValue(36);

            Assert.NotEmpty(part.MaterialPartNumber.Value);
            Assert.True(part.Bom.ItemMaterialCost.Value > 0);
        }

        [Fact]
        public void CheckFrameTieChannelMaterialNumberandCost()
        {
            var part = _fixture.CreateRootPart<FrameTieChannel>();
            part.FrameTieType.SetValue("rrx");
            part.ChannelSize.SetValue("C3x4.1");
            part.Length.SetValue(12);

            Assert.NotEmpty(part.MaterialPartNumber.Value);
            Assert.True(part.Bom.ItemMaterialCost.Value > 0);
        }

        [Fact]
        public void CheckXBCrossbarMaterialNumberandCost()
        {
            var part = _fixture.CreateRootPart<XBCrossbar>();

            Assert.NotEmpty(part.XBCrossbarAngle.MaterialPartNumber.Value);
            Assert.True(part.XBCrossbarAngle.Bom.ItemMaterialCost.Value > 0);
        }

        [Fact]
        public void CheckCrossbarAngleMaterialNumberandCost()
        {
            var parent = _fixture.CreateRootPart<WeldedCrossbar>();
            parent.OverallLength.SetValue(24);
            var part = parent.Angle;

            Assert.NotEmpty(part.MaterialPartNumber.Value);
            Assert.True(part.Bom.ItemMaterialCost.Value > 0);
        }

        [Fact]
        public void CheckCrossbarEndAngleMaterialNumberandCost()
        {
            var part = _fixture.CreateRootPart<CrossbarEndAngle>();

            Assert.NotEmpty(part.MaterialPartNumber.Value);
            Assert.True(part.Bom.ItemMaterialCost.Value > 0);
        }

        [Fact]
        public void CheckAnglePostProtectorMaterialNumberandCost()
        {
            var part = _fixture.CreateRootPart<AnglePostProtector>();

            Assert.NotEmpty(part.MaterialPartNumber.Value);
            Assert.True(part.Bom.ItemMaterialCost.Value > 0);
        }

        [Fact]
        public void CheckBoltonHeavyHorizontalAngleMaterialNumberandCost()
        {
            var part = _fixture.CreateRootPart<BoltonHeavyHorizontalAngle>();

            Assert.NotEmpty(part.MaterialPartNumber.Value);
            Assert.True(part.Bom.ItemMaterialCost.Value > 0);
        }

        [Fact]
        public void CheckDiagonalBraceMaterialNumberandCost()
        {
            var part = _fixture.CreateRootPart<DiagonalBrace>();

            Assert.NotEmpty(part.MaterialPartNumber.Value);
            Assert.True(part.Bom.ItemMaterialCost.Value > 0);
        }

        [Fact]
        public void CheckFrameTieAngleMaterialNumberandCost()
        {
            var part = _fixture.CreateRootPart<FrameTieAngle>();

            Assert.NotEmpty(part.MaterialPartNumber.Value);
            Assert.True(part.Bom.ItemMaterialCost.Value > 0);
        }

        [Fact]
        public void CheckHeavyHorizontalWeldClipMaterialNumberandCost()
        {
            var part = _fixture.CreateRootPart<HeavyHorizontalWeldClip>();

            Assert.NotEmpty(part.MaterialPartNumber.Value);
            Assert.True(part.Bom.ItemMaterialCost.Value > 0);
        }

        [Fact]
        public void CheckRSFrameTieMaterialNumberandCost()
        {
            var part = _fixture.CreateRootPart<RSFrameTie>();
            part.Length.SetValue(24);

            Assert.NotEmpty(part.RSFrameTieAngle.MaterialPartNumber.Value);
            Assert.True(part.RSFrameTieAngle.Bom.ItemMaterialCost.Value > 0);
        }

        [Fact]
        public void CheckRubRailAngleMaterialNumberandCost()
        {
            var parent = _fixture.CreateRootPart<WeldedRubRail>();
            parent.OverallLength.SetValue(24);
            parent.Type.SetValue("rrxID");
            parent.ChannelSize.SetValue("C3x4.1");
            parent.BeamWidth.SetValue(3);
            var part = parent.FrontRubRailAngle;

            Assert.NotEmpty(part.MaterialPartNumber.Value);
            Assert.True(part.Bom.ItemMaterialCost.Value > 0);
        }

        [Fact]
        public void CheckFrameTieFourHoleEndPlateMaterialNumberandCost()
        {
            var part = _fixture.CreateRootPart<FrameTieFourHoleEndPlate>();

            Assert.NotEmpty(part.MaterialPartNumber.Value);
            Assert.True(part.Bom.ItemMaterialCost.Value > 0);
        }

        [Fact]
        public void CheckShelfPalletStopMaterialNumberandCost()
        {
            var part = _fixture.CreateRootPart<ShelfPalletStop>();

            Assert.NotEmpty(part.ShelfPalletStopMaterial.MaterialPartNumber.Value);
            Assert.True(part.ShelfPalletStopMaterial.Bom.ItemMaterialCost.Value > 0);
        }

        [Fact]
        public void CheckWeldedCrossbarNotchPlateMaterialNumberandCost()
        {
            var part = _fixture.CreateRootPart<WeldedCrossbarNotchPlate>();

            Assert.NotEmpty(part.MaterialPartNumber.Value);
            Assert.True(part.Bom.ItemMaterialCost.Value > 0);
        }

        [Fact]
        public void CheckFootplateMaterialNumberandCost()
        {
            var part = _fixture.CreateRootPart<Footplate>();

            Assert.NotEmpty(part.MaterialPartNumber.Value);
            Assert.True(part.Bom.ItemMaterialCost.Value > 0);
        }

        [Fact]
        public void CheckDoublerCapMaterialNumberandCost()
        {
            var part = _fixture.CreateRootPart<DoublerCap>();

            Assert.NotEmpty(part.MaterialPartNumber.Value);
            Assert.True(part.Bom.ItemMaterialCost.Value > 0);
        }

        [Fact]
        public void CheckTubeCapPlateMaterialNumberandCost()
        {
            var part = _fixture.CreateRootPart<TubeCapPlate>();

            Assert.NotEmpty(part.MaterialPartNumber.Value);
            Assert.True(part.Bom.ItemMaterialCost.Value > 0);
        }

        [Fact]
        public void CheckHeavyHorizontalWeldPlateMaterialNumberandCost()
        {
            var part = _fixture.CreateRootPart<HeavyHorizontalWeldPlate>();

            Assert.NotEmpty(part.MaterialPartNumber.Value);
            Assert.True(part.Bom.ItemMaterialCost.Value > 0);
        }

        [Fact]
        public void CheckDoubledRubRailPlateMaterialNumberandCost()
        {
            var part = _fixture.CreateRootPart<DoubledRubRailPlate>();

            Assert.NotEmpty(part.MaterialPartNumber.Value);
            Assert.True(part.Bom.ItemMaterialCost.Value > 0);
        }

        [Fact]
        public void CheckCrossbarEndPlateMaterialNumberandCost()
        {
            var part = _fixture.CreateRootPart<CrossbarEndPlate>();

            Assert.NotEmpty(part.MaterialPartNumber.Value);
            Assert.True(part.Bom.ItemMaterialCost.Value > 0);
        }

        #endregion

        #endregion

        #region Private Methods

        private double GetTotalLaborByCategory(BomSummaryItem bomItem, string category)
        {
            var weldLaborItems = bomItem.Labor.Where(l => l.Category.ToLower() == category.ToLower());
            var totalWeldHrs = weldLaborItems?.Sum(l => l.Hours) ?? 0.0;

            bomItem.Children.ForEach(c => totalWeldHrs = totalWeldHrs + (GetTotalLaborByCategory(c, category) * c.Qty));

            return totalWeldHrs;
        }

        private Paint GetPaintChild(IEnumerable<Part> children)
        {
            var paintChild = children?.FirstOrDefault(c => c is Paint);
            return paintChild == null ? null : (Paint)paintChild;
        }

        #endregion

    }
}
