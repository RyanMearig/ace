﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ruler.Rules;
using Ruler.Rules.Graphics;
using Xunit;
using StorageConfigurator.TestData;

namespace StorageConfigurator.Rules.Tests
{
    [Collection(nameof(RulesCollection))]
    public class PartTests
    {
        #region Private Fields

        private readonly StorageConfiguratorFixture _fixture;

        #endregion

        #region Constructors

        public PartTests(StorageConfiguratorFixture fixture)
        {
            _fixture = fixture;
        }

        #endregion

        #region Tests

        #region Bay Tests

        [Theory]
        [InlineData(1, 1, 1)]
        [InlineData(1, 2, 2)]
        [InlineData(1, 4, 4)]
        [InlineData(3, 1, 3)]
        [InlineData(3, 2, 6)]
        [InlineData(3, 4, 12)]
        [InlineData(7, 1, 7)]
        [InlineData(7, 2, 14)]
        [InlineData(7, 4, 28)]
        public void CanGetCorrectSelectiveBayLevelQty(int levelQty, int loadsWide, int expectedProdQty)
        {
            var part = _fixture.CreateRootPart<Bay>();
            part.LevelQty.SetValue(levelQty);
            part.LoadsWide.SetValue(loadsWide);

            var activeProd = _fixture.CreateRootPart<Product>();
            part.SelectiveLevel.ToList().ForEach(s => s.ActiveProduct.SetValue(activeProd));

            int prodQty = part.SelectiveLevel.Sum(s => s.FrontProducts.Count());
            int levelsNotOnGround = part.SelectiveLevel.Where(s => !s.ProductOnGround.Value).Count();

            Assert.True(prodQty == expectedProdQty);
            Assert.True(levelsNotOnGround == (levelQty-1));
        }

        [Theory]
        [InlineData(1, 1, 2)]
        [InlineData(1, 2, 4)]
        [InlineData(1, 4, 8)]
        [InlineData(3, 1, 6)]
        [InlineData(3, 2, 12)]
        [InlineData(3, 4, 24)]
        [InlineData(7, 1, 14)]
        [InlineData(7, 2, 28)]
        [InlineData(7, 4, 56)]
        public void CanGetCorrectDoubleDeepBayLevelQty(int levelQty, int loadsWide, int expectedProdQty)
        {
            var part = _fixture.CreateRootPart<Bay>();
            part.LevelQty.SetValue(levelQty);
            part.LoadsWide.SetValue(loadsWide);
            part.IsDoubleDeep.SetValue(true);

            var activeProd = _fixture.CreateRootPart<Product>();
            part.DoubleDeepLevel.ToList().ForEach(s => s.ActiveProduct.SetValue(activeProd));

            int prodQty = part.DoubleDeepLevel.Sum(s => s.FrontProducts.Count()) + part.DoubleDeepLevel.Sum(s => s.RearProducts.Count());
            int levelsNotOnGround = part.DoubleDeepLevel.Where(s => !s.ProductOnGround.Value).Count();

            Assert.True(prodQty == expectedProdQty);
            Assert.True(levelsNotOnGround == (levelQty - 1));
        }

        [Theory]
        [InlineData(1, 48)]
        [InlineData(2, 96)]
        [InlineData(3, 144)]
        [InlineData(4, 192)]
        public void CanGetCorrectBayLengthChangingLoadQty(int loadsWide, float expectedLength)
        {
            var part = _fixture.CreateRootPart<Bay>();
            part.LevelQty.SetValue(2);
            part.LoadsWide.SetValue(loadsWide);

            var activeProd = _fixture.CreateRootPart<Product>();
            part.SelectiveLevel.ToList().ForEach(s => s.ActiveProduct.SetValue(activeProd));

            Assert.True(part.SelectiveLevel[1].BayWidth.Value == expectedLength);
        }

        [Theory]
        [InlineData(4, 8, 192)]
        [InlineData(9, 8, 202)]
        [InlineData(4, 10, 198)]
        [InlineData(14, 13, 227)]
        public void CanGetCorrectBayLoadToFrameChangingSpacings(float expectedLoadToFrameOffset, float loadToLoadOffset, float bayWidth)
        {
            var part = _fixture.CreateRootPart<Bay>();
            part.LevelQty.SetValue(2);
            part.LoadsWide.SetValue(4);
            part.BayWidth.SetValue(bayWidth);

            part.SelectiveLevel.First().LoadToLoadOffset.SetValue(loadToLoadOffset);

            var activeProd = _fixture.CreateRootPart<Product>();
            part.SelectiveLevel.ToList().ForEach(s => s.ActiveProduct.SetValue(activeProd));

            Assert.True(part.SelectiveLevel.First().LoadToFrameOffset.Value == expectedLoadToFrameOffset);
        }

        [Theory]
        [InlineData(6, 102)]
        [InlineData(12, 108)]
        [InlineData(20, 116)]
        public void CanGetCorrectDoubleDeepBayDepthChangingFrameTie(float frameTieLength, float expectedBayDepth)
        {
            var part = _fixture.CreateRootPart<Bay>();
            part.LevelQty.SetValue(2);
            part.IsDoubleDeep.SetValue(true);
            part.FrameTieLength.SetValue(frameTieLength);

            var activeProd = _fixture.CreateRootPart<Product>();
            part.DoubleDeepLevel.ToList().ForEach(s => s.ActiveProduct.SetValue(activeProd));

            float depth = part.FrontFrameDepth.Value + part.RearFrameDepth.Value + part.FrameTieLength.Value;
            Assert.True(part.DoubleDeepLevel.First().OverallDepth.Value == depth);
        }

        [Theory]
        [InlineData("frameFrame", 48, 0, 48, 0)]
        [InlineData("frameFrame", 40, 0, 48, 0)]
        [InlineData("frameFrame", 36, 0, 48, 0)]
        [InlineData("frameFrame", 40, 2, 40, 2)]
        [InlineData("frameFrame", 36, 6, 40, 2)]
        [InlineData("frameFrame", 48, 0, 40, 0)]
        [InlineData("frameFrame", 40, 2, 36, 6)]
        [InlineData("post", 49, 0, 47, 0)]
        [InlineData("post", 36, 13, 47, 0)]
        [InlineData("post", 49, 0, 40, 7)]
        [InlineData("post", 40, 9, 40, 7)]
        public void CanGetCorrectDoubleDeepProductOverhangs(string bayType, 
                                                    float frontFrameDepth, float expectedFrontFrameOverhang, 
                                                    float rearFrameDepth, float expectedRearFrameOverhang)
        {
            //Handle frame frame and frame post
            var part = _fixture.CreateRootPart<Bay>();
            part.LevelQty.SetValue(2);
            part.IsDoubleDeep.SetValue(true);
            part.DoubleDeepType.SetValue(bayType);
            part.FrontFrameDepth.SetValue(frontFrameDepth);
            part.RearFrameDepth.SetValue(rearFrameDepth);

            if (bayType.ToLower() == "frameframe")
            {
                part.FrameTieLength.SetValue(12);
            }

            var activeProd = _fixture.CreateRootPart<Product>();
            part.DoubleDeepLevel.ToList().ForEach(s => s.ActiveProduct.SetValue(activeProd));

            Assert.Equal(expectedFrontFrameOverhang, part.DoubleDeepLevel.First().FrontProductOverhang.Value);
            Assert.Equal(expectedRearFrameOverhang, part.DoubleDeepLevel.First().RearProductOverhang.Value);
        }

        [Theory]
        [InlineData(48, 14, 61.625, 58.5625, 1.5625, 48, 43.8125)]
        [InlineData(44, 24, 67.625, 64.5625, 1.5625, 44, 39.8125)]
        [InlineData(44, 14, 57.625, 54.5625, 1.5625, 44, 39.8125)]
        [InlineData(48, 24, 71.625, 68.5625, 1.5625, 48, 43.8125)]
        public void CanGetCorrectXBWDD1s(float frontFrameDepth, float rearFrameDepth,
                                         float expectedDimA, float expectedDimB, float expectedDimC,
                                         float expectedDimS, float expectedDimX)
        {
            var part = _fixture.CreateRootPart<Bay>();
            part.LevelQty.SetValue(2);
            part.IsDoubleDeep.SetValue(true);
            part.DoubleDeepType.SetValue("post");
            part.FrontFrameDepth.SetValue(frontFrameDepth);
            part.RearFrameDepth.SetValue(rearFrameDepth);

            var activeProd = _fixture.CreateRootPart<Product>();
            part.DoubleDeepLevel.ToList().ForEach(s => s.ActiveProduct.SetValue(activeProd));

            var crossbar = part.DoubleDeepLevel[1].FrontFrameCrossbars.First().WeldedCrossbar;

            Assert.Equal(expectedDimA, crossbar.RoundedOverallLength.Value, 3);
            Assert.Equal(expectedDimB, crossbar.AngleLength.Value, 3);
            Assert.Equal(expectedDimC, crossbar.AngleToChannelOffset.Value, 3);
            Assert.Equal(expectedDimS, crossbar.NotchOneAngleOffset.Value, 3);
            Assert.Equal(expectedDimX, crossbar.EndOfCrossbarAngleToNotch1RightEdge.Value, 3);
            Assert.Equal(8, crossbar.FlatBarCutLength.Value, 3);
        }

        [Theory]
        [InlineData("C4x4.5", 44, 44, 8, 95.625, 92.5625, 1.5625, 44, 39.8125, 51.75, 50.0625, 18.25)]
        [InlineData("C4x4.5", 44, 44, 18, 105.625, 102.5625, 1.5625, 44, 39.8125, 61.75, 60.0625, 28.25)]
        [InlineData("C4x4.5", 44, 34, 18, 95.625, 92.5625, 1.5625, 44, 39.8125, 61.75, 60.0625, 28.25)]
        [InlineData("C5x6.7", 44, 44, 8, 95.5, 92.25, 1.625, 43.9375, 39.6875, 51.6875, 49.9375, 18.25)]
        [InlineData("C5x6.7", 44, 44, 18, 105.5, 102.25, 1.625, 43.9375, 39.6875, 61.6875, 59.9375, 28.25)]
        [InlineData("C5x6.7", 44, 34, 18, 95.5, 92.25, 1.625, 43.9375, 39.6875, 61.6875, 59.9375, 28.25)]
        public void CanGetCorrectXBWDD2s(string channelSize, float frontFrameDepth, float rearFrameDepth, float frameTieLength, 
                                         float expectedDimA, float expectedDimB, float expectedDimC,
                                         float expectedDimS, float expectedDimX,
                                         float expectedDimS2, float expectedDimX2, float expectedFB)
        {
            var part = _fixture.CreateRootPart<Bay>();
            part.LevelQty.SetValue(2);
            part.IsDoubleDeep.SetValue(true);
            part.DoubleDeepType.SetValue("frameFrame");
            part.FrontFrameDepth.SetValue(frontFrameDepth);
            part.RearFrameDepth.SetValue(rearFrameDepth);
            part.FrameTieLength.SetValue(frameTieLength);
            part.DoubleDeepLevel.First().FrontBeamSize.SetValue(channelSize);
            part.DoubleDeepLevel.First().FrontInteriorBeamSize.SetValue(channelSize);

            var activeProd = _fixture.CreateRootPart<Product>();
            part.DoubleDeepLevel.ToList().ForEach(s => s.ActiveProduct.SetValue(activeProd));

            var crossbar = part.DoubleDeepLevel[1].FrontFrameCrossbars.First().WeldedCrossbar;

            Assert.Equal(expectedDimA, crossbar.RoundedOverallLength.Value, 3);
            Assert.Equal(expectedDimB, crossbar.AngleLength.Value, 3);
            Assert.Equal(expectedDimC, crossbar.AngleToChannelOffset.Value, 3);
            Assert.Equal(expectedDimS, crossbar.NotchOneAngleOffset.Value, 3);
            Assert.Equal(expectedDimX, crossbar.EndOfCrossbarAngleToNotch1RightEdge.Value, 3);
            Assert.Equal(expectedDimS2, crossbar.NotchTwoAngleOffset.Value, 3);
            Assert.Equal(expectedDimX2, crossbar.EndOfCrossbarAngleToNotch2RightEdge.Value, 3);
            Assert.Equal(expectedFB, crossbar.FlatBarCutLength.Value, 3);
        }

        #endregion

        #region Frame Line Tests

        [Theory]
        [InlineData(60, 1, 0)]
        [InlineData(84, 2, 0)]
        [InlineData(120, 3, 0)]
        [InlineData(140, 3, 0)]
        [InlineData(168, 4, 0)]
        [InlineData(200, 5, 0)]
        [InlineData(260, 6, 0)]
        [InlineData(300, 7, 0)]
        [InlineData(360, 8, 0)]
        [InlineData(384, 8, 0)]
        [InlineData(420, 9, 0)]
        [InlineData(432, 9, 0)]
        [InlineData(140, 4, 4)]
        public void CanGetCorrectFrameUprightHorizontalBraceQty(float height, int expectedQty, int panelQty)
        {
            var part = _fixture.CreateRootPart<FrameUpright>();
            part.Height.SetValue(height);

            if (panelQty > 0)
                part.PanelQty.SetValue(panelQty);

            Assert.True(part.PanelQty.Value == expectedQty);
        }

        [Theory]
        [InlineData(60, 48, 0)]
        [InlineData(84, 36, 0)]
        [InlineData(120, 36, 0)]
        [InlineData(140, 48, 0)]
        [InlineData(168, 36, 0)]
        [InlineData(200, 20, 0)]
        [InlineData(260, 32, 0)]
        [InlineData(300, 24, 0)]
        [InlineData(360, 36, 0)]
        [InlineData(384, 48, 0)]
        [InlineData(420, 48, 0)]
        [InlineData(432, 48, 0)]
        [InlineData(140, 48, 4)]
        public void CanGetCorrectStandardFrameUprightHorizontalBraceLocation(float height, float expectedLastPanelHeight, int panelQtyOverride)
        {
            var part = _fixture.CreateRootPart<FrameUpright>();
            part.Height.SetValue(height);

            if (panelQtyOverride > 0)
                part.PanelQty.SetValue(panelQtyOverride);

            Assert.Equal(expectedLastPanelHeight, part.HorizontalBrace.LastOrDefault().PanelHeight.Value);
        }

        [Theory]
        [InlineData(60, 2, "S", 0)]
        [InlineData(84, 3, "S", 0)]
        [InlineData(120, 4, "S", 0)]
        [InlineData(140, 4, "S", 0)]
        [InlineData(168, 5, "S", 0)]
        [InlineData(200, 6, "S", 0)]
        [InlineData(260, 7, "S", 0)]
        [InlineData(300, 8, "S", 0)]
        [InlineData(360, 9, "S", 0)]
        [InlineData(384, 9, "S", 0)]
        [InlineData(420, 10, "S", 0)]
        [InlineData(432, 10, "S", 0)]
        [InlineData(60, 2, "H", 0)]
        [InlineData(260, 7, "H", 0)]
        [InlineData(432, 10, "H", 0)]
        [InlineData(60, 2, "BH", 0)]
        [InlineData(84, 3, "BH", 0)]
        [InlineData(120, 4, "BH", 0)]
        [InlineData(140, 4, "BH", 0)]
        [InlineData(168, 5, "BH", 0)]
        [InlineData(200, 6, "BH", 0)]
        [InlineData(260, 7, "BH", 0)]
        [InlineData(300, 8, "BH", 0)]
        [InlineData(360, 9, "BH", 0)]
        [InlineData(384, 9, "BH", 0)]
        [InlineData(420, 10, "BH", 0)]
        [InlineData(432, 10, "BH", 0)]
        [InlineData(60, 2, "BTH", 0)]
        [InlineData(260, 7, "BTH", 0)]
        [InlineData(432, 10, "BTH", 0)]
        [InlineData(60, 2, "D", 0)]
        [InlineData(260, 7, "D", 0)]
        [InlineData(432, 10, "D", 0)]
        [InlineData(60, 2, "T", 0)]
        [InlineData(84, 3, "T", 0)]
        [InlineData(120, 4, "T", 0)]
        [InlineData(140, 4, "T", 0)]
        [InlineData(168, 5, "T", 0)]
        [InlineData(200, 6, "T", 0)]
        [InlineData(260, 7, "T", 0)]
        [InlineData(300, 8, "T", 0)]
        [InlineData(360, 9, "T", 0)]
        [InlineData(384, 9, "T", 0)]
        [InlineData(420, 10, "T", 0)]
        [InlineData(432, 10, "T", 0)]
        [InlineData(60, 2, "LS", 0)]
        [InlineData(84, 3, "LS", 0)]
        [InlineData(120, 4, "LS", 0)]
        [InlineData(140, 4, "LS", 0)]
        [InlineData(168, 5, "LS", 0)]
        [InlineData(200, 6, "LS", 0)]
        [InlineData(260, 7, "LS", 0)]
        [InlineData(300, 8, "LS", 0)]
        [InlineData(360, 9, "LS", 0)]
        [InlineData(384, 9, "LS", 0)]
        [InlineData(420, 10, "LS", 0)]
        [InlineData(432, 10, "LS", 0)]
        [InlineData(84, 4, "L", 0)]
        [InlineData(120, 4, "L", 0)]
        [InlineData(140, 5, "L", 0)]
        [InlineData(168, 5, "L", 0)]
        [InlineData(200, 6, "L", 0)]
        [InlineData(260, 7, "L", 0)]
        [InlineData(300, 8, "L", 0)]
        [InlineData(360, 9, "L", 0)]
        [InlineData(384, 10, "L", 0)]
        [InlineData(420, 11, "L", 0)]
        [InlineData(432, 11, "L", 0)]
        [InlineData(84, 3, "B", 27)]
        [InlineData(84, 3, "B", 31)]
        [InlineData(120, 4, "B", 27)]
        [InlineData(120, 4, "B", 31)]
        [InlineData(120, 4, "B", 35)]
        [InlineData(120, 4, "B", 39)]
        [InlineData(120, 4, "B", 43)]
        [InlineData(120, 3, "B", 47)]
        [InlineData(120, 4, "B", 51)]
        [InlineData(120, 4, "B", 55)]
        [InlineData(120, 4, "B", 59)]
        [InlineData(120, 4, "B", 63)]
        [InlineData(120, 4, "B", 67)]
        [InlineData(120, 4, "B", 71)]
        [InlineData(140, 4, "B", 27)]
        [InlineData(140, 4, "B", 31)]
        [InlineData(140, 4, "B", 35)]
        [InlineData(140, 4, "B", 39)]
        [InlineData(140, 4, "B", 43)]
        [InlineData(140, 4, "B", 47)]
        [InlineData(140, 5, "B", 51)]
        [InlineData(140, 5, "B", 55)]
        [InlineData(140, 5, "B", 59)]
        [InlineData(140, 5, "B", 63)]
        [InlineData(140, 4, "B", 67)]
        [InlineData(140, 4, "B", 71)]
        [InlineData(140, 4, "B", 75)]
        [InlineData(140, 4, "B", 79)]
        [InlineData(140, 4, "B", 83)]
        [InlineData(140, 5, "B", 87)]
        [InlineData(140, 5, "B", 91)]
        [InlineData(168, 5, "B", 27)]
        [InlineData(168, 5, "B", 31)]
        [InlineData(168, 5, "B", 35)]
        [InlineData(168, 5, "B", 39)]
        [InlineData(168, 5, "B", 43)]
        [InlineData(168, 4, "B", 47)]
        [InlineData(168, 5, "B", 51)]
        [InlineData(168, 5, "B", 55)]
        [InlineData(168, 5, "B", 59)]
        [InlineData(168, 5, "B", 63)]
        [InlineData(168, 5, "B", 67)]
        [InlineData(168, 5, "B", 71)]
        [InlineData(168, 5, "B", 75)]
        [InlineData(168, 5, "B", 79)]
        [InlineData(168, 5, "B", 83)]
        [InlineData(168, 6, "B", 87)]
        [InlineData(168, 6, "B", 91)]
        [InlineData(168, 5, "B", 95)]
        [InlineData(200, 6, "B", 27)]
        [InlineData(200, 5, "B", 31)]
        [InlineData(200, 5, "B", 35)]
        [InlineData(200, 5, "B", 39)]
        [InlineData(200, 5, "B", 43)]
        [InlineData(200, 5, "B", 47)]
        [InlineData(200, 6, "B", 51)]
        [InlineData(200, 6, "B", 55)]
        [InlineData(200, 6, "B", 59)]
        [InlineData(200, 6, "B", 63)]
        [InlineData(200, 6, "B", 67)]
        [InlineData(200, 6, "B", 71)]
        [InlineData(200, 6, "B", 75)]
        [InlineData(200, 5, "B", 79)]
        [InlineData(200, 5, "B", 83)]
        [InlineData(200, 6, "B", 87)]
        [InlineData(200, 6, "B", 91)]
        [InlineData(200, 6, "B", 95)]
        [InlineData(360, 9, "B", 27)]
        [InlineData(360, 9, "B", 31)]
        [InlineData(360, 9, "B", 35)]
        [InlineData(360, 9, "B", 39)]
        [InlineData(360, 9, "B", 43)]
        [InlineData(360, 8, "B", 47)]
        [InlineData(360, 9, "B", 51)]
        [InlineData(360, 9, "B", 55)]
        [InlineData(360, 9, "B", 59)]
        [InlineData(360, 9, "B", 63)]
        [InlineData(360, 9, "B", 67)]
        [InlineData(360, 9, "B", 71)]
        [InlineData(360, 9, "B", 75)]
        [InlineData(360, 9, "B", 79)]
        [InlineData(360, 9, "B", 83)]
        [InlineData(360, 10, "B", 87)]
        [InlineData(360, 10, "B", 91)]
        [InlineData(360, 9, "B", 95)]
        [InlineData(420, 10, "B", 27)]
        [InlineData(420, 10, "B", 31)]
        [InlineData(420, 10, "B", 35)]
        [InlineData(420, 10, "B", 39)]
        [InlineData(420, 10, "B", 43)]
        [InlineData(420, 10, "B", 47)]
        [InlineData(420, 11, "B", 51)]
        [InlineData(420, 11, "B", 55)]
        [InlineData(420, 10, "B", 59)]
        [InlineData(420, 10, "B", 63)]
        [InlineData(420, 10, "B", 67)]
        [InlineData(420, 10, "B", 71)]
        [InlineData(420, 10, "B", 75)]
        [InlineData(420, 10, "B", 79)]
        [InlineData(420, 10, "B", 83)]
        [InlineData(420, 11, "B", 87)]
        [InlineData(420, 11, "B", 91)]
        [InlineData(420, 11, "B", 95)]
        public void CanGetFrameLineHorizontalBraceQty(float height, int expectedQty, string uprightType, float slantElev)
        {
            var part = _fixture.CreateRootPart<SelectiveFrameType>();
            part.Height.SetValue(height);
            part.UprightType.SetValue(uprightType);

            if (slantElev > 0)
                part.SlantElev.SetValue(slantElev);

            Assert.Equal(expectedQty, part.Frame.Frame.DefaultPanelHeights.Value.Count());
        }

        [Theory]
        [InlineData(27, 6, 24, 0, 0)]
        [InlineData(31, 6, 28, 0, 0)]
        [InlineData(35, 6, 32, 0, 0)]
        [InlineData(39, 6, 36, 0, 0)]
        [InlineData(43, 6, 40, 0, 0)]
        [InlineData(47, 6, 44, 0, 0)]
        [InlineData(51, 6, 24, 24, 0)]
        [InlineData(55, 6, 24, 28, 0)]
        [InlineData(59, 6, 24, 32, 0)]
        [InlineData(63, 6, 24, 36, 0)]
        [InlineData(67, 6, 24, 40, 0)]
        [InlineData(71, 6, 24, 44, 0)]
        [InlineData(75, 6, 36, 36, 0)]
        [InlineData(79, 6, 36, 40, 0)]
        [InlineData(83, 6, 36, 44, 0)]
        [InlineData(87, 6, 24, 24, 36)]
        [InlineData(91, 6, 24, 24, 40)]
        [InlineData(95, 6, 24, 24, 44)]
        public void CanGetBentLegFrameLineHorizontalSpacing(float slantElev, float panelHeight1, float panelHeight2, float panelHeight3, float panelHeight4)
        {
            var part = _fixture.CreateRootPart<SelectiveFrameType>();
            part.Height.SetValue(220);
            part.UprightType.SetValue("B");
            part.SlantElev.SetValue(slantElev);

            List<float> panelHeights = part.Frame.Frame.DefaultPanelHeights.Value;

            Assert.Equal(panelHeight1, panelHeights[0]);
            Assert.Equal(panelHeight2, panelHeights[1]);

            if (panelHeight3 > 0)
                Assert.Equal(panelHeight3, panelHeights[2]);

            if (panelHeight4 > 0)
                Assert.Equal(panelHeight4, panelHeights[3]);
        }

        [Theory]
        [InlineData(48, false, false, 47.736)]
        [InlineData(48, true, false, 45.108)]
        [InlineData(48, false, true, 45.108)]
        [InlineData(48, true, true, 42.4800034)]
        [InlineData(60, false, false, 59.736)]
        [InlineData(60, true, false, 57.108)]
        [InlineData(60, false, true, 57.108)]
        [InlineData(60, true, true, 54.480003)]
        public void CanGetStandardHorizontalBraceLengths(float frameDepth, bool hasFrontDoubler, bool hasRearDoubler, float expectedLength)
        {
            var part = _fixture.CreateRootPart<SelectiveFrameType>();
            part.Frame.Frame.Depth.SetValue(frameDepth);
            part.Frame.Frame.HasFrontDoubler.SetValue(hasFrontDoubler);
            part.Frame.Frame.HasRearDoubler.SetValue(hasRearDoubler);

            Assert.Equal(expectedLength, part.Frame.Frame.HorizontalBrace.First().Length.Value, 4);
        }

        [Theory]
        [InlineData(28, false, false, 42.5, 42.5, 42.5)]
        [InlineData(28, true, false, 41.8125, 41.8125, 42.5)]
        [InlineData(28, false, true, 41.8125, 41.8125, 42.5625)]
        [InlineData(28, true, true, 41.125, 41.125, 42.5625)]
        [InlineData(44, false, false, 53.0625, 53.0625, 53.0625)]
        [InlineData(44, true, false, 52.0625, 52.0625, 53.0625)]
        [InlineData(44, false, true, 52.0625, 52.0625, 53.0625)]
        [InlineData(44, true, true, 51.0625, 51.0625, 53.0625)]
        [InlineData(58, false, false, 64.25, 64.25, 64.25)]
        [InlineData(58, true, false, 63.0625, 63.125, 64.25)]
        [InlineData(58, false, true, 63.0625, 63.0625, 64.25)]
        [InlineData(58, true, true, 61.9375, 61.9375, 64.25)]

        public void CanGetStandardDiagonalLengths(float frameDepth, bool hasFrontDoubler, bool hasRearDoubler, 
                                                float firstExpectedLength, float secondExpectedLength, float thirdExpectedLength)
        {
            var part = _fixture.CreateRootPart<SelectiveFrameType>();
            part.Height.SetValue(120);
            part.Frame.Frame.Depth.SetValue(frameDepth);
            part.Frame.Frame.HasFrontDoubler.SetValue(hasFrontDoubler);
            if (hasFrontDoubler)
                part.Frame.Frame.FrontDoublerHeight.SetValue(60);

            part.Frame.Frame.HasRearDoubler.SetValue(hasRearDoubler);

            Assert.True(part.Frame.Frame.DiagonalBrace[0].CutLength.Value == firstExpectedLength);
            Assert.True(part.Frame.Frame.DiagonalBrace[1].CutLength.Value == secondExpectedLength);
            Assert.True(part.Frame.Frame.DiagonalBrace[2].CutLength.Value == thirdExpectedLength);
        }

        [Theory]
        [InlineData(28, true, true, 39.4375, 41.125, 42.5625)]
        [InlineData(28, true, false, 40.125, 41.8125, 42.5)]
        [InlineData(28, false, true, 39.9375, 41.8125, 42.5625)]
        [InlineData(28, false, false, 40.5625, 42.5, 42.5)]
        [InlineData(44, true, true, 49, 51.0625, 53.0625)]
        [InlineData(44, true, false, 49.9375, 52.0625, 53.0625)]
        [InlineData(44, false, true, 49.6875, 52.0625, 53.0625)]
        [InlineData(44, false, false, 50.6875, 53.0625, 53.0625)]
        [InlineData(58, true, true, 59.8125, 61.9375, 64.25)]
        [InlineData(58, true, false, 60.9375, 63.125, 64.25)]
        [InlineData(58, false, true, 60.625, 63.0625, 64.25)]
        [InlineData(58, false, false, 61.75, 64.25, 64.25)]

        public void CanGetHeavyDiagonalLengths(float frameDepth, bool hasFrontDoubler, bool hasRearDoubler,
                                                float firstExpectedLength, float secondExpectedLength, float thirdExpectedLength)
        {
            var part = _fixture.CreateRootPart<SelectiveFrameType>();
            part.UprightType.SetValue("H");
            part.Height.SetValue(120);
            part.Frame.Frame.Depth.SetValue(frameDepth);
            part.Frame.Frame.HasFrontDoubler.SetValue(hasFrontDoubler);
            if (hasFrontDoubler)
                part.Frame.Frame.FrontDoublerHeight.SetValue(60);

            part.Frame.Frame.HasRearDoubler.SetValue(hasRearDoubler);

            Assert.True(part.Frame.Frame.DiagonalBrace[0].CutLength.Value == firstExpectedLength);
            Assert.True(part.Frame.Frame.DiagonalBrace[1].CutLength.Value == secondExpectedLength);
            Assert.True(part.Frame.Frame.DiagonalBrace[2].CutLength.Value == thirdExpectedLength);
        }

        [Theory]
        [InlineData("S", 53.0625, 53.0625, 53.0625)]
        [InlineData("H", 50.6875, 53.0625, 53.0625)]
        [InlineData("BH", 50.5, 53.0625, 53.0625)]
        [InlineData("BTH", 50.6875, 50.6875, 53.0625)]
        [InlineData("D", 49, 51.0625, 52.0625)]
        [InlineData("T", 48.375, 51.0625, 52.0625)]
        [InlineData("LS", 40.0625, 52.125, 57.25)]
        [InlineData("L", 34.1875, 40.4375, 60.5625)]
        [InlineData("B", 38.9375, 60.875, 53.0625)]
        public void VerifyDiagonalsPerUprightType(string uprightType, float firstExpectedLength,
                                                 float secondExpectedLength, float thirdExpectedLength)
        {
            var part = _fixture.CreateRootPart<SelectiveFrameType>();
            part.UprightType.SetValue(uprightType);

            if (part.Frame.Frame.IsSlantType.Value)
            {
                part.Frame.Frame.HasFrontDoubler.SetValue(false);
            }

            part.Height.SetValue(120);
            part.Frame.Frame.Depth.SetValue(44);

            Assert.Equal(firstExpectedLength, part.Frame.Frame.DiagonalBrace[0].CutLength.Value);
            Assert.Equal(secondExpectedLength, part.Frame.Frame.DiagonalBrace[1].CutLength.Value);
            Assert.Equal(thirdExpectedLength, part.Frame.Frame.DiagonalBrace[2].CutLength.Value);
        }

        [Theory]
        [InlineData(18, 18, 2, 24.418)]
        [InlineData(18, 44, 2, 46.788)]
        [InlineData(18, 60, 2, 62.043)]
        [InlineData(36, 18, 2, 39.446)]
        [InlineData(36, 44, 2, 55.841)]
        [InlineData(36, 60, 2, 69.062)]
        [InlineData(48, 18, 2, 50.616)]
        [InlineData(48, 44, 2, 64.111)]
        [InlineData(48, 60, 2, 75.817)]
        public void CanGetCorrectDiagonalLength(float overallHeight, float overallWidth, float angleHeight, float expectedValue)
        {
            var part = _fixture.CreateRootPart<SelectiveFrameType>();
            FrameUpright upright = part.Frame.Frame;

            float retVal = upright.GetDiagonalLength(overallHeight, overallWidth, angleHeight);

            Assert.Equal(expectedValue, retVal, 3);
        }

        [Theory]
        [InlineData(18, 24.418, 47.49)]
        [InlineData(18, 46.788, 22.626)]
        [InlineData(18, 62.043, 16.865)]
        [InlineData(36, 39.446, 65.873)]
        [InlineData(36, 55.841, 40.142)]
        [InlineData(36, 69.062, 31.418)]
        [InlineData(48, 50.616, 71.499)]
        [InlineData(48, 64.111, 48.478)]
        [InlineData(48, 75.817, 39.279)]
        public void CanGetCorrectDiagonalRotation(float overallHeight, float diagonalLength, float expectedValue)
        {
            var part = _fixture.CreateRootPart<SelectiveFrameType>();
            FrameUpright upright = part.Frame.Frame;

            float retVal = upright.GetDiagonalRotation(overallHeight, diagonalLength);

            Assert.Equal(expectedValue, Math.Abs(retVal), 3);
        }

        [Theory]
        [InlineData("S", 6)]
        [InlineData("H", 6)]
        [InlineData("BH", 10)]
        [InlineData("BTH", 6)]
        [InlineData("D", 6)]
        [InlineData("T", 10)]
        [InlineData("LS", 6)]
        [InlineData("L", 6)]
        [InlineData("B", 6)]
        public void CanGetSelectiveBottomHorizontalBraceOffset(string uprightType, float expectedOffset)
        {
            var part = _fixture.CreateRootPart<SelectiveFrameType>();
            part.UprightType.SetValue(uprightType);

            Assert.True(part.Frame.Frame.BottomHorizontalOffset.Value == expectedOffset);
        }

        [Theory]
        [InlineData("LS", 36, 12)]
        [InlineData("L", 56, 12)]
        public void CanGetSlantLegElevationandInset(string uprightType, float expectedElevation, float expectedInset)
        {
            var part = _fixture.CreateRootPart<SelectiveFrameType>();
            part.UprightType.SetValue(uprightType);

            Assert.True(part.SlantElev.Value == expectedElevation);
            Assert.True(part.SlantInset.Value == expectedInset);
        }

        [Theory]
        [InlineData("S", "frameFrame", 6)]
        [InlineData("H", "frameFrame", 6)]
        [InlineData("BH", "frameFrame", 10)]
        [InlineData("BTH", "frameFrame", 6)]
        [InlineData("D", "frameFrame", 6)]
        [InlineData("T", "frameFrame", 10)]
        [InlineData("LS", "frameFrame", 6)]
        [InlineData("L", "frameFrame", 6)]
        [InlineData("B", "frameFrame", 6)]
        [InlineData("S", "post", 6)]
        [InlineData("H", "post", 6)]
        [InlineData("BH", "post", 10)]
        [InlineData("BTH", "post", 6)]
        [InlineData("D", "post", 6)]
        [InlineData("T", "post", 10)]
        [InlineData("LS", "post", 6)]
        [InlineData("L", "post", 6)]
        [InlineData("B", "post", 6)]
        public void CanGetDoubleDeepBottomHorizontalBraceOffset(string uprightType, string doubleDeepType, float expectedOffset)
        {
            var part = _fixture.CreateRootPart<DoubleDeepFrameType>();
            part.UprightType.SetValue(uprightType);
            part.DoubleDeepType.SetValue(doubleDeepType);

            Assert.True(part.FrontFrame.Frame.BottomHorizontalOffset.Value == expectedOffset);
            Assert.True(part.RearFrame.Frame.BottomHorizontalOffset.Value == expectedOffset);
        }

        [Theory]
        [InlineData(false, false, 45.24)]
        [InlineData(true, false, 43.86)]
        [InlineData(false, true, 43.86)]
        [InlineData(true, true, 42.48)]
        public void HeavyTypeBottomHorizontalTest(bool hasFrontDoubler, bool hasRearDoubler, float expectedHorizLength)
        {
            var part = _fixture.CreateRootPart<SelectiveFrameType>();
            part.UprightType.SetValue("H");
            part.Frame.Frame.Depth.SetValue(48);
            part.Frame.Frame.HasFrontDoubler.SetValue(hasFrontDoubler);
            part.Frame.Frame.HasRearDoubler.SetValue(hasRearDoubler);

            Assert.True(part.Frame.Frame.HeavyHorizontalBraceQty.Value == 1);
            Assert.True(part.Frame.Frame.HeavyHorizontalWeldClip.Count() == 1);

            if (hasFrontDoubler)
                Assert.True(part.Frame.Frame.FrontHeavyHorizWeldPlate.Count() == 0);
            else
                Assert.True(part.Frame.Frame.FrontHeavyHorizWeldPlate.Count() == 1);

            if (hasRearDoubler)
                Assert.True(part.Frame.Frame.RearHeavyHorizWeldPlate.Count() == 0);
            else
                Assert.True(part.Frame.Frame.RearHeavyHorizWeldPlate.Count() == 1);

            Assert.True(part.Frame.Frame.HeavyHorizontalBrace.First().Length.Value == expectedHorizLength);
        }

        [Fact]
        public void BoltOnHeavyFrameTest()
        {
            var part = _fixture.CreateRootPart<SelectiveFrameType>();
            part.UprightType.SetValue("BH");
            part.Frame.Frame.Depth.SetValue(48);

            Assert.True(part.Frame.Frame.FrontUprightColumnAssembly.BoltOnHvyHorizontalAngle.IsActive.Value);
            Assert.True(part.Frame.Frame.RearUprightColumnAssembly.BoltOnHvyHorizontalAngle.IsActive.Value);
            Assert.True(part.Frame.BoltOnHeavyHorizontalBrace.IsActive.Value);
            Assert.True(part.Frame.BoltOnHeavyHorizontalBrace.Length.Value == 45.24f);
        }

        [Fact]
        public void BottomTwoHeavyFrameTest()
        {
            var part = _fixture.CreateRootPart<SelectiveFrameType>();
            part.UprightType.SetValue("BTH");

            Assert.True(part.Frame.Frame.HeavyHorizontalBraceQty.Value == 2);
            Assert.True(part.Frame.Frame.HeavyHorizontalWeldClip.Count() == 2);
        }

        [Fact]
        public void DoubleHeavyFrameTest()
        {
            var part = _fixture.CreateRootPart<SelectiveFrameType>();
            part.UprightType.SetValue("D");

            Assert.True(part.Frame.Frame.HeavyHorizontalBraceQty.Value == 1);
            Assert.True(part.Frame.Frame.HeavyHorizontalBraceDoubler.IsActive.Value);
        }

        [Fact]
        public void TippmannFrameTest()
        {
            var part = _fixture.CreateRootPart<SelectiveFrameType>();
            part.UprightType.SetValue("T");

            Assert.True(part.Frame.Frame.TopTippmannHorizontal.IsActive.Value);
            Assert.True(part.Frame.Frame.BtmTippmannHorizontal.IsActive.Value);
        }

        [Fact]
        public void SlantLeg36FrameTest()
        {
            var part = _fixture.CreateRootPart<SelectiveFrameType>();
            part.UprightType.SetValue("LS");
            part.Frame.Frame.Depth.SetValue(48);

            Assert.True(part.Frame.Frame.HeavyHorizontalBrace.First().IsActive.Value);
            Assert.True(part.Frame.Frame.HeavyHorizontalBrace.First().Length.Value == 33.5575f);
            Assert.True(part.Frame.Frame.HeavyHorizontalWeldClip.Count() == 1);
            Assert.True(part.Frame.Frame.FrontHeavyHorizWeldPlate.Qty.Value == 0);
            Assert.True(part.Frame.Frame.RearHeavyHorizWeldPlate.Qty.Value == 1);
            Assert.True(part.Frame.Frame.FrontUprightColumnAssembly.SlantLegColumnTube.IsActive.Value);
            Assert.True(part.Frame.Frame.FrontUprightColumnAssembly.TubeCapPlate.IsActive.Value);
        }

        [Theory]
        [InlineData("LS")]
        [InlineData("L")]
        public void SlantLegFrameTubeCapTest(string uprightType)
        {
            var part = _fixture.CreateRootPart<SelectiveFrameType>();
            part.UprightType.SetValue(uprightType);

            Assert.True(part.Frame.Frame.FrontUprightColumnAssembly.TubeCapPlate.IsActive.Value);
            part.Frame.Frame.HasFrontDoubler.SetValue(false);
            Assert.Equal(3.5f, part.Frame.Frame.FrontUprightColumnAssembly.TubeCapPlate.Width.Value);

            part.Frame.Frame.HasFrontDoubler.SetValue(true);
            Assert.Equal(4.5f, part.Frame.Frame.FrontUprightColumnAssembly.TubeCapPlate.Width.Value);
        }

        [Fact]
        public void SlantLeg56FrameTest()
        {
            var part = _fixture.CreateRootPart<SelectiveFrameType>();
            part.UprightType.SetValue("L");
            part.Frame.Frame.Depth.SetValue(48);

            Assert.True(part.Frame.Frame.HeavyHorizontalBrace.First().IsActive.Value);
            Assert.Equal(32.8075f, part.Frame.Frame.HeavyHorizontalBrace.First().Length.Value, 4);
            Assert.Equal(34.0555f, part.Frame.Frame.HorizontalBrace.First().Length.Value, 4);
            Assert.True(part.Frame.Frame.HeavyHorizontalWeldClip.Count() == 1);
            Assert.True(part.Frame.Frame.LUprightAngleWeldClip.IsActive.Value);
            Assert.Equal(0, part.Frame.Frame.FrontHeavyHorizWeldPlate.Qty.Value);
            Assert.Equal(1, part.Frame.Frame.RearHeavyHorizWeldPlate.Qty.Value);
            Assert.True(part.Frame.Frame.FrontUprightColumnAssembly.SlantLegColumnTube.IsActive.Value);
            Assert.True(part.Frame.Frame.FrontUprightColumnAssembly.TubeCapPlate.IsActive.Value);
        }

        [Theory]
        [InlineData(27, 24.567171, 38.774284, 45.108, 0, 0)]
        [InlineData(31, 20.409883, 38.232094, 45.108, 0, 0)]
        [InlineData(35, 17.41797, 37.86, 45.108, 0, 0)]
        [InlineData(39, 22.135492, 34.45322, 45.108, 0, 0)]
        [InlineData(43, 19.70798, 34.14358, 45.108, 0, 0)]
        [InlineData(47, 17.74467, 33.899998, 45.108, 0, 0)]
        [InlineData(51, 22.275898, 29.471445, 40.397156, 45.108, 0)]
        [InlineData(55, 20.487015, 29.241867, 39.31679, 45.108, 0)]
        [InlineData(59, 18.954233, 29.049393, 38.41103, 45.108, 0)]
        [InlineData(63, 20.497341, 26.243176, 36.322952, 45.108, 0)]
        [InlineData(67, 19.179007, 26.07739, 35.54278, 45.108, 0)]
        [InlineData(71, 18.014692, 25.933168, 34.864098, 45.108, 0)]
        [InlineData(75, 20.890825, 21.293205, 36.138535, 45.108, 0)]
        [InlineData(79, 19.784279, 21.153164, 35.21591, 45.108, 0)]
        [InlineData(83, 18.78502, 21.028366, 34.393715, 45.108, 0)]
        [InlineData(87, 20.515522, 17.245481, 27.333803, 36.314445, 45.108)]
        [InlineData(91, 19.586994, 17.128405, 26.782846, 35.322723, 45.108)]
        [InlineData(95, 18.735975, 17.02228, 26.283438, 34.42379, 45.108)]
        public void BentLegFrameTests(float slantElev, float slantAngle, float horizLength1, float horizLength2, float horizLength3, float horizLength4)
        {
            var part = _fixture.CreateRootPart<SelectiveFrameType>();
            part.Height.SetValue(220);
            part.UprightType.SetValue("B");
            part.SlantElev.SetValue(slantElev);
            part.Frame.Frame.Depth.SetValue(48);

            List<HorizontalBrace> activeHorizBraces = part.Frame.Frame.HorizontalBrace.Where(b => b.IsActive.Value).ToList();

            Assert.Equal(slantAngle, part.Frame.Frame.SlantAngle.Value);

            Assert.Equal(horizLength1, part.Frame.Frame.HeavyHorizontalBrace.First().Length.Value);
            Assert.Equal(horizLength2, activeHorizBraces[0].Length.Value);

            if (horizLength3 > 0)
                Assert.Equal(horizLength3, activeHorizBraces[1].Length.Value);

            if (horizLength4 > 0)
                Assert.Equal(horizLength4, activeHorizBraces[2].Length.Value);
        }

        [Fact]
        public void BentLegFrameInsetTest()
        {
            var part = _fixture.CreateRootPart<SelectiveFrameType>();
            part.UprightType.SetValue("B");

            Assert.True(part.HasFrontDoubler.Value);
            Assert.Equal(24.5671711f, part.Frame.Frame.SlantAngle.Value);

            part.SlantInset.SetValue(6);
            Assert.Equal(18.9246445f, part.Frame.Frame.SlantAngle.Value);
        }

        #endregion

        [Fact]
        public void CanGetCrossbarAngleFromLevel()
        {
            var bay = _fixture.CreateRootPart<Bay>();
            var product = _fixture.CreateRootPart<Product>();
            var level = bay.SelectiveLevel.Last();
            level.ActiveProduct.SetValue(product);
            level.AngleType.SetValue(@"2 x 2 x 1/8");

            var crossbar = level.Crossbars.First();

            Assert.Equal(@"2 x 2 x 1/8", crossbar.AngleType.Value);

        }

        [Fact]
        public void CanGetCrossbarAngleFromDDLevel()
        {
            var bay = _fixture.CreateRootPart<Bay>();
            bay.IsDoubleDeep.SetValue(true);
            var product = _fixture.CreateRootPart<Product>();
            var level = bay.DoubleDeepLevel.Last();
            level.ActiveProduct.SetValue(product);
            level.AngleType.SetValue(@"2 x 2 x 1/8");

            var crossbar = level.FrontFrameCrossbars.First();

            Assert.Equal(@"2 x 2 x 1/8", crossbar.AngleType.Value);

        }

        [Fact]
        public void CanEvaluateMyRule()
        {
            var part = _fixture.CreateRootPart<MyPart>();
            Assert.Equal(Colors.Blue, part.Color.Value);
        }

        [Fact]
        public void CanNameProduct()
        {
            var part = _fixture.CreateRootPart<StorageSystem>();
            var firstProduct = part.AddChild(new Ruler.Rules.ChildOptions<Product>());

            Assert.Equal("A", firstProduct.ProductName.Value);

            var secondProduct = part.AddChild(new Ruler.Rules.ChildOptions<Product>());
            var thirdProduct = part.AddChild(new Ruler.Rules.ChildOptions<Product>());

            Assert.Equal("C", thirdProduct.ProductName.Value);
            part.DeleteChild(secondProduct.Name);


            Assert.Equal("C", thirdProduct.ProductName.Value);
        }

        [Fact]
        public void CanGetBomDescription_LoadBeam()
        {

            var part = _fixture.CreateRootPart<ShelfLoadBeam>();

            var expDesc = "Load Beam, selective, 8\" clips" +
                "|C4x4.5x95.252\" long, standard, .438\" drop" +
                "|0.5625\" holes" +
                "|BLACK";
            var fullDescription = part.BOMDescription.Value;

            Assert.Equal(expDesc, fullDescription);
            Assert.False(string.IsNullOrWhiteSpace(part.BOMDescription1.Value));
            Assert.False(string.IsNullOrWhiteSpace(part.BOMDescription2.Value));
            Assert.False(string.IsNullOrWhiteSpace(part.BOMDescription3.Value));
            Assert.False(string.IsNullOrWhiteSpace(part.BOMDescription3.Value));
            Assert.True(string.IsNullOrWhiteSpace(part.BOMDescription5.Value));
            Assert.True(string.IsNullOrWhiteSpace(part.BOMDescription6.Value));
            Assert.True(string.IsNullOrWhiteSpace(part.BOMDescription7.Value));
            Assert.True(string.IsNullOrWhiteSpace(part.BOMDescription8.Value));
            Assert.True(string.IsNullOrWhiteSpace(part.BOMDescription9.Value));
            Assert.True(string.IsNullOrWhiteSpace(part.BOMDescription10.Value));
            Assert.True(string.IsNullOrWhiteSpace(part.BOMDescription11.Value));
        }

        [Fact]
        public void CanGetBomDescription_UprightNoPostProtectors()
        {
            var part = _fixture.CreateRootPart<FrameUpright>();

            var expDesc = "USA8 (360,44,NO,STD)" +
                "|Upright C3x3.5#, 44 x 360 high" +
                "|STD BH" +
                "|1.5x1/8 diag bracing, 1.5x1/8 horz bracing" +
                "|panels 36, 36, 48, 48, 48, 48, 48, 36" +
                "|Front FP 3/8X4 x 7,FP Hole A 5/8, Hole B NONE" +
                "|Rear FP 3/8X4 x 7,FP Hole A 5/8, Hole B NONE" +
                "|Front FP FP1K-A, Rear FP FP1K-A, BLACK" +
                "|Upright punched on 4\" Centers";
            var fullDescription = part.BOMDescription.Value;

            Assert.Equal(expDesc, fullDescription);
            Assert.False(string.IsNullOrWhiteSpace(part.BOMDescription1.Value));
            Assert.False(string.IsNullOrWhiteSpace(part.BOMDescription2.Value));
            Assert.False(string.IsNullOrWhiteSpace(part.BOMDescription3.Value));
            Assert.False(string.IsNullOrWhiteSpace(part.BOMDescription4.Value));
            Assert.False(string.IsNullOrWhiteSpace(part.BOMDescription5.Value));
            Assert.False(string.IsNullOrWhiteSpace(part.BOMDescription6.Value));
            Assert.False(string.IsNullOrWhiteSpace(part.BOMDescription7.Value));
            Assert.False(string.IsNullOrWhiteSpace(part.BOMDescription8.Value));
            Assert.False(string.IsNullOrWhiteSpace(part.BOMDescription9.Value));
            Assert.True(string.IsNullOrWhiteSpace(part.BOMDescription10.Value));
            Assert.True(string.IsNullOrWhiteSpace(part.BOMDescription11.Value));
        }

        [Fact]
        public void CanGetBomDescription_UprightWithPostProtectors()
        {
            var part = _fixture.CreateRootPart<FrameUpright>();
            part.FrontPostProtector.SetValue("angle");
            part.FrontPostProtectorHeight.SetValue(15.125f);
            part.RearPostProtector.SetValue("bullnose");
            part.RearPostProtectorHeight.SetValue(10f);


            var expDesc = "USA8 (360,44,AP,STD)" +
                "|Upright C3x3.5#, 44 x 360 high" +
                "|STD BH, 15.13\" AP Front PP, 10\" Rear PP" +
                "|1.5x1/8 diag bracing, 1.5x1/8 horz bracing" +
                "|panels 36, 36, 48, 48, 48, 48, 48, 36" +
                "|Front FP 3/8X4 x 7,FP Hole A 5/8, Hole B NONE" +
                "|Rear FP 3/8X4 x 7,FP Hole A 5/8, Hole B 7/8" +
                "|Front FP FP1K-C, Rear FP FP2K-A, BLACK" +
                "|Upright punched on 4\" Centers";
            var fullDescription = part.BOMDescription.Value;

            Assert.Equal(expDesc, fullDescription);
            Assert.False(string.IsNullOrWhiteSpace(part.BOMDescription1.Value));
            Assert.False(string.IsNullOrWhiteSpace(part.BOMDescription2.Value));
            Assert.False(string.IsNullOrWhiteSpace(part.BOMDescription3.Value));
            Assert.False(string.IsNullOrWhiteSpace(part.BOMDescription4.Value));
            Assert.False(string.IsNullOrWhiteSpace(part.BOMDescription5.Value));
            Assert.False(string.IsNullOrWhiteSpace(part.BOMDescription6.Value));
            Assert.False(string.IsNullOrWhiteSpace(part.BOMDescription7.Value));
            Assert.False(string.IsNullOrWhiteSpace(part.BOMDescription8.Value));
            Assert.False(string.IsNullOrWhiteSpace(part.BOMDescription9.Value));
            Assert.True(string.IsNullOrWhiteSpace(part.BOMDescription10.Value));
            Assert.True(string.IsNullOrWhiteSpace(part.BOMDescription11.Value));
        }

        [Fact]
        public void CanGetBomDescription_XBCrossbar()
        {
            var part = _fixture.CreateRootPart<XBCrossbar>();

            var expDesc = "Weldless Safety Bar 0\", 1.5x1/8" +
                "|With 9/16\" Round Holes" +
                "|for a # beam, BLACK";
            var fullDescription = part.BOMDescription.Value;

            Assert.Equal(expDesc, fullDescription);
            Assert.False(string.IsNullOrWhiteSpace(part.BOMDescription1.Value));
            Assert.False(string.IsNullOrWhiteSpace(part.BOMDescription2.Value));
            Assert.False(string.IsNullOrWhiteSpace(part.BOMDescription3.Value));
            Assert.True(string.IsNullOrWhiteSpace(part.BOMDescription4.Value));
            Assert.True(string.IsNullOrWhiteSpace(part.BOMDescription5.Value));
            Assert.True(string.IsNullOrWhiteSpace(part.BOMDescription6.Value));
            Assert.True(string.IsNullOrWhiteSpace(part.BOMDescription7.Value));
            Assert.True(string.IsNullOrWhiteSpace(part.BOMDescription8.Value));
            Assert.True(string.IsNullOrWhiteSpace(part.BOMDescription9.Value));
            Assert.True(string.IsNullOrWhiteSpace(part.BOMDescription10.Value));
            Assert.True(string.IsNullOrWhiteSpace(part.BOMDescription11.Value));
        }

        [Fact]
        public void CanGetBomDescription_WeldedCrossbar()
        {
            var part = _fixture.CreateRootPart<WeldedCrossbar>();

            var expDesc = "Welded Cross Bar" +
                "|1.5x1/8, for a # beam, BLACK" +
                "|with 1/2 HEX attachment hardware" +
                "|for 9/16\" ROUND holes";
            var fullDescription = part.BOMDescription.Value;

            Assert.Equal(expDesc, fullDescription);
            Assert.False(string.IsNullOrWhiteSpace(part.BOMDescription1.Value));
            Assert.False(string.IsNullOrWhiteSpace(part.BOMDescription2.Value));
            Assert.False(string.IsNullOrWhiteSpace(part.BOMDescription3.Value));
            Assert.False(string.IsNullOrWhiteSpace(part.BOMDescription4.Value));
            Assert.True(string.IsNullOrWhiteSpace(part.BOMDescription5.Value));
            Assert.True(string.IsNullOrWhiteSpace(part.BOMDescription6.Value));
            Assert.True(string.IsNullOrWhiteSpace(part.BOMDescription7.Value));
            Assert.True(string.IsNullOrWhiteSpace(part.BOMDescription8.Value));
            Assert.True(string.IsNullOrWhiteSpace(part.BOMDescription9.Value));
            Assert.True(string.IsNullOrWhiteSpace(part.BOMDescription10.Value));
            Assert.True(string.IsNullOrWhiteSpace(part.BOMDescription11.Value));
        }

        [Fact]
        public void CanGetBomDescription_RSFrameTie()
        {
            var part = _fixture.CreateRootPart<RSFrameTie>();

            var expDesc = "Weldless Row Spacer, \" long" +
                "|1.5x1/8 material, BLACK";
            var fullDescription = part.BOMDescription.Value;

            Assert.Equal(expDesc, fullDescription);
            Assert.False(string.IsNullOrWhiteSpace(part.BOMDescription1.Value));
            Assert.False(string.IsNullOrWhiteSpace(part.BOMDescription2.Value));
            Assert.True(string.IsNullOrWhiteSpace(part.BOMDescription3.Value));
            Assert.True(string.IsNullOrWhiteSpace(part.BOMDescription4.Value));
            Assert.True(string.IsNullOrWhiteSpace(part.BOMDescription5.Value));
            Assert.True(string.IsNullOrWhiteSpace(part.BOMDescription6.Value));
            Assert.True(string.IsNullOrWhiteSpace(part.BOMDescription7.Value));
            Assert.True(string.IsNullOrWhiteSpace(part.BOMDescription8.Value));
            Assert.True(string.IsNullOrWhiteSpace(part.BOMDescription9.Value));
            Assert.True(string.IsNullOrWhiteSpace(part.BOMDescription10.Value));
            Assert.True(string.IsNullOrWhiteSpace(part.BOMDescription11.Value));
        }

        [Fact]
        public void CanGetBomDescription_WeldedFrameTie()
        {
            var part = _fixture.CreateRootPart<WeldedFrameTie>();

            var expDesc = "Welded Row / Frame Spacer \" Long" +
                "|1.5x1/8 material with 3/16\" x 6 endplates" +
                "|Connecting to # column" +
                "|w/NO inside connections, BLACK";
            var fullDescription = part.BOMDescription.Value;

            Assert.Equal(expDesc, fullDescription);
            Assert.False(string.IsNullOrWhiteSpace(part.BOMDescription1.Value));
            Assert.False(string.IsNullOrWhiteSpace(part.BOMDescription2.Value));
            Assert.False(string.IsNullOrWhiteSpace(part.BOMDescription3.Value));
            Assert.False(string.IsNullOrWhiteSpace(part.BOMDescription4.Value));
            Assert.True(string.IsNullOrWhiteSpace(part.BOMDescription5.Value));
            Assert.True(string.IsNullOrWhiteSpace(part.BOMDescription6.Value));
            Assert.True(string.IsNullOrWhiteSpace(part.BOMDescription7.Value));
            Assert.True(string.IsNullOrWhiteSpace(part.BOMDescription8.Value));
            Assert.True(string.IsNullOrWhiteSpace(part.BOMDescription9.Value));
            Assert.True(string.IsNullOrWhiteSpace(part.BOMDescription10.Value));
            Assert.True(string.IsNullOrWhiteSpace(part.BOMDescription11.Value));
        }

        [Fact]
        public void CanGetLoadBeamLoadsWide()
        {
            var bay = _fixture.CreateRootPart<Bay>();
            var loads = 3;
            bay.LoadsWide.SetValue(loads);
            var topLevel = bay.SelectiveLevel.Last();
            var frontBeam = topLevel.FrontLoadBeam;
            var rearBeam = topLevel.RearLoadBeam;

            Assert.Equal(loads, frontBeam.LoadsWide.Value);
            Assert.Equal(loads, rearBeam.LoadsWide.Value);
            var beams = topLevel.ActiveChildParts.Value.OfType<ShelfLoadBeam>();
            foreach (var beam in beams)
            {
                Assert.Equal(loads, beam.LoadsWide.Value);
            }
        }

        [Fact]
        public void CanGetLoadBeamLoadsWideDoubleDeep()
        {
            var bay = _fixture.CreateRootPart<Bay>();
            var loads = 3;
            bay.IsDoubleDeep.SetValue(true);
            bay.LoadsWide.SetValue(loads);
            var topLevel = bay.DoubleDeepLevel.Last();
            var beams = topLevel.ActiveChildParts.Value.OfType<ShelfLoadBeam>();
            foreach(var beam in beams)
            {
                Assert.Equal(loads, beam.LoadsWide.Value);
            }
        }

        #endregion
    }
}
