﻿using System;
using System.Collections.Generic;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Engenious.Common;

namespace StorageConfigurator.Vault.Tests
{
    public class VaultTests : IClassFixture<InventorFixture>
    {
        InventorFixture _fixture;

        public VaultTests(InventorFixture fixture)
        {
            _fixture = fixture;
        }

        [Fact]
        [Trait("Category", "ManualOnly")]
        public void CanGetVaultConnection()
        {
            var settings = VaultConfigurationFactory.GetVaultSettings();
            var connection = Ruler.Vault.ConnectionFactory.GetConnection(settings);

            Assert.True(connection.IsConnected);

        }

        [Fact]
        [Trait("Category", "ManualOnly")]
        public void AssemblyUploadTest()
        {
            string assyPath = @"C:\Projects\Advance\Inventor Models\Designs\Projects\123455_Test Project\123455.StorageSystem\123455.StorageSystem.iam";
            string dwgPath = @"C:\Projects\Advance\Inventor Models\Designs\Projects\123455_Test Project\123455.StorageSystem\123455.StorageSystem.dwg";

            //string prtPath = @"C:\Projects\Advance\Inventor Models\Member Files\LoadBeam.999eabf95f97ab34c1c87c2ad3e5ecfa.iam";
            //string dwgPrtPath = @"C:\Projects\Advance\Inventor Models\Member Files\LoadBeam-999eabf95f97ab34c1c87c2ad3e5ecfa.dwg";

            List<(string OutputPath, string ComponentPath)> outputs = new List<(string OutputPath, string ComponentPath)>
            {
                (dwgPath, assyPath),
                //(dwgPrtPath, prtPath)
            };

            var settings = VaultConfigurationFactory.GetVaultSettings();
            IVaultWorkflowHandler vaultWorkflowHandler = new DebugVaultWorkflowHandler();

            VaultUploader vltUploader = new VaultUploader(GetLogger<VaultUploader>(), settings, vaultWorkflowHandler, _fixture.HostApplication);
            ProcessResult result = vltUploader.UploadToVault(assyPath, outputs);

            Assert.True(result.Success);
        }


        #region Helper Methods

        private static ILogger<T> GetLogger<T>()
        {
            var serviceProvider = new ServiceCollection()
                .AddLogging(logging =>
                {
                    logging.AddDebug();
                })
                .BuildServiceProvider();

            return serviceProvider.GetService<ILogger<T>>();
        }

        #endregion

    }
}
