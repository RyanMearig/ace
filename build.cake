#tool nuget:?package=GitVersion.CommandLine
#addin nuget:?package=Newtonsoft.Json&version=11.0.2
#addin nuget:?package=Ruler.Cake

using Newtonsoft.Json;

#region Configuration

// Variables
var projectName = "StorageConfigurator";

// Arguments
var target = Argument("target", "Default");
var configuration = Argument("Configuration", "Release");
var testResultsDir = System.IO.Path.GetFullPath(Argument("testResultsDir", "./test-results/"));
var buildDir = System.IO.Path.GetFullPath(Argument("buildDir", "./build/"));
var artifactsDir = System.IO.Path.GetFullPath(Argument("artifactsDir", "./artifacts/"));
var useLocalDb = Argument("useLocalDb", true);
var localDbInstanceName = Argument("localDbInstanceName", "TestDB");

// Misc
var baseTag = $"/{projectName.ToLower()}/";
// TODO: This will need to change when start running the other test projects as part of the build
var testDir = System.IO.Path.GetFullPath($"./test/{projectName}.Rules.Tests");
var clientDir = System.IO.Path.GetFullPath($"./src/{projectName.ToLower()}-web");
var apiProjPath = System.IO.Path.GetFullPath($"./src/{projectName}.Api/{projectName}.Api.csproj");
var jobProcessorProjPath = System.IO.Path.GetFullPath($"./src/{projectName}JobProcessor/{projectName}JobProcessor.csproj");

var configuratorBuildDir = $"{buildDir}configurator";
var jobProcessorBuildDir = $"{buildDir}job-processor";

#endregion

#region Tasks

Task("Clean")  
    .Does(() =>
    {
        if (DirectoryExists(testResultsDir))
        {
            DeleteDirectory(testResultsDir, new DeleteDirectorySettings
            {
                Recursive = true
            });
        }

        if (DirectoryExists(artifactsDir))
        {
            DeleteDirectory(artifactsDir, new DeleteDirectorySettings
            {
                Recursive = true
            });
        }
        CreateDirectory(artifactsDir);

        if (DirectoryExists(buildDir))
        {
            DeleteDirectory(buildDir, new DeleteDirectorySettings
            {
                Recursive = true
            });
        }
    });

GitVersion versionInfo = null;

Task("Version")
    .Does(() =>
    {
        GitVersion(new GitVersionSettings
        {
            OutputType = GitVersionOutput.BuildServer
        });

        versionInfo = GitVersion();
        System.IO.File.WriteAllText(System.IO.Path.Combine(artifactsDir, "versionInfo.json"), JsonConvert.SerializeObject(versionInfo));
    });

Task("Restore")
	.Does(() =>
	{
		DotNetCoreRestore();
  	});

Task("Test")
	.DoesForEach(GetFiles($"{testDir}/**/*.csproj"), csproj =>
	{
        var logFileName = $"{csproj.GetFilenameWithoutExtension()}.trx";
		var projectDir = csproj.GetDirectory().ToString();

		try
		{
            if (useLocalDb)
            {
                StartProcess(
                    "powershell",
                    new ProcessSettings()
                    {
                        Arguments = new ProcessArgumentBuilder()
                        .Append($"SqlLocalDB create '{localDbInstanceName}' -s")
                    });

                    SetLocalDbConnectionStringOverrides(projectDir, localDbInstanceName);
            }
            DotNetCoreTool(
                projectPath: csproj.FullPath,
                command: "test",
                arguments: $"-r {testResultsDir} -l \"trx;LogFileName={logFileName}\" -v n -c {configuration}"
            );
		}
		finally
        {
            if (useLocalDb)
            {

                StartProcess(
                    "powershell",
                    new ProcessSettings()
                    {
                        Arguments = new ProcessArgumentBuilder()
                            .Append($"SqlLocalDB stop '{localDbInstanceName}'")
                    });

                StartProcess(
                    "powershell",
                    new ProcessSettings()
                    {
                        Arguments = new ProcessArgumentBuilder()
                            .Append($"SqlLocalDB delete '{localDbInstanceName}'")
                    });
                    DropLocalDbDatabases(projectDir);
                }		
            }
        });

Task("BuildApi")
	.Does(() =>
	{
        DotNetCorePublish(
            apiProjPath,
            new DotNetCorePublishSettings()
            {
                Configuration = configuration,
                OutputDirectory = configuratorBuildDir,
                ArgumentCustomization = args => args.Append("--no-restore"),
                MSBuildSettings = new DotNetCoreMSBuildSettings()
                        .WithProperty("Version", versionInfo.NuGetVersion)
                        .WithProperty("AssemblyVersion", versionInfo.AssemblySemVer)
                        .WithProperty("FileVersion", versionInfo.AssemblySemVer)
            });
	});

Task("BuildJobProcessor")
	.Does(() =>
	{
        CreateDirectory(jobProcessorBuildDir);

        DotNetCorePublish(
            jobProcessorProjPath,
            new DotNetCorePublishSettings()
            {
                Configuration = configuration,
                OutputDirectory = jobProcessorBuildDir,
                ArgumentCustomization = args => args.Append("--no-restore")
            });
	});

Task("NpmInstall")
    .Does(() => 
    {
        StartProcess(
            "powershell",
            new ProcessSettings()
            {
                Arguments = new ProcessArgumentBuilder()
                    .Append("npm ci"),
                WorkingDirectory = clientDir
            });
    });

Task("BuildClient")
    .Does(() =>
    {
        var result = StartProcess(
            "powershell",
            new ProcessSettings()
            {
                Arguments = new ProcessArgumentBuilder()
                    .Append(".\\node_modules\\.bin\\ng build")
                    .Append($"--output-path={configuratorBuildDir}/wwwroot")
                    .Append($"--base-href {baseTag}")
                    .Append("--no-progress")
                    .Append("--prod")
                    .Append("--aot=false")
                    .Append("--build-optimizer=false"),
                WorkingDirectory = clientDir
            });

		if (result != 0)
		{
			throw new Exception("BuildClient failed");
		}
    });

Task("Package")
    .Does(() =>
    {
        Zip(configuratorBuildDir, $"{artifactsDir}\\{projectName}-{versionInfo.FullSemVer}.zip");
        Zip(jobProcessorBuildDir, $"{artifactsDir}\\{projectName}JobProcessor-{versionInfo.FullSemVer}.zip");
    });

#endregion

#region Activation

Task("Default")
	.IsDependentOn("Clean")
 	.IsDependentOn("Version")
	.IsDependentOn("Restore")
	.IsDependentOn("Test")
	.IsDependentOn("BuildApi")
	.IsDependentOn("BuildJobProcessor")
	.IsDependentOn("NpmInstall")
    .IsDependentOn("BuildClient")
    .IsDependentOn("Package");

RunTarget(target);

#endregion
