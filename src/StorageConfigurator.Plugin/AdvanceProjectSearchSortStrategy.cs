﻿using System.Collections.Generic;
using System.Linq;
using Ruler.Configurator.Extensibility;

namespace StorageConfigurator.Plugin
{
    public class AdvanceProjectSearchSortStrategy : IProjectSearchSortStrategy
    {
        public IOrderedEnumerable<IProject> Sort(IEnumerable<IProject> projects)
        {
            return projects.OrderBy(p => (string)p.GetPropertyValue("drawingNumber"));
        }
    }
}
