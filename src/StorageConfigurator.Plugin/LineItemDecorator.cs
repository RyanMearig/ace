﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Ruler.Configurator.Extensibility;
using Ruler.Cpq;

namespace StorageConfigurator.Plugin
{
    public class LineItemDecorator: ILineItemDecorator
    {

        #region Constructors

        public LineItemDecorator(ICostSummaryService costSummaryService)
        {
            _costSummaryService = costSummaryService;
        }

        #endregion

        #region Private Members

        ICostSummaryService _costSummaryService;

        #endregion

        public Task BeforeLineItemAddedAsync(ILineItem lineItem, IProject project)
        {
            var name = lineItem.Name;
            var suffix = 'B';

            while (project.LineItems.Any(li => li.Name == name))
            {
                name = $"{lineItem.Name}{suffix}";
                suffix++;
            }

            lineItem.Name = name;
            return Task.CompletedTask;
        }


        public async Task AfterLineItemAddedAsync(ILineItem lineItem)
        {
            var dto = GetEmptyLineItemCostSummary(lineItem.Id, lineItem.Name, lineItem.Name);
            var costSummary = await _costSummaryService.AddLineItemCostSummaryAsync(dto);
            return;
        }

        public Task BeforeLineItemUpdatedAsync(ILineItem lineItem) => Task.CompletedTask;

        public Task AfterLineItemUpdatedAsync(ILineItem lineItem) => Task.CompletedTask;
        //public async Task AfterLineItemUpdatedAsync(ILineItem lineItem)
        //{
        //    var dto = GetLineItemCostSummary(lineItem);
        //    var costSummary = await _costSummaryService.UpdateLineItemCostSummaryAsync(dto);
        //    return;
        //}

        //private LineItemCostSummaryDto GetLineItemCostSummary(ILineItem lineItem)
        //{
        //    var part = lineItem.Root;
        //    if (part is IBomPart bomPart)
        //    {
        //        var bom = bomPart.Bom;
        //        return new LineItemCostSummaryDto()
        //        {
        //            LineItemId = lineItem.Id,
        //            Cost = bom.UnitCost.Value,
        //            Qty = bom.ItemQty.Value,
        //            Name = bom.ShortDescription.Value,
        //            Description = bom.Description.Value,
        //            Margin = bom.Margin.Value,
        //            Include = true,
        //            ExtraData = ""
        //        };
        //    }
        //    return GetEmptyLineItemCostSummary(lineItem.Id, lineItem.Name, lineItem.Name);

        //}

        private LineItemCostSummaryDto GetEmptyLineItemCostSummary(Guid lineItemId, string name, string description)
        {
            return new LineItemCostSummaryDto()
            {
                LineItemId = lineItemId,
                Cost = 0,
                Qty = 1,
                Name = name,
                Description = description,
                Margin = 0,
                Include = true,
                ExtraData = ""
            };
        }

        public Task BeforeLineItemDeletedAsync(ILineItem lineItem) => Task.CompletedTask;
        public Task AfterLineItemDeletedAsync(Guid id) => Task.CompletedTask;

        public Task BeforeCopiedLineItemAddedAsync(ILineItem lineItem) => Task.CompletedTask;
        public Task AfterCopiedLineItemAddedAsync(ILineItem lineItem) => Task.CompletedTask;
    }
}
