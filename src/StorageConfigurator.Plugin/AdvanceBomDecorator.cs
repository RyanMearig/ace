﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ruler.Cpq;
using Ruler.Rules;
using StorageConfigurator.Data;

namespace StorageConfigurator.Plugin
{
    public class AdvanceBomDecorator : IBomDecorator
    {
        #region Private Fields

        private AdvanceCSIItemService _csiItemService;

        #endregion

        #region Constructors
        
        public AdvanceBomDecorator(AdvanceCSIItemService csiItemService)
        {
            _csiItemService = csiItemService;
        }

        #endregion

        #region Public Properties

        public void BeforeConfigureRules(BomData bom)
        {
            bom.ShortDescription.Define(() => bom.PartNumber.Value);
            bom.ScrapPercentage.Define(() => GetScrapPercentage(bom));
            bom.MaterialUnitCost.Define(() => GetConvertedCost(bom));
            bom.ItemPurchasedCost.Define(() => GetConvertedPurchasedCost(bom));
            bom.ItemPurchasedWeight.Define(() => GetPurchasedWeight(bom));
            bom.Item.Define(() => GetItemNumber(bom));
        }

        public void AfterConfigureRules(BomData bom)
        {
            return;
        }

        #endregion

        #region private Methods

        private string GetPlant(BomData bom)
        {
            return bom.PlantCode.Value ?? "GA_C";
        }

        private double GetConvertedCost(BomData bom)
        {
            if (string.IsNullOrEmpty(bom.MaterialPartNumber.Value)) return 0;
            var csiCost = _csiItemService.GetItemCost(bom.MaterialPartNumber.Value, GetPlant(bom));

            var uom = bom.MaterialUnitOfMeasure.Value;
            if (uom == UnitOfMeasure.Pounds)
            {
                var weight = _csiItemService.GetItemWeight(bom.MaterialPartNumber.Value, GetPlant(bom));
                var root = GetRootPart(bom);
                var steelOverhead = GetRuleValueOrDefault(root, "SteelOverhead");
                var steelDiscount = GetRuleValueOrDefault(root, "SteelDiscount");
                return (csiCost / weight) + steelOverhead - steelDiscount;
            }
            return csiCost;
        }

        private double GetConvertedPurchasedCost(BomData bom)
        {
            if (bom.IsPurchased.Value && !string.IsNullOrWhiteSpace(bom.PartNumber.Value))
            {
                return _csiItemService.GetItemCost(bom.PartNumber.Value, GetPlant(bom));
            }
            return 0;
        }

        private double GetPurchasedWeight(BomData bom)
        {
            if (bom.IsPurchased.Value && !string.IsNullOrWhiteSpace(bom.PartNumber.Value))
            {
                return _csiItemService.GetItemWeight(bom.PartNumber.Value, GetPlant(bom));
            }
            return 0;
        }

        private double GetRuleValueOrDefault(Part prt, string ruleName, double defaultVal = 0)
        {
            return prt != null && prt.HasRule(ruleName) ? prt.GetRule<double>(ruleName).Value : 0;
        }

        private double GetScrapPercentage(BomData bom)
        {
            if (((Part)bom.Parent).HasCadParts) { return 0; }

            return GetRuleValueOrDefault(GetRootPart(bom), "ScrapPercentage");
        }


        private string GetItemNumber(BomData bom)
        {
            var root = GetRootPart(bom);
            var ruleName = "ItemNumbers";

            if (!string.IsNullOrWhiteSpace(bom.PartNumber.Value))
            {
                var itemNums = root != null && root.HasRule(ruleName) ?
                root.GetRule<List<Tuple<string, int>>>(ruleName).Value : null;

                if (itemNums != null && itemNums.Any(n => n.Item1 == bom.PartNumber.Value))
                {
                    return itemNums.FirstOrDefault(n => n.Item1 == bom.PartNumber.Value).Item2.ToString();
                }
            }

            return "";
        }

        private Part GetRootPart(BomData bom)
        {
            Part prt = (Part)bom.Parent;
            while (!prt.IsRoot())
            {
                prt = prt.Parent;
            }
            return prt;
        }

        #endregion
    }
}
