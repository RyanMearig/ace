﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Ruler.Cpq;
using StorageConfigurator.Rules;

namespace StorageConfigurator.Plugin
{
    public class StorageBomSummarizer : IBomSummarizer
    {
        public BomSummaryItem GetBomSummary(BomData bom, double? qtyOverride = null)
        {
            var qty = qtyOverride ?? 1;
            var groupedChildren = GetGroupedChildren(bom);
            var children = groupedChildren.Select(g => GetBomSummary(g.child, g.qty)).ToList();
            children = children.OrderBy(c => (string.IsNullOrWhiteSpace(c.Item)) ? "0" : c.Item.PadLeft(3, '0')).ToList();

            // Using BomDescription as Description and Description as ShortDescription.
            var iBomParent = bom.Parent is Rules.IBomPart part ? part : null;
            var bomDescription = (iBomParent != null) ? iBomParent.BOMDescription.Value : bom.Description.Value;
            var shortDesc = (iBomParent != null) ? bom.ShortDescription.Value : bom.Description.Value;

            var summary = new BomSummaryItem()
            {
                Item = !string.IsNullOrWhiteSpace(bom.Item.Value) ? bom.Item.Value.PadLeft(3, '0') :  bom.Item.Value,
                Description = bom.Description.Value,
                ShortDescription = shortDesc,
                Notes = bomDescription,
                PartNumber = bom.PartNumber.Value,
                MaterialFullDescription = bom.MaterialFullDescription.Value,
                MaterialPartNumber = bom.MaterialPartNumber.Value,
                ItemWeight = bom.ItemWeight.Value,
                TotalWeight = bom.UnitWeight.Value * qty,
                ItemRawWeight = bom.ItemRawWeight.Value,
                TotalRawWeight = bom.UnitRawWeight.Value * qty,
                Qty = qty,
                ItemUnitOfMeasure = bom.ItemUnitOfMeasure.Value.ToString(),
                ItemMaterialQty = bom.MaterialQty.Value,
                ItemRawMaterialQty = bom.RawMaterialQty.Value,
                MtrRequired = bom.MtrRequired.Value,
                ItemCostNoLabor = bom.ItemCostNoLabor.Value,
                ItemPriceNoLabor = bom.ItemPriceNoLabor.Value,
                ItemBomLaborPrice = bom.ItemLaborPrice.Value,
                ItemBomLaborCost = bom.ItemLaborCost.Value,
                UnitMargin = bom.Margin.Value,
                OverriddenUnitPrice = bom.UnitPrice.Value,
                UseOverriddenUnitPrice = bom.UnitPrice.IsOverridden,
                isPurchased = bom.IsPurchased.Value,
                Path = bom.Path,
                ItemManuallyEnteredLaborHours = (bom.ItemLaborHours.Value > 0 && bom.LaborItems.Value.Length == 0) ? bom.ItemLaborHours.Value : default(double?),
                SetupLabor = bom.AllLaborItems.Value.SelectMany(li =>
                {
                    var result = new List<LaborSummaryItem>();
                    if (li.SetupHours != 0 && (
                    li.Operation.SetupHourStyle == SetupHourStyle.Once ||
                    li.Operation.SetupHourStyle == SetupHourStyle.Unique ||
                    li.Operation.SetupHourStyle == SetupHourStyle.Global
                    ))
                    {
                        result.Add(new LaborSummaryItem()
                        {
                            Name = li.OperationName + " (Setup)",
                            Hours = li.SetupHours,
                            Cost = li.SetupCost,
                            Price = li.SetupCost * (1 + bom.LaborMarginPercent.Value * 0.01),
                            Category = li.Category,
                            Style = li.Operation.SetupHourStyle
                        });
                    }
                    return result;
                }).OrderBy(i => i.Name).ToList(),
                Children = children
            };

            summary.Labor = bom.AllLaborItems.Value.SelectMany(li =>
            {
                var result = new List<LaborSummaryItem>();
                result.Add(new LaborSummaryItem()
                {
                    Name = li.OperationName,
                    Hours = li.Hours,
                    Cost = li.Cost,
                    Price = li.Cost * (1 + bom.LaborMarginPercent.Value * 0.01),
                    Category = li.Category
                });
                if (li.SetupHours != 0 && li.Operation.SetupHourStyle == SetupHourStyle.Every)
                {
                    var newItem = new LaborSummaryItem()
                    {
                        Name = li.OperationName + " (Setup)",
                        Hours = li.SetupHours,
                        Cost = li.SetupCost,
                        Price = li.SetupCost * (1 + bom.LaborMarginPercent.Value * 0.01),
                        Category = li.Category,
                        Style = li.Operation.SetupHourStyle
                    };
                    result.Add(newItem);
                    summary.ItemLaborSetupHours += newItem.Hours;
                    summary.ItemLaborSetupCost += newItem.Cost;
                    summary.ItemLaborSetupPrice += newItem.Price;
                };

                return result;
            }).OrderBy(i => i.Name).ToList();

            var preSetupCost = summary.TotalCalculatedCost;
            var preSetupPrice = summary.TotalCalculatedPrice;

            if (bom.IsRoot.Value)
            {
                //Once is added one time to the top level
                summary = AddOnceStyleSetupLabor(summary);
                // Unique is added at each level, divided by the totalled qty of that level
                // e.g. if I need 1 hour of setup time, but I have 5 of my parent and 2 of me, I add 1/(5*2) hours to my level
                summary = AddUniqueStyleSetupLabor(summary, 1);
                // Global finds the global quantity by Part Number and adds to each part, dividing expected time by global qty
                summary = AddGlobalStyleSetupLabor(summary);

                if (preSetupCost != summary.TotalCalculatedCost || preSetupPrice != summary.TotalCalculatedPrice)
                {
                    //setup labor has been added and recalced price, update margin value
                    var newPrice = summary.Children.Sum(s => s.TotalPrice) + summary.ItemPrice;
                    var newCost = summary.Children.Sum(s => s.TotalCost) + summary.ItemCost;
                    var newMargin = ((newPrice - newCost) / newPrice) * 100;
                    summary.UnitMargin = newMargin;
                };
                
            };

            summary.TotalCost = summary.TotalCalculatedCost;
            summary.TotalPrice = summary.TotalCalculatedPrice;

            summary.ExtraData = GetExtraDataJson(summary);

            return summary;
        }

        private IEnumerable<(BomData child, double qty)> GetGroupedChildren(BomData item)
        {
            var groupedChildren = item.BomChildren.Value
                    .GroupBy(c => (desc: c.PartNumber.Value, cost: Math.Round(c.TotalCost.Value, 2), margin: Math.Round(c.Margin.Value)))
                    .Select(g => (child: g.First(), qty: g.Sum(c => c.ItemQty.Value)));
            return groupedChildren;
        }

        private BomSummaryItem AddOnceStyleSetupLabor(BomSummaryItem summary)
        {
            var allSetupLabor = new List<LaborSummaryItem>();
            allSetupLabor = GetLaborSummaryItems(summary, allSetupLabor).ToList();
            summary.Labor = summary.Labor.Concat(allSetupLabor).ToList();
            return summary;
        }

        private IEnumerable<LaborSummaryItem> GetLaborSummaryItems(BomSummaryItem summary, IEnumerable<LaborSummaryItem> labor)
        {
            labor = labor.Concat(summary.SetupLabor.Where(l => l.Style == SetupHourStyle.Once));
            foreach (var item in summary.Children)
            {
                labor = GetLaborSummaryItems(item, labor);
            }
            return labor;
        }

        private BomSummaryItem AddUniqueStyleSetupLabor(BomSummaryItem summary, double qty)
        {
            var myQty = qty * summary.Qty;
            var labor = summary.SetupLabor.Where(l => l.Style == SetupHourStyle.Unique);
            foreach (var item in labor)
            {
                var newItem = new LaborSummaryItem()
                {
                    Name = item.Name,
                    Hours = item.Hours / myQty,
                    Cost = item.Cost / myQty,
                    Price = item.Price / myQty,
                    Category = item.Category,
                    Style = item.Style
                };
                summary.Labor.Add(newItem);
                summary.ItemLaborSetupHours += newItem.Hours;
                summary.ItemLaborSetupCost += newItem.Cost;
                summary.ItemLaborSetupPrice += newItem.Price;
            }

            foreach (var item in summary.Children)
            {
                AddUniqueStyleSetupLabor(item, myQty);
            }

            summary.TotalCost = summary.TotalCalculatedCost;
            summary.TotalPrice = summary.TotalCalculatedPrice;

            return summary;
        }

        private BomSummaryItem AddGlobalStyleSetupLabor(BomSummaryItem summary)
        {
            var pnQtyDict = new Dictionary<string, double>();
            pnQtyDict = GetQtys(summary, pnQtyDict, 1);
            return AddGlobalStyleSetupLabor(summary, pnQtyDict);
        }

        private BomSummaryItem AddGlobalStyleSetupLabor(BomSummaryItem summary, Dictionary<string, double> pnQtyDict)
        {
            var pn = summary.PartNumber ?? summary.Description;
            var myQty = pnQtyDict[pn];
            var labor = summary.SetupLabor.Where(l => l.Style == SetupHourStyle.Global);
            foreach (var item in labor)
            {
                var newItem = new LaborSummaryItem()
                {
                    Name = item.Name,
                    Hours = item.Hours / myQty,
                    Cost = item.Cost / myQty,
                    Price = item.Price / myQty,
                    Category = item.Category,
                    Style = item.Style
                };
                summary.Labor.Add(newItem);
                summary.ItemLaborSetupHours += newItem.Hours;
                summary.ItemLaborSetupCost += newItem.Cost;
                summary.ItemLaborSetupPrice += newItem.Price;
            }

            foreach (var item in summary.Children)
            {
                AddGlobalStyleSetupLabor(item, pnQtyDict);
            }

            summary.TotalCost = summary.TotalCalculatedCost;
            summary.TotalPrice = summary.TotalCalculatedPrice;
            return summary;
        }

        private Dictionary<string, double> GetQtys(BomSummaryItem summary, Dictionary<string, double> dict, double runningQty)
        {
            var qty = summary.Qty * runningQty;
            var pn = summary.PartNumber ?? summary.Description;
            if (dict.ContainsKey(pn))
            {
                var existingQty = dict[pn];
                dict[pn] = existingQty + qty;
            } else
            {
                dict.Add(pn, qty);
            }
            foreach (var item in summary.Children)
            {
                dict = GetQtys(item, dict, qty);
            }
            return dict;
        }

        private string GetExtraDataJson(BomSummaryItem summaryItem)
        {
            var manufacturedChildren = summaryItem.Children?.Where(c => !c.isPurchased);
            var manMatCost = manufacturedChildren?.Sum(c => c.TotalCost) ?? 0;
            var manMatPrice = manufacturedChildren?.Sum(c => c.TotalPrice) ?? 0;

            var purchasedChildren = summaryItem.Children?.Where(c => c.isPurchased);
            var purchMatCost = purchasedChildren?.Sum(c => c.TotalCost) ?? 0;
            var purchMatPrice = purchasedChildren?.Sum(c => c.TotalPrice) ?? 0;

            var datalist = new 
            {
                manMaterialCost = manMatCost,
                manMaterialMargin = GetMarginPercent(manMatCost, manMatPrice),
                manMaterialPrice = manMatPrice,
                buyoutMaterialCost = purchMatCost,
                buyoutMaterialMargin = GetMarginPercent(purchMatCost, purchMatPrice),
                buyoutMaterialPrice = purchMatPrice
            };

            return JsonConvert.SerializeObject(datalist);
        }

        private double GetMarginPercent(double cost, double price, double defaultVal = 0)
        {
            if (price == 0) return defaultVal;
            return ((price - cost) / price) * 100;
        }
    }
}
