﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;
using Newtonsoft.Json;
using Ruler.Rules.Serialization;
using System.Linq;

namespace StorageConfigurator.Plugin
{
    public class ReportingPartJsonConverter : JsonConverter
    {

        public string[] RulesToExclude = { "AllowedChildTypes", "IsNavigable", "three", "CadHash" };
        public Type[] ValidTypes = { typeof(float), typeof(string), typeof(int), typeof(double), typeof(bool) };

        #region JsonConverter Overrides

        public override bool CanConvert(Type objectType)
        {
            return objectType.IsPart() || objectType.IsPartData();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var host = (IRuleHost)value;

            writer.WriteStartObject();

            writer.WritePropertyName("path");
            writer.WriteValue(host.Path);

            writer.WritePropertyName("name");
            writer.WriteValue(host.Name);

            writer.WritePropertyName("partType");
            writer.WriteValue(host.TypeName);

            this.WriteRules(host, writer, serializer);

            if (host is IHierarchicalRuleHost parent)
            {
                this.WriteChildren(parent, writer, serializer);
            }

            writer.WriteEndObject();
        }

        #endregion

        #region Private Methods

        protected virtual void WriteRules(IRuleHost ruleHost, JsonWriter writer, JsonSerializer serializer)
        {
            foreach (var rule in ruleHost.Rules.Where(r => r.IsDefined && ValidTypes.Contains(r.ValueType) && !RulesToExclude.Contains(r.Name)))
            {
                writer.WritePropertyName(rule.Name.ToCamelCase());
                serializer.Serialize(writer, rule.ValueObject);

            }
        }

        protected virtual void WriteChildren(IHierarchicalRuleHost ruleHost, JsonWriter writer, JsonSerializer serializer)
        {
            foreach (var child in ruleHost.ActiveChildren.Value)
            {
                writer.WritePropertyName(child.Name.ToCamelCase());
                serializer.Serialize(writer, child);
            }
        }

        #endregion

    }
}
