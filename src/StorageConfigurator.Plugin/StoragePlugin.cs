﻿using System.Collections.Generic;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Ruler.Configurator.Extensibility;
using Ruler.Configurator.Extensibility.Plugins;
using Ruler.Rules.Configuration;
using StorageConfigurator.Data;
using StorageConfigurator.Rules;
using Ruler.Cpq;
using System;
using Ruler.Rules.Serialization;

namespace StorageConfigurator.Plugin
{
    public class StoragePlugin : IPlugin
    {
        public void AddPartTypes(IPartTypeMap map, IConfiguration configuration)
        {
            map.AddPartType("storagesystem", "StorageSystem", "Storage System", typeof(StorageSystem), allowAddMultiple: false);
            map.AddPartType("shippableparts", "Parts", "Parts", typeof(ShippableParts), allowAddMultiple: false);

            map.AddPartType("row", "Row", "Row", typeof(Row), allowAsLineItem: false);
            map.AddPartType("rowproxy", "Row", "Row", typeof(RowProxy), allowAsLineItem: false);
            map.AddPartType("product", "Product", "Product", typeof(Product), allowAsLineItem: false);
            map.AddPartType("bay", "Bay", "Bay", typeof(Bay), allowAsLineItem: false);
            map.AddPartType("selectiveFrameLine", "Selective Frame Line", "Selective Frame Line", typeof(SelectiveFrameLine), allowAsLineItem: false);
            map.AddPartType("selectiveFrameType", "Selective Frame Type", "Selective Frame Type", typeof(SelectiveFrameType), allowAsLineItem: false);
            map.AddPartType("doubleDeepFrameType", "Double Deep Frame Type", "Double Deep Frame Type", typeof(DoubleDeepFrameType), allowAsLineItem: false);
            map.AddPartType("frameuprightkit", "Upright", "Upright", typeof(FrameUprightKit), allowAsLineItem: false);
            map.AddPartType("frametie", "Frame Tie", "Frame Tie", typeof(FrameTie), allowAsLineItem: false);
            map.AddPartType("shelfloadbeam", "Load Beam", "Load Beam", typeof(ShelfLoadBeam), allowAsLineItem: false);
            map.AddPartType("shelfpalletstop", "Pallet Stop", "Pallet Stop", typeof(ShelfPalletStop), allowAsLineItem: false);
            map.AddPartType("shelfbeamcrossbar", "Crossbar", "Crossbar", typeof(ShelfBeamCrossbar), allowAsLineItem: false);
			map.AddPartType("uprightcolumnassembly", "Post", "Post", typeof(UprightColumnAssembly), allowAsLineItem: false);
            map.AddPartType("endaisleguard", "End Aisle Guard", "End Aisle Guard", typeof(EndAisleGuard), allowAsLineItem: false);
            map.AddPartType("shim", "Shim", "Shim", typeof(Shim), allowAsLineItem: false);
            map.AddPartType("touchUpCan", "Touch Up Can", "Touch-Up Can", typeof(TouchUpCan), allowAsLineItem: false);
            map.AddPartType("anchorBolt", "Anchor", "Anchor", typeof(AnchorBolt), allowAsLineItem: false);
            map.AddPartType("bolt", "Bolt", "Bolt", typeof(Bolt), allowAsLineItem: false);
            map.AddPartType("nut", "Nut", "Nut", typeof(Nut), allowAsLineItem: false);
            map.AddPartType("washers", "Washers", "Washers", typeof(Washers), allowAsLineItem: false);
            map.AddPartType("capacitySignKit", "Capacity Sign Kit", "Capacity Sign Kit", typeof(CapacitySignKit), allowAsLineItem: false);
            map.AddPartType("rackInstallGuide", "Rack Install Guide", "Rack Install Guide", typeof(RackInstallGuide), allowAsLineItem: false);
            map.AddPartType("custompart", "Custom Part", "Custom Part", typeof(Rules.CustomPart), allowAsLineItem: false);
            map.AddPartType("buyoutpart", "Buyout Part", "Buyout Part", typeof(Rules.BuyoutPart), allowAsLineItem: false);
            map.AddPartType("customlaboritem", "Custom Labor Item", "Custom Labor Item", typeof(CustomLaborItem), allowAsLineItem: false);
        }

        public void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            var connString = configuration.GetConnectionString("StorageConfigurator");
            Action<DbContextOptionsBuilder> configureDbContext = options => options.UseSqlServer(connString);
            services.AddPartNumbers(configureDbContext);
            services.AddDbContext<StorageConfiguratorContext>(configureDbContext);
            
            services.AddSingleton(GetRulesSeedDataProvider(typeof(StorageConfiguratorContext).Assembly));

            services.AddSingleton(s =>
            {
                using (var scope = s.CreateScope())
                {
                    var storageContext = scope.ServiceProvider.GetService<StorageConfiguratorContext>();
                    return new StorageConfiguratorRepository(storageContext);
                }
            });
            //services.AddSingleton<StorageConfiguratorRepository>();
            services.AddSingleton<FactoryRevisionService>();

            services.AddSingleton<IRulerCadJobBuilder, CadJobBuilder>();
            services.AddScoped<IDatabaseInitializer, StorageConfiguratorContextInitializer>();
            services.AddSingleton<IProjectSearchSortStrategy, AdvanceProjectSearchSortStrategy>();

            services.AddCpq(configureDbContext);
            services.AddSingleton(GetBomSeedDataProvider(typeof(StorageConfiguratorContext).Assembly));
            services.AddSingleton<IBomSummarizer, StorageBomSummarizer>();

            services.AddSingleton((s) => new AdvanceCSIConnectionFactory(configuration));

            var csiDbConnection = configuration.GetConnectionString("CSIDbConnection");
            services.AddDbContext<AdvanceCSIContext>(optionsBuilder => optionsBuilder.UseSqlServer(csiDbConnection), optionsLifetime: ServiceLifetime.Singleton);
            services.AddSingleton<AdvanceCSIContextFactory>();
            services.AddScoped<AdvanceCSIRepository>();
            //services.AddScoped<IDatabaseInitializer, AdvanceCSIContextInitializer>();

            services.AddSingleton<AdvanceCSIItemService>();
            services.AddSingleton<IBomDecorator, AdvanceBomDecorator>();

            services.AddScoped<ICostSummaryService, DefaultCostSummaryService>();
            services.AddScoped<IProjectDecorator, ProjectDecorator>();
            services.AddScoped<ILineItemDecorator, LineItemDecorator>();

            services.AddSingleton<IWebJsonProcessor, AdvanceIWebJsonProcessor>();
        }

        private static readonly Dictionary<string, IEnumerable<string>> seedDataResourceMap = new Dictionary<string, IEnumerable<string>>()
            {
                { "UprightColumn", new List<string>{ "BeamMaterialPartNumbers" } },
                { "HeavyHorizontalBrace", new List<string>{ "BeamMaterialPartNumbers" } },
                { "ShelfBeam", new List<string>{ "BeamMaterialPartNumbers" } },
                { "FrameTieChannel", new List<string>{ "BeamMaterialPartNumbers" } },
                { "BoltOnHorizontalBrace", new List<string>{ "BeamMaterialPartNumbers" } },
                { "BoltOnHorizontalChannel", new List<string>{ "BeamMaterialPartNumbers" } },
                { "TippmannHorizontal", new List<string>{ "BeamMaterialPartNumbers" } },
                { "EndAisleGuardBeam", new List<string>{ "BeamMaterialPartNumbers" } },

                { "XBCrossbarAngle", new List<string>{ "AngleMaterialPartNumbers" } },
                { "CrossbarAngle", new List<string>{ "AngleMaterialPartNumbers" } },
                { "CrossbarEndAngle", new List<string>{ "AngleMaterialPartNumbers" } },
                { "AnglePostProtector", new List<string>{ "AngleMaterialPartNumbers" } },
                { "BoltonHeavyHorizontalAngle", new List<string>{ "AngleMaterialPartNumbers" } },
                { "DiagonalBrace", new List<string>{ "AngleMaterialPartNumbers" } },
                { "FrameTieAngle", new List<string>{ "AngleMaterialPartNumbers" } },
                { "HeavyHorizontalWeldClip", new List<string>{ "AngleMaterialPartNumbers" } },
                { "RSFrameTieAngle", new List<string>{ "AngleMaterialPartNumbers" } },
                { "RubRailAngle", new List<string>{ "AngleMaterialPartNumbers" } },

                { "FrameTieFourHoleEndPlate", new List<string>{ "FlatBarMaterialPartNumbers" } },
                { "ShelfPalletStopMaterial", new List<string>{ "FlatBarMaterialPartNumbers" } },
                { "WeldedCrossbarNotchPlate", new List<string>{ "FlatBarMaterialPartNumbers" } },
                { "DoubledRubRailPlate", new List<string>{ "FlatBarMaterialPartNumbers" } },
                { "CrossbarEndPlate", new List<string>{ "FlatBarMaterialPartNumbers" } },

                { "Footplate", new List<string>{ "SheetSteelMaterialPartNumbers" } },
                { "DoublerCap", new List<string>{ "SheetSteelMaterialPartNumbers" } },
                { "TubeCapPlate", new List<string>{ "SheetSteelMaterialPartNumbers" } },
                { "HeavyHorizontalWeldPlate", new List<string>{ "SheetSteelMaterialPartNumbers" } },
            };


        public static IRulesSeedDataProvider GetRulesSeedDataProvider(Assembly assy) => 
            new JsonResourceSharedRulesSeedDataProvider(assy, "StorageConfigurator.Data.SeedData.Rules", "StorageConfigurator.Data.SeedData.SharedRules", seedDataResourceMap);
        public static IBomSeedDataProvider GetBomSeedDataProvider(Assembly assy) => new MixedFileBomSeedDataProvider(assy, "StorageConfigurator.Data.SeedData.Bom");
    }
}
