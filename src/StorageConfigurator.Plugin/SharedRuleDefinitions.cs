﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ruler.Rules.Configuration;

namespace StorageConfigurator.Plugin
{
    public class SharedRuleDefinitions
    {
        #region Public Properties

        public string Key { get; set; }

        public List<RuleDefinition> Rules { get; set; } = new List<RuleDefinition>();

        #endregion

        #region Public Methods

        public RuleDefinition GetRuleDefinition(string name)
        {
            return this.Rules.FirstOrDefault(r => string.Compare(r.Name, name, true) == 0);
        }

        #endregion
    }
}
