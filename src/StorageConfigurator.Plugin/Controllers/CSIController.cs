﻿using Microsoft.AspNetCore.Mvc;
using Ruler.Configurator.Extensibility;
using Ruler.Rules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StorageConfigurator.Data;
using StorageConfigurator.Rules;

namespace StorageConfigurator.Plugin.Controllers
{
    public class CSIController : Controller
    {

        #region Private Fields

        private readonly ILineItemService _lineItemService;
        private readonly IProjectService _projectService;
        private readonly AdvanceCSIRepository _csiRepository;

        #endregion

        public CSIController(ILineItemService lineItemService, IProjectService projectService, AdvanceCSIRepository csiRepository)
        {
            _lineItemService = lineItemService;
            _projectService = projectService;
            _csiRepository = csiRepository;
        }

        #region Actions

        [HttpGet("api/csi/customer/{custNum}/{custSeq}")]
        public async Task<Customer> GetCustomer(string custNum, int custSeq)
        {
            return await this._csiRepository.GetCustomerAsync(custNum, custSeq);
        }

        [HttpGet("api/csi/customer/{custNum}/getSequences")]
        public async Task<List<int>> GetCustomerSequences(string custNum)
        {
            return await this._csiRepository.GetCustomerSequences(custNum);
        }

        [HttpGet("api/csi/getSteelSurcharge")]
        public async Task<double> GetSteelSurcharge()
        {
            return await this._csiRepository.GetLatestSteelSurcharge();
        }

        #endregion

    }
}
