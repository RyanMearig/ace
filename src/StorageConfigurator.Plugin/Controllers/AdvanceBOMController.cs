﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Ruler.Configurator.Extensibility;
using Ruler.Configurator;
using Ruler.Cpq;
using StorageConfigurator.Rules;

namespace StorageConfigurator.Plugin.Controllers
{
    public class AdvanceBOMController : Controller
    {

        #region Private Fields

        private readonly ILineItemService _lineItemService;
        private readonly IBomSummarizer _bomItemSumarizer;
        private readonly ICostSummaryService _costSummaryService;
        private readonly IProjectService _projectService;

        #endregion

        public AdvanceBOMController(ILineItemService lineItemService, IBomSummarizer bomSummarizer, ICostSummaryService costSummaryService, IProjectService projectService)
        {
            _lineItemService = lineItemService;
            _bomItemSumarizer = bomSummarizer;
            _costSummaryService = costSummaryService;
            _projectService = projectService;
        }

        #region Actions

        [HttpGet]
        [Route("api/rowbom/{lineItemId}")]
        public async Task<IActionResult> GetRowBom(Guid lineItemId)
        {
            var lineItem = await _lineItemService.GetLineItemAsync(lineItemId);

            if (lineItem.Root is StorageSystem storageSys)
            {
                var summary = _bomItemSumarizer.GetBomSummary(storageSys.RowBom);
                return Json(summary);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpPost]
        [Route("api/setDefaultMargins/{lineItemId}")]
        public async Task<IActionResult> SetLineItemDefaultMargins(Guid lineItemId)
        {
            var lineItem = await _lineItemService.GetLineItemAsync(lineItemId);
            var project = await _projectService.GetProjectAsync(lineItem.ProjectId);

            if (lineItem.Root is ASLineItemPart lineItemPart)
            {
                var lineItemIds = project.LineItems.Select(li => li.Id);
                var summaries = await _costSummaryService.GetLineItemCostSummariesAsync(lineItemIds);
                var matCost = summaries.Sum(s => s.Cost);
                var engCost = project.GetPropertyValueOrDefault<double>("engCost");
                var installCost = project.GetPropertyValueOrDefault<double>("installCost");
                var freightCost = project.GetPropertyValueOrDefault<double>("freightCost");
                var otherCost = project.GetPropertyValueOrDefault<double>("otherCost");
                var totalCost = matCost + engCost + installCost + freightCost + otherCost;

                var customerType = project.GetPropertyValueOrDefault<string>("customerType");
                
                if (!string.IsNullOrWhiteSpace(customerType))
                {
                    lineItemPart.ResetBOMChildrenMargins(customerType, totalCost);
                }

                return Ok();
            }
            else
            {
                return NotFound();
            }
        }

        [HttpPost]
        [Route("api/setAllMargins/{lineItemId}")]
        public async Task<IActionResult> SetAllLineItemMargins(Guid lineItemId, double margin = 0.0)
        {
            var lineItem = await _lineItemService.GetLineItemAsync(lineItemId);

            if (lineItem.Root is ASLineItemPart lineItemPart)
            {
                lineItemPart.DistinctBomChildrenByPath.Value.ForEach(c => c.Margin.SetValue(margin));
                await _lineItemService.UpdateLineItemAsync(lineItem);
                return Ok();
            }
            else
            {
                return NotFound();
            }
        }

        [HttpPost]
        [Route("api/setMarginsByPartNumber/{lineItemId}")]
        public async Task<IActionResult> SetMarginsByPartNumber(Guid lineItemId, string partNumber = "", double margin = 0.0)
        {
            var lineItem = await _lineItemService.GetLineItemAsync(lineItemId);

            if (lineItem.Root is ASLineItemPart lineItemPart)
            {
                var prtsToUpdate = lineItemPart.DistinctBomChildrenByPath.Value?
                                    .Where(c => c.PartNumber.Value == partNumber)?
                                    .ToList();

                if (prtsToUpdate != null && prtsToUpdate.Any())
                {
                    prtsToUpdate.ForEach(c => c.Margin.SetValue(margin));
                    await _lineItemService.UpdateLineItemAsync(lineItem);
                }
                
                return Ok();
            }
            else
            {
                return NotFound();
            }
        }

        [HttpPost]
        [Route("api/setItemByPartNumber/{lineItemId}")]
        public async Task<IActionResult> SetItemByPartNumber(Guid lineItemId, string partNumber = "", string item = "")
        {
            var lineItem = await _lineItemService.GetLineItemAsync(lineItemId);

            if (lineItem.Root is ASLineItemPart lineItemPart)
            {
                if (lineItemPart.ActualItemNumbers.Value.Any(n => n.Item2 == item)) { return BadRequest("Item number already exists."); }

                var prtsToUpdate = lineItemPart.DistinctBomChildrenByPath.Value?
                                    .Where(c => c.PartNumber.Value == partNumber)?
                                    .ToList();

                if (prtsToUpdate != null && prtsToUpdate.Any())
                {
                    prtsToUpdate.ForEach(c => c.Item.SetValue(item));
                    await _lineItemService.UpdateLineItemAsync(lineItem);
                }

                return Ok();
            }
            else
            {
                return NotFound();
            }
        }

        #endregion

    }
}
