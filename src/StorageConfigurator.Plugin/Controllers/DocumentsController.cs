﻿using StorageConfigurator.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Ruler.Configurator.Extensibility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.IO;
using System.IO.Compression;
using StorageConfigurator.Outputs;
using Ruler.Cpq;
using StorageConfigurator.Rules;

namespace StorageConfigurator.Plugin
{
    public class DocumentsController : Controller
    {

        #region Private Fields

        private readonly IProjectService _projectService;
        private readonly ICostSummaryService _costSummaryService;

        #endregion

        #region Constructor

        public DocumentsController(IProjectService projectService, ICostSummaryService costSummaryService)
        {
            _projectService = projectService;
            _costSummaryService = costSummaryService;
        }

        #endregion

        [HttpGet]
        [Route("api/projects/{projectId}/downloadOutputs")]
        public async Task<FileResult> DownloadOutputs(Guid projectId, bool includeQuoteForm = false,
                                                      bool includeCustomerBOMSummary = false,
                                                      bool includeInternalBOMSummary = false,
                                                      bool includeRowBOMSummary = false,
                                                      bool includeBOMDetail = false)
        {
            var reportGenerator = new ASReportGenerator();
            var bomSummarizer = new StorageBomSummarizer();
            var userIsAdmin = true; //TODO: Wire this up with user authentication
            var project = await _projectService.GetProjectAsync(projectId);
            var projectJson = JsonConvert.SerializeObject(project, new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                Converters = new List<JsonConverter>
                {
                    new ReportingPartJsonConverter()
                },
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });

            var costSummaries = await _costSummaryService.GetLineItemCostSummariesAsync(project.LineItems.Select(li => li.Id));
            var includedLineItems = project.LineItems.Where(l => costSummaries?.FirstOrDefault(s => s.LineItemId == l.Id)?.Include == true);
            var bomLineItems = includedLineItems?.Select(li => li.Root).OfType<Ruler.Cpq.IBomPart>();
            var lineItemSummaries = bomLineItems?.Select(p => bomSummarizer.GetBomSummary(p.Bom));

            IEnumerable<BomSummaryItem> rowBOMSummaries = null;
            if (includeRowBOMSummary)
            {
                var includedStorageBomLineItems = includedLineItems?.Select(li => li.Root).OfType<StorageSystem>();
                rowBOMSummaries = includedStorageBomLineItems?.Select(p => bomSummarizer.GetBomSummary(p.RowBom)) ?? null;
            }

            var stream = new MemoryStream();
            using (var zipArchive = new ZipArchive(stream, ZipArchiveMode.Create, true))
            {
                foreach (var document in await reportGenerator.GetReports(projectJson, lineItemSummaries, rowBOMSummaries,
                                                userIsAdmin, includeQuoteForm, includeCustomerBOMSummary, 
                                                includeInternalBOMSummary, includeRowBOMSummary, includeBOMDetail))
                {
                    var safeName = System.IO.Path.GetInvalidFileNameChars()
                        .Aggregate(document.Filename, (current, c) => current.Replace(c.ToString(), string.Empty));
                    var entry = zipArchive.CreateEntry(Path.Combine(document.ArchiveFolder, safeName), CompressionLevel.Fastest);
                    using (var entryStream = entry.Open())
                    {
                        new FileStream(document.TempPath, FileMode.Open).CopyTo(entryStream);
                    }
                }
            }
            stream.Seek(0, SeekOrigin.Begin);
            var fileName = reportGenerator.GetArchiveName(project);
            return File(stream, "application/zip", fileName);
        }

    }
}
