﻿using Microsoft.AspNetCore.Mvc;
using Ruler.Configurator.Extensibility;
using Ruler.Rules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StorageConfigurator.Data;
using StorageConfigurator.Rules;
using Ruler.Configurator;
using Ruler.Rules.Serialization;

namespace StorageConfigurator.Plugin.Controllers
{
    public class StorageConfiguratorController : Controller
    {

        #region Private Fields

        private readonly StorageConfiguratorRepository _repo;
        private readonly IPartService _partService;
        private readonly IPartSerializerSettings _partSerializerSettings;
        private readonly ILineItemService _lineItemService;

        #endregion

        public StorageConfiguratorController(StorageConfiguratorRepository repo, 
                                            IPartService partService, 
                                            IPartSerializerSettings partSerializerSettings,
                                            ILineItemService lineItemService)
        {
            _repo = repo;
            _partService = partService;
            _partSerializerSettings = partSerializerSettings;
            _lineItemService = lineItemService;
        }

        #region Actions

        [HttpGet("api/getAdvanceMargin")]
        public async Task<double> GetAdvanceMargin(string family = "", 
                                                   string customerType = "", 
                                                   double cost = 0)
        {
            return this._repo.GetMinMargin(family, customerType, cost);
        }

        [HttpPost("api/lineitems/{lineItemId}/addParts/{qty}")]
        public async Task<IActionResult> AddChildren(Guid lineItemId, 
                                                    int qty, 
                                                    [FromBody] AdhocChildCreateRequest createRequest)
        {
            object result = null;
            for (int i = 0; i < qty; i++)
            {
                if (result == null)
                {
                    result = await _partService.AddChildAsync(lineItemId, createRequest);
                }
                else
                {
                    await _partService.AddChildAsync(lineItemId, createRequest);
                }
            }

            return Json(result, _partSerializerSettings.WebFullModel);
        }

        [HttpPost("api/lineitems/{lineItemId}/addFrameType")]
        public async Task<IActionResult> AddFrameType(Guid lineItemId, [FromBody] AdhocChildCreateRequest createRequest)
        {
            object ftResult = await _partService.AddChildAsync(lineItemId, createRequest);
            object flResult = await _partService.AddChildAsync(lineItemId, new AdhocChildCreateRequest()
            {
                PartPath = createRequest.PartPath.Replace("FrameTypes", "FrameLines"),
                PartTypeCode = "selectiveFrameLine"
            });

            var ftPath = ((AdhocChildCreateResult)ftResult).NewChildPath;
            var lineItem = await _lineItemService.GetLineItemAsync(lineItemId);
            var frameLineComp = lineItem.Root.FindDescendantByRelativePath(((AdhocChildCreateResult)flResult).NewChildPath.Replace("Root.", ""));

            if (frameLineComp != null)
            {
                ((SelectiveFrameLine)frameLineComp).FrontFrameTypeName.SetValue(ftPath);
            }
            
            return Json(ftResult, _partSerializerSettings.WebFullModel); ;
        }

        [HttpPost("api/lineitems/{lineItemId}/parts/{childPath}/Advance")]
        public async Task<IActionResult> CopyChildren(Guid lineItemId, string childPath)
        {
            var lineItem = await _lineItemService.GetLineItemAsync(lineItemId);
            var result = await _partService.CopyChildAsync(lineItemId, childPath, GetCopyChildOptions(lineItem, childPath));

            return Json(result, _partSerializerSettings.WebFullModel);
        }

        #endregion

        #region Private Methods

        private CopyChildOptions GetCopyChildOptions(ILineItem lineItem, string childPath)
        {
            var retList = new List<RuleSelector>();
            var comp = lineItem.Root.FindDescendantByRelativePath(childPath.Replace("Root.", ""));

            if (comp.TypeName == "Bay")
            {
                var bayComp = ((Bay)comp);
                retList.Add(new RuleSelector { Name = bayComp.BayName.Name });
                retList.Add(new RuleSelector { Name = bayComp.BayDrawingNumber.Name });
                retList.Add(new RuleSelector { Name = bayComp.Index.Name });
            }
            else if (comp.TypeName == "Product")
            {
                var bayComp = ((Product)comp);
                retList.Add(new RuleSelector { Name = bayComp.ProductName.Name });
                retList.Add(new RuleSelector { Name = bayComp.Index.Name });
            }
            else if (comp is FrameLine)
            {
                var frameComp = ((FrameLine)comp);
                retList.Add(new RuleSelector { Name = frameComp.FrameName.Name });
                retList.Add(new RuleSelector { Name = frameComp.Index.Name });
            }
            else if (comp is FrameType)
            {
                var frameTypeComp = ((FrameType)comp);
                retList.Add(new RuleSelector { Name = frameTypeComp.FrameTypeName.Name });
                retList.Add(new RuleSelector { Name = frameTypeComp.Index.Name });
            }
            else if (comp is RowProxy)
            {
                var rowProxyComp = ((RowProxy)comp);
                retList.Add(new RuleSelector { Name = rowProxyComp.RowNumber.Name });
                retList.Add(new RuleSelector { Name = rowProxyComp.Index.Name });
            }
            else if (comp.TypeName == "Row")
            {
                var rowComp = ((Row)comp);
                retList.Add(new RuleSelector { Name = rowComp.RowTypeName.Name });
                retList.Add(new RuleSelector { Name = rowComp.Index.Name });
            }

            return new CopyChildOptions() { ExcludeRules = retList };
        }

        #endregion


    }
}
