﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;
using Ruler.Rules.Serialization;
using Ruler.Cpq;
using StorageConfigurator.Rules;

namespace StorageConfigurator.Plugin
{
    public class AdvanceIWebJsonProcessor : IWebJsonProcessor
    {

        public bool ShouldSerialize(IRuleHost ruleHost)
        {
            return true;
        }

        public bool ShouldSerializeRules(IRuleHost ruleHost)
        {
            if (ruleHost is BomData bomData)
            {
                if (bomData.Parent is Rules.CustomPart
                    || bomData.Parent is Rules.BuyoutPart
                    || bomData.Parent.IsRoot())
                {
                    return true;
                }
                return false;
            }
            else
            {
                return true;
            }
        }

    }
}
