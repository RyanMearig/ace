﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Ruler.Configurator.Extensibility;
using Ruler.Jobs;

namespace StorageConfigurator.Plugin
{
    public class CadJobBuilder : DefaultCadJobBuilder
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public CadJobBuilder(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public override string JobTypeName => "StorageConfiguratorModeler";

        public override Task<Dictionary<string, JobArgument>> BuildCustomParametersAsync(IProject project, ILineItem lineItem, Dictionary<string, object> additionalData)
        {
            var orderNumber = project.GetPropertyValueOrDefault("drawingNumber", string.Empty);
            var lineItemName = $"{orderNumber}.{lineItem.Name}";

            var parameters = new Dictionary<string, JobArgument>
            {
                { "DrawingNumber", new JobArgument(orderNumber) },
                { "ProjectName", new JobArgument(project.GetPropertyValueOrDefault<string>("name", string.Empty)) },
                { "LineItemName", new JobArgument(lineItemName) }
            };

            var emailAddress = _httpContextAccessor.HttpContext?.User?.FindFirst(c => c.Type == "email");
            if (emailAddress != null)
            {
                parameters.Add("EmailNotificationRecipients", new JobArgument(emailAddress.Value));
            };

            var extraData = AddToAdditionalDataJson(project);
            parameters.Add("ExtraData", new JobArgument(extraData));

            return Task.FromResult(parameters);
        }

        public ForgeJobOptions BuildForgeJobOptions(IProject project, ILineItem lineItem, Dictionary<string, object> additionalData) => throw new NotSupportedException();

        public string GetJobName(IProject project, ILineItem lineItem, Dictionary<string, object> additionalData) => lineItem.Name;

        public override string ProcessOccurrenceJson(string occurrenceJson, IProject project, ILineItem lineItem, Dictionary<string, object> additionalData)
        {
            var orderNumber = project.GetPropertyValueOrDefault("drawingNumber", string.Empty);
            var lineItemName = $"{orderNumber}.{lineItem.Name}";

            occurrenceJson = occurrenceJson.Replace("[[OrderNumber]]", orderNumber, StringComparison.InvariantCultureIgnoreCase);
            occurrenceJson = occurrenceJson.Replace("[[LineItemName]]", lineItemName, StringComparison.InvariantCultureIgnoreCase);

            return occurrenceJson;
        }

        public IEnumerable<ILineItem> GetLineItems(IProject project)
        {
            return project.LineItems
                .Where(lineItem => !string.IsNullOrEmpty(lineItem.Root.FactoryName.Value));
        }

        private static string AddToAdditionalDataJson(IProject proj)
        {
            var startAddDataJson = JObject.Parse(proj.AdditionalDataJson);

            var lineItemQty = new JProperty("totalLineItemQty", proj.LineItems.Count.ToString());
            startAddDataJson.Add(lineItemQty);

            return JsonConvert.SerializeObject(startAddDataJson, Formatting.Indented);
        }

        public void BeforeSerializeOccurrence(IProject project, ILineItem lineItem) { }
    }
}
