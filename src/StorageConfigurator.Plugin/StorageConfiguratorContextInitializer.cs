﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Ruler.Configurator.Extensibility;
using StorageConfigurator.Data;

namespace StorageConfigurator.Plugin
{
    public class StorageConfiguratorContextInitializer: IDatabaseInitializer
    {
        private readonly StorageConfiguratorContext _storageConfiguratorContext;
        private readonly ILogger _logger;

        public bool AllowReset => false;

        public StorageConfiguratorContextInitializer(StorageConfiguratorContext storageConfiguratorContext, ILogger<StorageConfiguratorContextInitializer> logger)
        {
            _storageConfiguratorContext = storageConfiguratorContext;
            _logger = logger;
        }

        public async Task MigrateAsync()
        {
            _logger.LogInformation($"Applying {nameof(StorageConfiguratorContext)} migrations");
            await _storageConfiguratorContext.Database.MigrateAsync();
        }

        public async Task UpdateDataAsync()
        {
            _logger.LogInformation($"Adding {nameof(StorageConfiguratorContext)} seed data");

            var seeder = new JsonResourceDbContextSeeder<StorageConfiguratorContext>(_storageConfiguratorContext, "StorageConfigurator.Data.SeedData");

            await seeder.AddSeedDataAsync<AdvanceColor>();
            await seeder.AddSeedDataAsync<AdvanceMargin>();
            await seeder.AddSeedDataAsync<AdvanceOverhead>();
        }

        public Task ClearDataAsync()
        {
            throw new NotSupportedException();
        }
    }
}
