﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Ruler.Cpq;
using Ruler.Rules.Configuration;

namespace StorageConfigurator.Plugin
{
    class MixedFileBomSeedDataProvider : IBomSeedDataProvider
    {
        private readonly Assembly _assembly;
        private readonly string _resourceNamespace;

        public MixedFileBomSeedDataProvider(Assembly assembly, string resourceNamespace)
        {
            _assembly = assembly;
            _resourceNamespace = resourceNamespace;
        }

        //public IEnumerable<ComponentDefinition> GetComponentDefinitions() => ResourceHelper.DeserializeEmbeddedResourceCollection<ComponentDefinition>(_assembly, GetResourceName);

        public IEnumerable<ComponentDefinition> GetComponentDefinitions()
        {
            var compDef = _assembly.GetManifestResourceNames().First(rn => rn.Contains("ComponentDefinition"));

            if (compDef.EndsWith(".json"))
            {
                return ResourceHelper.DeserializeEmbeddedResourceCollection<ComponentDefinition>(_assembly, GetResourceName);
            }
            else
            {
                return DeserializeTabDelimitedComponentDefinition(compDef);
            }
        }

        public IEnumerable<LaborClass> GetLaborClasses() => ResourceHelper.DeserializeEmbeddedResourceCollection<LaborClass>(_assembly, GetResourceName);

        public IEnumerable<LaborClassOperation> GetLaborClassOperations() => ResourceHelper.DeserializeEmbeddedResourceCollection<LaborClassOperation>(_assembly, GetResourceName);

        public IEnumerable<LaborOperation> GetLaborOperations() => ResourceHelper.DeserializeEmbeddedResourceCollection<LaborOperation>(_assembly, GetResourceName);

        public IEnumerable<MaterialClass> GetMaterialClasses() => ResourceHelper.DeserializeEmbeddedResourceCollection<MaterialClass>(_assembly, GetResourceName);

        public IEnumerable<MaterialClassMaterial> GetMaterialClassMaterials() => ResourceHelper.DeserializeEmbeddedResourceCollection<MaterialClassMaterial>(_assembly, GetResourceName);

        public IEnumerable<Material> GetMaterials() => ResourceHelper.DeserializeEmbeddedResourceCollection<Material>(_assembly, GetResourceName);

        public IEnumerable<Plant> GetPlants() => ResourceHelper.DeserializeEmbeddedResourceCollection<Plant>(_assembly, GetResourceName);

        public IEnumerable<WorkCenter> GetWorkCenters() => ResourceHelper.DeserializeEmbeddedResourceCollection<WorkCenter>(_assembly, GetResourceName);

        public IEnumerable<PlantWorkCenter> GetPlantWorkCenters() => ResourceHelper.DeserializeEmbeddedResourceCollection<PlantWorkCenter>(_assembly, GetResourceName);

        private string GetResourceName(Type resourceType) => $"{_resourceNamespace}.{resourceType.Name}.json";

        List<ComponentDefinition> DeserializeTabDelimitedComponentDefinition(string resourceName)
        {
            var compDefs = new List<ComponentDefinition>();

            using (var reader = new StreamReader(_assembly.GetManifestResourceStream(resourceName)))
            {
                var columnNames = reader.ReadLine().Split('\t');

                while (reader.Peek() >= 0)
                {
                    var rowValues = reader.ReadLine().Split('\t');

                    var compDef = new ComponentDefinition()
                    {
                        PartNumber = rowValues[0],
                        Description = rowValues[1],
                        ShortDescription = rowValues[2],
                        UnitOfMeasure = rowValues[3],
                        Cost = Convert.ToDouble(rowValues[4]),
                        Weight = Convert.ToDouble(rowValues[5])
                    };

                    compDefs.Add(compDef);
                }
            }

            return compDefs;
        }

    }
}
