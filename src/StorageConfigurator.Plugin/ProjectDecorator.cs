﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Ruler.Configurator.Extensibility;
using Ruler.Cpq;

namespace StorageConfigurator.Plugin
{
    public class ProjectDecorator : IProjectDecorator
    {
        #region Constructors

        public ProjectDecorator(ICostSummaryService costSummaryService)
        {
            _costSummaryService = costSummaryService;
        }

        #endregion

        #region Private Members

        ICostSummaryService _costSummaryService;

        #endregion

        public Task BeforeProjectAddedAsync(IProject project)
        {
            project.SetProperty("name", string.Empty);
            project.SetProperty("drawingNumber", string.Empty);

            project.SetProperty("advancedSalesName", string.Empty);
            project.SetProperty("advancedSalesContactPhone", string.Empty);
            project.SetProperty("advancedSalesContactEmail", string.Empty);
            project.SetProperty("advancedProjectManagerName", string.Empty);
            project.SetProperty("jobsiteName", string.Empty);
            project.SetProperty("jobsiteAddress", string.Empty);
            project.SetProperty("jobsiteCity", string.Empty);
            project.SetProperty("jobsiteState", string.Empty);
            project.SetProperty("jobsiteZip", string.Empty);
            project.SetProperty("jobsiteContactName", string.Empty);
            project.SetProperty("jobsiteContactPhone", string.Empty);
            project.SetProperty("jobsiteContactEmail", string.Empty);
            project.SetProperty("csiCustomerNumber", string.Empty);
            project.SetProperty("customerName", string.Empty);
            project.SetProperty("customerAddress", string.Empty);
            project.SetProperty("customerCity", string.Empty);
            project.SetProperty("customerState", string.Empty);
            project.SetProperty("customerZip", string.Empty);
            project.SetProperty("customerComments", string.Empty);
            project.SetProperty("customerContactName", string.Empty);
            project.SetProperty("customerContactPhone", string.Empty);
            project.SetProperty("customerContactEmail", string.Empty);
            project.SetProperty("leadTime", string.Empty);
            project.SetProperty("terms", string.Empty);
            project.SetProperty("seismicEngName", string.Empty);
            project.SetProperty("seismicEngAddress", string.Empty);
            project.SetProperty("seismicEngCity", string.Empty);
            project.SetProperty("seismicEngState", string.Empty);
            project.SetProperty("seismicEngZip", string.Empty);
            project.SetProperty("seismicEngContactName", string.Empty);
            project.SetProperty("seismicEngContactPhone", string.Empty);
            project.SetProperty("seismicEngContactEmail", string.Empty);
			
			project.SetProperty("jobStatus", "Open");
			project.SetProperty("winPercentage", "0");

            project.SetProperty("engCost", 0);
            project.SetProperty("engMarginPercent", 0);
            project.SetProperty("engPrice", 0);
            project.SetProperty("installCost", 0);
            project.SetProperty("installMarginPercent", 0);
            project.SetProperty("installPrice", 0);
            project.SetProperty("freightCost", 0);
            project.SetProperty("freightMarginPercent", 0);
            project.SetProperty("freightPrice", 0);
            project.SetProperty("otherCost", 0);
            project.SetProperty("otherMarginPercent", 0);
            project.SetProperty("otherPrice", 0);
            project.SetProperty("totalCost", 0);
            project.SetProperty("totalMarginPercent", 0);
            project.SetProperty("totalPrice", 0);

            project.SetProperty("taxRate", 0);
            project.SetProperty("totalPrice", 0);
            project.SetProperty("totalWTaxes", 0);
            project.SetProperty("totalWTaxes", 0);
            project.SetProperty("totalWTaxes", 0);
            project.SetProperty("totalWTaxes", 0);
            project.SetProperty("materialTaxed", true);
            project.SetProperty("engTaxed", true);
            project.SetProperty("installTaxed", true);
            project.SetProperty("freightTaxed", true);
            project.SetProperty("otherTaxed", true);
            project.SetProperty("holdPrice", false);

            project.SetProperty("paymentMilestonesJson", string.Empty);

            return Task.CompletedTask;
        }

        public async Task AfterProjectAddedAsync(IProject project)
        {
            var dto = GetEmptyCostOverride(project.Id);
            var costOverride = await _costSummaryService.AddProjectPriceOverrideAsync(dto);
            return;
        }

        public Task BeforeCopiedProjectAddedAsync(IProject project)
        {
            var originalName = project.GetPropertyValueOrDefault<string>("name");
            var newName = $"{originalName ?? string.Empty} - Copy";

            var ogDrawingNumber = project.GetPropertyValueOrDefault<string>("drawingNumber");

            project.SetProperty("name", newName);
            project.SetProperty("drawingNumber", ogDrawingNumber ?? string.Empty);

            return Task.CompletedTask;
        }

        public async Task AfterCopiedProjectAddedAsync(IProject project)
        {
            var dto = GetEmptyCostOverride(project.Id);
            var costOverride = await _costSummaryService.AddProjectPriceOverrideAsync(dto);
            return;
        }

        public Task BeforeProjectUpdatedAsync(IProject project) => Task.CompletedTask;

        public Task AfterProjectUpdatedAsync(IProject project) => Task.CompletedTask;

        private ProjectCostOverrideDto GetEmptyCostOverride(Guid projectId)
        {
            return new ProjectCostOverrideDto()
            {
                ProjectId = projectId,
                PriceOverride = 0,
                MarginOverride = 0,
                PriceLock = true,
                MarginLock = false
            };
        }

        public Task AfterProjectRetrievedAsync(IProject project) => Task.CompletedTask;
        
    }
}
