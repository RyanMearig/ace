﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Newtonsoft.Json;
using Ruler.Rules.Configuration;
using Ruler.Rules.Serialization;

namespace StorageConfigurator.Plugin
{
    public class JsonResourceSharedRulesSeedDataProvider : IRulesSeedDataProvider
    {

        #region Private Fields

        private readonly Assembly _assembly;
        private readonly string _singlePartResourceNamespace;
        private readonly string _sharedResourceNamespace;
        private readonly Dictionary<string, IEnumerable<string>> _partNameResourceMap;

        #endregion

        #region Constructors

        public JsonResourceSharedRulesSeedDataProvider(Assembly assembly, string singlePartResourceNamespace, string sharedResourceNamespace,
            Dictionary<string, IEnumerable<string>> partNameResourceMap)
        {
            _assembly = assembly;
            _singlePartResourceNamespace = singlePartResourceNamespace;
            _sharedResourceNamespace = sharedResourceNamespace;
            _partNameResourceMap = partNameResourceMap;
        }

        #endregion


        public async IAsyncEnumerable<RuleHostDefinition> GetRuleHostDefinitionsAsync()
        {
            var singlePartDataResourceNames = GetValidResourcenames(_singlePartResourceNamespace);
            var sharedDataResourceNames = GetValidResourcenames(_sharedResourceNamespace);
            var serializerSettings = new JsonSerializerSettings
            {
                Converters = new JsonConverter[] { new RuleDefinitionJsonConverter() }
            };

            var sharedRuleDefinitions = new List<SharedRuleDefinitions>();
            foreach (var resourceName in sharedDataResourceNames)
            {
                sharedRuleDefinitions.Add(await ResourceHelper.DeserializeEmbeddedResourceObjectAsync<SharedRuleDefinitions>(_assembly, resourceName, serializerSettings));
            }

            var ruleHostDefinitions = new List<RuleHostDefinition>();
            foreach (var resourceName in singlePartDataResourceNames)
            {
                var hostDef = await ResourceHelper.DeserializeEmbeddedResourceObjectAsync<RuleHostDefinition>(_assembly, resourceName, serializerSettings);
                ruleHostDefinitions.Add(hostDef);
            }

            foreach (var partResourceMap in _partNameResourceMap)
            {
                var existingHostDef = ruleHostDefinitions.FirstOrDefault(h => h.Name == partResourceMap.Key);
                var importedRuleDefinitions = sharedRuleDefinitions.Where(r => partResourceMap.Value.Contains(r.Key));
                var sharedRules = importedRuleDefinitions.SelectMany(r => r.Rules);
                var rulesToAdd = sharedRules.Select(r => DuplicateRuleDefinition(r));

                if(existingHostDef != null)
                {
                    var duplicateKeys = existingHostDef.Rules.Concat(rulesToAdd)
                        .Select(r => r.Name)
                        .GroupBy(n => n)
                        .Where(g => g.Count() > 1)
                        .Select(g => g.First());
                    
                    if(duplicateKeys.Any())
                    {
                        throw new Exception($"Error seeding database, duplicate key for {string.Join(", ", duplicateKeys)} in {existingHostDef.Name}");
                    }
                    existingHostDef.Rules.AddRange(rulesToAdd);
                    yield return existingHostDef;
                }
                else
                {
                    var newHostDef = new RuleHostDefinition()
                    {
                        Name = partResourceMap.Key,
                        Rules = rulesToAdd.ToList()
                    };
                    yield return newHostDef;
                }
            }

            var singleRuleHostDefs = ruleHostDefinitions.Where(h => !_partNameResourceMap.Keys.Contains(h.Name));
            var singleRuleHostDefNames = singleRuleHostDefs.Select(h => h.Name).ToList();
            foreach(var hostDef in singleRuleHostDefs)
            {
                yield return hostDef;
            }
        }

        private IEnumerable<string> GetValidResourcenames(string resourceNamespace)
        {
            return _assembly.GetManifestResourceNames()
                .Where(name => name.StartsWith($"{resourceNamespace}.") && name.EndsWith(".json"));
        }

        private RuleDefinition DuplicateRuleDefinition(RuleDefinition ruleDef)
        {
            if(ruleDef is LiteralRuleDefinition literalDef)
            {
                return new LiteralRuleDefinition()
                {
                    LiteralValue = literalDef.LiteralValue,
                    Name = literalDef.Name,
                };
            }
            if (ruleDef is ScriptRuleDefinition scriptDef)
            {
                return new ScriptRuleDefinition()
                {
                    Code = scriptDef.Code,
                    Name = scriptDef.Name
                };
            }
            if (ruleDef is TabularRuleDefinition tabularDef)
            {
                return new TabularRuleDefinition()
                { 
                    PossibleValues = tabularDef.PossibleValues.Select(v => DuplicateRuleValue(v)).ToList(),
                    Name = tabularDef.Name
                };
            }
            throw new Exception("Error seeding database, rule definition type not handled");
        }

        private RuleValue DuplicateRuleValue (RuleValue ruleValue)
        {
            var expressions = ruleValue.Expressions.Select(e => DuplicateRuleExpression(e));

            return new RuleValue()
            {
                Expressions = expressions.ToList(),
                Value = ruleValue.Value,
                SequenceNumber = ruleValue.SequenceNumber,
            };
        }

        private RuleExpression DuplicateRuleExpression(RuleExpression ruleExpression)
        {
            return new RuleExpression()
            {
                ExactValue = ruleExpression.ExactValue,
                IsRange = ruleExpression.IsRange,
                RangeHighValue = ruleExpression.RangeHighValue,
                RangeIncludesHighValue = ruleExpression.RangeIncludesHighValue,
                RangeIncludesLowValue = ruleExpression.RangeIncludesLowValue,
                RangeLowValue = ruleExpression.RangeLowValue,
                RuleName = ruleExpression.RuleName,
                SequenceNumber = ruleExpression.SequenceNumber,
            };
        }

    }
}
