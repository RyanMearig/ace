﻿using Ruler.Configurator.Extensibility;
using Ruler.Cpq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace StorageConfigurator.Outputs
{
    public interface IReportGenerator
    {
        Task<IEnumerable<Report>> GetReports(string projectJson, IEnumerable<BomSummaryItem> bomSummaries,
                                                            IEnumerable<BomSummaryItem> rowBomSummaries,
                                                            bool userIsAdmin,
                                                            bool includeQuoteForm = false,
                                                            bool includeCustomerBOMSummary = false,
                                                            bool includeInternalBOMSummary = false,
                                                            bool includeRowBOMSummary = false,
                                                            bool includeBOMDetail = false);
        string GetArchiveName(IProject project);
    }
}
