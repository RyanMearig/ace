﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StorageConfigurator.Outputs
{
    public class Report
    {
        public string Filename { get; set; }
        public string ArchiveFolder { get; set; }
        public string TempPath { get; set; }
    }
}
