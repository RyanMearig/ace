﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace StorageConfigurator.Outputs
{
    public class ResourceUtils
    {
        public static async Task<Stream> GetTemplateStream(string templateFilename)
        {
            var assembly = typeof(ResourceUtils).Assembly;
            using (var resourceStream = assembly.GetManifestResourceStream($"StorageConfigurator.Outputs.Template_Documents.{templateFilename}"))
            {
                var templateStream = new MemoryStream((int)resourceStream.Length);
                resourceStream.Seek(0, SeekOrigin.Begin);
                await resourceStream.CopyToAsync(templateStream);
                return templateStream;
            }
        }
    }
}
