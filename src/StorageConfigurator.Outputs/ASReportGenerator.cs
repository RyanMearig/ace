﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using Bold = DocumentFormat.OpenXml.Wordprocessing.Bold;
using Break = DocumentFormat.OpenXml.Wordprocessing.Break;
using Font = DocumentFormat.OpenXml.Wordprocessing.Font;
using Path = System.IO.Path;
using Run = DocumentFormat.OpenXml.Wordprocessing.Run;
using Text = DocumentFormat.OpenXml.Wordprocessing.Text;
using Paragraph = DocumentFormat.OpenXml.Wordprocessing.Paragraph;
using Newtonsoft.Json.Linq;
using Ruler.Configurator.Extensibility;
using DocumentFormat.OpenXml.Spreadsheet;
using RunProperties = DocumentFormat.OpenXml.Wordprocessing.RunProperties;
using FontSize = DocumentFormat.OpenXml.Wordprocessing.FontSize;
using Underline = DocumentFormat.OpenXml.Wordprocessing.Underline;
using UnderlineValues = DocumentFormat.OpenXml.Wordprocessing.UnderlineValues;
using Table = DocumentFormat.OpenXml.Wordprocessing.Table;
using TopBorder = DocumentFormat.OpenXml.Wordprocessing.TopBorder;
using BottomBorder = DocumentFormat.OpenXml.Wordprocessing.BottomBorder;
using LeftBorder = DocumentFormat.OpenXml.Wordprocessing.LeftBorder;
using RightBorder = DocumentFormat.OpenXml.Wordprocessing.RightBorder;
using Ruler.Cpq;
using Microsoft.EntityFrameworkCore.Internal;
using System.Text.RegularExpressions;
using Hyperlink = DocumentFormat.OpenXml.Spreadsheet.Hyperlink;
using Spire.Xls;
using Worksheet = DocumentFormat.OpenXml.Spreadsheet.Worksheet;
using Workbook = DocumentFormat.OpenXml.Spreadsheet.Workbook;
using PdfSharp.Drawing;
using PdfSharp.Pdf.IO;
using PdfSharp.Pdf;
using Newtonsoft.Json;

namespace StorageConfigurator.Outputs
{
    public class ASReportGenerator : IReportGenerator
    {

        #region Private Fields

        private RunFonts _font = new RunFonts() { Ascii = "Arial" };
        private readonly List<string> _upperClosures = new List<string> { "Top", "Left", "Front" };
        private readonly List<string> _lowerClosures = new List<string> { "Bottom", "Right", "Rear" };

        #endregion

        #region IReportGenerator Implementation

        public async Task<IEnumerable<Report>> GetReports(string projectJson, IEnumerable<BomSummaryItem> bomSummaries,
                                                IEnumerable<BomSummaryItem> rowBomSummaries, bool userIsAdmin,
                                                bool includeQuoteForm = false, bool includeCustomerBOMSummary = false,
                                                bool includeInternalBOMSummary = false, bool includeRowBOMSummary = false,
                                                bool includeBOMDetail = false)
        {
            dynamic project = JObject.Parse(projectJson);
            dynamic lineItems = project.lineItems;
            var excelOutputFileExt = userIsAdmin ? "xlsx" : "pdf";
            var baseArchiveFolder = $"{project.drawingNumber}-{project.name}";
            var reports = new List<Report>();
            if (lineItems != null)
            {
                foreach (var lineItem in lineItems)
                {
                    var bomSummary = bomSummaries.FirstOrDefault(t => String.Equals(t.ShortDescription, (string)lineItem.name, StringComparison.OrdinalIgnoreCase));
                    // BOM Summary == null if Line Item Include? = false
                    if (bomSummary != null)
                    {
                        if (includeBOMDetail)
                        {
                            reports.Add(new Report
                            {
                                Filename = $"{project.drawingNumber}.{lineItem.name}-BOMDetail.{excelOutputFileExt}",
                                TempPath = await CreateBOMDetailDoc(bomSummary, userIsAdmin),
                                ArchiveFolder = baseArchiveFolder
                            });
                        }
                        if (includeCustomerBOMSummary)
                        {
                            reports.Add(await GetBOMSummaryReport($"{project.drawingNumber}.{lineItem.name}-CustomerBOMSummary.{excelOutputFileExt}",
                                                            bomSummary, baseArchiveFolder, false, userIsAdmin, false));
                        }
                        if (includeInternalBOMSummary)
                        {
                            reports.Add(await GetBOMSummaryReport($"{project.drawingNumber}.{lineItem.name}-InternalBOMSummary.{excelOutputFileExt}",
                                                            bomSummary, baseArchiveFolder, true, userIsAdmin, false));
                        }
                    }
                    if (includeRowBOMSummary && rowBomSummaries != null)
                    {
                        var rowBomSummary = rowBomSummaries.FirstOrDefault(t => String.Equals(t.ShortDescription, (string)lineItem.name, StringComparison.OrdinalIgnoreCase));
                        if (rowBomSummary != null)
                        {
                            reports.Add(await GetBOMSummaryReport($"{project.drawingNumber}.{lineItem.name}-RowBOMSummary.{excelOutputFileExt}",
                                                            rowBomSummary, baseArchiveFolder, true, userIsAdmin, true));
                        }
                    }
                }

                if (includeQuoteForm)
                {
                    reports.Add(new Report
                    {
                        Filename = $"{project.drawingNumber}-QuoteForm.{excelOutputFileExt}",
                        TempPath = await CreateQuoteFormDoc(bomSummaries, projectJson, userIsAdmin),
                        ArchiveFolder = baseArchiveFolder
                    });
                }
            }
            
            return reports;
        }

        public async Task<Report> GetBOMSummaryReport(string filename, BomSummaryItem bomSummary, string archiveFolderName, 
                                                    bool isInternalBOMSummary, bool userIsAdmin, bool isRowBOMSummary)
        {
            return new Report
            {
                Filename = filename,
                TempPath = await CreateBOMSummaryDoc(bomSummary, isInternalBOMSummary, userIsAdmin, isRowBOMSummary),
                ArchiveFolder = archiveFolderName
            };
        }

        public string GetArchiveName(IProject project)
        {
            dynamic projJObj = JObject.Parse(project.AdditionalDataJson);
            return $"{projJObj.drawingNumber}.zip";
        }

        #endregion

        #region Public Methods

        public async Task<string> CreateBOMDetailDoc(BomSummaryItem bomSummary, bool userIsAdmin)
        {
            var excelPath = await CreateBOMDetailExcelDoc(bomSummary);
            
            if (userIsAdmin)
            {
                return excelPath;
            }
            else
            {
                return ConvertExcelDocToPdf(excelPath);
            }
        }

        public async Task<string> CreateBOMSummaryDoc(BomSummaryItem bomSummary, bool isInternalBOMSummary, bool userIsAdmin, bool isRowBOMSummary)
        {
            var excelPath = await CreateBOMSummaryExcelDoc(bomSummary, isInternalBOMSummary, isRowBOMSummary);

            if (userIsAdmin)
            {
                return excelPath;
            }
            else
            {
                return ConvertExcelDocToPdf(excelPath);
            }
        }

        public async Task<string> CreateQuoteFormDoc(IEnumerable<BomSummaryItem> bomSummaries, string projectJson, bool userIsAdmin)
        {
            var excelPath = await CreateQuoteFormExcelDoc(bomSummaries, projectJson);

            if (userIsAdmin)
            {
                return excelPath;
            }
            else
            {
                return ConvertExcelDocToPdf(excelPath);
            }
        }

        public async Task<string> CreateBOMDetailExcelDoc(BomSummaryItem bomSummary)
        {
            var outputPath = Path.GetTempFileName();

            using (var documentStream = await ResourceUtils.GetTemplateStream("BOMDetailTemplate.xlsx"))
            {
                using (var document = SpreadsheetDocument.Open(documentStream, true))
                {
                    document.ChangeDocumentType(SpreadsheetDocumentType.Workbook);

                    var worksheetPart = document.WorkbookPart.WorksheetParts.First();
                    var data = worksheetPart.Worksheet.Descendants<DocumentFormat.OpenXml.Spreadsheet.SheetData>().First();

                    if (bomSummary != null)
                    {
                        int headerIndex = 3;
                        UpdateRowIndices(worksheetPart.Worksheet, headerIndex+1);
                        //var headerRow = worksheetPart.Worksheet.Descendants<DocumentFormat.OpenXml.Spreadsheet.Row>().First(r => r.RowIndex.Value == (uint)(headerIndex + 1));
                        //var header = BuildHeader(bomSummary, headerIndex);
                        //data.InsertBefore<DocumentFormat.OpenXml.Spreadsheet.Row>(header, headerRow); 
                        var rows = new List<Row>();
                        foreach (var lineItem in BuildBOMDetailItems(bomSummary, rows))
                        {
                            data.AppendChild(lineItem);
                        }
                    }
                }

                using (var fileStream = File.OpenWrite(outputPath))
                {
                    documentStream.Seek(0, SeekOrigin.Begin);
                    await documentStream.CopyToAsync(fileStream);
                }
            }

            return outputPath;
        }

        public async Task<string> CreateBOMSummaryExcelDoc(BomSummaryItem bomSummary, bool isInternalBOMSummary, 
                                                            bool isRowBOMSummary)
        {
            var outputPath = Path.GetTempFileName();
            var tempName = isInternalBOMSummary ? "InternalBOMSummaryTemplate.xlsx" : "CustomerBOMSummaryTemplate.xlsx";

            using (var documentStream = await ResourceUtils.GetTemplateStream(tempName))
            {
                using (var document = SpreadsheetDocument.Open(documentStream, true))
                {
                    document.ChangeDocumentType(SpreadsheetDocumentType.Workbook);

                    var worksheetPart = document.WorkbookPart.WorksheetParts.First();
                    var data = worksheetPart.Worksheet.Descendants<DocumentFormat.OpenXml.Spreadsheet.SheetData>().First();

                    if (bomSummary != null)
                    {
                        var firstLevelChildDepth = isRowBOMSummary ? 2 : 1;
                        int headerIndex = 3;
                        UpdateRowIndices(worksheetPart.Worksheet, headerIndex + 1); 
                        var rows = new List<Row>();
                        foreach (var lineItem in BuildBOMSummaryItems(bomSummary, rows, isInternalBOMSummary, firstLevelChildBomDepth: firstLevelChildDepth))
                        {
                            data.AppendChild(lineItem);
                        }
                    }
                }

                using (var fileStream = File.OpenWrite(outputPath))
                {
                    documentStream.Seek(0, SeekOrigin.Begin);
                    await documentStream.CopyToAsync(fileStream);
                }
            }

            return outputPath;
        }

        public async Task<string> CreateQuoteFormExcelDoc(IEnumerable<BomSummaryItem> bomSummaries, string projectJson)
        {
            var outputPath = Path.GetTempFileName();
            dynamic project = JObject.Parse(projectJson);

            using (var documentStream = await ResourceUtils.GetTemplateStream("QuoteFormTemplate.xlsx"))
            {
                using (var document = SpreadsheetDocument.Open(documentStream, true))
                {
                    document.ChangeDocumentType(SpreadsheetDocumentType.Workbook);
                    var workbookPrt = document.WorkbookPart;

                    try
                    {
                        PopulateProjectProperties(projectJson, workbookPrt);
                        PopulateBomItems(projectJson, bomSummaries, workbookPrt);
                    }
                    catch (Exception ex)
                    {
                        string temp = "";
                    }

                    workbookPrt.Workbook.CalculationProperties.ForceFullCalculation = true;
                    workbookPrt.Workbook.CalculationProperties.FullCalculationOnLoad = true;
                    document.DeletePartsRecursivelyOfType<CalculationChainPart>();
                }

                using (var fileStream = File.OpenWrite(outputPath))
                {
                    documentStream.Seek(0, SeekOrigin.Begin);
                    await documentStream.CopyToAsync(fileStream);
                }
            }

            return outputPath;
        }

        #endregion


        #region Private Methods

        private void UpdateRowIndices(DocumentFormat.OpenXml.Spreadsheet.Worksheet worksheet, int indexToInsert)
        {
            foreach (var row in worksheet.Descendants<DocumentFormat.OpenXml.Spreadsheet.Row>().Where(r => r.RowIndex >= indexToInsert))
            {
                var newIndex = row.RowIndex + 1;
                foreach (var cell in row.Descendants<DocumentFormat.OpenXml.Spreadsheet.Cell>())
                {
                    cell.CellReference = new StringValue(cell.CellReference.Value.Replace(row.RowIndex.ToString(), newIndex.ToString()));
                }
                row.RowIndex = newIndex;
            }
        }

        private DocumentFormat.OpenXml.Spreadsheet.Row BuildHeader(BomSummaryItem bomSummary, int rowIndex)
        {
            var displayName = bomSummary.ShortDescription;

            var row = new DocumentFormat.OpenXml.Spreadsheet.Row() { RowIndex = (uint)rowIndex };
            row.Append(
                BuildCell<string>(displayName)
            );
            return row;
        }

        #region BOM Detail

        private IEnumerable<Row> BuildBOMDetailItems(BomSummaryItem summary, List<Row> rows, int bomDepth = 0)
        {
            var newRows = BuildBOMDetailItem(summary, bomDepth);
            rows.AddRange(newRows);
            summary.Children.ForEach(c => BuildBOMDetailItems(c, rows, bomDepth + 1));
            return rows;
        }

        private IEnumerable<Row> BuildBOMDetailItem(BomSummaryItem summary, int bomDepth)
        {
            var newRows = new List<Row>();
            newRows.Add(GetBOMDetailItemRow(summary, bomDepth));
            newRows.AddRange(BuildLabor(summary));
            return newRows;
        }

        private Row GetBOMDetailItemRow(BomSummaryItem summary, int bomDepth)
        {
            var row = new Row();
            UInt32Value decimalStyleIndex = 9;
            UInt32Value percentStyleIndex = 8;
            UInt32Value currencyStyleIndex = 5;
            row.Append(BuildCell<string>(summary.Item));
            row.Append(BuildCell<string>($"{GetBOMIndention(bomDepth)}{summary.ShortDescription}"));
            row.Append(BuildCell<string>(summary.Description));
            row.Append(BuildCell<string>(summary.MaterialFullDescription));
            row.Append(BuildCell<string>(summary.ItemUnitOfMeasure));
            row.Append(BuildCell<double>(summary.Qty));
            row.Append(BuildCell<double>(GetLaborHoursByCategory("Non-Weld", summary), decimalStyleIndex));
            row.Append(BuildCell<double>(GetLaborHoursByCategory("Weld", summary), decimalStyleIndex));
            row.Append(BuildCell<double>(summary.TotalLaborHours, decimalStyleIndex));
            row.Append(BuildCell<double>(summary.TotalLaborCost, currencyStyleIndex));
            row.Append(BuildCell<double>(summary.UnitCostNoLabor, currencyStyleIndex));
            row.Append(BuildCell<double>(summary.TotalCostNoLabor, currencyStyleIndex));
            row.Append(BuildCell<double>(summary.TotalCost, currencyStyleIndex));

            if (bomDepth <= 1)
            {
                row.Append(BuildCell<double>(summary.UnitMargin/100, percentStyleIndex));
                row.Append(BuildCell<double>(summary.TotalPrice, currencyStyleIndex));
            }
            else
            {
                row.Append(new Cell());
                row.Append(new Cell());
            }

            return row;
        }

        private double GetLaborHoursByCategory(string categoryName, BomSummaryItem summary)
        {
            var labors = summary.Labor?.Where(s => s.Category.ToLower() == categoryName.ToLower());
            var hours = labors?.Sum(l => l.Hours);

            summary.Children.ForEach(c => hours = hours + GetLaborHoursByCategory(categoryName, c));

            return (hours * summary.Qty) ?? 0;
        }

        private IEnumerable<Row> BuildLabor(BomSummaryItem summary)
        {
            var rows = new List<Row>();
            summary.Labor.ForEach(l => rows.Add(GetLaborRow(l)));
            return rows;
        }

        private Row GetLaborRow(LaborSummaryItem labor)
        {
            var row = new Row();
            UInt32Value decimalStyleIndex = 9;
            UInt32Value currencyStyleIndex = 5;
            row.Append(new Cell());
            row.Append(new Cell());
            row.Append(BuildCell<string>(labor.Name));
            row.Append(new Cell());
            row.Append(new Cell());
            row.Append(new Cell());
            if (labor.Category.ToLower() == "non-weld")
            {
                row.Append(BuildCell<double>(labor.Hours, decimalStyleIndex));
                row.Append(new Cell());
            }
            else
            {
                row.Append(new Cell());
                row.Append(BuildCell<double>(labor.Hours, decimalStyleIndex));
            }
            row.Append(BuildCell<double>(labor.Hours, decimalStyleIndex));
            row.Append(BuildCell<double>(labor.Cost, currencyStyleIndex));

            return row;
        }

        #endregion

        #region BOM Summary

        private IEnumerable<Row> BuildBOMSummaryItems(BomSummaryItem summary, List<Row> rows, bool isInternalBOMSummary, 
                                                        int bomDepth = 0, int firstLevelChildBomDepth = 1)
        {
            if (isInternalBOMSummary)
            {
                rows.Add(GetInternalBOMSummaryItemRow(summary, bomDepth, firstLevelChildBomDepth));
            }
            else
            {
                rows.Add(GetCustomerBOMSummaryItemRow(summary, bomDepth, firstLevelChildBomDepth));
            }

            summary.Children.ForEach(c =>
            {
                if (bomDepth + 1 <= firstLevelChildBomDepth)
                {
                    BuildBOMSummaryItems(c, rows, isInternalBOMSummary, bomDepth + 1, firstLevelChildBomDepth);
                }
            });
            return rows;
        }

        private Row GetInternalBOMSummaryItemRow(BomSummaryItem summary, int bomDepth, int firstLevelChildDepth)
        {
            var row = new Row();
            UInt32Value decimalStyleIndex = 6;
            UInt32Value percentStyleIndex = 5;
            UInt32Value currencyStyleIndex = 3;
            row.Append(BuildCell<string>(summary.Item));
            row.Append(BuildCell<string>($"{GetBOMIndention(bomDepth)}{summary.ShortDescription}"));
            row.Append(BuildCell<string>(summary.Description));
            row.Append(BuildCell<string>(summary.MaterialFullDescription));
            row.Append(BuildCell<double>(summary.Qty));
            row.Append(BuildCell<double>(GetLaborHoursByCategory("Non-Weld", summary), decimalStyleIndex));
            row.Append(BuildCell<double>(GetLaborHoursByCategory("Weld", summary), decimalStyleIndex));
            row.Append(BuildCell<double>(summary.TotalLaborHours, decimalStyleIndex));
            row.Append(BuildCell<double>(summary.TotalWeight, decimalStyleIndex));
            row.Append(BuildCell<double>(summary.CostPerWeight, currencyStyleIndex));
            row.Append(BuildCell<double>(summary.TotalCost, currencyStyleIndex));

            if (bomDepth <= firstLevelChildDepth)
            {
                row.Append(BuildCell<double>(summary.UnitMargin/100, percentStyleIndex));
                row.Append(BuildCell<double>(summary.TotalPrice, currencyStyleIndex));
            }
            else
            {
                row.Append(new Cell());
                row.Append(new Cell());
            }

            return row;
        }

        private Row GetCustomerBOMSummaryItemRow(BomSummaryItem summary, int bomDepth, int firstLevelChildDepth)
        {
            var row = new Row();
            row.Append(BuildCell<string>(summary.Item));
            row.Append(BuildCell<string>($"{GetBOMIndention(bomDepth)}{summary.ShortDescription}"));
            row.Append(BuildCell<string>(summary.Description));
            row.Append(BuildCell<string>(summary.MaterialFullDescription));
            row.Append(BuildCell<double>(summary.Qty));
            row.Append(BuildCell<double>(summary.TotalWeight, 5));
            
            if (bomDepth <= firstLevelChildDepth)
            {
                row.Append(BuildCell(summary.TotalPrice, 2));
            }
            else
            {
                row.Append(new Cell());
            }

            return row;
        }

        #endregion

        #region Quote Form

        private void PopulateProjectProperties(string projectJson, WorkbookPart workbookPrt)
        {
            dynamic project = JObject.Parse(projectJson);
            dynamic lineItems = project.lineItems;
            var definedNames = workbookPrt.Workbook.DefinedNames?.Cast<DefinedName>().ToList();

            //Customer Info
            SetCellByDefinedName(definedNames, workbookPrt, "CustAddr1", $"{project.customerName}");
            SetCellByDefinedName(definedNames, workbookPrt, "CustAddr2", $"{project.customerAddress}");
            SetCellByDefinedName(definedNames, workbookPrt, "CustAddr3", $"{project.customerCity}, {project.customerState} {project.customerZip}");
            SetCellByDefinedName(definedNames, workbookPrt, "CustContact", $"{project.customerContactName}");
            SetCellByDefinedName(definedNames, workbookPrt, "CustEmail", $"{project.customerContactEmail}");
            SetCellByDefinedName(definedNames, workbookPrt, "CustPhone", $"{project.customerContactPhone}");

            //Ship Info
            SetCellByDefinedName(definedNames, workbookPrt, "ShipAddr1", $"{project.jobsiteName}");
            SetCellByDefinedName(definedNames, workbookPrt, "ShipAddr2", $"{project.jobsiteAddress}");
            SetCellByDefinedName(definedNames, workbookPrt, "ShipAddr3", $"{project.jobsiteCity}, {project.jobsiteState} {project.jobsiteZip}");
            SetCellByDefinedName(definedNames, workbookPrt, "ShipContact", $"{project.jobsiteContactName}");
            SetCellByDefinedName(definedNames, workbookPrt, "ShipEmail", $"{project.jobsiteContactEmail}");
            SetCellByDefinedName(definedNames, workbookPrt, "ShipPhone", $"{project.jobsiteContactPhone}");

            //Sales Info
            SetCellByDefinedName(definedNames, workbookPrt, "Quote_No", $"{project.drawingNumber}");
            SetCellByDefinedName(definedNames, workbookPrt, "SalesPerson", $"{project.advancedSalesName}");
            SetCellByDefinedName(definedNames, workbookPrt, "SalesEmail", $"{project.advancedSalesContactEmail}");
            SetCellByDefinedName(definedNames, workbookPrt, "Phone_No", $"{project.advancedSalesContactPhone}");
            SetCellByDefinedName(definedNames, workbookPrt, "FOBFactory", lineItems.First.root.fOBFactory.Value);
            SetCellByDefinedName(definedNames, workbookPrt, "Terms", $"{project.terms}");
            SetCellByDefinedName<string>(definedNames, workbookPrt, "Lead_Time", $"{project.leadTime}");

            //Top Level Pricing
            SetCellByDefinedName(definedNames, workbookPrt, "MaterialPrice",
                                GetDoubleOrDefault($"{project.mfdMaterialPrice}") + GetDoubleOrDefault($"{project.buyoutMaterialPrice}"));
            SetCellByDefinedName(definedNames, workbookPrt, "InstallPrice", GetDoubleOrDefault($"{project.installPrice}"));
            SetCellByDefinedName(definedNames, workbookPrt, "FreightPrice", GetDoubleOrDefault($"{project.freightPrice}"));
            SetCellByDefinedName(definedNames, workbookPrt, "OtherPrice", 
                                GetDoubleOrDefault($"{project.otherPrice}") + GetDoubleOrDefault($"{project.engPrice}"));

            SetCellByDefinedName(definedNames, workbookPrt, "Surcharge", GetDoubleOrDefault($"{project.steelSurcharge}"));

            //Sales Tax
            SetCellByDefinedName(definedNames, workbookPrt, "TaxRate", GetDoubleOrDefault($"{project.taxRate}")/100);
            SetCellByDefinedName(definedNames, workbookPrt, "MaterialTaxed", $"{project.materialTaxed}");
            SetCellByDefinedName(definedNames, workbookPrt, "InstallTaxed", $"{project.installTaxed}");
            SetCellByDefinedName(definedNames, workbookPrt, "FreightTaxed", $"{project.freightTaxed}");
            SetCellByDefinedName(definedNames, workbookPrt, "OtherTaxed", $"{project.otherTaxed}");
        }

        private double GetDoubleOrDefault(string value, double defaultValue = 0)
        {
            double dblVal = double.TryParse(value, out dblVal) ? dblVal : defaultValue;
            return dblVal;
        }

        private bool GetBooleanOrDefault(string value, bool defaultValue = false)
        {
            bool boolVal = Boolean.TryParse(value, out boolVal) ? boolVal : defaultValue;
            return boolVal;
        }

        private void PopulateBomItems(string projectJson, IEnumerable<BomSummaryItem> summaries, WorkbookPart workbook)
        {

            var combinedSummaries = CombineBomSummaries(summaries);

            var worksheetPart = workbook.WorksheetParts.FirstOrDefault(p => GetSheetFromWorkSheet(workbook, p).Name == "BOM");
            var calcChain = (CalculationChain)workbook.GetPartsOfType<CalculationChainPart>().First().RootElement;

            uint templateIndex = 20;
            var templateRow = worksheetPart.Worksheet.Descendants<Row>().First(r => r.RowIndex.Value == templateIndex);
            var summaryRows = new List<Row>();
            dynamic project = JObject.Parse(projectJson);
            var includeTotals = Math.Round(GetDoubleOrDefault($"{project.totalPrice}"), 2) == Math.Round(GetDoubleOrDefault($"{project.totalCalcPrice}"), 2);
            foreach (var summary in combinedSummaries)
            {
                summaryRows.AddRange(BuildBomItemRows(summary, includeTotals, templateRow).ToList());
            }

            summaryRows.AddRange(BuildProjectBomItemRows(projectJson, includeTotals, templateRow));
            templateRow.Remove();

            var previousRow = templateRow;
            foreach (var row in summaryRows)
            {
                InsertRow(templateIndex, worksheetPart, row);
                templateIndex++;
            }
            var lastIndex = UpdateRowIndices(worksheetPart.Worksheet, calcChain);
            worksheetPart.Worksheet.SheetDimension.Reference = $"A1:{lastIndex}";

        }

        private List<BomSummaryItem> CombineBomSummaries(IEnumerable<BomSummaryItem> summaries)
        {
            List<BomSummaryItem> flatSummaries = new List<BomSummaryItem>();
            
            foreach (var summary in summaries)
            {
                flatSummaries.AddRange(FlattenBomSummaries(summary).ToList());
            }
            
            var group = flatSummaries.GroupBy(i => new { i.Item, i.PartNumber, i.ShortDescription });

            List<BomSummaryItem> rSummaries = new List<BomSummaryItem>();
            foreach (var item in group)
            {
                var firstItem = item.First();
                firstItem.Qty = item.Sum(i => i.Qty);
                firstItem.TotalPrice = item.Sum(i => i.TotalPrice);
                firstItem.TotalWeight = item.Sum(i => i.TotalWeight);
                rSummaries.Add(firstItem);
            }

            return rSummaries;
        }

        private IEnumerable<BomSummaryItem> FlattenBomSummaries(BomSummaryItem summary)
        {
            var list = new List<BomSummaryItem>();
            summary.Children.ForEach(c => list.Add(FlattenBomSummary(c)));
            return list;
        }

        private BomSummaryItem FlattenBomSummary(BomSummaryItem summary)
        {
            return new BomSummaryItem()
            {
                Children = new List<BomSummaryItem>(),
                Item = summary.Item,
                ItemBomLaborCost = summary.ItemBomLaborCost,
                ItemBomLaborPrice = summary.ItemBomLaborPrice,
                ItemCostNoLabor = summary.ItemCostNoLabor,
                ItemLaborSetupCost = summary.ItemLaborSetupCost,
                ItemLaborSetupHours = summary.ItemLaborSetupHours,
                ItemLaborSetupPrice = summary.ItemLaborSetupPrice,
                ItemManuallyEnteredLaborHours = summary.ItemManuallyEnteredLaborHours,
                ItemMaterialQty = summary.ItemMaterialQty,
                ItemPriceNoLabor = summary.ItemPriceNoLabor,
                ItemUnitOfMeasure = summary.ItemUnitOfMeasure,
                ItemWeight = summary.ItemWeight,
                Description = summary.Description,
                MaterialFullDescription = summary.MaterialFullDescription,
                ShortDescription = summary.ShortDescription,
                SetupLabor = summary.SetupLabor,
                Labor = summary.Labor,
                LaborChildrenAlreadyCalculated = summary.LaborChildrenAlreadyCalculated,
                LaborChildrenQtyCalculated = summary.LaborChildrenQtyCalculated,
                MaterialPartNumber = summary.MaterialPartNumber,
                MtrRequired = summary.MtrRequired,
                Notes = summary.Notes,
                OverriddenUnitPrice = summary.OverriddenUnitPrice,
                UseOverriddenUnitPrice = summary.UseOverriddenUnitPrice,
                UnitMargin = summary.UnitMargin,
                PartNumber = summary.PartNumber,
                Path = summary.Path,
                Qty = summary.Qty,
                TotalCost = summary.TotalCost,
                TotalPrice = summary.TotalPrice,
                TotalWeight = summary.TotalWeight
            };
        }

        private List<Row> BuildProjectBomItemRows(string projectJson, bool includeTotals, Row templateRow = null)
        {
            var rows = new List<Row>();
            dynamic project = JObject.Parse(projectJson);
            var installPrice = GetDoubleOrDefault($"{project.installPrice}");
            var freightPrice = GetDoubleOrDefault($"{project.freightPrice}");
            var otherPrice = GetDoubleOrDefault($"{project.otherPrice}") + GetDoubleOrDefault($"{project.engPrice}");
            var numberingIndex = 970;
            
            if (installPrice > 0) 
            { 
                rows.Add(BuildProjectBomItemRow(numberingIndex.ToString(), "Install", installPrice, includeTotals, templateRow)); 
                numberingIndex++; 
            }
            if (freightPrice > 0) 
            { 
                rows.Add(BuildProjectBomItemRow(numberingIndex.ToString(), "Freight", freightPrice, includeTotals, templateRow));
                numberingIndex++;
            }
            if (otherPrice > 0) 
            { 
                rows.Add(BuildProjectBomItemRow(numberingIndex.ToString(), "Other", otherPrice, includeTotals, templateRow));
            }

            return rows;
        }

        private Row BuildProjectBomItemRow(string itemNumber, string description, double totalPrice, bool includeTotals, Row templateRow = null)
        {
            var row = templateRow == null ? new Row() : (Row)templateRow.Clone();
            var templateCells = row.Descendants<Cell>().ToList();
            if (templateCells.Count() > 0)
            {
                var ttlPrice = double.IsNaN(totalPrice) ? 0 : totalPrice;
                templateCells[2] = UpdateCellValue(includeTotals ? ttlPrice : 0, templateCells[2]);
                templateCells[3] = UpdateCellValue(itemNumber, templateCells[3]);
                templateCells[5] = UpdateCellValue(description, templateCells[5]);
                templateCells[8] = UpdateCellValue(1, templateCells[8]);
                templateCells[11].CellValue = new CellValue(string.Empty);
            }
            else
            {
                var unitPriceCell = BuildCell(includeTotals ? totalPrice : 0);
                var totalPriceCell = BuildCell(includeTotals ? totalPrice : 0);
                unitPriceCell.StyleIndex = 164;
                totalPriceCell.StyleIndex = 164;

                row.Append(new Cell());
                row.Append(new Cell());
                row.Append(new Cell());
                row.Append(new Cell());
                row.Append(new Cell());
                row.Append(BuildCell(itemNumber));
                row.Append(new Cell());
                row.Append(BuildCell(description));
                row.Append(new Cell());
                row.Append(new Cell());
                row.Append(BuildCell(1));
                row.Append(new Cell());
                row.Append(new Cell());
                row.Append(new Cell());
                row.Append(unitPriceCell);
                row.Append(totalPriceCell);
            }

            return row;
        }

        private IEnumerable<Row> BuildBomItemRows(BomSummaryItem summary, bool includeTotals, Row templateRow = null)
        {
            var rows = new List<Row>();
            return BuildBomItemRows(summary, rows, includeTotals, templateRow);
        }

        private IEnumerable<Row> BuildBomItemRows (BomSummaryItem summary, List<Row> rows, bool includeTotals, Row templateRow = null)
        {
            rows.AddRange(BuildBomItemRow(summary, includeTotals, templateRow));
            foreach (var child in summary.Children)
            {
                rows.AddRange(BuildBomItemRow(child, includeTotals, templateRow));
            }
            return rows;
        }

        private IEnumerable<Row> BuildBomItemRow(BomSummaryItem summary, bool includeTotals, Row templateRow = null)
        {
            var row = templateRow == null ? new Row() : (Row)templateRow.Clone();
            var templateCells = row.Descendants<Cell>().ToList();
            if (templateCells.Count() > 0)
            {
                var totalPrice = double.IsNaN(summary.TotalPrice) ? 0 : summary.TotalPrice;
                templateCells[2] = UpdateCellValue(includeTotals ? totalPrice : 0, templateCells[2]);
                templateCells[3] = UpdateCellValue(summary.Item, templateCells[3]);
                templateCells[5] = UpdateCellValue(summary.ShortDescription, templateCells[5]);
                templateCells[8] = UpdateCellValue(summary.Qty, templateCells[8]);
                templateCells[11] = UpdateCellValue(summary.TotalWeight, templateCells[11]);
            } else
            {
                var unitPriceCell = BuildCell(includeTotals ? summary.UnitPrice : 0);
                var totalPriceCell = BuildCell(includeTotals ? summary.TotalPrice : 0);
                unitPriceCell.StyleIndex = 164;
                totalPriceCell.StyleIndex = 164;

                row.Append(new Cell());
                row.Append(new Cell());
                row.Append(new Cell());
                row.Append(new Cell());
                row.Append(new Cell());
                row.Append(BuildCell(summary.Item));
                row.Append(new Cell());
                row.Append(BuildCell(summary.ShortDescription));
                row.Append(new Cell());
                row.Append(new Cell());
                row.Append(BuildCell(summary.Qty));
                row.Append(BuildCell(summary.ItemWeight));
                row.Append(new Cell());
                row.Append(BuildCell(summary.TotalWeight));
                row.Append(unitPriceCell);
                row.Append(totalPriceCell);
            }
            
            var rows = new List<Row>() { row };
            if (string.IsNullOrEmpty(summary.Notes)) return rows;
            var descriptions = summary.Notes.Split('|');
            if (descriptions.Length > 0)
            {
                rows.AddRange(descriptions.Select(d => BuildDescriptionRow(d, templateRow)));
            }
            return rows;
        }

        private Row BuildDescriptionRow(string desc, Row templateRow = null)
        {
            var row = templateRow == null ? new Row() : (Row)templateRow.Clone();
            var templateCells = row.Descendants<Cell>().ToList();
            if (templateCells.Count() > 0)
            {
                templateCells.ForEach(c => UpdateCellValue("", c));
                templateCells.ForEach(c => c.CellFormula = null);
                templateCells[5] = UpdateCellValue(desc, templateCells[5]);                
            }
            else
            {
                row.Append(new Cell());
                row.Append(new Cell());
                row.Append(new Cell());
                row.Append(new Cell());
                row.Append(new Cell());
                row.Append(new Cell());
                row.Append(new Cell());
                row.Append(BuildCell(desc));
            }
            
            return row;
        }

        #endregion

        #region Excel Helper Methods

        public void SetCellByDefinedName<T>(List<DefinedName> definedNames, WorkbookPart workbookPrt, string definedName,
                                        T cellValue)
        {
            var namedCell = GetCellByDefinedName(definedNames, workbookPrt, definedName);

            switch (Type.GetTypeCode(typeof(T)))
            {
                case TypeCode.Single:
                case TypeCode.Double:
                case TypeCode.Int32:
                    namedCell.DataType = CellValues.Number;
                    break;
                case TypeCode.Boolean:
                    namedCell.DataType = CellValues.Boolean;
                    break;
                default:
                    namedCell.DataType = CellValues.String;
                    break;
            }

            namedCell.CellValue = new CellValue(cellValue?.ToString() ?? string.Empty);
        }

        public Cell GetCellByDefinedName(List<DefinedName> definedNames, WorkbookPart workbookPrt, string definedName)
        {
            var definedCellText = definedNames?.FirstOrDefault(d => d.Name == definedName)?.InnerText ?? "";
            if (string.IsNullOrWhiteSpace(definedCellText)) return null;

            return GetCellByNamePath(workbookPrt, definedCellText);
        }

        public string GetDefinedNameLoc(Workbook workbook, string name)
        {
            return workbook.DefinedNames?.Cast<DefinedName>().FirstOrDefault(d => d.Name == name)?.InnerText ?? "";
        }

        public Cell GetCellByNamePath(WorkbookPart workbookPrt, string namedCellLocText)
        {
            if (string.IsNullOrWhiteSpace(namedCellLocText)) return null;
            var cellLocSplit = namedCellLocText.Split("!");
            var sheetName = cellLocSplit.FirstOrDefault();
            var cellCode = cellLocSplit.LastOrDefault().Replace("$", "");

            var sheetPrt = GetSheetByName(workbookPrt, sheetName);
            return GetCellByName(sheetPrt.Worksheet, cellCode);
        }

        public WorksheetPart GetSheetByName(WorkbookPart workbookPrt, string sheetName)
        {
            IEnumerable<Sheet> sheets = workbookPrt.Workbook.Descendants<Sheet>().Where(s => s.Name == sheetName);
            if (sheets.Count() == 0) return null;

            string relationshipId = sheets.FirstOrDefault().Id.Value;
            WorksheetPart worksheetPart = (WorksheetPart)workbookPrt.GetPartById(relationshipId);
            return worksheetPart;
        }

        private Cell GetCellByName(Worksheet worksheet, string cellCode)
        {
            if (string.IsNullOrWhiteSpace(cellCode)) return null;
            var sheetCells = worksheet.Descendants<Cell>().Where(c => c.CellReference == cellCode);

            return sheetCells?.FirstOrDefault() ?? null;
        }

        private Row GetRow(Worksheet worksheet, int rowIndex)
        {
            Row row = worksheet.GetFirstChild<SheetData>().Elements<Row>().FirstOrDefault(r => r.RowIndex == rowIndex);
            if (row == null)
            {
                throw new ArgumentException($"No row with index {rowIndex} found in spreadsheet");
            }
            return row;
        }

        private Cell BuildCell<T>(T value, UInt32Value styleIndex = null)
        {
            var dataType = CellValues.String;
            var cellValue = new CellValue(value?.ToString() ?? string.Empty);
            switch (Type.GetTypeCode(typeof(T)))
            {
                case TypeCode.Single:
                case TypeCode.Double:
                case TypeCode.Int32:
                    dataType = CellValues.Number;
                    break;
                case TypeCode.Boolean:
                    dataType = CellValues.Boolean;
                    break;
                default:
                    dataType = CellValues.String;
                    cellValue.Space = SpaceProcessingModeValues.Preserve; //allows spaces in text
                    break;
            }

            var newCell = new Cell() { CellValue = cellValue, DataType = dataType };

            if (styleIndex != null) newCell.StyleIndex = styleIndex;
            return newCell;
        }

        private string GetBOMIndention(int bomDepth)
        {
            var retVal = "";
            for(int i = 0; i < bomDepth; i++)
            {
                retVal = retVal + "   ";
            }

            return retVal;
        }

        private Cell UpdateCellValue<T>(T value, Cell cell, bool updateDataType = true)
        {
            var dataType = CellValues.String;
            switch (Type.GetTypeCode(typeof(T)))
            {
                case TypeCode.Double:
                case TypeCode.Single:
                case TypeCode.Int32:
                    dataType = CellValues.Number;
                    break;
                case TypeCode.Boolean:
                    dataType = CellValues.Boolean;
                    break;
                default:
                    dataType = CellValues.String;
                    break;
            }
            if (updateDataType) cell.DataType = dataType;
            cell.CellValue = new CellValue(value?.ToString() ?? string.Empty);
            return cell;
        }

        public static Sheet GetSheetFromWorkSheet
            (WorkbookPart workbookPart, WorksheetPart worksheetPart)
        {
            string relationshipId = workbookPart.GetIdOfPart(worksheetPart);
            IEnumerable<Sheet> sheets = workbookPart.Workbook.Sheets.Elements<Sheet>();
            return sheets.FirstOrDefault(s => s.Id.HasValue && s.Id.Value == relationshipId);
        }

        /// <summary>
        /// Inserts a new row at the desired index. If one already exists, then it is
        /// returned. If an insertRow is provided, then it is inserted into the desired
        /// rowIndex
        /// </summary>
        /// <param name="rowIndex">Row Index</param>
        /// <param name="worksheetPart">Worksheet Part</param>
        /// <param name="insertRow">Row to insert</param>
        /// <param name="isLastRow">Optional parameter - True, you can guarantee that this row is the last row (not replacing an existing last row) in the sheet to insert; false it is not</param>
        /// <returns>Inserted Row</returns>
        public static Row InsertRow(uint rowIndex, WorksheetPart worksheetPart, Row insertRow, bool isNewLastRow = false)
        {
            Worksheet worksheet = worksheetPart.Worksheet;
            SheetData sheetData = worksheet.GetFirstChild<SheetData>();

            Row retRow = !isNewLastRow ? sheetData.Elements<Row>().FirstOrDefault(r => r.RowIndex == rowIndex) : null;

            // If the worksheet does not contain a row with the specified row index, insert one.
            if (retRow != null)
            {
                // if retRow is not null and we are inserting a new row, then move all existing rows down.
                if (insertRow != null)
                {
                    UpdateRowIndexes(worksheetPart, rowIndex, false);
                    UpdateMergedCellReferences(worksheetPart, rowIndex, false);
                    UpdateHyperlinkReferences(worksheetPart, rowIndex, false);

                    // actually insert the new row into the sheet
                    retRow = sheetData.InsertBefore(insertRow, retRow);  // at this point, retRow still points to the row that had the insert rowIndex

                    string curIndex = retRow?.RowIndex.ToString();
                    string newIndex = rowIndex.ToString();

                    foreach (Cell cell in retRow.Elements<Cell>())
                    {
                        // Update the references for the rows cells.
                        cell.CellReference = new StringValue(cell.CellReference.Value.Replace(curIndex, newIndex));
                        if (cell.CellFormula != null)
                        {
                            var newFormula = new CellFormula
                            {
                                Text = cell.CellFormula.Text.Replace(curIndex, newIndex)
                            };
                            cell.CellFormula = newFormula;
                        }
                    }

                    // Update the row index.
                    retRow.RowIndex = rowIndex;
                }
            }
            else
            {
                // Row doesn't exist yet, shifting not needed.
                // Rows must be in sequential order according to RowIndex. Determine where to insert the new row.
                Row refRow = !isNewLastRow ? sheetData.Elements<Row>().FirstOrDefault(row => row.RowIndex > rowIndex) : null;

                // use the insert row if it exists
                retRow = insertRow ?? new Row() { RowIndex = rowIndex };
                
                IEnumerable<Cell> cellsInRow = retRow.Elements<Cell>();

                if (cellsInRow.Any())
                {
                    string curIndex = retRow.RowIndex.ToString();
                    string newIndex = rowIndex.ToString();

                    foreach (Cell cell in cellsInRow)
                    {
                        // Update the references for the rows cells.
                        cell.CellReference = new StringValue(cell.CellReference.Value.Replace(curIndex, newIndex));
                        if (cell.CellFormula != null)
                        {
                            var newFormula = new CellFormula
                            {
                                Text = cell.CellFormula.Text.Replace(curIndex, newIndex)
                            };
                            cell.CellFormula = newFormula;
                        }
                    }

                    // Update the row index.
                    retRow.RowIndex = rowIndex;
                }

                sheetData.InsertBefore(retRow, refRow);
            }

            return retRow;
        }

        /// <summary>
        /// Updates all of the Row indexes and the child Cells' CellReferences whenever
        /// a row is inserted or deleted.
        /// </summary>
        /// <param name="worksheetPart">Worksheet Part</param>
        /// <param name="rowIndex">Row Index being inserted or deleted</param>
        /// <param name="isDeletedRow">True if row was deleted, otherwise false</param>
        private static void UpdateRowIndexes(WorksheetPart worksheetPart, uint rowIndex, bool isDeletedRow)
        {
            // Get all the rows in the worksheet with equal or higher row index values than the one being inserted/deleted for reindexing.
            IEnumerable<Row> rows = worksheetPart.Worksheet.Descendants<Row>().Where(r => r.RowIndex.Value >= rowIndex);

            foreach (Row row in rows)
            {
                uint newIndex = (isDeletedRow ? row.RowIndex - 1 : row.RowIndex + 1);
                string curRowIndex = row.RowIndex.ToString();
                string newRowIndex = newIndex.ToString();

                foreach (Cell cell in row.Elements<Cell>())
                {
                    // Update the references for the rows cells.
                    cell.CellReference = new StringValue(cell.CellReference.Value.Replace(curRowIndex, newRowIndex));
                    if (cell.CellFormula != null)
                    {
                        var newFormula = new CellFormula
                        {
                            Text = cell.CellFormula.Text.Replace(curRowIndex, newRowIndex)
                        };
                        cell.CellFormula = newFormula;
                    }
                    
                }

                // Update the row index.
                row.RowIndex = newIndex;
            }
        }

        /// <summary>
        /// Updates the MergedCelss reference whenever a new row is inserted or deleted. It will simply take the
        /// row index and either increment or decrement the cell row index in the merged cell reference based on
        /// if the row was inserted or deleted.
        /// </summary>
        /// <param name="worksheetPart">Worksheet Part</param>
        /// <param name="rowIndex">Row Index being inserted or deleted</param>
        /// <param name="isDeletedRow">True if row was deleted, otherwise false</param>
        private static void UpdateMergedCellReferences(WorksheetPart worksheetPart, uint rowIndex, bool isDeletedRow)
        {
            if (worksheetPart.Worksheet.Elements<MergeCells>().Count() > 0)
            {
                MergeCells mergeCells = worksheetPart.Worksheet.Elements<MergeCells>().FirstOrDefault();

                if (mergeCells != null)
                {
                    // Grab all the merged cells that have a merge cell row index reference equal to or greater than the row index passed in
                    List<MergeCell> mergeCellsList = mergeCells.Elements<MergeCell>().Where(r => r.Reference.HasValue)
                                                                                     .Where(r => GetRowIndex(r.Reference.Value.Split(':').ElementAt(0)) >= rowIndex ||
                                                                                                 GetRowIndex(r.Reference.Value.Split(':').ElementAt(1)) >= rowIndex).ToList();

                    // Need to remove all merged cells that have a matching rowIndex when the row is deleted
                    if (isDeletedRow)
                    {
                        List<MergeCell> mergeCellsToDelete = mergeCellsList.Where(r => GetRowIndex(r.Reference.Value.Split(':').ElementAt(0)) == rowIndex ||
                                                                                       GetRowIndex(r.Reference.Value.Split(':').ElementAt(1)) == rowIndex).ToList();

                        // Delete all the matching merged cells
                        foreach (MergeCell cellToDelete in mergeCellsToDelete)
                        {
                            cellToDelete.Remove();
                        }

                        // Update the list to contain all merged cells greater than the deleted row index
                        mergeCellsList = mergeCells.Elements<MergeCell>().Where(r => r.Reference.HasValue)
                                                                         .Where(r => GetRowIndex(r.Reference.Value.Split(':').ElementAt(0)) > rowIndex ||
                                                                                     GetRowIndex(r.Reference.Value.Split(':').ElementAt(1)) > rowIndex).ToList();
                    }

                    // Either increment or decrement the row index on the merged cell reference
                    foreach (MergeCell mergeCell in mergeCellsList)
                    {
                        string[] cellReference = mergeCell.Reference.Value.Split(':');

                        if (GetRowIndex(cellReference.ElementAt(0)) >= rowIndex)
                        {
                            string columnName = GetColumnName(cellReference.ElementAt(0));
                            cellReference[0] = isDeletedRow ? columnName + (GetRowIndex(cellReference.ElementAt(0)) - 1).ToString() : IncrementCellReference(cellReference.ElementAt(0), CellReferencePartEnum.Row);
                        }

                        if (GetRowIndex(cellReference.ElementAt(1)) >= rowIndex)
                        {
                            string columnName = GetColumnName(cellReference.ElementAt(1));
                            cellReference[1] = isDeletedRow ? columnName + (GetRowIndex(cellReference.ElementAt(1)) - 1).ToString() : IncrementCellReference(cellReference.ElementAt(1), CellReferencePartEnum.Row);
                        }

                        mergeCell.Reference = new StringValue(cellReference[0] + ":" + cellReference[1]);
                    }
                }
            }
        }

        /// <summary>
        /// Updates all hyperlinks in the worksheet when a row is inserted or deleted.
        /// </summary>
        /// <param name="worksheetPart">Worksheet Part</param>
        /// <param name="rowIndex">Row Index being inserted or deleted</param>
        /// <param name="isDeletedRow">True if row was deleted, otherwise false</param>
        private static void UpdateHyperlinkReferences(WorksheetPart worksheetPart, uint rowIndex, bool isDeletedRow)
        {
            Hyperlinks hyperlinks = worksheetPart.Worksheet.Elements<Hyperlinks>().FirstOrDefault();

            if (hyperlinks != null)
            {
                Match hyperlinkRowIndexMatch;
                uint hyperlinkRowIndex;

                foreach (Hyperlink hyperlink in hyperlinks.Elements<Hyperlink>())
                {
                    hyperlinkRowIndexMatch = Regex.Match(hyperlink.Reference.Value, "[0-9]+");
                    if (hyperlinkRowIndexMatch.Success && uint.TryParse(hyperlinkRowIndexMatch.Value, out hyperlinkRowIndex) && hyperlinkRowIndex >= rowIndex)
                    {
                        // if being deleted, hyperlink needs to be removed or moved up
                        if (isDeletedRow)
                        {
                            // if hyperlink is on the row being removed, remove it
                            if (hyperlinkRowIndex == rowIndex)
                            {
                                hyperlink.Remove();
                            }
                            // else hyperlink needs to be moved up a row
                            else
                            {
                                hyperlink.Reference.Value = hyperlink.Reference.Value.Replace(hyperlinkRowIndexMatch.Value, (hyperlinkRowIndex - 1).ToString());

                            }
                        }
                        // else row is being inserted, move hyperlink down
                        else
                        {
                            hyperlink.Reference.Value = hyperlink.Reference.Value.Replace(hyperlinkRowIndexMatch.Value, (hyperlinkRowIndex + 1).ToString());
                        }
                    }
                }

                // Remove the hyperlinks collection if none remain
                if (hyperlinks.Elements<Hyperlink>().Count() == 0)
                {
                    hyperlinks.Remove();
                }
            }
        }

        /// <summary>
        /// Given a cell name, parses the specified cell to get the row index.
        /// </summary>
        /// <param name="cellReference">Address of the cell (ie. B2)</param>
        /// <returns>Row Index (ie. 2)</returns>
        public static uint GetRowIndex(string cellReference)
        {
            // Create a regular expression to match the row index portion the cell name.
            Regex regex = new Regex(@"\d+");
            Match match = regex.Match(cellReference);

            return uint.Parse(match.Value);
        }

        /// <summary>
        /// Increments the reference of a given cell.  This reference comes from the CellReference property
        /// on a Cell.
        /// </summary>
        /// <param name="reference">reference string</param>
        /// <param name="cellRefPart">indicates what is to be incremented</param>
        /// <returns></returns>
        public static string IncrementCellReference(string reference, CellReferencePartEnum cellRefPart)
        {
            string newReference = reference;

            if (cellRefPart != CellReferencePartEnum.None && !String.IsNullOrEmpty(reference))
            {
                string[] parts = Regex.Split(reference, "([A-Z]+)");

                if (cellRefPart == CellReferencePartEnum.Column || cellRefPart == CellReferencePartEnum.Both)
                {
                    List<char> col = parts[1].ToCharArray().ToList();
                    bool needsIncrement = true;
                    int index = col.Count - 1;

                    do
                    {
                        // increment the last letter
                        col[index] = Letters[Letters.IndexOf(col[index]) + 1];

                        // if it is the last letter, then we need to roll it over to 'A'
                        if (col[index] == Letters[Letters.Count - 1])
                        {
                            col[index] = Letters[0];
                        }
                        else
                        {
                            needsIncrement = false;
                        }

                    } while (needsIncrement && --index >= 0);

                    // If true, then we need to add another letter to the mix. Initial value was something like "ZZ"
                    if (needsIncrement)
                    {
                        col.Add(Letters[0]);
                    }

                    parts[1] = new String(col.ToArray());
                }

                if (cellRefPart == CellReferencePartEnum.Row || cellRefPart == CellReferencePartEnum.Both)
                {
                    // Increment the row number. A reference is invalid without this componenet, so we assume it will always be present.
                    parts[2] = (int.Parse(parts[2]) + 1).ToString();
                }

                newReference = parts[1] + parts[2];
            }

            return newReference;
        }

        /// <summary>
        /// Given a cell name, parses the specified cell to get the column name.
        /// </summary>
        /// <param name="cellReference">Address of the cell (ie. B2)</param>
        /// <returns>Column name (ie. A2)</returns>
        private static string GetColumnName(string cellName)
        {
            // Create a regular expression to match the column name portion of the cell name.
            Regex regex = new Regex("[A-Za-z]+");
            Match match = regex.Match(cellName);

            return match.Value;
        }
        public enum CellReferencePartEnum
        {
            None,
            Column,
            Row,
            Both
        }
        private static List<char> Letters = new List<char>() { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', ' ' };

        #endregion

        private string UpdateRowIndices(Worksheet worksheet, OpenXmlPartRootElement calcChainRoot)
        {
            var rows = worksheet.Descendants<Row>().OrderBy(r => (uint)r.RowIndex).ToList();
            char lastColumnIndex = 'A';
            var updatedCalcCells = new List<CalculationCell>();
            for (int i = 1; i <= rows.Count(); i++)
            {
                var row = rows[i - 1];
                if (row.RowIndex != i)
                {
                    var oldIndex = row.RowIndex.ToString();
                    foreach (var cell in row.Descendants<Cell>().Where(c => c.CellReference != null))
                    {
                        var oldRefValue = cell.CellReference.Value;
                        var newRefValue = new StringValue(cell.CellReference.Value.Replace(row.RowIndex.ToString(), i.ToString()));
                        cell.CellReference = newRefValue;
                        var calcCellsToUpdate = calcChainRoot.Descendants<CalculationCell>()
                            .Where(c => !updatedCalcCells.Contains(c) && c.CellReference != null && c.CellReference == oldRefValue);
                        foreach (var calcCell in calcCellsToUpdate)
                        {
                            calcCell.CellReference = newRefValue;
                            updatedCalcCells.Add(calcCell);
                        }
                        if (cell.CellFormula != null)
                        {
                            if (cell.CellFormula.Text.Contains(oldIndex))
                            {
                                cell.CellFormula.Text = cell.CellFormula.Text.Replace(oldIndex, i.ToString());
                            }
                        }
                        calcChainRoot.InnerXml.Replace(oldRefValue, newRefValue);
                    }
                    row.RowIndex = (uint)i;
                }
                var columnIndexInt = int.Parse(row.Spans.InnerText.Split(':').Last());
                var columnIndex = Convert.ToChar(columnIndexInt + 96);
                var columnIndexUpper = char.ToUpper(columnIndex);
                if (columnIndexUpper > lastColumnIndex)
                {
                    lastColumnIndex = columnIndexUpper;
                }
            }

            var missingCalcCells = worksheet.Descendants<Cell>()
                .Where(c => c.CellFormula != null &&
                calcChainRoot.Descendants<CalculationCell>().All(cc => cc.CellReference != c.CellReference));

            foreach (var cell in missingCalcCells)
            {
                var calcCell = new CalculationCell { CellReference = cell.CellReference, SheetId = 1, NewLevel = true };
                calcChainRoot.InsertAfter(calcCell, calcChainRoot.Descendants<CalculationCell>().Last());
            }
            return $"{lastColumnIndex}{rows.Count()}";
        }

        #endregion

        private string ConvertExcelDocToPdf(string workbookFilename)
        {
            var pdfFilename = Path.ChangeExtension(workbookFilename, "pdf");
            var workbook = new Spire.Xls.Workbook();
            workbook.LoadFromFile(workbookFilename);
            
            workbook.SaveToFile(pdfFilename, Spire.Xls.FileFormat.PDF);

            AddWatermark(pdfFilename);

            return pdfFilename;
        }

        void AddWatermark(string pdfFilepath)
        {
            var doc = PdfReader.Open(pdfFilepath, PdfDocumentOpenMode.Modify);

            var font = new XFont("arial", 80);
            
            string watermark = "DRAFT";

            foreach (PdfPage page in doc.Pages)
            {
                var gfx = XGraphics.FromPdfPage(page, XGraphicsPdfPageOptions.Prepend);

                var size = gfx.MeasureString(watermark, font);

                gfx.TranslateTransform(page.Width / 2, page.Height / 2);
                gfx.RotateTransform(-Math.Atan(page.Height / page.Width) * 180 / Math.PI);
                gfx.TranslateTransform(-page.Width / 2, -page.Height / 2);

                var format = new XStringFormat();
                format.Alignment = XStringAlignment.Near;
                format.LineAlignment = XLineAlignment.Near;
           
                XBrush brush = new XSolidBrush(XColor.FromArgb(90, 255, 0, 0));
                                
                gfx.DrawString(watermark, font, brush,
                    new XPoint((page.Width - size.Width) / 2, (page.Height - size.Height) / 2),
                    format);
            }

            doc.Save(pdfFilepath);
        }

    }
}
