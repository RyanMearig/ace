﻿using System;
using System.Threading;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Ruler.Jobs;
using Serilog;
using StorageConfigurator.Jobs;


namespace StorageConfiguratorJobProcessor
{
    class Program
    {
        static int Main(string[] args)
        {
            Console.Title = $"Job Processor - {Environment.MachineName}";

            var configuration = GetConfiguration();
            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(configuration)
                .CreateLogger();

            try
            {
                var source = new CancellationTokenSource();
                var settings = configuration.GetSection("ModelerJob").Get<ModelerJobSettings>();

                JobProcessor.CreateBuilder(configuration)
                    .ConfigureLogging(logging =>
                    {
                        logging.AddSerilog();
                    })
                    .AddHandlerFor<ModelerJobHandler>("StorageConfiguratorModeler")
                    .AddServices(services =>
                    {
                        services.AddSingleton(settings);
                    })
                    .Build()
                    .RunAsync(source.Token).GetAwaiter().GetResult();

                return 0;
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Host terminated unexpectedly");
                Console.WriteLine("Press any key to continue");
                Console.ReadKey();
                return 1;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IConfiguration GetConfiguration()
        {
            return new ConfigurationBuilder()
                .SetBasePath(Environment.CurrentDirectory)
                .AddJsonFile("appsettings.json", optional: true)
                .AddEnvironmentVariables()
                .Build();
        }
    }
}
