﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Ruler.Drawings;
using Inventor;
using Ruler.InvUtils;
using Newtonsoft.Json;

namespace StorageConfigurator.Drawings
{
    public class StorageSystemDrawingContext : DefaultDrawingDataContext
    {

        #region Private Fields

        private List<Document> _elevationDocs;
        private List<ComponentOccurrence> _elevationOccs;
        private List<ElevationSheet> _elevationSheets;
        private List<ComponentOccurrence> _products;
        private List<ComponentOccurrence> _topLevelProducts;
        private List<ComponentOccurrence> _bottomLevelProducts;
        private List<ComponentOccurrence> _rightSideProducts;
        private List<List<ComponentOccurrence>> _allLevels;
        private List<ComponentOccurrence> _frameLines;
        private List<ComponentOccurrence> _frontFrameLineTypes;
        private List<ComponentOccurrence> _rearFrameLineTypes;
        private List<ComponentOccurrence> _rearFrameUprights;
        private List<ComponentOccurrence> _rearFrameColumns;
        private List<ComponentOccurrence> _rearFramePostColumns;
        private List<ComponentOccurrence> _rearFrameDoublers;
        private List<ComponentOccurrence> _footplates;
        private List<ComponentOccurrence> _uniqueFootplates;
        private List<FootplateBlockInfo> _uniqueFootplateInstallBlocks;
        private List<CalloutInfo> _callouts;
        public List<InstallSheet> _miscInstallSheets;
        public List<InstallSheet> _footPlateInstallSheets;
        private List<ComponentOccurrence> _bays;
        private IDrawingDataContext _parentContext;
        private Dictionary<Document, string> _bomNumbers;
        private List<ComponentOccurrence> _bomItems;
        private DataTable _bomData;
        private List<DataTable> _bomTables;

        #endregion

        public const double ElevationViewPortWidth = 5.75;
        public const double ElevationViewPortHeight = 7.75;
        public const double ViewToViewSpacing = 2.75;

        public const string FrameUprightFactory = "FrameUpright";
        public const string PostFactory = "UprightColumnAssembly";
        public const string LoadBeamFactory = "LoadBeam";

        public const string IsDoubleDeepPropName = "IsDoubleDeep";
        public const string IsDoubleDeepPostPropName = "IsDoubleDeepPost";
        public const string HasFrontDoublerPropName = "HasFrontDoubler";
        public const string HasRearDoublerPropName = "HasRearDoubler";
        public const string HashPropName = "rulerHash";


        #region Constructors

        public StorageSystemDrawingContext(Document document, Dictionary<string, object> extraData) : base(document, extraData) { }

        public StorageSystemDrawingContext(ComponentOccurrence occurrence, IDrawingDataContext parentContext) : base(occurrence, parentContext) 
        {
            _parentContext = parentContext;
        }

        public StorageSystemDrawingContext(Document document) : base(document)
        {
        }

        public StorageSystemDrawingContext(Document document, IDrawingDataContext parent) : base(document, parent)
        {
            _parentContext = parent;
        }

        #endregion

        #region Public Properties

        public List<Document> ElevationDocs
        {
            get
            {
                if (_elevationDocs == null || _elevationDocs.Count < 1)
                {
                    Document[] elevs = GetDescendantDocumentsByFactoryFile("Elevation").ToArray();

                    if (elevs != null && elevs.Count() > 0)
                        _elevationDocs = elevs.ToList();
                    else
                        _elevationDocs = new List<Document>();
                }

                return _elevationDocs;
            }
        }

        public List<ComponentOccurrence> ElevationOccs
        {
            get
            {
                if (_elevationOccs == null)
                {
                    _elevationOccs = GetDescendantOccurrencesByFactoryFile("Elevation").ToList();
                }

                return _elevationOccs;
            }
        }

        public List<ElevationSheet> ElevationSheets
        {
            get
            {
                if (_elevationSheets == null || _elevationSheets.Count < 1)
                {
                    _elevationSheets = new List<ElevationSheet>();
                    var includedElevations = ElevationDocs.Where(e => e.GetPropertyValue<bool>("IncludeInDrawing"));

                    if (includedElevations.Any())
                    {
                        List<List<Document>> elevGroups = includedElevations.Select((value, index) => new { IncNum = index / 3, value })
                                                                    .GroupBy(pair => pair.IncNum)
                                                                    .Select(grp => grp.Select(g => g.value).ToList())
                                                                    .ToList();

                        int id = 0;
                        elevGroups.ForEach(eg => _elevationSheets.Add(new ElevationSheet() { Elevations = eg, SheetIndex = id++ }));
                    }
                }

                return _elevationSheets;
            }
        }

        public List<InstallSheet> MiscInstallSheets
        {
            get
            {
                var blocksPerSheet = 16;
                if(_miscInstallSheets == null)
                {
                    var blockNames = GetMiscInstallSheetBlocks();
                    var blockGroups = blockNames.Select((b, i) => new { index = i, value = b })
                                                                .GroupBy(p => p.index / blocksPerSheet).ToList();

                    _miscInstallSheets = blockGroups
                                                .Select((g, i) =>
                                                {
                                                    var blocks = g.ToList().Select(p => p.value).ToList();

                                                    return new InstallSheet()
                                                    {
                                                        SheetIndex = i,
                                                        BeginningBlockIndex = g.ToList().First().index + 1,
                                                        InstallationBlockNames = blocks
                                                    };
                                                })
                                                .ToList();
                }
                return _miscInstallSheets;
            }
        }

        public List<InstallSheet> FootPlateInstallSheets
        {
            get
            {
                var blocksPerFirstSheet = 8;
                var blocksPerOtherSheets = 16;
                if (_footPlateInstallSheets == null && this.UniqueFootplateInstallBlockInfos.Count > 0)
                {
                    var indexedFootPlates = this.UniqueFootplateInstallBlockInfos.Select((b, i) => new { index = i, value = b });
                    var footPlateGroups = indexedFootPlates.Where(f => f.index < blocksPerFirstSheet)
                                                            .GroupBy(p => p.index / blocksPerFirstSheet).ToList();

                    if (indexedFootPlates.Count() > blocksPerFirstSheet)
                    {
                        footPlateGroups.AddRange(indexedFootPlates.Where(f => f.index >= blocksPerFirstSheet)
                                                                    .GroupBy(p => (p.index- blocksPerFirstSheet) / blocksPerOtherSheets).ToList());
                    }
                    
                    _footPlateInstallSheets = footPlateGroups
                                                .Select((g, i) => 
                                                {
                                                    var blockInfos = g.ToList().Select(p => p.value).ToList();

                                                    return new InstallSheet()
                                                    {
                                                        SheetIndex = i,
                                                        BeginningBlockIndex = g.ToList().First().index + 1,
                                                        FootPlateComps = blockInfos.Select(b => b.FootplateOcc).ToList(),
                                                        InstallationBlockNames = blockInfos.Select(b => b.FootplateBlock).ToList()
                                                    };
                                                })
                                                .ToList();
                }

                return _footPlateInstallSheets;
            }
        }

        public int WeldTypesSheetIndex
        {
            get
            {
                return FootPlateInstallSheets.Count() + MiscInstallSheets.Count();
            }
        }

        //TODO: add caching for bool members?
        public bool FrameLineIsDoubleDeep
        {
            get
            {
                var frameline = FrameLines.First();
                return GetPropFromOcc(frameline, IsDoubleDeepPropName).ToLower() == "true";
            }
        }

        public bool FrameLineIsDoubleDeepPost
        {
            get
            {
                var frameline = FrameLines.First();
                return GetPropFromOcc(frameline, IsDoubleDeepPostPropName).ToLower() == "true";
            }
        }

        public bool FrameLineIsDoubleDeepFrame
        {
            get
            {
                return FrameLineIsDoubleDeep && !FrameLineIsDoubleDeepPost;
            }
        }

        public bool HasFrontDoubler
        {
            get
            {
                var frontUpright = RearFrameUprights.First();
                return GetPropFromOcc(frontUpright, HasFrontDoublerPropName).ToLower() == "true";
            }
        }

        public bool HasRearDoubler
        {
            get
            {
                var rearUpright = RearFrameUprights.Last();
                return GetPropFromOcc(rearUpright, HasRearDoublerPropName).ToLower() == "true";
            }
        }

        public bool ElevationIsBackToBack
        {
            get
            {
                return DataDocument.GetPropertyValue<bool>("IsBackToBack");
            }
        }

        public string ElevationTitleTop
        {
            get
            {
                return DataDocument.GetPropertyValue("RackType");
            }
        }

        public string FrontElevationTitleBottom
        {
            get
            {
                string prodsWide = DataDocument.GetPropertyValue("LoadsWide");
                return $"FRONT ELEVATION - {prodsWide} WIDE";
            }
        }

        public string SideElevationTitleBottom
        {
            get
            {
                string rackType = ElevationIsBackToBack ? "- BACK TO BACK" : "";
                return $"SIDE ELEVATION {rackType}";
            }
        }

        #region Component Collections

        public List<ComponentOccurrence> Bays
        {
            get
            {
                if(_bays == null)
                {
                    _bays = GetChildOccurrencesByFactoryFile("Bay")?
                        .OrderByDescending(f => GetWorkPointProxy(f, "Center Point").Point.X).ToList();
                }
                return _bays;
            }
        }

        public List<List<ComponentOccurrence>> AllLevels
        {
            get
            {
                //returns a list of list of levels where the bays are from front to rear
                //levels are from bottom to top
                if (_allLevels == null)
                {
                    _allLevels = Bays?.Select(b => GetChildOccurrencesByFactory(b, "Level")?.ToList())?.ToList();
                }
                return _allLevels;
            }
        }

        public List<ComponentOccurrence> FirstBayLevels
        {
            get
            {
                return AllLevels?.FirstOrDefault();
            }
        }
        public List<ComponentOccurrence> Products
        {
            get
            {
                if (_products == null)
                {
                    _products = GetDescendantOccurrencesByFactoryFile("Product").ToList();
                }
                return _products;
            }
        }

        public List<ComponentOccurrence> TopLevelProducts
        {
            get
            {
                if(_topLevelProducts == null)
                {
                    ComponentOccurrence topLevel = FirstBayLevels.Last(); 
                    _topLevelProducts = GetProductsFromLevel(topLevel);
                }
                return _topLevelProducts;
            }
        }

        public List<ComponentOccurrence> BottomLevelProducts
        {
            get
            {
                if (_bottomLevelProducts == null)
                {
                    ComponentOccurrence btmLevel = FirstBayLevels.First(); 
                    _bottomLevelProducts = GetProductsFromLevel(btmLevel);
                }
                return _bottomLevelProducts;
            }
        }

        public List<ComponentOccurrence> FrameLines
        {
            get
            {
                if (_frameLines == null)
                {
                    _frameLines = GetDescendantOccurrencesByFactoryFile("FrameLine").ToList();
                }
                return _frameLines;
            }
        }

        public List<ComponentOccurrence> FrontFrameLineTypes
        {
            get
            {
                if (_frontFrameLineTypes == null)
                {
                    _frontFrameLineTypes = GetChildOccurrencesByFactory(FrameLines.First(), "FrameType")?
                                                        .OrderByDescending(f => GetWorkPointProxy(f, "Center Point").Point.X).ToList();
                }
                return _frontFrameLineTypes;
            }
        }

        public List<ComponentOccurrence> RearFrameLineTypes
        {
            get
            {
                if (_rearFrameLineTypes == null)
                {
                    _rearFrameLineTypes = GetChildOccurrencesByFactory(FrameLines.Last(), "FrameType")?
                                                       .OrderByDescending(f => GetWorkPointProxy(f, "Center Point").Point.X).ToList();
                }
                return _rearFrameLineTypes;
            }
        }

        public List<DimensionInfo> TopLevelProductLoadtoLoadDims
        {
            get
            {
                var retList = new List<DimensionInfo>();

                if (TopLevelProducts.Count > 1)
                {
                    if (GetPropFromOcc(Bays.FirstOrDefault(), "IsDoubleDeep").ToLower() == "true")
                    {
                        retList = TopLevelProducts.Select((p, index) =>
                        {
                            if ((index + 1) >= (TopLevelProducts.Count() / 2))
                                return null;
                            else
                                return new DimensionInfo() { Part1 = p, Part2 = TopLevelProducts[index + 1] };
                        }).ToList();
                    }
                    else
                    {
                        retList = TopLevelProducts.Select((p, index) =>
                        {
                            if ((index + 1) >= TopLevelProducts.Count())
                                return null;
                            else
                                return new DimensionInfo() { Part1 = p, Part2 = TopLevelProducts[index + 1] };
                        }).ToList();
                    }

                    retList.RemoveAll(d => d == null);
                }

                return retList;
            }

        }

        public List<ComponentOccurrence> RightSideProducts
        {
            get
            {
                if (_rightSideProducts == null)
                {
                    var retList = FirstBayLevels.Select(lvl => GetProductsFromLevel(lvl).Last()).ToList();
                    _rightSideProducts = retList;
                }
                return _rightSideProducts;
            }
        }

        public List<ComponentOccurrence> OrdinateComps
        {
            get
            {
                var levels = new List<ComponentOccurrence>(FirstBayLevels);
                //check bottom level to see if it has load beams
                if (!GetLoadBeamsFromLevel(levels.First()).Any())
                    levels.RemoveAt(0);

                return levels;
            }
        }

        public List<ComponentOccurrence> RearFrameUprights
        {
            get
            {
                if(_rearFrameUprights == null)
                {
                    _rearFrameUprights = RearFrameLineTypes?.SelectMany(t => GetOccurrenceDescendantsByFactory(t, FrameUprightFactory))
                                            .OrderByDescending(f => GetWorkPointProxy(f, "Center Point").Point.X).ToList();
                }
                return _rearFrameUprights;
            }
        }

        public List<ComponentOccurrence> RearFrameColumns 
        {
            get
            {
                if (_rearFrameColumns == null)
                {
                    _rearFrameColumns = GetOccurrenceDescendantsByFactory(FrameLines.Last(), PostFactory)?
                                                       .OrderByDescending(p => GetWorkPointProxy(p, "Center Point").Point.X).ToList();
                }
                return _rearFrameColumns;
            }
        }

        public List<ComponentOccurrence> RearFrameNonFramePosts
        {
            get
            {
                if (_rearFramePostColumns == null)
                {
                    _rearFramePostColumns = RearFrameLineTypes?.SelectMany(t => GetChildOccurrencesByFactory(t, PostFactory))
                                            .OrderByDescending(f => GetWorkPointProxy(f, "Center Point").Point.X).ToList();
                }
                return _rearFramePostColumns;
            }
        }

        public List<ComponentOccurrence> RearFrameDoublers
        {
            get
            {
                if (_rearFrameDoublers == null)
                {
                    _rearFrameDoublers = RearFrameColumns.Where(p => GetPropertyValue(p, "HasDoubler").ToLower() == "true")?
                                                             .Select(u => GetUprightColumnAssemblyDoubler(u)).ToList();
                }
                return _rearFrameDoublers;
            }
        }

        public List<ComponentOccurrence> Footplates
        {
            get
            {
                if (_footplates == null)
                {
                    _footplates = GetDescendantOccurrencesByFactoryFileContains("Footplate")
                                                .OrderBy(f => GetWorkPointProxy(f, "Center Point").Point.Z).ToList();
                }

                return _footplates;
            }
        }

        public List<ComponentOccurrence> UniqueFootplates
        {
            get
            {
                if (_uniqueFootplates == null)
                {
                    var uniqueFootplates = this.Footplates
                        .GroupBy(f => GetPropertyValue(f, HashPropName))
                        .Select(g => g.OrderBy(f => GetWorkPointProxy(f, "Center Point").Point.Z))
                        .Select(g => g.First());
                    _uniqueFootplates = uniqueFootplates.ToList();
                }
                return _uniqueFootplates;
            }
        }

        public List<FootplateBlockInfo> UniqueFootplateInstallBlockInfos
        {
            get
            {
                if (_uniqueFootplateInstallBlocks == null)
                {
                    var footplateBlocks = this.Footplates.Select(f => GetFootplateBlockInfo(f));
                    _uniqueFootplateInstallBlocks = footplateBlocks
                                                    .GroupBy(f => f.FootplateBlock)
                                                    .Select(g => g.First()).ToList();
                }

                return _uniqueFootplateInstallBlocks;
            }
        }

        public List<CalloutInfo> Callouts
        {
            get
            {
                if(_callouts == null)
                {

                    var allCallouts = new List<CalloutInfo>();
                    var isDoubleDeep = FrameLineIsDoubleDeep;
                    var isDoubleDeepFrame = FrameLineIsDoubleDeepFrame;

                    //Crossbeams
                    var crossBeamCallouts = AllLevels.SelectMany(a => a).Select(l =>
                    {
                        var crossBeams = GetChildOccurrencesByFactory(l, "ShelfBeamCrossbar");
                        if (!crossBeams.Any()) { return null; }
                        var palletQty = GetProductsFromLevel(l).Count();
                        var beamPerPallet = (int)(crossBeams.Count() / palletQty);
                        var supportType = GetPropertyValue(l, "PalletSupportType")?.ToUpper() ?? "";
                        return new CalloutInfo()
                        {
                            Part = crossBeams.First(),
                            WorkpointName = "NotePoint",
                            Id = GetBomNumber(crossBeams.First(), true),
                            AboveLine = supportType,
                            BelowLine1 = $"{beamPerPallet} PER PALLET",
                            BelowLine2 = string.Empty,
                            Side = "Left",
                            Xoffset = -1,
                            Yoffset = .75
                        };
                    });

                    //Pallet Stops
                    var palStopCallouts = AllLevels.SelectMany(a => a).Select(l =>
                    {
                        var palStops = l.SubOccurrences
                            .Cast<ComponentOccurrence>()
                            .Where(occ => GetPropFromOcc(occ, "rulerFactoryFile").Contains("PalletStop"));
                        if (!palStops.Any()) { return null; }
                        var palletQty = GetProductsFromLevel(l).Count();
                        var rearPalletQty = (isDoubleDeep ? palletQty/2 : palletQty);
                        var stopsPerPallet = (int)(palStops.Count() / rearPalletQty);
                        return new CalloutInfo()
                        {
                            Part = palStops.First(),
                            WorkpointName = "Center Point", //TODO: add workpoints to factory and implement
                            Id = GetBomNumber(palStops.First()),
                            AboveLine = GetPropertyValue(l, "PalletStopStyle"),
                            BelowLine1 = string.Empty,
                            BelowLine2 = $"{stopsPerPallet} PER REAR PALLET",
                            Side = "Right",
                            Xoffset = 1,
                            Yoffset = .75
                        };
                    });
                    var groupedStops = palStopCallouts
                        .Where(c => c != null)
                        .GroupBy(c => (c.Id, c.BelowLine2));
                    var updatedStopCallouts = groupedStops.Select(g =>
                    {
                        var levelIds = g.Select(c => GetLevelIndex(c.Part.ParentOccurrence));
                        var levelDescription = "";
                        if (levelIds.Count() >= 2)
                        {
                            levelDescription = $"LEVELS {string.Join(", ", levelIds)} ONLY"; //TODO ex 2-5
                        }
                        else
                        {
                            levelDescription = $"LEVEL {levelIds.First()} ONLY"; //TODO ex 2-5
                        }
                        g.First().BelowLine1 = levelDescription;
                        return g.First();
                    });

                    var uprightCallouts = RearFrameUprights.Select(u => new CalloutInfo()
                    {
                        Part = u,
                        WorkpointName = "v011",
                        Id = GetBomNumber(u),
                        AboveLine = GetPropertyValue(u, "Description"),
                        BelowLine1 = string.Empty,
                        BelowLine2 = string.Empty,
                        Side = "Left",
                        Xoffset = -1,
                        Yoffset = 1.375
                    });

                    var postCallouts = RearFrameNonFramePosts.Select(u => new CalloutInfo()
                    {
                        Part = u,
                        WorkpointName = "eTopLeft",
                        Id = GetBomNumber(u),
                        AboveLine = GetPropertyValue(u, "Description"),
                        BelowLine1 = string.Empty,
                        BelowLine2 = string.Empty,
                        Side = "Left",
                        Xoffset = -1,
                        Yoffset = 1.375
                    });


                    //Rub Rail
                    var rearRubRails = GetDescendantOccurrencesByFactoryFile("FrameTieAssembly")
                        .Where(u => (GetWorkPointProxy(u, "Center Point")?.Point.Z ?? 0) < -7); //only upright on right side showing in view
                    var rubRailCallouts = rearRubRails.Select(r => new CalloutInfo()
                    {
                        Part = r,
                        WorkpointName = "Center Point",
                        Id = GetBomNumber(r, true), 
                        AboveLine = GetPropertyValue(r, "Type")?.ToUpper() ?? "",
                        BelowLine1 = string.Empty,
                        BelowLine2 = string.Empty,
                        Side = "Right",
                        Xoffset = 1,
                        Yoffset = .65
                    });


                    //Load Beam
                    var loadBeamCallouts = AllLevels?.SelectMany((b, index) => GetBayLoadBeamCallouts(b, index, AllLevels.Count()))?
                                                    .ToList();


                    allCallouts.AddRange(DistinctCallouts(crossBeamCallouts));
                    allCallouts.AddRange(DistinctCallouts(uprightCallouts));
                    allCallouts.AddRange(DistinctCallouts(postCallouts));
                    allCallouts.AddRange(DistinctCallouts(updatedStopCallouts));
                    allCallouts.AddRange(DistinctCallouts(rubRailCallouts));
                    allCallouts.AddRange(loadBeamCallouts);

                    _callouts = allCallouts;
                }
                return _callouts;
            }
        }

        public List<ProductBlockInfo> ElevationUniqueProductBlockInfos
        {
            get
            {
                var distinctProducts = GetDescendantDocumentsByFactoryFile("Product").Distinct();
                List<ProductBlockInfo> retVal = distinctProducts
                                                .Select((p, index) =>
                                                {
                                                    return new ProductBlockInfo()
                                                    {
                                                        Width = p.GetParameterValue("LoadWidth").ToString() + "\"",
                                                        Depth = p.GetParameterValue("LoadDepth").ToString() + "\"",
                                                        Height = p.GetParameterValue("LoadHeight").ToString() + "\"",
                                                        MaxWeight = p.GetPropertyValue("Weight"),
                                                        AvgWeight = p.GetPropertyValue("Weight"),
                                                        BlockName = $"Product{p.GetPropertyValue("PalletType")}Block",
                                                        VertOffset = GetProductBlockVertSpacing(index)
                                                    };
                                                }).ToList();

                return retVal;
            }
        }

        #endregion

        public List<ComponentOccurrence> BomItems
        {
            get
            {
                if (_bomItems == null)
                {
                    var items = new List<ComponentOccurrence>(GetDescendantOccurrencesByFactoryFile("FrameUpright"));
                    items.AddRange(GetFramePostColumnAssemblies());
                    items.AddRange(GetDescendantOccurrencesByFactoryFile("RSFrameTie"));
                    items.AddRange(GetDescendantOccurrencesByFactoryFile("WeldedFrameTie"));
                    items.AddRange(GetDescendantOccurrencesByFactoryFile("WeldedHDFrameTie"));
                    items.AddRange(GetDescendantOccurrencesByFactoryFile("WeldedRubRail"));
                    items.AddRange(GetDescendantOccurrencesByFactoryFile("DoubledRubRail"));
                    items.AddRange(GetDescendantOccurrencesByFactoryFile(LoadBeamFactory));
                    items.AddRange(GetDescendantOccurrencesByFactoryFile("WeldedCrossbar"));
                    items.AddRange(GetDescendantOccurrencesByFactoryFile("XBCrossbar"));
                    items.AddRange(GetDescendantOccurrencesByFactoryFile("SelectivePalletStop"));
                    items.AddRange(GetDescendantOccurrencesByFactoryFile("DoubleDeepPalletStop"));
                    items.AddRange(GetDescendantOccurrencesByFactoryFile("AnchorBolts"));
                    items.AddRange(GetDescendantOccurrencesByFactoryFile("HardwareKit"));
                    items.AddRange(GetDescendantOccurrencesByFactoryFile("Washers"));

                    _bomItems = items.Where(i => !ElevationOccs
                        .SelectMany(occ => GetOccurrenceDescendants(occ))
                        .Contains(i)).ToList();
                }
                return _bomItems;
            }
        }
        
        public List<DataTable> BomTables
        {
            get
            {
                if (_bomTables == null)
                {
                    var table = new DataTable("BOM Table");
                    table.Columns.Add("Item No.");
                    table.Columns.Add("Description");
                    table.Columns.Add("Color");
                    table.Columns.Add("Quantity");

                    var hardwareDescendants = GetDescendantOccurrencesByFactoryFile("HardwareKit");
                    var washerDescendants = GetDescendantOccurrencesByFactoryFile("Washers");
                    var anchorBoltDescendants = GetDescendantOccurrencesByFactoryFile("AnchorBolts");

                    //TODO:This might need to be grouped by part number or description instead of distinct file
                    var groupedBomItems = BomItems.GroupBy(i => i.Definition.Document)
                        .Select(g =>
                                {
                                    if (hardwareDescendants.Contains(g.First()) || washerDescendants.Contains(g.First()) ||
                                        anchorBoltDescendants.Contains(g.First()))
                                    {
                                        //special logic for hardware.  The quantity is in an iProperty.
                                        int qtyPropVal = int.TryParse(GetPropertyValue(g.First(), "Qty"), out qtyPropVal) ? qtyPropVal : 0;
                                        return (g.First(), qtyPropVal);
                                    }
                                    else
                                    {
                                        return (g.First(), g.Count());
                                    }
                                });

                    var maxLines = 85;
                    var linesLeft = maxLines;
                    var tables = new List<DataTable>() { table };

                    foreach (var group in groupedBomItems)
                    {
                        var lines = GetBomLines(group.Item1, group.Item2);
                        if (linesLeft < lines.Count)
                        {
                            var newTable = new DataTable("BOM Table");
                            newTable.Columns.Add("Item No.");
                            newTable.Columns.Add("Description");
                            newTable.Columns.Add("Color");
                            newTable.Columns.Add("Quantity");
                            tables.Add(newTable);
                            linesLeft = maxLines;
                        }
                        var lastTable = tables.LastOrDefault();
                        lines.ForEach(r => lastTable.Rows.Add(r));
                        linesLeft -= lines.Count;
                    }

                    _bomTables = tables;
                }
                return _bomTables;
            }
        }

        public string GetAttributeValue(Inventor.Document doc, string key)
        {
            string retVal = "";

            if (doc.AttributeSets.NameIsUsed["ruler"])
            {
                var attSet = doc.AttributeSets["ruler"];

                if (attSet != null)
                {
                    retVal = attSet.NameIsUsed[key] ? attSet[key].Value.ToString() : "";
                }
            }

            return retVal;
        }

        public List<KeyValuePair<string, string>> GetAttributes(Inventor.Document doc)
        {
            List<KeyValuePair<string, string>> attributes = new List<KeyValuePair<string, string>>();

            if (doc.AttributeSets.NameIsUsed["ruler"])
            {
                var attSet = doc.AttributeSets["ruler"];

                if (attSet != null)
                {
                    foreach (Inventor.Attribute at in attSet)
                    {
                        attributes.Add(new KeyValuePair<string, string>(at.Name, Convert.ToString(at.Value)));
                    }
                }
            }

            return attributes;
        }

        public Dictionary<Document, string> BomNumbers
        {
            get
            {
                if(_bomNumbers == null)
                {
                    var nums = GetBomDocItemNumbers();
                    
                    if (nums != null && nums.Any()) 
                    {
                        _bomNumbers = new Dictionary<Document, string>();
                        nums.ToList().ForEach(n => _bomNumbers.Add(n.Key, n.Value)); 
                    }
                }
                return _bomNumbers;
            }
        }

        private IEnumerable<KeyValuePair<Document, string>> GetBomDocItemNumbers()
        {
            var itemNumbers = GetItemNumberAttribute(DataDocument);

            if (itemNumbers != null && itemNumbers.Any())
            {
                var docs = BomItems.Select(b =>
                {
                    var partNum = GetPropFromOcc(b, "Part Number");
                    if (!string.IsNullOrWhiteSpace(partNum) && itemNumbers.Any(n => n.Item1 == partNum))
                    {
                        return new KeyValuePair<Document, string>(b.Definition.Document as Document, itemNumbers.FirstOrDefault(n => n.Item1 == partNum).Item2);
                    }
                    return new KeyValuePair<Document, string>();
                })
                .Distinct()
                .Where(d => d.Key != null).ToList();

                return docs;
            }

            return null;
        }
        
        private List<Tuple<string, string>> GetItemNumberAttribute(Document doc)
        {
            var retVal = new List<Tuple<string, string>>();
            var itemJson = GetAttributeValue(doc, "ItemNumbers");

            if (!string.IsNullOrWhiteSpace(itemJson))
            {
                retVal = JsonConvert.DeserializeObject<List<Tuple<string, string>>>(itemJson);
            }

            return retVal;
        }

        private IEnumerable<KeyValuePair<Document, int>> GetBomDocIndexes(List<string> factoryNames, int startIndex)
        {
            var bomDocs = BomItems
                .Where(occ => factoryNames.Contains(GetPropFromOcc(occ, "rulerFactoryFile")))
                .Select(occ => occ.Definition.Document as Document)
                .Distinct()
                .ToList();
            return bomDocs.Select(d => new KeyValuePair<Document, int>(d, startIndex + bomDocs.IndexOf(d)));
        }

        #endregion

        #region Public Methods

        public IEnumerable<CalloutInfo> GetBayLoadBeamCallouts(List<ComponentOccurrence> bayLevelComps, int bayIndex, int numBays)
        {
            var isFirstBay = bayIndex == 0;
            var isLastBay = (bayIndex + 1) == numBays;
            return bayLevelComps.SelectMany(l =>
            {
                var loadBeams = GetChildOccurrencesByFactory(l, LoadBeamFactory)?
                    .OrderByDescending(f => GetWorkPointProxy(f, "v000").Point.X).ToList(); ;
                return loadBeams?
                            .Select((b, index) =>
                            {
                                var placeBalloonOnLeft = (isFirstBay && index == 0) ? true : 
                                    ((isLastBay && (index + 1) == loadBeams.Count) ? false : GetWorkPointProxy(b, "v000").Point.X > GetWorkPointProxy(b, "v100").Point.X);
                                return GetLoadBeamCallout(b, index, placeBalloonOnLeft, !((isFirstBay && index == 0) || (isLastBay && (index + 1) == loadBeams.Count)));
                            });
            });
        }

        public CalloutInfo GetLoadBeamCallout(ComponentOccurrence loadBeamOcc, int index, bool isLeftSideLoadBeam, bool isInternalLoadBeam)
        {
            var xOffset = isLeftSideLoadBeam ? -0.75 : .75;
            var yOffset = 0.0;

            if (isInternalLoadBeam)
            {
                yOffset = (index % 2 == 0 ? -0.35 : 0.35);
            }

            return new CalloutInfo()
            {
                Part = loadBeamOcc,
                WorkpointName = "v111",
                Id = GetBomNumber(loadBeamOcc),
                AboveLine = string.Empty,
                BelowLine1 = string.Empty,
                BelowLine2 = string.Empty,
                Side = "No Text",
                Xoffset = xOffset,
                Yoffset = yOffset
            };
        }

        public string GetDoublerHeightDimText(ComponentOccurrence compOcc)
        {
            return " " + (GetBooleanPropValue(compOcc, "IsFrontColumn") ? "FRONT" : "REAR")
                + " <br/>DOUBLERS";
        }

        public bool GetBooleanPropValue(ComponentOccurrence compOcc, string propName)
        {
            return GetPropertyValue<bool>(compOcc, propName);
        }

        public List<ComponentOccurrence> GetFramePostColumnAssemblies()
        {
            var allColAssys = GetDescendantOccurrencesByFactoryFile(PostFactory);

            return allColAssys.Where(c =>
            {
                var retVal = GetPropertyValue(c, "IsFramePostColumn");

                return retVal != null && retVal.ToLower() == "true";
            }).ToList();
        }

        public string GetCityStateZip(string cityKey, string stateKey, string zipKey)
        {
            return ($"{GetData(cityKey)}, {GetData(stateKey)} {GetData(zipKey)}");
        }

        public string GetExtraDataValue(string key, string defaultValue = "")
        {
            object value;
            return ExtraData.TryGetValue(key, out value) ? value.ToString() : defaultValue;
        }

        public string ReplaceTextWithValue(string input, string stringToReplace, string value)
        {
            return input.Replace(stringToReplace, value);
        }

        public double InToCm(double inches)
        {
            return inches * 2.54;
        }

        public double CmToIn(double centimeters)
        {
            return centimeters / 2.54;
        }

        public bool FramelinesDifferentHeight(Document doc) //TODO: verify changes here!!!
        {
            var frontFramelineHeight = GetParameterValue(FrontFrameLineTypes.First(), "Height");
            var rearFramelineHeight = GetParameterValue(RearFrameLineTypes.First(), "Height");

            return (frontFramelineHeight != rearFramelineHeight);
        }

        public bool CreateBtmLevelClearanceDim(Document doc)
        {
            List<ComponentOccurrence> firstLevelLoadBeams = GetLoadBeamsFromLevel(FirstBayLevels.First());

            //check bottom level to see if it has load beams
            if (firstLevelLoadBeams.Any())
            {
                return (CmToIn(firstLevelLoadBeams.First().Transformation.Translation.Y) > 15);
            }
            else
                return false;
        }
        
        public List<ComponentOccurrence> GetSideViewHiddenParts(Document doc)
        {
            List<ComponentOccurrence> retList = new List<ComponentOccurrence>();

            retList.Add(FrameLines.First());
            retList.AddRange(Products);

            return retList;
        }

        public double GetProductBlockVertSpacing(int index)
        {
            double retVal = 0;
            double startingOffset = 2.5;
            double blockSpacing = 4.25;

            if (index == 0)
                retVal = startingOffset;
            else
                retVal = startingOffset + (blockSpacing * index);

            return InToCm(retVal);
        }

        public string GetElevationFrontViewId(int sheetIndex, int viewIndex)
        {
            var retVal = (sheetIndex * 6);

            if (viewIndex == 0)
            {
                retVal = retVal + 1;
            }
            else if (viewIndex == 1)
            {
                retVal = retVal + 3;
            }
            else
            {
                retVal = retVal + 5;
            }

            return retVal.ToString();
        }

        public string GetElevationSideViewId(int sheetIndex, int viewIndex)
        {
            var retVal = (sheetIndex * 6);

            if (viewIndex == 0)
            {
                retVal = retVal + 2;
            }
            else if (viewIndex == 1)
            {
                retVal = retVal + 4;
            }
            else
            {
                retVal = retVal + 6;
            }

            return retVal.ToString();
        }

        public string GetViewId(int sheetIndex, int viewIndex)
        {
            if (sheetIndex == 0)
                return viewIndex.ToString();
            else
                return (viewIndex + (6 * sheetIndex)).ToString();
        }

        public double GetFrontViewXOffset(int index)
        {
            if(index == 0) return 6.35;
            return index < 2 ? (2.5 + (ElevationViewPortWidth + ViewToViewSpacing) * 2) * 2.54 
                : (2.5 + ElevationViewPortWidth + ViewToViewSpacing) * 2.54;
        }

        public double GetFrontViewYOffset(int index)
        {
            return index < 2 ? 20 * 2.54 : (7.75 + 2.5) * 2.54;
        }

        public double GetSideViewXOffset(int index)
        {
            if (index == 1) return 6.35;
            return index == 0 ? (2.5 + ElevationViewPortWidth + ViewToViewSpacing) * 2.54
                : (2.5 + (ElevationViewPortWidth + ViewToViewSpacing) * 2) * 2.54;
        }

        public double GetSideViewYOffset(int index)
        {
            return (index == 0) ? 20 * 2.54 : (7.75 + 2.5) * 2.54;
        }

        public string GetOccFractionalParameterValue(ComponentOccurrence occ, string paramName)
        {
            var val = occ.GetParameterValue(paramName);
            if (val is double decimalVal)
            {
                var fraction = DrawingsHelper.DoubleToFraction(decimalVal);
                return DrawingsHelper.DoubleToFraction(decimalVal);
            }
            return string.Empty;
        }

        public IEnumerable<ComponentOccurrence> GetChildOccurrencesByFactory(ComponentOccurrence compOcc, string factoryName)
        {
            return GetOccurrenceChildren(compOcc)
                .Where(occ => GetPropFromOcc(occ, "rulerFactoryFile") == factoryName);
        }

        public IEnumerable<ComponentOccurrence> GetOccurrenceDescendantsByFactory(ComponentOccurrence compOcc, string factoryName)
        {
            return GetOccurrenceDescendants(compOcc)
                .Where(occ => GetPropFromOcc(occ, "rulerFactoryFile") == factoryName);
        }

        public string GetPropFromOcc(ComponentOccurrence occ, string propName)
        {
            if (occ.IsSubstituteOccurrence) return null;
            return ((Document)occ.Definition.Document).GetPropertyValue(propName);
        }

        public float GetFloatPropertyValueOrDefault(ComponentOccurrence comp, string propName, float defaultVal = 0)
        {
            var val = GetPropertyValue(comp, propName);

            return (!string.IsNullOrWhiteSpace(val) ? float.Parse(val) : defaultVal);
        }

        #endregion

        #region Private Methods

        private List<ComponentOccurrence> GetLoadBeamsFromLevel(ComponentOccurrence level)
        {
            IEnumerable<ComponentOccurrence> loadBeams = GetChildOccurrencesByFactory(level, LoadBeamFactory);

            if (loadBeams != null && loadBeams.Any())
                return loadBeams.ToList();
            else
                return new List<ComponentOccurrence>();
        }

        private List<ComponentOccurrence> GetProductsFromLevel(ComponentOccurrence levelComp)
        {
            return GetChildOccurrencesByFactory(levelComp, "Product").ToList();
        }

        private string GetBomNumber(ComponentOccurrence occ, bool useFirstSubOcc = false)
        {
            if(_parentContext != null && _parentContext is StorageSystemDrawingContext storageContext)
            {
                var doc = useFirstSubOcc && occ.SubOccurrences.Count > 0 
                    ? occ.SubOccurrences[1].Definition.Document as Document 
                    : occ.Definition.Document as Document;
                return storageContext.BomNumbers.TryGetValue(doc, out string result) ? $"{result:D3}" : "???";
            }
            return BomNumbers.TryGetValue(occ.Definition.Document as Document, out string i) ? $"{i:D3}" : "???";       
        }
        
        private List<CalloutInfo> DistinctCallouts(IEnumerable<CalloutInfo> allCallouts)
        {
            var seenCallouts = new List<CalloutInfo>();
            foreach(var callout in allCallouts.Where(c => c != null))
            {
                var previouslyAdded = seenCallouts
                    .Any(c => c.Part.Definition.Document == callout.Part.Definition.Document //TODO: use id when logic is complete
                    && c.AboveLine == callout.AboveLine && c.BelowLine1 == callout.BelowLine1
                    && c.BelowLine2 == callout.BelowLine2);
                if(!previouslyAdded)
                {
                    seenCallouts.Add(callout);
                }
            }
            return seenCallouts;
        }
            
        private int GetLevelIndex(ComponentOccurrence levelOcc)
        {
            var bay = levelOcc.ParentOccurrence;
            var allLevels = GetChildOccurrencesByFactory(bay, "Level").ToList();
            return allLevels.IndexOf(levelOcc) + 1;
        }

        private ComponentOccurrence GetUprightColumnAssemblyColumnChannel(ComponentOccurrence uprightColAssy)
        {
            //First channel is the column.  Second channel is the doubler.
            List<ComponentOccurrence> channels = GetChildOccurrencesByFactory(uprightColAssy, "Channel").ToList();

            if (channels.Count > 0)
                return channels.First();
            else
            {
                //Handle bent leg
                List<ComponentOccurrence> bentLegChannels = GetChildOccurrencesByFactory(uprightColAssy, "BentLegColumnChannel").ToList();

                if (bentLegChannels.Count > 0)
                    return bentLegChannels.First();
                else
                    return null;
            }
        }

        private ComponentOccurrence GetUprightColumnAssemblyDoubler(ComponentOccurrence uprightColAssy)
        {
            //First channel is the column.  Second channel is the doubler.
            List<ComponentOccurrence> channels = GetChildOccurrencesByFactory(uprightColAssy, "Channel").ToList();

            if (channels.Count > 1)
                return channels.Last();
            else
            {
                //Handle bent leg doubler
                List<ComponentOccurrence> bentLegDoublers = GetChildOccurrencesByFactory(uprightColAssy, "ReverseBentLegColumnChannel").ToList();

                if (bentLegDoublers.Count > 0)
                    return bentLegDoublers.First();
                else
                    return null;
            }
        }

        private List<string> GetMiscInstallSheetBlocks()
        {
            var retList = new List<string>();

            retList.Add("NOTES");
            retList.Add("WARN_SIGN");
            retList.AddRange(GetBeamMiscInstallBlocks());
            retList.AddRange(GetCrossbarInstallBlocks());
            retList.AddRange(GetPalletStopInstallBlocks());
            retList.AddRange(GetHorizontalInstallBlocks());
            retList.AddRange(GetFrameTieInstallBlocks());

            return retList;
        }

        private List<string> GetBeamMiscInstallBlocks()
        {
            return ElevationOccs.SelectMany(e => GetElevationBeamMiscInstallBlocks(e))?.Distinct()?.ToList()
                ?? new List<string>();
        }

        private List<string> GetElevationBeamMiscInstallBlocks(ComponentOccurrence elev)
        {
            var retVal = new List<string>();
            var frameLines = GetChildOccurrencesByFactory(elev, "FrameLine")
                        .OrderBy(f => GetWorkPointProxy(f, "Center Point").Point.Z).ToList();

            //Get Posts 
            var allFramePostInfo = frameLines.SelectMany(f => GetOccurrenceDescendantsByFactory(f, PostFactory))
                                          .Select(p => (GetWorkPointProxy(p, "Center Point").Point.X, GetFloatPropertyValueOrDefault(p, "DoublerHeight")))
                                          .OrderBy(t => t.X);
            
            return GetOccurrenceDescendantsByFactory(elev, "Level")?.ToList()?
                         .SelectMany(l => GetLevelBeamMiscInstallBlocks(l, allFramePostInfo))?.ToList() 
                    ?? new List<string>();
        }

        private List<string> GetLevelBeamMiscInstallBlocks(ComponentOccurrence level, IEnumerable<(double, float)> postInfo)
        {
            if (postInfo == null || !postInfo.Any()) return new List<string>();
            var levelVerticalLoc = CmToIn(GetWorkPointProxy(level, "Center Point").Point.Y);
            var beams = GetChildOccurrencesByFactory(level, LoadBeamFactory);

            return beams?.ToList()?.SelectMany(b =>
            {
                var postsToCompare = postInfo.Where(p => IsInRange(p.Item1, GetWorkPointProxy(b, "Center Point").Point.X))?.ToList()
                                        ?? new List<(double, float)>();
                return (postsToCompare.Any() && postsToCompare.Count > 1) ?
                                                    GetBeamMiscInstallBlocks(b,
                                                        postsToCompare.FirstOrDefault().Item2 >= levelVerticalLoc,
                                                        postsToCompare.LastOrDefault().Item2 >= levelVerticalLoc)
                                                    : null;
            })?
            .Where(s => s != null)?.ToList()
            ?? new List<string>();
        }

        public bool IsInRange(double numToCompare, double location, double range = 6)
        {
            return numToCompare >= (location - (range / 2)) && numToCompare <= (location + (range / 2));
        }

        private List<string> GetBeamMiscInstallBlocks(ComponentOccurrence loadBeam, bool hasFrontDoubler, bool hasRearDoubler)
        {
            var retVal = new List<string>();
            var bracketHeight = GetPropertyValue<float>(loadBeam, "BracketHeight");

            retVal.Add(GetBeamMiscInstallBlock(bracketHeight, hasFrontDoubler));

            if (hasFrontDoubler != hasRearDoubler)
            {
                retVal.Add(GetBeamMiscInstallBlock(bracketHeight, hasRearDoubler));
            }

            return retVal;
        }

        private string GetBeamMiscInstallBlock(float bracketHeight, bool hasDoubler)
        {
            string retVal = $"B-{bracketHeight}IN";

            if (hasDoubler)
            {
                retVal = retVal + "-DOUBLER";
            }

            return retVal;
        }

        private List<string> GetCrossbarInstallBlocks()
        {
            var retList = new List<string>();

            var elevationBays = ElevationOccs.SelectMany(e => GetChildOccurrencesByFactory(e, "Bay"));

            if (elevationBays != null && elevationBays.Any())
            {
                var allLevels = elevationBays.SelectMany(b => GetChildOccurrencesByFactory(b, "Level"));

                retList = allLevels.SelectMany(l => GetCrossbarBlocks(l)).ToList();
            }

            if (retList.Any())
            {
                return retList.Distinct().ToList();
            }
            else
            {
                return retList;
            }
        }

        private List<string> GetCrossbarBlocks(ComponentOccurrence levelComp)
        {
            var retList = new List<string>();
            var crossbarType = GetPropertyValue<string>(levelComp, "PalletSupportType");
            var frontBeamOrient = GetPropertyValue<string>(levelComp, "FrontBeamOrientation");
            var rearBeamOrient = GetPropertyValue<string>(levelComp, "RearBeamOrientation");

            if (crossbarType == "xb")
            {
                retList.AddRange(GetXBCrossbarBlocks(levelComp, frontBeamOrient, rearBeamOrient));
            }
            if (crossbarType == "xbw")
            {
                retList.Add("XB-W-STD_STD");
            }
            else if (crossbarType == "xbwr1")
            {
                retList.Add("XB-W-STD_REV");
            }
            else if (crossbarType == "xbwr2")
            {
                retList.Add("XB-W-REV_REV");
            }
            else if (crossbarType == "xbw-dd2")
            {
                retList.Add("XB-W-DD-2I");
            }
            else if (crossbarType == "xbw-dd1")
            {
                if (frontBeamOrient == "std")
                {
                    retList.Add("XB-W-DD");
                }
                else
                {
                    retList.Add("XB-W-DD-REV");
                }
            }
            
            if (retList.Any())
            {
                return retList.Distinct().ToList();
            }
            else
            {
                return retList;
            }
        }

        private List<string> GetXBCrossbarBlocks(ComponentOccurrence levelComp, string frontBeamOrient, string rearBeamOrient)
        {
            var retList = new List<string>();

            if (GetPropertyValue<string>(levelComp, IsDoubleDeepPropName).ToLower() == "true")
            {
                var frontIntBeamOrient = GetPropertyValue<string>(levelComp, "FrontInteriorBeamOrientation");

                retList.Add(GetXBCrossbarBlock(frontBeamOrient, frontIntBeamOrient));

                if (GetPropertyValue<string>(levelComp, IsDoubleDeepPostPropName).ToLower() == "false")
                {
                    retList.Add(GetXBCrossbarBlock(GetPropertyValue<string>(levelComp, "RearInteriorBeamOrientation"), rearBeamOrient));
                }
            }
            else
            {
                retList.Add(GetXBCrossbarBlock(frontBeamOrient, rearBeamOrient));
            }

            return retList;
        }

        private string GetXBCrossbarBlock(string beam1Orient, string beam2Orient)
        {
            if (beam1Orient == "std" && beam2Orient == "std")
            {
                return "XB-NW-STD_STD";
            }
            else if (beam1Orient != "std" && beam2Orient != "std")
            {
                return "XB-NW-REV_REV";
            }
            else
            {
                return "XB-NW-STD_REV";
            }
        }

        
        //public List<ComponentOccurrence> Levels
        //{
        //    get
        //    {
        //        if (_levels == null)
        //        {
        //            _levels = GetChildOccurrencesByFactory(Bay, "Level").ToList();
        //        }
        //        return _levels;
        //    }
        //}

        private List<string> GetPalletStopInstallBlocks()
        {
            var retList = new List<string>();

            var palletStopOccs = GetDescendantOccurrencesByFactoryFileContains("PalletStop");

            if (palletStopOccs != null && palletStopOccs.Any())
            {
                var palletStopFactories = palletStopOccs.Select(p => GetPropertyValue<string>(p, PropertyKeys.FactoryFile)).ToList();

                if (palletStopFactories.Contains("SelectivePalletStop"))
                {
                    retList.Add("PS-R-OFFSET");
                }

                if (palletStopFactories.Contains("DoubleDeepPalletStop"))
                {
                    retList.Add("PS-R-STD");
                }
            }

            return retList;
        }

        private List<string> GetHorizontalInstallBlocks()
        {
            var retList = new List<string>();
            var boltOnHeavyFrameUprights = GetDescendantOccurrencesByFactoryFile(FrameUprightFactory);

            if (boltOnHeavyFrameUprights != null && boltOnHeavyFrameUprights.Any() && 
                boltOnHeavyFrameUprights.ToList().Any(b => GetPropertyValue<string>(b, "UprightType") == "BH"))
            {
                retList.Add("HH-BO");
            }

            return retList;
        }

        private List<string> GetFrameTieInstallBlocks()
        {
            var retList = new List<string>();

            var doubleDeepFrameLines = FrameLines.SelectMany(f => GetChildOccurrencesByFactory(f, "FrameType"))?
                                            .Where(f => GetPropertyValue<string>(f, IsDoubleDeepPropName).ToLower() == "true");

            if (doubleDeepFrameLines != null && doubleDeepFrameLines.Any())
            {
                if (doubleDeepFrameLines.Any(f => (GetPropertyValue<string>(f, IsDoubleDeepPostPropName).ToLower() == "true") &&
                                                   GetPropertyValue<string>(f, "FrameTieType").Contains("rr")))
                {
                    retList.Add("RR-1I");
                }

                if (doubleDeepFrameLines.Any(f => (GetPropertyValue<string>(f, IsDoubleDeepPostPropName).ToLower() == "false") &&
                                                   GetPropertyValue<string>(f, "FrameTieType").Contains("rr")))
                {
                    retList.Add("RR-STD");
                }

                if (doubleDeepFrameLines.Any(f => GetPropertyValue<string>(f, "FrameTieType") == "rfsw"))
                {
                    retList.Add("RS-FT-STD");
                }

                if (doubleDeepFrameLines.Any(f => GetPropertyValue<string>(f, "FrameTieType") == "rfswhd"))
                {
                    retList.Add("RS-FT-HVY");
                }
            }

            return retList;
        }

        public FootplateBlockInfo GetFootplateBlockInfo(ComponentOccurrence compOcc)
        {
            var columnAssyComp = compOcc.ParentOccurrence;
            var columnOcc = GetUprightColumnAssemblyColumnChannel(columnAssyComp);

            if (columnOcc != null)
            {
                return new FootplateBlockInfo() { FootplateOcc = compOcc, FootplateBlock = GetFootplateBlockName(compOcc, columnOcc) };
            }
            else
            {
                return null;
            }
        }

        public string GetFootplateBlockName(ComponentOccurrence compOcc, ComponentOccurrence columnOcc)
        {
            var plateType = GetPropertyValue(compOcc, "Type");
            var columnChannelWidth = GetParameterValue(columnOcc, "G_H");
            var width = GetParameterValue(compOcc, "Width");
            var holeDiameter = GetOccFractionalParameterValue(compOcc, "HoleDiameter");
            var holeDiaChar = holeDiameter.FirstOrDefault();

            return $"{plateType}-{columnChannelWidth}-{width}-{holeDiaChar}-F";
        }

        private IEnumerable<ComponentOccurrence> GetDescendantOccurrencesByFactoryFileContains(string containsValue)
        {
            return DescendantOccurrences.Where(o =>
            {
                var value = ModelCache.GetPropertyObject(o, PropertyKeys.FactoryFile).GetValue();

                return (!string.IsNullOrEmpty(value) && value.Contains(containsValue));
            });
        }

        private List<string[]> GetBomLines(ComponentOccurrence occ, int qty)
        {
            var itemNo = GetBomNumber(occ);
            var color = GetPropertyValue(occ, "Color"); 

            var occName = occ.Name;

            var maxDescLines = 10;
            var desc = new string[maxDescLines];
            for(int i = 0; i < maxDescLines; i++)
            {
                desc[i] = GetPropertyValue(occ, $"BOMDescription{i + 1}");
            }

            var lines = new List<string[]>
            {
                new string[]{ itemNo, string.IsNullOrWhiteSpace(desc[0]) ? "Descripton Needed" : desc[0], color, qty.ToString() }
            };

            lines.AddRange(desc.Skip(1)
                .Where(d => !string.IsNullOrWhiteSpace(d))
                .Select(d => new string[] { string.Empty, d, string.Empty, string.Empty }));

            return lines;
        }

        #endregion
    }

    public class ElevationSheet
    {
        public int SheetIndex { get; set; }
        public List<Document> Elevations { get; set; }
    }

    public class InstallSheet
    {
        public int SheetIndex { get; set; }
        public int BeginningBlockIndex { get; set; }
        public List<string> InstallationBlockNames { get; set; }
        public List<ComponentOccurrence> FootPlateComps { get; set; }
    }
    
    public class ProductBlockInfo
    {
        public string BlockName { get; set; }
        public double VertOffset { get; set; }
        public string Width { get; set; }
        public string Depth { get; set; }
        public string Height { get; set; }
        public string MaxWeight { get; set; }
        public string AvgWeight { get; set; }
    }

    public class CalloutInfo
    {
        public ComponentOccurrence Part { get; set; }
        public string WorkpointName { get; set; }
        public string Id { get; set; }
        public string AboveLine { get; set; }
        public string BelowLine1 { get; set; }
        public string BelowLine2 { get; set; }
        public string Side { get; set; }
        public double Xoffset { get; set; }
        public double Yoffset { get; set; }
    }

    public class FootplateBlockInfo
    {
        public ComponentOccurrence FootplateOcc { get; set; }
        public string FootplateBlock { get; set; }
    }
}
