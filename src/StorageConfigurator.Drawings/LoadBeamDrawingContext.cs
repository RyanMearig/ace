﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inventor;
using Ruler.Drawings;

namespace StorageConfigurator.Drawings
{
    public class LoadBeamDrawingContext : DefaultDrawingDataContext
    {

        #region Constructors
        public LoadBeamDrawingContext(Document document) : base(document)
        {
        }

        public LoadBeamDrawingContext(Document document, Dictionary<string, object> extraData) : base(document, extraData)
        {
        }

        public LoadBeamDrawingContext(Document document, IDrawingDataContext parent) : base(document, parent)
        {
        }

        public LoadBeamDrawingContext(ComponentOccurrence occurrence, IDrawingDataContext parent) : base(occurrence, parent)
        {
        }

        #endregion

        #region Private Properties

        private List<DimensionInfo> _horizontalDimensions;
        private ComponentOccurrence _beam;
        private List<ComponentOccurrence> _clips;
        private int _holeQty = 0;
        private int _loadQty = 0;

        #endregion

        #region Public Properties

        public int HoleQty
        {
            get
            {
                if(_holeQty == 0)
                {
                    _holeQty = GetPropertyValue<int>("HoleQty");
                }
                return _holeQty;
            }
        }

        public int LoadQty
        {
            get
            {
                if (_loadQty == 0)
                {
                    _loadQty = GetPropertyValue<int>("LoadsWide");
                }
                return _loadQty;
            }
        }

        public List<ComponentOccurrence> Clips
        {
            get
            {
               if(_clips == null)
                {
                    _clips = GetChildOccurrencesByFactoryFile("LoadBeamClip").ToList();
                }
                return _clips;  
            }
        }

        public ComponentOccurrence Beam
        {
            get
            {
                if(_beam == null)
                {
                    _beam = GetChildOccurrencesByFactoryFile("Channel").FirstOrDefault();
                }
                return _beam;
            }
        }

        public List<DimensionInfo> HorizontalDimensions
        {
            get
            {
                if(_horizontalDimensions == null)
                {
                    _horizontalDimensions = new List<DimensionInfo>();

                    //Clip Hole Centerlines
                    _horizontalDimensions.Add(new DimensionInfo()
                    {
                        Part1 = Clips.FirstOrDefault(),
                        Part2 = Clips.LastOrDefault(),
                        Point1 = "HoleCenterTop", 
                        Point2 = "HoleCenterBottom",
                        FormatText = "CL2CL",
                        YOffset = 3.25
                    });

                    //Bean Cut Length
                    _horizontalDimensions.Add(new DimensionInfo()
                    {
                        Part1 = Beam,
                        Part2 = Beam,
                        Point1 = "v010", 
                        Point2 = "v011",
                        FormatText = "BCL",
                        YOffset = 2.25
                    });

                    //Right clip to first hole
                    _horizontalDimensions.Add(new DimensionInfo()
                    {
                        Part1 = Beam,
                        Part2 = Beam,
                        Point1 = IsReversed ? "v000" : "v001",
                        Point2 = IsReversed ? $"Hole1Center" : $"Hole{HoleQty}Center",
                        FormatText = "Z",
                        YOffset = -3.25
                    });

                    var centerLHoleIndex = (int)(HoleQty / 2);
                    var centerRHoleIndex = (int)((HoleQty / 2) + 1);
                    var loadEndHoles = new List<List<int>>();
                    var psHolesPerLoad = LoadQty > 0 ? (HoleQty - LoadQty * 2) / LoadQty : 0;

                    for (int i = 0; i < LoadQty; i++)
                    {
                        var increment = 1 + psHolesPerLoad;
                        var start = (i * (increment + 1)) + 1;
                        var end = start + increment;
                        loadEndHoles.Add(new List<int>() { start, end });
                    }

                    //Between load beam to load beam in same load
                    _horizontalDimensions.AddRange(loadEndHoles.Select(holes => new DimensionInfo()
                    {
                        Part1 = Beam,
                        Part2 = Beam,
                        Point1 = $"Hole{holes.First()}Center",
                        Point2 = $"Hole{holes.Last()}Center",
                        FormatText = "Y",
                        YOffset = -2.25

                    }));

                    //Space between loads
                    for(int i = 1; i < loadEndHoles.Count; i++)
                    {
                        _horizontalDimensions.Add(new DimensionInfo()
                        {
                            Part1 = Beam,
                            Part2 = Beam,
                            Point1 = $"Hole{loadEndHoles[i - 1].Last()}Center",
                            Point2 = $"Hole{loadEndHoles[i].First()}Center",
                            FormatText = "Z3",
                            YOffset = -3.25
                        });
                    }

                    //load beam to centered pallet stop NOTE: if 3 load beams are configured, this will identify the center beam as a pallet stop hole
                    if (psHolesPerLoad > 0)
                    {
                        _horizontalDimensions.AddRange(loadEndHoles.Select(ends =>
                        {
                            var psHoles = new List<int>();
                            for (int i = 0; i < psHolesPerLoad; i++)
                            {
                                psHoles.Add(ends.Last() - i);
                                psHoles.Add(ends.Last() - i - 1);
                            }
                            return new DimensionInfo()
                            {
                                Part1 = Beam,
                                Part2 = Beam,
                                Point1 = $"Hole{psHoles.First()}Center",
                                Point2 = $"Hole{psHoles.Last()}Center",
                                FormatText = "PS",
                                YOffset = -1.25
                            };
                        }));
                    };
                }
                return _horizontalDimensions;
            }
        }

        public bool HasSquareHoles
        {
            get
            {
                return (double)GetParameterValue(Beam, "SquareHole1Offset") > 0.125;
            }
        }

        public double HoleDiameter
        {
            get
            {
                return (double)GetParameterValue(Beam, "HoleDiameter");
            }
        }

        public string HoleSizeText
        {
            get
            {
                if (HasSquareHoles)
                {
                    return $"{DrawingsHelper.DoubleToFraction((double)GetParameterValue(Beam, "SquareHoleSize"), 32)}\" SQ HOLES";
                }
                else
                {
                    return $"{DrawingsHelper.DoubleToFraction(HoleDiameter)}\" Ø HOLES";
                }
            }
        }

        public bool IsReversed
        {
            get
            {
                return GetPropertyValue("Orientation") == "rev";
            }
        }

        #endregion

        #region Public Methods


        public string GetOccFractionalParameterValue(ComponentOccurrence occ, string paramName)
        {
            var val = occ.GetParameterValue(paramName);
            if (val is double decimalVal)
            {
                var fraction = DrawingsHelper.DoubleToFraction(decimalVal);
                return DrawingsHelper.DoubleToFraction(decimalVal);
            }
            return string.Empty;
        }


        #endregion
    }
}
