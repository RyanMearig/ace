﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Ruler.Drawings;
using Ruler.InvUtils;
using Inventor;

namespace StorageConfigurator.Drawings
{
    public class StorageDrawingComponent : DrawingComponent
    {
        #region Constructors
        public StorageDrawingComponent(Document model, XElement markup, DrawingComponentOptions options, bool disableUI) : base(model, markup, options, disableUI) { }
        public StorageDrawingComponent(IInventorHost inventorHost, string modelPath, XElement markup, DrawingComponentOptions options) : base(inventorHost, modelPath, markup, options) { }

        #endregion

        #region Overrides

        protected override void AfterRenderSubtree()
        {
            base.AfterRenderSubtree();

            var elevationSheets = this.DrawingDocument.Sheets.Cast<Inventor.Sheet>()
                                                            .Where(n => n.Name.ToLower().Contains("elevation"));

            if (elevationSheets.Count() > 0)
            {
                var elevationSheetsWViews = elevationSheets.Where(es => es.DrawingViews.Count > 0);

                if (elevationSheetsWViews != null && elevationSheetsWViews.ToList().Count() > 0)
                {
                    var allViews = elevationSheetsWViews.SelectMany(s => s.DrawingViews.Cast<Inventor.DrawingView>());

                    if (allViews != null && allViews.ToList().Count() > 0)
                    {
                        var views = allViews.ToList().OrderBy(av => av.Name.Last().ToString()).ToList();
                        var maxViewHeight = views.Max(v => v.Height);

                        for (int i = 0; i < views.Count(); i++)
                        {
                            Inventor.DrawingView activeView = views[i];

                            if (activeView.Height != maxViewHeight)
                                AlignViews(maxViewHeight, activeView);
                        }
                    }
                }
            }
        }

        public void AlignViews(double maxHeight, DrawingView sideView)
        {
            double newYPosChange = (sideView.Height - maxHeight) / 2;
            Point2d newPos = sideView.Position;
            newPos.Y = newPos.Y + newYPosChange;

            sideView.Position = newPos;
        }

        #endregion

    }
}
