﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Ruler.Drawings;
using Inventor;

namespace StorageConfigurator.Drawings
{
    public class AdvanceRefSketchSymbolComponent : SketchSymbolComponent
    {
        #region Constructors

        public AdvanceRefSketchSymbolComponent(MarkupComponent parent, XElement markup, MarkupComponentOptions options) : base(parent, markup, options) { }

        #endregion

        #region Public Properties

        public string RefTemplateFilePath { get; set; }

        #endregion

        #region MarkupComponent Overrides

        protected override void ParseMarkup()
        {
            string tempFileName = this.EvaluateAttribute<string>("refTemplateFileName");

            if (!string.IsNullOrWhiteSpace(tempFileName))
            {
                if (System.IO.Path.IsPathRooted(tempFileName))
                    this.RefTemplateFilePath = tempFileName;
                else
                    this.RefTemplateFilePath = System.IO.Path.Combine(Root.TemplateFolder, tempFileName);
            }
            else
                this.RefTemplateFilePath = tempFileName;

            base.ParseMarkup();
        }

        protected override void RenderSelf()
        {
            if (!string.IsNullOrWhiteSpace(this.RefTemplateFilePath) && System.IO.File.Exists(this.RefTemplateFilePath))
                CopySketchSymbol();

            base.RenderSelf();
        }

        #endregion

        #region Public Methods

        public void CopySketchSymbol()
        {
            DrawingDocument dwgDoc = (DrawingDocument)this.InventorHost.Documents.Open(this.RefTemplateFilePath, true);
            SketchedSymbolDefinition refSketchedSymbolDef = dwgDoc.SketchedSymbolDefinitions[this.Name];

            if (refSketchedSymbolDef == null)
            {
                throw new Exception($"Unable to find Sketch Symbol '{this.Name}' in reference template file");
            }

            refSketchedSymbolDef.CopyTo((_DrawingDocument)this.Drawing, true);

            dwgDoc.Close(true);
            this.Drawing.Activate();
        }

        #endregion

    }
}
