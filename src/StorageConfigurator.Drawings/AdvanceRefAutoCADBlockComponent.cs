﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Ruler.Drawings;
using Inventor;

namespace StorageConfigurator.Drawings
{
    public class AdvanceRefAutoCADBlockComponent : AutoCADBlockComponent
    {

        #region Constructors

        public AdvanceRefAutoCADBlockComponent(MarkupComponent parent, XElement markup, MarkupComponentOptions options) : base(parent, markup, options) { }

        #endregion

        #region Constants

        private const string _defaultBlockName = "MissingBlock";
        private const string _defaultBlockAttName = "MISSINGBLOCKNAME";

        #endregion

        #region Public Properties

        public string RefTemplateFilePath { get; set; }

        public ComponentOccurrence Part { get; set; }
        public string WorkPointName { get; set; }
        public double WorkPointOffsetX { get; set; }
        public double WorkPointOffsetY { get; set; }

        #endregion

        #region MarkupComponent Overrides

        protected override void ParseMarkup()
        {
            string tempFileName = this.EvaluateAttribute<string>("refTemplateFileName");
            this.Part = this.EvaluateAttribute<ComponentOccurrence>("part");
            this.WorkPointName = this.EvaluateAttribute<string>("workpoint", defaultValue: "Origin");
            this.WorkPointOffsetX = this.EvaluateAttribute<double>("offset-x", defaultValue: 0);
            this.WorkPointOffsetY = this.EvaluateAttribute<double>("offset-y", defaultValue: 0);

            if (!string.IsNullOrWhiteSpace(tempFileName))
            {
                if (System.IO.Path.IsPathRooted(tempFileName))
                    this.RefTemplateFilePath = tempFileName;
                else
                    this.RefTemplateFilePath = System.IO.Path.Combine(Root.TemplateFolder, tempFileName);
            }
            else
                this.RefTemplateFilePath = tempFileName;

            base.ParseMarkup();
        }

        protected override void RenderSelf()
        {
            if (!string.IsNullOrWhiteSpace(this.RefTemplateFilePath) && System.IO.File.Exists(this.RefTemplateFilePath))
            {
                if(!CopyAutoCADBlock())
                {
                    AddBlockNotFoundBlock();
                    return;
                }
            }
            base.RenderSelf();
        }

        #endregion

        #region Public Methods

        public bool CopyAutoCADBlock()
        {
            DrawingDocument dwgDoc = (DrawingDocument)this.InventorHost.Documents.Open(this.RefTemplateFilePath, false);
            var refAutoCADBlockDefinitions = dwgDoc.AutoCADBlockDefinitions.Cast<AutoCADBlockDefinition>();
            var refBlockNames = refAutoCADBlockDefinitions.Select(bd => bd.Name);
            var refAutoCADBlockDefinition = dwgDoc.AutoCADBlockDefinitions.Cast<AutoCADBlockDefinition>()
                   .FirstOrDefault(bd => string.Compare(bd.Name, Name, true) == 0);

            if (refAutoCADBlockDefinition == null)
            {
                return false;
            }

            //this.Drawing.Activate();
            refAutoCADBlockDefinition.CopyTo((_DrawingDocument)this.Drawing, true);
            dwgDoc.Close(true);
            return true;
        }

        #endregion

        #region Private Methods

        private void AddBlockNotFoundBlock()
        {
            var blockDef = this.Drawing.AutoCADBlockDefinitions.Cast<AutoCADBlockDefinition>()
                   .FirstOrDefault(bd => string.Compare(bd.Name, _defaultBlockName, true) == 0);

            if (blockDef == null)
            {
                throw new Exception($"Unable to find AutoCAD block '{_defaultBlockName}' in template file");
            }

            Point2d blockLoc;

            //If view is nothing then position the block in sheet coordinates using origin-x and origin-y values
            //If there is a view then require a workpoint location and a model for locating.  Allow offset
            if (this.View != null)
            {
                if (this.Part != null)
                {
                    blockLoc = this.DataContext.GetWorkPointGeometryIntent(this.View, this.WorkPointName, this.Part).PointOnSheet;
                }
                else
                {
                    blockLoc = this.DataContext.GetWorkPointGeometryIntent(this.View, this.WorkPointName).PointOnSheet;
                }

                Vector2d transform = this.InventorHost.TransientGeometry.CreateVector2d(WorkPointOffsetX, WorkPointOffsetY);
                blockLoc.TranslateBy(transform);
            }
            else
            {
                blockLoc = this.GetInventorPoint(this.AnchorPosition);
            }

            var addedBlock = this.Sheet.AutoCADBlocks.Add(blockDef, blockLoc, this.Rotation, this.Scale);
            addedBlock.SetPromptTextValues(new string[] { _defaultBlockAttName }, new string[] { this.Name });
        }

        #endregion

    }
}
