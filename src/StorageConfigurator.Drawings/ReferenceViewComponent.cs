﻿using Inventor;
using Ruler.Drawings;
using Ruler.InvUtils;
using System.Xml.Linq;

namespace StorageConfigurator.Drawings
{
    public class ReferenceViewComponent : BaseViewComponent
    {
        #region Constructors

        public ReferenceViewComponent(MarkupComponent parent, XElement markup, MarkupComponentOptions options) : base(parent, markup, options) { }

        #endregion

        #region MarkupComponent Overrides

        protected override void RenderSelf()
        {
            // Note that the scale will potentially be changed after rendering
            this.Scale = 0.015625;
            this.ViewStyle = ViewStyles.HiddenLineRemoved;
            this.Orientation = ViewOrientations.Top;
            this.Name = "Reference View";
            base.RenderSelf();
            this.View.Suppressed = true;            
        }
        
        #endregion
        
    }
}
