﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inventor;

namespace StorageConfigurator.Drawings
{
    public class DimensionInfo
    {
        public ComponentOccurrence Part1 { get; set; }
        public string Point1 { get; set; }
        public ComponentOccurrence Part2 { get; set; }
        public string Point2 { get; set; }
        public string FormatText { get; set; }
        public double XOffset { get; set; }
        public double YOffset { get; set; }
    }
}
