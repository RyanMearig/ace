﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inventor;
using Ruler.Drawings;
using Ruler.InvUtils;

namespace StorageConfigurator.Drawings
{
    public class PostDrawingContext : DefaultDrawingDataContext
    {

        #region Constructors

        public PostDrawingContext(Document document) : base(document)
        {
        }

        public PostDrawingContext(Document document, Dictionary<string, object> extraData) : base(document, extraData)
        {
        }

        public PostDrawingContext(Document document, IDrawingDataContext parent) : base(document, parent)
        {
        }

        public PostDrawingContext(ComponentOccurrence occurrence, IDrawingDataContext parent) : base(occurrence, parent)
        {
        }

        #endregion

        #region Private Members

        private ComponentOccurrence _footplate = null;
        private string _footplateBlockName = null;
        private List<ComponentOccurrence> _channels = null;
        private ComponentOccurrence _columnChannel = null;
        private List<PartsListItem> _partsListItems = null;
        private PartsListItem _columnPartsListItem = null;
        private PartsListItem _doublerPartsListItem = null;
        private PartsListItem _footPlateListItem = null;

        #endregion

        #region ComponentCollections

        public ComponentOccurrence Bullnose
        {
            get
            {
                var bullnoses = GetDescendantOccurrencesByFactoryFile("Bullnose");
                return bullnoses.FirstOrDefault() ?? null;
            }
        }

        public ComponentOccurrence FrontAngleProtector
        {
            get
            {
                var protectors = GetDescendantOccurrencesByFactoryFile("Angle");
                return protectors.FirstOrDefault() ?? null;
            }
        }

        public ComponentOccurrence Footplate
        {
            get
            {
                if (_footplate == null)
                {
                    var footplates = GetDescendantOccurrencesByFactoryFileContains("Footplate");
                    _footplate = footplates.FirstOrDefault() ?? null;
                }
                return _footplate;
            }
        }

        public string FootplateBlockName
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_footplateBlockName))
                {
                    _footplateBlockName = GetFootplateBlockName(Footplate);
                }
                return _footplateBlockName;
            }
        }

        public List<ComponentOccurrence> Channels
        {
            get
            {
                if (_channels == null)
                {
                    _channels = GetDescendantOccurrencesByFactoryFile("Channel").ToList();
                }
                return _channels;
            }
        }

        public ComponentOccurrence ColumnChannel
        {
            get
            {
                if (_columnChannel == null)
                {
                    _columnChannel = Channels.OrderBy(c => GetParameterValue(c, "B_L")).Last();
                }
                return _columnChannel;
            }
        }

        public string DrawingName2
        {
            get
            {
                return $"{FormattedChannelSize}# CHANNEL";
            }
        }

        public string DrawingName3
        {
            get
            {
                var suffix = (Doubler == null ? "& FOOTPLATE" : "W/ DOUBLER");
                return $"POST {suffix}";
            }
        }

        public string FormattedChannelSize
        {
            get
            {
                var desc = GetPropertyValue(ColumnChannel, "Description");
                var descSplit = desc.Split('x');
                return $"{descSplit[0]} x {descSplit[1]}";
            }
        }

        public ComponentOccurrence Doubler
        {
            get
            {
                if (Channels.Count() < 2) { return null; }
                return Channels.OrderBy(c => GetParameterValue(c, "B_L")).First();
            }
        }

        public PartsListItem ColumnPartsListItem
        {
            get
            {
                if (_columnPartsListItem == null)
                {
                    _columnPartsListItem = new PartsListItem()
                    {
                        LabeledParts = new List<ComponentOccurrence>() { ColumnChannel },
                        ID = "1",
                        Material = DrawingName2,
                        Qty = 1
                    };
                }

                return _columnPartsListItem;
            }
        }

        public PartsListItem DoublerPartsListItem
        {
            get
            {
                if (_doublerPartsListItem == null)
                {
                    if (Doubler != null)
                    {
                        _doublerPartsListItem = new PartsListItem()
                        {
                            LabeledParts = new List<ComponentOccurrence>() { Doubler },
                            ID = "2",
                            Material = DrawingName2,
                            Qty = 1
                        };
                    }
                }

                return _doublerPartsListItem;
            }
        }

        public PartsListItem FootPlatePartsListItem
        {
            get
            {
                if (_footPlateListItem == null)
                {
                    _footPlateListItem = new PartsListItem()
                    {
                        LabeledParts = new List<ComponentOccurrence>() { Footplate },
                        ID = (Doubler == null ? "2" : "3"),
                        Material = "FOOT PLATE",
                        Qty = 1
                    };
                }
                return _footPlateListItem;
            }
        }

        public List<PartsListItem> PartsListItems
        {
            get
            {
                if (_partsListItems == null)
                {
                    var listItems = new List<PartsListItem>();

                    listItems.Add(ColumnPartsListItem);

                    if (Doubler != null)
                    {
                        listItems.Add(DoublerPartsListItem);
                    }

                    listItems.Add(FootPlatePartsListItem);

                    _partsListItems = listItems;
                }
                return _partsListItems;
            }
        }

        public DataTable PartsList
        {
            get
            {
                var table = new DataTable("PartsList");
                table.Columns.Add("ITEM");
                table.Columns.Add("MATERIAL");
                var rows = PartsListItems.Select(p => new string[]
                {
                    p.ID,
                    p.Material
                });
                var addedRows = new List<string[]>();
                foreach (var row in rows)
                {
                    if (!addedRows.Any(r => r[0] == row[0]))
                    {
                        table.Rows.Add(row);
                        addedRows.Add(row);
                    }
                }
                return table;
            }
        }

        #endregion

        #region Public Methods

        private IEnumerable<ComponentOccurrence> GetDescendantOccurrencesByFactoryFileContains(string containsValue)
        {
            return DescendantOccurrences.Where(o =>
            {
                var value = ModelCache.GetPropertyObject(o, PropertyKeys.FactoryFile).GetValue();

                return (!string.IsNullOrEmpty(value) && value.Contains(containsValue));
            });
        }

        public string GetFootplateBlockName(ComponentOccurrence compOcc, bool frontFootplate = false)
        {
            var plateType = GetPropertyValue(compOcc, "Type");
            var columnChannelWidth = GetParameterValue(ColumnChannel, "G_H");
            var width = GetParameterValue(compOcc, "Width");
            var holeDiameter = GetOccFractionalParameterValue(compOcc, "HoleDiameter");
            var holeDiaChar = holeDiameter.FirstOrDefault();
            var frontRearChar = frontFootplate ? "F" : "R";

            return $"{plateType}-{columnChannelWidth}-{width}-{holeDiaChar}-{frontRearChar}";
        }

        public PartsListItem GetPartsListItem(ComponentOccurrence occurrence)
        {
            var item = PartsListItems.FirstOrDefault(pl => pl.LabeledParts.Contains(occurrence));
            return item;
        }

        public string GetCalloutId(ComponentOccurrence occurrence)
        {
            var item = GetPartsListItem(occurrence);
            return item.ID;
        }

        public string GetOccFractionalParameterValue(ComponentOccurrence occ, string paramName)
        {
            var val = occ.GetParameterValue(paramName);
            if (val is double decimalVal)
            {
                var fraction = DrawingsHelper.DoubleToFraction(decimalVal);
                return DrawingsHelper.DoubleToFraction(decimalVal);
            }
            return string.Empty;
        }

        #endregion

        public class PartsListItem
        {
            public List<ComponentOccurrence> LabeledParts { get; set; }
            public string ID { get; set; }
            public string Material { get; set; }
            public int Qty { get; set; }
        }

    }
}
