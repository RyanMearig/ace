﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inventor;
using Ruler.Drawings;

namespace StorageConfigurator.Drawings
{
    public class SchematicDrawingContext : DefaultDrawingDataContext
    {

        #region Constructors

        public SchematicDrawingContext(Document document, Dictionary<string, object> extraData) : base(document, extraData)
        {
        }


        public SchematicDrawingContext(ComponentOccurrence occurrence, IDrawingDataContext parent) : base(occurrence, parent)
        {
        }

        #endregion

        #region Public Properties

        public float CrossbarScale
        {
            get
            {
                string cType = DataDocument.GetPropertyValue("CrossbarType");

                switch (cType.ToLower())
                {
                    case "xbrnd":
                    case "xbsqr":
                        return 0.228f;
                    case "xbw":
                    case "xbwr1":
                    case "xbwr2":
                        return 0.165f;
                    case "xbw-dd1":
                    case "xbw-dd2":
                        return 0.122f;
                    default:
                        return 1;
                }
            }
        }

        #endregion

        #region Public Methods

        public string FractionalPropValue(string propName, bool includeInchQuote = false)
        {
            var val = GetPropertyValue(propName);
            if (string.IsNullOrEmpty(val)) return string.Empty;
            if(!double.TryParse(val, out var decimalVal))
            {
                return val;
            }

            return DoubleToFraction(decimalVal, includeInchQuote: includeInchQuote);

        }

        public string FractionalParameterValue(string paramName, bool includeInchQuote = false)
        {
            var val = GetParameterValue(paramName);
            if(val is double decimalVal)
            {
                var fraction = DrawingsHelper.DoubleToFraction(decimalVal);
                return DoubleToFraction(decimalVal, includeInchQuote: includeInchQuote);
            }
            return string.Empty;
        }

        public string DoubleToFraction(double value, int maxDenominator = 16, bool includeInchQuote = false)
        {
            return DrawingsHelper.DoubleToFraction(value, maxDenominator) + (includeInchQuote ? @"""" : "");
        }

        #endregion
    }
}
