﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inventor;
using Ruler.Drawings;

namespace StorageConfigurator.Drawings
{
    public class UprightDrawingContext : DefaultDrawingDataContext
    {

        #region Constructors

        public UprightDrawingContext(Document document) : base(document)
        {
        }

        public UprightDrawingContext(Document document, Dictionary<string, object> extraData) : base(document, extraData)
        {
        }

        public UprightDrawingContext(Document document, IDrawingDataContext parent) : base(document, parent)
        {
        }

        public UprightDrawingContext(ComponentOccurrence occurrence, IDrawingDataContext parent) : base(occurrence, parent)
        {
        }

        #endregion

        #region Private Members

        private List<ComponentOccurrence> _horizontals = null;
        private List<ComponentOccurrence> _diagonals = null;
        private List<ComponentOccurrence> _columnAssemblies = null;
        private List<ComponentOccurrence> _slantLegTube = null;
        private ComponentOccurrence _frontFootplate = null;
        private ComponentOccurrence _rearFootplate = null;
        private ComponentOccurrence _mountAngle = null;
        private string _frontFootplateBlockName = null;
        private string _rearFootplateBlockName = null;
        private List<ComponentOccurrence> _columnChannels = null;
        private List<ComponentOccurrence> _hvyWeldPlate = null;
        private List<ComponentOccurrence> _weldAngle = null;
        private List<PartsListItem> _partsListItems = null;
        private List<PartsListItem> _columnPartsListItems = null;
        private List<PartsListItem> _doublerPartsListItems = null;
        private List<PartsListItem> _footPlateListItems = null;
        private List<PartsListItem> _weldPlateListItems = null;
        private List<PartsListItem> _weldAngleListItems = null;
        private List<PartsListItem> _mountAngleListItems = null;
        private List<string> _hDetalBlockNames = null;

        #endregion

        #region ComponentCollections

        public List<ComponentOccurrence> Horizontals
        {
            get
            {
                if(_horizontals == null)
                {
                    var angles = GetChildOccurrencesByFactoryFile("Angle");
                    var comps = angles.Concat(GetChildOccurrencesByFactoryFile("Channel"));
                    var horizontals = comps.Where(a => Math.Abs(GetWorkPointProxy(a, "v000").Point.Y - GetWorkPointProxy(a, "v001").Point.Y) < .01).ToList();
                    _horizontals = horizontals.OrderBy(a => Math.Abs(GetWorkPointProxy(a, "v000").Point.Y)).ToList();
                }
                return _horizontals;
            }
        }

        //ASP IMPROVEMENT: TO ADD HORIZONTAL NOTE
        public string HorzSize
        {
            get
            {
                var desc = GetPropertyValue(Horizontals.Last(), "Description");
                return desc;
            }
        }

        public List<ComponentOccurrence> Diagonals
        {
            get
            {
                if (_diagonals == null)
                {
                    var angles = GetChildOccurrencesByFactoryFile("Angle").Where(a => !Horizontals.Contains(a));
                    _diagonals = angles.Where(a => Math.Abs(GetWorkPointProxy(a, "v000").Point.X - GetWorkPointProxy(a, "v001").Point.X) > .01).ToList();
                }
                return _diagonals;
            }
        }

        public string DiagSize
        {
            get
            {
                var desc = GetPropertyValue(Diagonals.First(), "Description");
                return desc;
            }
        }

        public string Note
        {
            get
            {
                if (HorzSize[7] != '2' && HorzSize[8] != '2')
                {
                    if (DiagSize[7] == '2' || DiagSize[8] == '2' || DiagSize[8] == '3')
                    {
                        var type = this.GetPropertyValue("UprightType");

                        var typeId = "STD";
                        if (type == "H") typeId = "HVY";
                        if (type == "BH") typeId = "BO";
                        if (typeId == "HVY")
                        {
                            var note = "NOTE: 1/8\" X 2\" X 2\" @ 2ND HORIZONTAL";
                            return note;
                        }
                        else
                        {
                            var note = "NOTE: 1/8\" X 2\" X 2\" @ BOTTOM(2) HORIZONTALS";
                            return note;
                        }
                    }
                    else return null;
                }
                else return null;
            }
        }

        public string UprightType
        {
            get
            {
                var type = this.GetPropertyValue("UprightType");
                if (type == "L" || type == "LS")
                {
                    return type;
                }
                else return null;
            }
        }

        public List<ComponentOccurrence> ColumnAssemblies
        {
            get
            {
                if (_columnAssemblies == null)
                {
                    _columnAssemblies = GetChildOccurrencesByFactoryFile("UprightColumnAssembly").ToList();
                }
                return _columnAssemblies;
            }
        }

        public List<ComponentOccurrence> SlantTube
        {
            get
            {
                if (_slantLegTube == null)
                {
                    var frontColumn = ColumnAssemblies.First();
                    _slantLegTube = GetOccurrenceChildren(frontColumn)
                    .Where(occ => GetPropertyValue(occ, "rulerFactoryFile") == "SlantLegColumnTube").ToList();
                }
                return _slantLegTube;
            }
        }

        public ComponentOccurrence FrontDoubler
        {
            get
            {
                var frontColumn = ColumnAssemblies.First();
                var channels = GetOccurrenceChildren(frontColumn)
                    .Where(occ => GetPropertyValue(occ, "rulerFactoryFile") == "Channel");
                if(channels.Count() < 2) { return null; }
                return channels.OrderBy(c => GetParameterValue(c, "B_L")).First();
            }
        }

        public ComponentOccurrence RearDoubler
        {
            get
            {
                var rearColumn = ColumnAssemblies.Last();
                var channels = GetOccurrenceChildren(rearColumn)
                    .Where(occ => GetPropertyValue(occ, "rulerFactoryFile") == "Channel");
                if (channels.Count() < 2) { return null; }
                return channels.OrderBy(c => GetParameterValue(c, "B_L")).First();
            }
        }

        public List<ComponentOccurrence> WeldPlates
        {
            get
            {
                if (_hvyWeldPlate == null)
                {
                    _hvyWeldPlate = GetChildOccurrencesByFactoryFile("HeavyHorizontalWeldPlate").ToList();
                }
                return _hvyWeldPlate;
            }
        }

        public List<ComponentOccurrence> WeldAngle
        {
            get
            {
                if (_weldAngle == null)
                {
                    _weldAngle = GetChildOccurrencesByFactoryFile("Angle").Where(n => n._DisplayName == "P40103:1").ToList();

                }
                return _weldAngle;
            }
        }

        public ComponentOccurrence MountAngle
        {
            get
            {
                if (_mountAngle == null)
                {
                    var frontColumn = ColumnAssemblies.First();
                    _mountAngle = GetOccurrenceChildren(frontColumn)
                        .FirstOrDefault(occ => GetPropertyValue(occ, "rulerFactoryFile") == "BoltonHeavyHorizontalAngle");

                }
                return _mountAngle;
            }
        }

        public ComponentOccurrence FrontBullnose
        {
            get
            {
                var frontColumn = ColumnAssemblies.First();
                return GetOccurrenceChildren(frontColumn)
                    .FirstOrDefault(occ => GetPropertyValue(occ, "rulerFactoryFile") == "Bullnose");
            }
        }

        public ComponentOccurrence RearBullnose
        {
            get
            {
                var rearColumn = ColumnAssemblies.Last();
                return GetOccurrenceChildren(rearColumn)
                    .FirstOrDefault(occ => GetPropertyValue(occ, "rulerFactoryFile") == "Bullnose");
            }
        }

        public ComponentOccurrence FrontAngleProtector
        {
            get
            {
                var frontColumn = ColumnAssemblies.First();
                return GetOccurrenceChildren(frontColumn)
                    .FirstOrDefault(occ => GetPropertyValue(occ, "rulerFactoryFile") == "Angle");
            }
        }

        public ComponentOccurrence RearAngleProtector
        {
            get
            {
                var rearColumn = ColumnAssemblies.Last();
                return GetOccurrenceChildren(rearColumn)
                    .FirstOrDefault(occ => GetPropertyValue(occ, "rulerFactoryFile") == "Angle");
            }
        }

        public ComponentOccurrence FrontFootplate
        {
            get
            {
                if(_frontFootplate == null)
                {

                    var frontColumn = ColumnAssemblies.First();
                    _frontFootplate = GetOccurrenceChildren(frontColumn)
                        .FirstOrDefault(occ => GetPropertyValue(occ, "rulerFactoryFile").Contains("Footplate"));
                }
                return _frontFootplate;
            }
        }

        public string FrontFootplateBlockName
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_frontFootplateBlockName))
                {
                    _frontFootplateBlockName = GetFootplateBlockName(FrontFootplate);
                }

                return _frontFootplateBlockName;
            }
        }

        public ComponentOccurrence RearFootplate
        {
            get
            {
                if (_rearFootplate == null)
                {
                    var rearColumn = ColumnAssemblies.Last();
                    _rearFootplate = GetOccurrenceChildren(rearColumn)
                        .FirstOrDefault(occ => GetPropertyValue(occ, "rulerFactoryFile").Contains("Footplate"));
                }
                return _rearFootplate;
            }
        }

        public string RearFootplateBlockName
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_rearFootplateBlockName))
                {
                    _rearFootplateBlockName = GetFootplateBlockName(RearFootplate, false);
                }

                return _rearFootplateBlockName;
            }
        }

        public ComponentOccurrence UnevenRearColumn
        {
            get
            {
                var columnAssemblies = GetChildOccurrencesByFactoryFile("UprightColumnAssembly");
                var firstHeight = columnAssemblies.First().GetParameterValue("Height");
                var nonMatchingHeight = columnAssemblies.FirstOrDefault(c => c.GetParameterValue("Height") != firstHeight);
                return nonMatchingHeight;
            }
        }
        public List<ComponentOccurrence> ColumnChannels
        {
            get
            {
                if(_columnChannels == null)
                {
                    _columnChannels = ColumnAssemblies.SelectMany(c => GetOccurrenceChildren(c)
                        .Where(occ => GetPropertyValue(occ, "rulerFactoryFile") == "Channel")).ToList();
                }
                return _columnChannels;
            }
        }

        public string ForamattedChannelSize
        {
            get
            {
                var desc = GetPropertyValue(ColumnChannels.First(), "Description");
                var descSplit = desc.Split('x');
                return $"{descSplit[0]} x {descSplit[1]}";
            }
        }

        public List<PartsListItem> ColumnPartsListItems
        {
            get
            {
                if (_columnPartsListItems == null)
                {
                    var groupedChannels = ColumnChannels
                        .Where(c => c != FrontDoubler && c != RearDoubler)
                        .GroupBy(occ => occ.Definition.Document);
                    _columnPartsListItems = groupedChannels.Select((g, index) => new PartsListItem()
                    {
                        LabeledParts = new List<ComponentOccurrence>() { g.Last() },
                        ID = groupedChannels.Count() > 1 ? $"1{(index == 0 ? 'F' : 'R')}" : "1",
                        Material = "UPRIGHT COLUMN",
                        Qty = g.Count()
                    }).ToList();
                }
                return _columnPartsListItems;
            }
        }

        public List<PartsListItem> DoublerPartsListItem
        {
            get
            {
                if (_doublerPartsListItems == null)
                {
                    var doublers = new List<ComponentOccurrence>();

                    if (FrontDoubler != null)
                    {
                        doublers.Add(FrontDoubler);
                    }

                    if (RearDoubler != null)
                    {
                        doublers.Add(RearDoubler);
                    }

                    var groupedChannels = doublers?.GroupBy(occ => occ.Definition.Document);
                    var startIndex = 8;
                    _doublerPartsListItems = groupedChannels?.Select((g, index) => new PartsListItem()
                    {
                        LabeledParts = new List<ComponentOccurrence>() { g.Last() },
                        ID = (index + startIndex).ToString(),
                        Material = "DOUBLER",
                        Qty = g.Count()
                    }).ToList();
                }

                return _doublerPartsListItems;
            }
        }

        public List<PartsListItem> FootPlatePartsListItems
        {
            get
            {
                if(_footPlateListItems == null)
                {

                    var footplates = new List<ComponentOccurrence>() { FrontFootplate, RearFootplate };
                    var groupedFootplates = footplates.GroupBy(occ => occ.Definition.Document);
                    _footPlateListItems = groupedFootplates.Select((g, index) =>
                    {
                        var sideLabel = "";
                        if (groupedFootplates.Count() > 1)
                        {
                            sideLabel = index == 0 ? "FRONT" : "REAR";
                        }
                        return new PartsListItem()
                        {
                            LabeledParts = new List<ComponentOccurrence>() { g.First() },
                            ID = groupedFootplates.Count() > 1 ? $"3{(index == 0 ? 'F' : 'R')}" : "3",
                            Material = $"{sideLabel} FOOTPLATE",
                            Qty = g.Count()
                        };
                    }).ToList();
                }
                return _footPlateListItems;
            }
        }

        public List<PartsListItem> WeldPlateListItems
        {
            get
            {
                if (_weldPlateListItems == null)
                {
                    if (WeldPlates.Count() > 0)
                    {
                        _weldPlateListItems = WeldPlates.Select((g, index) =>
                        {
                            return new PartsListItem()
                            {
                                LabeledParts = new List<ComponentOccurrence>() { WeldPlates.First() },
                                ID = "2",
                                Material = "HVY WELD PLATE",
                                Qty = WeldPlates.Count()
                            };
                        }).ToList();
                    }
                }
                return _weldPlateListItems;
            }
        }

        public List<PartsListItem> WeldAngleListItems
        {
            get
            {
                if (_weldAngleListItems == null)
                {
                    if (WeldAngle.Count() > 0)
                    {
                        _weldAngleListItems = WeldAngle.Select((g, index) =>
                        {
                            return new PartsListItem()
                            {
                                LabeledParts = new List<ComponentOccurrence>() { WeldAngle.First() },
                                ID = "7",
                                Material = "WELD ANGLE",
                                Qty = WeldAngle.Count()
                            };
                        }).ToList();
                    }
                }
                return _weldAngleListItems;
            }
        }

        public List<PartsListItem> MountAngleListItems
        {
            get
            {
                if (_mountAngleListItems == null)
                {
                    if (MountAngle != null)
                    {
                        var mountangle = new List<ComponentOccurrence>() { MountAngle };
                        _mountAngleListItems = mountangle.Select((g, index) =>
                        {
                            return new PartsListItem()
                            {
                                LabeledParts = new List<ComponentOccurrence>() { mountangle.First() },
                                ID = "10",
                                Material = "MOUNT ANGLE (BOLT ON)",
                                Qty = mountangle.Count()
                            };
                        }).ToList();
                    }
                }
                return _mountAngleListItems;
            }
        }

        public List<PartsListItem> PartsListItems
        {
            get
            {
                if(_partsListItems == null)
                {
                    var listItems = new List<PartsListItem>(ColumnPartsListItems);
                    if (WeldPlateListItems != null)
                    {
                        listItems.AddRange(WeldPlateListItems);
                    }
                    listItems.AddRange(FootPlatePartsListItems);

                    var groupedHorizontals = Horizontals.GroupBy(occ => occ.Definition.Document);
                    listItems.AddRange(groupedHorizontals.Select((g, index) => new PartsListItem()
                    {
                        LabeledParts = g.ToList(),
                        ID = groupedHorizontals.Count() > 1 ? $"5-H{index + 1}" : "5-H1",
                        Material = groupedHorizontals.Count() > 1 ? $"HORIZONTAL-{index + 1}" : "STANDARD HORIZONTAL",
                        Qty = g.Count()
                    }));

                    var groupedDiagonals = Diagonals.GroupBy(occ => occ.Definition.Document);
                    listItems.AddRange(groupedDiagonals.Select((g, index) =>
                    {
                        var panelHeight = $"{ GetPanelHeightFromDiagonal(g.First()):#}";
                        return new PartsListItem()
                        {
                            LabeledParts = g.ToList(),
                            ID = $"6-{panelHeight}",
                            Material =$"DIAGONAL {panelHeight} INCH PANEL",
                            Qty = g.Count()
                        };
                    }));
                    if (WeldAngleListItems != null)
                    {
                        listItems.AddRange(WeldAngleListItems);
                    }
                    listItems.AddRange(DoublerPartsListItem);
                    if (MountAngleListItems != null)
                    {
                        listItems.AddRange(MountAngleListItems);
                    }
                    _partsListItems = listItems;
                }
                return _partsListItems;
            }
        }

        public DataTable PartsList
        {
            get
            {
                var table = new DataTable("PartsList");
                table.Columns.Add("ITEM");
                table.Columns.Add("MATERIAL");
                var rows = PartsListItems.Select(p => new string[]
                {
                    p.ID,
                    p.Material
                });
                var addedRows = new List<string[]>();
                foreach(var row in rows)
                {
                    if (!addedRows.Any(r => r[0] == row[0]))
                    {
                        table.Rows.Add(row);
                        addedRows.Add(row);
                    }
                }
                return table;
            }
        }

        public List<string> DetailHBlockNames
        {
            get
            {
                if (_hDetalBlockNames == null)
                {
                    _hDetalBlockNames = GetDeteailHBlockNames();
                }
                return _hDetalBlockNames;
            }
        }

        public string DetailHLeftBlockName
        {
            get
            {
                return DetailHBlockNames.First();
            }
        }

        public string DetailHMidBlockName
        {
            get
            {
                return DetailHBlockNames[1];
            }
        }
        public string DetailHRightBlockName
        {
            get
            {
                return DetailHBlockNames[2];
            }
        }

        public string DoublerDrawingName
        {
            get
            {
                if(FrontDoubler != null)
                {
                    return RearDoubler == null ? "FRONT DOUBLER" : "FRONT AND REAR DOUBLER";
                }
                return RearDoubler == null ? "" : "REAR DOUBLER";
            }
        }
        
        #endregion

        #region Public Methods

        public string GetFootplateBlockName(ComponentOccurrence compOcc, bool frontFootplate = true)
        {
            var plateType = GetPropertyValue(compOcc, "Type");
            var columnChannelWidth = GetParameterValue(ColumnChannels.FirstOrDefault(), "G_H");
            var width = GetParameterValue(compOcc, "Width");
            var holeDiameter = GetOccFractionalParameterValue(compOcc, "HoleDiameter");
            var holeDiaChar = holeDiameter.FirstOrDefault();
            var frontRearChar = frontFootplate ? "F" : "R";

            return $"{plateType}-{columnChannelWidth}-{width}-{holeDiaChar}-{frontRearChar}";
        }

        public PartsListItem GetPartsListItem(ComponentOccurrence occurrence)
        {
            var item = PartsListItems.FirstOrDefault(pl => pl.LabeledParts.Contains(occurrence));
            return item;
        }

        public string GetCalloutId(ComponentOccurrence occurrence)
        {
            var item = GetPartsListItem(occurrence);
            return item.ID;
        }
        
        public string GetOccFractionalParameterValue(ComponentOccurrence occ, string paramName)
        {
            var val = occ.GetParameterValue(paramName);
            if (val is double decimalVal)
            {
                var fraction = DrawingsHelper.DoubleToFraction(decimalVal);
                return DrawingsHelper.DoubleToFraction(decimalVal);
            }
            return string.Empty;
        }

        public List<string> GetDeteailHBlockNames()
        {
            var type = this.GetPropertyValue("UprightType");

            var typeId = "STD";
            if (type == "H") typeId = "HVY";
            if (type == "BH") typeId = "BO";

            var doublerId = "NO";
            if (FrontDoubler != null)
            {
                doublerId = RearDoubler == null ? "FD" : "DD";
            }
            else if (RearDoubler != null) doublerId = "RD";


            var frontDoublerId = FrontDoubler == null ? "NO" : "D";

            var protectorId = "NO";
            if (FrontBullnose != null || RearBullnose != null) protectorId = "BN";
            else if (FrontAngleProtector != null || RearAngleProtector != null) protectorId = "AP";

            var boltOnId = type == "BH" ? "BO_" : string.Empty;
            var bullnoseId = protectorId == "BN" ? "BN" : "NO";

            var leftBlock = string.Empty;
            if (typeId != "STD")
            {
                leftBlock = typeId == "HVY" ? $"HWDL_{boltOnId}{frontDoublerId}_{bullnoseId}" : $"HWDL_{boltOnId}{doublerId}_{bullnoseId}";
            }
            var midBlock = typeId == "STD" ? string.Empty : $"HWDR_{boltOnId}{doublerId}";
            var detailBlock = type == "BH" ? $"H_{typeId}_{doublerId}_{protectorId}" : string.Empty;
            //var detailBlock = type == "H" ? $"H_{typeId}_{frontDoublerId}_{protectorId}" : $"H_{typeId}_{doublerId}_{protectorId}";

            return new List<string>() { leftBlock, midBlock, detailBlock };

        }
        
        #endregion

        #region HelperMethods

        private double GetPanelHeightFromDiagonal(ComponentOccurrence diagonal)
        {
            var diagIndex = Diagonals.IndexOf(diagonal);
            try
            {
                var horizontals = new List<ComponentOccurrence>() { Horizontals[diagIndex], Horizontals[diagIndex + 1] };

                var yVals = horizontals.Select(h => GetWorkPointProxy(h, "Center Point").Point.Y).ToList();
                var deltaY = Math.Abs(yVals[0] - yVals[1]);
                return deltaY / 2.54;
            }
            catch
            {
                return 0;
            }
        }

        

        #endregion
    }


    public class PartsListItem
    {
        public List<ComponentOccurrence> LabeledParts { get; set; }
        public string ID { get; set; }
        public string Material { get; set; }
        public int Qty { get; set; }
    }
}
