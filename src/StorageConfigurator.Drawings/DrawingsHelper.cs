﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StorageConfigurator.Drawings
{
    public class DrawingsHelper
    {
        public static string DoubleToFraction(double value, int maxDenominator = 16)
        {
            var dec = 1 / Convert.ToDouble(maxDenominator);
            var number = RoundToNearest(value, dec);
            int whole = (int)number;
            int denominator = maxDenominator;
            int numerator = (int)((number - whole) * denominator);

            if (numerator == 0)
            {
                return whole.ToString();
            }
            while (numerator % 2 == 0) // simplify fraction
            {
                numerator /= 2;
                denominator /= 2;
            }
            if (whole > 0)
            {
                return string.Format("{0} {1}/{2}", whole, numerator, denominator);
            }
            else
            {
                return string.Format("{0}/{1}", numerator, denominator);
            }
        }

        public static double RoundToNearest(double value, double nearest = 1)
        {
            return ((int)Math.Round(value / nearest)) * nearest;
        }
    }
}
