﻿using Autodesk.Connectivity.WebServices;
using Autodesk.DataManagement.Client.Framework.Vault.Currency.Connections;
using Autodesk.DataManagement.Client.Framework.Vault.Currency.Entities;
using Microsoft.Extensions.Logging;

namespace StorageConfigurator.Vault
{
    public class DebugVaultWorkflowHandler : IVaultWorkflowHandler
    {

        #region IVaultWorkflowHandler Implementation

        public LfCycState GetLifeCycleState(File vaultFile, LfCycDef lifeCycleDef, string lifeCycleStateTo, ILogger logger) { return null; }
        public void SetFileCategory(FileIteration iteration, Connection conn, string categoryName, ILogger logger) { }
        public void SetLifeCycleState(string lifeCycleStateTo, File vaultFile, Connection conn, string lifeCycleDefName, ILogger logger) { }

        #endregion

    }
}
