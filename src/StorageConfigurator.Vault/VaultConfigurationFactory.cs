﻿using System;
using Ruler.Vault;
using Microsoft.Extensions.Configuration;

namespace StorageConfigurator.Vault
{
    public static class VaultConfigurationFactory
    {
        public static IConfiguration GetConfiguration()
        {
            return new ConfigurationBuilder()
                .SetBasePath(System.Environment.CurrentDirectory)
                .AddJsonFile("appsettings.json", optional: true)
                .Build();
        }

        public static VaultSettings GetVaultSettings()
        {
            var config = GetConfiguration();
            return config.GetSection("Vault").Get<VaultSettings>() ?? new VaultSettings();
        }
    }
}
