﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Connectivity.WebServices;

namespace StorageConfigurator.Vault
{
    class TransitionItem
    {
        #region Private Fields
        private LfCycTrans _transition;
        private LfCycState _toState;
        #endregion

        #region Constructors
        public TransitionItem(LfCycTrans trans, LfCycState toState)
        {
            this.Transition = trans;
            this.ToState = toState;
        }
        #endregion

        #region Public Properties
        public LfCycTrans Transition
        {
            get { return _transition; }
            private set { _transition = value; }
        }
        
        public LfCycState ToState
        {
            get { return _toState; }
            private set { _toState = value; }
        }
        #endregion

        #region Overrides
        public override string ToString()
        {
            return ToState.DispName;
        }
        #endregion
    }
}
