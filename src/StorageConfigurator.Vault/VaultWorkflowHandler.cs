﻿using Autodesk.Connectivity.WebServices;
using Autodesk.Connectivity.WebServicesTools;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using VDF = Autodesk.DataManagement.Client.Framework;

namespace StorageConfigurator.Vault
{
    public class VaultWorkflowHandler : IVaultWorkflowHandler
    {

        #region IVaultWorkflowHandler Implementation

        public void SetFileCategory(VDF.Vault.Currency.Entities.FileIteration iteration, VDF.Vault.Currency.Connections.Connection conn,
                                            string categoryName, ILogger logger)
        {
            try
            {
                if (iteration.Category.Name != categoryName)
                {
                    logger.LogInformation($@"Get Category object for Category Name '{categoryName}'");
                    VDF.Vault.Currency.Entities.EntityCategory category = conn.CategoryManager.FindCategory("File", categoryName, true);

                    logger.LogInformation($@"Update file category - {iteration.EntityName}");
                    conn.WebServiceManager.DocumentServiceExtensions.UpdateFileCategories(new long[] { iteration.EntityMasterId }, new long[] { category.ID }, "Update Category");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("Error changing Category in file: {0}  Error: {1}", iteration.EntityName, ex.Message + "  " + ex.StackTrace));
            }
        }

        public void SetLifeCycleState(string lifeCycleStateTo, Autodesk.Connectivity.WebServices.File vaultFile,
        VDF.Vault.Currency.Connections.Connection conn, string lifeCycleDefName, ILogger logger)
        {
            Autodesk.Connectivity.WebServices.File updatedFile = null;
            WebServiceManager WSManager = conn.WebServiceManager;
            LfCycDef lifeCycleDef;
            LfCycState stateTo;

            //get vault file again if it does not contain the lifecycle definition
            if (vaultFile.FileLfCyc.LfCycDefId == 0)
            {
                try
                {
                    logger.LogInformation($@"No lifecycle definitions loaded - {vaultFile.Name}");
                    lifeCycleDef = WSManager.LifeCycleService.GetAllLifeCycleDefinitions().FirstOrDefault(d => d.DispName == lifeCycleDefName);
                    stateTo = GetLifeCycleState(vaultFile, lifeCycleDef, lifeCycleStateTo, logger);

                    logger.LogInformation($@"Update file lifecycle definition '{lifeCycleDefName}' and State '{lifeCycleStateTo}' on {vaultFile.Name}");
                    updatedFile = WSManager.DocumentServiceExtensions.UpdateFileLifeCycleDefinitions(new long[] { vaultFile.MasterId }, new long[] { lifeCycleDef.Id }, new long[] { stateTo.Id }, "Update Lifecycle Defintion and State").First();
                }
                catch (Exception ex)
                {
                    throw new Exception($"Failed to set Definition or Lifecycle on {vaultFile.Name} to State {lifeCycleStateTo}" + System.Environment.NewLine + ex.Message);
                }

                if (updatedFile == null)
                {
                    throw new Exception($"Failed to set Definition or Lifecycle on {vaultFile.Name} to State {lifeCycleStateTo}.");
                }
            }
            else if (vaultFile.FileLfCyc.LfCycStateName != lifeCycleStateTo)
            {
                try
                {
                    //grab information about the lifecycle definition that the item is in
                    logger.LogInformation($@"Lifecycle state needs transitioning - {vaultFile.Name}");
                    logger.LogInformation($@"Get file lifecycle definitions - {vaultFile.Name}");
                    lifeCycleDef = WSManager.LifeCycleService.GetLifeCycleDefinitionsByIds(new long[] { vaultFile.FileLfCyc.LfCycDefId }).First();
                    stateTo = GetLifeCycleState(vaultFile, lifeCycleDef, lifeCycleStateTo, logger);

                    logger.LogInformation($@"Update LifeCycle State on {vaultFile.Name} to '{lifeCycleStateTo}'");
                    updatedFile = WSManager.DocumentServiceExtensions.UpdateFileLifeCycleStates(new long[] { vaultFile.MasterId }, new long[] { stateTo.Id }, "Update Lifecycle State").First();
                }
                catch (Exception ex)
                {
                    throw new Exception($"Failed to set Lifecycle on {vaultFile.Name} to State {lifeCycleStateTo}" + System.Environment.NewLine + ex.Message);
                }

                if (updatedFile == null)
                {
                    throw new Exception($"Failed to set Lifecycle on {vaultFile.Name} to State {lifeCycleStateTo}");
                }
            }
        }

        public LfCycState GetLifeCycleState(Autodesk.Connectivity.WebServices.File vaultFile, LfCycDef lifeCycleDef,
                                            string lifeCycleStateTo, ILogger logger)
        {
            //get all life cycle states in the passed in file's LifeCycleDefinition
            logger.LogInformation($@"Getting all Lifecycle States for LifeCycleDefinition - {lifeCycleDef.DispName}");
            List<TransitionItem> lifeCycleStates = GetLifeCycleStateItems(vaultFile, lifeCycleDef, logger);

            //grab the lifecyclestate
            logger.LogInformation($@"Get {lifeCycleStateTo} transition object");
            return lifeCycleStates.Where(b => b.ToState.DispName.ToUpper() == lifeCycleStateTo.ToUpper()).FirstOrDefault().ToState;
        }

        #endregion

        #region Private Methods

        private List<TransitionItem> GetLifeCycleStateItems(Autodesk.Connectivity.WebServices.File vaultFile, LfCycDef lifeCycleDef, ILogger logger)
        {
            List<TransitionItem> result = new List<TransitionItem>();

            // create a map of the states so that we can look them up easily.
            Dictionary<long, LfCycState> stateMap = lifeCycleDef.StateArray.ToDictionary(n => n.Id);

            //No lifecycle has been set
            if (vaultFile.FileLfCyc.LfCycStateId == -1)
            {
                result.AddRange(lifeCycleDef.TransArray.Select(t => new TransitionItem(t, stateMap[t.ToId])).ToList());
            }
            else
            {
                foreach (LfCycTrans trans in lifeCycleDef.TransArray)
                {
                    // figure out the transitions from the current state and add them to the combo box
                    if (trans.FromId == vaultFile.FileLfCyc.LfCycStateId)
                    {
                        TransitionItem item = new TransitionItem(trans, stateMap[trans.ToId]);
                        result.Add(item);
                    }
                }
            }

            return result;
        }

        #endregion

    }
}
