﻿using Autodesk.Connectivity.WebServices;
using Autodesk.Connectivity.WebServicesTools;
using Autodesk.DataManagement.Client.Framework.Vault.Currency.Connections;
using Engenious.Common;
using Engenious.Inventor;
using Ruler.Vault;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using VDF = Autodesk.DataManagement.Client.Framework;
using System.Diagnostics;
using File = Autodesk.Connectivity.WebServices.File;

namespace StorageConfigurator.Vault
{
    public class VaultUploader
    {
        #region Private Fields
        private List<(string ComponentPath, FileAssocParam AssocParam)> _uploadedFileAssocParams;
        private List<string> _uploadedFilePaths;
        private readonly string _rootFolder;
        private readonly string _factoryFilesFolder;
        private readonly string _memberFilesFolder;
        private readonly string _contentCenterFolder;
        private string _memberFileCategoryName;
        private string _nonMemberFileCategoryName;
        public string _memberFileLifecycleDefinitionName;
        public string _nonMemberFileLifecycleDefinitionName;
        private string _nonMemberFileFinalState;
        private string _memberFileFinalState;
        private string _transitionStateAfterReleased;
        private string _preReleasedStateName;
        private string _releasedStateName;
        private VaultSettings _vaultSettings;
        private IVaultWorkflowHandler _workflowHandler;

        private Inventor.ApprenticeServerComponent _appr;
        private Inventor.Application _invApp;

        #endregion

        #region Constants

        public const string RootFolder = "INVENTOR MODELS";
        public const string FactoryFilesFolder = "FACTORY FILES";
        public const string MemberFilesFolder = "MEMBER FILES";
        public const string ContentCenterFolder = "CONTENT CENTER FILES";

        public const string DefaultNonMemberFileCategoryName = "Standard";
        public const string DefaultNonMemberFileLifecycleDefinitionName = "Engineering Release Process";
        public const string DefaultNonMemberFileFinalState = "Work In Progress";

        public const string DefaultMemberFileCategoryName = "Standard";
        public const string DefaultMemberFileLifecycleDefinitionName = "Engineering Release Process";
        public const string DefaultMemberFileFinalState = "Pre-Released";

        public const string DefaultStateAfterReleased = "Work In Progress";

        private const string DefaultPreReleasedStateName = "Pre-Released";
        private const string DefaultReleasedStateName = "Released";


        #endregion

        #region Constructors

        public VaultUploader(ILogger logger, VaultSettings vaultSettings, IVaultWorkflowHandler workflowHandler, Inventor.Application inventorApp)
                                    :this(RootFolder, logger, vaultSettings, workflowHandler, inventorApp)
        {
        }

        public VaultUploader(string rootFolder, ILogger logger, VaultSettings vaultSettings, IVaultWorkflowHandler workflowHandler, Inventor.Application inventorApp)
            :this(rootFolder, FactoryFilesFolder, MemberFilesFolder, ContentCenterFolder, logger, vaultSettings, workflowHandler, inventorApp)
        {
        }

        public VaultUploader(string rootFolder, string factoryFilesFolder, string memberFilesFolder, string contentCenterFolder, ILogger logger, VaultSettings vaultSettings, IVaultWorkflowHandler workflowHandler, Inventor.Application inventorApp)
        {
            _rootFolder = rootFolder;
            _factoryFilesFolder = factoryFilesFolder;
            _memberFilesFolder = memberFilesFolder;
            _contentCenterFolder = contentCenterFolder;
            this.Logger = logger;
            _vaultSettings = vaultSettings;
            _workflowHandler = workflowHandler;
            _appr = new Inventor.ApprenticeServerComponent();
            _invApp = inventorApp;
        }

        #endregion

        #region Protected Properties

        protected ILogger Logger { get; }

        #endregion

        #region Public Properties

        public string MemberFileCategoryName
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_memberFileCategoryName))
                {
                    _memberFileCategoryName = DefaultMemberFileCategoryName;
                }

                return _memberFileCategoryName;
            }
            set { _memberFileCategoryName = value; }
        }
        
        public string MemberFileLifecycleDefinitionName
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_memberFileLifecycleDefinitionName))
                {
                    _memberFileLifecycleDefinitionName = DefaultMemberFileLifecycleDefinitionName;
                }

                return _memberFileLifecycleDefinitionName;
            }
            set { _memberFileLifecycleDefinitionName = value; }
        }

        public string MemberFileFinalState
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_memberFileFinalState))
                {
                    _memberFileFinalState = DefaultMemberFileFinalState;
                }

                return _memberFileFinalState;
            }
            set { _memberFileFinalState = value; }
        }

        public string NonMemberFileCategoryName
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_nonMemberFileCategoryName))
                {
                    _nonMemberFileCategoryName = DefaultNonMemberFileCategoryName;
                }

                return _nonMemberFileCategoryName;
            }
            set { _nonMemberFileCategoryName = value; }
        }

        public string NonMemberFileLifecycleDefinitionName
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_nonMemberFileLifecycleDefinitionName))
                {
                    _nonMemberFileLifecycleDefinitionName = DefaultNonMemberFileLifecycleDefinitionName;
                }

                return _nonMemberFileLifecycleDefinitionName;
            }
            set { _nonMemberFileLifecycleDefinitionName = value; }
        }

        public string NonMemberFileFinalState
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_nonMemberFileFinalState))
                {
                    _nonMemberFileFinalState = DefaultNonMemberFileFinalState;
                }

                return _nonMemberFileFinalState;
            }
            set { _nonMemberFileFinalState = value; }
        }

        public string TransitionStateAfterReleased
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_transitionStateAfterReleased))
                {
                    _transitionStateAfterReleased = DefaultStateAfterReleased;
                }

                return _transitionStateAfterReleased;
            }
            set { _transitionStateAfterReleased = value; }
        }

        public string PreReleasedStateName
        {
            get
            {
                if (string.IsNullOrEmpty(_preReleasedStateName))
                {
                    _preReleasedStateName = DefaultPreReleasedStateName;
                }

                return _preReleasedStateName;
            }
            set { _preReleasedStateName = value; }
        }

        public string ReleasedStateName
        {
            get
            {
                if (string.IsNullOrEmpty(_releasedStateName))
                {
                    _releasedStateName = DefaultReleasedStateName;
                }

                return _releasedStateName;
            }
            set { _releasedStateName = value; }
        }

        #endregion

        #region Public Methods

        public string UploadLineItemToVault(string assemblyPath, List<(string OutputPath, string ComponentPath)> outputFilePaths)
        {
            ProcessResult result = UploadToVault(assemblyPath, outputFilePaths);

            if (result.Success)
                return "";
            else
                return result.Message + " Stack Trace: " + result.DetailText;
        }

        public ProcessResult UploadToVault(string assemblyPath, List<(string OutputPath, string ComponentPath)> outputFilePaths)
        {
            ProcessResult result = new ProcessResult(true);
            _uploadedFileAssocParams = new List<(string ComponentPath, FileAssocParam AssocParam)>();
            _uploadedFilePaths = new List<string>();

            List<FileAssocParam> associatedList = new List<FileAssocParam>();

            //var george = Library.ConnectionManager.LogIn("127.0.0.1", "Advance_Vault", "Administrator", "", AuthenticationFlags.Standard, null);
            VDF.Vault.Currency.Connections.Connection connection = ConnectionFactory.GetConnection(_vaultSettings);

            if (connection == null)
                return new ProcessResult(false, "Unable to get Vault connection");

            try
            {
                //upload assembly children
                this.Logger.LogInformation($"Uploading children");
                associatedList = UploadAssemblyStructure(assemblyPath, connection);
            }
            catch (Exception ex)
            {
                return new ProcessResult(false, ex.Message, ex.StackTrace);
            }

            //upload assembly
            this.Logger.LogInformation($"Uploading lineitem");
            VDF.Vault.Currency.Entities.FileIteration assyIteration = UpdateVaultFile(assemblyPath, connection, associatedList);

            if (assyIteration == null)
                return new ProcessResult(false, "Unable to add " + assemblyPath + " to the vault");

            _uploadedFileAssocParams.Add((ComponentPath: assemblyPath, GetFileAssocParam(connection, assyIteration.EntityName, AssociationType.Dependency)));

            //upload outputs
            this.Logger.LogInformation($"Uploading outputs");
            List<(string ComponentPath, FileAssocParam AssocParam)> distinctUploadedFileAssocParams = _uploadedFileAssocParams.GroupBy(x => x.ComponentPath).Select(p => p.First()).ToList();

            foreach (var paths in outputFilePaths)
            {
                if (System.IO.File.Exists(paths.OutputPath) == false)
                    return new ProcessResult(false, $@"{paths.OutputPath} does not exist");

                VDF.Vault.Currency.Entities.FileIteration iteration = UpdateVaultFile(paths.OutputPath, connection, GetOutputFileAssocParams(paths.ComponentPath, distinctUploadedFileAssocParams, connection));

                if (iteration == null)
                    return new ProcessResult(false, "Unable to add " + paths.OutputPath + " to the vault");
            }

            if (connection.IsConnected)
                VDF.Vault.Library.ConnectionManager.LogOut(connection);

            return result;
        }

        public VDF.Vault.Currency.Entities.FileIteration UpdateVaultFile(string filePath, VDF.Vault.Currency.Connections.Connection conn, List<FileAssocParam> associatedFiles, bool keepExistingAssociations = false)
        {
            ProcessResult result = new ProcessResult(true);
            bool shouldUploadNewVersion = true;
            VDF.Vault.Currency.Entities.FileIteration newIteration = null;

            List<FileAssocParam> associationList = new List<FileAssocParam> { };
            associationList.AddRange(associatedFiles);

            WebServiceManager WSManager = conn.WebServiceManager;
            string fileName = Path.GetFileName(filePath);

            this.Logger.LogInformation($@"Get Vault File - {fileName}");
            Autodesk.Connectivity.WebServices.File vaultFile = WSManager.DocumentService.GetFileByFileName(fileName);
                                    
            if (vaultFile == null)
            {
                this.Logger.LogInformation($@"Get Vault Folder - {filePath}");
                VDF.Vault.Currency.Entities.Folder uploadFolder = GetVaultFolder(filePath, conn);

                this.Logger.LogInformation($@"Uploading new file to Vault - {filePath}");
                //add file and return iteration if not already in vault
                newIteration = UploadFile(filePath, conn, uploadFolder, associationList);

                _uploadedFilePaths.Add(filePath);
            }
            else if (vaultFile.CheckedOut)
                throw new Exception(String.Format("File '{0}' is Checked Out.", vaultFile.Name));
            else if (filePath.ToUpper().Contains(ContentCenterFolder))
                newIteration = vaultFile.GetIteration(conn);
            else
            {
                if (filePath.ToUpper().Contains(_memberFilesFolder))
                {
                    long vaultFileChecksum = vaultFile.GetIteration(conn).Checksum;
                    long localFileChecksum = (int)Checksum.CalcCRC32(filePath);

                    shouldUploadNewVersion = (localFileChecksum != vaultFileChecksum);
                }
                else
                {
                    shouldUploadNewVersion = (System.IO.File.GetLastWriteTime(filePath) > vaultFile.ModDate);
                }

                //TODO: check hash and not modified dates

                //Compare Output Hash iProperty vs Vault Hash iProperty.
                //  Requires iLogic to remove Hash                
                //      If Output.iPropHash = Vault.iPropHash, then DO NOT upload
                //      If Vault.iPropHash is EMPTY, then DO WHAT?!?!
                //      If Output.iPropHash <> Vault.iPropHash, then DO upload new version to Vault



                //Compare Output binary file vs Vault binary file.
                //  Any user change will cause the binary to be changed.
                //      If Output.Binary = Vault.Binary, then DO NOT upload
                //      If Output.Binary <> Vault.Binary, then check more stuff about the files
                //          If Output.Date is older than Vault.Date, then DO NOT upload
                //          If Output.Date is newer than Vault.Date, then DO upload  <--- unsafe to rely on output server date

                this.Logger.LogInformation($@"Should Upload New Version = {shouldUploadNewVersion}");
                if (shouldUploadNewVersion)
                {
                    newIteration = UploadNewVersion(conn, vaultFile, filePath, associationList);
                }
                else
                {
                    newIteration = vaultFile.GetIteration(conn);
                }
            }

            return newIteration;
        }
        private VDF.Vault.Currency.Entities.FileIteration UploadNewVersion(VDF.Vault.Currency.Connections.Connection conn, Autodesk.Connectivity.WebServices.File vaultFile,
                                                                            string filePath, List<FileAssocParam> associationList, bool keepExistingAssociations = false)
        {
            VDF.Vault.Currency.Entities.FileIteration newIteration = null;

            if ((vaultFile.FileLfCyc.LfCycStateName == PreReleasedStateName))
            {
                throw new Exception($"Unable to Upload New Version: File '{vaultFile.Name}' is in the {PreReleasedStateName} Lifecycle State.");
            }
            else
            {
                //This is done due to vault restrictions that lock a file if it is in the Released state where it cannot be checked out
                if (vaultFile.FileLfCyc.LfCycStateName == ReleasedStateName)
                {
                    this.Logger.LogInformation($@"File is released - {vaultFile.Name}");
                    SetLifeCycleState(filePath, TransitionStateAfterReleased, vaultFile, conn);
                }

                newIteration = UploadNewFileVersion(conn, vaultFile, filePath, associationList, keepExistingAssociations);

                _uploadedFilePaths.Add(filePath);

                SetFileCategory(filePath, newIteration, conn);

                if (filePath.ToUpper().Contains(_memberFilesFolder))
                {
                    this.Logger.LogInformation($@"Set Member File Lifecycle state - {vaultFile.Name}");
                    SetLifeCycleState(filePath, MemberFileFinalState, vaultFile, conn);
                }
                else
                {
                    this.Logger.LogInformation($@"Set Non-Member File Lifecycle state - {vaultFile.Name}");
                    SetLifeCycleState(filePath, NonMemberFileFinalState, vaultFile, conn);
                }
            }

            return newIteration;
        }

        private VDF.Vault.Currency.Entities.FileIteration UploadNewFileVersion(VDF.Vault.Currency.Connections.Connection conn, Autodesk.Connectivity.WebServices.File vaultFile,
                                                                            string filePath, List<FileAssocParam> associationList, bool keepExistingAssociations = false)
        {
            VDF.Vault.Currency.Entities.FileIteration newIteration = null;

            try
            {
                VDF.Vault.Settings.AcquireFilesSettings acqSettings = new VDF.Vault.Settings.AcquireFilesSettings(conn, false);
                acqSettings.DefaultAcquisitionOption = VDF.Vault.Settings.AcquireFilesSettings.AcquisitionOption.Checkout;

                this.Logger.LogInformation($@"Check out - {filePath}");
                acqSettings.AddEntityToAcquire(vaultFile.GetIteration(conn));

                VDF.Vault.Results.AcquireFilesResults acqResults = conn.FileManager.AcquireFiles(acqSettings);
                newIteration = acqResults.FileResults.First().NewFileIteration;

                //add current file associations to list
                if (keepExistingAssociations)
                {
                    this.Logger.LogInformation($"Keep Existing Associations - {vaultFile.Name}");
                    associationList.AddRange(newIteration.GetFileAssocParam(conn));
                }

                var associatedFilesArraysWithRefIds = GetFileAssociations(filePath, associationList);

                this.Logger.LogInformation($@"Check in file - {vaultFile.Name}");
                var doc = _invApp.Documents.Open(filePath, false);
                doc.Save();
                doc.Close();

                var fileClass = GetExistingFileClassification(newIteration);
                this.Logger.LogInformation($@"Vault File Class - {fileClass}");

                var vaultFileAbsolute = new VDF.Currency.FilePathAbsolute(filePath);
                this.Logger.LogInformation($@"Vault File Absolute - {vaultFileAbsolute.FullPath}");

                newIteration = conn.FileManager.CheckinFile(newIteration, "Updated from the Configurator", false, associatedFilesArraysWithRefIds, null, false, null, fileClass, false, vaultFileAbsolute);


                RemoveReadOnlyAtt(filePath);
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("Error Checking in file: {0}  Error: {1}", vaultFile.Name, ex.Message + "  || " + ex.StackTrace));
            }

            return newIteration;
        }

        private void SetLifeCycleState(string filePath, string lifeCycleStateTo, Autodesk.Connectivity.WebServices.File vaultFile, VDF.Vault.Currency.Connections.Connection conn)
        {
            if (filePath.ToUpper().Contains(_memberFilesFolder))
            {
                _workflowHandler.SetLifeCycleState(lifeCycleStateTo, vaultFile, conn, MemberFileLifecycleDefinitionName, this.Logger);
            }
            else
            {
                _workflowHandler.SetLifeCycleState(lifeCycleStateTo, vaultFile, conn, NonMemberFileLifecycleDefinitionName, this.Logger);
            }
        }

        public void SetFileCategory(string filePath, VDF.Vault.Currency.Entities.FileIteration iteration, VDF.Vault.Currency.Connections.Connection conn)
        {
            if (filePath.ToUpper().Contains(_memberFilesFolder))
            {
                _workflowHandler.SetFileCategory(iteration, conn, this.MemberFileCategoryName, this.Logger);
            }
            else
            {
                _workflowHandler.SetFileCategory(iteration, conn, this.NonMemberFileCategoryName, this.Logger);
            }
        }

        #endregion

        #region Private Methods

        private FileClassification GetExistingFileClassification(File iteration)
        {
            return iteration.FileClass;
        }

        private FileClassification GetNewFileClassification(string fileName)
        {
            var extension = Path.GetExtension(fileName).ToLower();

            switch (extension)
            {
                case ".ipt":
                case ".iam":
                    return FileClassification.None;
                default:
                    return FileClassification.DesignDocument;
            }
        }

        private List<FileAssocParam> UploadAssemblyStructure(string assemblyPath, VDF.Vault.Currency.Connections.Connection conn)
        {
            List<FileAssocParam> associatedList = new List<FileAssocParam>();
            HierarchicalFile assyHierachical = GetHierarchicalFile(assemblyPath);

            //upload assembly children 
            foreach (HierarchicalFile child in assyHierachical.Children)
            {
                associatedList.Add(UploadHeirarchy(child, conn));
            }

            return associatedList;
        }

        private void GetRefIds(string filePath, out string[] paths, out int[] refIds)
        {
            this.Logger.LogInformation($@"Apprentice Project File - {_appr.DesignProjectManager.ActiveDesignProject.Name}");
            this.Logger.LogInformation($@"Getting RefIds for - {filePath}");

            Inventor.ApprenticeServerDocument appd = _appr.Open(filePath);

            string DatabaseRevisionId, LastSavedLocation;
            object Indices, OldPaths, CurrentPaths;
            appd._GetReferenceInfo(out DatabaseRevisionId, out LastSavedLocation, out Indices, out OldPaths, out CurrentPaths, false);
            
            paths = (string[])OldPaths;
            refIds = (int[])Indices;
        }

        private FileAssocParam[] GetFileAssociations(string filePath, List<FileAssocParam> associatedFiles)
        {
            FileAssocParam[] associatedFilesArray = associatedFiles.ToArray<FileAssocParam>();
            string[] paths;
            int[] refIds;

            var doc = _invApp.Documents.Open(filePath, false);

            doc.Update();
            doc.Save();
            doc.Close();

            GetRefIds(filePath, out paths, out refIds);

            var count = paths.Count();
            this.Logger.LogInformation($@"Found {count} ref Ids");

            var associatedFilesWithRefIds = new List<FileAssocParam>();

            for (int i = 0; i < count; i++)
            {
                var path = paths[i];
                FileAssocParam assocFile = null;
                var refIdString = refIds[i].ToString();
                string matchVaultPath = "";

                if (String.IsNullOrEmpty(path) == false || String.IsNullOrEmpty(refIdString) == false)
                {
                    var vaultPath = path.Replace("\\", "/");

                    this.Logger.LogInformation($@"Getting File Assoc for - {vaultPath}");

                    matchVaultPath = vaultPath.Replace("C:/Inventor Models/", "");
                    this.Logger.LogInformation($@"Get existing Assoc for {matchVaultPath}, RefId={refIdString}");

                    foreach (var file in associatedFilesArray)
                    {
                        var expectedPath = file.ExpectedVaultPath ?? $"NULL PATH - {file.CldFileId}";
                        this.Logger.LogInformation($@"Compare {matchVaultPath} to {expectedPath}");
                        var expectedFileName = Path.GetFileNameWithoutExtension(expectedPath);
                        this.Logger.LogInformation($@"Expected FileName {expectedFileName}");
                        if (matchVaultPath.Contains(expectedFileName))
                        {
                            this.Logger.LogInformation($@"{matchVaultPath} contains {expectedFileName}");
                            assocFile = file;
                            break;
                        }
                        this.Logger.LogInformation($@">>>{matchVaultPath}<<< DOES NOT CONTAIN >>>{expectedFileName}<<<");
                    }
                }

                if (assocFile != null)
                {
                    this.Logger.LogInformation($@"Creating FileAssocParam - {assocFile.ExpectedVaultPath}, RefId={refIdString}");
                    var fileParam = new FileAssocParam()
                    {
                        CldFileId = assocFile.CldFileId,
                        ExpectedVaultPath = assocFile.ExpectedVaultPath,
                        Typ = AssociationType.Dependency,
                        Source = "INVENTOR",
                        RefId = refIdString,
                    };
                    associatedFilesWithRefIds.Add(fileParam);
                }
                else
                {
                    this.Logger.LogInformation($@"FILE ASSOC NOT FOUND FOR - {matchVaultPath}, RefId={refIdString}");
                }
            }

            var associatedFilesArraysWithRefIds = associatedFilesWithRefIds.ToArray();

            this.Logger.LogInformation($@"Associated file count - {associatedFilesArraysWithRefIds.Count()}");

            return associatedFilesArraysWithRefIds;
        }

        private VDF.Vault.Currency.Entities.FileIteration UploadFile(string filePath, VDF.Vault.Currency.Connections.Connection conn, VDF.Vault.Currency.Entities.Folder uploadFolder, List<FileAssocParam> associatedFiles)
        {
            ProcessResult result = new ProcessResult(true);
            FileAssocParam[] associatedFilesArray = associatedFiles.ToArray<FileAssocParam>();
            VDF.Vault.Currency.Entities.FileIteration iteration = null;

            this.Logger.LogInformation($"Calling AddFile() for {filePath}");
            var vaultUploadStopWatch = new Stopwatch();
            vaultUploadStopWatch.Start();

            var associatedFilesArraysWithRefIds = GetFileAssociations(filePath, associatedFiles);

            var filePathAbsolute = new VDF.Currency.FilePathAbsolute(filePath);

            iteration = conn.FileManager.AddFile(uploadFolder, "Uploaded from the Configurator",
                                                    associatedFilesArraysWithRefIds, null, GetNewFileClassification(filePath),
                                                    false, filePathAbsolute);

            vaultUploadStopWatch.Stop();

            var uploadTimeSpan = vaultUploadStopWatch.Elapsed;
            string uploadElapsedTime = string.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                                    uploadTimeSpan.Hours, uploadTimeSpan.Minutes, uploadTimeSpan.Seconds, uploadTimeSpan.Milliseconds / 10);
            this.Logger.LogInformation("    AddFile() completed in " + uploadElapsedTime);

            SetFileCategory(filePath, iteration, conn);

            RemoveReadOnlyAtt(filePath);

            return iteration;
        }

        private VDF.Vault.Currency.Entities.Folder GetVaultFolder(string filePath, VDF.Vault.Currency.Connections.Connection connection)
        {
            VDF.Vault.Services.Connection.IFolderManager folderMan = connection.FolderManager;
            VDF.Vault.Currency.Entities.Folder jobFolder = folderMan.RootFolder;

            string workingFolder = _rootFolder;

            if (filePath.ToUpper().Contains(ContentCenterFolder))
                workingFolder = ContentCenterFolder;

            string folderLoc = System.IO.Path.GetDirectoryName(filePath);
            Char delimiter = '\\';

            string[] paths = folderLoc.Split(delimiter);
            bool foundWorkspace = false;

            foreach (string folderName in paths)
            {
                if (foundWorkspace)
                    jobFolder = GetVaultFolder(connection, jobFolder, folderName);

                if (folderName.ToUpper() == workingFolder.ToUpper())
                    foundWorkspace = true;
            }

            return jobFolder;
        }

        private VDF.Vault.Currency.Entities.Folder GetVaultFolder(VDF.Vault.Currency.Connections.Connection connection, VDF.Vault.Currency.Entities.Folder ParentFolder, string FolderName)
        {
            string validFolderName = string.Empty;

            VDF.Vault.Services.Connection.IFolderManager folderMan = connection.FolderManager;
            folderMan.ValidateFolderName(FolderName, out validFolderName);

            IEnumerable<VDF.Vault.Currency.Entities.Folder> allFolders = folderMan.GetChildFolders(ParentFolder, true, true);
            IEnumerable<VDF.Vault.Currency.Entities.Folder> folderCollection = (from f in allFolders
                                                                                where f.EntityName.ToLower() == validFolderName.ToLower()
                                                                                select f);

            if (folderCollection.Count() == 0)
            {
                Logger.LogInformation($@"Creating Vault Folder '{validFolderName}' in Parent Folder '{ParentFolder.FolderPath}'");
                VDF.Vault.Currency.Entities.EntityCategory category = VDF.Vault.Currency.Entities.EntityCategory.EmptyCategory;

                IEnumerable<VDF.Vault.Currency.Entities.EntityCategory> categories = connection.CategoryManager.GetAvailableCategories("FLDR");

                if (categories != null && categories.Any())
                    category = categories.ToList().FirstOrDefault(c => c.IsDefault);

                return folderMan.CreateFolder(ParentFolder, validFolderName, ParentFolder.IsLibraryFolder, category);
            }
            else
                return folderCollection.First();
        }

        private FileAssocParam UploadHeirarchy(HierarchicalFile hFile, VDF.Vault.Currency.Connections.Connection conn, List<FileAssocParam> fileAssociations = null)
        {
            VDF.Vault.Currency.Entities.FileIteration iteration = null;
            List<HierarchicalFile> uploadedHierFiles = new List<HierarchicalFile>();
            FileAssocParam componentFileAssocParam;
            WebServiceManager WSManager = conn.WebServiceManager;

            if (fileAssociations == null)
                fileAssociations = new List<FileAssocParam>();

            foreach (HierarchicalFile hChild in hFile.Children)
            {
                // if child is assembly with children recurse through children
                if (hChild.Children.Count() > 0)
                {
                    this.Logger.LogInformation($@"Upload children");
                    FileAssocParam param = UploadHeirarchy(hChild, conn);
                    fileAssociations.Add(param);
                }
                else
                {
                    VDF.Vault.Currency.Entities.FileIteration childIteration = null;

                    //no need to update file if added this session
                    if (!uploadedHierFiles.Contains(hChild))
                    {
                        childIteration = UpdateVaultFile(hChild.Filename, conn, new List<FileAssocParam>());

                        if (childIteration == null)
                            throw new Exception("Unable to add " + hChild.Filename + " to the vault");

                        uploadedHierFiles.Add(hChild);
                    }
                    else
                    {
                        string vaultFileName = Path.GetFileName(hChild.Filename);

                        Autodesk.Connectivity.WebServices.File childVaultFile = WSManager.DocumentService.GetFileByFileName(vaultFileName);
                        childIteration = childVaultFile.GetIteration(conn);
                    }

                    componentFileAssocParam = GetFileAssocParam(conn, hChild.Filename, AssociationType.Dependency);
                    fileAssociations.Add(componentFileAssocParam);
                    _uploadedFileAssocParams.Add((ComponentPath: hChild.Filename, AssocParam: componentFileAssocParam));
                }
            }

            if (!uploadedHierFiles.Contains(hFile))
            {
                iteration = UpdateVaultFile(hFile.Filename, conn, fileAssociations);

                if (iteration == null)
                    throw new Exception("Unable to add " + hFile.Filename + " to the vault");

                uploadedHierFiles.Add(hFile);
            }

            componentFileAssocParam = GetFileAssocParam(conn, hFile.Filename, AssociationType.Dependency, "Inventor");
            _uploadedFileAssocParams.Add((ComponentPath: hFile.Filename, AssocParam: componentFileAssocParam));

            return componentFileAssocParam;
        }

        private List<FileAssocParam> GetOutputFileAssocParams(string componentFilepath, List<(string ComponentPath, FileAssocParam AssocParam)> distinctUploadedFileAssocParams, VDF.Vault.Currency.Connections.Connection conn)
        {
            List<FileAssocParam> retVal = new List<FileAssocParam>();

            if (_uploadedFileAssocParams != null && _uploadedFileAssocParams.Count > 0)
            {
                List<(string ComponentPath, FileAssocParam AssocParam)> compMatches = distinctUploadedFileAssocParams.Where(s => s.ComponentPath == componentFilepath).ToList();

                if (compMatches != null && compMatches.Count > 0)
                    retVal.AddRange(compMatches.Select(m => m.AssocParam).ToList());
            }
            this.Logger.LogInformation($@"Checking for factory link on {componentFilepath}");
            _invApp.SilentOperation = true;
            var memberDoc = (Inventor.Document)_invApp.Documents.Open(componentFilepath, false);
            var factoryName = memberDoc.GetIProperty("rulerFactoryFile");
            var extension = System.IO.Path.GetExtension(componentFilepath);
            if (!string.IsNullOrEmpty(factoryName))
            {
                this.Logger.LogInformation($@"Checking for vault file {factoryName}{extension}");
                Autodesk.Connectivity.WebServices.File factoryFile = conn.WebServiceManager.DocumentService.GetFileByFileName($"{factoryName}{extension}");
                if (factoryFile != null)
                {
                    var expectedVaultFolder = conn.WebServiceManager.DocumentService.FindFoldersByIds(new long[] { factoryFile.FolderId }).First().FullName;

                    var factoryAssoc = new FileAssocParam()
                    {
                        CldFileId = factoryFile.Id,
                        ExpectedVaultPath = $@"{expectedVaultFolder}/{factoryName}{extension}",
                        Typ = AssociationType.Dependency,
                        Source = "INVENTOR"
                    };
                    this.Logger.LogInformation($@"Adding link to {factoryName}{extension}");
                    retVal.Add(factoryAssoc);
                }
            }
            return retVal;
        }

        private HierarchicalFile GetHierarchicalFile(string filePath, Inventor.Document doc = null)
        {
            HierarchicalFile retFile;

            bool docSupplied = (doc != null);

            this.Logger.LogInformation($@"Get hierarchical file - {filePath}");

            try
            {

                _invApp.SilentOperation = true;

                if (!docSupplied)
                {
                    this.Logger.LogInformation($@"Open - {filePath}");
                    //doc = invApp.Documents.Open(filePath, false);
                    doc = OpenFileWithLOD(filePath, "Master", openVisible: false);
                }

                this.Logger.LogInformation($@"Execute 'ToHierarchicalFile'");

                retFile = doc.ToHierarchicalFile();
            }
            finally
            {
                this.Logger.LogInformation($@"Close all documents");
                _invApp.CloseAllDocuments();
            }

            return retFile;
        }

        private Inventor.Document OpenFileWithLastActiveLOD(string filePath, bool openVisible = false)
        {
            Inventor.FileManager fm = _invApp.FileManager;
            string lastActiveLOD = fm.GetLastActiveLevelOfDetailRepresentation(filePath);

            return OpenFileWithLOD(filePath, lastActiveLOD, openVisible: openVisible);
        }

        private Inventor.Document OpenFileWithLOD(string filePath, string lodName, bool openVisible = false)
        {
            Inventor.FileManager fm = _invApp.FileManager;

            string fullDocName = fm.GetFullDocumentName(filePath, lodName);

            Inventor.NameValueMap nvm = _invApp.TransientObjects.CreateNameValueMap();
            Inventor.Document doc = _invApp.Documents.OpenWithOptions(fullDocName, nvm, OpenVisible: openVisible);

            return doc;
        }

        private FileAssocParam GetFileAssocParam(VDF.Vault.Currency.Connections.Connection connection,
            string fileName, AssociationType type, string source = null, string refID = null)
        {
            string vaultFileName = string.Empty;
            FileAssocParam association = new FileAssocParam();

            //Parse file name if full file path is passed in
            if (fileName.Contains("\\"))
                vaultFileName = Path.GetFileName(fileName);
            else
                vaultFileName = fileName;

            WebServiceManager WSManager = connection.WebServiceManager;
            Autodesk.Connectivity.WebServices.File attachedVaultFile = WSManager.DocumentService.GetFileByFileName(vaultFileName);
            association.CldFileId = attachedVaultFile.Id;
            association.Typ = type;

            var expectedVaultFolder = WSManager.DocumentService.FindFoldersByIds(new long[] { attachedVaultFile.FolderId }).First().FullName;

            association.ExpectedVaultPath = expectedVaultFolder + "/" + vaultFileName;

            if (source != null)
                association.Source = source;

            if (refID != null)
                association.RefId = refID;

            return association;
        }

        private LfCycState GetLifeCycleState(Autodesk.Connectivity.WebServices.File vaultFile, LfCycDef lifeCycleDef, string lifeCycleStateTo)
        {
            //get all life cycle states in the passed in file's LifeCycleDefinition
            List<TransitionItem> lifeCycleStates = GetLifeCycleStateItems(vaultFile, lifeCycleDef);

            //grab the lifecyclestate
            this.Logger.LogInformation($@"Get {lifeCycleStateTo} transition object");
            return lifeCycleStates.Where(b => b.ToState.DispName.ToUpper() == lifeCycleStateTo.ToUpper()).FirstOrDefault().ToState;
        }

        private List<TransitionItem> GetLifeCycleStateItems(Autodesk.Connectivity.WebServices.File vaultFile, LfCycDef lifeCycleDef)
        {
            List<TransitionItem> result = new List<TransitionItem>();

            // create a map of the states so that we can look them up easily.
            Dictionary<long, LfCycState> stateMap = lifeCycleDef.StateArray.ToDictionary(n => n.Id);

            //No lifecycle has been set
            if (vaultFile.FileLfCyc.LfCycStateId == -1)
            {
                result.AddRange(lifeCycleDef.TransArray.Select(t => new TransitionItem(t, stateMap[t.ToId])).ToList());
            }
            else
            {
                foreach (LfCycTrans trans in lifeCycleDef.TransArray)
                {
                    // figure out the transitions from the current state and add them to the combo box
                    if (trans.FromId == vaultFile.FileLfCyc.LfCycStateId)
                    {
                        TransitionItem item = new TransitionItem(trans, stateMap[trans.ToId]);
                        result.Add(item);
                    }
                }
            }

            return result;
        }

        private void RemoveReadOnlyAtt(string filePath)
        {
            try
            {
                FileAttributes fileAtts = System.IO.File.GetAttributes(filePath);

                if ((fileAtts & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                {
                    this.Logger.LogInformation($@"Removing Read Only Attributes on - {filePath}");
                    fileAtts = fileAtts & ~FileAttributes.ReadOnly;
                    System.IO.File.SetAttributes(filePath, fileAtts);
                }
            }
            catch (Exception ex)
            {

            }
        }

        #endregion
    }
}
