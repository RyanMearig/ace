﻿using Autodesk.Connectivity.WebServices;
using Microsoft.Extensions.Logging;
using VDF = Autodesk.DataManagement.Client.Framework;

namespace StorageConfigurator.Vault
{
    public interface IVaultWorkflowHandler
    {
        void SetFileCategory(VDF.Vault.Currency.Entities.FileIteration iteration, 
                    VDF.Vault.Currency.Connections.Connection conn, string categoryName, ILogger logger);

        void SetLifeCycleState(string lifeCycleStateTo, Autodesk.Connectivity.WebServices.File vaultFile,
                    VDF.Vault.Currency.Connections.Connection conn, string lifeCycleDefName, ILogger logger);

        LfCycState GetLifeCycleState(Autodesk.Connectivity.WebServices.File vaultFile, LfCycDef lifeCycleDef,
                                            string lifeCycleStateTo, ILogger logger);

    }
}
