﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Cpq;
using Ruler.Rules;
using Ruler.Rules.Graphics;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class BoltOnHorizontalChannel : AdvancePart, IChannel, Ruler.Cpq.IBomPart, IBomPart
    {

        #region Private Fields

        public AdvanceCSIItemService CsiPartService { get; }

        #endregion

        #region Constructors

        public BoltOnHorizontalChannel(ModelContext modelContext, StorageConfiguratorRepository repo, AdvanceCSIItemService csiPartService) : base(modelContext)
        {
            CsiPartService = csiPartService;
            Repository = repo;
        }

        public BoltOnHorizontalChannel(string name, Part parent, StorageConfiguratorRepository repo, AdvanceCSIItemService csiPartService) : base(name, parent)
        {
            CsiPartService = csiPartService;
            Repository = repo;
        }

        #endregion

        #region Rules

        public ReadOnlyRule<float> Length { get; set; }
        protected virtual void ConfigureLength(ReadOnlyRule<float> rule)
        {
            rule.Define(default(float));
        }

        #endregion

        #region Labor Rules

        public Rule<double> PartCarriersPerHour { get; set; }
        protected virtual void ConfigurePartCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> BundleQty { get; set; }
        protected virtual void ConfigureBundleQty(Rule<double> rule)
        {
            rule.Define(() => CsiPartService.GetItemBundle(MaterialPartNumber.Value, Bom.PlantCode.Value));
        }

        #endregion

        #region IChannel Implementation

        public Rule<List<ChannelData>> AllChannelData { get; set; }
        public Rule<List<ChoiceListItem>> AllChannelSizesChoiceList { get; set; }
        public Rule<ChannelData> ActiveChannelData { get; set; }
        public Rule<string> ChannelSize { get; set; }
        public Rule<float> ChannelWidth { get; set; }
        public Rule<float> ChannelDepth { get; set; }
        public Rule<float> ChannelWebThickness { get; set; }
        public Rule<List<string>> ChannelSizesAvailable { get; set; }
        protected virtual void ConfigureChannelSizesAvailable(Rule<List<string>> rule)
        {
            rule.Define(() => new List<string>());
        }

        public Rule<float> ChannelWeightPerFt { get; set; }
        public Rule<string> MaterialPartNumber { get; set; }

        #endregion

        #region IBomPart Implementation

        public Rule<string> DatabasePartNumber { get; set; }
        public Rule<string> PartNumberPrefix { get; set; }
        public PartNumberRule<string> PartNumber { get; set; }
        public Rule<string> Description { get; set; }
        public Rule<string> BOMDescription { get; set; }
        protected virtual void ConfigureBOMDescription(Rule<string> rule)
        {
            rule.Define(() => Description.Value.Replace(" CHANNEL", $" X {Helper.fractionString(Length.Value, feetToo: false, suppressUnits: false)} Bolt on Hvy Horz"));
        }
        public Rule<string> BOMDescription1 { get; set; }
        public Rule<string> BOMDescription2 { get; set; }
        public Rule<string> BOMDescription3 { get; set; }
        public Rule<string> BOMDescription4 { get; set; }
        public Rule<string> BOMDescription5 { get; set; }
        public Rule<string> BOMDescription6 { get; set; }
        public Rule<string> BOMDescription7 { get; set; }
        public Rule<string> BOMDescription8 { get; set; }
        public Rule<string> BOMDescription9 { get; set; }
        public Rule<string> BOMDescription10 { get; set; }
        public Rule<string> BOMDescription11 { get; set; }

        public Rule<string> Color { get; set; }
        public ReadOnlyRule<AdvanceColor> AdvanceColor { get; set; }
        public ReadOnlyRule<string> ColorDescription { get; set; }
        public StorageConfiguratorRepository Repository { get; set; }
        public Rule<string> ColorBOMName { get; set; }
        public ReadOnlyRule<string> ColorShortDescription { get; set; }

        public Rule<double> PartWeight { get; set; }
        public Rule<double> PartPaintWeight { get; set; }
        protected virtual void ConfigurePartPaintWeight(Rule<double> rule)
        {
            rule.Define(0);
        }
        public Rule<float> WeightPerGallon { get; set; }
        public CsiRule<double> PaintCostPerGallon { get; set; }
        public Rule<float> PaintCost { get; set; }


        #endregion

        #region Ruler CPQ IBomPart Implementation

        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {
            child.IsPurchased.Define(false);
            child.PartNumber.Define(() =>
            {
                return PartNumber.Value;
            });
            child.Description.Define(BOMDescription);
            child.MaterialPartNumber.Define(MaterialPartNumber);
            child.MaterialQty.Define(() => Length.Value / 12);
            child.ItemMaterialCost.Define(() => child.ItemRawMaterialWeight.Value * child.MaterialUnitCost.Value);
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(Colors.Gray);
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(false);
        }

        public override bool IsCadPart => false;

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureBomPartRules();
            this.ConfigureChannelRules();
        }

        #endregion

    }
}
