﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StorageConfigurator.Rules
{
    public class Report
    {

        public string ArchiveFolder { get; set; }

        public string Filename { get; set; }

        public string TempPath { get; set; }

    }
}
