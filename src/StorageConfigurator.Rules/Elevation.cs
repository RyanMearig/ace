﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;
using Ruler.Rules;
using Ruler.Rules.Graphics;

namespace StorageConfigurator.Rules
{
    public class Elevation : Part, IRevisablePart
    {
        #region Constructors

        public Elevation(ModelContext modelContext) : base(modelContext) { }

        public Elevation(string name, Part parent) : base(name, parent) { }

        #endregion

        #region Rules

        public Rule<Elevation> ElevationMaster { get; set; }
        protected virtual void ConfigureElevationMaster(Rule<Elevation> rule)
        {
            rule.Define(default(Elevation));
        }

        public Rule<bool> MasterSupplied { get; set; }
        protected virtual void ConfigureMasterSupplied(Rule<bool> rule)
        {
            rule.Define(() => ElevationMaster.Value != default(Elevation));
        }

        [UIRule]
        public Rule<string> ElevationName { get; set; }
        protected virtual void ConfigureElevationName(Rule<string> rule)
        {
            rule.Define(() => ElevationMaster.Value?.ElevationName.Value ?? BayDrawingNumber.Value);
            rule.AddToCAD(cad =>
            {
                cad.CadRuleUsageType = CadRuleUsageType.Property;
            });
        }

        [UIRule]
        public ReadOnlyRule<string> FrontFrameName { get; set; }
        protected virtual void ConfigureFrontFrameName(ReadOnlyRule<string> rule)
        {
            rule.Define(() => FrontFrame.Value?.FrameName.Value);
        }

        public ReadOnlyRule<string> BayDrawingNumber { get; set; }
        protected virtual void ConfigureBayDrawingNumber(ReadOnlyRule<string> rule)
        {
            rule.Define(() => Bay.Value?.BayDrawingNumber.Value ?? "empty");
        }

        [UIRule]
        public ReadOnlyRule<string> BayIds { get; set; }
        protected virtual void ConfigureBayIds(ReadOnlyRule<string> rule)
        {
            rule.Define(() => BayDrawingNumber.Value + (HasRearBay.Value ? ", " + RearBay.Value.BayDrawingNumber.Value : ""));
        }

        [UIRule]
        public ReadOnlyRule<string> RearFrameName { get; set; }
        protected virtual void ConfigureRearFrameName(ReadOnlyRule<string> rule)
        {
            rule.Define(() => RearFrame.Value?.FrameName.Value);
        }

        public Rule<bool> RenderInCad { get; set; }
        protected virtual void ConfigureRenderInCad(Rule<bool> rule)
        {
            rule.Define(false);
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> RackType { get; set; }
        protected virtual void ConfigureRackType(Rule<string> rule)
        {
            rule.Define(() =>
            {
                if (FrontFrame.Value.IsSelectiveFrameLine.Value)
                {
                    var selectiveLine = (SelectiveFrameLine)FrontFrame.Value;
                    var retVal = selectiveLine.FrontFrame.Value.IsDoubleDeep.Value ? "DOUBLE DEEP RACK" : "SELECTIVE RACK";

                    if (selectiveLine.IsBackToBack.Value && selectiveLine.FrontFrame.Value.IsDoubleDeep.Value != selectiveLine.RearFrame.Value.IsDoubleDeep.Value)
                    {
                        retVal = retVal + " / " + (selectiveLine.RearFrame.Value.IsDoubleDeep.Value ? "DOUBLE DEEP RACK" : "SELECTIVE RACK");
                    }

                    return retVal + $" - TYPE {ElevationName.Value}";
                }
                else
                {
                    return "GET INFO";
                }
            });
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> IsBackToBack { get; set; }
        protected virtual void ConfigureIsBackToBack(Rule<string> rule)
        {
            rule.Define(() => FrontFrame.Value.IsBackToBack.Value.ToString());
        }

        public Rule<int> LoadsWide { get; set; }
        protected virtual void ConfigureLoadsWide(Rule<int> rule)
        {
            rule.Define(() => Bay.Value?.LoadsWide.Value ?? 0);
        }

        [CadRule(CadRuleUsageType.Property, "LoadsWide")]
        public Rule<string> LoadsWideProp { get; set; }
        protected virtual void ConfigureLoadsWideProp(Rule<string> rule)
        {
            rule.Define(() => LoadsWide.Value.ToString());
        }

        [UIRule()]
        [CadRule(CadRuleUsageType.Property)]
        public Rule<bool> IncludeInDrawing { get; set; }
        protected virtual void ConfigureIncludeInDrawing(Rule<bool> rule)
        {
            rule.Define(true);
        }

        [CadRule(CadRuleUsageType.Property)]
        public LookupRule<string> LineItemName { get; set; }
        protected virtual void ConfigureLineItemName(LookupRule<string> rule)
        {
            rule.Define("[[LineItemName]]");
        }

        public Rule<bool> HasRearBay { get; set; }
        protected virtual void ConfigureHasRearBay(Rule<bool> rule)
        {
            rule.Define(() => RearBay.Value != default(Bay));
        }

        //public PartNumberRule<string> PartNumber { get; set; }
        //protected virtual void ConfigurePartNumber(PartNumberRule<string> rule)
        //{
        //    rule.Define(() => $"[[LineItemName]].{ElevationName.Value}");
        //    rule.AddToCAD(cad =>
        //    {
        //        cad.CadRuleUsageType = CadRuleUsageType.Property;
        //        cad.NameInCad = "Part Number";
        //    });
        //}

        #endregion

        #region IRevisablePartImplementation

        public FactoryRevisionRule<string> FactoryRevision { get; set; }

        #endregion

        #region Child Rules

        public Rule<FrameLine> FrontFrame { get; set; }
        protected virtual void ConfigureFrontFrame(Rule<FrameLine> rule)
        {
            rule.Define(() => ElevationMaster.Value?.FrontFrame.Value ?? default(FrameLine));
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(RenderInCad);
                cad.CadRuleUsageType = CadRuleUsageType.ChildProxy;
                cad.ChildProxyMatrixFactory = () => Matrix4x4.CreateTranslation(Helper.Vz(-FrontFrame.Value.Width.Value/2));
            });
        }

        public Rule<Bay> Bay { get; set; }
        protected virtual void ConfigureBay(Rule<Bay> rule)
        {
            rule.Define(() => ElevationMaster.Value?.Bay.Value ?? default(Bay));
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(RenderInCad);
                cad.CadRuleUsageType = CadRuleUsageType.ChildProxy;
                cad.ChildProxyMatrixFactory = () => Matrix4x4.CreateTranslation(Helper.Vz(-FrontFrame.Value.Width.Value));
            });
        }

        public Rule<Bay> RearBay { get; set; }
        protected virtual void ConfigureRearBay(Rule<Bay> rule)
        {
            rule.Define(() => ElevationMaster.Value?.RearBay.Value ?? default(Bay));
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(() => RenderInCad.Value && HasRearBay.Value);
                cad.CadRuleUsageType = CadRuleUsageType.ChildProxy;
                cad.ChildProxyMatrixFactory = () => Matrix4x4.Transform(Matrix4x4.CreateTranslation(Helper.Vxz(RearBayXOffset.Value, (FrontFrame.Value.Width.Value + RearBay.Value.BayWidth.Value))), 
                                                                            Helper.CreateRotation(Vector3.UnitY, 180));
            });
        }

        public Rule<float> RearBayXOffset { get; set; }
        protected virtual void ConfigureRearBayXOffset(Rule<float> rule)
        {
            rule.Define(() => ElevationMaster.Value?.RearBayXOffset.Value ?? (HasRearBay.Value ? Bay.Value.OverallDepth.Value : 0));
        }

        public Rule<FrameLine> RearFrame { get; set; }
        protected virtual void ConfigureRearFrame(Rule<FrameLine> rule)
        {
            rule.Define(() => ElevationMaster.Value?.RearFrame.Value ?? default(FrameLine));
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(RenderInCad);
                cad.CadRuleUsageType = CadRuleUsageType.ChildProxy;
                cad.ChildProxyMatrixFactory = () => Matrix4x4.CreateTranslation(Helper.Vz(-(FrontFrame.Value.Width.Value + (Bay.Value?.BayWidth.Value ?? 0) + (RearFrame.Value.Width.Value/2))));
            });
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(Colors.Yellow);
        }

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define("Elevation");
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(() => (Parent != null && Parent.GetType() != typeof(Row)));
        }

        public override bool IsCadPart
        {
            get
            {
                return RenderInCad.Value;
            }
        }

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureRevisablePart();
        }

        #endregion

    }
}
