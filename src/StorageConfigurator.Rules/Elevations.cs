﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;
using Ruler.Rules.Graphics;

namespace StorageConfigurator.Rules
{
    public class Elevations : Part, IRevisablePart
    {
        #region Constructors

        public Elevations(ModelContext modelContext) : base(modelContext) { }

        public Elevations(string name, Part parent) : base(name, parent) { }

        #endregion

        #region Rules

        [UIRule]
        public ReadOnlyRule<int> ElevationQty { get; set; }
        protected virtual void ConfigureElevationQty(ReadOnlyRule<int> rule)
        {
            rule.Define(() => UniqueElevationsList.Value.Count);
        }

        public Rule<List<Elevation>> UniqueElevationsList { get; set; }
        protected virtual void ConfigureUniqueElevationsList(Rule<List<Elevation>> rule)
        {
            rule.Define(() =>
            {
                return new List<Elevation>();
            });
        }

        [CadRule(CadRuleUsageType.Property)]
        public LookupRule<string> LineItemName { get; set; }
        protected virtual void ConfigureLineItemName(LookupRule<string> rule)
        {
            rule.Define("[[LineItemName]]");
        }

        public PartNumberRule<string> PartNumber { get; set; }
        protected virtual void ConfigurePartNumber(PartNumberRule<string> rule)
        {
            rule.Define("[[LineItemName]]_Elevations");
            rule.AddToCAD(cad =>
            {
                cad.CadRuleUsageType = CadRuleUsageType.Property;
                cad.NameInCad = "Part Number";
            });
        }

        #endregion

        #region IRevisablePartImplementation

        public FactoryRevisionRule<string> FactoryRevision { get; set; }

        #endregion

        #region Children

        public ChildList<Elevation> Elevation { get; set; }
        protected virtual void ConfigureElevation(ChildList<Elevation> list)
        {
            list.Qty.Define(ElevationQty);
            list.ConfigureChildren((child, index) =>
            {
                child.RenderInCad.Define(true);
                child.ElevationMaster.Define(() => UniqueElevationsList.Value[index]);
            });
        }


        #endregion

        #region Part Overrides

        public override bool IsCadPart => true;

        //protected override void ConfigureUIMaterial(UIMaterial material)
        //{
        //    material.Color.Define(CubeColor);
        //    material.Opacity.Define(0.5f);
        //}

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define(() => ElevationQty.Value > 0 ? "Elevations" : "");
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(() => ElevationQty.Value > 0);
        }

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureRevisablePart();
        }

        #endregion

    }
}
