﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;
using Ruler.Rules.Graphics;
using Ruler.Rules.Validation;

namespace StorageConfigurator.Rules
{
    public class BoundingCube : Part
    {
        #region Constructors

        public BoundingCube(ModelContext modelContext) : base(modelContext) { }

        public BoundingCube(string name, Part parent) : base(name, parent) { }

        #endregion

        #region Rules

        [UIRule]
        public Rule<float> OverallWidth { get; set; }
        protected virtual void ConfigureOverallWidth(Rule<float> rule)
        {
            rule.Define(1);
        }

        [UIRule]
        public Rule<float> OverallHeight { get; set; }
        protected virtual void ConfigureOverallHeight(Rule<float> rule)
        {
            rule.Define(1);
        }

        [UIRule]
        public Rule<float> OverallDepth { get; set; }
        protected virtual void ConfigureOverallDepth(Rule<float> rule)
        {
            rule.Define(1);
        }

        public Rule<bool> GraphicsActive { get; set; }
        protected virtual void ConfigureGraphicsActive(Rule<bool> rule)
        {
            rule.Define(true);
        }

        public Rule<int> CubeColor { get; set; }
        protected virtual void ConfigureCubeColor(Rule<int> rule)
        {
            rule.Define(Colors.Blue);
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(CubeColor);
            material.Opacity.Define(0.5f);
        }

        //protected override void ConfigureFactoryName(Rule<string> rule)
        //{
        //    rule.Define("BoundingCube");
        //}

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(false);
        }

        public override bool IsCadPart => false;

        #endregion

        #region Graphics

        public Block BoundingCubeBlock { get; set; }
        protected virtual void ConfigureBoundingCubeBlock(Block geo)
        {
            geo.IsActive.Define(GraphicsActive);
            geo.Width.Define(OverallDepth);
            geo.Height.Define(OverallHeight);
            geo.Depth.Define(OverallWidth);
            geo.Position.Define(() => Helper.Vxyz(-geo.Width.Value / 2, geo.Height.Value / 2, -geo.Depth.Value / 2 ));
        }

        //public UCS UCS { get; set; }
        //protected virtual void ConfigureUCS(UCS child)
        //{
        //    child.Diameter.Define(0.125f);
        //    child.Position.Define(() => (Helper.Vy(0)));
        //}

        #endregion


    }
}
