﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ruler.Rules;

namespace StorageConfigurator.Rules
{
    public interface IChannel : IChannelData
    {
        Rule<ChannelData> ActiveChannelData { get; set; }
        Rule<List<string>> ChannelSizesAvailable { get; set; }
        Rule<string> ChannelSize { get; set; }
        Rule<float> ChannelWidth { get; set; }
        Rule<float> ChannelDepth { get; set; }
        Rule<float> ChannelWebThickness { get; set; }
        Rule<float> ChannelWeightPerFt { get; set; }
        Rule<string> MaterialPartNumber { get; set; }
    }

    public static class IChannelExtensions
    {
        public static void ConfigureChannelRules(this IChannel comp)
        {
            comp.ConfigureChannelDataRules();

            comp.ChannelSize.Define(DefaultChannelSize);
            comp.ChannelSize.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() => Helper.GetFilteredChoices(comp.AllChannelSizesChoiceList.Value, comp.ChannelSizesAvailable.Value));
                //{
                //    List<ChoiceListItem> choices = new List<ChoiceListItem>();

                //    if (comp.ChannelSizesAvailable != null && comp.ChannelSizesAvailable.Value != null && comp.ChannelSizesAvailable.Value.Count > 0)
                //    {
                //        comp.AllChannelSizesChoiceList.Value.ForEach(c =>
                //            {
                //                if (comp.ChannelSizesAvailable.Value.Any(s => s == c.Text))
                //                    choices.Add(c);
                //            });
                //    }
                //    else
                //        choices = comp.AllChannelSizesChoiceList.Value;

                //    return choices;
                //});
            });

            comp.ActiveChannelData.Define(() => (comp.AllChannelData.Value.FirstOrDefault(d => d.ChannelSize == comp.ChannelSize.Value)));

            comp.ChannelWidth.Define(() => comp.ActiveChannelData.Value.Width);
            comp.ChannelDepth.Define(() => comp.ActiveChannelData.Value.Depth);
            comp.ChannelWebThickness.Define(() => comp.ActiveChannelData.Value.WebThickness);
            comp.ChannelWeightPerFt.Define(() => comp.ActiveChannelData.Value.WeightPerFt);
        }

        public static string GetChannelId(this IChannel comp, string channelSize = "")
        {
            var ids = new Dictionary<string, string>()
            {
                { "C3x3.5", "A"},
                { "C3x4.1", "B"},
                { "C4x4.5", "C"},
                { "C4x5.4", "D"},
                { "C5x6.1", "EL"},
                { "C5x6.7", "E"},
                { "C6x8.2", "F"},
                { "C7x9.8", "G"},
                { "C8x11.5", "H"}
            };

            //Add two options to get the ID. If the channel size is supplied, use it. For cases that are not channels.
            var sizeToUse = string.IsNullOrWhiteSpace(channelSize) ? comp.ChannelSize.Value : channelSize;

            return ids.TryGetValue(sizeToUse, out var id) ? id : string.Empty;
        }
        
        const string DefaultChannelSize = "C3x3.5";
    }
}
