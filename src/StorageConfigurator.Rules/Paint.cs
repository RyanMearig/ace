﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ruler.Cpq;
using Ruler.Rules;
using Ruler.Rules.Graphics;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class Paint : Part, Ruler.Cpq.IBomPart, IBomPart
    {
        #region Constructors

        public Paint(ModelContext modelContext, StorageConfiguratorRepository repo) : base(modelContext) 
        {
            Repository = repo;
        }

        public Paint(string name, Part parent, StorageConfiguratorRepository repo) : base(name, parent) 
        {
            Repository = repo;
        }

        #endregion

        #region Rules


        #endregion

        #region IBomPart Implementation

        public Rule<string> DatabasePartNumber { get; set; }
        public Rule<string> PartNumberPrefix { get; set; }
        public PartNumberRule<string> PartNumber { get; set; }
        protected virtual void ConfigurePartNumber(PartNumberRule<string> rule)
        {
            rule.Define(Color);
            rule.AddToUI();
        }

        public Rule<string> Description { get; set; }
        protected virtual void ConfigureDescription(Rule<string> rule)
        {
            rule.Define(() => !string.IsNullOrWhiteSpace(PartNumber.Value) ? $"{PartNumber.Value}-{TypeName.Length}" : DisplayName.Value);
            rule.AddToUI();
        }

        public Rule<string> BOMDescription { get; set; }
        protected virtual void ConfigureBOMDescription(Rule<string> rule)
        {
            rule.Define(Description);
        }
        public Rule<string> BOMDescription1 { get; set; }
        public Rule<string> BOMDescription2 { get; set; }
        public Rule<string> BOMDescription3 { get; set; }
        public Rule<string> BOMDescription4 { get; set; }
        public Rule<string> BOMDescription5 { get; set; }
        public Rule<string> BOMDescription6 { get; set; }
        public Rule<string> BOMDescription7 { get; set; }
        public Rule<string> BOMDescription8 { get; set; }
        public Rule<string> BOMDescription9 { get; set; }
        public Rule<string> BOMDescription10 { get; set; }
        public Rule<string> BOMDescription11 { get; set; }

        public Rule<string> Color { get; set; }
        public ReadOnlyRule<AdvanceColor> AdvanceColor { get; set; }
        public ReadOnlyRule<string> ColorDescription { get; set; }
        public StorageConfiguratorRepository Repository { get; set; }
        public Rule<string> ColorBOMName { get; set; }
        public ReadOnlyRule<string> ColorShortDescription { get; set; }

        public Rule<double> PartWeight { get; set; }
        public Rule<double> PartPaintWeight { get; set; }
        protected virtual void ConfigurePartPaintWeight(Rule<double> rule)
        {
            rule.Define(() =>
            {
                //return 0;
                var matWeight = GetParentRawPartWeightWithoutPaint();
                var bomPlant = Bom.PlantCode.Value ?? "GA_C";

                if (matWeight <= 0 || WeightPerGallon.Value <= 0) { return 0; }
                var paintWeightPerGallon = PaintCostPerGallon.CsiPartService.GetItemWeight(Color.Value, bomPlant);
                var gallPaintReq = matWeight / WeightPerGallon.Value;

                return paintWeightPerGallon * gallPaintReq;
            });
        }
        public Rule<float> WeightPerGallon { get; set; }
        public CsiRule<double> PaintCostPerGallon { get; set; }
        public Rule<float> PaintCost { get; set; }


        #endregion

        #region Ruler CPQ IBomPart Implementation

        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {
            child.Description.Define(ColorDescription);
            child.PartNumber.Define(Color);
            child.IsPurchased.Define(true);
            child.ItemPurchasedCost.Define(() => PaintCost.Value);
            child.ItemPurchasedWeight.Define(PartPaintWeight);
        }

        #endregion

        #region Public Methods

        public double GetParentRawPartWeightWithoutPaint()
        {
            if (Parent != null && Parent is Ruler.Cpq.IBomPart parentBomPart)
            {
                var nonPaintChildren = parentBomPart.Bom?.BomChildren.Value?.Where(c => c.Parent.GetType() != typeof(Paint))?.ToList() ?? new List<BomData>();
                var nonPaintChildrenRawWeight = nonPaintChildren.Any() ? nonPaintChildren.Sum(c => c.TotalRawWeight.Value) : 0;

                return (parentBomPart.Bom.IsPurchased.Value ? parentBomPart.Bom.ItemPurchasedWeight.Value : parentBomPart.Bom.ItemRawMaterialWeight.Value)
                                + nonPaintChildrenRawWeight;
            }

            return 0.0;
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(Colors.Gray);
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(false);
        }

        public override bool IsCadPart => false;

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureBomPartRules();
        }

        #endregion

    }
}
