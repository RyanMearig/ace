﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ruler.Rules;

namespace StorageConfigurator.Rules
{
    public interface IAnchorData
    {
        Rule<List<AnchorData>> AllAnchorData { get; set; }
        Rule<List<ChoiceListItem>> AllAnchorTypesChoiceList { get; set; }
    }

    public static class IAnchorDataExtensions
    {
        public static void ConfigureAnchorDataRules(this IAnchorData comp)
        {
            comp.AllAnchorData.Define(() =>
            {
                List<AnchorData> anchorData = new List<AnchorData>();

                anchorData.Add(new AnchorData() { Type = @"HW.ANC.SWA.A654", TypeDisplay = @"1/2""x 5-1/2"" Seismic W Anchor", BoltDiameter = 0.5f, Length = 5.5f});
                anchorData.Add(new AnchorData() { Type = @"HW.ANC.SWA.A2A6", TypeDisplay = @"5/8"" x 6"" Seismic W Anchor", BoltDiameter = 0.625f, Length = 6 });
                anchorData.Add(new AnchorData() { Type = @"HW.ANC.STB2.A554", TypeDisplay = @"3/4""x 5-1/2"" Str.Bolt 2 Seismic Anchor", BoltDiameter = 0.75f, Length = 5.5f });
                anchorData.Add(new AnchorData() { Type = @"HW.ANC.A245", TypeDisplay = @"5/8"" x 4-3/4"" Wedge Anchor", BoltDiameter = 0.625f, Length = 4.75f });
                anchorData.Add(new AnchorData() { Type = @"HW.ANC.A26A", TypeDisplay = @"5/8"" x 6"" Wedge Anchor", BoltDiameter = 0.625f, Length = 6 });
                anchorData.Add(new AnchorData() { Type = @"HW.ANC.A545", TypeDisplay = @"3/4"" x 4-3/4"" Wedge Anchor", BoltDiameter = 0.75f, Length = 4.75f });
                anchorData.Add(new AnchorData() { Type = @"HW.ANC.A554", TypeDisplay = @"3/4"" x 5-1/2"" Wedge Anchor", BoltDiameter = 0.75f, Length = 5.5f });
                anchorData.Add(new AnchorData() { Type = @"HW.ANC.A646", TypeDisplay = @"1/2""x 4-1/2"" Wedge Anchor", BoltDiameter = 0.5f, Length = 4.5f });
                anchorData.Add(new AnchorData() { Type = @"HW.ANC.A65A", TypeDisplay = @"1/2""x 5"" LDT Screw Anchor", BoltDiameter = 0.5f, Length = 5 });
                anchorData.Add(new AnchorData() { Type = @"HW.ANC.EXY.A510", TypeDisplay = @"3/4"" x 10"" Epoxy Anchor", BoltDiameter = 0.75f, Length = 10 });
                anchorData.Add(new AnchorData() { Type = @"HW.ANC.STB.A554", TypeDisplay = @"3/4""x 5-1/2"" Str.Bolt NonSeismic Anchor", BoltDiameter = 0.75f, Length = 5.5f });
                anchorData.Add(new AnchorData() { Type = @"HW.ANC.STB2.A245", TypeDisplay = @"5/8""x 5"" Str.Bolt 2 Seismic Anchor", BoltDiameter = 0.625f, Length = 5 });
                anchorData.Add(new AnchorData() { Type = @"HW.ANC.STB2.A246", TypeDisplay = @"5/8""x 6"" Str.Bolt 2 Seismic Anchor", BoltDiameter = 0.625f, Length = 6 });
                anchorData.Add(new AnchorData() { Type = @"HW.ANC.STB2.A644", TypeDisplay = @"1/2""x 4-3/4"" StrBolt 2 Seismic Anchor", BoltDiameter = 0.5f, Length = 4.75f });
                anchorData.Add(new AnchorData() { Type = @"HW.ANC.STB2.A654", TypeDisplay = @"1/2""x 5-1/2"" Str.Blt2 Seismic Anchor", BoltDiameter = 0.5f, Length = 5.5f });
                anchorData.Add(new AnchorData() { Type = @"HW.ANC.SWA.A245", TypeDisplay = @"5/8"" x 4-3/4"" Seismic W Anchor", BoltDiameter = 0.625f, Length = 4.75f });
                anchorData.Add(new AnchorData() { Type = @"HW.ANC.TZ.A135", TypeDisplay = @"3/8"" x 3-3/4"" TZ Expan Anchor", BoltDiameter = 0.375f, Length = 3.75f });
                anchorData.Add(new AnchorData() { Type = @"HW.ANC.TZ.A245", TypeDisplay = @"5/8"" x 4-3/4"" TZ Expan Anchor", BoltDiameter = 0.625f, Length = 4.75f });
                anchorData.Add(new AnchorData() { Type = @"HW.ANC.TZ.A286", TypeDisplay = @"5/8"" x 8-1/2""  TZ Expan Anchor", BoltDiameter = 0.625f, Length = 8.5f });
                anchorData.Add(new AnchorData() { Type = @"HW.ANC.TZ.A246", TypeDisplay = @"5/8"" x 6"" TZ Expan Anchor", BoltDiameter = 0.625f, Length = 6 });
                anchorData.Add(new AnchorData() { Type = @"HW.ANC.TZ.A554", TypeDisplay = @"3/4"" x 5-1/2"" TZ Expan Anchor", BoltDiameter = 0.75f, Length = 5.5f });
                anchorData.Add(new AnchorData() { Type = @"HW.ANC.TZ.A5A8", TypeDisplay = @"3/4"" x 8"" TZ Expan Anchor", BoltDiameter = 0.75f, Length = 8 });
                anchorData.Add(new AnchorData() { Type = @"HW.ANC.TZ.A635", TypeDisplay = @"1/2"" x 3-3/4"" TZ Expan Anchor", BoltDiameter = 0.5f, Length = 3.75f });
                anchorData.Add(new AnchorData() { Type = @"HW.ANC.TZ.A646", TypeDisplay = @"1/2"" x 4-1/2"" TZ Expan Anchor", BoltDiameter = 0.5f, Length = 4.5f });
                anchorData.Add(new AnchorData() { Type = @"HW.ANC.TZ.A654", TypeDisplay = @"1/2""x 5-1/2"" TZ Expan Anchor", BoltDiameter = 0.5f, Length = 5.5f });
                anchorData.Add(new AnchorData() { Type = @"HW.ANC.TZ.A6A7", TypeDisplay = @"1/2"" x 7"" TZ Expan Anchor", BoltDiameter = 0.5f, Length = 7 });
                anchorData.Add(new AnchorData() { Type = @"HW.ANC.VTZ.A556", TypeDisplay = @"3/4"" x 5-1/2"" KB-VTZ Anchor", BoltDiameter = 0.75f, Length = 5.5f });
                
                return anchorData;
            });

            comp.AllAnchorTypesChoiceList.Define(() =>
            {
                List<ChoiceListItem> choices = new List<ChoiceListItem>();

                if (comp.AllAnchorData != null && comp.AllAnchorData.Value != null && comp.AllAnchorData.Value.Any())
                    comp.AllAnchorData.Value.ForEach(d => choices.Add(new ChoiceListItem(d.Type, d.TypeDisplay)));

                return choices;
            });
        }

        public static List<ChoiceListItem> GetAnchorChoiceList(this IAnchorData comp, float boltDiameter)
        {
            List<ChoiceListItem> choices = new List<ChoiceListItem>();

            if (comp.AllAnchorData != null && comp.AllAnchorData.Value != null && comp.AllAnchorData.Value.Any())
            {
                choices = comp.AllAnchorData.Value.Where(a => a.BoltDiameter == boltDiameter).Select(d => new ChoiceListItem(d.Type, d.TypeDisplay)).ToList();
            }

            return choices;
        }

    }
}
