﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;
using Ruler.Rules.Graphics;

namespace StorageConfigurator.Rules
{
    public class ChannelGfx : Part
    {
        #region Constructors

        public ChannelGfx(ModelContext modelContext) : base(modelContext) { }

        public ChannelGfx(string name, Part parent) : base(name, parent) { }

        #endregion

        #region Rules

        public Rule<float> WebThickness { get; set; }
        protected virtual void ConfigureWebThickness(Rule<float> rule)
        {
            rule.Define(0.125f);
        }

        public Rule<float> ChannelWidth { get; set; }
        protected virtual void ConfigureChannelWidth(Rule<float> rule)
        {
            rule.Define(default(float));
        }

        public Rule<float> ChannelDepth { get; set; }
        protected virtual void ConfigureChannelDepth(Rule<float> rule)
        {
            rule.Define(default(float));
        }

        public Rule<float> Length { get; set; }
        protected virtual void ConfigureLength(Rule<float> rule)
        {
            rule.Define(default(float));
        }

        #endregion

        #region Graphics

        public Block ChannelWebBlock { get; set; }
        protected virtual void ConfigureChannelWebBlock(Block geo)
        {
            geo.Width.Define(WebThickness);
            geo.Height.Define(ChannelWidth);
            geo.Depth.Define(Length);
            geo.Position.Define(() => Helper.Vxz(WebThickness.Value / 2, Length.Value / 2));
        }

        public Block ChannelTopFlangeBlock { get; set; }
        protected virtual void ConfigureChannelTopFlangeBlock(Block geo)
        {
            geo.Width.Define(ChannelDepth);
            geo.Height.Define(WebThickness);
            geo.Depth.Define(Length);
            geo.Position.Define(() => new System.Numerics.Vector3(geo.Width.Value / 2, (ChannelWidth.Value / 2) - (geo.Height.Value / 2), Length.Value / 2));
        }

        public Block ChannelBtmFlangeBlock { get; set; }
        protected virtual void ConfigureChannelBtmFlangeBlock(Block geo)
        {
            geo.Width.Define(ChannelDepth);
            geo.Height.Define(WebThickness);
            geo.Depth.Define(Length);
            geo.Position.Define(() => new System.Numerics.Vector3(geo.Width.Value / 2, (-ChannelWidth.Value / 2) + (geo.Height.Value / 2), Length.Value / 2));
        }

        //public UCS UCS { get; set; }
        //protected virtual void ConfigureUCS(UCS child)
        //{
        //    child.Position.Define(() => (Helper.Vy(0)));
        //}

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(Colors.Red);
            material.Opacity.Define(0.9f);
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(false);
        }

        #endregion

    }
}
