﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ruler.Rules;

namespace StorageConfigurator.Rules
{
    public interface IAngle : IAngleData
    {
        Rule<AngleData> ActiveAngleData { get; set; }
        Rule<List<string>> AngleSizesAvailable { get; set; }
        Rule<string> AngleType { get; set; }
        Rule<float> AngleWidth { get; set; }
        Rule<float> AngleHeight { get; set; }
        Rule<float> AngleThickness { get; set; }
        Rule<string> MaterialPartNumber { get; set; }
    }

    public static class IAngleExtensions
    {
        public static void ConfigureAngleRules(this IAngle comp)
        {
            comp.ConfigureAngleDataRules();

            comp.AngleType.Define(DefaultAngleType);
            comp.AngleType.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() => Helper.GetFilteredChoices(comp.AllAngleTypesChoiceList.Value, comp.AngleSizesAvailable.Value));
            });

            comp.ActiveAngleData.Define(() => (comp.AllAngleData.Value.FirstOrDefault(d => d.Type == comp.AngleType.Value)));

            comp.AngleWidth.Define(() => comp.ActiveAngleData.Value?.Width ?? 0);
            comp.AngleHeight.Define(() => comp.ActiveAngleData.Value?.Height ?? 0);
            comp.AngleThickness.Define(() => comp.ActiveAngleData.Value?.Thickness ?? 0);
        }

        const string DefaultAngleType = "1-1/2 x 1-1/2 x 1/8";
    }
}
