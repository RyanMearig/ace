﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;
using Ruler.Rules.Validation;

namespace StorageConfigurator.Rules
{
    public interface IDepthMaster
    {
        Rule<bool> IsDoubleDeep { get; set; }
        Rule<string> DoubleDeepType { get; set; }
        Rule<bool> IsDoubleDeepPost { get; set; }
        Rule<float> FrontFrameDepth { get; set; }
        Rule<float> RearFrameDepth { get; set; }
        Rule<bool> HasFrameTies { get; set; }
        Rule<string> FrameTieType { get; set; }
        Rule<float> FrameTieLengthMin { get; set; }
        Rule<float> FrameTieLength { get; set; }
        ReadOnlyRule<float> OverallDepth { get; set; }
    }

    public static class IDepthMasterExtensions
    {
        public static void ConfigureDepthMasterRules(this IDepthMaster comp)
        {
            comp.IsDoubleDeep.Define(false);
            comp.IsDoubleDeep.AddToUI();

            comp.DoubleDeepType.Define("frameFrame");
            comp.DoubleDeepType.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !comp.IsDoubleDeep.Value);
                ui.ChoiceList.Define(() =>
                {
                    return new List<ChoiceListItem>()
                    {
                        new ChoiceListItem("frameFrame", "FrameFrame"),
                        new ChoiceListItem("post", "Post")
                    };
                });
            });

            comp.IsDoubleDeepPost.Define(() => (comp.DoubleDeepType.Value == "post"));
            comp.IsDoubleDeepPost.AddToUI(ui =>
            {
                ui.ReadOnly.Define(true);
            });

            comp.FrontFrameDepth.Define(() => (comp.IsDoubleDeep.Value && comp.IsDoubleDeepPost.Value ? 49 : 48));
            comp.FrontFrameDepth.AddToUI(ui =>
            {
                ui.Label.Define(() => (comp.IsDoubleDeep.Value ? "Front Frame Depth" : "Frame Depth"));
            });

            comp.RearFrameDepth.Define(() => (comp.IsDoubleDeep.Value && comp.IsDoubleDeepPost.Value ? (comp.FrontFrameDepth.Value - 2) : comp.FrontFrameDepth.Value));
            comp.RearFrameDepth.AddToUI(ui =>
            {
                ui.Label.Define(() => (comp.IsDoubleDeepPost.Value ? "Rear Space Depth" : "Rear Frame Depth"));
            });

            comp.HasFrameTies.Define(() => comp.IsDoubleDeep.Value);
            comp.HasFrameTies.AddToUI();

            comp.FrameTieLengthMin.Define(() => (comp.IsDoubleDeepPost.Value ? 0 : 6));

            //Will be overridden by Double Deep Post Type
            comp.FrameTieLength.Define(12);
            comp.FrameTieLength.Min(comp.FrameTieLengthMin);
            comp.FrameTieLength.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !comp.HasFrameTies.Value || comp.IsDoubleDeepPost.Value);
            });

            comp.FrameTieType.Define("rs");
            comp.FrameTieType.AddToUI((ui) =>
            {
                ui.ChoiceList.Define(() => (new List<ChoiceListItem>()
                {
                    new ChoiceListItem("rs", "No Weld"),
                    new ChoiceListItem("rfsw", "Welded"),
                    new ChoiceListItem("rfswhd", "Heavy Welded"),
                    new ChoiceListItem("rrx", "Rub Rail(RRx)"),
                    new ChoiceListItem("rrxI", "Rub Rail(RRxI)"),
                    new ChoiceListItem("rrxID", "Rub Rail(RRxID)"),
                    new ChoiceListItem("rrxd", "Doubled Rub Rail")
                }));
            });

            comp.OverallDepth.Define(() =>
            {
                if (comp.IsDoubleDeep.Value)
                {
                    if (comp.IsDoubleDeepPost.Value)
                        return comp.FrontFrameDepth.Value + comp.RearFrameDepth.Value;
                    else
                        return comp.FrontFrameDepth.Value + comp.RearFrameDepth.Value + comp.FrameTieLength.Value;
                }
                else
                    return comp.FrontFrameDepth.Value;
            });
            comp.OverallDepth.AddToUI();

        }

    }
}
