﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Ruler.Cpq;
using Ruler.Rules;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class ASLineItemPart : AdvancePart
    {
        #region Constructors

        public ASLineItemPart(ModelContext modelContext) : base(modelContext) { }

        public ASLineItemPart(string name, Part parent) : base(name, parent) { }

        #endregion

        #region Rules

        public StorageConfiguratorRepository Repository { get; set; }

        [UIRule]
        public Rule<string> ProjectNumber { get; set; }
        protected virtual void ConfigureProjectNumber(Rule<string> rule)
        {
            rule.Define(default(string));
        }

        [UIRule]
        public Rule<string> LineItemName { get; set; }
        protected virtual void ConfigureLineItemName(Rule<string> rule)
        {
            rule.Define(default(string));
        }

        //[UIRule]
        public Rule<IEnumerable<BomData>> BOMChildren { get; set; }
        protected virtual void ConfigureBOMChildren(Rule<IEnumerable<BomData>> rule)
        {
            rule.Define(() => new List<BomData>());
        }

        public Rule<List<BomData>> DistinctBomChildren { get; set; }
        protected virtual void ConfigureDistinctBomChildren(Rule<List<BomData>> rule)
        {
            rule.Define(() =>
            {
                return BOMChildren.Value?.GroupBy(c => (desc: c.PartNumber.Value, cost: Math.Round(c.TotalCost.Value, 2), margin: c.Margin.Value))
                    .Select(g => g.First()).ToList() ?? new List<BomData>();
            });
        }

        
        public Rule<List<BomData>> OrderedDistinctBomChildren { get; set; }
        protected virtual void ConfigureOrderedDistinctBomChildren(Rule<List<BomData>> rule)
        {
            rule.Define(() =>
            {
                return DistinctBomChildren.Value?.OrderBy(p => (string.IsNullOrWhiteSpace(p.Item.Value)) ? "0" : p.Item.Value.PadLeft(3, '0'))
                                                 .ToList() ?? new List<BomData>();
            });
        }

        [UIRule(valueOnly: true)]
        public Rule<List<string>> OrderedDistinctBomChildrenByPath { get; set; }
        protected virtual void ConfigureOrderedDistinctBomChildrenByPath(Rule<List<string>> rule)
        {
            rule.Define(() => OrderedDistinctBomChildren.Value?.Select(c => c.Parent.Path)?.ToList() ?? new List<string>());
        }

        public Rule<List<BomData>> DistinctBomChildrenByPath { get; set; }
        protected virtual void ConfigureDistinctBomChildrenByPath(Rule<List<BomData>> rule)
        {
            rule.Define(() =>
            {
                return BOMChildren.Value?.GroupBy(b => new { b.Path, b.PartNumber })
                                                             .Select(g => g.First())
                                                             .ToList() ??
                                                             new List<BomData>();
            });
        }

        [UIRule]
        public Rule<double> SteelDiscount { get; set; }
        protected virtual void ConfigureSteelDiscount(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        [UIRule]
        public Rule<double> SteelOverhead { get; set; }
        protected virtual void ConfigureSteelOverhead(Rule<double> rule)
        {
            rule.Define(() => Repository?.GetDefaultOverhead("SteelOverhead") ?? 0);
        }

        public Rule<double> ScrapPercentage { get; set; }
        protected virtual void ConfigureScrapPercentage(Rule<double> rule)
        {
            rule.Define(5);
        }

        public ReadOnlyRule<List<Tuple<string, int>>> ItemNumbers { get; set; }
        protected virtual void ConfigureItemNumbers(ReadOnlyRule<List<Tuple<string, int>>> rule)
        {
            rule.Define(() =>
            {
                var results = GetBomItemNumbersByFactory(new List<string> { "FrameUpright", "UprightColumnAssembly", "RSFrameTie",
                    "WeldedFrameTie", "WeldedHDFrameTie", "WeldedRubRail", "DoubledRubRail" }, 1);
                results.AddRange(GetBomItemNumbersByFactory(new List<string> { "LoadBeam", "WeldedCrossbar", "XBCrossbar" }, 100));
                results.AddRange(GetBomItemNumbersByFactory(new List<string> { "EndAisleGuard", "SelectivePalletStop", "DoubleDeepPalletStop" }, 740));
                results.AddRange(GetBomItemNumbersByFactory(new List<string> { "AnchorBolts" }, 900));
                results.AddRange(GetBomItemNumbersByFactory(new List<string> { "HardwareKit", "Washers", "BullnoseCapPlug" }, 903));

                return results ?? new List<Tuple<string, int>>();
            });
        }

        public ReadOnlyRule<List<Tuple<string, string>>> ActualItemNumbers { get; set; }
        protected virtual void ConfigureActualItemNumbers(ReadOnlyRule<List<Tuple<string, string>>> rule)
        {
            rule.Define(() =>
            {
                return DistinctBomChildren.Value?
                            .OrderBy(n => n.Item.Value)?
                            .Select((d, index) => Tuple.Create(d.PartNumber.Value, d.Item.Value))?
                            .ToList() ?? null;
            });
        }

        public Rule<string> ItemNumbersAtt { get; set; }
        protected virtual void ConfigureItemNumbersAtt(Rule<string> rule)
        {
            rule.Define(() => JsonConvert.SerializeObject(ActualItemNumbers.Value));
            rule.AddToCAD(cad =>
            {
                //cad.IsActive.Define();
                cad.CadRuleUsageType = CadRuleUsageType.Attribute;
                cad.NameInCad = "ItemNumbers";
            });
        }

        #endregion

        #region Public Methods

        public void ResetBOMChildrenMargins(string customerType, double totalCost)
        {
            if (DistinctBomChildrenByPath.Value != null && DistinctBomChildrenByPath.Value.Any())
            {
                //Possibly lookup all margins that match cost and customer type. One query.
                var minMarg = Repository?.GetMinMargin("PalletRack", customerType, totalCost);

                DistinctBomChildrenByPath.Value.ForEach(c =>
                {
                    if (minMarg != null && minMarg > 0)
                    {
                        c.Margin.SetAdhocDefaultValue((double)(minMarg + 10));
                        c.Margin.Reset();
                        c.MarginMin.SetAdhocDefaultValue((double)minMarg);
                        //c.Margin.Min(c.MarginMin, new Ruler.Rules.Validation.ValidatorOptions(false));
                    }
                });
            }
        }

        public List<Tuple<string, int>> GetBomItemNumbersByFactory(List<string> factoryNames, int startIndex)
        {
            var factoryChildren = DistinctBomChildren.Value?
                .Where(c => factoryNames.Contains(((Part)c.Parent).FactoryName.Value))?
                .Distinct()
                .OrderBy(n => factoryNames.IndexOf(((Part)n.Parent).FactoryName.Value));
            return factoryChildren?.Select((d, index) => Tuple.Create(d.PartNumber.Value, startIndex + index))?.ToList() ?? null;
        }

        #endregion
    }
}
