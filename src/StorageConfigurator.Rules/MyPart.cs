﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;
using Ruler.Rules.Graphics;

namespace StorageConfigurator.Rules
{
    public class MyPart: Part
    {
        #region Constructors

        public MyPart(ModelContext modelContext): base(modelContext) { }

        public MyPart(string name, Part parent) : base(name, parent) { }

        #endregion

        #region Rules

        [UIRule]
        public Rule<bool> TestBool { get; set; }
        protected virtual void ConfigureTestBool(Rule<bool> rule)
        {
            rule.Define(default(bool));
        }

        public Rule<int> Color { get; set; }
        protected virtual void ConfigureColor(Rule<int> rule)
        {
            rule.Define(Colors.Blue);
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(new ChoiceListItem[]
                {
                    new ChoiceListItem(Colors.Blue, "Blue"),
                    new ChoiceListItem(Colors.Red, "Red"),
                    new ChoiceListItem(Colors.Green, "Green"),
                });
            });
        }

        [UIRule]
        public Rule<float> Width { get; set; }
        protected virtual void ConfigureWidth(Rule<float> rule)
        {
            rule.Define(5);
            rule.AddToCAD();
        }

        [UIRule]
        public Rule<float> Height { get; set; }
        protected virtual void ConfigureHeight(Rule<float> rule)
        {
            rule.Define(5);
            rule.AddToCAD();
        }

        [UIRule]
        public Rule<float> Depth { get; set; }
        protected virtual void ConfigureDepth(Rule<float> rule)
        {
            rule.Define(5);
            rule.AddToCAD();
        }

        [UIRule]
        public ReadOnlyRule<string> CurrentUserName { get; set; }
        protected virtual void ConfigureCurrentUserName(ReadOnlyRule<string> rule)
        {
            rule.Define(this.ModelContext.User.Identity?.Name);
        }

        [UIRule]
        public ReadOnlyRule<string> CurrentEmailAddress { get; set; }
        protected virtual void ConfigureCurrentEmailAddress(ReadOnlyRule<string> rule)
        {
            rule.Define(this.ModelContext.User.FindFirst("email")?.Value);
        }

        [UIRule]
        public ReadOnlyRule<bool> IsAdmin { get; set; }
        protected virtual void ConfigureIsAdmin(ReadOnlyRule<bool> rule)
        {
            rule.Define(this.ModelContext.User.IsInRole("Developers"));
        }

        #endregion

        #region Children

        public MyChildPart MyChild { get; set; }
        protected virtual void ConfigureMyChild(MyChildPart child)
        {

        }

        #endregion

        #region Graphics

        public Block Block { get; set; }
        protected virtual void ConfigureBlock(Block geo)
        {
            geo.Width.Define(Width);
            geo.Height.Define(Height);
            geo.Depth.Define(Depth);
        }

        protected override void ConfigureUIMaterial(UIMaterial uiMaterial)
        {
            uiMaterial.Color.Define(this.Color);
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureAllowedChildTypes(ReadOnlyRule<IEnumerable<Type>> rule)
        {
            rule.Define(new Type[] { typeof(MyChildPart) });
        }

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define("enBlock");
        }

        #endregion
    }
}
