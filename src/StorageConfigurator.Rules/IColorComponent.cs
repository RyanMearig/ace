﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ruler.Cpq;
using Ruler.Rules;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public interface IColorComponent
    {
        Rule<string> Color { get; set; }
        Rule<string> ColorBOMName { get; set; }
        ReadOnlyRule<AdvanceColor> AdvanceColor { get; set; }
        ReadOnlyRule<string> ColorShortDescription { get; set; }
        ReadOnlyRule<string> ColorDescription { get; set; }
        StorageConfiguratorRepository Repository { get; set; }

        Rule<double> PartWeight { get; set; }
        Rule<double> PartPaintWeight { get; set; }
        Rule<float> WeightPerGallon { get; set; }
        CsiRule<double> PaintCostPerGallon { get; set; }
        Rule<float> PaintCost { get; set; }
    }

    public static class IColorComponentExtensions
    {
        public static void ConfigureColorComponent(this IColorComponent comp)
        {
            if (comp.Repository != null)
            {
                comp.Color.Define(() => GetParentColorComponent(comp as Part)?.Color.Value
                ?? comp.Color.UIMetadata.ChoiceList.Value.First().Value as string);
                comp.Color.AddToUI(ui => ui.ChoiceList.Define(() =>
                    comp.Repository.GetAllColors()
                                    .Where(a => a.Available)
                                    .OrderBy(s => s.ShortDescription)
                                    .Select(c => new ChoiceListItem(c.Item, c.ShortDescription))));
                comp.AdvanceColor.Define(() => comp.Repository.GetColorByKey(comp.Color.Value));
                comp.ColorBOMName.Define(() => comp.AdvanceColor.Value?.Name);
                comp.ColorShortDescription.Define(() => comp.AdvanceColor.Value?.ShortDescription);
                comp.ColorDescription.Define(() => comp.AdvanceColor.Value?.Description);
            }

            comp.PartWeight.Define(0);
            comp.PartPaintWeight.Define(() =>
            {
                var matWeight = 0.0;
                var bomPlant = "GA_C";
                if (comp is Ruler.Cpq.IBomPart bomPart)
                {
                    bomPlant = bomPart.Bom.PlantCode.Value ?? "GA_C";
                    matWeight = GetRawPartWeightWithoutPaint(bomPart);
                }

                if (matWeight <= 0 || comp.WeightPerGallon.Value <= 0) { return 0; }
                var paintWeightPerGallon = comp.PaintCostPerGallon.CsiPartService.GetItemWeight(comp.Color.Value, bomPlant);
                var gallPaintReq = matWeight / comp.WeightPerGallon.Value;

                return paintWeightPerGallon * gallPaintReq;
            });
            comp.WeightPerGallon.Define(500);
            comp.PaintCostPerGallon.Define(() =>
            {
                var bomPlant = "GA_C"; 
                if (comp is Ruler.Cpq.IBomPart bomPart)
                {
                    bomPlant = bomPart.Bom.PlantCode.Value ?? "GA_C";
                }

                return comp.PaintCostPerGallon.CsiPartService.GetItemCost(comp.Color.Value, bomPlant);
            });
            comp.PaintCost.Define(() => (float)(comp.PartWeight.Value > 0 ? ((float)comp.PartWeight.Value / comp.WeightPerGallon.Value) * comp.PaintCostPerGallon.Value : 0));

        }

        public static double GetPartWeightWithoutPaint(this IColorComponent comp)
        {
            if (comp is Ruler.Cpq.IBomPart bomPart)
            {
                return (bomPart.Bom.IsPurchased.Value ? bomPart.Bom.ItemPurchasedWeight.Value : bomPart.Bom.ItemMaterialWeight.Value)
                            + GetNonPaintChildrenWeight(bomPart);
            }
            return 0;
        }

        public static double GetNonPaintChildrenWeight(Ruler.Cpq.IBomPart bomPart)
        {
            var nonPaintChildren = GetNonPaintChildren(bomPart);
            return nonPaintChildren.Any() ? nonPaintChildren.Sum(c => c.TotalWeight.Value) : 0;
        }

        public static double GetRawPartWeightWithoutPaint(Ruler.Cpq.IBomPart bomPart)
        {
            return (bomPart.Bom.IsPurchased.Value ? bomPart.Bom.ItemPurchasedWeight.Value : bomPart.Bom.ItemRawMaterialWeight.Value)
                            + GetNonPaintChildrenRawWeight(bomPart);
        }

        public static double GetNonPaintChildrenRawWeight(Ruler.Cpq.IBomPart bomPart)
        {
            var nonPaintChildren = GetNonPaintChildren(bomPart);
            return nonPaintChildren.Any() ? nonPaintChildren.Sum(c => c.TotalRawWeight.Value) : 0;
        }

        public static List<BomData> GetNonPaintChildren(Ruler.Cpq.IBomPart bomPart)
        {
            return bomPart.Bom.BomChildren.Value?.Where(c => !(c.Parent.GetType() == typeof(Paint)))?.ToList() ?? new List<BomData>();
        }

        public static IColorComponent GetParentColorComponent(Part comp)
        {
            if(comp.Parent != null)
            {
                if(comp.Parent is IColorComponent colorCompParent)
                {
                    return colorCompParent;
                }
                return GetParentColorComponent(comp.Parent);
            }
            return null;
        }
    }
}
