﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using Ruler.Rules;
using Ruler.Rules.Graphics;

namespace StorageConfigurator.Rules
{
    public class Product : Part, IRevisablePart
    {
        #region Constructors

        public Product(ModelContext modelContext) : base(modelContext) { }

        public Product(string name, Part parent) : base(name, parent) 
        {
            this.SetIndex();
        }

        #endregion

        #region Rules

        public Rule<int> Index { get; set; }
        protected virtual void ConfigureIndex(Rule<int> rule)
        {
            rule.Define(0);
        }

        [UIRule]
        public Rule<string> ProductName { get; set; }
        protected virtual void ConfigureProductName(Rule<string> rule)
        {
            rule.Define(() =>
            {
                var id = Convert.ToChar(64 + Index.Value);
                return id.ToString();
            });
            rule.Unique();
        }

        [UIRule]
        public Rule<float> LiftOff { get; set; }
        protected virtual void ConfigureLiftOff(Rule<float> rule)
        {
            rule.Define(6);
            rule.AddToCAD(cad =>
            {
                cad.CadRuleUsageType = CadRuleUsageType.Property;
            });
        }

        [UIRule]
        public Rule<float> LoadWidth { get; set; }
        protected virtual void ConfigureLoadWidth(Rule<float> rule)
        {
            rule.Define(40);
            rule.AddToCAD(cad =>
            {
                cad.CadRuleUsageType = CadRuleUsageType.Parameter;
            });
        }

        [UIRule]
        public Rule<float> LoadHeight { get; set; }
        protected virtual void ConfigureLoadHeight(Rule<float> rule)
        {
            rule.Define(60);
            rule.AddToCAD(cad =>
            {
                cad.CadRuleUsageType = CadRuleUsageType.Parameter;
            });
        }

        [UIRule]
        public Rule<float> LoadDepth { get; set; }
        protected virtual void ConfigureLoadDepth(Rule<float> rule)
        {
            rule.Define(48);
            rule.AddToCAD(cad =>
            {
                cad.CadRuleUsageType = CadRuleUsageType.Parameter;
            });
        }

        public Rule<string> PalletType { get; set; }
        protected virtual void ConfigurePalletType(Rule<string> rule)
        {
            rule.Define("GMA");
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(new List<ChoiceListItem>()
                {
                    new ChoiceListItem("GMA", "GMA"),
                    new ChoiceListItem("CHEP", "CHEP")
                });
            });
            rule.AddToCAD(cad =>
            {
                cad.CadRuleUsageType = CadRuleUsageType.Property;
            });
        }

        [UIRule]
        public Rule<float> PalletWidth { get; set; }
        protected virtual void ConfigurePalletWidth(Rule<float> rule)
        {
            rule.Define(LoadWidth);
            rule.AddToCAD(cad =>
            {
                cad.CadRuleUsageType = CadRuleUsageType.Parameter;
            });
        }

        public ReadOnlyRule<float> PalletLoadWidth { get; set; }
        protected virtual void ConfigurePalletLoadWidth(ReadOnlyRule<float> rule)
        {
            rule.Define(() =>
            {
                if (PalletWidth.Value > LoadWidth.Value)
                    return PalletWidth.Value;
                else
                    return LoadWidth.Value;
            });
        }

        public ReadOnlyRule<float> PalletLoadDepth { get; set; }
        protected virtual void ConfigurePalletLoadDepth(ReadOnlyRule<float> rule)
        {
            rule.Define(() =>
            {
                if (PalletDepth.Value > LoadDepth.Value)
                    return PalletDepth.Value;
                else
                    return LoadDepth.Value;
            });
        }

        [UIRule]
        public Rule<float> PalletHeight { get; set; }
        protected virtual void ConfigurePalletHeight(Rule<float> rule)
        {
            rule.Define(5.25f);
            rule.AddToCAD(cad =>
            {
                cad.CadRuleUsageType = CadRuleUsageType.Parameter;
            });
        }

        [UIRule]
        public Rule<float> PalletDepth { get; set; }
        protected virtual void ConfigurePalletDepth(Rule<float> rule)
        {
            rule.Define(LoadDepth);
            rule.AddToCAD(cad =>
            {
                cad.CadRuleUsageType = CadRuleUsageType.Parameter;
            });
        }

        [UIRule]
        public Rule<float> Weight { get; set; }
        protected virtual void ConfigureWeight(Rule<float> rule)
        {
            rule.Define(2500);
            rule.AddToCAD(cad =>
            {
                cad.CadRuleUsageType = CadRuleUsageType.Property;
            });
        }

        public Rule<float> LoadOverlap { get; set; }
        protected virtual void ConfigureLoadOverlap(Rule<float> rule)
        {
            rule.Define(() => (LoadWidth.Value - PalletWidth.Value)/2);
        }

        public Rule<bool> ShowGraphics { get; set; }
        protected virtual void ConfigureShowGraphics(Rule<bool> rule)
        {
            rule.Define(true);
        }

        #endregion

        #region IRevisablePartImplementation

        public FactoryRevisionRule<string> FactoryRevision { get; set; }

        #endregion

        #region Public Methods

        private Vector3 GetVzFromOrigin(float zOffset)
        {
            return (Vector3.Add(this.Position.Value, Helper.Vz(zOffset)));
        }

        #endregion

        #region Private Methods

        public void SetIndex()
        {
            if (this.IsAdhoc)
            {
                var existingProds = Parent.ActiveChildParts.Value
                    .Where(c => c != this && c.TypeName == "Product");
                var index = existingProds.Count() + 1;
                this.Index.SetAdhocDefaultValue(index);
            }
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            //Light brown
            material.Color.Define(12293733);
            material.Opacity.Define(0.3f);
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(() => (Parent != null && Parent.GetType() == typeof(Products)));
        }

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define("Product");
        }

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureRevisablePart();
        }

        #endregion

        #region Graphics

        public Block PalletBlock { get; set; }
        protected virtual void ConfigurePalletBlock(Block geo)
        {
            geo.IsActive.Define(ShowGraphics);
            geo.Width.Define(PalletDepth);
            geo.Height.Define(PalletHeight);
            geo.Depth.Define(PalletWidth);
            geo.Position.Define(() => Helper.Vyz(geo.Height.Value / 2, -(geo.Depth.Value / 2) - LoadOverlap.Value));
        }

        public Block LoadBlock { get; set; }
        protected virtual void ConfigureLoadBlock(Block geo)
        {
            geo.IsActive.Define(ShowGraphics);
            geo.Width.Define(LoadDepth);
            geo.Height.Define(() => LoadHeight.Value - PalletHeight.Value);
            geo.Depth.Define(LoadWidth);
            geo.Position.Define(() => Helper.Vyz(PalletBlock.Height.Value + (geo.Height.Value / 2), -geo.Depth.Value / 2));
        }

        //public UCS UCS { get; set; }
        //protected virtual void ConfigureUCS(UCS child)
        //{
        //    child.Diameter.Define(0.25f);
        //    child.Position.Define(() => (Helper.Vy(0)));
        //}

        #endregion

    }
}
