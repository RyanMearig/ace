﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ruler.Rules;

namespace StorageConfigurator.Rules
{
    public class Products : Part
    {
        #region Constructors

        public Products(ModelContext modelContext) : base(modelContext) { }

        public Products(string name, Part parent) : base(name, parent) { }

        #endregion

        #region Rules

        [UIRule]
        public ReadOnlyRule<int> ProductQty { get; set; }
        protected virtual void ConfigureProductQty(ReadOnlyRule<int> rule)
        {
            rule.Define(() => ProductList.Value.Count);
        }

        public Rule<List<Product>> ProductList { get; set; }
        protected virtual void ConfigureProductList(Rule<List<Product>> rule)
        {
            rule.Define(() =>
            {
                List<Product> retVal = new List<Product>();

                if (ActiveChildParts.Value != null && ActiveChildParts.Value.Any())
                    retVal = ActiveChildParts.Value.Where(c => c.GetType() == typeof(Product)).Select(p => (Product)p).ToList();

                return retVal;
            });
        }

        protected override void ConfigureAllowedChildTypes(ReadOnlyRule<IEnumerable<Type>> rule)
        {
            rule.Define(() => new List<Type>
            {
                typeof(Product)
            });
        }

        #endregion

        #region Children

        public Product Default { get; set; }
        protected virtual void ConfigureDefault(Product child)
        {
            //child.IsActive.Define(() => ActiveChildParts.Value == null || !ActiveChildParts.Value.Any());
            child.ProductName.Define("Default Product");
        }

        #endregion

        #region Part Overrides

        public override bool IsCadPart => false;

        #endregion

    }
}
