﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;
using Ruler.Cpq;

namespace StorageConfigurator.Rules
{
    public class AdvancePart : Part
    {
        #region Constructors

        public AdvancePart(ModelContext modelContext) : base(modelContext) { }

        public AdvancePart(string name, Part parent) : base(name, parent) { }

        #endregion

        #region Rules

        //This is being created to reduce the BOMData being passed over in the Web Json.  Once the Ruler.CPQ has
        //changed to allow for specifying what UI Properties to active, this can be removed.
        public Rule<string> BOMDataDescription { get; set; }
        protected virtual void ConfigureBOMDataDescription(Rule<string> rule)
        {
            rule.Define(() => this.HasChild("Bom") ? ((BomData)this.GetChild("Bom")).Description.Value : "");
            rule.AddToUI(ui =>
            {
                ui.IsActive.Define(() => this.HasChild("Bom"));
                ui.ReadOnly.Define(true);
            });
        }

        public Rule<double> BOMDataMargin { get; set; }
        protected virtual void ConfigureBOMDataMargin(Rule<double> rule)
        {
            rule.Define(() => this.HasChild("Bom") ? ((BomData)this.GetChild("Bom")).Margin.Value : 0.0);
            rule.AddToUI(ui =>
            {
                ui.IsActive.Define(() => this.HasChild("Bom"));
                ui.ReadOnly.Define(true);
            });
        }

        public Rule<string> BOMItemNumber { get; set; }
        protected virtual void ConfigureBOMItemNumber(Rule<string> rule)
        {
            rule.Define(() => this.HasChild("Bom") ? ((BomData)this.GetChild("Bom")).Item.Value : string.Empty);
            rule.AddToUI(ui =>
            {
                ui.IsActive.Define(() => this.HasChild("Bom"));
                ui.ReadOnly.Define(true);
            });
        }

        #endregion

    }
}
