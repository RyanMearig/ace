﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;
using Ruler.Rules.Graphics;

namespace StorageConfigurator.Rules
{
    public class MyChildPart : Part
    {
        #region Constructors

        public MyChildPart(ModelContext modelContext): base(modelContext) { }

        public MyChildPart(string name, Part parent) : base(name, parent) { }

        #endregion

        #region Rules

        public Rule<int> MyRule { get; set; }
        protected virtual void ConfigureMyRule(Rule<int> rule)
        {
            rule.Define(7);
            rule.AddToUI();
        }

        [UIRule]
        public Rule<float> Width { get; set; }
        protected virtual void ConfigureWidth(Rule<float> rule)
        {
            rule.Define(5);
        }

        [UIRule]
        public Rule<float> Height { get; set; }
        protected virtual void ConfigureHeight(Rule<float> rule)
        {
            rule.Define(5);
        }

        [UIRule]
        public Rule<float> Depth { get; set; }
        protected virtual void ConfigureDepth(Rule<float> rule)
        {
            rule.Define(5);
        }

        #endregion

        #region Graphics

        public Block Block { get; set; }
        protected virtual void ConfigureBlock(Block geo)
        {
            geo.Width.Define(Width);
            geo.Height.Define(Height);
            geo.Depth.Define(Depth);
        }

        protected override void ConfigureUIMaterial(UIMaterial uiMaterial)
        {
            uiMaterial.Color.Define(Colors.Blue);
        }

        #endregion
    }
}
