﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;
using Ruler.Cpq;

namespace StorageConfigurator.Rules
{
    public class GenericBomPart : AdvancePart, Ruler.Cpq.IBomPart
    {
        public GenericBomPart(ModelContext modelContext) : base(modelContext)
        {
        }

        public GenericBomPart(string name, Part parent) : base(name, parent)
        {
        }

        public BomData Bom { get;set; }
    }
}
