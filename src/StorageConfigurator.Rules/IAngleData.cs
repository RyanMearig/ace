﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ruler.Rules;
using Ruler.Rules.Validation;

namespace StorageConfigurator.Rules
{
    public interface IAngleData
    {
        Rule<List<AngleData>> AllAngleData { get; set; }
        Rule<List<ChoiceListItem>> AllAngleTypesChoiceList { get; set; }
    }

    public static class IAngleDataExtensions
    {
        public static void ConfigureAngleDataRules(this IAngleData comp)
        {
            comp.AllAngleData.Define(() =>
            {
                List<AngleData> angleData = new List<AngleData>();

                angleData.Add(new AngleData() { Type = @"1-1/2 x 1-1/2 x 1/8", TypeDisplay = @"1-1/2 x 1-1/2 x 1/8", Height = 1.5f, Width = 1.5f, Thickness = 0.125f });
                angleData.Add(new AngleData() { Type = @"2 x 2 x 1/8", TypeDisplay = @"2 x 2 x 1/8", Height = 2, Width = 2, Thickness = 0.125f });
                angleData.Add(new AngleData() { Type = @"2 x 2 x 3/16", TypeDisplay = @"2 x 2 x 3/16", Height = 2, Width = 2, Thickness = 0.1875f });
                angleData.Add(new AngleData() { Type = @"2 x 2 x 1/4", TypeDisplay = @"2 x 2 x 1/4", Height = 2, Width = 2, Thickness = 0.25f });
                angleData.Add(new AngleData() { Type = @"3 x 2 x 3/16", TypeDisplay = @"3 x 2 x 3/16", Height = 3, Width = 2, Thickness = 0.1875f });
                angleData.Add(new AngleData() { Type = @"2-1/2 x 2-1/2 x 3/16", TypeDisplay = @"2-1/2 x 2-1/2 x 3/16", Height = 2.5f, Width = 2.5f, Thickness = 0.1875f });
                angleData.Add(new AngleData() { Type = @"2-1/2 x 2-1/2 x 1/4", TypeDisplay = @"2-1/2 x 2-1/2 x 1/4", Height = 2.5f, Width = 2.5f, Thickness = 0.25f });
                angleData.Add(new AngleData() { Type = @"3 x 2 x 1/4", TypeDisplay = @"3 x 2 x 1/4", Height = 3, Width = 2, Thickness = 0.25f });
                angleData.Add(new AngleData() { Type = @"3 x 3 x 1/4", TypeDisplay = @"3 x 3 x 1/4", Height = 3, Width = 3, Thickness = 0.25f });
                angleData.Add(new AngleData() { Type = @"4 x 4 x 1/4", TypeDisplay = @"4 x 4 x 1/4", Height = 4, Width = 4, Thickness = 0.25f });

                return angleData;
            });

            comp.AllAngleTypesChoiceList.Define(() =>
            {
                List<ChoiceListItem> choices = new List<ChoiceListItem>();

                if (comp.AllAngleData != null && comp.AllAngleData.Value != null && comp.AllAngleData.Value.Any())
                    comp.AllAngleData.Value.ForEach(d => choices.Add(new ChoiceListItem(d.Type, d.TypeDisplay)));

                return choices;
            });
        }

        public static string GetAngleShortDesc(this IAngleData comp, string angleType)
        {
            var angle = comp.AllAngleData.Value.FirstOrDefault(ad => ad.Type == angleType);
            if (angle == null) return angleType;
            if (angle.Height == angle.Width) return $"{angle.Height:#.#}x{Helper.fractionString(angle.Thickness, feetToo: false)}";
            return (angle.Height == angle.Width) ? $"{angle.Height:#.#}x{Helper.fractionString(angle.Thickness, feetToo: false)}"
                : $"{angle.Height:#.#}x{angle.Width:#.#}x{Helper.fractionString(angle.Thickness, feetToo: false)}";
        }
    }
}
