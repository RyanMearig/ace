﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ruler.Rules;
using Ruler.Rules.Validation;

namespace StorageConfigurator.Rules
{
    public interface IShelfLoadBeamInputs : ILoadBeamData
    {
        Rule<float> PalletWidth { get; set; }
        Rule<float> LoadWidth { get; set; }
        Rule<float> LoadOverlap { get; set; }
        Rule<int> LoadsWide { get; set; }
        Rule<float> LoadToFrameOffset { get; set; }
        Rule<float> LoadToFrameOffsetMin { get; set; }
        Rule<float> LoadToLoadOffset { get; set; }
        Rule<float> LoadToLoadOffsetMin { get; set; }
        Rule<float> SupportOffsetFromPallet { get; set; }
    }

    public static class IShelfLoadBeamInputsExtensions
    {
        public static void ConfigureShelfLoadBeamInputsRules(this IShelfLoadBeamInputs comp)
        {
            comp.ConfigureLoadBeamDataRules();

            comp.LoadWidth.Define(40);
            comp.LoadWidth.AddToUI();

            comp.PalletWidth.Define(() => comp.LoadWidth.Value);
            comp.PalletWidth.AddToUI();

            comp.LoadOverlap.Define(() => (comp.LoadWidth.Value - comp.PalletWidth.Value)/2);

            comp.LoadsWide.Define(1);
            comp.LoadsWide.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() => (new List<ChoiceListItem>()
                        {
                            new ChoiceListItem(1),
                            new ChoiceListItem(2),
                            new ChoiceListItem(3),
                            new ChoiceListItem(4)
                        }));
            });

            comp.LoadToLoadOffsetMin.Define(2);

            comp.LoadToLoadOffset.Define(8);
            comp.LoadToLoadOffset.Min(comp.LoadToLoadOffsetMin);
            comp.LoadToLoadOffset.AddToUI();

            comp.LoadToFrameOffsetMin.Define(1);

            comp.LoadToFrameOffset.Define(4);
            comp.LoadToFrameOffset.Min(comp.LoadToFrameOffsetMin);
            comp.LoadToFrameOffset.AddToUI();

            comp.SupportOffsetFromPallet.Define(4);
        }

    }
}
