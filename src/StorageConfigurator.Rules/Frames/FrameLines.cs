﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ruler.Rules;

namespace StorageConfigurator.Rules
{
    public class FrameLines : Part
    {
        #region Constructors

        public FrameLines(ModelContext modelContext) : base(modelContext) { }

        public FrameLines(string name, Part parent) : base(name, parent) { }

        #endregion

        #region Rules

        public LookupRule<StorageSystem> StorageSystemComp { get; set; }

        [UIRule]
        public ReadOnlyRule<int> FrameLineQty { get; set; }
        protected virtual void ConfigureFrameLineQty(ReadOnlyRule<int> rule)
        {
            rule.Define(() => FrameLineList.Value.Count);
        }

        public Rule<List<FrameLine>> FrameLineList { get; set; }
        protected virtual void ConfigureFrameLineList(Rule<List<FrameLine>> rule)
        {
            rule.Define(() =>
            {
                List<FrameLine> retVal = new List<FrameLine>();

                if (ActiveChildParts.Value != null && ActiveChildParts.Value.Any())
                    retVal = ActiveChildParts.Value.Where(c =>
                    {
                        return (c.GetType() == typeof(SelectiveFrameLine));
                    })
                    .Select(p => (FrameLine)p).ToList();

                return retVal;
            });
        }

        public Rule<List<FrameType>> FrameTypeList { get; set; }
        protected virtual void ConfigureFrameTypeList(Rule<List<FrameType>> rule)
        {
            rule.Define(() => StorageSystemComp.Value?.FrameTypes.FrameTypeList.Value);
        }

        public Rule<List<string>> FrameTypesUsedInFrameLines { get; set; }
        protected virtual void ConfigureFrameTypesUsedInFrameLines(Rule<List<string>> rule)
        {
            rule.Define(() =>
            {
                List<string> retList = new List<string>();

                if (FrameLineQty.Value > 0)
                {
                    FrameLineList.Value.ForEach(f =>
                    {
                        if (f.IsSelectiveFrameLine.Value)
                        {
                            var selectFrameLine = (SelectiveFrameLine)f;
                            retList.Add(selectFrameLine.FrontFrame.Value.Path);
                            
                            if (selectFrameLine.IsBackToBack.Value) retList.Add(selectFrameLine.RearFrame.Value.Path);
                        }
                    });

                    retList = retList.Distinct().ToList();
                }

                return retList;
            });
        }

        protected override void ConfigureAllowedChildTypes(ReadOnlyRule<IEnumerable<Type>> rule)
        {
            rule.Define(() => new List<Type>
            {
                typeof(SelectiveFrameLine)
            });
        }

        #endregion

        #region Children

        

        #endregion

        #region Part Overrides

        public override bool IsCadPart => false;

        #endregion

    }
}
