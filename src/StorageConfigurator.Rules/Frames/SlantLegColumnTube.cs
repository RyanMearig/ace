﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;
using Ruler.Cpq;
using Ruler.Rules.Graphics;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class SlantLegColumnTube : AdvancePart, IRevisablePart, IBomPart, Ruler.Cpq.IBomPart
    {
        #region Constructors

        public SlantLegColumnTube(ModelContext modelContext) : base(modelContext) { }

        public SlantLegColumnTube(string name, Part parent) : base(name, parent) { }

        #endregion

        #region Rules

        public Rule<string> MaterialPartNumber { get; set; }
        protected virtual void ConfigureMaterialPartNumber(Rule<string> rule)
        {
            rule.Define(default(string));
        }

        public Rule<string> UprightType { get; set; }
        protected virtual void ConfigureUprightType(Rule<string> rule)
        {
            rule.Define("L");
        }

        public Rule<float> Width { get; set; }
        protected virtual void ConfigureWidth(Rule<float> rule)
        {
            rule.Define(3);
            rule.AddToCAD(cad =>
            {
                cad.NameInCad = "G_W";
                cad.CadRuleUsageType = CadRuleUsageType.Parameter;
            });
        }

        public Rule<float> Depth { get; set; }
        protected virtual void ConfigureDepth(Rule<float> rule)
        {
            rule.Define(3);
            rule.AddToCAD(cad =>
            {
                cad.NameInCad = "G_H";
                cad.CadRuleUsageType = CadRuleUsageType.Parameter;
            });
        }

        public Rule<float> Length { get; set; }
        protected virtual void ConfigureLength(Rule<float> rule)
        {
            rule.Define(() => UprightType.Value == "L" ? 57.6875f : 38.6875f);
            rule.AddToCAD(cad =>
            {
                cad.NameInCad = "B_L";
                cad.CadRuleUsageType = CadRuleUsageType.Parameter;
            });
        }

        [CadRule(CadRuleUsageType.Parameter)]
        public Rule<float> MiterAngle { get; set; }
        protected virtual void ConfigureMiterAngle(Rule<float> rule)
        {
            rule.Define(() => UprightType.Value == "L" ? 12.15f : 18.4f);
        }

        public Rule<float> Thickness { get; set; }
        protected virtual void ConfigureThickness(Rule<float> rule)
        {
            rule.Define(0.1875f);
        }

        #endregion

        #region IRevisablePartImplementation

        public FactoryRevisionRule<string> FactoryRevision { get; set; }

        #endregion

        #region IBomPartImplementation

        public Rule<string> DatabasePartNumber { get; set; }
        public PartNumberRule<string> PartNumber { get; set; }
        public Rule<string> PartNumberPrefix { get; set; }
        public Rule<string> Description { get; set; }
        public Rule<string> BOMDescription { get; set; }
        public Rule<string> BOMDescription1 { get; set; }
        public Rule<string> BOMDescription2 { get; set; }
        public Rule<string> BOMDescription3 { get; set; }
        public Rule<string> BOMDescription4 { get; set; }
        public Rule<string> BOMDescription5 { get; set; }
        public Rule<string> BOMDescription6 { get; set; }
        public Rule<string> BOMDescription7 { get; set; }
        public Rule<string> BOMDescription8 { get; set; }
        public Rule<string> BOMDescription9 { get; set; }
        public Rule<string> BOMDescription10 { get; set; }
        public Rule<string> BOMDescription11 { get; set; }
        public Rule<string> Color { get; set; }
        public ReadOnlyRule<AdvanceColor> AdvanceColor { get; set; }
        public ReadOnlyRule<string> ColorDescription { get; set; }
        public StorageConfiguratorRepository Repository { get; set; }
        public Rule<string> ColorBOMName { get; set; }
        public ReadOnlyRule<string> ColorShortDescription { get; set; }

        public Rule<double> PartWeight { get; set; }
        public Rule<double> PartPaintWeight { get; set; }
        public Rule<float> WeightPerGallon { get; set; }
        public CsiRule<double> PaintCostPerGallon { get; set; }
        public Rule<float> PaintCost { get; set; }

        #endregion

        #region CPQ.IBomPart Implementation

        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {
            child.PartNumber.Define(PartNumber);
            child.MaterialPartNumber.Define(MaterialPartNumber);
            child.MaterialQty.Define(() => Length.Value / 12);
            child.ItemMaterialCost.Define(() => child.ItemRawMaterialWeight.Value * child.MaterialUnitCost.Value);
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(Colors.Red);
        }

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define("SlantLegColumnTube");
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(true);
        }

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureRevisablePart();
            this.ConfigureBomPartRules();
        }

        #endregion

        #region Graphics

        public Block TubeBlock { get; set; }
        protected virtual void ConfigureTubeBlock(Block geo)
        {
            geo.Width.Define(Width);
            geo.Height.Define(Length);
            geo.Depth.Define(Depth);
            geo.Position.Define(() => Helper.Vxy(geo.Width.Value / 2, -geo.Height.Value/2));
        }

        //public UCS UCS { get; set; }
        //protected virtual void ConfigureUCS(UCS child)
        //{
        //    child.Diameter.Define(0.0125f);
        //    child.Position.Define(() => (Helper.Vy(0)));
        //}

        #endregion

    }
}
