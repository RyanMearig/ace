﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;
using Ruler.Rules.Validation;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class SelectiveFrameType : FrameType
    {

        #region Constructors

        public SelectiveFrameType(ModelContext modelContext, StorageConfiguratorRepository repo) : base(modelContext, repo) { }

        public SelectiveFrameType(string name, Part parent, StorageConfiguratorRepository repo) : base(name, parent, repo) { }

        #endregion

        #region Rules

        public Rule<SelectiveFrameType> SelectiveFrameTypeMaster { get; set; }
        protected virtual void ConfigureSelectiveFrameTypeMaster(Rule<SelectiveFrameType> rule)
        {
            rule.Define(default(SelectiveFrameType));
        }

        #endregion

        #region FrameType Overrides

        protected override void ConfigureFrameTypeMaster(Rule<FrameType> rule)
        {
            rule.Define(() => SelectiveFrameTypeMaster.Value == default(SelectiveFrameType) ? default(FrameType) : SelectiveFrameTypeMaster.Value);
        }

        protected virtual void ConfigureIsDoubleDeep(Rule<bool> rule)
        {
            rule.Define(false);
        }

        protected override void ConfigurePanelQty(ReadOnlyRule<int> rule)
        {
            //To be overridden to pull data from front frame
            rule.Define(() => Frame?.Frame?.PanelQty.Value ?? 0);
        }

        protected virtual void ConfigureFrontDoublerHeightMin(Rule<float> rule)
        {
            rule.Define(() =>
            {
                float retVal = 12;

                if (UprightType.Value == "B")
                    retVal = SlantElev.Value + 48;
                else if (Slanted.Value)
                    retVal = retVal + SlantElev.Value;

                return retVal;
            });
        }

        protected virtual void ConfigureFrontDoublerHeight(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (SelectiveFrameTypeMaster.Value != default(SelectiveFrameType))
                {
                    return SelectiveFrameTypeMaster.Value.FrontDoublerHeight.Value;
                }

                if (UprightType.Value == "B")
                {
                    return FrontDoublerHeightMin.Value;
                }
                else if (Frame.Frame.FrontColumnHeight.Value > 84)
                {
                    return 84;
                }
                return Helper.RoundDownToNearest(Frame.Frame.FrontColumnHeight.Value, increment: 4);
            });
            rule.Min(FrontDoublerHeightMin);
            rule.Max(Height);
            rule.MultipleOf(4);
            rule.AddToUI(ui =>
            {
                //to be used for turning off the property
                ui.ReadOnly.Define(() => !HasFrontDoubler.Value);
            });
        }

        protected virtual void ConfigureRearDoublerHeight(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (SelectiveFrameTypeMaster.Value != default(SelectiveFrameType))
                {
                    return SelectiveFrameTypeMaster.Value.RearDoublerHeight.Value;
                }

                return (Frame.Frame.RearColumnHeight.Value > 84 ? 84 : Helper.RoundDownToNearest(Frame.Frame.RearColumnHeight.Value, increment: 4));
            });
            rule.Max(Height);
            rule.MultipleOf(4);
            rule.AddToUI(ui =>
            {
                //to be used for turning off the property
                ui.ReadOnly.Define(() => !HasRearDoubler.Value);
            });
        }

        protected override void ConfigureWidth(Rule<float> rule)
        {
            rule.Define(() => Frame.Frame.FrameWidth.Value);
        }

        protected override void ConfigureFrontFrameDepthDisplay(Rule<float> rule)
        {
            rule.Define(() => Frame.Frame.Depth.Value);
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(true);
                ui.Label.Define("Front Frame Depth");
            });
        }

        protected virtual void ConfigureFootplateWidth(Rule<float> rule)
        {
            rule.Define(() => Frame.Frame.DefaultFootplateWidth.Value);
            rule.AddToUI(ui =>
            {
                ui.Label.Define("Default Footplate Width");
            });
        }

        #endregion

        #region Children

        public FrameUprightKit Frame { get; set; }
        protected virtual void ConfigureFrame(FrameUprightKit child)
        {
            child.FrameUprightKitMaster.Define(() => SelectiveFrameTypeMaster.Value?.Frame ?? default(FrameUprightKit));
            child.IsFrontFrame.Define(true);
            child.DefaultDepth.Define(FrontFrameDepth);
            child.Height.Define(Height);
            child.UprightColumnSize.Define(UprightColumnSize);
            child.FrontAnchorQty.Define(DefaultAnchorQtyPerFootplate);
            child.RearAnchorQty.Define(DefaultAnchorQtyPerFootplate);
            child.FrontAnchorSize.Define(DefaultAnchorSizePerFootplate);
            child.RearAnchorSize.Define(DefaultAnchorSizePerFootplate);
            child.DefaultAnchorTypePerFootplate.Define(DefaultAnchorTypePerFootplate);
            child.UprightType.Define(UprightType);
            child.SlantElev.Define(SlantElev);
            child.SlantInset.Define(SlantInset);
            child.FootplateWidth.Define(FootplateWidth);
            child.FootplateThickness.Define(FootplateThickness);
        }

        #endregion


    }
}
