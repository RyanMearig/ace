﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;
using Ruler.Rules;
using Ruler.Cpq;
using Ruler.Rules.Graphics;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class WeldedRubRail : FrameTieComponent, IChannel, IRevisablePart, Ruler.Cpq.IBomPart
    {
        #region Constructors

        public WeldedRubRail(ModelContext modelContext, StorageConfiguratorRepository repo) : base(modelContext, repo) { }

        public WeldedRubRail(string name, Part parent, StorageConfiguratorRepository repo) : base(name, parent, repo) { }

        #endregion

        #region Rules

        protected override void ConfigureType(Rule<string> rule)
        {
            rule.Define("rrx");
            rule.AddToUI();
            rule.AddToCAD(cad =>
            {
                cad.CadRuleUsageType = CadRuleUsageType.Property;
            });
            //rule.AddToUI((ui) =>
            //{
            //    ui.ChoiceList.Define(() => (new List<ChoiceListItem>()
            //    {
            //        new ChoiceListItem("rrx", "Rub Rail(RRx)"),
            //        new ChoiceListItem("rrxI", "Rub Rail(RRxI)"),
            //        new ChoiceListItem("rrxID", "Rub Rail(RRxID)")
            //    }));
            //});
        }

        public Rule<double> Qty { get; set; }
        protected virtual void ConfigureQty(Rule<double> rule)
        {
            rule.Define(1);
            rule.AddToUI(ui =>
            {
                ui.Label.Define("Quantity");
                ui.ReadOnly.Define(true);
            });
        }

        public Rule<float> BeamWidth { get; set; }
        protected virtual void ConfigureBeamWidth(Rule<float> rule)
        {
            rule.Define(default(float));
        }

        public Rule<float> PlateWidth { get; set; }
        protected virtual void ConfigurePlateWidth(Rule<float> rule)
        {
            rule.Define(BeamWidth);
        }

        public Rule<float> EndPlateVerticalOffset { get; set; }
        protected virtual void ConfigureEndPlateVerticalOffset(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (Type.Value == "rrxI")
                    return 2.125f;
                else
                    return 2.25f;
            });
        }

        public Rule<float> RubRailNotchedAngleOffset { get; set; }
        protected virtual void ConfigureRubRailNotchedAngleOffset(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (ChannelWidth.Value == 5)
                    return 1.625f;
                else if (ChannelWidth.Value == 4)
                    return 1.4375f;
                else
                    return 1.3125f;
            });
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(() => Type.Value == "rrxID");
                cad.CadRuleUsageType = CadRuleUsageType.Property;
            });
        }

        public Rule<float> ChannelLength { get; set; }
        protected virtual void ConfigureChannelLength(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (Type.Value == "rrx")
                    return OverallLength.Value - (FrontEndPlate.Thickness.Value * 2);
                else if (Type.Value == "rrxI")
                    return OverallLength.Value - FrontEndPlate.Thickness.Value - RubRailNotchedAngleOffset.Value;
                else if (Type.Value == "rrxID")
                    return OverallLength.Value - (RubRailNotchedAngleOffset.Value*2);
                else
                    return OverallLength.Value;
            });
        }

        public Rule<float> ChannelXOffset { get; set; }
        protected virtual void ConfigureChannelXOffset(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (Type.Value == "rrxID")
                    return -RubRailNotchedAngleOffset.Value;
                else
                    return -FrontEndPlate.Thickness.Value;
            });
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<float> BeamCutLength { get; set; }
        protected virtual void ConfigureBeamCutLength(Rule<float> rule)
        {
            rule.Define(() => Channel.Length.Value);
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> ChannelPartNumber { get; set; }
        protected virtual void ConfigureChannelPartNumber(Rule<string> rule)
        {
            rule.Define(() =>
            {
                switch (ChannelSize.Value)
                {
                    case @"C3x3.5":
                        return "PC31100";
                    case @"C3x4.1":
                        return "PC31101";
                    case @"C4x4.5":
                        return "PC31102";
                    case @"C4x5.4":
                        return "PC31103";
                    case @"C5x6.1":
                        return "PC31104L";
                    case @"C5x6.7":
                        return "PC31104";
                    default:
                        return "GET INFO";
                }
            });
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> RubRailChannelDescription { get; set; }
        protected virtual void ConfigureRubRailChannelDescription(Rule<string> rule)
        {
            rule.Define(() => Channel.Description.Value);
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<float> ChannelKSI { get; set; }
        protected virtual void ConfigureChannelKSI(Rule<float> rule)
        {
            rule.Define(() => Channel.KSI.Value);
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> WeldLength { get; set; }
        protected virtual void ConfigureWeldLength(Rule<string> rule)
        {
            rule.Define(() =>
            {
                switch (ChannelSize.Value)
                {
                    case @"C3x3.5":
                    case @"C3x4.1":
                        return @"4-1/2""";
                    case @"C4x4.5":
                    case @"C4x5.4":
                        return @"5-1/2""";
                    case @"C5x6.1":
                    case @"C5x6.7":
                        return @"6-1/2""";
                    default:
                        return "GET INFO";
                }
            });
        }

        
        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> EndPlatePartNumber { get; set; }
        protected virtual void ConfigureEndPlatePartNumber(Rule<string> rule)
        {
            rule.Define(() =>
            {
                string basePartNumber = "P40802";

                if (PlateWidth.Value == 3)
                {
                    return basePartNumber;
                }
                else
                {
                    return $"{basePartNumber}-{PlateWidth.Value}";
                }
            });
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> EndPlateDescription { get; set; }
        protected virtual void ConfigureEndPlateDescription(Rule<string> rule)
        {
            rule.Define(() => FrontEndPlate.Description.Value);
        }

        public Rule<float> AngleCutLength { get; set; }
        protected virtual void ConfigureAngleCutLength(Rule<float> rule)
        {
            rule.Define(() => Type.Value == "rrxI" || Type.Value == "rrxID" ? RearRubRailAngle.Length.Value : 0);
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(() => Type.Value == "rrxI" || Type.Value == "rrxID");
                cad.CadRuleUsageType = CadRuleUsageType.Property;
            });
        }

        #endregion

        #region FrameTieComponent Overrides

        protected override void ConfigureFrameTieDescription1(ReadOnlyRule<string> rule)
        {
            rule.Define(() => Type.Value.ToLower() == "rrx" ? "WELDED" : "WELDED RUB RAIL");
        }

        protected override void ConfigureFrameTieDescription2(ReadOnlyRule<string> rule)
        {
            rule.Define(() =>
            {
                switch (Type.Value.ToLower())
                {
                    case "rrx":
                        return "RUB RAIL";
                    case "rrxi":
                        return "SINGLE INSIDE";
                    case "rrxid":
                        return "DOUBLE INSIDE";
                    default:
                        return "GET INFO";
                }
            });
        }

        protected virtual void ConfigureBOMDescription(Rule<string> rule)
        {
            rule.Define(() =>
            {
                var desc = $"Welded Rub Rail, {OverallLength.Value:#.##}\" Long";
                var channelSize1 = 3; //TODO: Logic for this
                switch (Type.Value)
                {
                    case "rrx":
                        desc += $"|{ChannelSize.Value}# Material with 3/16\" x {ChannelWidth.Value}\" x 6\" End Plates";
                        break;
                    case "rrxI":
                        desc += $"|{ChannelSize.Value}# Material with (1) 3/16\" x {ChannelWidth.Value}\" x 6\"";
                        desc += $"|End Plates and (1) 1/4\"x4\"x4\"x{channelSize1}\" Mtg Clip";
                        break;
                    case "rrxID":
                        desc += $"|{ChannelSize.Value}# Material with (2) 1/4\"x4\"x4\"x{channelSize1}\" Mtg Clip";
                        break;
                }

                desc += $"|{ColorBOMName.Value}";
                return desc;
            });
        }

        protected override void ConfigurePartNumberPrefix(Rule<string> rule)
        {
            rule.Define(() => $"{Type.Value.ToUpper().Replace("X", Channel.ChannelId.Value.ToUpper())}-");
        }

        #endregion

        #region IChannel Implementation

        public Rule<List<ChannelData>> AllChannelData { get; set; }
        public Rule<List<ChoiceListItem>> AllChannelSizesChoiceList { get; set; }
        public Rule<ChannelData> ActiveChannelData { get; set; }
        public Rule<float> ChannelWidth { get; set; }
        public Rule<float> ChannelDepth { get; set; }
        public Rule<float> ChannelWebThickness { get; set; }
        public Rule<List<string>> ChannelSizesAvailable { get; set; }
        public Rule<string> ChannelSize { get; set; }
        public Rule<float> ChannelWeightPerFt { get; set; }
        public Rule<string> MaterialPartNumber { get; set; }

        #endregion

        #region IRevisablePartImplementation

        public FactoryRevisionRule<string> FactoryRevision { get; set; }

        #endregion

        #region CPQ.IBomPart Implementation

        //This is the description used only by the BOM Summary.
        public Rule<string> BOMUIDescription { get; set; }
        protected virtual void ConfigureBOMUIDescription(Rule<string> rule)
        {
            rule.Define(() => $"RR({Helper.fractionString(OverallLength.Value, feetToo: false)}, {Channel.ChannelId.Value})");
        }

        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {
            child.PartNumber.Define(PartNumber);
            child.Description.Define(BOMUIDescription);
            child.ItemQty.Define(Qty);
        }

        #endregion

        #region Labor Rules

        public Rule<double> WeldPartsPerHour { get; set; }
        protected virtual void ConfigureWeldPartsPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> PaintCarriersPerHour { get; set; }
        protected virtual void ConfigurePaintCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> PartsPerCarrier { get; set; }
        protected virtual void ConfigurePartsPerCarrier(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> UnloadCarriersPerHour { get; set; }
        protected virtual void ConfigureUnloadCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        #endregion

        #region Children

        public FrameTieChannel Channel { get; set; }
        protected virtual void ConfigureChannel(FrameTieChannel child)
        {
            child.FrameTieType.Define(Type);
            child.ChannelSize.Define(ChannelSize);
            child.Length.Define(ChannelLength);
            child.Position.Define(() => Helper.Vx(ChannelXOffset.Value));
            child.Rotation.Define(() => Quaternion.CreateFromYawPitchRoll(Helper.DegToRad(-90), 0, Helper.DegToRad(270)));
        }

        public FrameTieFourHoleEndPlate FrontEndPlate { get; set; }
        protected virtual void ConfigureFrontEndPlate(FrameTieFourHoleEndPlate child)
        {
            child.IsActive.Define(() => Type.Value != "rrxID");
            child.FrameTieType.Define(Type);
            child.Thickness.Define(0.1875f);
            child.Width.Define(PlateWidth);
            child.Height.Define(6);
            child.HoleDiameter.Define(0.5625f);
            child.HoleVerticalOffset.Define(1);
            child.HoleHorizOffset.Define(0.875f);
            child.Position.Define(() => Helper.Vy(EndPlateVerticalOffset.Value));
        }

        public RubRailAngle FrontRubRailAngle { get; set; }
        protected virtual void ConfigureFrontRubRailAngle(RubRailAngle child)
        {
            child.IsActive.Define(() => Type.Value == "rrxID");
            child.Length.Define(() => BeamWidth.Value - 1);
            child.Position.Define(() => Helper.Vy(-Channel.ChannelWebThickness.Value));
            child.Rotation.Define(() => Helper.CreateRotation(Vector3.UnitY, 180));
        }

        public FrameTieFourHoleEndPlate RearEndPlate { get; set; }
        protected virtual void ConfigureRearEndPlate(FrameTieFourHoleEndPlate child)
        {
            child.IsActive.Define(() => Type.Value == "rrx");
            child.FrameTieType.Define(Type);
            child.Thickness.Define(0.1875f);
            child.Width.Define(PlateWidth);
            child.Height.Define(6);
            child.HoleDiameter.Define(0.5625f);
            child.HoleVerticalOffset.Define(1);
            child.HoleHorizOffset.Define(0.875f);
            child.Position.Define(() => Helper.Vxy(-OverallLength.Value, EndPlateVerticalOffset.Value));
            child.Rotation.Define(() => Helper.CreateRotation(Vector3.UnitY, 180));
        }

        public RubRailAngle RearRubRailAngle { get; set; }
        protected virtual void ConfigureRearRubRailAngle(RubRailAngle child)
        {
            child.IsActive.Define(() => Type.Value != "rrx");
            child.Length.Define(() => BeamWidth.Value - 1);
            child.Position.Define(() => Helper.Vxy(-OverallLength.Value, -Channel.ChannelWebThickness.Value));
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(Colors.Blue);
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(true);
        }

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define("WeldedRubRail");
        }

        #endregion

        #region IBomPart Overrides

        protected virtual void ConfigurePartWeight(Rule<double> rule)
        {
            rule.Define(() => this.GetPartWeightWithoutPaint());
        }

        #endregion

        #region Graphics

        //public UCS UCS { get; set; }
        //protected virtual void ConfigureUCS(UCS child)
        //{
        //    child.Diameter.Define(0.125f);
        //    child.Position.Define(() => (Helper.Vy(0)));
        //}

        #endregion

        #region Protected Methods

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureChannelRules();
            this.ConfigureRevisablePart();
        }

        #endregion

    }
}
