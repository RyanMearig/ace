﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;
using Ruler.Rules;
using Ruler.Cpq;
using Ruler.Rules.Graphics;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class WeldedHDFrameTie : FrameTieComponent, IChannel, IRevisablePart, Ruler.Cpq.IBomPart
    {
        #region Constructors

        public WeldedHDFrameTie(ModelContext modelContext, StorageConfiguratorRepository repo) : base(modelContext, repo) { }

        public WeldedHDFrameTie(string name, Part parent, StorageConfiguratorRepository repo) : base(name, parent, repo) { }

        #endregion

        #region Rules

        public Rule<double> Qty { get; set; }
        protected virtual void ConfigureQty(Rule<double> rule)
        {
            rule.Define(1);
            rule.AddToUI(ui =>
            {
                ui.Label.Define("Quantity");
                ui.ReadOnly.Define(true);
            });
        }

        public Rule<float> BeamWidth { get; set; }
        protected virtual void ConfigureBeamWidth(Rule<float> rule)
        {
            rule.Define(default(float));
        }

        public Rule<float> PlateWidth { get; set; }
        protected virtual void ConfigurePlateWidth(Rule<float> rule)
        {
            rule.Define(BeamWidth);
        }

        public Rule<float> EndPlateVerticalOffset { get; set; }
        protected virtual void ConfigureEndPlateVerticalOffset(Rule<float> rule)
        {
            rule.Define(2.25f);
        }

        public Rule<float> ChannelLength { get; set; }
        protected virtual void ConfigureChannelLength(Rule<float> rule)
        {
            rule.Define(() => OverallLength.Value - (FrontEndPlate.Thickness.Value * 2));
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<float> BeamCutLength { get; set; }
        protected virtual void ConfigureBeamCutLength(Rule<float> rule)
        {
            rule.Define(() => Channel.Length.Value);
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> ChannelPartNumber { get; set; }
        protected virtual void ConfigureChannelPartNumber(Rule<string> rule)
        {
            rule.Define(() => Channel.PartNumber.Value);
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> ChannelMaterialDescription { get; set; }
        protected virtual void ConfigureChannelMaterialDescription(Rule<string> rule)
        {
            rule.Define(() => Channel.Description.Value);
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> EndPlatePartNumber { get; set; }
        protected virtual void ConfigureEndPlatePartNumber(Rule<string> rule)
        {
            rule.Define(() =>
            {
                string basePartNumber = "P40802HD";

                if (FrontEndPlate.Width.Value == 3)
                {
                    return basePartNumber;
                }
                else
                {
                    return $"{basePartNumber}-{FrontEndPlate.Width.Value}";
                }
            });
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> EndPlateDescription { get; set; }
        protected virtual void ConfigureEndPlateDescription(Rule<string> rule)
        {
            rule.Define(() => FrontEndPlate.Description.Value);
        }

        public Rule<string> BeamSize { get; set; }
        protected virtual void ConfigureBeamSize(Rule<string> rule)
        {
            rule.Define(default(string));
        }

        #endregion

        #region FrameTieComponent Overrides

        protected override void ConfigureFrameTieDescription1(ReadOnlyRule<string> rule)
        {
            rule.Define("WELDED HEAVY DUTY");
        }

        protected override void ConfigureFrameTieDescription2(ReadOnlyRule<string> rule)
        {
            rule.Define("ROW/FRAME SPACER");
        }

        protected virtual void ConfigureBOMDescription(Rule<string> rule)
        {
            rule.Define(() =>
            {
                var desc = $"Welded HD Row/Frame Spacer {OverallLength.Value:#.##}\" Long";
                desc += $"|{ChannelSize.Value} Material w/ 3/16\" x 10\"H End Plates";
                desc += $"|Connecting to {BeamSize.Value} column";
                desc += $"|{ColorBOMName.Value}";
                return desc;
            });
        }

        #endregion

        #region IChannel Implementation

        public Rule<List<ChannelData>> AllChannelData { get; set; }
        public Rule<List<ChoiceListItem>> AllChannelSizesChoiceList { get; set; }
        public Rule<ChannelData> ActiveChannelData { get; set; }
        public Rule<float> ChannelWidth { get; set; }
        public Rule<float> ChannelDepth { get; set; }
        public Rule<float> ChannelWebThickness { get; set; }
        public Rule<float> ChannelWeightPerFt { get; set; }
        public Rule<List<string>> ChannelSizesAvailable { get; set; }
        public Rule<string> ChannelSize { get; set; }
        public Rule<string> MaterialPartNumber { get; set; }

        #endregion

        #region IRevisablePartImplementation

        public FactoryRevisionRule<string> FactoryRevision { get; set; }

        #endregion

        #region CPQ.IBomPart Implementation

        //This is the description used only by the BOM Summary.
        public Rule<string> BOMUIDescription { get; set; }
        protected virtual void ConfigureBOMUIDescription(Rule<string> rule)
        {
            rule.Define(() => $"{Type.Value.ToUpper()}({Helper.fractionString(OverallLength.Value, feetToo: false)}, {Channel.ChannelId.Value})");
        }

        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {
            child.PartNumber.Define(PartNumber);
            child.Description.Define(BOMUIDescription);
            child.ItemQty.Define(Qty);
        }

        #endregion

        #region Labor Rules

        public Rule<double> WeldPartsPerHour { get; set; }
        protected virtual void ConfigureWeldPartsPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> PaintCarriersPerHour { get; set; }
        protected virtual void ConfigurePaintCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> UnloadCarriersPerHour { get; set; }
        protected virtual void ConfigureUnloadCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> PartsPerCarrier { get; set; }
        protected virtual void ConfigurePartsPerCarrier(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        #endregion

        #region Children

        public FrameTieChannel Channel { get; set; }
        protected virtual void ConfigureChannel(FrameTieChannel child)
        {
            child.FrameTieType.Define(Type);
            child.ChannelSize.Define(ChannelSize);
            child.Length.Define(ChannelLength);
            child.Position.Define(() => Helper.Vxyz(-FrontEndPlate.Thickness.Value, -child.ChannelWidth.Value/2, child.ChannelDepth.Value/2));
            child.Rotation.Define(() => Quaternion.CreateFromYawPitchRoll(Helper.DegToRad(-90), 0, Helper.DegToRad(180)));
        }

        public FrameTieFourHoleEndPlate FrontEndPlate { get; set; }
        protected virtual void ConfigureFrontEndPlate(FrameTieFourHoleEndPlate child)
        {
            child.FrameTieType.Define(Type);
            child.Thickness.Define(0.1875f);
            child.Width.Define(PlateWidth);
            child.Height.Define(10);
            child.HoleDiameter.Define(0.5625f);
            child.HoleVerticalOffset.Define(1);
            child.HoleHorizOffset.Define(0.875f);
            child.Position.Define(() => Helper.Vy(EndPlateVerticalOffset.Value));
        }

        public FrameTieFourHoleEndPlate RearEndPlate { get; set; }
        protected virtual void ConfigureRearEndPlate(FrameTieFourHoleEndPlate child)
        {
            child.FrameTieType.Define(Type);
            child.Thickness.Define(0.1875f);
            child.Width.Define(PlateWidth);
            child.Height.Define(10);
            child.HoleDiameter.Define(0.5625f);
            child.HoleVerticalOffset.Define(1);
            child.HoleHorizOffset.Define(0.875f);
            child.Position.Define(() => Helper.Vxy(-OverallLength.Value, EndPlateVerticalOffset.Value));
            child.Rotation.Define(() => Helper.CreateRotation(Vector3.UnitY, 180));
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(Colors.Blue);
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(false);
        }

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define("WeldedHDFrameTie");
        }

        #endregion

        #region IBomPart Overrides

        protected virtual void ConfigurePartWeight(Rule<double> rule)
        {
            rule.Define(() => this.GetPartWeightWithoutPaint());
        }

        #endregion

        #region Graphics

        //public UCS UCS { get; set; }
        //protected virtual void ConfigureUCS(UCS child)
        //{
        //    child.Diameter.Define(0.0125f);
        //    child.Position.Define(() => (Helper.Vy(0)));
        //}

        #endregion

        #region Protected Methods

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureChannelRules();
            this.ConfigureRevisablePart();
        }

        #endregion
    }
}
