﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;
using Ruler.Cpq;
using Ruler.Rules.Graphics;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class HeavyHorizontalWeldPlate : AdvancePart, IRevisablePart, IBomPart, Ruler.Cpq.IBomPart
    {
        #region Constructors

        public HeavyHorizontalWeldPlate(ModelContext modelContext) : base(modelContext) { }

        public HeavyHorizontalWeldPlate(string name, Part parent) : base(name, parent) { }

        #endregion

        #region Rules

        public Rule<float> ChannelWidth { get; set; }
        protected virtual void ConfigureChannelWidth(Rule<float> rule)
        {
            rule.Define(3);
        }

        [CadRule(CadRuleUsageType.Parameter)]
        public Rule<float> OutsideWidth { get; set; }
        protected virtual void ConfigureOutsideWidth(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (ChannelWidth.Value <= 3)
                    return 2.625f;
                else if (ChannelWidth.Value <= 4)
                    return 3.625f;
                else
                    return 4.625f;
            });
        }

        [CadRule(CadRuleUsageType.Parameter)]
        public Rule<float> InsideWidth { get; set; }
        protected virtual void ConfigureInsideWidth(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (ChannelWidth.Value <= 3)
                    return 1.5625f;
                else if (ChannelWidth.Value <= 4)
                    return 2.5f;
                else
                    return 3.375f;
            });
        }

        [CadRule(CadRuleUsageType.Parameter)]
        public Rule<float> Length { get; set; }
        protected virtual void ConfigureLength(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (ChannelWidth.Value <= 3)
                    return 1.875f;
                else if (ChannelWidth.Value <= 4)
                    return 2.0625f;
                else
                    return 2.1875f;
            });
        }

        public Rule<float> Thickness { get; set; }
        protected virtual void ConfigureThickness(Rule<float> rule)
        {
            rule.Define(0.25f);
        }

        #endregion

        #region IRevisablePartImplementation

        public FactoryRevisionRule<string> FactoryRevision { get; set; }

        #endregion

        #region IBomPart Implementation

        public Rule<string> DatabasePartNumber { get; set; }
        public PartNumberRule<string> PartNumber { get; set; }
        public Rule<string> PartNumberPrefix { get; set; }
        public Rule<string> Description { get; set; }
        public Rule<string> BOMDescription { get; set; }
        public Rule<string> BOMDescription1 { get; set; }
        public Rule<string> BOMDescription2 { get; set; }
        public Rule<string> BOMDescription3 { get; set; }
        public Rule<string> BOMDescription4 { get; set; }
        public Rule<string> BOMDescription5 { get; set; }
        public Rule<string> BOMDescription6 { get; set; }
        public Rule<string> BOMDescription7 { get; set; }
        public Rule<string> BOMDescription8 { get; set; }
        public Rule<string> BOMDescription9 { get; set; }
        public Rule<string> BOMDescription10 { get; set; }
        public Rule<string> BOMDescription11 { get; set; }
        public Rule<string> Color { get; set; }
        public ReadOnlyRule<AdvanceColor> AdvanceColor { get; set; }
        public ReadOnlyRule<string> ColorDescription { get; set; }
        public StorageConfiguratorRepository Repository { get; set; }
        public Rule<string> ColorBOMName { get; set; }
        public ReadOnlyRule<string> ColorShortDescription { get; set; }

        public Rule<double> PartWeight { get; set; }
        public Rule<double> PartPaintWeight { get; set; }
        public Rule<float> WeightPerGallon { get; set; }
        public CsiRule<double> PaintCostPerGallon { get; set; }
        public Rule<float> PaintCost { get; set; }

        #endregion

        #region CPQ.IBomPart Implementation

        //This is the description used only by the BOM Summary.
        public Rule<string> BOMUIDescription { get; set; }
        protected virtual void ConfigureBOMUIDescription(Rule<string> rule)
        {
            rule.Define(() =>
            {
                return $"Hvy Horiz Weld Plate({Helper.fractionString(Thickness.Value, feetToo: false)} x " +
                $"{Helper.fractionString(OutsideWidth.Value, feetToo: false)} x " +
                $"{Helper.fractionString(Length.Value, feetToo: false)})";
            });
        }

        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {
            child.PartNumber.Define(PartNumber);
            child.Description.Define(BOMUIDescription);
            child.MaterialPartNumber.Define(MaterialPartNumber);
            child.MaterialQty.Define(() => OutsideWidth.Value * Length.Value / 144);
            child.ItemMaterialCost.Define(() => child.ItemRawMaterialWeight.Value * child.MaterialUnitCost.Value);
        }

        #endregion

        #region Material Rules

        public Rule<string> MaterialPartNumber { get; set; }
        protected virtual void ConfigureMaterialPartNumber(Rule<string> rule)
        {
            rule.Define(default(string));
        }

        #endregion

        #region Labor Rules

        public Rule<double> PartCarriersPerHour { get; set; }
        protected virtual void ConfigurePartCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(Colors.Blue);
        }

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define("HeavyHorizontalWeldPlate");
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(false);
        }

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureBomPartRules();
            this.ConfigureRevisablePart();
        }

        #endregion

        #region Graphics

        public Block Block { get; set; }
        protected virtual void ConfigureBlock(Block geo)
        {
            geo.Width.Define(Length);
            geo.Height.Define(Thickness);
            geo.Depth.Define(OutsideWidth);
            geo.Position.Define(() => Helper.Vxy(-geo.Width.Value/2, geo.Height.Value / 2));
        }

        //public UCS UCS { get; set; }
        //protected virtual void ConfigureUCS(UCS child)
        //{
        //    child.Diameter.Define(0.125f);
        //    child.Position.Define(() => (Helper.Vy(0)));
        //}

        #endregion

    }
}
