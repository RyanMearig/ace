﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ruler.Rules;
using Ruler.Rules.Validation;

namespace StorageConfigurator.Rules
{
    public interface IFrameUprightData : IBasePlateData, IAnchorData
    {
        Rule<string> UprightType { get; set; }
        Rule<bool> Slanted { get; set; }
        Rule<float> SlantElev { get; set; }
        Rule<float> SlantInset { get; set; }
        Rule<float> SlantInsetMax { get; set; }
        Rule<float> BendToFirstHorizOffset { get; set; }
        Rule<string> UprightColumnSize { get; set; }
        Rule<float> FootplateWidth { get; set; }
        Rule<float> FootplateThickness { get; set; }
        Rule<float> HoleStartPunch { get; set; }

        Rule<string> FrontPostProtector { get; set; }
        Rule<float> FrontPostProtectorHeight { get; set; }
        Rule<bool> HasFrontDoubler { get; set; }
        Rule<float> FrontDoublerHeight { get; set; }
        Rule<float> FrontDoublerHeightMin { get; set; }
        Rule<string> FrontFootPlateType { get; set; }

        Rule<bool> HasRearDoubler { get; set; }
        Rule<float> RearDoublerHeight { get; set; }
        Rule<float> RearDoublerHeightMin { get; set; }
        Rule<string> RearPostProtector { get; set; }
        Rule<float> RearPostProtectorHeight { get; set; }
        Rule<string> RearFootPlateType { get; set; }
        Rule<int> FrontAnchorQty { get; set; }
        Rule<int> RearAnchorQty { get; set; }
        Rule<string> FrontAnchorSize { get; set; }
        Rule<string> RearAnchorSize { get; set; }
        Rule<string> FrontAnchorType { get; set; }
        Rule<string>FrontBullnoseAnchorType { get; set; }
        Rule<string> RearAnchorType { get; set; }
        Rule<string> RearBullnoseAnchorType { get; set; }
        Rule<string> Location { get; set; }
        Rule<float> MaxHeight { get; set; }
    }

    public static class IFrameUprightDataExtensions
    {
        public static void ConfigureFrameUprightDataRules(this IFrameUprightData comp)
        {
            comp.ConfigureBasePlateDataRules();
            comp.ConfigureAnchorDataRules();

            comp.UprightType.Define("S");
            comp.UprightType.AddToUI(ui =>
            {
                ui.ChoiceList.Define(new List<ChoiceListItem>()
                {
                    new ChoiceListItem("S", @"Standard"),
                    new ChoiceListItem("H", @"Heavy"),
                    new ChoiceListItem("BH", @"Bolt on Heavy"),
                    new ChoiceListItem("BTH", @"Bottom Two Heavy"),
                    new ChoiceListItem("D", @"Double Heavy"),
                    new ChoiceListItem("T", @"Tippmann"),
                    new ChoiceListItem("LS", @"Slant Leg 12x36(welded)"),
                    new ChoiceListItem("L", @"Slant Leg 12x56(welded)"),
                    new ChoiceListItem("B", @"Bent Leg")
                });
            });
            comp.UprightType.ValueSet += (sender, e) =>
            {
                comp.HasFrontDoubler.Reset();
                comp.HasRearDoubler.Reset();

                if (e.OldValue == "B")
                {
                    comp.SlantElev.Reset();
                    comp.SlantInset.Reset();
                }
                    
                if (e.NewValue == "B" || e.NewValue == "L" || e.NewValue == "LS")
                    comp.FrontPostProtector.Reset();
            };

            comp.UprightColumnSize.Define("C3x3.5");
            comp.UprightColumnSize.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() =>
                {
                    if (comp.UprightType.Value != "B")
                        return new List<ChoiceListItem>()
                            {
                                new ChoiceListItem("C3x3.5", "A-C3x3.5#"),
                                new ChoiceListItem("C3x4.1", "B-C3x4.1#"),
                                new ChoiceListItem("C4x4.5", "C-C4x4.5#"),
                                new ChoiceListItem("C4x5.4", "D-C4x5.4#"),
                                new ChoiceListItem("C5x6.1", "EL-C5x6.1#"),
                                new ChoiceListItem("C5x6.7", "E-C5x6.7#")
                            };
                    else
                        return new List<ChoiceListItem>()
                            {
                                new ChoiceListItem("C3x3.5", "A-C3x3.5#"),
                                new ChoiceListItem("C3x4.1", "B-C3x4.1#"),
                                new ChoiceListItem("C4x4.5", "C-C4x4.5#"),
                                new ChoiceListItem("C4x5.4", "D-C4x5.4#")
                            };
                });
            });

            comp.FootplateWidth.Define(10);
            comp.FootplateWidth.AddToUI();

            comp.FootplateThickness.Define(0.375f);
            comp.FootplateThickness.AddToUI();

            comp.FrontPostProtector.Define("none");
            comp.FrontPostProtector.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() => comp.PostProtectorChoices.Value);
                ui.ReadOnly.Define(() => (comp.UprightType.Value == "B" || comp.UprightType.Value == "L" || comp.UprightType.Value == "LS"));
            });

            comp.FrontPostProtectorHeight.Define(() => comp.FrontPostProtector.Value == "bullnose" ? 3.75f : 12);
            comp.FrontPostProtectorHeight.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => comp.FrontPostProtector.Value == "none");
                ui.ChoiceList.Define(() =>
                {
                    if (comp.FrontPostProtector.Value == "bullnose")
                    {
                        return new List<ChoiceListItem>()
                            {
                                new ChoiceListItem(2.75, "3"),
                                new ChoiceListItem(3.75, "4"),
                                new ChoiceListItem(4.75, "5"),
                                new ChoiceListItem(5.75, "6"),
                                new ChoiceListItem(7.75, "8"),
                            };
                    }
                    else
                    {
                        return new List<ChoiceListItem>();
                    }
                });
            });

            comp.HasFrontDoubler.Define(() => (comp.UprightType.Value == "T" || comp.UprightType.Value == "B" || comp.UprightType.Value == "L" || comp.UprightType.Value == "LS") ? true : false);
            comp.HasFrontDoubler.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => (comp.UprightType.Value == "T" || comp.UprightType.Value == "B" || comp.UprightType.Value == "D" || comp.UprightType.Value == "L" || comp.UprightType.Value == "LS"));
            });

            comp.FrontDoublerHeightMin.Define(() =>
            {
                if (comp.UprightType.Value == "B")
                    return comp.SlantElev.Value + 48;
                else if (comp.UprightType.Value == "D")
                    return 8;
                else
                    return 12;
            });

            comp.FrontDoublerHeight.Define(12);
            comp.FrontDoublerHeight.Min(comp.FrontDoublerHeightMin);
            comp.FrontDoublerHeight.MultipleOf(4);
            comp.FrontDoublerHeight.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !comp.HasFrontDoubler.Value);
            });

            comp.FrontFootPlateType.Define(() =>
            {
                BasePlateTypeData bpData = comp.AllBasePlateData.Value.FirstOrDefault(d =>
                {
                    return (d.HasDoubler == comp.HasFrontDoubler.Value &&
                        d.PostProtector == comp.FrontPostProtector.Value &&
                        d.AnchorQty == comp.FrontAnchorQty.Value);
                });

                if (bpData != null)
                    return bpData.BasePlateType;
                else
                    return comp.BasePlateTypeChoiceList.Value.First().Text;
            });
            comp.FrontFootPlateType.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() => comp.BasePlateTypeChoiceList.Value);
            });

            comp.HasRearDoubler.Define(() => (comp.UprightType.Value == "T" || comp.UprightType.Value == "D"));
            comp.HasRearDoubler.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => (comp.UprightType.Value == "T" || comp.UprightType.Value == "D"));
            });

            comp.RearDoublerHeightMin.Define(() => comp.FrontDoublerHeightMin.Value);

            comp.RearDoublerHeight.Define(12);
            comp.RearDoublerHeight.Min(comp.RearDoublerHeightMin);
            comp.RearDoublerHeight.MultipleOf(4);
            comp.RearDoublerHeight.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !comp.HasRearDoubler.Value);
            });

            comp.RearPostProtector.Define("none");
            comp.RearPostProtector.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() => comp.PostProtectorChoices.Value);
            });

            comp.RearPostProtectorHeight.Define(() => comp.RearPostProtector.Value == "bullnose" ? 3.75f : 12);
            comp.RearPostProtectorHeight.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => comp.RearPostProtector.Value == "none");
                ui.ChoiceList.Define(() =>
                {
                    if (comp.RearPostProtector.Value == "bullnose")
                    {
                        return new List<ChoiceListItem>()
                            {
                                new ChoiceListItem(2.75, "3"),
                                new ChoiceListItem(3.75, "4"),
                                new ChoiceListItem(4.75, "5"),
                                new ChoiceListItem(5.75, "6"),
                                new ChoiceListItem(7.75, "8"),
                            };
                    }
                    else
                    {
                        return new List<ChoiceListItem>();
                    }
                });
            });

            comp.RearFootPlateType.Define(() =>
            {
                BasePlateTypeData bpData = comp.AllBasePlateData.Value.FirstOrDefault(d =>
                {
                    return (d.HasDoubler == comp.HasRearDoubler.Value &&
                        d.PostProtector == comp.RearPostProtector.Value &&
                        d.AnchorQty == comp.RearAnchorQty.Value);
                });

                if (bpData != null)
                    return bpData.BasePlateType;
                else
                    return comp.BasePlateTypeChoiceList.Value.First().Text;
            });
            comp.RearFootPlateType.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() => comp.BasePlateTypeChoiceList.Value);
            });

            comp.FrontAnchorQty.Define(2);
            comp.FrontAnchorQty.AddToUI(ui =>
            {
                ui.ChoiceList.Define(new List<ChoiceListItem>()
                {
                    new ChoiceListItem(1),
                    new ChoiceListItem(2),
                    new ChoiceListItem(4)
                });
            });

            comp.RearAnchorQty.Define(2);
            comp.RearAnchorQty.AddToUI(ui =>
            {
                ui.ChoiceList.Define(new List<ChoiceListItem>()
                {
                    new ChoiceListItem(1),
                    new ChoiceListItem(2),
                    new ChoiceListItem(4)
                });
            });

            comp.FrontAnchorSize.Define(DefaultAnchorSize);
            comp.FrontAnchorSize.AddToUI(ui =>
            {
                ui.ChoiceList.Define(new List<ChoiceListItem>()
                {
                    new ChoiceListItem("0.5", @"1/2"""),
                    new ChoiceListItem("0.625", @"5/8"""),
                    new ChoiceListItem("0.75", @"3/4""")
                });
            });

            comp.FrontAnchorType.Define(() =>
            {
                return comp.FrontAnchorType.UIMetadata.ChoiceList.Value.First().Value.ToString();
            });
            comp.FrontAnchorType.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() => comp.GetAnchorChoiceList(float.Parse(comp.FrontAnchorSize.Value)));
            });

            comp.FrontBullnoseAnchorType.Define(() =>
            {
                return comp.FrontBullnoseAnchorType.UIMetadata.ChoiceList.Value.First().Value.ToString();
            });
            comp.FrontBullnoseAnchorType.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() => comp.AllAnchorTypesChoiceList.Value);
            });

            comp.RearAnchorSize.Define(DefaultAnchorSize);
            comp.RearAnchorSize.AddToUI(ui =>
            {
                ui.ChoiceList.Define(new List<ChoiceListItem>()
                {
                    new ChoiceListItem("0.5", @"1/2"""),
                    new ChoiceListItem("0.625", @"5/8"""),
                    new ChoiceListItem("0.75", @"3/4""")
                });
            });

            comp.RearAnchorType.Define(() =>
            {
                return comp.RearAnchorType.UIMetadata.ChoiceList.Value.First().Value.ToString();
            });
            comp.RearAnchorType.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() => comp.GetAnchorChoiceList(float.Parse(comp.RearAnchorSize.Value)));
            });

            comp.RearBullnoseAnchorType.Define(() =>
            {
                return comp.RearBullnoseAnchorType.UIMetadata.ChoiceList.Value.First().Value.ToString();
            });
            comp.RearBullnoseAnchorType.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() => comp.AllAnchorTypesChoiceList.Value);
            });

            comp.BendToFirstHorizOffset.Define(3);

            comp.Slanted.Define(() => (comp.UprightType.Value == "B" || comp.UprightType.Value == "L" || comp.UprightType.Value == "LS"));
            comp.Slanted.AddToUI();

            comp.SlantElev.Define(() =>
            {
                if (comp.UprightType.Value == "L")
                    return 56;
                else if (comp.UprightType.Value == "LS")
                    return 36;
                else
                    return 27;
            });
            comp.SlantElev.Min(3);
            comp.SlantElev.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => comp.UprightType.Value != "B");
                ui.ChoiceList.Define(() =>
                {
                    if (comp.UprightType.Value == "B")
                    {
                        return new List<ChoiceListItem>()
                        {
                            new ChoiceListItem(27),
                            new ChoiceListItem(31),
                            new ChoiceListItem(35),
                            new ChoiceListItem(39),
                            new ChoiceListItem(43),
                            new ChoiceListItem(47),
                            new ChoiceListItem(51),
                            new ChoiceListItem(55),
                            new ChoiceListItem(59),
                            new ChoiceListItem(63),
                            new ChoiceListItem(67),
                            new ChoiceListItem(71),
                            new ChoiceListItem(75),
                            new ChoiceListItem(79),
                            new ChoiceListItem(83),
                            new ChoiceListItem(87),
                            new ChoiceListItem(91),
                            new ChoiceListItem(95)
                        };
                    }
                    else
                        return null;
                });
            });
            //comp.SlantElev.ValueSet += (sender, e) =>
            //{
            //    comp.SlantInset.Reset();
            //};

            comp.SlantInsetMax.Define(() =>
            {
                if (comp.UprightType.Value != "B")
                    return 100;
                else if (comp.UprightColumnSize.Value.Contains("C3"))
                {
                    if (comp.SlantElev.Value <= 27)
                        return 8;
                    else if (comp.SlantElev.Value <= 31)
                        return 10;
                    else if (comp.SlantElev.Value <= 35)
                        return 12;
                    else if (comp.SlantElev.Value <= 39)
                        return 14;
                    else if (comp.SlantElev.Value <= 43)
                        return 16;
                    else if (comp.SlantElev.Value <= 47)
                        return 18;
                    else if (comp.SlantElev.Value <= 51)
                        return 20;
                    else if (comp.SlantElev.Value <= 55)
                        return 22;
                    else if (comp.SlantElev.Value <= 59)
                        return 24;
                    else if (comp.SlantElev.Value <= 63)
                        return 26;
                    else if (comp.SlantElev.Value <= 67)
                        return 28;
                    else if (comp.SlantElev.Value <= 71)
                        return 29;
                    else if (comp.SlantElev.Value <= 75)
                        return 31;
                    else if (comp.SlantElev.Value <= 79)
                        return 33;
                    else if (comp.SlantElev.Value <= 83)
                        return 35;
                    else if (comp.SlantElev.Value <= 87)
                        return 37;
                    else if (comp.SlantElev.Value <= 91)
                        return 39;
                    else if (comp.SlantElev.Value <= 95)
                        return 41;
                    else
                        return 0;
                }
                else
                {
                    if (comp.SlantElev.Value <= 27)
                        return 6;
                    else if (comp.SlantElev.Value <= 31)
                        return 7;
                    else if (comp.SlantElev.Value <= 35)
                        return 8;
                    else if (comp.SlantElev.Value <= 39)
                        return 10;
                    else if (comp.SlantElev.Value <= 43)
                        return 11;
                    else if (comp.SlantElev.Value <= 47)
                        return 12;
                    else if (comp.SlantElev.Value <= 51)
                        return 14;
                    else if (comp.SlantElev.Value <= 55)
                        return 15;
                    else if (comp.SlantElev.Value <= 59)
                        return 17;
                    else if (comp.SlantElev.Value <= 63)
                        return 18;
                    else if (comp.SlantElev.Value <= 67)
                        return 19;
                    else if (comp.SlantElev.Value <= 71)
                        return 21;
                    else if (comp.SlantElev.Value <= 75)
                        return 22;
                    else if (comp.SlantElev.Value <= 79)
                        return 23;
                    else if (comp.SlantElev.Value <= 83)
                        return 25;
                    else if (comp.SlantElev.Value <= 87)
                        return 26;
                    else if (comp.SlantElev.Value <= 91)
                        return 28;
                    else if (comp.SlantElev.Value <= 95)
                        return 29;
                    else
                        return 0;
                }
            });

            comp.SlantInset.Define(() =>
            {
                if (comp.UprightType.Value == "L" || comp.UprightType.Value == "LS")
                    return 12;
                else if (comp.UprightType.Value == "B")
                {
                    if (comp.UprightColumnSize.Value.Contains("C3"))
                    {
                        if (comp.SlantElev.Value < 39)
                            return 8;
                        else if (comp.SlantElev.Value < 51)
                            return 12;
                        else if (comp.SlantElev.Value < 63)
                            return 17;
                        else if (comp.SlantElev.Value < 75)
                            return 20;
                        else if (comp.SlantElev.Value < 87)
                            return 25;
                        else
                            return 29;
                    }
                    else
                    {
                        if (comp.SlantElev.Value < 39)
                            return 6;
                        else if (comp.SlantElev.Value < 51)
                            return 10;
                        else if (comp.SlantElev.Value < 63)
                            return 13;
                        else if (comp.SlantElev.Value < 75)
                            return 16;
                        else if (comp.SlantElev.Value < 87)
                            return 19;
                        else
                            return 22;
                    }
                }
                else
                    return 0;
            });
            comp.SlantInset.Min(3);
            comp.SlantInset.Max(comp.SlantInsetMax);
            comp.SlantInset.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => comp.UprightType.Value != "B");
            });

            comp.Location.Define("GA");
            comp.Location.AddToUI(ui =>
            {
                ui.ChoiceList.Define(new List<ChoiceListItem>()
                {
                    new ChoiceListItem("GA", "GA"),
                    new ChoiceListItem("UT", "UT")
                });
            });

            comp.MaxHeight.Define(() =>
            {
                if (comp.Location.Value == "UT")
                    return 408;
                else
                    return 432;
            });

        }

        const string DefaultAnchorSize = "0.5";

        public static string GetBtmHorizontalAbbreviation(this IFrameUprightData comp)
        {
            var type = comp.UprightType.Value;
            switch(type)
            {
                case "S":
                    return "STD";
                case "BH":
                    return "BHVY";
                case "D":
                case "T":
                    return "DBLHVY";
                default:
                    return "HVY";
            }
        }
    }

}
