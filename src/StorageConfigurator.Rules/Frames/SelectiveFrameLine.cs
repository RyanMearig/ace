﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using Ruler.Rules;
using Ruler.Rules.Graphics;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class SelectiveFrameLine : FrameLine
    {
        #region Constructors

        public SelectiveFrameLine(ModelContext modelContext, StorageConfiguratorRepository repo) : base(modelContext, repo) { }

        public SelectiveFrameLine(string name, Part parent, StorageConfiguratorRepository repo) : base(name, parent, repo) { }

        #endregion

        #region Rules

        public Rule<string> FrontFrameTypeName { get; set; }
        protected virtual void ConfigureFrontFrameTypeName(Rule<string> rule)
        {
            rule.Define(() => FrameTypeChoices.Value?.FirstOrDefault()?.Value.ToString() ?? "");
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() =>
                {
                    if (FrameTypeChoices.Value.Any())
                        return FrameTypeChoices.Value;
                    else
                        return new List<ChoiceListItem>();
                });
            });
        }

        [UIRule]
        public Rule<float> Space { get; set; }
        protected virtual void ConfigureSpace(Rule<float> rule)
        {
            rule.Define(() => IsBackToBack.Value ? 12 : 0);
            rule.Min(6);
        }

        public Rule<string> RearFrameTypeName { get; set; }
        protected virtual void ConfigureRearFrameTypeName(Rule<string> rule)
        {
            rule.Define(() => IsBackToBack.Value ? FrontFrameTypeName.Value : "");
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() =>
                {
                    if (IsBackToBack.Value && FrameTypeChoices.Value.Any())
                        return FrameTypeChoices.Value;
                    else
                        return new List<ChoiceListItem>();
                });
                ui.ReadOnly.Define(() => !IsBackToBack.Value);
            });
        }

        public Rule<List<ChoiceListItem>> FrameTypeChoices { get; set; }
        protected virtual void ConfigureFrameTypeChoices(Rule<List<ChoiceListItem>> rule)
        {
            rule.Define(() =>
            {
                List<ChoiceListItem> retList = new List<ChoiceListItem>();

                if (AllAvailableFrameTypes.Value != null)
                {
                    AllAvailableFrameTypes.Value.ForEach(b => retList.Add(new ChoiceListItem(b.Path, b.FrameTypeName.Value)));
                }

                return retList;
            });
        }

        public Rule<List<FrameType>> AllAvailableFrameTypes { get; set; }
        protected virtual void ConfigureAllAvailableFrameTypes(Rule<List<FrameType>> rule)
        {
            rule.Define(() =>
            {
                List<FrameType> retVal = new List<FrameType>();

                if (StorageSystemComp.Value != null)
                {
                    if (StorageSystemComp.Value.FrameTypes.FrameTypeList.Value.Any())
                        retVal = StorageSystemComp.Value.FrameTypes.FrameTypeList.Value;
                }

                return retVal;
            });
        }

        public Rule<Dictionary<string, float>> ShortestFrameHorizBraceDict { get; set; }
        protected virtual void ConfigureShortestFrameHorizBraceDict(Rule<Dictionary<string, float>> rule)
        {
            rule.Define(() =>
            {
                var retVal = new Dictionary<string, float>();
                if (!IsBackToBack.Value) return retVal;

                if (FrontFrame.Value.Height.Value > RearFrame.Value.Height.Value)
                {
                    if (RearFrame.Value.IsDoubleDeep.Value)
                    {
                        var frame = (DoubleDeepFrameType)RearFrame.Value;
                        frame?.RearFrame?.Frame?.HorizontalBrace?.ToList().ForEach(b => retVal.Add(b.Path, b.OffsetFromBottom.Value));
                    }
                    else
                    {
                        var frame = (SelectiveFrameType)RearFrame.Value;
                        frame?.Frame.Frame?.HorizontalBrace?.ToList().ForEach(b => retVal.Add(b.Path, b.OffsetFromBottom.Value));
                    }
                }
                else
                {
                    if (FrontFrame.Value.IsDoubleDeep.Value)
                    {
                        var frame = (DoubleDeepFrameType)FrontFrame.Value;
                        frame?.RearFrame?.Frame?.HorizontalBrace?.ToList().ForEach(b => retVal.Add(b.Path, b.OffsetFromBottom.Value));
                    }
                    else
                    {
                        var frame = (SelectiveFrameType)FrontFrame.Value;
                        frame?.Frame.Frame?.HorizontalBrace?.ToList().ForEach(b => retVal.Add(b.Path, b.OffsetFromBottom.Value));
                    }
                }

                return retVal;
            });
        }

        public Rule<bool> FrontFrameIsDoubleDeep { get; set; }
        protected virtual void ConfigureFrontFrameIsDoubleDeep(Rule<bool> rule)
        {
            rule.Define(() => FrontFrame.Value?.IsDoubleDeep.Value ?? false);
        }

        [CadRule(CadRuleUsageType.Property, "IsDoubleDeep")]
        public Rule<string> FrontFrameIsDoubleDeepProp { get; set; }
        protected virtual void ConfigureFrontFrameIsDoubleDeepProp(Rule<string> rule)
        {
            rule.Define(() => FrontFrameIsDoubleDeep.Value.ToString());
        }

        public Rule<bool> FrontFrameIsDoubleDeepPost { get; set; }
        protected virtual void ConfigureFrontFrameIsDoubleDeepPost(Rule<bool> rule)
        {
            rule.Define(() => FrontFrame.Value?.IsDoubleDeepPost.Value ?? false);
        }

        [CadRule(CadRuleUsageType.Property, "IsDoubleDeepPost")]
        public Rule<string> FrontFrameIsDoubleDeepPostProp { get; set; }
        protected virtual void ConfigureFrontFrameIsDoubleDeepPostProp(Rule<string> rule)
        {
            rule.Define(() => FrontFrameIsDoubleDeepPost.Value.ToString());
        }

        public Rule<float> FrontFrameUprightDepth { get; set; }
        protected virtual void ConfigureFrontFrameUprightDepth(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (FrontFrameIsDoubleDeep.Value)
                {
                    return ((DoubleDeepFrameType)FrontFrame.Value).FrontFrame.Frame.Depth.Value;
                }
                return ((SelectiveFrameType)FrontFrame.Value).Frame.Frame.Depth.Value;
            });
        }

        public Rule<bool> RearFrameIsDoubleDeep { get; set; }
        protected virtual void ConfigureRearFrameIsDoubleDeep(Rule<bool> rule)
        {
            rule.Define(() => RearFrame.Value?.IsDoubleDeep.Value ?? IsBackToBack.Value);
        }

        public Rule<string> RearFrameIsDoubleDeepProp { get; set; }
        protected virtual void ConfigureRearFrameIsDoubleDeepProp(Rule<string> rule)
        {
            rule.Define(() => RearFrameIsDoubleDeep.Value.ToString());
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(IsBackToBack);
                cad.CadRuleUsageType = CadRuleUsageType.Property;
                cad.NameInCad = "RearIsDoubleDeep";
            });
        }

        public Rule<bool> RearFrameIsDoubleDeepPost { get; set; }
        protected virtual void ConfigureRearFrameIsDoubleDeepPost(Rule<bool> rule)
        {
            rule.Define(() => RearFrame.Value?.IsDoubleDeepPost.Value ?? IsBackToBack.Value);
        }

        public Rule<string> RearFrameIsDoubleDeepPostProp { get; set; }
        protected virtual void ConfigureRearFrameIsDoubleDeepPostProp(Rule<string> rule)
        {
            rule.Define(() => RearFrameIsDoubleDeepPost.Value.ToString());
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(IsBackToBack);
                cad.CadRuleUsageType = CadRuleUsageType.Property;
                cad.NameInCad = "RearIsDoubleDeepPost";
            });
        }

        public Rule<float> RearFrameUprightDepth { get; set; }
        protected virtual void ConfigureRearFrameUprightDepth(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (RearFrame.Value == null) return 0;
                if (RearFrameIsDoubleDeep.Value)
                {
                    return ((DoubleDeepFrameType)RearFrame.Value).FrontFrame.Frame.Depth.Value;
                }
                return ((SelectiveFrameType)RearFrame.Value).Frame.Frame.Depth.Value;
            });
        }

        public Rule<float> RearFrameXOffset { get; set; }
        protected virtual void ConfigureRearFrameXOffset(Rule<float> rule)
        {
            rule.Define(() => RearFrame.Value != null ? -(FrontFrame.Value.OverallDepth.Value + Space.Value + RearFrame.Value.OverallDepth.Value) : 0);
        }

        #endregion

        #region Frameline Overrides

        protected override void ConfigureIsSelectiveFrameLine(Rule<bool> rule)
        {
            rule.Define(true);
        }

        protected override void ConfigureIsBackToBack(Rule<bool> rule)
        {
            rule.Define(false);
            rule.AddToUI();
            rule.AddToCAD(cad => cad.CadRuleUsageType = CadRuleUsageType.Property);
            rule.ValueSet += (sender, e) =>
            {
                RearFrameTypeName.Reset();
            };
        }

        protected override void ConfigureWidth(ReadOnlyRule<float> rule)
        {
            rule.Define(() => FrontFrame.Value.Width.Value);
        }

        protected override void ConfigureOverallDepth(ReadOnlyRule<float> rule)
        {
            rule.Define(() => IsBackToBack.Value ? FrontFrame.Value.OverallDepth.Value + FrameTiesSpace.OverallDepth.Value + RearFrame.Value.OverallDepth.Value
                                                 : FrontFrame.Value.OverallDepth.Value);
        }

        protected override void ConfigureHeight(ReadOnlyRule<float> rule)
        {
            rule.Define(() => IsBackToBack.Value && FrontFrame.Value.Height.Value < RearFrame.Value.Height.Value ? RearFrame.Value.Height.Value
                                                 : FrontFrame.Value.Height.Value);
        }

        #endregion

        #region Children

        public Rule<FrameType> FrontFrame { get; set; }
        protected virtual void ConfigureFrontFrame(Rule<FrameType> rule)
        {
            rule.Define(() =>
            {
                FrameType retVal = null;

                if (FrameTypeChoices.Value.Any() &&
                FrameTypeChoices.Value.Any(p => p.Value.ToString() == FrontFrameTypeName.Value))
                {
                    retVal = AllAvailableFrameTypes.Value.FirstOrDefault(p => p.Path == FrontFrameTypeName.Value);
                }

                return retVal;
            });
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(() => FrontFrame.Value != null);
                cad.CadRuleUsageType = CadRuleUsageType.ChildProxy;
                cad.ChildProxyMatrixFactory = () => Matrix4x4.CreateTranslation(Helper.Vz(0));
            });
        }

        public ChildList<SelectiveFrameType> FrontFramesForGfx { get; set; }
        protected virtual void ConfigureFrontFramesForGfx(ChildList<SelectiveFrameType> list)
        {
            list.Qty.Define(() => FrontFrameIsDoubleDeep.Value ? 0 : 1);
            list.ConfigureChildren((child, index) =>
            {
                child.SelectiveFrameTypeMaster.Define(() => FrontFrame.Value as SelectiveFrameType);
            });
        }

        public ChildList<DoubleDeepFrameType> FrontDDFramesForGfx { get; set; }
        protected virtual void ConfigureFrontDDFramesForGfx(ChildList<DoubleDeepFrameType> list)
        {
            list.Qty.Define(() => FrontFrameIsDoubleDeep.Value ? 1 : 0);
            list.ConfigureChildren((child, index) =>
            {
                child.DoubleDeepFrameTypeMaster.Define(() => FrontFrame.Value as DoubleDeepFrameType);
            });
        }

        public FrameTiesSpace FrameTiesSpace { get; set; }
        protected virtual void ConfigureFrameTiesSpace(FrameTiesSpace child)
        {
            child.IsActive.Define(IsBackToBack);
            child.HasFrameTies.Define(IsBackToBack);
            child.UprightColumnSize.Define(() => FrontFrame.Value.UprightColumnSize.Value);
            child.FrontDoublerHeight.Define(() => FrontFrame.Value.HasRearDoubler.Value ? FrontFrame.Value.RearDoublerHeight.Value : 0);
            child.RearDoublerHeight.Define(() => RearFrame.Value.HasFrontDoubler.Value ? RearFrame.Value.FrontDoublerHeight.Value : 0);
            child.HorizBraceDict.Define(ShortestFrameHorizBraceDict);
            child.FrameTieLength.Define(Space);
            child.FrameTieLengthReadOnly.Define(true);
            child.Position.Define(() => Helper.Vx(-FrontFrame.Value.OverallDepth.Value));
        }

        public Rule<FrameType> RearFrame { get; set; }
        protected virtual void ConfigureRearFrame(Rule<FrameType> rule)
        {
            rule.Define(() =>
            {
                FrameType retVal = null;

                if (IsBackToBack.Value && FrameTypeChoices.Value.Any() &&
                FrameTypeChoices.Value.Any(p => p.Value.ToString() == RearFrameTypeName.Value))
                {
                    retVal = AllAvailableFrameTypes.Value.FirstOrDefault(p => p.Path == RearFrameTypeName.Value);
                }

                return retVal;
            });
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(() => IsBackToBack.Value && RearFrame.Value != null);
                cad.CadRuleUsageType = CadRuleUsageType.ChildProxy;
                cad.ChildProxyMatrixFactory = () => Matrix4x4.Transform(Matrix4x4.CreateTranslation(Helper.Vx(Math.Abs(RearFrameXOffset.Value))), Helper.CreateRotation(Vector3.UnitY, 180));
            });
        }

        public ChildList<SelectiveFrameType> RearFramesForGfx { get; set; }
        protected virtual void ConfigureRearFramesForGfx(ChildList<SelectiveFrameType> list)
        {
            list.Qty.Define(() => !IsBackToBack.Value || RearFrameIsDoubleDeep.Value ? 0 : 1);
            list.ConfigureChildren((child, index) =>
            {
                child.SelectiveFrameTypeMaster.Define(() => RearFrame.Value as SelectiveFrameType);
                child.Position.Define(() => Helper.Vx(RearFrameXOffset.Value));
                child.Rotation.Define(() => Helper.CreateRotation(Vector3.UnitY, 180));
            });
        }

        public ChildList<DoubleDeepFrameType> RearDDFramesForGfx { get; set; }
        protected virtual void ConfigureRearDDFramesForGfx(ChildList<DoubleDeepFrameType> list)
        {
            list.Qty.Define(() => IsBackToBack.Value && RearFrameIsDoubleDeep.Value ? 1 : 0);
            list.ConfigureChildren((child, index) =>
            {
                child.DoubleDeepFrameTypeMaster.Define(() => RearFrame.Value as DoubleDeepFrameType);
                child.Position.Define(() => Helper.Vx(RearFrameXOffset.Value));
                child.Rotation.Define(() => Helper.CreateRotation(Vector3.UnitY, 180));
            });
        }

        #endregion

        #region Graphics

        //public UCS UCS { get; set; }
        //protected virtual void ConfigureUCS(UCS child)
        //{
        //    child.Position.Define(() => (Helper.Vy(0)));
        //}

        #endregion


    }
}
