﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;
using Ruler.Cpq;
using Ruler.Rules.Graphics;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class FrameTieFourHoleEndPlate : AdvancePart, IRevisablePart, IBomPart, Ruler.Cpq.IBomPart
    {
        #region Constructors

        public FrameTieFourHoleEndPlate(ModelContext modelContext) : base(modelContext) { }

        public FrameTieFourHoleEndPlate(string name, Part parent) : base(name, parent) { }

        #endregion

        #region Rules

        public Rule<string> MaterialPartNumber { get; set; }
        protected virtual void ConfigureMaterialPartNumber(Rule<string> rule)
        {
            rule.Define(default(string));
        }

        [CadRule(CadRuleUsageType.Parameter)]
        public Rule<float> Width { get; set; }
        protected virtual void ConfigureWidth(Rule<float> rule)
        {
            rule.Define(2);
        }

        [CadRule(CadRuleUsageType.Parameter)]
        public Rule<float> Height { get; set; }
        protected virtual void ConfigureHeight(Rule<float> rule)
        {
            rule.Define(6);
        }

        [CadRule(CadRuleUsageType.Parameter)]
        public Rule<float> Thickness { get; set; }
        protected virtual void ConfigureThickness(Rule<float> rule)
        {
            rule.Define(0.1875f);
        }

        [CadRule(CadRuleUsageType.Parameter)]
        public Rule<float> HoleVerticalOffset { get; set; }
        protected virtual void ConfigureHoleVerticalOffset(Rule<float> rule)
        {
            rule.Define(1);
        }

        [CadRule(CadRuleUsageType.Parameter)]
        public Rule<float> HoleHorizOffset { get; set; }
        protected virtual void ConfigureHoleHorizOffset(Rule<float> rule)
        {
            rule.Define(0.375f);
        }

        [CadRule(CadRuleUsageType.Parameter)]
        public Rule<float> HoleDiameter { get; set; }
        protected virtual void ConfigureHoleDiameter(Rule<float> rule)
        {
            rule.Define(0.5625f);
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> Description { get; set; }
        protected virtual void ConfigureDescription(Rule<string> rule)
        {
            rule.Define(() => @"3/16"" x " + Width.Value.ToString() + @""" FLAT BAR");
        }

        public Rule<string> FrameTieType { get; set; }
        protected virtual void ConfigureFrameTieType(Rule<string> rule)
        {
            rule.Define(default(string));
        }

        #endregion

        #region IRevisablePartImplementation

        public FactoryRevisionRule<string> FactoryRevision { get; set; }

        #endregion

        #region IBomPart Implementation

        public Rule<string> DatabasePartNumber { get; set; }
        public PartNumberRule<string> PartNumber { get; set; }        
        public Rule<string> PartNumberPrefix { get; set; }
        public Rule<string> BOMDescription { get; set; }
        public Rule<string> BOMDescription1 { get; set; }
        public Rule<string> BOMDescription2 { get; set; }
        public Rule<string> BOMDescription3 { get; set; }
        public Rule<string> BOMDescription4 { get; set; }
        public Rule<string> BOMDescription5 { get; set; }
        public Rule<string> BOMDescription6 { get; set; }
        public Rule<string> BOMDescription7 { get; set; }
        public Rule<string> BOMDescription8 { get; set; }
        public Rule<string> BOMDescription9 { get; set; }
        public Rule<string> BOMDescription10 { get; set; }
        public Rule<string> BOMDescription11 { get; set; }
        public Rule<string> Color { get; set; }
        public ReadOnlyRule<AdvanceColor> AdvanceColor { get; set; }
        public ReadOnlyRule<string> ColorDescription { get; set; }
        public StorageConfiguratorRepository Repository { get; set; }
        public Rule<string> ColorBOMName { get; set; }
        public ReadOnlyRule<string> ColorShortDescription { get; set; }

        public Rule<double> PartWeight { get; set; }
        public Rule<double> PartPaintWeight { get; set; }
        public Rule<float> WeightPerGallon { get; set; }
        public CsiRule<double> PaintCostPerGallon { get; set; }
        public Rule<float> PaintCost { get; set; }

        #endregion

        #region CPQ.IBomPart Implementation

        //This is the description used only by the BOM Summary.
        public Rule<string> BOMUIDescription { get; set; }
        protected virtual void ConfigureBOMUIDescription(Rule<string> rule)
        {
            rule.Define(() =>
            {
                return $"Frame Tie End Plate({Helper.fractionString(Thickness.Value, feetToo: false)} x " +
                $"{Helper.fractionString(Width.Value, feetToo: false)} x " +
                $"{Helper.fractionString(Height.Value, feetToo: false)})";
            });
        }

        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {
            child.PartNumber.Define(PartNumber);
            child.Description.Define(BOMUIDescription);
            child.MaterialPartNumber.Define(MaterialPartNumber);
            child.MaterialQty.Define(() => Height.Value / 12);
            child.ItemMaterialCost.Define(() => child.ItemRawMaterialWeight.Value * child.MaterialUnitCost.Value);
        }

        #endregion

        #region Labor Rules

        public Rule<double> IronworkerPartCarriersPerHour { get; set; }
        protected virtual void ConfigureIronworkerPartCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> AccurpressPartCarriersPerHour { get; set; }
        protected virtual void ConfigureAccurpressPartCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(Colors.Blue);
        }

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define("FourHoleEndPlate");
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(false);
        }

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureRevisablePart();
            this.ConfigureBomPartRules();
        }

        #endregion

        #region Graphics

        public Block Block { get; set; }
        protected virtual void ConfigureBlock(Block geo)
        {
            geo.Width.Define(Thickness);
            geo.Height.Define(Height);
            geo.Depth.Define(Width);
            geo.Position.Define(() => Helper.Vxy(-geo.Width.Value/2, -geo.Height.Value / 2));
        }

        //public UCS UCS { get; set; }
        //protected virtual void ConfigureUCS(UCS child)
        //{
        //    child.Diameter.Define(0.0125f);
        //    child.Position.Define(() => (Helper.Vy(0)));
        //}

        #endregion

    }
}
