﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;
using Ruler.Cpq;
using Ruler.Rules.Graphics;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class RSFrameTie : FrameTieComponent, IAngle, IRevisablePart, Ruler.Cpq.IBomPart
    {
        #region Constructors

        public RSFrameTie(ModelContext modelContext, StorageConfiguratorRepository repo) : base(modelContext, repo) { }

        public RSFrameTie(string name, Part parent, StorageConfiguratorRepository repo) : base(name, parent, repo) { }

        #endregion

        #region Rules

        [CadRule(CadRuleUsageType.Parameter)]
        public Rule<float> Length { get; set; }
        protected virtual void ConfigureLength(Rule<float> rule)
        {
            rule.Define(OverallLength);
        }

        public Rule<double> Qty { get; set; }
        protected virtual void ConfigureQty(Rule<double> rule)
        {
            rule.Define(1);
            rule.AddToUI(ui =>
            {
                ui.Label.Define("Quantity");
                ui.ReadOnly.Define(true);
            });
        }

        public Rule<float> HoleHorizOffset { get; set; }
        protected virtual void ConfigureHoleHorizOffset(Rule<float> rule)
        {
            rule.Define(0.75f);
        }

        public Rule<float> HoleVertOffsetFromBottom { get; set; }
        protected virtual void ConfigureHoleVertOffsetFromBottom(Rule<float> rule)
        {
            rule.Define(0.625f);
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> AnglePartNumber { get; set; }
        protected virtual void ConfigureAnglePartNumber(Rule<string> rule)
        {
            rule.Define(() => AngleHeight.Value == 2 ? "P40099-1-RFS" : "P40099-RFS");
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> AngleMaterialDescription { get; set; }
        protected virtual void ConfigureAngleMaterialDescription(Rule<string> rule)
        {
            rule.Define(() => $"{Helper.fractionString(AngleThickness.Value)} x {Helper.fractionString(AngleHeight.Value)} x {Helper.fractionString(AngleWidth.Value)} ANGLE");
        }

        public Rule<float> EndFlangeExtension { get; set; }
        protected virtual void ConfigureEndFlangeExtension(Rule<float> rule)
        {
            rule.Define(0.0625f);
        }

        public Rule<float> SideFlangeTotalNotchDist { get; set; }
        protected virtual void ConfigureSideFlangeTotalNotchDist(Rule<float> rule)
        {
            rule.Define(0.8125f);
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<float> AngleFlatCutLength { get; set; }
        protected virtual void ConfigureAngleFlatCutLength(Rule<float> rule)
        {
            rule.Define(() => Length.Value + EndFlangeExtension.Value + (2 * AngleHeight.Value));
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<float> SideFlangeFlatCutLength { get; set; }
        protected virtual void ConfigureSideFlangeFlatCutLength(Rule<float> rule)
        {
            rule.Define(() => Length.Value - SideFlangeTotalNotchDist.Value);
        }

        [CadRule(CadRuleUsageType.Property, "DimD")]
        public Rule<float> FlatPatternDistFromHoleToOppositeEdge { get; set; }
        protected virtual void ConfigureFlatPatternDistFromHoleToOppositeEdge(Rule<float> rule)
        {
            rule.Define(() => AngleFlatCutLength.Value - HoleVertOffsetFromBottom.Value);
        }

        //This is the description used only by the BOM Summary.
        public Rule<string> BOMUIDescription { get; set; }
        protected virtual void ConfigureBOMUIDescription(Rule<string> rule)
        {
            rule.Define(() => $"RS({Helper.fractionString(Length.Value, feetToo: false)}, {AngleType.Value})");
        }

        #endregion

        #region FrameTieComponent Overrides

        protected override void ConfigureFrameTieDescription1(ReadOnlyRule<string> rule)
        {
            rule.Define("WELDLESS");
        }

        protected override void ConfigureFrameTieDescription2(ReadOnlyRule<string> rule)
        {
            rule.Define("ROW SPACER");
        }

        protected virtual void ConfigureBOMDescription(Rule<string> rule)
        {
            rule.Define(() =>
            {
                var desc = $"Weldless Row Spacer, {OverallLength.Value:#.##}\" long";
                desc += $"|{this.GetAngleShortDesc(AngleType.Value)} material, {ColorBOMName.Value}";
                return desc;
            });
        }

        #endregion

        #region IAngleData Implementation
        public Rule<List<AngleData>> AllAngleData { get; set; }
        public Rule<List<ChoiceListItem>> AllAngleTypesChoiceList { get; set; }
        public Rule<AngleData> ActiveAngleData { get; set; }
        public Rule<List<string>> AngleSizesAvailable { get; set; }
        public Rule<string> AngleType { get; set; }
        public Rule<float> AngleWidth { get; set; }
        protected virtual void ConfigureAngleWidth(Rule<float> rule)
        {
            rule.AddToCAD(cad =>
            {
                cad.NameInCad = "Width";
            });
        }

        public Rule<float> AngleHeight { get; set; }
        protected virtual void ConfigureAngleHeight(Rule<float> rule)
        {
            rule.AddToCAD(cad =>
            {
                cad.NameInCad = "Height";
            });
        }
        public Rule<float> AngleThickness { get; set; }
        protected virtual void ConfigureAngleThickness(Rule<float> rule)
        {
            //rule.AddToCAD(cad =>
            //{
            //    cad.NameInCad = "Thickness";
            //});
        }

        public Rule<string> MaterialPartNumber { get; set; }

        #endregion

        #region IRevisablePartImplementation

        public FactoryRevisionRule<string> FactoryRevision { get; set; }

        #endregion

        #region CPQ.IBomPart Implementation

        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {
            child.PartNumber.Define(PartNumber);
            child.Description.Define(BOMUIDescription);
            child.ItemQty.Define(Qty);
        }

        #endregion

        #region Labor Rules

        public Rule<double> PaintCarriersPerHour { get; set; }
        protected virtual void ConfigurePaintCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> UnloadCarriersPerHour { get; set; }
        protected virtual void ConfigureUnloadCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> PartsPerCarrier { get; set; }
        protected virtual void ConfigurePartsPerCarrier(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        #endregion

        #region Children

        public RSFrameTieAngle RSFrameTieAngle { get; set; }
        protected virtual void ConfigureRSFrameTieAngle(RSFrameTieAngle child)
        {
            child.PartNumberPrefix.Define(() => $"{AnglePartNumber.Value}-");
            child.AngleType.Define(AngleType);
            child.AngleFlatCutLength.Define(AngleFlatCutLength);
            child.Length.Define(Length);
            child.Description.Define(AngleMaterialDescription);
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(Colors.Blue);
        }

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define("RSFrameTie");
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(false);
        }

        #endregion

        #region IBomPart Overrides

        protected virtual void ConfigurePartWeight(Rule<double> rule)
        {
            rule.Define(() => this.GetPartWeightWithoutPaint());
        }

        #endregion

        #region Graphics

        public Block HorizontalLegBlock { get; set; }
        protected virtual void ConfigureHorizontalLegBlock(Block geo)
        {
            geo.Width.Define(Length);
            geo.Height.Define(AngleThickness);
            geo.Depth.Define(AngleWidth);
            geo.Position.Define(() => Helper.Vxyz(-geo.Width.Value / 2, -geo.Height.Value / 2, (-geo.Depth.Value / 2) + HoleHorizOffset.Value));
        }

        public Block VerticalLegBlock { get; set; }
        protected virtual void ConfigureVerticalLegBlock(Block geo)
        {
            geo.Width.Define(Length);
            geo.Height.Define(AngleHeight);
            geo.Depth.Define(AngleThickness);
            geo.Position.Define(() => Helper.Vxyz(-geo.Width.Value / 2, -geo.Height.Value / 2, (-geo.Depth.Value / 2) + HoleHorizOffset.Value));
        }

        public Block FrontEndBlock { get; set; }
        protected virtual void ConfigureFrontEndBlock(Block geo)
        {
            geo.Width.Define(AngleThickness);
            geo.Height.Define(() => AngleHeight.Value + 0.125f);
            geo.Depth.Define(AngleWidth);
            geo.Position.Define(() => Helper.Vxyz(-geo.Width.Value / 2, -geo.Height.Value / 2, (-geo.Depth.Value / 2) + HoleHorizOffset.Value));
        }

        public Block RearEndBlock { get; set; }
        protected virtual void ConfigureRearEndBlock(Block geo)
        {
            geo.Width.Define(AngleThickness);
            geo.Height.Define(() => AngleHeight.Value + 0.125f);
            geo.Depth.Define(AngleWidth);
            geo.Position.Define(() => Helper.Vxyz((geo.Width.Value / 2) - Length.Value, -geo.Height.Value / 2, (-geo.Depth.Value / 2) + HoleHorizOffset.Value));
        }

        //public UCS UCS { get; set; }
        //protected virtual void ConfigureUCS(UCS child)
        //{
        //    child.Diameter.Define(0.0125f);
        //    child.Position.Define(() => (Helper.Vy(0)));
        //}

        #endregion

        #region Protected Methods

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureAngleRules();
            this.ConfigureRevisablePart();
        }

        #endregion
    }
}
