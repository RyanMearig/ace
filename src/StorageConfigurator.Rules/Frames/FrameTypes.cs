﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ruler.Rules;

namespace StorageConfigurator.Rules
{
    public class FrameTypes : Part
    {
        #region Constructors

        public FrameTypes(ModelContext modelContext) : base(modelContext) { }

        public FrameTypes(string name, Part parent) : base(name, parent) { }

        #endregion

        #region Rules

        [UIRule]
        public ReadOnlyRule<int> FrameTypeQty { get; set; }
        protected virtual void ConfigureFrameTypeQty(ReadOnlyRule<int> rule)
        {
            rule.Define(() => FrameTypeList.Value.Count);
        }

        public Rule<List<FrameType>> FrameTypeList { get; set; }
        protected virtual void ConfigureFrameTypeList(Rule<List<FrameType>> rule)
        {
            rule.Define(() =>
            {
                List<FrameType> retVal = new List<FrameType>();

                if (ActiveChildParts.Value != null && ActiveChildParts.Value.Any())
                    retVal = ActiveChildParts.Value.Where(c =>
                    {
                        return (c.GetType() == typeof(SelectiveFrameType) || c.GetType() == typeof(DoubleDeepFrameType));
                    })
                    .Select(p => (FrameType)p).ToList();

                return retVal;
            });
        }

        protected override void ConfigureAllowedChildTypes(ReadOnlyRule<IEnumerable<Type>> rule)
        {
            rule.Define(() => new List<Type>
            {
                typeof(SelectiveFrameType),
                typeof(DoubleDeepFrameType)
            });
        }

        #endregion

        #region Part Overrides

        public override bool IsCadPart => false;

        #endregion

    }
}
