﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class FrameTieComponent : PaintedPart
    {
        #region Constructors

        public FrameTieComponent(ModelContext modelContext, StorageConfiguratorRepository repo) : base(modelContext) 
        {
            Repository = repo;
        }

        public FrameTieComponent(string name, Part parent, StorageConfiguratorRepository repo) : base(name, parent)
        {
            Repository = repo;
        }

        #endregion

        #region IBomPart Implementation 

        protected virtual void ConfigurePartNumberPrefix(Rule<string> rule)
        {
            rule.Define(() => $"{Type.Value.ToUpper()}-");
        }

        #endregion

        #region Rules

        [CadRule(CadRuleUsageType.Property)]
        public Rule<float> OverallLength { get; set; }
        protected virtual void ConfigureOverallLength(Rule<float> rule)
        {
            rule.Define(default(float));
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> Type { get; set; }
        protected virtual void ConfigureType(Rule<string> rule)
        {
            rule.Define("rrx");
        }

        [CadRule(CadRuleUsageType.Property)]
        public ReadOnlyRule<string> FrameTieDescription1 { get; set; }
        protected virtual void ConfigureFrameTieDescription1(ReadOnlyRule<string> rule)
        {
            rule.Define("");
        }

        [CadRule(CadRuleUsageType.Property)]
        public ReadOnlyRule<string> FrameTieDescription2 { get; set; }
        protected virtual void ConfigureFrameTieDescription2(ReadOnlyRule<string> rule)
        {
            rule.Define("");
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> rulerDrawingXml { get; set; }
        protected virtual void ConfigurerulerDrawingXml(Rule<string> rule)
        {
            rule.Define(() => $"{Type.Value.ToUpper()}FrameTie");
        }

        #endregion

        #region Overrides

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureBomPartRules();
        }

        #endregion


    }
}
