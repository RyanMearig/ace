﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ruler.Rules;
using Ruler.Rules.Graphics;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class FrameTiesSpace : Part, IDepthMaster, IColorComponent, IRevisablePart
    {
        #region Constructors

        public FrameTiesSpace(ModelContext modelContext, StorageConfiguratorRepository repo) : base(modelContext)
        {
            Repository = repo;
        }

        public FrameTiesSpace(string name, Part parent, StorageConfiguratorRepository repo) : base(name, parent)
        {
            Repository = repo;
        }

        #endregion

        #region Rules

        public LookupRule<StorageSystem> StorageSystemComp { get; set; }

        [UIRule]
        public Rule<int> FrameTieQty { get; set; }
        protected virtual void ConfigureFrameTieQty(Rule<int> rule)
        {
            rule.Define(() => DefaultFrameTieLocations.Value.Count());
            rule.ValueReset += (sender, e) =>
            {
                FrameTies?.ToList().ForEach(t => t.AttachmentLocation.Reset());
            };
        }

        //Pass In
        public Rule<Dictionary<string, float>> HorizBraceDict { get; set; }
        protected virtual void ConfigureHorizBraceDict(Rule<Dictionary<string, float>> rule)
        {
            rule.Define(() =>
            {
                var retVal = new Dictionary<string, float>();
                //FrontFrame?.HorizontalBrace.ToList().ForEach(b => retVal.Add(b.Path, b.OffsetFromBottom.Value));

                return retVal;
            });
        }

        //Pass In
        public Rule<string> UprightColumnSize { get; set; }
        protected virtual void ConfigureUprightColumnSize(Rule<string> rule)
        {
            rule.Define("C3x3.5");
        }

        //Pass In
        public Rule<float> RearDoublerHeight { get; set; }
        protected virtual void ConfigureRearDoublerHeight(Rule<float> rule)
        {
            rule.Define(default(float));
        }

        //Pass In
        public Rule<float> FrontDoublerHeight { get; set; }
        protected virtual void ConfigureFrontDoublerHeight(Rule<float> rule)
        {
            rule.Define(default(float));
        }


        public Rule<List<string>> HeavyWeldedFrameTieSizesAvailable { get; set; }
        protected virtual void ConfigureHeavyWeldedFrameTieSizesAvailable(Rule<List<string>> rule)
        {
            rule.Define(() => (new List<string> { "C3x3.5", "C3x4.1", "C4x4.5", "C4x5.4", "C5x6.1", "C5x6.7" }));
        }

        public Rule<int> RubRailQty { get; set; }
        protected virtual void ConfigureRubRailQty(Rule<int> rule)
        {
            rule.Define(() => FrameTieType.Value.ToLower().Contains("rrx") ? 1 : 0);
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !(IsDoubleDeepPost.Value && FrameTieType.Value.ToLower().Contains("rrx")));
            });
        }

        public Rule<bool> IsAngleFrameTie { get; set; }
        protected virtual void ConfigureIsAngleFrameTie(Rule<bool> rule)
        {
            rule.Define(() => (FrameTieType.Value == "rs" || FrameTieType.Value == "rfsw"));
        }

        public Rule<string> FrameTieAngleType { get; set; }
        protected virtual void ConfigureFrameTieAngleType(Rule<string> rule)
        {
            rule.Define(() => FrameTies.Any() ? rule.UIMetadata.ChoiceList.Value?.First().Value.ToString() : "");
            rule.AddToUI(ui =>
            {
                ui.Label.Define("Default Frame Tie Angle Type");
                ui.ReadOnly.Define(() => !IsAngleFrameTie.Value);
                ui.ChoiceList.Define(() =>
                {
                    if (FrameTies.Any())
                    {
                        return Helper.GetFilteredChoices(FrameTies.First().AllAngleTypesChoiceList.Value, DefaultFrameTieAngleSizes.Value);
                    }
                    return new List<ChoiceListItem>();
                });
            });
        }

        public Rule<List<string>> DefaultFrameTieAngleSizes { get; set; }
        protected virtual void ConfigureDefaultFrameTieAngleSizes(Rule<List<string>> rule)
        {
            rule.Define(() =>
            {
                List<string> choices = new List<string>();

                if (FrameTieType.Value == "rs")
                {
                    choices.Add(@"1-1/2 x 1-1/2 x 1/8");
                    choices.Add(@"2 x 2 x 1/8");
                }
                else if (FrameTieType.Value == "rfsw")
                {
                    choices.Add(@"1-1/2 x 1-1/2 x 1/8");
                    choices.Add(@"2 x 2 x 1/8");
                    choices.Add(@"2 x 2 x 3/16");
                }

                return choices;
            });
        }

        public Rule<List<ChoiceListItem>> FrameHorizBraces { get; set; }
        protected virtual void ConfigureFrameHorizBraces(Rule<List<ChoiceListItem>> rule)
        {
            rule.Define(() =>
            {
                if (IsDoubleDeepPost.Value)
                {
                    return new List<ChoiceListItem>();
                }
                return HorizBraceDict.Value?.Select((b, i) => new ChoiceListItem(b.Key, $"Horiz Brace {i + 1}"))
                                            .Reverse().ToList();
            });
        }

        public Rule<List<string>> DefaultFrameTieLocations { get; set; }
        protected virtual void ConfigureDefaultFrameTieLocations(Rule<List<string>> rule)
        {
            rule.Define(() =>
            {
                List<string> retVal = new List<string>();
                if (HorizBraceDict.Value == null || !HorizBraceDict.Value.Any()) return retVal;
                Dictionary<string, float> horizBraceLocsTopDown = HorizBraceDict.Value?.OrderByDescending(d => d.Value).ToDictionary(b => b.Key, y => y.Value);

                retVal = RecurseGetLocs(horizBraceLocsTopDown, retVal, horizBraceLocsTopDown.FirstOrDefault().Value);
                return retVal;
            });
        }

        public Rule<List<string>> FrameTieLocations { get; set; }
        protected virtual void ConfigureFrameTieLocations(Rule<List<string>> rule)
        {
            rule.Define(() =>
            {
                if (FrameTieQty.Value != DefaultFrameTieLocations.Value.Count())
                {
                    return HorizBraceDict.Value?.Select(d => d.Key).Take(FrameTieQty.Value).ToList();
                }
                return DefaultFrameTieLocations.Value;
            });
        }


        public Rule<string> FrameTieBeamSize { get; set; }
        protected virtual void ConfigureFrameTieBeamSize(Rule<string> rule)
        {
            rule.Define(UprightColumnSize);
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => FrameTieType.Value != "rfswhd");
                ui.ChoiceList.Define(() => HeavyWeldedFrameTieChoices.Value);
            });
        }

        public Rule<List<ChoiceListItem>> HeavyWeldedFrameTieChoices { get; set; }
        protected void ConfigureHeavyWeldedFrameTieChoices(Rule<List<ChoiceListItem>> rule)
        {
            rule.Define(() =>
            {
                if (FrameTieBeamSize.UIMetadata.ReadOnly.Value)
                {
                    return null;
                }
                else
                {
                    return Helper.GetFilteredChoices(FrameTies.First().AllChannelSizesChoiceList.Value, HeavyWeldedFrameTieSizesAvailable.Value);
                }
            });
        }

        public Rule<bool> FrameTieLengthReadOnly { get; set; }
        protected virtual void ConfigureFrameTieLengthReadOnly(Rule<bool> rule)
        {
            rule.Define(false);
        }

        #endregion

        #region IDepthMaster Implementation

        public Rule<bool> IsDoubleDeep { get; set; }
        public Rule<string> DoubleDeepType { get; set; }
        public Rule<bool> IsDoubleDeepPost { get; set; }
        public Rule<float> FrontFrameDepth { get; set; }
        public Rule<float> RearFrameDepth { get; set; }
        public Rule<bool> HasFrameTies { get; set; }
        protected virtual void ConfigureHasFrameTies(Rule<bool> rule)
        {
            rule.Define(true);
        }
        public Rule<string> FrameTieType { get; set; }
        protected virtual void ConfigureFrameTieType(Rule<string> rule)
        {
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() => (new List<ChoiceListItem>()
                {
                    new ChoiceListItem("rs", "No Weld"),
                    new ChoiceListItem("rfsw", "Welded"),
                    new ChoiceListItem("rfswhd", "Heavy Welded"),
                    new ChoiceListItem("rrx", "Rub Rail(RRx)"),
                    new ChoiceListItem("rrxd", "Doubled Rub Rail")
                }));
            });
        }
        public Rule<float> FrameTieLengthMin { get; set; }
        public Rule<float> FrameTieLength { get; set; }
        protected virtual void ConfigureFrameTieLength(Rule<float> rule)
        {
            rule.Define(12);
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(FrameTieLengthReadOnly);
            });
        }

        public ReadOnlyRule<float> OverallDepth { get; set; }
        protected virtual void ConfigureOverallDepth(ReadOnlyRule<float> rule)
        {
            rule.Define(FrameTieLength);
        }

        #endregion

        #region IColorComponent Implementation

        public Rule<string> Color { get; set; }
        public ReadOnlyRule<AdvanceColor> AdvanceColor { get; set; }
        public ReadOnlyRule<string> ColorDescription { get; set; }
        public StorageConfiguratorRepository Repository { get; set; }
        public Rule<string> ColorBOMName { get; set; }
        public ReadOnlyRule<string> ColorShortDescription { get; set; }

        public Rule<double> PartWeight { get; set; }
        public Rule<double> PartPaintWeight { get; set; }
        public Rule<float> WeightPerGallon { get; set; }
        public CsiRule<double> PaintCostPerGallon { get; set; }
        public Rule<float> PaintCost { get; set; }


        #endregion

        #region IRevisablePartImplementation

        public FactoryRevisionRule<string> FactoryRevision { get; set; }

        #endregion

        #region Private Methods

        private float GetFrameTieVertOffset(string horizPath)
        {
            return HorizBraceDict.Value.TryGetValue(horizPath, out float offset) ? offset : 0;
        }

        private List<string> RecurseGetLocs(Dictionary<string, float> searchDict, List<string> locList, float searchVal)
        {
            List<string> retVal = locList;
            string closestLoc = GetClosestLocation(searchDict, searchVal);
            retVal.Add(closestLoc);

            float newSearchVal = searchDict.GetValueOrDefault(closestLoc) - 120;
            if (newSearchVal > searchDict.Last().Value)
            {
                retVal = RecurseGetLocs(searchDict, retVal, newSearchVal);
            }

            return retVal;
        }

        public string GetClosestLocation(Dictionary<string, float> searchDict, float searchVal)
        {
            return searchDict?.Where(x => x.Value >= searchVal).Last().Key;
        }

        private List<float> RecurseGetLocs(List<float> searchList, List<float> locList, float searchVal)
        {
            List<float> retVal = locList;
            float closestLoc = GetClosestLocation(searchList, searchVal);
            retVal.Add(closestLoc);

            float newSearchVal = closestLoc - 120;
            if (newSearchVal > searchList.Last())
                retVal = RecurseGetLocs(searchList, retVal, newSearchVal);

            return retVal;
        }

        public float GetClosestLocation(List<float> searchList, float searchVal)
        {
            return searchList.Where(x => x >= searchVal).Last();
        }

        #endregion

        #region Children

        public ChildList<FrameTie> FrameTies { get; set; }
        protected virtual void ConfigureFrameTies(ChildList<FrameTie> list)
        {
            list.Qty.Define(() => (HasFrameTies.Value ? FrameTieQty.Value : 0));
            list.ConfigureChildren((child, index) =>
            {
                child.Type.Define(FrameTieType);
                child.ColumnChannelSize.Define(UprightColumnSize);
                child.ChannelSize.Define(FrameTieBeamSize);
                child.Length.Define(FrameTieLength);
                child.AngleType.Define(FrameTieAngleType);
                child.HorizBraceChoices.Define(FrameHorizBraces);
                child.AttachmentLocation.Define(() => FrameTieLocations.Value[index]);
                //child.Rotation.Define(() => index == 0 ? Helper.CreateRotation(Vector3.UnitY, 90) : Helper.CreateRotation(Vector3.UnitY, 0));
                child.Position.Define(() => Helper.Vy(GetFrameTieVertOffset(child.AttachmentLocation.Value)));
                child.FrontDoubleratConnection.Define(() => RearDoublerHeight.Value >= child.Position.Value.Y);
                child.RearDoubleratConnection.Define(() => FrontDoublerHeight.Value >= child.Position.Value.Y);
            });
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(Colors.Lime);
        }

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define("FrameTiesSpace");
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(false);
        }

        #endregion

        #region Protected Methods

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureDepthMasterRules();
            this.ConfigureColorComponent();
            this.ConfigureRevisablePart();
        }

        #endregion

        #region Graphics

        //public UCS UCS { get; set; }
        //protected virtual void ConfigureUCS(UCS child)
        //{
        //    child.Position.Define(() => (Helper.Vy(0)));
        //}

        #endregion



    }
}
