﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Ruler.Rules;
using Ruler.Cpq;
using Ruler.Rules.Graphics;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class Footplate : AdvancePart, IBasePlate, IBomPart, IRevisablePart, Ruler.Cpq.IBomPart
    {
        #region Constructors

        public Footplate(ModelContext modelContext, StorageConfiguratorRepository repo) : base(modelContext)
        {
            Repository = repo;
        }

        public Footplate(string name, Part parent, StorageConfiguratorRepository repo) : base(name, parent)
        {
            Repository = repo;
        }

        #endregion

        #region Rules
               

        public Rule<string> BeamSize { get; set; }
        protected virtual void ConfigureBeamSize(Rule<string> rule)
        {
            rule.Define("C3x3.5");
        }

        public Rule<string> BeamType { get; set; }
        protected virtual void ConfigureBeamType(Rule<string> rule)
        {
            rule.Define("S");
        }

        public Rule<float> Width { get; set; }
        protected virtual void ConfigureWidth(Rule<float> rule)
        {
            rule.Define(() => (BeamType.Value == "S" ? ChannelWidth.Value + 1 : ChannelWidth.Value + 2));
            rule.AddToUI();
            rule.AddToCAD();
        }

        public Rule<float> HoleDiameter { get; set; }
        protected virtual void ConfigureHoleDiameter(Rule<float> rule)
        {
            rule.Define(() => (float.Parse(AnchorSize.Value) + 0.125f));
            rule.AddToCAD();
        }

        public Rule<float> Thickness { get; set; }
        protected virtual void ConfigureThickness(Rule<float> rule)
        {
            rule.Define(0.375f);
            rule.AddToUI();
            rule.AddToCAD();
        }

        public Rule<float> ChannelWidth { get; set; }
        protected virtual void ConfigureChannelWidth(Rule<float> rule)
        {
            rule.Define(1);
        }

        public Rule<float> HoleOffsetFromOrigin { get; set; }
        protected virtual void ConfigureHoleOffsetFromOrigin(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if(BasePlateType.Value.Contains("FP5") || BasePlateType.Value.Contains("FP6"))
                {
                    var bullnoseOffset = HasBullnose.Value ? 1.75f : 0;
                    return (float.Parse(AnchorSize.Value) + 0.25f) + bullnoseOffset;
                }
                return HasDoubler.Value ? 4.375f : 3.5f;
                });
            rule.AddToCAD();
        }

        public Rule<float> HoleOffsetFromEdge { get; set; }
        protected virtual void ConfigureHoleOffsetFromEdge(Rule<float> rule)
        {
            rule.Define(() => (float.Parse(AnchorSize.Value) + 0.25f));
            rule.AddToCAD();
        }

        public ReadOnlyRule<float> ColumnWidth { get; set; }
        protected virtual void ConfigureColumnWidth(ReadOnlyRule<float> rule)
        {
            rule.Define(3);
        }

        [CadRule(CadRuleUsageType.Parameter)]
        public Rule<float> BullnoseTrimLength { get; set; }
        protected virtual void ConfigureBullnoseTrimLength(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (!HasBullnose.Value)
                    return 0;
                return ColumnWidth.Value >= 5 || BasePlateType.Value.StartsWith("FP6")
                    ? 1.75f : (Width.Value - 1) / 2;
            });
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> Type { get; set; }
        protected virtual void ConfigureType(Rule<string> rule)
        {
            rule.Define(BasePlateType);
        }

        public Rule<bool> IsFront { get; set; }
        protected virtual void ConfigureIsFront(Rule<bool> rule)
        {
            rule.Define(false);
        }

        #endregion

        #region IBasePlate Implementation

        public Rule<string> BasePlateType { get; set; }
        public Rule<List<BasePlateTypeData>> AllBasePlateData { get; set; }
        public Rule<List<ChoiceListItem>> BasePlateTypeChoiceList { get; set; }
        public Rule<BasePlateTypeData> ActiveBasePlateData { get; set; }
        public Rule<bool> HasDoubler { get; set; }
        public Rule<string> PostProtector { get; set; }
        public Rule<List<ChoiceListItem>> PostProtectorChoices { get; set; }
        public Rule<float> AnchorQty { get; set; }
        protected virtual void ConfigureAnchorQty(Rule<float> rule)
        {
            rule.AddToCAD();
        }
        public Rule<string> AnchorSize { get; set; }
        public Rule<float> BasePlateLength { get; set; }
        protected virtual void ConfigureBasePlateLength(Rule<float> rule)
        {
            rule.AddToCAD(cad =>
            {
                cad.NameInCad = "Length";
            });
        }

        public Rule<bool> HasBullnose { get; set; }

        public Rule<string> BasePartNumber { get; set; }
        protected virtual void ConfigureBasePartNumber(Rule<string> rule)
        {
            rule.Define(default(string));
        }

        #endregion

        #region IBomPart Implementation

        public Rule<string> DatabasePartNumber { get; set; }
        public PartNumberRule<string> PartNumber { get; set; }
        protected virtual void ConfigurePartNumber(PartNumberRule<string> rule)
        {
            rule.Define(() =>
            {
                var holeSizeArray = Helper.fractionString(HoleDiameter.Value, 8).Split("/");
                int num = Convert.ToInt32(holeSizeArray[0]);
                int den = Convert.ToInt32(holeSizeArray[1].Substring(0,1));
                
                var factor = 8 / den;

                var holeSizeNum = num * factor;

                var loc = IsFront.Value ? "F" : "R";

                return $"{BasePartNumber.Value}-{BasePlateType.Value}-{ChannelWidth.Value}-{Width.Value}-{holeSizeNum}-{loc}";
            });
            
        }

        public Rule<string> PartNumberPrefix { get; set; }

        public Rule<string> Description { get; set; }
        protected virtual void ConfigureDescription(Rule<string> rule)
        {
            rule.Define(() =>
            {
                var holeBSize = "7/8";
                var holeBDesc = HasBullnose.Value ? holeBSize : "NONE";
                return $"{Helper.fractionString(Thickness.Value, feetToo: false)}" +
                $"X{Helper.fractionString(Width.Value, feetToo: false)} " +
                $"x {Helper.fractionString(BasePlateLength.Value, feetToo: false)}," +
                $"FP Hole A {Helper.fractionString(HoleDiameter.Value, feetToo: false)}, Hole B {holeBDesc}";
            });
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(false);
            });
        }
        public Rule<string> BOMDescription { get; set; }
        protected virtual void ConfigureBOMDescription(Rule<string> rule)
        {
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(false);
            });
        }
        public Rule<string> BOMDescription1 { get; set; }
        public Rule<string> BOMDescription2 { get; set; }
        public Rule<string> BOMDescription3 { get; set; }
        public Rule<string> BOMDescription4 { get; set; }
        public Rule<string> BOMDescription5 { get; set; }
        public Rule<string> BOMDescription6 { get; set; }
        public Rule<string> BOMDescription7 { get; set; }
        public Rule<string> BOMDescription8 { get; set; }
        public Rule<string> BOMDescription9 { get; set; }
        public Rule<string> BOMDescription10 { get; set; }
        public Rule<string> BOMDescription11 { get; set; }

        public Rule<string> Color { get; set; }
        public ReadOnlyRule<AdvanceColor> AdvanceColor { get; set; }
        public ReadOnlyRule<string> ColorDescription { get; set; }
        public StorageConfiguratorRepository Repository { get; set; }
        public Rule<string> ColorBOMName { get; set; }
        public ReadOnlyRule<string> ColorShortDescription { get; set; }

        public Rule<double> PartWeight { get; set; }
        public Rule<double> PartPaintWeight { get; set; }
        public Rule<float> WeightPerGallon { get; set; }
        public CsiRule<double> PaintCostPerGallon { get; set; }
        public Rule<float> PaintCost { get; set; }

        #endregion

        #region IRevisablePartImplementation

        public FactoryRevisionRule<string> FactoryRevision { get; set; }

        #endregion

        #region CPQ.IBomPart Implementation

        //This is the description used only by the BOM Summary.
        public Rule<string> BOMUIDescription { get; set; }
        protected virtual void ConfigureBOMUIDescription(Rule<string> rule)
        {
            rule.Define(() =>
            {
                return $"{Helper.fractionString(Thickness.Value, feetToo: false)}" +
                $"x{Helper.fractionString(Width.Value, feetToo: false)}" +
                $"x{Helper.fractionString(BasePlateLength.Value, feetToo: false)} " +
                Type.Value;
            });
        }

        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {
            child.PartNumber.Define(PartNumber);
            child.Description.Define(BOMUIDescription);
            child.MaterialPartNumber.Define(MaterialPartNumber);
            child.MaterialQty.Define(() => (BasePlateLength.Value * Width.Value) / 144);
            child.ItemMaterialCost.Define(() => child.ItemRawMaterialWeight.Value * child.MaterialUnitCost.Value);
        }

        #endregion

        #region Material Rules

        public Rule<string> MaterialPartNumber { get; set; }
        protected virtual void ConfigureMaterialPartNumber(Rule<string> rule)
        {
            rule.Define(default(string));
        }

        #endregion

        #region Labor Rules

        public Rule<double> PartCarriersPerHour { get; set; }
        protected virtual void ConfigurePartCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(Colors.Blue);
        }

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define(() => HasBullnose.Value ? "FootplateWBullnose" : "SquareFootplate");
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(false);
        }

        #endregion

        #region Public Methods

        public string GetBaseTypeValue(string basePlateType)
        {
            return basePlateType.Split('-').FirstOrDefault(); 
        }

        #endregion

        #region Graphics

        public Block FootplateBlock { get; set; }
        protected virtual void ConfigureFootplateBlock(Block geo)
        {
            geo.Width.Define(() => Width.Value);
            geo.Height.Define(() => Thickness.Value);
            geo.Depth.Define(() => BasePlateLength.Value);
            geo.Position.Define(() => Helper.Vyz(Thickness.Value/2, -BasePlateLength.Value/2));
        }

        //public UCS UCS { get; set; }
        //protected virtual void ConfigureUCS(UCS child)
        //{
        //    child.Diameter.Define(0.0125f);
        //    child.Position.Define(() => (Helper.Vy(0)));
        //}

        #endregion

        #region Protected Methods

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureBasePlateRules();
            this.ConfigureBomPartRules();
            this.ConfigureRevisablePart();
        }

        #endregion
    }
}
