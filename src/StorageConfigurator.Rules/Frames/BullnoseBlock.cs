﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;
using Ruler.Rules.Graphics;

namespace StorageConfigurator.Rules
{
    public class BullnoseBlock : Part
    {
        #region Constructors

        public BullnoseBlock(ModelContext modelContext) : base(modelContext) { }

        public BullnoseBlock(string name, Part parent) : base(name, parent) { }

        #endregion

        #region Rules

        public Rule<float> Width { get; set; }
        protected virtual void ConfigureWidth(Rule<float> rule)
        {
            rule.Define(2);
        }

        public Rule<float> Height { get; set; }
        protected virtual void ConfigureHeight(Rule<float> rule)
        {
            rule.Define(2);
        }

        public Rule<float> Depth { get; set; }
        protected virtual void ConfigureDepth(Rule<float> rule)
        {
            rule.Define(2);
        }

        public Rule<int> Color { get; set; }
        protected virtual void ConfigureColor(Rule<int> rule)
        {
            rule.Define(Colors.Black);
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(Color);
            material.Opacity.Define(0.99f);
        }

        #endregion

        #region Graphics

        public Block Block { get; set; }
        protected virtual void ConfigureBlock(Block geo)
        {
            geo.Width.Define(Width);
            geo.Height.Define(Height);
            geo.Depth.Define(Depth);
            geo.Position.Define(() => Helper.Vxyz(-geo.Width.Value / 2, geo.Height.Value / 2, 0));
        }

        //public UCS UCS { get; set; }
        //protected virtual void ConfigureUCS(UCS child)
        //{
        //    child.Position.Define(() => (Helper.Vy(0)));
        //}

        #endregion
    }
}
