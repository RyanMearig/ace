﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;
using Ruler.Rules;
using Ruler.Cpq;
using Ruler.Rules.Graphics;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class Bullnose : AdvancePart, IRevisablePart, IBomPart, Ruler.Cpq.IBomPart
    {

        #region Constructors

        public Bullnose(ModelContext modelContext) : base(modelContext) { }

        public Bullnose(string name, Part parent) : base(name, parent) { }

        #endregion

        #region Rules

        public Rule<float> ColumnWidth { get; set; }
        protected virtual void ConfigureColumnWidth(Rule<float> rule)
        {
            rule.Define(3);
        }

        [CadRule(CadRuleUsageType.Parameter)]
        public Rule<float> Width { get; set; }
        protected virtual void ConfigureWidth(Rule<float> rule)
        {
            rule.Define(() => ColumnWidth.Value + 0.0625f);
        }

        [CadRule(CadRuleUsageType.Parameter)]
        public Rule<float> Depth { get; set; }
        protected virtual void ConfigureDepth(Rule<float> rule)
        {
            rule.Define(() => (ColumnWidth.Value < 5 ? 3.25f : 3.375f));
        }

        [CadRule(CadRuleUsageType.Parameter)]
        public Rule<float> BendAngle { get; set; }
        protected virtual void ConfigureBendAngle(Rule<float> rule)
        {
            rule.Define(() => (ColumnWidth.Value < 5 ? 90 : 121));
        }

        [CadRule(CadRuleUsageType.Parameter)]
        public Rule<float> Height { get; set; }
        protected virtual void ConfigureHeight(Rule<float> rule)
        {
            rule.Define(12);
        }

        public Rule<float> AttachmentDepth { get; set; }
        protected virtual void ConfigureAttachmentDepth(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (ColumnWidth.Value <= 3)
                    return 1.5f;
                else if (ColumnWidth.Value <= 4)
                    return 1;
                else
                    return 1.59375f;
            });
        }

        public Rule<float> Thickness { get; set; }
        protected virtual void ConfigureThickness(Rule<float> rule)
        {
            rule.Define(0.25f);
        }

        public Rule<float> BullnoseAngleBlockLength { get; set; }
        protected virtual void ConfigureBullnoseAngleBlockLength(Rule<float> rule)
        {
            rule.Define(() =>
            {
                //use Pythagorean Theorem
                float a = (Depth.Value - AttachmentDepth.Value);
                float b = (Width.Value/2);
                return (float)Math.Sqrt((Math.Pow(a, 2) + Math.Pow(b, 2)));
            });
        }

        public Rule<string> BasePartNumber { get; set; }
        protected virtual void ConfigureBasePartNumber(Rule<string> rule)
        {
            rule.Define(default(string));
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(Colors.Blue);
        }

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define("Bullnose");
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(true);
        }

        #endregion

        #region Public Methods



        #endregion

        #region IRevisablePartImplementation

        public FactoryRevisionRule<string> FactoryRevision { get; set; }

        #endregion

        #region IBomPart Implementation

        public Rule<string> DatabasePartNumber { get; set; }
        public PartNumberRule<string> PartNumber { get; set; }
        protected virtual void ConfigurePartNumber(PartNumberRule<string> rule)
        {
            rule.Define(() => $"{BasePartNumber.Value}-{Height.Value + .25}P{ColumnWidth.Value}");
        }
        public Rule<string> PartNumberPrefix { get; set; }        
        public Rule<string> Description { get; set; }
        public Rule<string> BOMDescription { get; set; }
        public Rule<string> BOMDescription1 { get; set; }
        public Rule<string> BOMDescription2 { get; set; }
        public Rule<string> BOMDescription3 { get; set; }
        public Rule<string> BOMDescription4 { get; set; }
        public Rule<string> BOMDescription5 { get; set; }
        public Rule<string> BOMDescription6 { get; set; }
        public Rule<string> BOMDescription7 { get; set; }
        public Rule<string> BOMDescription8 { get; set; }
        public Rule<string> BOMDescription9 { get; set; }
        public Rule<string> BOMDescription10 { get; set; }
        public Rule<string> BOMDescription11 { get; set; }
        public Rule<string> Color { get; set; }
        public ReadOnlyRule<AdvanceColor> AdvanceColor { get; set; }
        public ReadOnlyRule<string> ColorDescription { get; set; }
        public StorageConfiguratorRepository Repository { get; set; }
        public Rule<string> ColorBOMName { get; set; }
        public ReadOnlyRule<string> ColorShortDescription { get; set; }

        public Rule<double> PartWeight { get; set; }
        public Rule<double> PartPaintWeight { get; set; }
        public Rule<float> WeightPerGallon { get; set; }
        public CsiRule<double> PaintCostPerGallon { get; set; }
        public Rule<float> PaintCost { get; set; }

        #endregion

        #region CPQ.IBomPart Implementation

        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {
            child.PartNumber.Define(PartNumber);
        }

        #endregion

        #region Labor Rules

        public Rule<double> PartCarriersPerHour { get; set; }
        protected virtual void ConfigurePartCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        #endregion

        #region Graphics

        public Block RightAttachmentLegBlock { get; set; }
        protected virtual void ConfigureRightAttachmentLegBlock(Block geo)
        {
            geo.Width.Define(AttachmentDepth);
            geo.Height.Define(Height);
            geo.Depth.Define(Thickness);
            geo.Position.Define(() => Helper.Vxyz(geo.Width.Value / 2, geo.Height.Value / 2, -((geo.Depth.Value / 2) + Width.Value/2)));
        }

        public Block LeftAttachmentLegBlock { get; set; }
        protected virtual void ConfigureLeftAttachmentLegBlock(Block geo)
        {
            geo.Width.Define(AttachmentDepth);
            geo.Height.Define(Height);
            geo.Depth.Define(Thickness);
            geo.Position.Define(() => Helper.Vxyz(geo.Width.Value / 2, geo.Height.Value / 2, ((geo.Depth.Value / 2) + Width.Value / 2)));
        }

        public BullnoseBlock RightAngleBlock { get; set; }
        protected virtual void ConfigureRightAngleBlock(BullnoseBlock child)
        {
            child.Width.Define(BullnoseAngleBlockLength);
            child.Height.Define(Height);
            child.Depth.Define(Thickness);
            child.Color.Define(() => UIMaterial.Color.Value);
            child.Position.Define(() => Helper.Vx(Depth.Value));
            child.Rotation.Define(() => Helper.CreateRotation(Vector3.UnitY, -BendAngle.Value / 2));
        }

        public BullnoseBlock LeftAngleBlock { get; set; }
        protected virtual void ConfigureLeftAngleBlock(BullnoseBlock child)
        {
            child.Width.Define(BullnoseAngleBlockLength);
            child.Height.Define(Height);
            child.Depth.Define(Thickness);
            child.Color.Define(() => UIMaterial.Color.Value);
            child.Position.Define(() => Helper.Vx(Depth.Value));
            child.Rotation.Define(() => Helper.CreateRotation(Vector3.UnitY, BendAngle.Value / 2));
        }

        //public UCS UCS { get; set; }
        //protected virtual void ConfigureUCS(UCS child)
        //{
        //    child.Diameter.Define(0.0125f);
        //    child.Position.Define(() => (Helper.Vy(0)));
        //}

        #endregion

        #region Protected Methods

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureRevisablePart();
            this.ConfigureBomPartRules();
        }

        #endregion

    }
}
