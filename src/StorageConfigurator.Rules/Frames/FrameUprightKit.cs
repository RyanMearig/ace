﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using Ruler.Rules;
using Ruler.Rules.Graphics;

namespace StorageConfigurator.Rules
{
    public class FrameUprightKit : Part, IBasePlateData, IFrameUprightData
    {
        #region Constructors

        public FrameUprightKit(ModelContext modelContext) : base(modelContext) { }

        public FrameUprightKit(string name, Part parent) : base(name, parent) { }

        #endregion

        #region Rules

        public Rule<FrameUprightKit> FrameUprightKitMaster { get; set; }
        protected virtual void ConfigureFrameUprightKitMaster(Rule<FrameUprightKit> rule)
        {
            rule.Define(default(FrameUprightKit));
        }

        public Rule<bool> MasterSupplied { get; set; }
        protected virtual void ConfigureMasterSupplied(Rule<bool> rule)
        {
            rule.Define(() => FrameUprightKitMaster.Value != default(FrameUprightKit));
        }

        public Rule<bool> IsStandalone { get; set; }
        protected virtual void ConfigureIsStandalone(Rule<bool> rule)
        {
            rule.Define(() =>
            {
                if (MasterSupplied.Value)
                {
                    return FrameUprightKitMaster.Value.IsStandalone.Value;
                }
                return (Parent == null || (Parent.GetType() != typeof(SelectiveFrameType) && Parent.GetType() != typeof(DoubleDeepFrameType)));
            });
        }

        public Rule<double> Qty { get; set; }
        protected virtual void ConfigureQty(Rule<double> rule)
        {
            rule.Define(1);
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !IsStandalone.Value);
                ui.Label.Define("Quantity");
            });
        }

        //[CadRule(CadRuleUsageType.Property)]
        //public Rule<string> rulerDrawingXml { get; set; }
        //protected virtual void ConfigurerulerDrawingXml(Rule<string> rule)
        //{
        //    rule.Define("Upright");
        //}

        public Rule<bool> IsBentType { get; set; }
        protected virtual void ConfigureIsBentType(Rule<bool> rule)
        {
            rule.Define(() => UprightType.Value == "B");
        }

        #region Inputs Rules

        public Rule<bool> IsFrontFrame { get; set; }
        protected virtual void ConfigureIsFrontFrame(Rule<bool> rule)
        {
            rule.Define(IsStandalone);
        }

        public Rule<float> Height { get; set; }
        protected virtual void ConfigureHeight(Rule<float> rule)
        {
            rule.Define(360);
            rule.Min(HeightMin);
            rule.Max(MaxHeight);
            rule.MultipleOf(4);
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !IsStandalone.Value);
            });
        }

        public Rule<float> OverallHeight { get; set; }
        protected virtual void ConfigureOverallHeight(Rule<float> rule)
        {
            rule.Define(() => Height.Value + FootplateThickness.Value);
        }

        public Rule<float> HeightMin { get; set; }
        protected virtual void ConfigureHeightMin(Rule<float> rule)
        {
            rule.Define(() => Frame.HeightMin.Value);
        }

        //[UIRule]
        //public Rule<float> Depth { get; set; }
        //protected virtual void ConfigureDepth(Rule<float> rule)
        //{
        //    rule.Define(() => FrameUprightKitMaster.Value?.Depth.Value ?? DefaultDepth.Value);
        //    rule.Min(12);
        //    rule.Max(72);
        //}

        public Rule<float> DefaultDepth { get; set; }
        protected virtual void ConfigureDefaultDepth(Rule<float> rule)
        {
            rule.Define(44);
        }

        public Rule<float> DefaultFootplateWidth { get; set; }
        protected virtual void ConfigureDefaultFootplateWidth(Rule<float> rule)
        {
            rule.Define(() => Frame.DefaultFootplateWidth.Value);
        }

        public Rule<string> DefaultAnchorTypePerFootplate { get; set; }
        protected virtual void ConfigureDefaultAnchorTypePerFootplate(Rule<string> rule)
        {
            rule.Define(() => FrameUprightKitMaster.Value?.DefaultAnchorTypePerFootplate.Value ?? rule.UIMetadata.ChoiceList.Value.First().Value.ToString());
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() => this.GetAnchorChoiceList(float.Parse(DefaultAnchorSizePerFootplate.Value)));
            });
        }

        public Rule<string> DefaultAnchorSizePerFootplate { get; set; }
        protected virtual void ConfigureDefaultAnchorSizePerFootplate(Rule<string> rule)
        {
            rule.Define(() => FrameUprightKitMaster.Value?.DefaultAnchorSizePerFootplate.Value ?? FrontAnchorSize.Value);
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() => FrontAnchorSize.UIMetadata.ChoiceList.Value);
            });
            rule.ValueSet += (sender, e) =>
            {
                DefaultAnchorTypePerFootplate.Reset();
            };
            rule.ValueReset += (sender, e) =>
            {
                DefaultAnchorTypePerFootplate.Reset();
            };
        }

        public Rule<int> DefaultAnchorQtyPerFootplate { get; set; }
        protected virtual void ConfigureDefaultAnchorQtyPerFootplate(Rule<int> rule)
        {
            rule.Define(() => FrameUprightKitMaster.Value?.DefaultAnchorQtyPerFootplate.Value ?? 2);
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() => FrontAnchorQty.UIMetadata.ChoiceList.Value);
            });
        }

        #endregion

        #endregion

        #region IBasePlateData Implementation

        public Rule<List<BasePlateTypeData>> AllBasePlateData { get; set; }
        public Rule<List<ChoiceListItem>> BasePlateTypeChoiceList { get; set; }
        public Rule<List<ChoiceListItem>> PostProtectorChoices { get; set; }

        #endregion

        #region IFrameUprightData Implementation

        public Rule<string> UprightType { get; set; }
        protected virtual void ConfigureUprightType(Rule<string> rule)
        {
            rule.Define(() => FrameUprightKitMaster.Value?.UprightType.Value ?? "S");
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !IsStandalone.Value && IsFrontFrame.Value);
            });
            rule.AddToCAD(cad => cad.CadRuleUsageType = CadRuleUsageType.Property);
            rule.ValueSet += (sender, e) =>
            {
                HasFrontDoubler.Reset();
                HasRearDoubler.Reset();

                if (e.NewValue == "B" || e.NewValue == "L" || e.NewValue == "LS")
                    FrontPostProtector.Reset();

                if (e.OldValue == "B")
                {
                    SlantElev.Reset();
                    SlantInset.Reset();
                }
            };
        }

        public Rule<bool> Slanted { get; set; }
        protected virtual void ConfigureSlanted(Rule<bool> rule)
        {
            rule.Define(() => Frame.Slanted.Value);
        }

        public Rule<float> SlantElev { get; set; }
        protected virtual void ConfigureSlantElev(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (MasterSupplied.Value)
                {
                    return FrameUprightKitMaster.Value.SlantElev.Value;
                }
                else
                {
                    if (UprightType.Value == "L")
                    {
                        return 56;
                    }
                    else if (UprightType.Value == "LS")
                    {
                        return 36;
                    }
                    return 27;
                }
            });
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !IsStandalone.Value || !IsBentType.Value);
            });
        }
        public Rule<float> SlantInset { get; set; }
        protected virtual void ConfigureSlantInset(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (MasterSupplied.Value)
                {
                    return FrameUprightKitMaster.Value.SlantInset.Value;
                }
                else
                {
                    if (UprightType.Value == "L" || UprightType.Value == "LS")
                    {
                        return 12;
                    }
                    else if (UprightType.Value == "B")
                    {
                        if (UprightColumnSize.Value.Contains("C3"))
                        {
                            if (SlantElev.Value < 39)
                            {
                                return 8;
                            }
                            else if (SlantElev.Value < 51)
                            {
                                return 12;
                            }
                            else if (SlantElev.Value < 63)
                            {
                                return 17;
                            }
                            else if (SlantElev.Value < 75)
                            {
                                return 20;
                            }
                            else if (SlantElev.Value < 87)
                            {
                                return 25;
                            }
                            return 29;
                        }
                        else
                        {
                            if (SlantElev.Value < 39)
                            {
                                return 6;
                            }
                            else if (SlantElev.Value < 51)
                            {
                                return 10;
                            }
                            else if (SlantElev.Value < 63)
                            {
                                return 13;
                            }
                            else if (SlantElev.Value < 75)
                            {
                                return 16;
                            }
                            else if (SlantElev.Value < 87)
                            {
                                return 19;
                            }
                            return 22;
                        }
                    }
                    return 0;
                }
            });
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !IsStandalone.Value || !IsBentType.Value);
            });
        }

        public Rule<float> SlantInsetMax { get; set; }
        public Rule<float> BendToFirstHorizOffset { get; set; }

        public Rule<string> UprightColumnSize { get; set; }
        protected virtual void ConfigureUprightColumnSize(Rule<string> rule)
        {
            rule.Define(() => FrameUprightKitMaster.Value?.UprightColumnSize.Value ?? "C3x3.5");
            rule.AddToUI(ui =>
            {
                ui.Label.Define("Default Upright Column Size");
            });
        }

        public Rule<float> FootplateWidth { get; set; }
        protected virtual void ConfigureFootplateWidth(Rule<float> rule)
        {
            rule.Define(() => FrameUprightKitMaster.Value?.FootplateWidth.Value ?? DefaultFootplateWidth.Value);
            rule.AddToUI(ui =>
            {
                ui.Label.Define("Default Footplate Width");
            });
        }

        public Rule<float> FootplateThickness { get; set; }
        protected virtual void ConfigureFootplateThickness(Rule<float> rule)
        {
            rule.Define(() => FrameUprightKitMaster.Value?.FootplateThickness.Value ?? 0.375f);
            rule.AddToUI(ui =>
            {
                ui.Label.Define("Default Footplate Thickness");
            });
        }

        public Rule<float> HoleStartPunch { get; set; }
        protected virtual void ConfigureHoleStartPunch(Rule<float> rule)
        {
            rule.Define(3.125f);
            rule.AddToUI();
        }

        public Rule<string> FrontPostProtector { get; set; }
        protected virtual void ConfigureFrontPostProtector(Rule<string> rule)
        {
            rule.Define(() => FrameUprightKitMaster.Value?.FrontPostProtector.Value ?? "none");
        }

        public Rule<float> FrontPostProtectorHeight { get; set; }
        protected virtual void ConfigureFrontPostProtectorHeight(Rule<float> rule)
        {
            rule.Define(() => FrameUprightKitMaster.Value?.FrontPostProtectorHeight.Value ?? (FrontPostProtector.Value == "bullnose" ? 3.75f : 12));
        }

        public Rule<float> FrontDoublerHeightMin { get; set; }
        protected virtual void ConfigureFrontDoublerHeightMin(Rule<float> rule)
        {
            rule.Define(default(float));
        }

        public Rule<bool> HasFrontDoubler { get; set; }
        protected virtual void ConfigureHasFrontDoubler(Rule<bool> rule)
        {
            rule.Define(() => Frame.HasFrontDoubler.Value);
        }
        public Rule<float> FrontDoublerHeight { get; set; }
        protected virtual void ConfigureFrontDoublerHeight(Rule<float> rule)
        {
            rule.Define(() => Frame.FrontDoublerHeight.Value);
            rule.AddToUI(ui =>
            {
                //to be used for turning off the property
                ui.IsActive.Define(false);
            });
        }

        public Rule<string> FrontFootPlateType { get; set; }
        protected virtual void ConfigureFrontFootPlateType(Rule<string> rule)
        {
            rule.Define(() => Frame.FrontFootPlateType.Value);
        }

        public Rule<bool> HasRearDoubler { get; set; }
        protected virtual void ConfigureHasRearDoubler(Rule<bool> rule)
        {
            rule.Define(() => Frame.HasRearDoubler.Value);
        }

        public Rule<float> RearDoublerHeight { get; set; }
        protected virtual void ConfigureRearDoublerHeight(Rule<float> rule)
        {
            rule.Define(() => Frame.RearDoublerHeight.Value);
            rule.AddToUI(ui =>
            {
                ui.IsActive.Define(false);
            });
        }
        public Rule<float> RearDoublerHeightMin { get; set; }

        public Rule<string> RearPostProtector { get; set; }
        protected virtual void ConfigureRearPostProtector(Rule<string> rule)
        {
            rule.Define(() => Frame.RearPostProtector.Value);
        }
        public Rule<float> RearPostProtectorHeight { get; set; }
        protected virtual void ConfigureRearPostProtectorHeight(Rule<float> rule)
        {
            rule.Define(() => Frame.RearPostProtectorHeight.Value);
        }
        public Rule<string> RearFootPlateType { get; set; }
        protected virtual void ConfigureRearFootPlateType(Rule<string> rule)
        {
            rule.Define(() => Frame.RearFootPlateType.Value);
        }
        public Rule<int> FrontAnchorQty { get; set; }
        protected virtual void ConfigureFrontAnchorQty(Rule<int> rule)
        {
            rule.Define(DefaultAnchorQtyPerFootplate);
        }

        public Rule<int> RearAnchorQty { get; set; }
        protected virtual void ConfigureRearAnchorQty(Rule<int> rule)
        {
            rule.Define(DefaultAnchorQtyPerFootplate);
        }

        public Rule<string> FrontAnchorSize { get; set; }

        public Rule<string> RearAnchorSize { get; set; }
        
        public Rule<string> FrontAnchorType { get; set; }
        protected virtual void ConfigureFrontAnchorType(Rule<string> rule)
        {
            rule.Define(DefaultAnchorTypePerFootplate);
        }

        public Rule<string> FrontBullnoseAnchorType { get; set; }
        protected virtual void ConfigureFrontBullnoseAnchorType(Rule<string> rule)
        {
            rule.AddToUI(ui =>
            {
                ui.IsActive.Define(false);
            });
        }

        public Rule<string> RearAnchorType { get; set; }
        protected virtual void ConfigureRearAnchorType(Rule<string> rule)
        {
            rule.Define(DefaultAnchorTypePerFootplate);
        }
        public Rule<string> RearBullnoseAnchorType { get; set; }
        protected virtual void ConfigureRearBullnoseAnchorType(Rule<string> rule)
        {
            rule.AddToUI(ui =>
            {
                ui.IsActive.Define(false);
            });
        }

        public Rule<List<AnchorData>> AllAnchorData { get; set; }
        public Rule<List<ChoiceListItem>> AllAnchorTypesChoiceList { get; set; }
        public Rule<float> MaxHeight { get; set; }
        public Rule<string> Location { get; set; }
        //protected virtual void ConfigureLocation(Rule<string> rule)
        //{
        //    rule.Define(() =>
        //    {
        //        var bomLoc = Bom?.PlantCode.Value;
        //        return bomLoc != null && bomLoc.ToUpper().Contains("UT") ? "UT" : "GA";
        //    });
        //    rule.AddToUI(ui =>
        //    {
        //        ui.ReadOnly.Define(() => !IsStandalone.Value);
        //    });
        //    rule.ValueSet += (sender, e) =>
        //    {
        //        if (Height.Value > MaxHeight.Value)
        //            Height.Reset();
        //    };
        //}

        #endregion

        #region Children

        public FrameUpright Frame { get; set; }
        protected virtual void ConfigureFrame(FrameUpright child)
        {
            child.FrameUprightMaster.Define(() => FrameUprightKitMaster.Value?.Frame ?? default(FrameUpright));
            child.IsFrontFrame.Define(IsFrontFrame);
            child.Qty.Define(Qty);
            child.DefaultDepth.Define(DefaultDepth);
            child.Height.Define(Height);
            child.UprightColumnSize.Define(UprightColumnSize);
            child.FrontAnchorQty.Define(FrontAnchorQty);
            child.RearAnchorQty.Define(RearAnchorQty);
            child.FrontAnchorSize.Define(DefaultAnchorSizePerFootplate);
            child.RearAnchorSize.Define(DefaultAnchorSizePerFootplate);
            child.DefaultFrontAnchorType.Define(DefaultAnchorTypePerFootplate);
            child.DefaultRearAnchorType.Define(DefaultAnchorTypePerFootplate);
            child.HoleStartPunch.Define(HoleStartPunch);
            child.UprightType.Define(UprightType);
            child.SlantElev.Define(SlantElev);
            child.SlantInset.Define(SlantInset);
            child.FootplateWidth.Define(FootplateWidth);
            child.FootplateThickness.Define(FootplateThickness);
            child.Position.Define(() => Helper.Vxz(-child.Depth.Value / 2, 0));
        }

        public BoltOnHorizontalBrace BoltOnHeavyHorizontalBrace { get; set; }
        protected virtual void ConfigureBoltOnHeavyHorizontalBrace(BoltOnHorizontalBrace child)
        {
            child.IsActive.Define(() => Frame.UprightType.Value == "BH");
            child.DisplayName.Define($"Bolt-on Hvy Horizontal Brace 1");
            child.Bom.Item.Define(() => $"{Frame.Bom.Item.Value.PadLeft(3, '0')}-01");
            child.Id.Define(0);
            child.Color.Define(() => Frame.Color.Value);
            child.ChannelSize.Define(() => Frame.UprightColumnSize.Value);
            child.PanelHeight.Define(6);
            child.OffsetFromBottom.Define(6);
            child.CutForFrontDoubler.Define(() => (Frame.HasFrontDoubler.Value && (child.OffsetFromBottom.Value <= Frame.FrontDoublerHeight.Value)));
            child.CutForRearDoubler.Define(() => (Frame.HasRearDoubler.Value && (child.OffsetFromBottom.Value <= Frame.RearDoublerHeight.Value)));
            child.Length.Define(() => Frame.BoltOnHeavyHorizontalBraceLength.Value);
            child.Position.Define(() =>
            {
                Vector3 retVal = Helper.Vxy(-(Frame.Depth.Value / 2) + (child.Length.Value / 2), 6 + child.ChannelWebThickness.Value);

                if (child.CutForRearDoubler.Value || child.CutForFrontDoubler.Value)
                {
                    if (child.CutForFrontDoubler.Value)
                        retVal.X = (- (Frame.RearUprightColumnAssembly.UprightColumnChannel.ChannelDepth.Value * 2));
                    else
                        retVal.X = (- Frame.RearUprightColumnAssembly.UprightColumnChannel.ChannelWebThickness.Value);
                }

                return retVal;
            });
            child.Rotation.Define(() => Quaternion.CreateFromYawPitchRoll(Helper.DegToRad(-90), 0, Helper.DegToRad(270)));
        }

        #endregion

        #region Public Methods



        #endregion

        #region Protected Methods

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureBasePlateDataRules();
            this.ConfigureFrameUprightDataRules();
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(Colors.Gray);
        }

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define("FrameUprightKit");
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(true);
        }

        protected override void ConfigureAllowedChildTypes(ReadOnlyRule<IEnumerable<Type>> rule)
        {
            rule.Define(() => new List<Type>
            {
                typeof(CustomPart)
            });
        }

        #endregion

        #region Graphics

        //public Block FrameUprightBlock { get; set; }
        //protected virtual void ConfigureFrameUprightBlock(Block geo)
        //{
        //    geo.Width.Define(Depth);
        //    geo.Height.Define(Height);
        //    geo.Depth.Define(() => RearUprightColumn.ChannelWidth.Value);
        //    geo.Position.Define(() => Helper.Vy(Height.Value / 2));
        //}

        //public UCS UCS { get; set; }
        //protected virtual void ConfigureUCS(UCS child)
        //{
        //    child.Position.Define(() => (Helper.Vy(0)));
        //}

        #endregion

    }
}
