﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;
using Ruler.Rules.Validation;
using Ruler.Rules.Graphics;
using System.Numerics;
using System.Linq;
using StorageConfigurator.Data;
using Ruler.Cpq;

namespace StorageConfigurator.Rules
{
    public class FrameUpright : PaintedPart, IBasePlateData, IAngleData, IFrameUprightData, IRevisablePart, Ruler.Cpq.IBomPart
    {

        #region Constants

        const string PANELS_CATEGORY = "Panels";

        #endregion

        #region Constructors

        public FrameUpright(ModelContext modelContext, StorageConfiguratorRepository repo) : base(modelContext)
        {
            Repository = repo;
        }

        public FrameUpright(string name, Part parent, StorageConfiguratorRepository repo) : base(name, parent)
        {
            Repository = repo;
        }

        #endregion

        #region Rules


        public Rule<FrameUpright> FrameUprightMaster { get; set; }
        protected virtual void ConfigureFrameUprightMaster(Rule<FrameUpright> rule)
        {
            rule.Define(default(FrameUpright));
        }

        public Rule<bool> MasterSupplied { get; set; }
        protected virtual void ConfigureMasterSupplied(Rule<bool> rule)
        {
            rule.Define(() => FrameUprightMaster.Value != default(FrameUpright));
        }

        //TODO: Need info on this
        //protected virtual void ConfigureName(Rule<string> rule)
        //{
        //    rule.Define(default(string));
        //}

        //[CadRule(CadRuleUsageType.Property)]
        //public LookupRule<string> LineItemName { get; set; }
        //protected virtual void ConfigureLineItemName(LookupRule<string> rule)
        //{
        //    rule.Define(() => FrameUprightMaster.Value?.LineItemName.Value ?? "[[LineItemName]]");
        //}

        public Rule<bool> IsStandalone { get; set; }
        protected virtual void ConfigureIsStandalone(Rule<bool> rule)
        {
            rule.Define(() =>
            {
                if (MasterSupplied.Value)
                {
                    return FrameUprightMaster.Value.IsStandalone.Value;
                }
                return (Parent == null ||
                        Parent.Parent == null || 
                        (Parent.Parent.GetType() != typeof(SelectiveFrameType) && Parent.Parent.GetType() != typeof(DoubleDeepFrameType)));
            });
        }

        public Rule<double> Qty { get; set; }
        protected virtual void ConfigureQty(Rule<double> rule)
        {
            rule.Define(1);
            rule.AddToUI(ui =>
            {
                ui.Label.Define("Quantity");
                ui.ReadOnly.Define(() => !IsStandalone.Value);
            });
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> rulerDrawingXml { get; set; }
        protected virtual void ConfigurerulerDrawingXml(Rule<string> rule)
        {
            rule.Define("Upright");
        }

        #region Inputs Rules

        public Rule<bool> IsFrontFrame { get; set; }
        protected virtual void ConfigureIsFrontFrame(Rule<bool> rule)
        {
            rule.Define(IsStandalone);
        }

        public Rule<float> Height { get; set; }
        protected virtual void ConfigureHeight(Rule<float> rule)
        {
            rule.Define(360);
            rule.Min(HeightMin);
            rule.Max(MaxHeight);
            rule.MultipleOf(4);
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !IsStandalone.Value);
            });
        }

        [CadRule(CadRuleUsageType.Parameter, "Height")]
        public Rule<float> OverallHeight { get; set; }
        protected virtual void ConfigureOverallHeight(Rule<float> rule)
        {
            rule.Define(() => Height.Value + FootplateThickness.Value);
        }

        public Rule<float> HeightMin { get; set; }
        protected virtual void ConfigureHeightMin(Rule<float> rule)
        {
            rule.Define(() => UprightType.Value == "B" ? 80 : 12);
        }

        [UIRule]
        public Rule<float> Depth { get; set; }
        protected virtual void ConfigureDepth(Rule<float> rule)
        {
            rule.Define(() => FrameUprightMaster.Value?.Depth.Value ?? DefaultDepth.Value);
            rule.Min(12);
            rule.Max(72);
            rule.AddToCAD(cad =>
            {
                cad.CadRuleUsageType = CadRuleUsageType.Parameter;
                cad.NameInCad = "Width";
            });
        }

        public Rule<float> DefaultDepth { get; set; }
        protected virtual void ConfigureDefaultDepth(Rule<float> rule)
        {
            rule.Define(44);
        }

        //TODO: Add logic here
        public Rule<float> DefaultPanelHeight { get; set; }
        protected virtual void ConfigureDefaultPanelHeight(Rule<float> rule)
        {
            rule.Define(36);
        }

        [UIRule]
        public Rule<int> PanelQty { get; set; }
        protected virtual void ConfigurePanelQty(Rule<int> rule)
        {
            rule.Define(() => DefaultPanelHeights.Value.Count-1);
            rule.Min(1);
            rule.Max(12);
        }

        public Rule<float> RearColumnHeight { get; set; }
        protected virtual void ConfigureRearColumnHeight(Rule<float> rule)
        {
            rule.Define(() => Height.Value);
        }

        public Rule<float> FrontColumnHeight { get; set; }
        protected virtual void ConfigureFrontColumnHeight(Rule<float> rule)
        {
            rule.Define(() => Height.Value);
        }

        public Rule<float> FrameWidth { get; set; }
        protected virtual void ConfigureFrameWidth(Rule<float> rule)
        {
            rule.Define(() => FrontUprightColumnAssembly.OverallWidth.Value);
            rule.AddToCAD(cad =>
            {
                cad.CadRuleUsageType = CadRuleUsageType.Parameter;
                cad.NameInCad = "Depth";
                //drives workpoints in FrameUpright part
            });
        }

        [CadRule(CadRuleUsageType.Property, "HasFrontDoubler")]
        public Rule<string> HasFrontDoublerProp { get; set; }
        protected virtual void ConfigureHasFrontDoublerProp(Rule<string> rule)
        {
            rule.Define(() => HasFrontDoubler.Value.ToString());
        }

        [CadRule(CadRuleUsageType.Property, "HasRearDoubler")]
        public Rule<string> HasRearDoublerProp { get; set; }
        protected virtual void ConfigureHasRearDoublerProp(Rule<string> rule)
        {
            rule.Define(() => HasRearDoubler.Value.ToString());
        }

        public Rule<bool> IsSlantType { get; set; }
        protected virtual void ConfigureIsSlantType(Rule<bool> rule)
        {
            rule.Define(() => UprightType.Value == "LS" || UprightType.Value == "L");
        }

        public Rule<bool> IsBentType { get; set; }
        protected virtual void ConfigureIsBentType(Rule<bool> rule)
        {
            rule.Define(() => UprightType.Value == "B");
        }

        public Rule<float> DefaultFootplateWidth { get; set; }
        protected virtual void ConfigureDefaultFootplateWidth(Rule<float> rule)
        {
            rule.Define(() => (UprightType.Value == "S" ? FrontUprightColumnAssembly.ColumnChannelWidth.Value + 1 : FrontUprightColumnAssembly.ColumnChannelWidth.Value + 2));
        }

        public Rule<string> DefaultFrontAnchorType { get; set; }
        protected virtual void ConfigureDefaultFrontAnchorType(Rule<string> rule)
        {
            rule.Define(default(string));
        }

        public Rule<string> DefaultRearAnchorType { get; set; }
        protected virtual void ConfigureDefaultRearAnchorType(Rule<string> rule)
        {
            rule.Define(default(string));
        }

        #endregion

        #region Panels Rules

        //TODO: Change this to a list of props for tab
        public Rule<float> PanelHeight { get; set; }
        protected virtual void ConfigurePanelHeight(Rule<float> rule)
        {
            rule.Define(12);
            rule.Min(12);
            rule.Max(60);
        }

        [UIRule]
        public Rule<string> DiagonalType { get; set; }
        protected virtual void ConfigureDiagonalType(Rule<string> rule)
        {
            rule.Define(() => FrameUprightMaster.Value?.DiagonalType.Value ?? @"1-1/2 x 1-1/2 x 1/8");
            rule.AddToUI(ui =>
            {
                ui.Label.Define("Default Diagonal Type");
                ui.Category.Define(PANELS_CATEGORY);
                ui.ChoiceList.Define(() =>
                {
                    List<string> choices = new List<string>() { @"1-1/2 x 1-1/2 x 1/8", @"2 x 2 x 1/8", @"2 x 2 x 3/16", @"3 x 2 x 3/16" };
                    return Helper.GetFilteredChoices(AllAngleTypesChoiceList.Value, choices);
                });
            });
        }

        [UIRule]
        public Rule<string> DefaultHorizontalType { get; set; }
        protected virtual void ConfigureDefaultHorizontalType(Rule<string> rule)
        {
            rule.Define(() => FrameUprightMaster.Value?.DefaultHorizontalType.Value ?? @"1-1/2 x 1-1/2 x 1/8");
            rule.AddToUI(ui =>
            {
                ui.Category.Define(PANELS_CATEGORY);
                ui.ChoiceList.Define(() =>
                {
                    List<string> choices = new List<string>() { @"1-1/2 x 1-1/2 x 1/8", @"2 x 2 x 1/8", @"2 x 2 x 3/16" };
                    return Helper.GetFilteredChoices(AllAngleTypesChoiceList.Value, choices);
                });
            });
        }

        public Rule<Quaternion> HorizontalBraceRotation { get; set; }
        protected virtual void ConfigureHorizontalBraceRotation(Rule<Quaternion> rule)
        {
            rule.Define(() => Quaternion.CreateFromYawPitchRoll(Helper.DegToRad(-90), 0, Helper.DegToRad(180)));
        }

        public Rule<Quaternion> FlippedHorizontalBraceRotation { get; set; }
        protected virtual void ConfigureFlippedHorizontalBraceRotation(Rule<Quaternion> rule)
        {
            rule.Define(() => Helper.CreateRotation(Vector3.UnitY, -90));
        }

        public Rule<float> BottomHorizontalOffset { get; set; }
        protected virtual void ConfigureBottomHorizontalOffset(Rule<float> rule)
        {
            rule.Define(() => UprightType.Value == "T" || UprightType.Value == "BH" ? 10 : 6);
        }

        public Rule<float> MinHorizontalOffsetFromTop { get; set; }
        protected virtual void ConfigureMinHorizontalOffsetFromTop(Rule<float> rule)
        {
            rule.Define(6);
        }

        public Rule<float> MaxHorizontalOffsetFromTop { get; set; }
        protected virtual void ConfigureMaxHorizontalOffsetFromTop(Rule<float> rule)
        {
            rule.Define(24);
        }

        public Rule<float> BottomTwoPanelHeight { get; set; }
        protected virtual void ConfigureBottomTwoPanelHeight(Rule<float> rule)
        {
            rule.Define(36);
        }

        public Rule<List<float>> DefaultPanelHeights { get; set; }
        protected virtual void ConfigureDefaultPanelHeights(Rule<List<float>> rule)
        {
            rule.Define(() =>
            {
                List<float> heights = new List<float>();
                float remainingSpace = Height.Value - BottomHorizontalOffset.Value;

                //First brace location from the bottom
                heights.Add(BottomHorizontalOffset.Value);

                //Special logic for one panel configurations
                if (Height.Value < 84)
                {
                    if (Height.Value < 48)
                        heights.Add(remainingSpace - MinHorizontalOffsetFromTop.Value);
                    else if (Height.Value < 60)
                        heights.Add(BottomTwoPanelHeight.Value);
                    else if (Height.Value < 72)
                        heights.Add(48);
                    else
                        heights.Add(60);
                }
                else
                {
                    if (IsSlantType.Value && IsFrontFrame.Value)
                    {
                        heights.AddRange(SlantTypeSpecialPanelHeights.Value);
                        remainingSpace = remainingSpace - (SlantTypeSpecialPanelHeights.Value.Sum());
                    }
                    else if (IsBentType.Value && IsFrontFrame.Value)
                    {
                        heights.AddRange(BentTypeSpecialPanelHeights.Value);
                        remainingSpace = remainingSpace - (BentTypeSpecialPanelHeights.Value.Sum());
                    }
                    else
                    {
                        //Special logic for Bolt On Heavy and Tipppman where 
                        //the first panel is measured from the heavy horizontal
                        if (UprightType.Value == "T" || UprightType.Value == "BH")
                            heights.Add(BottomTwoPanelHeight.Value - 4);
                        else
                            heights.Add(BottomTwoPanelHeight.Value);
                        heights.Add(BottomTwoPanelHeight.Value);

                        remainingSpace = remainingSpace - (heights[1] + heights[2]);
                    }

                    if (remainingSpace > MaxHorizontalOffsetFromTop.Value)
                    {
                        int numFortyEightPanels = (int)Helper.RoundDownToNearest((remainingSpace-MinHorizontalOffsetFromTop.Value) / 48, 1);

                        for (var i = 1; i < numFortyEightPanels+1; i++)
                        {
                            heights.Add(48);
                            remainingSpace = remainingSpace - 48;
                        }

                        //Add another panel if the remaining space is greater than the Max Offset
                        if (remainingSpace > MaxHorizontalOffsetFromTop.Value)
                            heights.Add(remainingSpace - MinHorizontalOffsetFromTop.Value);
                    }
                }
                
                return heights;
            });
        }

        public Rule<bool> RequiresMPanel { get; set; }
        protected virtual void ConfigureRequiresMPanel(Rule<bool> rule)
        {
            rule.Define(() => (Height.Value - HorizontalBrace.Last().Position.Value.Y) > 12);
        }

        public Rule<List<float>> SlantTypeSpecialPanelHeights { get; set; }
        protected virtual void ConfigureSlantTypeSpecialPanelHeights(Rule<List<float>> rule)
        {
            rule.Define(() =>
            {
                List<float> retVal = new List<float>();

                //special panel location logic for slant type
                if (UprightType.Value == "LS")
                {
                    retVal.Add(30);
                    retVal.Add(BottomTwoPanelHeight.Value);
                }
                else
                {
                    retVal.Add(24);
                    retVal.Add(26);
                }

                return retVal;
            });
        }

        public Rule<List<float>> BentTypeSpecialPanelHeights { get; set; }
        protected virtual void ConfigureBentTypeSpecialPanelHeights(Rule<List<float>> rule)
        {
            rule.Define(() =>
            {
                List<float> retVal = new List<float>();

                if (SlantElev.Value < 51)
                    retVal.Add((SlantElev.Value - BottomHorizontalOffset.Value) + BendToFirstHorizOffset.Value);
                else if (SlantElev.Value < 75)
                {
                    retVal.Add(24);
                    retVal.Add((SlantElev.Value - (BottomHorizontalOffset.Value + 24)) + BendToFirstHorizOffset.Value);
                }
                else if (SlantElev.Value < 87)
                {
                    retVal.Add(36);
                    retVal.Add((SlantElev.Value - (BottomHorizontalOffset.Value + 36)) + BendToFirstHorizOffset.Value);
                }
                else
                {
                    retVal.Add(24);
                    retVal.Add(24);
                    retVal.Add((SlantElev.Value - (BottomHorizontalOffset.Value + 48)) + BendToFirstHorizOffset.Value);
                }

                return retVal;
            });
        }

        public Rule<float> DiagonalOffsetFromVertical { get; set; }
        protected virtual void ConfigureDiagonalOffsetFromVertical(Rule<float> rule)
        {
            rule.Define(0.5f);
        }

        public Rule<int> HeavyHorizontalBraceQty { get; set; }
        protected virtual void ConfigureHeavyHorizontalBraceQty(Rule<int> rule)
        {
            rule.Define(() =>
            {
                switch (UprightType.Value)
                {
                    case "H":
                    case "D":
                    case "L":
                    case "LS":
                    case "B":
                        return 1;
                    case "BTH":
                        return 2;
                    default:
                        return 0;
                }
            });
        }

        public Rule<float> BoltOnHeavyHorizontalBraceLength { get; set; }
        protected virtual void ConfigureBoltOnHeavyHorizontalBraceLength(Rule<float> rule)
        {
            rule.Define(() =>
            {
                float retLength = Depth.Value - (RearUprightColumnAssembly.UprightColumnChannel.ChannelDepth.Value * 2);

                if (HasRearDoubler.Value)
                    retLength = (retLength - RearUprightColumnAssembly.Doubler.ChannelDepth.Value);

                if (HasFrontDoubler.Value)
                    retLength = (retLength - FrontUprightColumnAssembly.Doubler.ChannelDepth.Value);

                return retLength;
            });
        }

        public Rule<int> HeavyHorizontalFrontWeldPlateQty { get; set; }
        protected virtual void ConfigureHeavyHorizontalFrontWeldPlateQty(Rule<int> rule)
        {
            rule.Define(() => HasFrontDoubler.Value || (IsSlantType.Value && IsFrontFrame.Value) ? 0 : HeavyHorizontalBraceQty.Value);
        }


        public Rule<int> HeavyHorizontalRearWeldPlateQty { get; set; }
        protected virtual void ConfigureHeavyHorizontalRearWeldPlateQty(Rule<int> rule)
        {
            rule.Define(() => HasRearDoubler.Value ? 0 : HeavyHorizontalBraceQty.Value);
        }

        public Rule<float> TippmannHorizontalLength { get; set; }
        protected virtual void ConfigureTippmannHorizontalLength(Rule<float> rule)
        {
            rule.Define(() => Depth.Value - (RearUprightColumnAssembly.UprightColumnChannel.ChannelDepth.Value * 4));
        }

        public Rule<float> SlantAngle { get; set; }
        protected virtual void ConfigureSlantAngle(Rule<float> rule)
        {
            rule.Define(() =>
            {
                //use Pythagorean Theorem
                float a = SlantInset.Value;
                float b = BentTangentLineElev.Value;
                float hyp = (float)Math.Sqrt((Math.Pow(a, 2) + Math.Pow(b, 2)));
                double angleInRad = Math.Asin(a / hyp);

                return Helper.RadToDeg((float)angleInRad);
            });
        }

        public Rule<float> BentTangentLineElev { get; set; }
        protected virtual void ConfigureBentTangentLineElev(Rule<float> rule)
        {
            rule.Define(() => SlantElev.Value - 9.5f);
        }

        public Rule<float> HeavyHorizontalRearMiterAngle { get; set; }
        protected virtual void ConfigureHeavyHorizontalRearMiterAngle(Rule<float> rule)
        {
            rule.Define(() =>
            {
                float retVal = 0;

                if (IsFrontFrame.Value)
                {
                    if (IsSlantType.Value)
                    {
                        if (UprightType.Value == "LS")
                            retVal = 18.4f;
                        else
                        {
                            switch (UprightColumnSize.Value)
                            {
                                case "C3x4.1":
                                case "C4x4.5":
                                case "C4x5.4":
                                    retVal = 11;
                                    break;
                                default:
                                    retVal = 12;
                                    break;
                            }
                        }
                    }
                    else if (IsBentType.Value)
                        retVal = SlantAngle.Value;
                }

                return retVal;
            });
        }

        public Rule<List<float>> HorizontalYLocations { get; set; }
        protected virtual void ConfigureHorizontalYLocations(Rule<List<float>> rule)
        {
            rule.Define(() =>
            {
                List<float> locs = new List<float>();

                locs.AddRange(HorizontalBrace.Select(b => b.OffsetFromBottom.Value).ToList());

                if (RequiresMPanel.Value)
                    locs.Add(MPanelBrace.First().OffsetFromBottom.Value);

                return locs;
            });
        }

        #endregion

        #endregion

        #region IBasePlateData Implementation

        public Rule<List<BasePlateTypeData>> AllBasePlateData { get; set; }
        public Rule<List<ChoiceListItem>> BasePlateTypeChoiceList { get; set; }
        public Rule<List<ChoiceListItem>> PostProtectorChoices { get; set; }

        #endregion

        #region IAngleData Implementation
        public Rule<List<AngleData>> AllAngleData { get; set; }
        public Rule<List<ChoiceListItem>> AllAngleTypesChoiceList { get; set; }
        #endregion

        #region IFrameUprightData Implementation

        public Rule<string> UprightType { get; set; }
        protected virtual void ConfigureUprightType(Rule<string> rule)
        {
            rule.Define(() => FrameUprightMaster.Value?.UprightType.Value ?? "S");
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !IsStandalone.Value && IsFrontFrame.Value);
            });
            rule.AddToCAD(cad => cad.CadRuleUsageType = CadRuleUsageType.Property);
            rule.ValueSet += (sender, e) =>
            {
                HasFrontDoubler.Reset();
                HasRearDoubler.Reset();

                if (e.NewValue == "B" || e.NewValue == "L" || e.NewValue == "LS")
                    FrontPostProtector.Reset();

                if (e.OldValue == "B")
                {
                    SlantElev.Reset();
                    SlantInset.Reset();
                }
            };
        }

        public Rule<bool> Slanted { get; set; }
        protected virtual void ConfigureSlanted(Rule<bool> rule)
        {
            rule.Define(() => FrameUprightMaster.Value?.Slanted.Value ?? (UprightType.Value == "B" || UprightType.Value == "L" || UprightType.Value == "LS"));
        }

        public Rule<float> SlantElev { get; set; }
        protected virtual void ConfigureSlantElev(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (MasterSupplied.Value)
                {
                    return FrameUprightMaster.Value.SlantElev.Value;
                }

                if (UprightType.Value == "L")
                {
                    return 56;
                }
                else if (UprightType.Value == "LS")
                {
                    return 36;
                }
                return 27;
            });
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !IsStandalone.Value || !IsBentType.Value);
            });
        }
        public Rule<float> SlantInset { get; set; }
        protected virtual void ConfigureSlantInset(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (MasterSupplied.Value)
                {
                    return FrameUprightMaster.Value.SlantInset.Value;
                }

                if (UprightType.Value == "L" || UprightType.Value == "LS")
                {
                    return 12;
                }
                else if (UprightType.Value == "B")
                {
                    if (UprightColumnSize.Value.Contains("C3"))
                    {
                        if (SlantElev.Value < 39)
                        {
                            return 8;
                        }
                        else if (SlantElev.Value < 51)
                        {
                            return 12;
                        }
                        else if (SlantElev.Value < 63)
                        {
                            return 17;
                        }
                        else if (SlantElev.Value < 75)
                        {
                            return 20;
                        }
                        else if (SlantElev.Value < 87)
                        {
                            return 25;
                        }
                        return 29;
                    }
                    else
                    {
                        if (SlantElev.Value < 39)
                        {
                            return 6;
                        }
                        else if (SlantElev.Value < 51)
                        {
                            return 10;
                        }
                        else if (SlantElev.Value < 63)
                        {
                            return 13;
                        }
                        else if (SlantElev.Value < 75)
                        {
                            return 16;
                        }
                        else if (SlantElev.Value < 87)
                        {
                            return 19;
                        }
                        return 22;
                    }
                }
                return 0;
            });
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !IsStandalone.Value || !IsBentType.Value);
            });
        }
        public Rule<float> SlantInsetMax { get; set; }
        public Rule<float> BendToFirstHorizOffset { get; set; }

        public Rule<string> UprightColumnSize { get; set; }
        protected virtual void ConfigureUprightColumnSize(Rule<string> rule)
        {
            rule.Define(() => FrameUprightMaster.Value?.UprightColumnSize.Value ?? "C3x3.5");
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !IsStandalone.Value);
            });
        }

        public Rule<float> FootplateWidth { get; set; }
        protected virtual void ConfigureFootplateWidth(Rule<float> rule)
        {
            rule.Define(() => FrameUprightMaster.Value?.FootplateWidth.Value ?? DefaultFootplateWidth.Value);
        }

        public Rule<float> FootplateThickness { get; set; }

        public Rule<float> HoleStartPunch { get; set; }
        protected virtual void ConfigureHoleStartPunch(Rule<float> rule)
        {
            rule.Define(3.125f);
            rule.AddToUI();
            rule.AddToCAD(cad =>
            {
                cad.CadRuleUsageType = CadRuleUsageType.Parameter;
            });
        }

        public Rule<string> FrontPostProtector { get; set; }
        protected virtual void ConfigureFrontPostProtector(Rule<string> rule)
        {
            rule.Define(() => FrameUprightMaster.Value?.FrontPostProtector.Value ?? "none");
        }

        public Rule<float> FrontPostProtectorHeight { get; set; }
        protected virtual void ConfigureFrontPostProtectorHeight(Rule<float> rule)
        {
            rule.Define(() => FrameUprightMaster.Value?.FrontPostProtectorHeight.Value ?? (FrontPostProtector.Value == "bullnose" ? 3.75f : 12));
        }

        public Rule<float> FrontDoublerHeightMin { get; set; }
        protected virtual void ConfigureFrontDoublerHeightMin(Rule<float> rule)
        {
            rule.Define(() =>
            {
                float retVal = HasFrontDoubler.Value ? 12 : 0;

                if (UprightType.Value == "B")
                    retVal = SlantElev.Value + 48;
                else if (Slanted.Value)
                    retVal = retVal + SlantElev.Value - 4;
                else if (UprightType.Value == "D")
                    return 8;

                return retVal;
            });
        }

        public Rule<bool> HasFrontDoubler { get; set; }
        protected virtual void ConfigureHasFrontDoubler(Rule<bool> rule)
        {
            rule.Define(() =>
            {
                if (MasterSupplied.Value)
                {
                    return FrameUprightMaster.Value.HasFrontDoubler.Value;
                }

                if (UprightType.Value == "D" || UprightType.Value == "T" || UprightType.Value == "L" || UprightType.Value == "LS")
                    return true;
                else
                    return ((IsFrontFrame.Value && (UprightType.Value == "T" || UprightType.Value == "B")));
            });
        }
        public Rule<float> FrontDoublerHeight { get; set; }
        protected virtual void ConfigureFrontDoublerHeight(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (MasterSupplied.Value)
                {
                    return FrameUprightMaster.Value.FrontDoublerHeight.Value;
                }

                if (!HasFrontDoubler.Value)
                {
                    return 0;
                }
                else if (IsFrontFrame.Value && IsBentType.Value)
                    return FrontDoublerHeightMin.Value;
                else
                {
                    if (FrontColumnHeight.Value > 84)
                        return 84;
                    else
                        return Helper.RoundDownToNearest(FrontColumnHeight.Value, increment: 4);
                }
            });
            rule.Min(FrontDoublerHeightMin);
            rule.Max(Height);
            rule.MultipleOf(4);
            rule.NoInterferenceWithHorizontals(HorizontalYLocations);
            rule.AddToUI(ui =>
            {
                //to be used for turning off the property
                ui.ReadOnly.Define(() => !HasFrontDoubler.Value);
            });
            rule.AddToCAD(cad =>
            {
                cad.CadRuleUsageType = CadRuleUsageType.Property;
                cad.IsActive.Define(HasFrontDoubler);
            });
        }

        public Rule<string> FrontFootPlateType { get; set; }
        protected virtual void ConfigureFrontFootPlateType(Rule<string> rule)
        {
            rule.Define(() =>
            {
                if (MasterSupplied.Value)
                {
                    return FrameUprightMaster.Value.FrontFootPlateType.Value;
                }

                BasePlateTypeData bpData = AllBasePlateData.Value.FirstOrDefault(d =>
                {
                    return (d.HasDoubler == HasFrontDoubler.Value &&
                        d.PostProtector == FrontPostProtector.Value &&
                        d.AnchorQty == FrontAnchorQty.Value);
                });

                if (bpData != null)
                {
                    return bpData.BasePlateType;
                }
                return BasePlateTypeChoiceList.Value.First().Text;
            });
        }

        public Rule<bool> HasRearDoubler { get; set; }
        protected virtual void ConfigureHasRearDoubler(Rule<bool> rule)
        {
            rule.Define(() => FrameUprightMaster.Value?.HasRearDoubler.Value ?? (UprightType.Value == "T" || UprightType.Value == "D"));
        }

        public Rule<float> RearDoublerHeight { get; set; }
        protected virtual void ConfigureRearDoublerHeight(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (!HasRearDoubler.Value) return 0;
                return FrameUprightMaster.Value?.RearDoublerHeight.Value ?? (RearColumnHeight.Value > 84 ? 84 : Helper.RoundDownToNearest(RearColumnHeight.Value, increment: 4));
            });
            rule.Max(Height);
            rule.MultipleOf(4);
            rule.NoInterferenceWithHorizontals(HorizontalYLocations);
            rule.AddToUI(ui =>
            {
                //to be used for turning off the property
                ui.ReadOnly.Define(() => !HasRearDoubler.Value);
            });
            rule.AddToCAD(cad =>
            {
                cad.CadRuleUsageType = CadRuleUsageType.Property;
                cad.IsActive.Define(HasRearDoubler);
            });
        }
        public Rule<float> RearDoublerHeightMin { get; set; }

        public Rule<string> RearPostProtector { get; set; }
        protected virtual void ConfigureRearPostProtector(Rule<string> rule)
        {
            rule.Define(() => FrameUprightMaster.Value?.RearPostProtector.Value ?? "none");
        }
        public Rule<float> RearPostProtectorHeight { get; set; }
        protected virtual void ConfigureRearPostProtectorHeight(Rule<float> rule)
        {
            rule.Define(() => FrameUprightMaster.Value?.RearPostProtectorHeight.Value ?? (RearPostProtector.Value == "bullnose" ? 3.75f : 12));
        }
        public Rule<string> RearFootPlateType { get; set; }
        protected virtual void ConfigureRearFootPlateType(Rule<string> rule)
        {
            rule.Define(() =>
            {
                if (MasterSupplied.Value)
                {
                    return FrameUprightMaster.Value.RearFootPlateType.Value;
                }

                BasePlateTypeData bpData = AllBasePlateData.Value.FirstOrDefault(d =>
                {
                    return (d.HasDoubler == HasRearDoubler.Value &&
                        d.PostProtector == RearPostProtector.Value &&
                        d.AnchorQty == RearAnchorQty.Value);
                });

                if (bpData != null)
                {
                    return bpData.BasePlateType;
                }
                return BasePlateTypeChoiceList.Value.First().Text;
            });
        }
        public Rule<int> FrontAnchorQty { get; set; }
        protected virtual void ConfigureFrontAnchorQty(Rule<int> rule)
        {
            rule.Define(() => FrameUprightMaster.Value?.FrontAnchorQty.Value ?? 2);
        }

        public Rule<int> RearAnchorQty { get; set; }
        protected virtual void ConfigureRearAnchorQty(Rule<int> rule)
        {
            rule.Define(() => FrameUprightMaster.Value?.RearAnchorQty.Value ?? 2);
        }

        public Rule<string> FrontAnchorSize { get; set; }
        protected virtual void ConfigureFrontAnchorSize(Rule<string> rule)
        {
            rule.ValueSet += (sender, e) =>
            {
                FrontAnchorType.Reset();
            };
            rule.ValueReset += (sender, e) =>
            {
                FrontAnchorType.Reset();
            };
        }

        public Rule<string> RearAnchorSize { get; set; }
        protected virtual void ConfigureRearAnchorSize(Rule<string> rule)
        {
            rule.ValueSet += (sender, e) =>
            {
                RearAnchorType.Reset();
            };
            rule.ValueReset += (sender, e) =>
            {
                RearAnchorType.Reset();
            };
        }
        public Rule<string> FrontAnchorType { get; set; }
        protected virtual void ConfigureFrontAnchorType(Rule<string> rule)
        {
            rule.Define(() =>
            {
                if (FrontAnchorSize.IsOverridden)
                {
                    return rule.UIMetadata.ChoiceList.Value.First().Value.ToString();
                }
                else
                {
                    return DefaultFrontAnchorType.Value;
                }
            });
        }

        public Rule<string> FrontBullnoseAnchorType { get; set; }
        protected virtual void ConfigureFrontBullnoseAnchorType(Rule<string> rule)
        {
            rule.Define(() => FrontPostProtector.Value.ToLower().Contains("bull") ? FrontAnchorType.Value : "");
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !FrontPostProtector.Value.ToLower().Contains("bull"));
            });
        }

        public Rule<string> RearAnchorType { get; set; }
        protected virtual void ConfigureRearAnchorType(Rule<string> rule)
        {
            rule.Define(() =>
            {
                if (RearAnchorSize.IsOverridden)
                {
                    return rule.UIMetadata.ChoiceList.Value.First().Value.ToString();
                }
                else
                {
                    return DefaultRearAnchorType.Value;
                }
            });
        }
        public Rule<string> RearBullnoseAnchorType { get; set; }
        protected virtual void ConfigureRearBullnoseAnchorType(Rule<string> rule)
        {
            rule.Define(() => RearPostProtector.Value.ToLower().Contains("bull") ? RearAnchorType.Value : "");
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !RearPostProtector.Value.ToLower().Contains("bull"));
            });
        }

        public Rule<List<AnchorData>> AllAnchorData { get; set; }
        public Rule<List<ChoiceListItem>> AllAnchorTypesChoiceList { get; set; }
        public Rule<float> MaxHeight { get; set; }
        public Rule<string> Location { get; set; }
        protected virtual void ConfigureLocation(Rule<string> rule)
        {
            rule.Define(() =>
            {
                var bomLoc = Bom?.PlantCode.Value;
                return bomLoc != null && bomLoc.ToUpper().Contains("UT") ? "UT" : "GA";
            });
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !IsStandalone.Value);
            });
            rule.ValueSet += (sender, e) =>
            {
                if (Height.Value > MaxHeight.Value)
                    Height.Reset();
            };
        }

        public Rule<float> HvyHorizontalClipDiagonalBtmVertOffset { get; set; }
        protected virtual void ConfigureHvyHorizontalClipDiagonalBtmVertOffset(Rule<float> rule)
        {
            rule.Define(0.255f);
        }

        public Rule<float> HeavyHorizontalClipDiagonalHorizOffset { get; set; }
        protected virtual void ConfigureHeavyHorizontalClipDiagonalHorizOffset(Rule<float> rule)
        {
            rule.Define(0.75f);
        }

        #endregion

        #region IBomPart Implementation

        protected virtual void ConfigurePartNumberPrefix(Rule<string> rule)
        {
            rule.Define(() => $"{Description.Value}-");
        }

        protected virtual void ConfigureDescription(Rule<string> rule)
        {
            rule.Define(() =>
            {
                var columnId = GetColumnSizeAbbrev(FrontUprightColumnAssembly.UprightColumnSize.Value);
                string retVal = $"U{GetUprightTypeDescriptionAbbrev()}{columnId}{PanelQty.Value.ToString()}";

                if (FrontUprightColumnAssembly.HasDoubler.Value)
                {
                    retVal = retVal + "F";
                }

                if (RearUprightColumnAssembly.HasDoubler.Value)
                {
                    retVal = retVal + "R";
                }

                return retVal;
            });
            rule.AddToCAD(cad =>
            {
                cad.CadRuleUsageType = CadRuleUsageType.Parameter;
            });
        }

        protected virtual void ConfigureBOMDescription(Rule<string> rule)
        {
            rule.Define(() =>
            {
                var ppDescFront = FrontPostProtector.Value == "none" ? "NO" : FrontPostProtector.Value == "angle" ? "AP" : "BP";
                var ppDescRear = RearPostProtector.Value == "none" ? "NO" : RearPostProtector.Value == "angle" ? "AP" : "BP";
                var btmHorzType = this.GetBtmHorizontalAbbreviation();
                var frontDoublerHeightDesc = HasFrontDoubler.Value ? $"{FrontDoublerHeight.Value}," : "";
                var rearDoublerHeightDesc = HasRearDoubler.Value ? $"{RearDoublerHeight.Value}," : "";

                var retVal = $"{Description.Value} ({Height.Value},{Depth.Value},{frontDoublerHeightDesc}{rearDoublerHeightDesc}{ppDescFront},{btmHorzType})";

                retVal = retVal + $"|Upright {FrontUprightColumnAssembly.UprightColumnSize.Value}#, {Depth.Value} x {Height.Value} high";

                var frontDoublerDesc = HasFrontDoubler.Value ? $"{FrontDoublerHeight.Value} front dblr, " : "";
                var rearDoublerDesc = HasRearDoubler.Value ? $"{RearDoublerHeight.Value} rear dblr, " : "";
                var columnProtectorDesc = FrontPostProtector.Value == "none" ? "" : $", {FrontPostProtectorHeight.Value:#.##}\" {ppDescFront} Front PP";
                columnProtectorDesc += RearPostProtector.Value == "none" ? ""
                    : $", {RearPostProtectorHeight.Value:#.##}\" Rear PP";
                retVal = retVal + "|" + frontDoublerDesc + rearDoublerDesc + $"{btmHorzType} BH" + columnProtectorDesc;

                var diagShortDesc = this.GetAngleShortDesc(DiagonalBrace.FirstOrDefault()?.AngleType.Value);
                var horzShortDesc = this.GetAngleShortDesc(HorizontalBrace.LastOrDefault()?.AngleType.Value);
                retVal = retVal + $"|{diagShortDesc} diag bracing, {horzShortDesc} horz bracing";

                var panelDescription = string.Join(", ", DefaultPanelHeights.Value.Skip(1).Select(h => $"{h:#.##}"));
                retVal += $"|panels {panelDescription}";

                var frontFp = FrontUprightColumnAssembly.Footplate;
                var rearFp = RearUprightColumnAssembly.Footplate;
                retVal = retVal + $"|Front FP {frontFp.Description.Value}";
                retVal = retVal + $"|Rear FP {rearFp.Description.Value}";
                retVal = retVal + $"|Front FP {frontFp.Type.Value}, Rear FP {rearFp.Type.Value}, {ColorBOMName.Value}";

                var punchingCenters = FrontUprightColumnAssembly.HoleSpacing.Value;
                retVal = retVal + $"|Upright punched on {punchingCenters:#.##}\" Centers";

                return retVal;
            });
        }

        protected virtual void ConfigurePartWeight(Rule<double> rule)
        {
            rule.Define(() => this.GetPartWeightWithoutPaint());
        }
        
        #endregion

        #region CPQ.IBomPart Implementation

        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {
            child.IsActive.Define(() => FrameUprightMaster.Value == default(FrameUpright));
            child.PartNumber.Define(PartNumber);
            child.ItemQty.Define(Qty);
            child.Description.Define(BOMDescription1);
        }

        #endregion

        #region Labor Hour Rules

        public Rule<double> PaintCarriersPerHour { get; set; }
        protected virtual void ConfigurePaintCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> PaintPartsPerCarrier { get; set; }
        protected virtual void ConfigurePaintPartsPerCarrier(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> PaintPartsPerCarrierFactor { get; set; }
        protected virtual void ConfigurePaintPartsPerCarrierFactor(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> UnloadPartsPerCarrier { get; set; }
        protected virtual void ConfigureUnloadPartsPerCarrier(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> UnloadPartsPerCarrierFactor { get; set; }
        protected virtual void ConfigureUnloadPartsPerCarrierFactor(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> UnloadCarriersPerHour { get; set; }
        protected virtual void ConfigureUnloadCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public ReadOnlyRule<double> Weight { get; set; }
        protected virtual void ConfigureWeight(ReadOnlyRule<double> rule)
        {
            rule.Define(() => Bom.UnitWeight.Value);
        }

        public Rule<double> WeldBaseLabor { get; set; }
        protected virtual void ConfigureWeldBaseLabor(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> FrontDoublerWeldAddon { get; set; }
        protected virtual void ConfigureFrontDoublerWeldAddon(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> FrontEDH { get; set; }
        protected virtual void ConfigureFrontEDH(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> RearDoublerWeldAddon { get; set; }
        protected virtual void ConfigureRearDoublerWeldAddon(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> RearEDH { get; set; }
        protected virtual void ConfigureRearEDH(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> FrontDoublerCapWeldAddon { get; set; }
        protected virtual void ConfigureFrontDoublerCapWeldAddon(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> RearDoublerCapWeldAddon { get; set; }
        protected virtual void ConfigureRearDoublerCapWeldAddon(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> FrontAngleProtectorHeightFactor { get; set; }
        protected virtual void ConfigureFrontAngleProtectorHeightFactor(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> RearAngleProtectorHeightFactor { get; set; }
        protected virtual void ConfigureRearAngleProtectorHeightFactor(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> FrontAngleProtectorWeldAddon { get; set; }
        protected virtual void ConfigureFrontAngleProtectorWeldAddon(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> RearAngleProtectorWeldAddon { get; set; }
        protected virtual void ConfigureRearAngleProtectorWeldAddon(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> FrontBullnoseWeldAddon { get; set; }
        protected virtual void ConfigureFrontBullnoseWeldAddon(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> RearBullnoseWeldAddon { get; set; }
        protected virtual void ConfigureRearBullnoseWeldAddon(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> HeavyHorizontalWeldAddon { get; set; }
        protected virtual void ConfigureHeavyHorizontalWeldAddon(Rule<double> rule)
        {
            rule.Define(default(double));
        }
        
        public Rule<double> SlantLegWeldAddon { get; set; }
        protected virtual void ConfigureSlantLegWeldAddon(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> SlantLegWeldSetup { get; set; }
        protected virtual void ConfigureSlantLegWeldSetup(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> BentLegWeldAddon { get; set; }
        protected virtual void ConfigureBentLegWeldAddon(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> TippmanWeldAddon { get; set; }
        protected virtual void ConfigureTippmanWeldAddon(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> TippmanWeldMultiplier { get; set; }
        protected virtual void ConfigureTippmanWeldMultiplier(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> TippmanWeldSetupMultiplier { get; set; }
        protected virtual void ConfigureTippmanWeldSetupMultiplier(Rule<double> rule)
        {
            rule.Define(default(double));
        }
        public Rule<double> HeavyHorizontalChannelandAngleWeld { get; set; }
        protected virtual void ConfigureHeavyHorizontalChannelandAngleWeld(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> HeavyHorizontalWeldPlatesWeld { get; set; }
        protected virtual void ConfigureHeavyHorizontalWeldPlatesWeld(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> BoltOnHeavyHorizontalAngleMountsWeld { get; set; }
        protected virtual void ConfigureBoltOnHeavyHorizontalAngleMountsWeld(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public double DoublerEDH(double doublerLength)
        {
            double lim1 = 108;
            double lim2 = 216;
            double lim3 = 324;

            double returnVal = doublerLength;

            switch (doublerLength)
            {
                case double n when (n > lim3):
                    returnVal += (doublerLength - lim3) + (doublerLength - lim2) + (doublerLength - lim1);
                    break;
                case double n when (n > lim2):
                    returnVal += (doublerLength - lim2) + (doublerLength - lim1);
                    break;
                case double n when (n > lim1):
                    returnVal += (doublerLength - lim1);
                    break;
            }

            return returnVal;
        }

        #endregion

        #region IRevisablePartImplementation

        public FactoryRevisionRule<string> FactoryRevision { get; set; }

        #endregion

        #region Children

        public ChildList<HorizontalBrace> HorizontalBrace { get; set; }
        protected virtual void ConfigureHorizontalBrace(ChildList<HorizontalBrace> list)
        {
            list.Qty.Define(() => PanelQty.Value + 1);
            list.ConfigureChildren((child, index) =>
            {
                child.IsActive.Define(() => ((index + 1) > HeavyHorizontalBraceQty.Value));
                child.HorizontalBraceMaster.Define(() => MasterSupplied.Value ? FrameUprightMaster.Value.HorizontalBrace[index] : default(HorizontalBrace));
                child.DisplayName.Define($"Horizontal Brace {index+1}");
                child.Id.Define(() => index);
                child.AngleTypeDefault.Define(() => GetHorizontalBraceAngleType(index));
                child.CutForFrontDoubler.Define(() => (HasFrontDoubler.Value && (child.OffsetFromBottom.Value <= FrontDoublerHeight.Value)));
                child.CutForRearDoubler.Define(() => (HasRearDoubler.Value && (child.OffsetFromBottom.Value <= RearDoublerHeight.Value)));
                child.Length.Define(() => GetHorizontalBraceLength(child.OffsetFromBottom.Value, index, false));
                child.ColumnChannelDepth.Define(() => FrontUprightColumnAssembly.UprightColumnChannel.ChannelDepth.Value);
                child.ColumnChannelThickness.Define(() => FrontUprightColumnAssembly.UprightColumnChannel.ChannelWebThickness.Value);
                child.PanelHeightDefault.Define(() => GetPanelHeight(index));
                child.RearMiterAngle.Define(() => GetHorizontalBraceRearMiterAngle(index));
                child.OffsetFromBottom.Define(() =>
                {
                    if (index == 0)
                        return child.PanelHeight.Value + FrontUprightColumnAssembly.Footplate.Thickness.Value;
                    else
                        return HorizontalBrace[index - 1].Position.Value.Y + child.PanelHeight.Value - FrontUprightColumnAssembly.Footplate.Thickness.Value;
                });
                child.Position.Define(() => GetHorizontalBraceLocation(index, child.CutLength.Value, child.AngleWidth.Value, child.PanelHeight.Value, child.OffsetFromBottom.Value));
                child.Rotation.Define(() => GetHorizontalBraceRotation(index));
            });
        }

        public ChildList<DiagonalBrace> DiagonalBrace { get; set; }
        protected virtual void ConfigureDiagonalBrace(ChildList<DiagonalBrace> list)
        {
            list.Qty.Define(PanelQty);
            list.ConfigureChildren((child, index) =>
            {
                child.DisplayName.Define($"Diagonal Brace {index + 1}");
                child.DiagonalBraceMaster.Define(() => MasterSupplied.Value ? FrameUprightMaster.Value.DiagonalBrace[index] : default(DiagonalBrace));
                child.AngleTypeDefault.Define(DiagonalType);
                child.Length.Define(() => GetDiagonalLength(index, child.AngleHeight.Value));
                child.Position.Define(() => GetDiagonalBracePosition(index));
                child.Rotation.Define(() =>
                {
                    return Quaternion.Concatenate(HorizontalBraceRotation.Value, Helper.CreateRotation(Vector3.UnitZ,
                                                  GetDiagonalRotation(index, child.Length.Value)));
                });
            });
        }

        //Made into a list for an easy way to add the brace after the Horizontal Braces in the UI.
        public ChildList<HorizontalBrace> MPanelBrace { get; set; }
        protected virtual void ConfigureMPanelBrace(ChildList<HorizontalBrace> list)
        {
            list.Qty.Define(1);
            list.ConfigureChildren((child, index) =>
            {
                child.IsActive.Define(RequiresMPanel);
                child.HorizontalBraceMaster.Define(() => MasterSupplied.Value ? FrameUprightMaster.Value.MPanelBrace[index] : default(HorizontalBrace));
                child.DisplayName.Define(() => $"Horizontal Brace {child.Id.Value + 1}");
                child.Id.Define(() => PanelQty.Value + 1);
                child.AngleTypeDefault.Define(() => GetHorizontalBraceAngleType(child.Id.Value));
                child.CutForFrontDoubler.Define(() => (HasFrontDoubler.Value && (child.OffsetFromBottom.Value <= FrontDoublerHeight.Value)));
                child.CutForRearDoubler.Define(() => (HasRearDoubler.Value && (child.OffsetFromBottom.Value <= RearDoublerHeight.Value)));
                child.Length.Define(() => GetHorizontalBraceLength(child.OffsetFromBottom.Value, child.Id.Value, false));
                child.ColumnChannelDepth.Define(() => FrontUprightColumnAssembly.UprightColumnChannel.ChannelDepth.Value);
                child.ColumnChannelThickness.Define(() => FrontUprightColumnAssembly.UprightColumnChannel.ChannelWebThickness.Value);
                child.PanelHeight.Define(() => (Height.Value - (HorizontalBrace.Last().Position.Value.Y - FrontUprightColumnAssembly.Footplate.Thickness.Value)) - 6);
                child.RearMiterAngle.Define(() => GetHorizontalBraceRearMiterAngle(child.Id.Value));
                child.OffsetFromBottom.Define(() => HorizontalBrace.Last().Position.Value.Y + child.PanelHeight.Value - FrontUprightColumnAssembly.Footplate.Thickness.Value);
                child.Position.Define(() => HorizontalBrace.Last().Position.Value + Helper.Vy(child.PanelHeight.Value));
                child.Rotation.Define(HorizontalBraceRotation);
            });
        }

        public UprightColumnAssembly FrontUprightColumnAssembly { get; set; }
        protected virtual void ConfigureFrontUprightColumnAssembly(UprightColumnAssembly child)
        {
            child.IsFrontUprightColumn.Define(true);
            child.IsColumnToSlant.Define(IsFrontFrame);
            child.UprightColumnSize.Define(UprightColumnSize);
            child.ColumnHeight.Define(FrontColumnHeight);
            child.AnchorQty.Define(FrontAnchorQty);
            child.AnchorSize.Define(FrontAnchorSize);
            child.AnchorType.Define(FrontAnchorType);
            child.BullNoseAnchorType.Define(FrontBullnoseAnchorType);
            child.HasDoubler.Define(HasFrontDoubler);
            child.DoublerHeight.Define(FrontDoublerHeight);
            child.FootPlateType.Define(FrontFootPlateType);
            child.FootplateWidth.Define(FootplateWidth);
            child.FootplateThickness.Define(FootplateThickness);
            child.PostProtectorType.Define(FrontPostProtector);
            child.PostProtectorHeight.Define(FrontPostProtectorHeight);
            child.UprightType.Define(UprightType);
            child.Location.Define(Location);
            child.SlantInset.Define(SlantInset);
            child.SlantElev.Define(SlantElev);
            child.HasHeavyBtmHorizontal.Define(() => UprightType.Value == "BH");
            child.Position.Define(() => Helper.Vx(Depth.Value / 2));
            child.Rotation.Define(() => Helper.CreateRotation(Vector3.UnitY, 180));
        }

        public UprightColumnAssembly RearUprightColumnAssembly { get; set; }
        protected virtual void ConfigureRearUprightColumnAssembly(UprightColumnAssembly child)
        {
            child.IsFrontUprightColumn.Define(false);
            child.UprightColumnSize.Define(UprightColumnSize);
            child.ColumnHeight.Define(RearColumnHeight);
            child.AnchorQty.Define(RearAnchorQty);
            child.AnchorSize.Define(RearAnchorSize);
            child.AnchorType.Define(RearAnchorType);
            child.BullNoseAnchorType.Define(RearBullnoseAnchorType);
            child.HasDoubler.Define(HasRearDoubler);
            child.DoublerHeight.Define(RearDoublerHeight);
            child.FootPlateType.Define(RearFootPlateType);
            child.FootplateWidth.Define(FootplateWidth);
            child.FootplateThickness.Define(FootplateThickness);
            child.PostProtectorType.Define(RearPostProtector);
            child.PostProtectorHeight.Define(RearPostProtectorHeight);
            child.UprightType.Define(UprightType);
            child.Location.Define(Location);
            child.HasHeavyBtmHorizontal.Define(() => UprightType.Value == "BH");
            child.Position.Define(() => Helper.Vx(-Depth.Value / 2));
        }

        public ChildList<HeavyHorizontalWeldClip> HeavyHorizontalWeldClip { get; set; }
        protected virtual void ConfigureHeavyHorizontalWeldClip(ChildList<HeavyHorizontalWeldClip> list)
        {
            list.Qty.Define(HeavyHorizontalBraceQty);
            list.ConfigureChildren((child, index) =>
            {
                child.Position.Define(() => GetHeavyHorizontalWeldClipLocation(index));
                child.Rotation.Define(() => Quaternion.CreateFromYawPitchRoll(Helper.DegToRad(90), Helper.DegToRad(-90), 0));
            });
        }

        public HeavyHorizontalWeldClip LUprightAngleWeldClip { get; set; }
        protected virtual void ConfigureLUprightAngleWeldClip(HeavyHorizontalWeldClip child)
        {
            child.IsActive.Define(() => (UprightType.Value == "L" && IsFrontFrame.Value));
            child.Position.Define(() =>
            {
                HorizontalBrace attachPart = HorizontalBrace[1];
                UprightColumnAssembly frontColumn = FrontUprightColumnAssembly;
                Vector3 horizBracePos = attachPart.Position.Value;
                float xOffset = (GetSlantHorizontalCutLengthForSlant(1) + 0.75f);
                float zOffset = (attachPart.AngleWidth.Value / 2) - 0.5f;

                return new Vector3(frontColumn.Position.Value.X - xOffset, horizBracePos.Y, horizBracePos.Z + zOffset);
            });
            child.Rotation.Define(() => Quaternion.CreateFromYawPitchRoll(Helper.DegToRad(90), Helper.DegToRad(-90), 0));
        }

        public ChildList<HeavyHorizontalWeldPlate> FrontHeavyHorizWeldPlate { get; set; }
        protected virtual void ConfigureFrontHeavyHorizWeldPlate(ChildList<HeavyHorizontalWeldPlate> list)
        {
            list.Qty.Define(HeavyHorizontalFrontWeldPlateQty);
            list.ConfigureChildren((child, index) =>
            {
                child.ChannelWidth.Define(() => HeavyHorizontalBrace.First().ChannelWidth.Value);
                child.Position.Define(() => GetHeavyHorizontalWeldPlateLocation(index, true));
            });
        }

        public ChildList<HeavyHorizontalWeldPlate> RearHeavyHorizWeldPlate { get; set; }
        protected virtual void ConfigureRearHeavyHorizWeldPlate(ChildList<HeavyHorizontalWeldPlate> list)
        {
            list.Qty.Define(HeavyHorizontalRearWeldPlateQty);
            list.ConfigureChildren((child, index) =>
            {
                child.ChannelWidth.Define(() => HeavyHorizontalBrace.First().ChannelWidth.Value);
                child.Position.Define(() => GetHeavyHorizontalWeldPlateLocation(index, false));
                child.Rotation.Define(() => Helper.CreateRotation(Vector3.UnitY, 180));
            });
        }

        public ChildList<HeavyHorizontalBrace> HeavyHorizontalBrace { get; set; }
        protected virtual void ConfigureHeavyHorizontalBrace(ChildList<HeavyHorizontalBrace> list)
        {
            list.Qty.Define(HeavyHorizontalBraceQty);
            list.ConfigureChildren((child, index) =>
            {
                child.HeavyHorizontalBraceMaster.Define(() => MasterSupplied.Value ? FrameUprightMaster.Value.HeavyHorizontalBrace[index] : default(HeavyHorizontalBrace));
                child.DisplayName.Define($"Hvy Horizontal Brace {index+1}");
                child.Id.Define(() => index);
                child.ChannelSize.Define(UprightColumnSize);
                child.UprightType.Define(UprightType);
                child.PanelHeightDefault.Define(() => GetPanelHeight(index));
                child.HasFrontHole.Define(() =>
                {
                    if (FrontUprightColumnAssembly.FootPlateType.Value.Contains("FP3") || FrontUprightColumnAssembly.FootPlateType.Value.Contains("FP4"))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                });
                child.HasRearHole.Define(() =>
                {
                    if (RearUprightColumnAssembly.FootPlateType.Value.Contains("FP3") || RearUprightColumnAssembly.FootPlateType.Value.Contains("FP4"))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                });
                child.OffsetFromBottom.Define(() =>
                {
                    if (index == 0)
                        return child.PanelHeight.Value + FrontUprightColumnAssembly.Footplate.Thickness.Value;
                    else
                        return HorizontalBrace[index - 1].Position.Value.Y + child.PanelHeight.Value;
                });
                child.CutForFrontDoubler.Define(() => (HasFrontDoubler.Value && (child.OffsetFromBottom.Value <= FrontDoublerHeight.Value)));
                child.CutForRearDoubler.Define(() => (HasRearDoubler.Value && (child.OffsetFromBottom.Value <= RearDoublerHeight.Value)));
                child.Length.Define(() => GetHorizontalBraceLength(child.OffsetFromBottom.Value, index, true));
                child.RearMiterAngle.Define(HeavyHorizontalRearMiterAngle);
                child.Position.Define(() => GetHeavyHorizontalBraceLocation(index, child.Length.Value, child.PanelHeight.Value, child.OffsetFromBottom.Value));
                child.Rotation.Define(() => Quaternion.CreateFromYawPitchRoll(Helper.DegToRad(-90), 0, Helper.DegToRad(270)));
            });
        }

        public HeavyHorizontalBrace HeavyHorizontalBraceDoubler { get; set; }
        protected virtual void ConfigureHeavyHorizontalBraceDoubler(HeavyHorizontalBrace child)
        {
            child.IsActive.Define(() => UprightType.Value == "D");
            child.HeavyHorizontalBraceMaster.Define(() => MasterSupplied.Value ? FrameUprightMaster.Value.HeavyHorizontalBraceDoubler : default(HeavyHorizontalBrace));
            child.DisplayName.Define($"Hvy Horizontal Brace Dblr");
            child.ChannelSize.Define(UprightColumnSize);
            child.Length.Define(() => HeavyHorizontalBrace.FirstOrDefault().Length.Value);
            child.HasFrontHole.Define(() => HeavyHorizontalBrace.FirstOrDefault().HasFrontHole.Value);
            child.HasRearHole.Define(() => HeavyHorizontalBrace.FirstOrDefault().HasRearHole.Value);
            child.CutForFrontDoubler.Define(() => HeavyHorizontalBrace.FirstOrDefault().CutForFrontDoubler.Value);
            child.CutForRearDoubler.Define(() => HeavyHorizontalBrace.FirstOrDefault().CutForRearDoubler.Value);
            child.UprightType.Define(UprightType);

            child.Position.Define(() => HeavyHorizontalBrace.FirstOrDefault().Position.Value + Helper.Vy(-child.ChannelDepth.Value * 2));
            child.Rotation.Define(() => Quaternion.CreateFromYawPitchRoll(Helper.DegToRad(-90), 0, Helper.DegToRad(90)));
        }

        public TippmannHorizontal TopTippmannHorizontal { get; set; }
        protected virtual void ConfigureTopTippmannHorizontal(TippmannHorizontal child)
        {
            child.IsActive.Define(() => UprightType.Value == "T");
            //child.DisplayName.Define($"Tippmann Horizontal Top Brace 0");
            child.Id.Define(0);
            child.ChannelSize.Define(UprightColumnSize);
            child.Length.Define(TippmannHorizontalLength);
            child.IsTopChannel.Define(true);
            child.Position.Define(() =>
            {
                float xOffset = ((Depth.Value / 2) - (RearUprightColumnAssembly.UprightColumnChannel.ChannelDepth.Value * 2));
                return Helper.Vxy(xOffset, child.ChannelDepth.Value* 2);
            });
            child.Rotation.Define(() => Quaternion.CreateFromYawPitchRoll(Helper.DegToRad(-90), 0, Helper.DegToRad(270)));
        }

        public TippmannHorizontal BtmTippmannHorizontal { get; set; }
        protected virtual void ConfigureBtmTippmannHorizontal(TippmannHorizontal child)
        {
            child.IsActive.Define(() => UprightType.Value == "T");
            //child.DisplayName.Define($"Tippmann Horizontal Btm Brace 0");
            child.Id.Define(0);
            child.ChannelSize.Define(UprightColumnSize);
            child.Length.Define(TippmannHorizontalLength);
            child.IsTopChannel.Define(false);
            child.Position.Define(() =>
            {
                float xOffset = ((Depth.Value / 2) - (RearUprightColumnAssembly.UprightColumnChannel.ChannelDepth.Value * 2));
                return Helper.Vxy(xOffset, 0);
            });
            child.Rotation.Define(() => Quaternion.CreateFromYawPitchRoll(Helper.DegToRad(-90), 0, Helper.DegToRad(90)));
        }

        public HardwareGroup Hardware { get; set; }
        protected virtual void ConfigureHardware(HardwareGroup child)
        {
            child.IsActive.Define(() => UprightType.Value == "BH");
            child.BoltLength.Define(1);
            child.BoltSize.Define(0.5f);
            child.HasWashers.Define(true);
            child.WasherType.Define(WasherTypes.Flat);
            child.WasherMaterial.Define(HardwareMaterial.Grade2);
            child.Qty.Define(2);
        }


        #endregion

        #region Public Methods

        public Vector3 GetDiagonalBracePosition(int index)
        {
            float zOffset = 0;
            float yOffset = 0;
            HorizontalBrace btmBrace = HorizontalBrace[index];

            if (IsFrontFrame.Value && HorizontalBraceIsFlipped(index))
            {
                zOffset = DiagonalBrace[index].AngleWidth.Value;
                yOffset = (btmBrace.AngleHeight.Value - 0.125f);
            }

            if (!btmBrace.IsActive.Value)
            {
                HeavyHorizontalWeldClip weldClip = HeavyHorizontalWeldClip[index];
                return weldClip.Position.Value + new Vector3(-HeavyHorizontalClipDiagonalHorizOffset.Value, 
                                                            GetHeavyHorizontalClipDiagonalVerticalOffset(index, btmBrace: btmBrace), 
                                                            0);
            }
            else if (index == 1 && LUprightAngleWeldClip.IsActive.Value)
            {
                //Special Case code only for Slant Leg 12x56
                return LUprightAngleWeldClip.Position.Value + new Vector3(-HeavyHorizontalClipDiagonalHorizOffset.Value,
                                                            GetHeavyHorizontalClipDiagonalVerticalOffset(index, btmBrace: btmBrace),
                                                            0);
            }
            else
                return btmBrace.Position.Value + new Vector3(-btmBrace.FrontDiagZOffset.Value, yOffset, zOffset);
        }

        public float GetDiagonalVerticalSpace(int diagIndex, HorizontalBrace btmBrace = null, HorizontalBrace topBrace = null)
        {
            if (btmBrace == null)
                btmBrace = HorizontalBrace[diagIndex];

            if (topBrace == null)
                topBrace = HorizontalBrace[diagIndex + 1];
            bool btmBraceIsFlipped = HorizontalBraceIsFlipped(diagIndex);
            bool topBraceIsFlipped = HorizontalBraceIsFlipped(diagIndex + 1);
            float verticalDist = (topBrace.PanelHeight.Value - topBrace.AngleThickness.Value);

            //if horizontal brace is not active, there is a heavy horizontal in its place
            if (!btmBrace.IsActive.Value)
            {
                HeavyHorizontalWeldClip weldClip = HeavyHorizontalWeldClip[diagIndex];
                verticalDist = verticalDist - GetHeavyHorizontalClipDiagonalVerticalOffset(diagIndex, topBrace: topBrace, btmBrace: btmBrace);
            }
            else if (diagIndex == 1 && LUprightAngleWeldClip.IsActive.Value)
            {
                //Special Case code only for Slant Leg 12x56
                verticalDist = verticalDist - GetHeavyHorizontalClipDiagonalVerticalOffset(diagIndex, topBrace: topBrace, btmBrace: btmBrace);
            }

            if (btmBraceIsFlipped)
                verticalDist = verticalDist - btmBrace.AngleHeight.Value;

            if (topBraceIsFlipped)
                verticalDist = verticalDist + topBrace.AngleHeight.Value + topBrace.AngleThickness.Value;

            return verticalDist;
        }

        public float GetDiagonalRotation(int index, float angleLength)
        {
            float verticalDist = GetDiagonalVerticalSpace(index);

            return GetDiagonalRotation(verticalDist, angleLength);
        }

        public float GetDiagonalRotation(float verticalDistance, float angleLength)
        {
            var angle = -Helper.RadToDeg((float)Math.Asin(verticalDistance / angleLength));
            return Convert.ToSingle(Math.Round(angle, 5));
        }

        public float GetDiagonalHorizontalDistance(int diagIndex, HorizontalBrace btmBrace = null, HorizontalBrace topBrace = null)
        {
            if (btmBrace == null)
            {
                btmBrace = HorizontalBrace[diagIndex];
            }

            if (topBrace == null)
            {
                topBrace = HorizontalBrace[diagIndex + 1];
            }
            var horizontalDist = Math.Abs(btmBrace.FrontDiagOffsetConnectionPoint.WorldPosition.Value.X - topBrace.RearDiagOffsetConnectionPoint.WorldPosition.Value.X);

            //if horizontal brace is not active, there is a heavy horizontal in its place
            if (!btmBrace.IsActive.Value)
            {
                HeavyHorizontalWeldClip weldClip = HeavyHorizontalWeldClip[diagIndex];
                horizontalDist = Math.Abs((weldClip.WorldPosition.Value.X - HeavyHorizontalClipDiagonalHorizOffset.Value) - topBrace.RearDiagOffsetConnectionPoint.WorldPosition.Value.X);
            }
            else if (diagIndex == 1 && LUprightAngleWeldClip.IsActive.Value)
            {
                //Special Case code only for Slant Leg 12x56
                horizontalDist = Math.Abs((LUprightAngleWeldClip.WorldPosition.Value.X - HeavyHorizontalClipDiagonalHorizOffset.Value) - topBrace.RearDiagOffsetConnectionPoint.WorldPosition.Value.X);
            }

            return horizontalDist;
        }

        public float GetHeavyHorizontalClipDiagonalVerticalOffset(int diagIndex, HorizontalBrace topBrace = null, HorizontalBrace btmBrace = null)
        {
            if (btmBrace == null)
            {
                btmBrace = HorizontalBrace[diagIndex];
            }
            if (topBrace == null)
            {
                topBrace = HorizontalBrace[diagIndex + 1];
            }
            var horizDist = GetDiagonalHorizontalDistance(diagIndex, btmBrace: btmBrace, topBrace: topBrace);
            var verticalDist = (topBrace.PanelHeight.Value - topBrace.AngleThickness.Value - HvyHorizontalClipDiagonalBtmVertOffset.Value);
            var angleOppositeHeight = GetHeavyHorizontalClipDiagonalVerticalOffset(verticalDist, horizDist, DiagonalBrace[diagIndex].AngleHeight.Value);

            return angleOppositeHeight + HvyHorizontalClipDiagonalBtmVertOffset.Value;
        }

        public float GetHeavyHorizontalClipDiagonalVerticalOffset(float overallHeight, float overallWidth, float angleHeight)
        {
            //Get vertical offset where the bottom of the diagonal is HvyHorizontalClipDiagonalBtmVertOffset from the top of the horizontal
            var step = 0.0625f;
            float angleOppositeHeight = 0;
            float b = overallHeight;

            //Start with top of diagonal angle at the offset location and iterate up until the bottom point is at the offset
            for (var i = 1; i < 32; i++)
            {
                b = b - step;
                var hypLength = (float)Math.Sqrt((Math.Pow(b, 2) + Math.Pow(overallWidth, 2)));
                var angleRotationInDeg = Helper.RadToDeg((float)Math.Asin(b / hypLength));

                var endOfDiagonalAngle = 90 - angleRotationInDeg;
                angleOppositeHeight = GetAngleOppositeLength(endOfDiagonalAngle, angleHeight);
                var vertDistFromTopOfAngleToBtmAngle = (b - angleOppositeHeight);
                var bottomYOffsetFromGoalHeight = overallHeight - angleOppositeHeight;

                if (bottomYOffsetFromGoalHeight >= 0)
                {
                    return angleOppositeHeight;
                }
            }

            return angleOppositeHeight;
        }

        public float GetDiagonalLength(int index, float angleHeight)
        {
            HorizontalBrace btmBrace = HorizontalBrace[index];
            HorizontalBrace topBrace = HorizontalBrace[index + 1];
            float verticalDist = GetDiagonalVerticalSpace(index, btmBrace: btmBrace, topBrace: topBrace);
            float horizontalDist = GetDiagonalHorizontalDistance(index, btmBrace: btmBrace, topBrace: topBrace);

            return GetDiagonalLength(verticalDist, horizontalDist, angleHeight);
        }

        public float GetDiagonalLength(float overallHeight, float overallWidth, float angleHeight)
        {
            float step = 0.0625f;
            float hypLength = 0;
            float angleRotationInDegrees = 0;
            float b = overallWidth;

            for (int i = 1; i < 32; i++)
            {
                b = b - step;
                hypLength = (float)Math.Sqrt((Math.Pow(overallHeight, 2) + Math.Pow(b, 2)));
                angleRotationInDegrees = Helper.RadToDeg((float)Math.Asin(overallHeight / hypLength));

                float widthFromBottomAngleToFrontHalfIn = (GetDiagonalBottomAngleOffset(angleRotationInDegrees, angleHeight) + b);
                float bottomXOffsetFromFrontSide = overallWidth - widthFromBottomAngleToFrontHalfIn;

                if (bottomXOffsetFromFrontSide >= 0)
                    return hypLength;
            }

            return hypLength;
        }

        public float GetDiagonalBottomAngleOffset(float angleRotation, float angleHeight)
        {
            float endOfDiagonalAngle = 90 - angleRotation;

            //sin Angle = Opposite/Hyp
            float hyp = angleHeight;
            float opposite = GetAngleOppositeLength(endOfDiagonalAngle, hyp);
            float adjacent = (float)Math.Sqrt(Math.Pow(hyp, 2) - Math.Pow(opposite, 2));

            return adjacent;
        }

        public float GetAngleOppositeLength(float angleRotation, float hypotenuse)
        {
            return Helper.Sin(angleRotation) * hypotenuse;
        }

        public float GetHorizontalBraceRearMiterAngle(int index)
        {
            float retVal = 0;

            if (IsFrontFrame.Value)
            {
                if (UprightType.Value == "L" && index == 1)
                {
                    if (HorizontalBrace[index].AngleThickness.Value == 0.1875f)
                        retVal = 11;
                    else
                        retVal = 12;
                }
                else if (IsBentType.Value && index <= (BentTypeSpecialPanelHeights.Value.Count() - 1))
                    retVal = SlantAngle.Value;
            }

            return retVal;
        }

        public float GetHorizontalBraceLength(float offsetFromBottom, int index, bool heavyBrace)
        {
            if (IsFrontFrame.Value && UprightType.Value == "LS" && index == 0)
                return GetSlantHorizontalBraceLength(index, heavyBrace);
            else if (IsFrontFrame.Value && UprightType.Value == "L" && index <= 1)
                return GetSlantHorizontalBraceLength(index, heavyBrace);
            else if (IsFrontFrame.Value && IsBentType.Value && index <= (BentTypeSpecialPanelHeights.Value.Count()-1))
                return GetBentHorizontalBraceLength(offsetFromBottom, index, heavyBrace);
            else
                return GetHorizontalBraceLength(offsetFromBottom, heavyBrace);
        }

        public float GetHorizontalBraceLength(float offsetFromBottom, bool heavyBrace)
        {
            float retLength = Depth.Value;

            if (heavyBrace)
                retLength = retLength - (FrontUprightColumnAssembly.UprightColumnChannel.ChannelDepth.Value * 2);

            if (HasRearDoubler.Value && (offsetFromBottom <= RearDoublerHeight.Value))
            {
                if (heavyBrace)
                    retLength = (retLength - RearUprightColumnAssembly.Doubler.ChannelDepth.Value);
                else
                    retLength = (retLength - (RearUprightColumnAssembly.Doubler.ChannelDepth.Value * 2));
            }
            else if (!heavyBrace)
                retLength = retLength - RearUprightColumnAssembly.UprightColumnChannel.ChannelWebThickness.Value;

            if (HasFrontDoubler.Value && (offsetFromBottom <= FrontDoublerHeight.Value))
            {
                if (heavyBrace)
                    retLength = retLength - FrontUprightColumnAssembly.Doubler.ChannelDepth.Value;
                else
                    retLength = retLength - (FrontUprightColumnAssembly.Doubler.ChannelDepth.Value * 2);
            }
            else if (!heavyBrace)
                retLength = retLength - FrontUprightColumnAssembly.UprightColumnChannel.ChannelWebThickness.Value;

            return retLength;
        }

        public float GetBentHorizontalOffset(float offsetFromBottom)
        {
            float oppFromIntersection = (offsetFromBottom * Helper.Tan(SlantAngle.Value));

            return ((SlantInset.Value - oppFromIntersection) +
                    (FrontUprightColumnAssembly.UprightColumnChannel.ChannelDepth.Value * 2));
        }

        public float GetBentHorizontalBraceLength(float offsetFromBottom, int index, bool heavyBrace)
        {
            float retLength = Depth.Value;
            float horizOffsetOnBend = GetBentHorizontalOffset(offsetFromBottom);

            if (HasRearDoubler.Value)
                retLength = retLength - (RearUprightColumnAssembly.Doubler.ChannelDepth.Value * 2);
            else
            {
                if (heavyBrace)
                    retLength = retLength - RearUprightColumnAssembly.UprightColumnChannel.ChannelDepth.Value;
                else
                    retLength = retLength - RearUprightColumnAssembly.UprightColumnChannel.ChannelWebThickness.Value;
            }

            return (retLength - horizOffsetOnBend);
        }

        public float GetSlantHorizontalBraceLength(int index, bool heavyBrace)
        {
            float retLength = Depth.Value;
            float cutLengthForSlant = GetSlantHorizontalCutLengthForSlant(index);

            if (HasRearDoubler.Value)
                retLength = retLength - (RearUprightColumnAssembly.Doubler.ChannelDepth.Value*2);
            else
            {
                if (heavyBrace)
                    retLength = retLength - RearUprightColumnAssembly.UprightColumnChannel.ChannelDepth.Value;
                else
                    retLength = retLength - RearUprightColumnAssembly.UprightColumnChannel.ChannelWebThickness.Value;
            }
            
            return (retLength - cutLengthForSlant);
        }

        //Offset from outside of front face to where the horizontal starts along the slant
        public float GetSlantHorizontalCutLengthForSlant(int index)
        {
            if (UprightType.Value == "LS")
                return 13.0625f;
            else
            {
                if (index == 0)
                    return 13.8125f;
                else
                    return 8.625f;
            }
        }

        public float GetPanelHeight(int index)
        {
            if ((index + 1) > DefaultPanelHeights.Value.Count)
                return 48;
            else
                return DefaultPanelHeights.Value[index];
        }

        public string GetHorizontalBraceAngleType(int childIndex)
        {
            string retVal = DefaultHorizontalType.Value;

            if (DefaultHorizontalType.Value == @"1-1/2 x 1-1/2 x 1/8" && 
                (DiagonalType.Value == @"2 x 2 x 1/8" || DiagonalType.Value == @"2 x 2 x 3/16") &&
                childIndex < 2)
            {
                retVal = @"2 x 2 x 1/8";
            }

            return retVal;
        }

        public Vector3 GetHorizontalBraceLocation(int index, float length, float width, float panelHeight, float offsetFromBottom)
        {
            float yOffset = FrontUprightColumnAssembly.Footplate.Thickness.Value;
            float webThickness = RearUprightColumnAssembly.UprightColumnChannel.ChannelWebThickness.Value;

            if (index > 0)
                yOffset = HorizontalBrace[index - 1].Position.Value.Y + HorizontalBrace[index - 1].AngleThickness.Value - HorizontalBrace[index].AngleThickness.Value;

            Vector3 retVal = Helper.Vxyz((length / 2), (yOffset + panelHeight), width / 2);

            bool cutForRearDoubler = (HasRearDoubler.Value && (offsetFromBottom <= RearDoublerHeight.Value));
            bool cutForFrontDoubler = (HasFrontDoubler.Value && (offsetFromBottom <= FrontDoublerHeight.Value));

            if (cutForRearDoubler || cutForFrontDoubler)
            {
                if (cutForFrontDoubler)
                    retVal.X = ((Depth.Value / 2) - (RearUprightColumnAssembly.UprightColumnChannel.ChannelDepth.Value * 2));
                else
                    retVal.X = ((Depth.Value / 2) - webThickness);
            }

            if ((IsFrontFrame.Value && UprightType.Value == "LS" && index == 0) ||
                (IsFrontFrame.Value && UprightType.Value == "L" && index <= 1))
                retVal.X = ((Depth.Value / 2) - GetSlantHorizontalCutLengthForSlant(index));
            else if (IsFrontFrame.Value && IsBentType.Value && index <= (BentTypeSpecialPanelHeights.Value.Count() - 1))
                retVal.X = ((Depth.Value / 2) - GetBentHorizontalOffset(offsetFromBottom));

            //Flipped angle braces
            if (IsFrontFrame.Value && HorizontalBraceIsFlipped(index))
                retVal.Z = -retVal.Z;

            return retVal;
        }

        public Quaternion GetHorizontalBraceRotation(int index)
        {
            if (IsFrontFrame.Value && HorizontalBraceIsFlipped(index))
                return FlippedHorizontalBraceRotation.Value;
            else
                return HorizontalBraceRotation.Value;
        }

        public bool HorizontalBraceIsFlipped(int index)
        {
            return ((UprightType.Value == "LS" && index == 1) || (UprightType.Value == "L" && index == 2));
        }

        public Vector3 GetHeavyHorizontalWeldClipLocation(int index)
        {
            HeavyHorizontalBrace attachPart = HeavyHorizontalBrace[index];
            UprightColumnAssembly frontColumn = FrontUprightColumnAssembly;
            Vector3 horizBracePos = attachPart.Position.Value;
            float xOffset = HasFrontDoubler.Value ? 4.25f : 3.25f;
            float zOffset = (attachPart.ChannelWidth.Value / 2) - 0.5f;

            if (IsSlantType.Value && IsFrontFrame.Value)
                xOffset = (GetSlantHorizontalCutLengthForSlant(index) + 0.75f);
            else if (IsBentType.Value && IsFrontFrame.Value)
                xOffset = (GetBentHorizontalOffset(attachPart.OffsetFromBottom.Value) + 0.75f);
            else
                xOffset = xOffset - frontColumn.UprightColumnChannel.ChannelWebThickness.Value;

            return new Vector3(frontColumn.Position.Value.X - xOffset, horizBracePos.Y, horizBracePos.Z + zOffset);
        }

        public Vector3 GetHeavyHorizontalWeldPlateLocation(int index, bool frontLoc)
        {
            HeavyHorizontalBrace attachPart = HeavyHorizontalBrace[index];
            Vector3 horizBracePos = attachPart.Position.Value;
            float xOffset = 0;

            if (frontLoc)
            {
                xOffset = FrontUprightColumnAssembly.UprightColumnChannel.ChannelDepth.Value - FrontUprightColumnAssembly.UprightColumnChannel.ChannelWebThickness.Value;
            }
            else
            {
                xOffset = -attachPart.Length.Value;
                xOffset = xOffset - (FrontUprightColumnAssembly.UprightColumnChannel.ChannelDepth.Value - FrontUprightColumnAssembly.UprightColumnChannel.ChannelWebThickness.Value);
            }

            return new Vector3(horizBracePos.X + xOffset, horizBracePos.Y, horizBracePos.Z);
        }

        public Vector3 GetHeavyHorizontalBraceLocation(int index, float length, float panelHeight, float offsetFromBottom)
        {
            float yOffset = FrontUprightColumnAssembly.Footplate.Thickness.Value;

            if (index > 0)
                yOffset = HeavyHorizontalBrace[index - 1].Position.Value.Y;
            
            Vector3 retVal = Helper.Vxy((length / 2), (yOffset + panelHeight));

            bool cutForRearDoubler = (HasRearDoubler.Value && (offsetFromBottom <= RearDoublerHeight.Value));
            bool cutForFrontDoubler = (HasFrontDoubler.Value && (offsetFromBottom <= FrontDoublerHeight.Value));

            if (cutForRearDoubler || cutForFrontDoubler)
            {
                if (cutForFrontDoubler)
                    retVal.X = ((Depth.Value / 2) - (RearUprightColumnAssembly.UprightColumnChannel.ChannelDepth.Value * 2));
                else
                    retVal.X = ((Depth.Value / 2) - RearUprightColumnAssembly.UprightColumnChannel.ChannelDepth.Value);
            }

            if ((IsFrontFrame.Value && UprightType.Value == "LS" && index == 0) || 
                (IsFrontFrame.Value && UprightType.Value == "L" && index <= 1))
                retVal.X = ((Depth.Value /2) - GetSlantHorizontalCutLengthForSlant(index));
            else if (IsFrontFrame.Value && IsBentType.Value && index <= (BentTypeSpecialPanelHeights.Value.Count() - 1))
                retVal.X = ((Depth.Value / 2) - GetBentHorizontalOffset(offsetFromBottom));

            return retVal;
        }

        #endregion

        #region Helper Methods

        private string GetUprightTypeDescriptionAbbrev()
        {
            switch(UprightType.Value)
            {
                case "S":
                case "H":
                case "BH":
                    return "S";
                case "BTH":
                    return "H";
                case "B":
                    return "BL";
                default:
                    return UprightType.Value;
            }
        }

        private string GetColumnSizeAbbrev(string columnSize)
        {
            switch(columnSize)
            {
                case "C3x3.5":
                    return "A";
                case "C3x4.1":
                    return "B";
                case "C4x4.5":
                    return "C";
                case "C4x5.4":
                    return "D";
                case "C5x6.1":
                    return "EL";
                case "C5x6.7":
                    return "E";
            }
            return "";
        }

        #endregion

        #region Protected Methods

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureBasePlateDataRules();
            this.ConfigureAngleDataRules();
            this.ConfigureFrameUprightDataRules();
            this.ConfigureBomPartRules();
            this.ConfigureRevisablePart();
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(Colors.Gray);
        }

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define("FrameUpright");
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(false);
        }

        protected override void ConfigureAllowedChildTypes(ReadOnlyRule<IEnumerable<Type>> rule)
        {
            rule.Define(() => new List<Type>
            {
                typeof(CustomPart)
            });
        }

        #endregion

        #region Graphics

        //public Block FrameUprightBlock { get; set; }
        //protected virtual void ConfigureFrameUprightBlock(Block geo)
        //{
        //    geo.Width.Define(Depth);
        //    geo.Height.Define(Height);
        //    geo.Depth.Define(() => RearUprightColumn.ChannelWidth.Value);
        //    geo.Position.Define(() => Helper.Vy(Height.Value / 2));
        //}

        //public UCS UCS { get; set; }
        //protected virtual void ConfigureUCS(UCS child)
        //{
        //    child.Position.Define(() => (Helper.Vy(0)));
        //}

        #endregion
    }
}
