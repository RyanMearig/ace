﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;
using Ruler.Rules;
using Ruler.Cpq;
using Ruler.Rules.Graphics;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class DoubledRubRail : FrameTieComponent, IChannel, IRevisablePart, Ruler.Cpq.IBomPart
    {
        #region Constructors

        public DoubledRubRail(ModelContext modelContext, StorageConfiguratorRepository repo) : base(modelContext, repo) { }

        public DoubledRubRail(string name, Part parent, StorageConfiguratorRepository repo) : base(name, parent, repo) { }

        #endregion

        #region Rules

        public Rule<double> Qty { get; set; }
        protected virtual void ConfigureQty(Rule<double> rule)
        {
            rule.Define(1);
            rule.AddToUI(ui =>
            {
                ui.Label.Define("Quantity");
                ui.ReadOnly.Define(true);
            });
        }

        public Rule<float> EndPlateVerticalOffset { get; set; }
        protected virtual void ConfigureEndPlateVerticalOffset(Rule<float> rule)
        {
            rule.Define(2.25f);
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<float> BeamCutLength { get; set; }
        protected virtual void ConfigureBeamCutLength(Rule<float> rule)
        {
            rule.Define(() => TopChannel.Length.Value);
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<float> EndPlateWidth { get; set; }
        protected virtual void ConfigureEndPlateWidth(Rule<float> rule)
        {
            rule.Define(() => FrontEndPlate.Width.Value);
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> ChannelPartNumber { get; set; }
        protected virtual void ConfigureChannelPartNumber(Rule<string> rule)
        {
            rule.Define(() =>
            {
                switch (ChannelSize.Value)
                {
                    case @"C3x3.5":
                        return "PC31100";
                    case @"C3x4.1":
                        return "PC31101";
                    case @"C4x4.5":
                        return "PC31102";
                    case @"C4x5.4":
                        return "PC31103";
                    case @"C5x6.1":
                        return "PC31104L";
                    case @"C5x6.7":
                        return "PC31104";
                    default:
                        return "GET INFO";
                }
            });
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> RubRailChannelDescription { get; set; }
        protected virtual void ConfigureRubRailChannelDescription(Rule<string> rule)
        {
            rule.Define(() => TopChannel.Description.Value);
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<float> ChannelKSI { get; set; }
        protected virtual void ConfigureChannelKSI(Rule<float> rule)
        {
            rule.Define(() => TopChannel.KSI.Value);
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> WeldLength { get; set; }
        protected virtual void ConfigureWeldLength(Rule<string> rule)
        {
            rule.Define(() => Helper.fractionString(TopChannel.ChannelWidth.Value));
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> EndPlatePartNumber { get; set; }
        protected virtual void ConfigureEndPlatePartNumber(Rule<string> rule)
        {
            rule.Define(() =>
            {
                string basePartNumber = "P40834";

                if (FrontEndPlate.Width.Value == 3)
                {
                    return basePartNumber;
                }
                else
                {
                    return $"{basePartNumber}-{FrontEndPlate.Width.Value}";
                }
            });
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> EndPlateDescription { get; set; }
        protected virtual void ConfigureEndPlateDescription(Rule<string> rule)
        {
            rule.Define(() => FrontEndPlate.Description.Value);
        }

        #endregion

        #region FrameTieComponent Overrides

        protected override void ConfigureFrameTieDescription1(ReadOnlyRule<string> rule)
        {
            rule.Define("DOUBLED");
        }

        protected override void ConfigureFrameTieDescription2(ReadOnlyRule<string> rule)
        {
            rule.Define("RUB RAIL");
        }

        protected virtual void ConfigureBOMDescription(Rule<string> rule)
        {
            rule.Define(() =>
            {
                var desc = $"Doubled Rub Rail, {OverallLength.Value:#.##}\" Long";
                desc += $"|{ChannelSize.Value}# Material with 3/16\" x {ChannelWidth.Value:#.##}\" x 6\" End Plates";
                desc += $"|{ColorBOMName.Value}";
                return desc;
            });
        }

        protected override void ConfigurePartNumberPrefix(Rule<string> rule)
        {
            rule.Define(() => $"{Type.Value.ToUpper().Replace("X", TopChannel.ChannelId.Value.ToUpper())}-");
        }

        #endregion

        #region IChannel Implementation

        public Rule<List<ChannelData>> AllChannelData { get; set; }
        public Rule<List<ChoiceListItem>> AllChannelSizesChoiceList { get; set; }
        public Rule<ChannelData> ActiveChannelData { get; set; }
        public Rule<float> ChannelWidth { get; set; }
        public Rule<float> ChannelDepth { get; set; }
        public Rule<float> ChannelWebThickness { get; set; }
        public Rule<float> ChannelWeightPerFt { get; set; }
        public Rule<List<string>> ChannelSizesAvailable { get; set; }
        public Rule<string> ChannelSize { get; set; }
        public Rule<string> MaterialPartNumber { get; set; }

        #endregion

        #region IRevisablePartImplementation

        public FactoryRevisionRule<string> FactoryRevision { get; set; }

        #endregion

        #region CPQ.IBomPart Implementation

        //This is the description used only by the BOM Summary.
        public Rule<string> BOMUIDescription { get; set; }
        protected virtual void ConfigureBOMUIDescription(Rule<string> rule)
        {
            rule.Define(() => $"RR({Helper.fractionString(OverallLength.Value, feetToo: false)}, {TopChannel.ChannelId.Value})");
        }

        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {
            child.PartNumber.Define(PartNumber);
            child.Description.Define(BOMUIDescription);
            child.ItemQty.Define(Qty);
        }

        #endregion

        #region Labor Rules

        public Rule<double> WeldPartsPerHour { get; set; }
        protected virtual void ConfigureWeldPartsPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> PaintCarriersPerHour { get; set; }
        protected virtual void ConfigurePaintCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> UnloadCarriersPerHour { get; set; }
        protected virtual void ConfigureUnloadCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> PartCarriersPerHour { get; set; }
        protected virtual void ConfigurePartCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        #endregion

        #region Children

        public FrameTieChannel TopChannel { get; set; }
        protected virtual void ConfigureTopChannel(FrameTieChannel child)
        {
            child.FrameTieType.Define(Type);
            child.ChannelSize.Define(ChannelSize);
            child.Length.Define(() => OverallLength.Value - (FrontEndPlate.Thickness.Value * 2));
            child.Position.Define(() => Helper.Vx(-FrontEndPlate.Thickness.Value));
            child.Rotation.Define(() => Quaternion.CreateFromYawPitchRoll(Helper.DegToRad(-90), 0, Helper.DegToRad(270)));
        }

        public FrameTieChannel BottomChannel { get; set; }
        protected virtual void ConfigureBottomChannel(FrameTieChannel child)
        {
            child.FrameTieType.Define(Type);
            child.ChannelSize.Define(ChannelSize);
            child.Length.Define(() => OverallLength.Value - (FrontEndPlate.Thickness.Value * 2));
            child.Position.Define(() => Helper.Vxy(-FrontEndPlate.Thickness.Value, -(child.ChannelDepth.Value * 2)));
            child.Rotation.Define(() => Quaternion.CreateFromYawPitchRoll(Helper.DegToRad(-90), 0, Helper.DegToRad(90)));
        }

        public DoubledRubRailPlate FrontEndPlate { get; set; }
        protected virtual void ConfigureFrontEndPlate(DoubledRubRailPlate child)
        {            
            child.Width.Define(() => TopChannel.ChannelWidth.Value);
            child.Position.Define(() => Helper.Vy(EndPlateVerticalOffset.Value));
        }

        public DoubledRubRailPlate RearEndPlate { get; set; }
        protected virtual void ConfigureRearEndPlate(DoubledRubRailPlate child)
        {
            child.Width.Define(() => TopChannel.ChannelWidth.Value);
            child.Position.Define(() => Helper.Vxy(-OverallLength.Value, EndPlateVerticalOffset.Value));
            child.Rotation.Define(() => Helper.CreateRotation(Vector3.UnitY, 180));
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(Colors.Blue);
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(false);
        }

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define("DoubledRubRail");
        }

        #endregion

        #region IBomPart Overrides

        protected virtual void ConfigurePartWeight(Rule<double> rule)
        {
            rule.Define(() => this.GetPartWeightWithoutPaint());
        }

        #endregion

        #region Graphics

        //public UCS UCS { get; set; }
        //protected virtual void ConfigureUCS(UCS child)
        //{
        //    child.Diameter.Define(0.125f);
        //    child.Position.Define(() => (Helper.Vy(0)));
        //}

        #endregion

        #region Protected Methods

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureChannelRules();
            this.ConfigureRevisablePart();
        }

        #endregion

    }
}
