﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;
using Ruler.Cpq;
using Ruler.Rules.Graphics;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class RubRailAngle : AdvancePart, IRevisablePart, IBomPart, IAngle, Ruler.Cpq.IBomPart
    {
        #region Constructors

        public RubRailAngle(ModelContext modelContext) : base(modelContext) { }

        public RubRailAngle(string name, Part parent) : base(name, parent) { }

        #endregion

        #region Rules

        [CadRule(CadRuleUsageType.Parameter)]
        public Rule<float> Length { get; set; }
        protected virtual void ConfigureLength(Rule<float> rule)
        {
            rule.Define(3);
        }

        //[CadRule(CadRuleUsageType.Parameter)]
        //public Rule<float> Width { get; set; }
        //protected virtual void ConfigureWidth(Rule<float> rule)
        //{
        //    rule.Define(4);
        //}

        //[CadRule(CadRuleUsageType.Parameter)]
        //public Rule<float> Height { get; set; }
        //protected virtual void ConfigureHeight(Rule<float> rule)
        //{
        //    rule.Define(4);
        //}

        //[CadRule(CadRuleUsageType.Parameter, nameInCad: "G_T")]
        //public Rule<float> Thickness { get; set; }
        //protected virtual void ConfigureThickness(Rule<float> rule)
        //{
        //    rule.Define(0.25f);
        //}

        //[CadRule(CadRuleUsageType.Parameter)]
        public Rule<float> NotchDiameter { get; set; }
        protected virtual void ConfigureNotchDiameter(Rule<float> rule)
        {
            rule.Define(0.5625f);
        }

        //[CadRule(CadRuleUsageType.Parameter)]
        public Rule<float> NotchOffset { get; set; }
        protected virtual void ConfigureNotchOffset(Rule<float> rule)
        {
            rule.Define(0.375f);
        }

        #endregion

        #region IRevisablePartImplementation

        public FactoryRevisionRule<string> FactoryRevision { get; set; }

        #endregion

        #region IBomPart

        public Rule<string> DatabasePartNumber { get; set; }
        public PartNumberRule<string> PartNumber { get; set; }
        public Rule<string> PartNumberPrefix { get; set; }
        public Rule<string> Description { get; set; }
        public Rule<string> BOMDescription { get; set; }
        public Rule<string> BOMDescription1 { get; set; }
        public Rule<string> BOMDescription2 { get; set; }
        public Rule<string> BOMDescription3 { get; set; }
        public Rule<string> BOMDescription4 { get; set; }
        public Rule<string> BOMDescription5 { get; set; }
        public Rule<string> BOMDescription6 { get; set; }
        public Rule<string> BOMDescription7 { get; set; }
        public Rule<string> BOMDescription8 { get; set; }
        public Rule<string> BOMDescription9 { get; set; }
        public Rule<string> BOMDescription10 { get; set; }
        public Rule<string> BOMDescription11 { get; set; }
        public Rule<string> Color { get; set; }
        public ReadOnlyRule<AdvanceColor> AdvanceColor { get; set; }
        public ReadOnlyRule<string> ColorDescription { get; set; }
        public StorageConfiguratorRepository Repository { get; set; }
        public Rule<string> ColorBOMName { get; set; }
        public ReadOnlyRule<string> ColorShortDescription { get; set; }

        public Rule<double> PartWeight { get; set; }
        public Rule<double> PartPaintWeight { get; set; }
        public Rule<float> WeightPerGallon { get; set; }
        public CsiRule<double> PaintCostPerGallon { get; set; }
        public Rule<float> PaintCost { get; set; }

        #endregion

        #region IAngle Implementation

        public Rule<AngleData> ActiveAngleData { get; set; }
        public Rule<List<string>> AngleSizesAvailable { get; set; }
        public Rule<string> AngleType { get; set; }
        protected virtual void ConfigureAngleType(Rule<string> rule)
        {
            rule.Define("4 x 4 x 1/4");
        }
        public Rule<float> AngleWidth { get; set; }
        protected virtual void ConfigureAngleWidth(Rule<float> rule)
        {
            rule.AddToCAD(cad =>
            {
                cad.NameInCad = "Height";
            });
        }
        public Rule<float> AngleHeight { get; set; }
        protected virtual void ConfigureAngleHeight(Rule<float> rule)
        {
            rule.AddToCAD(cad =>
            {
                cad.NameInCad = "Width";
            });
        }
        public Rule<float> AngleThickness { get; set; }
        protected virtual void ConfigureAngleThickness(Rule<float> rule)
        {
            rule.AddToCAD(cad =>
            {
                cad.NameInCad = "Thickness";
            });
        }
        public Rule<List<AngleData>> AllAngleData { get; set; }
        public Rule<List<ChoiceListItem>> AllAngleTypesChoiceList { get; set; }

        #endregion

        #region CPQ.IBomPart Implementation

        //This is the description used only by the BOM Summary.
        public Rule<string> BOMUIDescription { get; set; }
        protected virtual void ConfigureBOMUIDescription(Rule<string> rule)
        {
            rule.Define(() => $"RR Angle({AngleType.Value} x {Helper.fractionString(Length.Value, feetToo: false)})");
        }

        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {
            child.PartNumber.Define(PartNumber);
            child.Description.Define(BOMUIDescription);
            child.MaterialPartNumber.Define(MaterialPartNumber);
            child.MaterialQty.Define(() => Length.Value / 12);
            child.ItemMaterialCost.Define(() => child.ItemRawMaterialWeight.Value * child.MaterialUnitCost.Value);
        }

        #endregion

        #region Material Rules

        public Rule<string> MaterialPartNumber { get; set; }
        protected virtual void ConfigureMaterialPartNumber(Rule<string> rule)
        {
            rule.Define(default(string));
        }

        #endregion

        #region Labor Rules

        public Rule<double> PartCarriersPerHour { get; set; }
        protected virtual void ConfigurePartCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(Colors.Blue);
        }

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define("RubRailNotchedAngle");
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(true);
        }

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureBomPartRules();
            this.ConfigureRevisablePart();
        }

        #endregion

        #region Graphics

        public Block HorizontalLegBlock { get; set; }
        protected virtual void ConfigureHorizontalLegBlock(Block geo)
        {
            geo.Width.Define(AngleHeight);
            geo.Height.Define(AngleThickness);
            geo.Depth.Define(Length);
            geo.Position.Define(() => Helper.Vxy(geo.Width.Value / 2, -geo.Height.Value / 2));
        }

        public Block VerticalLegBlock { get; set; }       

        protected virtual void ConfigureVerticalLegBlock(Block geo)
        {
            geo.Width.Define(AngleThickness);
            geo.Height.Define(AngleWidth);
            geo.Depth.Define(Length);
            geo.Position.Define(() => Helper.Vxy(geo.Width.Value / 2, -geo.Height.Value / 2));
        }

        //public UCS UCS { get; set; }
        //protected virtual void ConfigureUCS(UCS child)
        //{
        //    child.Diameter.Define(0.0125f);
        //    child.Position.Define(() => (Helper.Vy(0)));
        //}

        #endregion

    }
}
