﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;
using Ruler.Cpq;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class DoublerCap : AdvancePart, IRevisablePart, IBomPart, Ruler.Cpq.IBomPart
    {

        #region Constructors

        public DoublerCap(ModelContext modelContext) : base(modelContext)
        {
        }

        public DoublerCap(string name, Part parent) : base(name, parent)
        {
        }

        #endregion

        #region Rules

        [CadRule(CadRuleUsageType.Parameter)]
        public ReadOnlyRule<float> OutsideWidth { get; set; }
        protected virtual void ConfigureOutsideWidth(ReadOnlyRule<float> rule)
        {
            rule.Define(3);
        }

        [CadRule(CadRuleUsageType.Parameter)]
        public ReadOnlyRule<float> OutsideDepth { get; set; }
        protected virtual void ConfigureOutsideDepth(ReadOnlyRule<float> rule)
        {
            rule.Define(() =>
            {
                if (OutsideWidth.Value >= 5)
                    return 3.793f;
                return OutsideWidth.Value >= 4 ? 3.471f : 2.973f;
            });
        }


        [CadRule(CadRuleUsageType.Parameter)]
        public ReadOnlyRule<float> FlatDepth { get; set; }
        protected virtual void ConfigureFlatDepth(ReadOnlyRule<float> rule)
        {
            rule.Define(() =>
            {
                if (OutsideWidth.Value >= 5)
                    return 1.812f;
                return OutsideWidth.Value >= 4 ? 1.642f : 1.397f;
            });
        }


        [CadRule(CadRuleUsageType.Parameter)]
        public ReadOnlyRule<float> InsideWidth { get; set; }
        protected virtual void ConfigureInsideWidth(ReadOnlyRule<float> rule)
        {
            rule.Define(() =>
            {
                if (OutsideWidth.Value >= 5)
                    return 3.533f;
                return OutsideWidth.Value >= 4 ? 2.627f : 1.724f;
            });
        }


        [CadRule(CadRuleUsageType.Parameter)]
        public ReadOnlyRule<float> InsideRadius { get; set; }
        protected virtual void ConfigureInsideRadius(ReadOnlyRule<float> rule)
        {
            rule.Define(() =>
            {
                if (OutsideWidth.Value >= 5)
                    return 0.31f;
                return OutsideWidth.Value >= 4 ? 0.3f : 0.29f;
            });
        }

        [CadRule(CadRuleUsageType.Parameter)]
        public ReadOnlyRule<float> ThicknessOffset { get; set; }
        protected virtual void ConfigureThicknessOffset(ReadOnlyRule<float> rule)
        {
            rule.Define(() =>
            {
                if (OutsideWidth.Value >= 5)
                    return 0.21f;
                return OutsideWidth.Value >= 4 ? 0.172f : 0.165f;
            });
        }

        public ReadOnlyRule<float> SlantOffset { get; set; }
        protected virtual void ConfigureSlantOffset(ReadOnlyRule<float> rule)
        {
            rule.Define(() => .074f * Helper.Cos(TiltAnlge.Value));
        }

        public ReadOnlyRule<float> TiltAnlge { get; set; }
        protected virtual void ConfigureTiltAnlge(ReadOnlyRule<float> rule)
        {
            rule.Define(() =>
            {
                if (OutsideWidth.Value >= 5)
                    return 30.37f;
                return OutsideWidth.Value >= 4 ? 30.44f : 29.25f;
            });
        }

        public Rule<float> Thickness { get; set; }
        protected virtual void ConfigureThickness(Rule<float> rule)
        {
            rule.Define(0.0747f);
        }

        #endregion

        #region IRevisablePartImplementation

        public FactoryRevisionRule<string> FactoryRevision { get; set; }

        #endregion

        #region IBomPart Implementation

        public Rule<string> DatabasePartNumber { get; set; }
        public PartNumberRule<string> PartNumber { get; set; }
        public Rule<string> PartNumberPrefix { get; set; }
        public Rule<string> Description { get; set; }
        public Rule<string> BOMDescription { get; set; }
        public Rule<string> BOMDescription1 { get; set; }
        public Rule<string> BOMDescription2 { get; set; }
        public Rule<string> BOMDescription3 { get; set; }
        public Rule<string> BOMDescription4 { get; set; }
        public Rule<string> BOMDescription5 { get; set; }
        public Rule<string> BOMDescription6 { get; set; }
        public Rule<string> BOMDescription7 { get; set; }
        public Rule<string> BOMDescription8 { get; set; }
        public Rule<string> BOMDescription9 { get; set; }
        public Rule<string> BOMDescription10 { get; set; }
        public Rule<string> BOMDescription11 { get; set; }
        public Rule<string> Color { get; set; }
        public ReadOnlyRule<AdvanceColor> AdvanceColor { get; set; }
        public ReadOnlyRule<string> ColorDescription { get; set; }
        public StorageConfiguratorRepository Repository { get; set; }
        public Rule<string> ColorBOMName { get; set; }
        public ReadOnlyRule<string> ColorShortDescription { get; set; }

        public Rule<double> PartWeight { get; set; }
        public Rule<double> PartPaintWeight { get; set; }
        public Rule<float> WeightPerGallon { get; set; }
        public CsiRule<double> PaintCostPerGallon { get; set; }
        public Rule<float> PaintCost { get; set; }

        #endregion

        #region CPQ.IBomPart Implementation

        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {
            child.PartNumber.Define(PartNumber);
            child.MaterialPartNumber.Define(() => Location.Value == "GA" ? MaterialPartNumber.Value : "");
            child.MaterialQty.Define(() => OutsideWidth.Value * OutsideDepth.Value / 144);
            child.ItemMaterialCost.Define(() =>
            {
                if (Location.Value == "GA")
                {
                    return child.ItemRawMaterialWeight.Value * child.MaterialUnitCost.Value;
                }
                else
                {
                    return 0;
                }
            });
        }

        #endregion

        #region Material Rules

        public Rule<string> MaterialPartNumber { get; set; }
        protected virtual void ConfigureMaterialPartNumber(Rule<string> rule)
        {
            rule.Define(default(string));
        }

        //Used to determine whether component is purchased. UT = Purchased
        public Rule<string> Location { get; set; }
        protected virtual void ConfigureLocation(Rule<string> rule)
        {
            rule.Define(() =>
            {
                var bomLoc = Bom?.PlantCode.Value;
                return bomLoc != null && bomLoc.ToUpper().Contains("UT") ? "UT" : "GA";
            });
        }

        #endregion

            #region Labor Rules

        public Rule<double> PartCarriersPerHour { get; set; }
        protected virtual void ConfigurePartCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        #endregion

        #region Overrides

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(false);
        }

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define("DoublerCap");
        }

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureRevisablePart();
            this.ConfigureBomPartRules();
        }

        #endregion

        
    }
}
