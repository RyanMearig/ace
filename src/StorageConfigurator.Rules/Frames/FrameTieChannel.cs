﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;
using Ruler.Cpq;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class FrameTieChannel : Channel, Ruler.Cpq.IBomPart
    {
        #region Private Fields

        public AdvanceCSIItemService CsiPartService { get; }

        #endregion

        #region Constructors

        public FrameTieChannel(ModelContext modelContext, StorageConfiguratorRepository repo, AdvanceCSIItemService csiPartService) : base(modelContext, repo)
        {
            Repository = repo;
            CsiPartService = csiPartService;
        }

        public FrameTieChannel(string name, Part parent, StorageConfiguratorRepository repo, AdvanceCSIItemService csiPartService) : base(name, parent, repo)
        {
            Repository = repo;
            CsiPartService = csiPartService;
        }

        #endregion

        #region Rules

        public Rule<string> FrameTieType { get; set; }
        protected virtual void ConfigureFrameTieType(Rule<string> rule)
        {
            rule.Define(default(string));
        }

        public Rule<string> HDPartNumberPrefix { get; set; }
        protected virtual void ConfigureHDPartNumberPrefix(Rule<string> rule)
        {
            rule.Define(default(string));
        }

        public Rule<string> NonHDPartNumberPrefix { get; set; }
        protected virtual void ConfigureNonHDPartNumberPrefix(Rule<string> rule)
        {
            rule.Define(default(string));
        }

        protected virtual void ConfigurePartNumberPrefix(Rule<string> rule)
        {
            rule.Define(() => FrameTieType.Value == "rfswhd" ? HDPartNumberPrefix.Value : NonHDPartNumberPrefix.Value);
        }

        #endregion

        #region CPQ.IBomPart Implementation

        protected override void ConfigureDescriptionPrefix(Rule<string> rule)
        {
            rule.Define(() => $"{PartNumberPrefix.Value.Replace("-", "")}");
        }

        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {
            child.PartNumber.Define(PartNumber);
            child.Description.Define(BOMUIDescription);
            child.MaterialPartNumber.Define(MaterialPartNumber);
            child.MaterialQty.Define(() => Length.Value / 12);
            child.ItemMaterialCost.Define(() => child.ItemRawMaterialWeight.Value * child.MaterialUnitCost.Value);
        }

        #endregion

        #region Labor Rules

        public Rule<double> BundleQty { get; set; }
        protected virtual void ConfigureBundleQty(Rule<double> rule)
        {
            rule.Define(() => CsiPartService.GetItemBundle(MaterialPartNumber.Value, Bom.PlantCode.Value));
        }

        #endregion
    }
}
