﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;
using Ruler.Rules;
using Ruler.Cpq;
using Ruler.Rules.Graphics;
using Ruler.Rules.Validation;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class TippmannHorizontal : Channel, IRevisablePart, Ruler.Cpq.IBomPart
    {
        #region Private Fields

        public AdvanceCSIItemService CsiPartService { get; }

        #endregion

        #region Constructors

        public TippmannHorizontal(ModelContext modelContext, StorageConfiguratorRepository repo, AdvanceCSIItemService csiPartService) : base(modelContext, repo)
        {
            CsiPartService = csiPartService;
        }

        public TippmannHorizontal(string name, Part parent, StorageConfiguratorRepository repo, AdvanceCSIItemService csiPartService) : base(name, parent, repo)
        {
            CsiPartService = csiPartService;
        }

        #endregion

        #region Rules

        public Rule<int> Id { get; set; }
        protected virtual void ConfigureId(Rule<int> rule)
        {
            rule.Define(default(int));
        }

        public Rule<bool> IsTopChannel { get; set; }
        protected virtual void ConfigureIsTopChannel(Rule<bool> rule)
        {
            rule.Define(false);
        }

        [CadRule(CadRuleUsageType.Parameter)]
        public Rule<float> HoleDiameter { get; set; }
        protected virtual void ConfigureHoleDiameter(Rule<float> rule)
        {
            rule.Define(() => IsTopChannel.Value ? 1.5f : 0.75f);
        }

        #endregion

        #region IRevisablePartImplementation

        public FactoryRevisionRule<string> FactoryRevision { get; set; }

        #endregion

        #region CPQ.IBomPart Implementation

        protected override void ConfigureDescriptionPrefix(Rule<string> rule)
        {
            rule.Define(() => $"Tip Horiz {PartNumberPrefix.Value.Replace("-", "")}");
        }

        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {
            child.PartNumber.Define(PartNumber);
            child.Description.Define(BOMUIDescription);
        }

        #endregion

        #region Labor Rules

        public Rule<double> BundleQty { get; set; }
        protected virtual void ConfigureBundleQty(Rule<double> rule)
        {
            rule.Define(() => CsiPartService.GetItemBundle(MaterialPartNumber.Value, Bom.PlantCode.Value));
        }

        public Rule<double> PartCarriersPerHour { get; set; }
        protected virtual void ConfigurePartCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(Colors.Red);
        }

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define("TippmannHorizontal");
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(false);
        }

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureRevisablePart();
        }

        #endregion


    }
}
