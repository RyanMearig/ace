﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;
using Ruler.Cpq;
using StorageConfigurator.Data;
using Ruler.Rules.Graphics;

namespace StorageConfigurator.Rules
{
    public class AngleProtectorDustCap : AdvancePart, IRevisablePart, IBomPart, Ruler.Cpq.IBomPart
    {
        #region Constructors

        public AngleProtectorDustCap(ModelContext modelContext) : base(modelContext) { }

        public AngleProtectorDustCap(string name, Part parent) : base(name, parent) { }

        #endregion

        #region Rules

        public Rule<float> ColumnWidth { get; set; }
        protected virtual void ConfigureColumnWidth(Rule<float> rule)
        {
            rule.Define(3);
        }

        [CadRule(CadRuleUsageType.Parameter)]
        public Rule<float> FoldedWidth { get; set; }
        protected virtual void ConfigureFoldedWidth(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (ColumnWidth.Value <= 3)
                {
                    return 2.8125f;
                }
                else if (ColumnWidth.Value < 5)
                {
                    return 3.5625f;
                }
                return 4.25f;
            });
        }

        //[CadRule(CadRuleUsageType.Parameter)]
        //public Rule<float> FoldedAngle { get; set; }
        //protected virtual void ConfigureFoldedAngle(Rule<float> rule)
        //{
        //    rule.Define(46);
        //    rule.AddToUI();
        //}

        [CadRule(CadRuleUsageType.Parameter)]
        public Rule<float> HypLength { get; set; }
        protected virtual void ConfigureHypLength(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (ColumnWidth.Value <= 3)
                {
                    return 2f;
                }
                else if (ColumnWidth.Value < 5)
                {
                    return 2.5f;
                }
                return 3f;
            });
        }

        #endregion

        #region IRevisablePartImplementation

        public FactoryRevisionRule<string> FactoryRevision { get; set; }

        #endregion

        #region IBomPart Implementation

        public Rule<string> DatabasePartNumber { get; set; }
        public PartNumberRule<string> PartNumber { get; set; }
        public Rule<string> PartNumberPrefix { get; set; }
        public Rule<string> Description { get; set; }
        public Rule<string> BOMDescription { get; set; }
        public Rule<string> BOMDescription1 { get; set; }
        public Rule<string> BOMDescription2 { get; set; }
        public Rule<string> BOMDescription3 { get; set; }
        public Rule<string> BOMDescription4 { get; set; }
        public Rule<string> BOMDescription5 { get; set; }
        public Rule<string> BOMDescription6 { get; set; }
        public Rule<string> BOMDescription7 { get; set; }
        public Rule<string> BOMDescription8 { get; set; }
        public Rule<string> BOMDescription9 { get; set; }
        public Rule<string> BOMDescription10 { get; set; }
        public Rule<string> BOMDescription11 { get; set; }
        public Rule<string> Color { get; set; }
        public ReadOnlyRule<AdvanceColor> AdvanceColor { get; set; }
        public ReadOnlyRule<string> ColorDescription { get; set; }
        public StorageConfiguratorRepository Repository { get; set; }
        public Rule<string> ColorBOMName { get; set; }
        public ReadOnlyRule<string> ColorShortDescription { get; set; }

        public Rule<double> PartWeight { get; set; }
        public Rule<double> PartPaintWeight { get; set; }
        public Rule<float> WeightPerGallon { get; set; }
        public CsiRule<double> PaintCostPerGallon { get; set; }
        public Rule<float> PaintCost { get; set; }

        #endregion

        #region CPQ.IBomPart Implementation

        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {
            child.PartNumber.Define(PartNumber);
            child.Description.Define("Angle Protector Dust Cap");
            child.MaterialPartNumber.Define(() => Location.Value == "GA" ? MaterialPartNumber.Value : "");
            child.MaterialQty.Define(() => HypLength.Value /12);
            child.ItemMaterialCost.Define(() =>
            {
                if (Location.Value == "GA")
                {
                    return child.ItemRawMaterialWeight.Value * child.MaterialUnitCost.Value;
                }
                else
                {
                    return 0;
                }
            });
        }

        #endregion

        #region Material Rules

        public Rule<string> MaterialPartNumber { get; set; }
        protected virtual void ConfigureMaterialPartNumber(Rule<string> rule)
        {
            rule.Define(default(string));
        }

        //Used to determine whether component is purchased. UT = Purchased
        public Rule<string> Location { get; set; }
        protected virtual void ConfigureLocation(Rule<string> rule)
        {
            rule.Define(() =>
            {
                var bomLoc = Bom?.PlantCode.Value;
                return bomLoc != null && bomLoc.ToUpper().Contains("UT") ? "UT" : "GA";
            });
        }

        #endregion

        #region Labor Rules

        public Rule<double> BurnPartCarriersPerHour { get; set; }
        protected virtual void ConfigureBurnPartCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> FormPartCarriersPerHour { get; set; }
        protected virtual void ConfigureFormPartCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        #endregion

        #region Overrides

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(false);
        }

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define("AngleProtectorDustCap");
        }

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureRevisablePart();
            this.ConfigureBomPartRules();
        }

        #endregion

        #region Graphics

        //public Block Block { get; set; }
        //protected virtual void ConfigureBlock(Block geo)
        //{
        //    geo.Width.Define(Depth);
        //    geo.Height.Define(0.25f);
        //    geo.Depth.Define(Width);
        //    geo.Position.Define(() => Helper.Vxy(geo.Width.Value / 2, geo.Height.Value / 2));
        //}

        //public UCS UCS { get; set; }
        //protected virtual void ConfigureUCS(UCS child)
        //{
        //    child.Diameter.Define(0.25f);
        //    child.Position.Define(() => (Helper.Vy(0)));
        //}

        #endregion

    }
}
