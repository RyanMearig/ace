﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ruler.Cpq;
using Ruler.Rules;
using Ruler.Rules.Graphics;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class FrameLine : Part, IColorComponent, IRevisablePart
    {
        #region Constructors

        public FrameLine(ModelContext modelContext, StorageConfiguratorRepository repo) : base(modelContext)
        {
            Repository = repo;
        }

        public FrameLine(string name, Part parent, StorageConfiguratorRepository repo) : base(name, parent)
        {
            this.setIndex();
            Repository = repo;

        }

        #endregion

        #region Rules

        public LookupRule<StorageSystem> StorageSystemComp { get; set; }

        public Rule<int> Index { get; set; }
        protected virtual void ConfigureIndex(Rule<int> rule)
        {
            rule.Define(0);
        }

        [UIRule]
        public Rule<string> FrameName { get; set; }
        protected virtual void ConfigureFrameName(Rule<string> rule)
        {
            rule.Define(() =>
            {
                return $"FL{Index.Value}";
            });
            rule.Unique();
        }

        [UIRule]
        public ReadOnlyRule<float> Height { get; set; }
        protected virtual void ConfigureHeight(ReadOnlyRule<float> rule)
        {
            rule.Define(12);
        }

        [UIRule]
        public ReadOnlyRule<float> OverallDepth { get; set; }
        protected virtual void ConfigureOverallDepth(ReadOnlyRule<float> rule)
        {
            rule.Define(default(float));
        }

        //[CadRule(CadRuleUsageType.Parameter, "Depth")]
        public ReadOnlyRule<float> Width { get; set; }
        protected virtual void ConfigureWidth(ReadOnlyRule<float> rule)
        {
            rule.Define(1);
        }

        public Rule<bool> IsSelectiveFrameLine { get; set; }
        protected virtual void ConfigureIsSelectiveFrameLine(Rule<bool> rule)
        {
            rule.Define(false);
        }

        public Rule<bool> IsBackToBack { get; set; }
        protected virtual void ConfigureIsBackToBack(Rule<bool> rule)
        {
            rule.Define(false);
        }

        //[CadRule(CadRuleUsageType.Property, "IsDoubleDeep")]
        //public Rule<string> IsDoubleDeepProp { get; set; }
        //protected virtual void ConfigureIsDoubleDeepProp(Rule<string> rule)
        //{
        //    rule.Define(() => IsDoubleDeep.Value.ToString());
        //}

        //[CadRule(CadRuleUsageType.Property, "IsDoubleDeepPost")]
        //public Rule<string> IsDoubleDeepPostProp { get; set; }
        //protected virtual void ConfigureIsDoubleDeepPostProp(Rule<string> rule)
        //{
        //    rule.Define(() => IsDoubleDeepPost.Value.ToString());
        //}


        //public Rule<string> FrameTieBeamSize { get; set; }
        //protected virtual void ConfigureFrameTieBeamSize(Rule<string> rule)
        //{
        //    rule.Define(UprightColumnSize);
        //    rule.AddToUI(ui =>
        //    {
        //        ui.ReadOnly.Define(() => FrameTieType.Value != "rfswhd");
        //        ui.ChoiceList.Define(() => HeavyWeldedFrameTieChoices.Value);
        //    });
        //}

        public Rule<List<ChoiceListItem>> HeavyWeldedFrameTieChoices { get; set; }
        protected virtual void ConfigureHeavyWeldedFrameTieChoices(Rule<List<ChoiceListItem>> rule)
        {
            rule.Define(() => new List<ChoiceListItem>());
        }

        public Rule<IEnumerable<FrameType>> UsedFrameTypes { get; set; }
        protected virtual void ConfigureUsedFrameTypes(Rule<IEnumerable<FrameType>> rule)
        {
            rule.Define(() =>
            {
                return Helper.GetChildProxyRules<FrameType>(this)?.Select(f => f.Value)?.OrderBy(r => r.Position.Value.X)
                                        ?? Enumerable.Empty<FrameType>();
            });
        }

        public Rule<IEnumerable<FrameTiesSpace>> UsedFrameTieSpaces { get; set; }
        protected virtual void ConfigureUsedFrameTieSpaces(Rule<IEnumerable<FrameTiesSpace>> rule)
        {
            rule.Define(() => this.ActiveChildren.Value?.Where(c => c.TypeName == "FrameTiesSpace")?.Select(s => (FrameTiesSpace)s));
        }

        [UIRule]
        public ReadOnlyRule<bool> IsUsedInElevation { get; set; }
        protected virtual void ConfigureIsUsedInElevation(ReadOnlyRule<bool> rule)
        {
            rule.Define(() =>
            {
                if (StorageSystemComp.Value != null && StorageSystemComp.Value.FrameLinesUsedInElevations.Value.Any())
                {
                    return StorageSystemComp.Value.FrameLinesUsedInElevations.Value.Any(f => f != null && f.Path == this.Path);
                }
                else
                    return false;
            });
        }

        //[CadRule(CadRuleUsageType.Property)]
        //public LookupRule<string> LineItemName { get; set; }
        //protected virtual void ConfigureLineItemName(LookupRule<string> rule)
        //{
        //    rule.Define("[[LineItemName]]");
        //}

        #endregion

        #region IColorComponent Implementation

        public Rule<string> Color { get; set; }
        public ReadOnlyRule<AdvanceColor> AdvanceColor { get; set; }
        public ReadOnlyRule<string> ColorDescription { get; set; }
        public StorageConfiguratorRepository Repository { get; set; }
        public Rule<string> ColorBOMName { get; set; }
        public ReadOnlyRule<string> ColorShortDescription { get; set; }

        public Rule<double> PartWeight { get; set; }
        public Rule<double> PartPaintWeight { get; set; }
        public Rule<float> WeightPerGallon { get; set; }
        public CsiRule<double> PaintCostPerGallon { get; set; }
        public Rule<float> PaintCost { get; set; }


        #endregion

        #region IRevisablePartImplementation

        public FactoryRevisionRule<string> FactoryRevision { get; set; }

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(Colors.Lime);
        }

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define("FrameLine");
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(true);
        }

        protected override void ConfigureAllowedChildTypes(ReadOnlyRule<IEnumerable<Type>> rule)
        {
            rule.Define(() => new List<Type>
            {
                typeof(CustomPart),
                typeof(BuyoutPart),
                typeof(FrameTie),
                typeof(Shim),
                typeof(TouchUpCan)
            });
        }

        #endregion

        #region Protected Methods

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureColorComponent();
            this.ConfigureRevisablePart();
        }

        #endregion

        #region private methods

        private void setIndex()
        {
            if (!Index.HasAdhocDefinedValue)
            {
                var existingFrameLines = Parent.ActiveChildParts.Value
                    .Where(c => c != this && c.TypeName.Contains("FrameLine"));
                var index = existingFrameLines.Count() + 1;
                this.Index.SetAdhocDefaultValue(index);

            }
        }

        #endregion

    }
}
