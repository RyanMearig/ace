﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ruler.Rules;
using Ruler.Rules.Graphics;
using Ruler.Rules.Validation;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class FrameTie : AdvancePart, IChannel, IAngle, IColorComponent, IRevisablePart
    {
        #region Constructors

        public FrameTie(ModelContext modelContext, StorageConfiguratorRepository repo) : base(modelContext)
        {
            Repository = repo;
        }

        public FrameTie(string name, Part parent, StorageConfiguratorRepository repo) : base(name, parent)
        {
            Repository = repo;
        }

        #endregion

        public Rule<FrameTie> FrameTieMaster { get; set; }
        protected virtual void ConfigureFrameTieMaster(Rule<FrameTie> rule)
        {
            rule.Define(default(FrameTie));
        }

        public Rule<bool> IsStandalone { get; set; }
        protected virtual void ConfigureIsStandalone(Rule<bool> rule)
        {
            rule.Define(() => (Parent == null || Parent.GetType() == typeof(ShippableParts)));
        }

        #region Rules
        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> Type { get; set; }
        protected virtual void ConfigureType(Rule<string> rule)
        {
            rule.Define(() => FrameTieMaster.Value?.Type.Value ?? TypeDefault.Value);
            rule.AddToUI((ui) =>
            {
                ui.ChoiceList.Define(() => (new List<ChoiceListItem>()
                {
                    new ChoiceListItem("rs", "No Weld"),
                    new ChoiceListItem("rfsw", "Welded"),
                    new ChoiceListItem("rfswhd", "Heavy Welded"),
                    new ChoiceListItem("rrx", "Rub Rail(RRx)"),
                    new ChoiceListItem("rrxI", "Rub Rail(RRxI)"),
                    new ChoiceListItem("rrxID", "Rub Rail(RRxID)"),
                    new ChoiceListItem("rrxd", "Doubled Rub Rail")
                }));
            });
            rule.ValueSet += (sender, e) =>
            {
                ChannelSize.Reset();
                AngleType.Reset();
            };
        }

        public Rule<string> TypeDefault { get; set; }
        protected virtual void ConfigureTypeDefault(Rule<string> rule)
        {
            rule.Define("rs");
        }

        public Rule<string> ChannelSizeDefault { get; set; }
        protected virtual void ConfigureChannelSizeDefault(Rule<string> rule)
        {
            rule.Define(ColumnChannelSize);
        }

        public Rule<string> AngleTypeDefault { get; set; }
        protected virtual void ConfigureAngleTypeDefault(Rule<string> rule)
        {
            rule.Define(() =>
            {
                if (AngleSizesAvailable.Value.Count > 0)
                {
                    return AngleSizesAvailable.Value.First();
                }
                return "";
            });
        }

        public Rule<bool> HasFrameTypeParent { get; set; }
        protected virtual void ConfigureHasFrameTypeParent(Rule<bool> rule)
        {
            rule.Define(() => (Parent != null && (Parent.GetType() == typeof(SelectiveFrameType) || Parent.GetType() == typeof(DoubleDeepFrameType))));
        }

        [CadRule(CadRuleUsageType.Property)] //DIM_A
        [UIRule]
        public Rule<float> Length { get; set; }
        protected virtual void ConfigureLength(Rule<float> rule)
        {
            rule.Define(12);
            rule.Min(3);
        }

        public Rule<string> ColumnChannelSize { get; set; }
        protected virtual void ConfigureColumnChannelSize(Rule<string> rule)
        {
            rule.Define(() => AllChannelSizesChoiceList.Value.First().Value.ToString());
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() =>
                {
                    if (HasFrameTypeParent.Value)
                        return AllChannelSizesChoiceList.Value;
                    else
                        return ChannelSize.UIMetadata.ChoiceList.Value;
                });
            });
        }

        public Rule<float> ColumnChannelWidth { get; set; }
        protected virtual void ConfigureColumnChannelWidth(Rule<float> rule)
        {
            rule.Define(() => AllChannelData.Value.FirstOrDefault(d => d.ChannelSize == ColumnChannelSize.Value).Width);
        }

        public Rule<string> ColumnChannelSizeId { get; set; }
        protected virtual void ConfigureColumnChannelSizeId(Rule<string> rule)
        {
            rule.Define(() => this.GetChannelId());
        }

        public Rule<bool> IsAngleFrameTie { get; set; }
        protected virtual void ConfigureIsAngleFrameTie(Rule<bool> rule)
        {
            rule.Define(() => (Type.Value == "rs" || Type.Value == "rfsw"));
        }

        public Rule<string> UprightColor { get; set; }
        protected virtual void ConfigureUprightColor(Rule<string> rule)
        {
            rule.Define("blue");
            //rule.AddToUI((ui) =>
            //{
            //    ui.ChoiceList.Define(() => (new List<ChoiceListItem>()
            //    {
            //        new ChoiceListItem("blue", "Blue"),
            //        new ChoiceListItem("green", "Green"),
            //        new ChoiceListItem("orange", "Orange")
            //    }));
            //});
        }

        public Rule<double> Qty { get; set; }
        protected virtual void ConfigureQty(Rule<double> rule)
        {
            rule.Define(1);
            rule.AddToUI(ui =>
            {
                ui.Label.Define("Quantity");
                //ui.ReadOnly.Define(() => !IsStandalone.Value);
            });
        }

        public Rule<List<ChoiceListItem>> HorizBraceChoices { get; set; }
        protected virtual void ConfigureHorizBraceChoices(Rule<List<ChoiceListItem>> rule)
        {
            rule.Define(() => new List<ChoiceListItem>());
        }

        public Rule<string> AttachmentLocationDefault { get; set; }
        protected virtual void ConfigureAttachmentLocationDefault(Rule<string> rule)
        {
            rule.Define(() => HorizBraceChoices.Value.Any() ? HorizBraceChoices.Value.First().Value.ToString() : "");
        }

        public Rule<string> AttachmentLocation { get; set; }
        protected virtual void ConfigureAttachmentLocation(Rule<string> rule)
        {
            rule.Define(() => FrameTieMaster.Value?.AttachmentLocation.Value ?? AttachmentLocationDefault.Value);
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() => HorizBraceChoices.Value);
                ui.ReadOnly.Define(() => !HorizBraceChoices.Value.Any());
            });
        }

        #endregion

        #region IAngleData Implementation
        public Rule<List<AngleData>> AllAngleData { get; set; }
        public Rule<List<ChoiceListItem>> AllAngleTypesChoiceList { get; set; }
        public Rule<AngleData> ActiveAngleData { get; set; }
        public Rule<List<string>> AngleSizesAvailable { get; set; }
        protected virtual void ConfigureAngleSizesAvailable(Rule<List<string>> rule)
        {
            rule.Define(() =>
            {
                List<string> choices = new List<string>();

                if (Type.Value == "rs")
                {
                    choices.Add(@"1-1/2 x 1-1/2 x 1/8");
                    choices.Add(@"2 x 2 x 1/8");
                }
                else if (Type.Value == "rfsw")
                {
                    choices.Add(@"1-1/2 x 1-1/2 x 1/8");
                    choices.Add(@"2 x 2 x 1/8");
                    choices.Add(@"2 x 2 x 3/16");
                }
                
                return choices;
            });
        }

        public Rule<string> AngleType { get; set; }
        protected virtual void ConfigureAngleType(Rule<string> rule)
        {
            rule.Define(() => FrameTieMaster.Value?.AngleType.Value ?? AngleTypeDefault.Value);
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !IsAngleFrameTie.Value);
                ui.ChoiceList.Define(() => Helper.GetFilteredChoices(AllAngleTypesChoiceList.Value, AngleSizesAvailable.Value));
            });
        }
        public Rule<float> AngleWidth { get; set; }
        public Rule<float> AngleHeight { get; set; }
        public Rule<float> AngleThickness { get; set; }
        public Rule<string> MaterialPartNumber { get; set; }

        #endregion

        #region IChannel Implementation

        public Rule<List<ChannelData>> AllChannelData { get; set; }
        public Rule<List<ChoiceListItem>> AllChannelSizesChoiceList { get; set; }
        public Rule<ChannelData> ActiveChannelData { get; set; }
        public Rule<float> ChannelWidth { get; set; }
        public Rule<float> ChannelDepth { get; set; }
        public Rule<float> ChannelWebThickness { get; set; }
        public Rule<float> ChannelWeightPerFt { get; set; }
        public Rule<List<string>> ChannelSizesAvailable { get; set; }
        protected virtual void ConfigureChannelSizesAvailable(Rule<List<string>> rule)
        {
            rule.Define(() =>
            {
                if (Type.Value == "rfswhd")
                    return (new List<string> { "C3x3.5", "C3x4.1", "C4x4.5", "C4x5.4", "C5x6.1", "C5x6.7" });
                else if (Type.Value.Contains("rrx"))
                    return (new List<string> { "C3x3.5", "C3x4.1", "C4x4.5", "C4x5.4", "C5x6.7" });
                else
                    return new List<string>();
            });
        }

        public Rule<string> ChannelSize { get; set; }
        protected virtual void ConfigureChannelSize(Rule<string> rule)
        {
            rule.Define(() => FrameTieMaster.Value?.ChannelSize.Value ?? ChannelSizeDefault.Value);
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() =>
                {
                    if (Type.Value == "rfswhd")
                        return false;
                    else
                        return (IsAngleFrameTie.Value || !HasFrameTypeParent.Value);
                });
                ui.Label.Define("Beam Size");
                ui.ChoiceList.Define(() => Helper.GetFilteredChoices(AllChannelSizesChoiceList.Value, ChannelSizesAvailable.Value));
            });
        }

        public Rule<bool> FrontDoubleratConnection { get; set; }
        protected virtual void ConfigureFrontDoubleratConnection(Rule<bool> rule)
        {
            rule.Define(false);
        }

        public Rule<bool> RearDoubleratConnection { get; set; }
        protected virtual void ConfigureRearDoubleratConnection(Rule<bool> rule)
        {
            rule.Define(false);
        }

        public Rule<List<BoltDefinition>> BoltDefinitions { get; set; }
        protected virtual void ConfigureBoltDefinitions(Rule<List<BoltDefinition>> rule)
        {
            rule.Define(() =>
            {
                var bolts = new List<BoltDefinition>();

                var frontBoltConnectionBolts = GetEndBoltDefinition(FrontDoubleratConnection.Value);
                var rearBoltConnectionBolts = GetEndBoltDefinition(RearDoubleratConnection.Value);
                
                if (frontBoltConnectionBolts.Length == rearBoltConnectionBolts.Length && 
                    frontBoltConnectionBolts.HasWashers == rearBoltConnectionBolts.HasWashers &&
                    frontBoltConnectionBolts.BoltType == rearBoltConnectionBolts.BoltType)
                {
                    var retBoltDefinition = frontBoltConnectionBolts;
                    retBoltDefinition.Qty = (frontBoltConnectionBolts.Qty + rearBoltConnectionBolts.Qty);
                    bolts.Add(retBoltDefinition);
                }
                else
                {
                    bolts.Add(frontBoltConnectionBolts);
                    bolts.Add(rearBoltConnectionBolts);
                }

                return bolts;
            });
        }

        #endregion

        #region IRevisablePartImplementation

        public FactoryRevisionRule<string> FactoryRevision { get; set; }

        #endregion

        #region IColorComponent Implementation

        public Rule<string> Color { get; set; }
        public ReadOnlyRule<AdvanceColor> AdvanceColor { get; set; }
        public ReadOnlyRule<string> ColorDescription { get; set; }
        public StorageConfiguratorRepository Repository { get; set; }
        public Rule<string> ColorBOMName { get; set; }
        public ReadOnlyRule<string> ColorShortDescription { get; set; }

        public Rule<double> PartWeight { get; set; }
        public Rule<double> PartPaintWeight { get; set; }
        public Rule<float> WeightPerGallon { get; set; }
        public CsiRule<double> PaintCostPerGallon { get; set; }
        public Rule<float> PaintCost { get; set; }

        #endregion

        #region Children

        public RSFrameTie RSFrameTie { get; set; }
        protected virtual void ConfigureRSFrameTie(RSFrameTie child)
        {
            child.IsActive.Define(() => Type.Value == "rs");
            child.AngleType.Define(AngleType);
            child.OverallLength.Define(Length);
            child.Type.Define(Type);
            child.Qty.Define(Qty);
        }

        public WeldedFrameTie RSFWFrameTie { get; set; }
        protected virtual void ConfigureRSFWFrameTie(WeldedFrameTie child)
        {
            child.IsActive.Define(() => Type.Value == "rfsw");
            child.AngleType.Define(AngleType);
            child.OverallLength.Define(Length);
            child.BeamWidth.Define(ColumnChannelWidth);
            child.BeamSize.Define(ColumnChannelSize);
            child.BeamID.Define(ColumnChannelSizeId);
            child.Type.Define(Type);
            child.Qty.Define(Qty);
        }

        public WeldedRubRail WeldedRubRail { get; set; }
        protected virtual void ConfigureWeldedRubRail(WeldedRubRail child)
        {
            child.IsActive.Define(() => (Type.Value == "rrx" || Type.Value == "rrxI" || Type.Value == "rrxID"));
            child.Type.Define(Type);
            child.ChannelSize.Define(ChannelSize);
            child.OverallLength.Define(Length);
            child.BeamWidth.Define(ColumnChannelWidth);
            child.Qty.Define(Qty);
        }

        public WeldedHDFrameTie RFSWHDFrameTie { get; set; }
        protected virtual void ConfigureRFSWHDFrameTie(WeldedHDFrameTie child)
        {
            child.IsActive.Define(() => Type.Value == "rfswhd");
            child.ChannelSize.Define(ChannelSize);
            child.OverallLength.Define(Length);
            child.BeamWidth.Define(ColumnChannelWidth);
            child.BeamSize.Define(ColumnChannelSize);
            child.Type.Define(Type);
            child.Qty.Define(Qty);
        }

        public DoubledRubRail RRXDRubRail { get; set; }
        protected virtual void ConfigureRRXDRubRail(DoubledRubRail child)
        {
            child.IsActive.Define(() => Type.Value == "rrxd");
            child.ChannelSize.Define(ChannelSize);
            child.OverallLength.Define(Length);
            child.Type.Define(Type);
            child.Qty.Define(Qty);
        }

        public ChildList<HardwareGroup> Hardwares { get; set; }
        protected virtual void ConfigureHardwares(ChildList<HardwareGroup> list)
        {
            list.Qty.Define(() => BoltDefinitions.Value.Count);
            list.ConfigureChildren((child, index) =>
            {
                child.Qty.Define(() => BoltDefinitions.Value[index].Qty);
                child.BoltSize.Define(() => BoltDefinitions.Value[index].BoltSize);
                child.BoltLength.Define(() => BoltDefinitions.Value[index].Length);
                child.BoltType.Define(() => BoltDefinitions.Value[index].BoltType);
                child.BoltMaterial.Define(() => BoltDefinitions.Value[index].BoltMaterial);
                child.HasWashers.Define(() => BoltDefinitions.Value[index].HasWashers);
                child.WasherType.Define(() => BoltDefinitions.Value[index].WasherType);
                child.WasherMaterial.Define(() => BoltDefinitions.Value[index].WasherMaterial);
            });
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(() => 
            {
                if (UprightColor.Value == "blue")
                    return Colors.Blue;
                else if (UprightColor.Value == "green")
                    return Colors.Green;
                else if (UprightColor.Value == "orange")
                    return 16753920;
                else
                    return Colors.Teal;
            });
        }

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define("FrameTieAssembly");
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(() => !HasFrameTypeParent.Value || IsAdhoc);
        }

        protected override void ConfigureAllowedChildTypes(ReadOnlyRule<IEnumerable<Type>> rule)
        {
            rule.Define(() => new List<Type>
            {
                typeof(CustomPart)
            });
        }

        #endregion

        #region Methods

        public BoltDefinition GetEndBoltDefinition(bool hasDoubler)
        {
            var boltDef = new BoltDefinition()
            {
                BoltSize = 0.5f,
                BoltType = BoltTypes.Hex,
                BoltMaterial = HardwareMaterial.Grade5,
                Qty = Type.Value == "rs" ? 1 : 2
            };

            if (hasDoubler)
            {
                if (ChannelWidth.Value <= 4)
                {
                    boltDef.Length = 4;
                }
                else
                {
                    boltDef.Length = 4.5f;
                }
            }
            else
            {
                if (ChannelWidth.Value <= 4)
                {
                    boltDef.Length = 1;
                }
                else
                {
                    boltDef.Length = 1.25f;
                    boltDef.HasWashers = true;
                    boltDef.WasherType = WasherTypes.Clipped;
                    boltDef.WasherMaterial = HardwareMaterial.ZincPlated;
                }
            }

            return boltDef;
        }

        #endregion

        #region Graphics

        //public Block FrameTieBlock { get; set; }
        //protected virtual void ConfigureFrameTieBlock(Block geo)
        //{
        //    geo.Width.Define(Length);
        //    geo.Height.Define(() =>
        //    {
        //        if (IsAngleFrameTie.Value)
        //            return AngleHeight.Value;
        //        else if (Type.Value == "rfswhd")
        //            return ChannelWidth.Value;
        //        else if (Type.Value == "rrxd")
        //            return (ChannelDepth.Value * 2);
        //        else
        //            return (ChannelDepth.Value);
        //    });
        //    geo.Depth.Define(() =>
        //    {
        //        if (IsAngleFrameTie.Value)
        //            return AngleWidth.Value;
        //        else if (Type.Value == "rfswhd")
        //            return ChannelDepth.Value;
        //        else
        //            return (ChannelWidth.Value);
        //    });
        //    geo.Position.Define(() => Helper.Vxy(-Length.Value / 2, -geo.Height.Value/2));
        //}

        //public UCS UCS { get; set; }
        //protected virtual void ConfigureUCS(UCS child)
        //{
        //    child.Diameter.Define(0.25f);
        //    child.Position.Define(() => (Helper.Vy(0)));
        //}

        #endregion

        #region Protected Methods

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureChannelRules();
            this.ConfigureAngleRules();
            this.ConfigureRevisablePart();
            this.ConfigureColorComponent();
        }

        #endregion
    }
}
