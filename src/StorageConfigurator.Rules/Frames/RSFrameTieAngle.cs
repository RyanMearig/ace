﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Cpq;
using Ruler.Rules;
using Ruler.Rules.Graphics;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class RSFrameTieAngle : AdvancePart, IAngle, Ruler.Cpq.IBomPart, IBomPart
    {
        #region Constructors

        public RSFrameTieAngle(ModelContext modelContext, StorageConfiguratorRepository repo) : base(modelContext)
        {
            Repository = repo;
        }

        public RSFrameTieAngle(string name, Part parent, StorageConfiguratorRepository repo) : base(name, parent)
        {
            Repository = repo;
        }

        #endregion

        #region Rules

        public ReadOnlyRule<float> AngleFlatCutLength { get; set; }
        protected virtual void ConfigureAngleFlatCutLength(ReadOnlyRule<float> rule)
        {
            rule.Define(default(float));
        }

        public Rule<float> Length { get; set; }
        protected virtual void ConfigureLength(Rule<float> rule)
        {
            rule.Define(default(float));
        }

        #endregion

        #region Labor Hour Rules

        public Rule<double> FicepPartCarriersPerHour { get; set; }
        protected virtual void ConfigureFicepPartCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> AccurpressPartCarriersPerHour { get; set; }
        protected virtual void ConfigureAccurpressPartCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        #endregion

        #region IAngleData Implementation
        public Rule<List<AngleData>> AllAngleData { get; set; }
        public Rule<List<ChoiceListItem>> AllAngleTypesChoiceList { get; set; }
        public Rule<AngleData> ActiveAngleData { get; set; }
        public Rule<List<string>> AngleSizesAvailable { get; set; }
        public Rule<string> AngleType { get; set; }
        public Rule<float> AngleWidth { get; set; }
        public Rule<float> AngleHeight { get; set; }
        public Rule<float> AngleThickness { get; set; }
        public Rule<string> MaterialPartNumber { get; set; }

        #endregion

        #region IBomPart Implementation

        public Rule<string> DatabasePartNumber { get; set; }
        public Rule<string> PartNumberPrefix { get; set; }
        public PartNumberRule<string> PartNumber { get; set; }
        public Rule<string> Description { get; set; }
        public Rule<string> BOMDescription { get; set; }
        public Rule<string> BOMDescription1 { get; set; }
        public Rule<string> BOMDescription2 { get; set; }
        public Rule<string> BOMDescription3 { get; set; }
        public Rule<string> BOMDescription4 { get; set; }
        public Rule<string> BOMDescription5 { get; set; }
        public Rule<string> BOMDescription6 { get; set; }
        public Rule<string> BOMDescription7 { get; set; }
        public Rule<string> BOMDescription8 { get; set; }
        public Rule<string> BOMDescription9 { get; set; }
        public Rule<string> BOMDescription10 { get; set; }
        public Rule<string> BOMDescription11 { get; set; }

        public Rule<string> Color { get; set; }
        public ReadOnlyRule<AdvanceColor> AdvanceColor { get; set; }
        public ReadOnlyRule<string> ColorDescription { get; set; }
        public StorageConfiguratorRepository Repository { get; set; }
        public Rule<string> ColorBOMName { get; set; }
        public ReadOnlyRule<string> ColorShortDescription { get; set; }

        public Rule<double> PartWeight { get; set; }
        public Rule<double> PartPaintWeight { get; set; }
        protected virtual void ConfigurePartPaintWeight(Rule<double> rule)
        {
            rule.Define(0);
        }
        public Rule<float> WeightPerGallon { get; set; }
        public CsiRule<double> PaintCostPerGallon { get; set; }
        public Rule<float> PaintCost { get; set; }


        #endregion

        #region Ruler CPQ IBomPart Implementation

        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {
            child.IsPurchased.Define(false);
            child.PartNumber.Define(PartNumber);
            child.Description.Define(Description);
            child.MaterialPartNumber.Define(MaterialPartNumber);
            child.MaterialQty.Define(() => AngleFlatCutLength.Value / 12);
            child.ItemMaterialCost.Define(() => child.ItemRawMaterialWeight.Value * child.MaterialUnitCost.Value);
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(Colors.Gray);
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(false);
        }

        public override bool IsCadPart => false;

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureBomPartRules();
            this.ConfigureAngleRules();
        }

        #endregion

    }
}
