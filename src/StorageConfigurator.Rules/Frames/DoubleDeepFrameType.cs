﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using Ruler.Rules;
using Ruler.Rules.Validation;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class DoubleDeepFrameType : FrameType
    {
        #region Constructors

        public DoubleDeepFrameType(ModelContext modelContext, StorageConfiguratorRepository repo) : base(modelContext, repo) { }

        public DoubleDeepFrameType(string name, Part parent, StorageConfiguratorRepository repo) : base(name, parent, repo) { }

        #endregion

        #region Rules

        public Rule<DoubleDeepFrameType> DoubleDeepFrameTypeMaster { get; set; }
        protected virtual void ConfigureDoubleDeepFrameTypeMaster(Rule<DoubleDeepFrameType> rule)
        {
            rule.Define(default(DoubleDeepFrameType));
        }

        public Rule<bool> MasterSupplied { get; set; }
        protected virtual void ConfigureMasterSupplied(Rule<bool> rule)
        {
            rule.Define(() => DoubleDeepFrameTypeMaster.Value != default(DoubleDeepFrameType));
        }

        [UIRule]
        public Rule<int> FrameTieQty { get; set; }
        protected virtual void ConfigureFrameTieQty(Rule<int> rule)
        {
            rule.Define(() =>
            {
                if (MasterSupplied.Value)
                {
                    return DoubleDeepFrameTypeMaster.Value.FrameTieQty.Value;
                }

                return (IsDoubleDeepPost.Value ? HorizBraceDict.Value.Count() : DefaultFrameFrameTieLocations.Value.Count());
            });
            rule.ValueReset += (sender, e) =>
            {
                FrameFrameTie?.ToList().ForEach(t => t.AttachmentLocation.Reset());
            };
        }

        public Rule<List<string>> HeavyWeldedFrameTieSizesAvailable { get; set; }
        protected virtual void ConfigureHeavyWeldedFrameTieSizesAvailable(Rule<List<string>> rule)
        {
            rule.Define(() => (new List<string> { "C3x3.5", "C3x4.1", "C4x4.5", "C4x5.4", "C5x6.1", "C5x6.7" }));
        }

        public Rule<int> RubRailQty { get; set; }
        protected virtual void ConfigureRubRailQty(Rule<int> rule)
        {
            rule.Define(() =>
            {
                if (MasterSupplied.Value)
                {
                    return DoubleDeepFrameTypeMaster.Value.RubRailQty.Value;
                }

                return (FrameTieType.Value.ToLower().Contains("rrx") ? 1 : 0);
            });
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !(IsDoubleDeepPost.Value && FrameTieType.Value.ToLower().Contains("rrx")));
            });
        }

        public Rule<bool> IsAngleFrameTie { get; set; }
        protected virtual void ConfigureIsAngleFrameTie(Rule<bool> rule)
        {
            rule.Define(() =>
            {
                if (MasterSupplied.Value)
                {
                    return DoubleDeepFrameTypeMaster.Value.IsAngleFrameTie.Value;
                }
                return (FrameTieType.Value == "rs" || FrameTieType.Value == "rfsw");
            });
        }

        public Rule<string> FrameTieAngleType { get; set; }
        protected virtual void ConfigureFrameTieAngleType(Rule<string> rule)
        {
            rule.Define(() =>
            {
                if (MasterSupplied.Value)
                {
                    return DoubleDeepFrameTypeMaster.Value.FrameTieAngleType.Value;
                }
                return rule.UIMetadata.ChoiceList.Value.First().Value.ToString();
            });
            rule.AddToUI(ui =>
            {
                ui.Label.Define("Default Frame Tie Angle Type");
                ui.ReadOnly.Define(() => !IsAngleFrameTie.Value);
                ui.ChoiceList.Define(() =>
                {
                    if (IsDoubleDeepPost.Value)
                    {
                        return Helper.GetFilteredChoices(FramePostTie.First().AllAngleTypesChoiceList.Value, DefaultFrameTieAngleSizes.Value);
                    }
                    else
                    {
                        return Helper.GetFilteredChoices(FrameFrameTie.First().AllAngleTypesChoiceList.Value, DefaultFrameTieAngleSizes.Value);
                    }
                });
            });
        }

        public Rule<List<string>> DefaultFrameTieAngleSizes { get; set; }
        protected virtual void ConfigureDefaultFrameTieAngleSizes(Rule<List<string>> rule)
        {
            rule.Define(() =>
            {
                List<string> choices = new List<string>();

                if (FrameTieType.Value == "rs")
                {
                    choices.Add(@"1-1/2 x 1-1/2 x 1/8");
                    choices.Add(@"2 x 2 x 1/8");
                }
                else if (FrameTieType.Value == "rfsw")
                {
                    choices.Add(@"1-1/2 x 1-1/2 x 1/8");
                    choices.Add(@"2 x 2 x 1/8");
                    choices.Add(@"2 x 2 x 3/16");
                }

                return choices;
            });
        }

        public Rule<Dictionary<string, float>> HorizBraceDict { get; set; }
        protected virtual void ConfigureHorizBraceDict(Rule<Dictionary<string, float>> rule)
        {
            rule.Define(() =>
            {
                if (MasterSupplied.Value)
                {
                    return DoubleDeepFrameTypeMaster.Value.HorizBraceDict.Value;
                }

                var retVal = new Dictionary<string, float>();
                FrontFrame?.Frame.HorizontalBrace.ToList().ForEach(b => retVal.Add(b.Path, b.OffsetFromBottom.Value));

                return retVal;
            });
        }

        public Rule<List<ChoiceListItem>> FrameHorizBraces { get; set; }
        protected virtual void ConfigureFrameHorizBraces(Rule<List<ChoiceListItem>> rule)
        {
            rule.Define(() =>
            {
                if (IsDoubleDeepPost.Value)
                {
                    return new List<ChoiceListItem>();
                }
                return HorizBraceDict.Value?.Select((b, i) => new ChoiceListItem(b.Key, $"Horiz Brace {i + 1}")).ToList();
            });
        }

        public Rule<List<string>> DefaultFrameFrameTieLocations { get; set; }
        protected virtual void ConfigureDefaultFrameFrameTieLocations(Rule<List<string>> rule)
        {
            rule.Define(() =>
            {
                List<string> retVal = new List<string>();
                Dictionary<string, float> horizBraceLocsTopDown = HorizBraceDict.Value?.OrderByDescending(d => d.Value).ToDictionary(b => b.Key, y => y.Value);

                retVal = RecurseGetLocs(horizBraceLocsTopDown, retVal, horizBraceLocsTopDown.FirstOrDefault().Value);
                return retVal;
            });
        }

        public Rule<List<string>> FrameFrameTieLocations { get; set; }
        protected virtual void ConfigureFrameFrameTieLocations(Rule<List<string>> rule)
        {
            rule.Define(() =>
            {
                if (FrameTieQty.Value != DefaultFrameFrameTieLocations.Value.Count())
                {
                    return HorizBraceDict.Value?.Select(d => d.Key).Take(FrameTieQty.Value).ToList();
                }
                return DefaultFrameFrameTieLocations.Value;
            });
        }

        #endregion

        #region FrameType Overrides

        protected override void ConfigureFrameTypeMaster(Rule<FrameType> rule)
        {
            rule.Define(() => DoubleDeepFrameTypeMaster.Value == default(DoubleDeepFrameType) ? default(FrameType) : DoubleDeepFrameTypeMaster.Value);
        }

        protected override void ConfigureHeavyWeldedFrameTieChoices(Rule<List<ChoiceListItem>> rule)
        {
            rule.Define(() =>
            {
                if (FrameTieBeamSize.UIMetadata.ReadOnly.Value)
                {
                    return null;
                }
                else
                {
                    if (IsDoubleDeepPost.Value)
                    {
                        return Helper.GetFilteredChoices(FramePostTie.First().AllChannelSizesChoiceList.Value, HeavyWeldedFrameTieSizesAvailable.Value);
                    }
                    else
                    {
                        return Helper.GetFilteredChoices(FrameFrameTie.First().AllChannelSizesChoiceList.Value, HeavyWeldedFrameTieSizesAvailable.Value);
                    }
                }
            });
        }

        protected virtual void ConfigureDoubleDeepType(Rule<string> rule)
        {
            rule.Define(() => DoubleDeepFrameTypeMaster.Value?.DoubleDeepType.Value ?? "frameFrame");
            rule.ValueSet += (sender, e) =>
            {
                FrameTieQty.Reset();
            };
        }

        protected virtual void ConfigureFrameTieLength(Rule<float> rule)
        {
            rule.Define(() => DoubleDeepFrameTypeMaster.Value?.FrameTieLength.Value ?? 12);
        }

        protected virtual void ConfigureIsDoubleDeep(Rule<bool> rule)
        {
            rule.Define(true);
        }

        protected virtual void ConfigureFrontDoublerHeightMin(Rule<float> rule)
        {
            rule.Define(() =>
            {
                float retVal = 12;

                if (UprightType.Value == "B")
                    retVal = SlantElev.Value + 48;
                else if (Slanted.Value)
                    retVal = retVal + SlantElev.Value;

                return retVal;
            });
        }

        protected virtual void ConfigureFrontDoublerHeight(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (MasterSupplied.Value)
                {
                    return DoubleDeepFrameTypeMaster.Value.FrontDoublerHeight.Value;
                }
                return (FrontFrame.Frame.FrontColumnHeight.Value > 84 ? 84 : Helper.RoundDownToNearest(FrontFrame.Frame.FrontColumnHeight.Value, increment: 4));
            });
            rule.Min(FrontDoublerHeightMin);
            rule.Max(Height);
            rule.MultipleOf(4);
            rule.AddToUI(ui =>
            {
                //to be used for turning off the property
                ui.ReadOnly.Define(() => !HasFrontDoubler.Value);
            });
        }

        protected virtual void ConfigureRearDoublerHeight(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (MasterSupplied.Value)
                {
                    return DoubleDeepFrameTypeMaster.Value.RearDoublerHeight.Value;
                }
                return (FrontFrame.Frame.RearColumnHeight.Value > 84 ? 84 : Helper.RoundDownToNearest(FrontFrame.Frame.RearColumnHeight.Value, increment: 4));
            });
            rule.Max(Height);
            rule.MultipleOf(4);
            rule.AddToUI(ui =>
            {
                //to be used for turning off the property
                ui.ReadOnly.Define(() => !HasRearDoubler.Value);
            });
        }

        protected override void ConfigurePanelQty(ReadOnlyRule<int> rule)
        {
            //To be overridden to pull data from front frame
            rule.Define(() => FrontFrame.Frame.PanelQty.Value);
        }

        protected override void ConfigureWidth(Rule<float> rule)
        {
            rule.Define(() => FrontFrame.Frame.FrameWidth.Value);
        }

        protected override void ConfigureFrontFrameDepthDisplay(Rule<float> rule)
        {
            rule.Define(() => FrontFrame.Frame.Depth.Value);
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(true);
                ui.Label.Define("Front Frame Depth");
            });
        }

        protected override void ConfigureRearFrameDepthDisplay(Rule<float> rule)
        {
            rule.Define(() => RearFrame.Frame.Depth.Value);
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !IsDoubleDeepPost.Value);
                ui.Label.Define(() => (IsDoubleDeepPost.Value ? "Rear Space Depth" : "Rear Frame Depth"));
            });
        }

        protected virtual void ConfigureRearFrameDepth(Rule<float> rule)
        {
            rule.Define(() => (IsDoubleDeep.Value && IsDoubleDeepPost.Value ? (FrontFrameDepthDisplay.Value - 2) : FrontFrameDepthDisplay.Value));
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !IsDoubleDeepPost.Value);
            });
        }

        protected virtual void ConfigureFootplateWidth(Rule<float> rule)
        {
            rule.Define(() => FrontFrame.Frame.DefaultFootplateWidth.Value);
            rule.AddToUI(ui =>
            {
                ui.Label.Define("Default Footplate Width");
            });
        }

        protected virtual void ConfigureFrameTieType(Rule<string> rule)
        {
            rule.Define(() => DoubleDeepFrameTypeMaster.Value?.FrameTieType.Value ?? "rs");
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() => (new List<ChoiceListItem>()
                {
                    new ChoiceListItem("rs", "No Weld"),
                    new ChoiceListItem("rfsw", "Welded"),
                    new ChoiceListItem("rfswhd", "Heavy Welded"),
                    new ChoiceListItem("rrx", "Rub Rail(RRx)"),
                    new ChoiceListItem("rrxd", "Doubled Rub Rail")
                }));
            });
            rule.AddToCAD(cad =>
            {
                cad.CadRuleUsageType = CadRuleUsageType.Property;
            });
        }

        #endregion

        #region Private Methods

        private float GetFrameTieVertOffset(string horizPath)
        {
            return HorizBraceDict.Value.TryGetValue(horizPath, out float offset) ? offset : 0;
        }

        private List<string> RecurseGetLocs(Dictionary<string, float> searchDict, List<string> locList, float searchVal)
        {
            List<string> retVal = locList;
            string closestLoc = GetClosestLocation(searchDict, searchVal);
            retVal.Add(closestLoc);

            float newSearchVal = searchDict.GetValueOrDefault(closestLoc) - 120;
            if (newSearchVal > searchDict.Last().Value)
            {
                retVal = RecurseGetLocs(searchDict, retVal, newSearchVal);
            }

            return retVal;
        }

        public string GetClosestLocation(Dictionary<string, float> searchDict, float searchVal)
        {
            return searchDict.Where(x => x.Value >= searchVal).Last().Key;
        }

        private List<float> RecurseGetLocs(List<float> searchList, List<float> locList, float searchVal)
        {
            List<float> retVal = locList;
            float closestLoc = GetClosestLocation(searchList, searchVal);
            retVal.Add(closestLoc);

            float newSearchVal = closestLoc - 120;
            if (newSearchVal > searchList.Last())
                retVal = RecurseGetLocs(searchList, retVal, newSearchVal);

            return retVal;
        }

        public float GetClosestLocation(List<float> searchList, float searchVal)
        {
            return searchList.Where(x => x >= searchVal).Last();
        }

        private float GetFramePostTieLength(float yOffset)
        {
            UprightColumn framePostChannel = FramePostUprightColumnAssembly.UprightColumnChannel;
            float offset = framePostChannel.ChannelWebThickness.Value;

            if (FramePostUprightColumnAssembly.HasDoubler.Value)
            {
                float basePlateThickness = FramePostUprightColumnAssembly.Footplate.Thickness.Value;

                if (yOffset < (basePlateThickness + FramePostUprightColumnAssembly.DoublerHeight.Value))
                    offset = (framePostChannel.ChannelDepth.Value * 2);
            }

            return (RearFrameDepthDisplay.Value - offset);
        }

        private string GetFramePostTieType(bool attachesToPostDoubler, int index)
        {
            if (FrameTieType.Value == "rrx")
            {
                if (RubRailQty.Value >= (index + 1))
                {
                    return (attachesToPostDoubler ? "rrx" : "rrxI");
                }
                else
                {
                    return "rfsw";
                }
            }
            else if (FrameTieType.Value == "rrxd")
            {
                if (RubRailQty.Value >= (index + 1))
                {
                    return (attachesToPostDoubler ? "rrxd" : "rrxID");
                }
                else
                {
                    return "rfsw";
                }
            }
            else
            {
                return FrameTieType.Value;
            }
        }

        #endregion

        #region Children

        public FrameUprightKit FrontFrame { get; set; }
        protected virtual void ConfigureFrontFrame(FrameUprightKit child)
        {
            child.FrameUprightKitMaster.Define(() => DoubleDeepFrameTypeMaster.Value?.FrontFrame ?? default(FrameUprightKit));
            child.IsFrontFrame.Define(true);
            child.DefaultDepth.Define(FrontFrameDepth);
            child.Height.Define(Height);
            child.UprightColumnSize.Define(UprightColumnSize);
            child.FrontAnchorQty.Define(DefaultAnchorQtyPerFootplate);
            child.RearAnchorQty.Define(DefaultAnchorQtyPerFootplate);
            child.FrontAnchorSize.Define(DefaultAnchorSizePerFootplate);
            child.RearAnchorSize.Define(DefaultAnchorSizePerFootplate);
            child.DefaultAnchorTypePerFootplate.Define(DefaultAnchorTypePerFootplate);
            child.UprightType.Define(UprightType);
            child.SlantElev.Define(SlantElev);
            child.SlantInset.Define(SlantInset);
            child.FootplateWidth.Define(FootplateWidth);
            child.FootplateThickness.Define(FootplateThickness);
        }

        public FrameUprightKit RearFrame { get; set; }
        protected virtual void ConfigureRearFrame(FrameUprightKit child)
        {
            child.IsActive.Define(() => !IsDoubleDeepPost.Value);
            child.FrameUprightKitMaster.Define(() => DoubleDeepFrameTypeMaster.Value?.RearFrame ?? default(FrameUprightKit));
            child.DefaultDepth.Define(RearFrameDepth);
            child.Height.Define(Height);
            child.UprightColumnSize.Define(UprightColumnSize);
            child.FrontAnchorQty.Define(DefaultAnchorQtyPerFootplate);
            child.RearAnchorQty.Define(DefaultAnchorQtyPerFootplate);
            child.FrontAnchorSize.Define(DefaultAnchorSizePerFootplate);
            child.RearAnchorSize.Define(DefaultAnchorSizePerFootplate);
            child.DefaultAnchorTypePerFootplate.Define(DefaultAnchorTypePerFootplate);
            child.UprightType.Define(UprightType);
            child.FootplateWidth.Define(FootplateWidth);
            child.FootplateThickness.Define(FootplateThickness);
            child.Position.Define(() => Helper.Vx(-(FrontFrame.Frame.Depth.Value + FrameTieLength.Value)));
        }

        public ChildList<FrameTie> FrameFrameTie { get; set; }
        protected virtual void ConfigureFrameFrameTie(ChildList<FrameTie> list)
        {
            list.Qty.Define(() => (HasFrameTies.Value && !IsDoubleDeepPost.Value ? FrameTieQty.Value : 0));
            list.ConfigureChildren((child, index) =>
            {
                child.FrameTieMaster.Define(() => MasterSupplied.Value ? DoubleDeepFrameTypeMaster.Value.FrameFrameTie[index] : default(FrameTie));
                child.TypeDefault.Define(FrameTieType);
                child.ColumnChannelSize.Define(UprightColumnSize);
                child.ChannelSizeDefault.Define(FrameTieBeamSize);
                child.Length.Define(FrameTieLength);
                child.AngleTypeDefault.Define(FrameTieAngleType);
                child.HorizBraceChoices.Define(FrameHorizBraces);
                child.AttachmentLocationDefault.Define(() => FrameFrameTieLocations.Value[index]);
                //child.Rotation.Define(() => index == 0 ? Helper.CreateRotation(Vector3.UnitY, 90) : Helper.CreateRotation(Vector3.UnitY, 0));
                child.Position.Define(() => Helper.Vxy(-FrontFrame.Frame.Depth.Value, GetFrameTieVertOffset(child.AttachmentLocation.Value)));
                child.FrontDoubleratConnection.Define(() => FrontFrame.Frame.RearDoublerHeight.Value >= child.Position.Value.Y);
                child.RearDoubleratConnection.Define(() => RearFrame.Frame.FrontDoublerHeight.Value >= child.Position.Value.Y);
            });
        }

        public UprightColumnAssembly FramePostUprightColumnAssembly { get; set; }
        protected virtual void ConfigureFramePostUprightColumnAssembly(UprightColumnAssembly child)
        {
            child.IsActive.Define(IsDoubleDeepPost);
            child.ColumnAssyMaster.Define(() => MasterSupplied.Value ? DoubleDeepFrameTypeMaster.Value.FramePostUprightColumnAssembly : default(UprightColumnAssembly));
            child.IsFramePostColumn.Define(true);
            child.UprightColumnSize.Define(UprightColumnSize);
            child.ColumnHeight.Define(() => FrontFrame.Frame.RearColumnHeight.Value);
            child.HasDoublerDefault.Define(HasRearDoubler);
            child.DoublerHeightDefault.Define(RearDoublerHeight);
            child.AnchorQty.Define(() => FrontFrame.Frame.RearAnchorQty.Value);
            child.AnchorSize.Define(DefaultAnchorSizePerFootplate);
            child.AnchorType.Define(() => FrontFrame.Frame.RearAnchorType.Value);
            child.BullNoseAnchorType.Define(() => FrontFrame.Frame.RearBullnoseAnchorType.Value);
            child.FootPlateTypeDefault.Define(RearFootPlateType);
            child.UprightType.Define(() => FrontFrame.Frame.UprightType.Value);
            child.Location.Define(Location);
            child.FootplateWidth.Define(() => FrontFrame.Frame.FootplateWidth.Value);
            child.FootplateThickness.Define(() => FrontFrame.Frame.FootplateThickness.Value);
            child.Position.Define(() => Helper.Vx(-(FrontFrame.Frame.Depth.Value + RearFrameDepthDisplay.Value)));
        }

        public ChildList<FrameTie> FramePostTie { get; set; }
        protected virtual void ConfigureFramePostTie(ChildList<FrameTie> list)
        {
            list.Qty.Define(() => (HasFrameTies.Value && IsDoubleDeepPost.Value ? HorizBraceDict.Value.Count() : 0));
            list.ConfigureChildren((child, index) =>
            {
                child.FrameTieMaster.Define(() => MasterSupplied.Value ? DoubleDeepFrameTypeMaster.Value.FramePostTie[index] : default(FrameTie));
                child.TypeDefault.Define(() => GetFramePostTieType(child.RearDoubleratConnection.Value, index));
                child.ColumnChannelSize.Define(UprightColumnSize);
                child.ChannelSizeDefault.Define(FrameTieBeamSize);
                child.Length.Define(() => GetFramePostTieLength(child.Position.Value.Y));
                child.AngleTypeDefault.Define(FrameTieAngleType);
                child.FrontDoubleratConnection.Define(() => FrontFrame.Frame.HasRearDoubler.Value && FrontFrame.Frame.RearDoublerHeight.Value >= child.Position.Value.Y);
                child.RearDoubleratConnection.Define(() => FramePostUprightColumnAssembly.HasDoubler.Value && FramePostUprightColumnAssembly.DoublerHeight.Value >= child.Position.Value.Y);
                child.Position.Define(() => Helper.Vxy(-FrontFrame.Frame.Depth.Value, HorizBraceDict.Value.ElementAt(index).Value));
            });
        }

        #endregion

    }
}
