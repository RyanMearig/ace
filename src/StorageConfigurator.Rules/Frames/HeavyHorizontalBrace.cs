﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;
using Ruler.Rules;
using Ruler.Cpq;
using Ruler.Rules.Validation;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class HeavyHorizontalBrace : Channel, Ruler.Cpq.IBomPart
    {
        #region Private Fields

        public AdvanceCSIItemService CsiPartService { get; }

        #endregion

        #region Constructors

        public HeavyHorizontalBrace(ModelContext modelContext, StorageConfiguratorRepository repo, AdvanceCSIItemService csiPartService) : base(modelContext, repo)
        {
            CsiPartService = csiPartService;
        }

        public HeavyHorizontalBrace(string name, Part parent, StorageConfiguratorRepository repo, AdvanceCSIItemService csiPartService) : base(name, parent, repo)
        {
            CsiPartService = csiPartService;
        }

        #endregion

        #region Rules

        public Rule<HeavyHorizontalBrace> HeavyHorizontalBraceMaster { get; set; }
        protected virtual void ConfigureHeavyHorizontalBraceMaster(Rule<HeavyHorizontalBrace> rule)
        {
            rule.Define(default(HeavyHorizontalBrace));
        }

        public Rule<int> Id { get; set; }
        protected virtual void ConfigureId(Rule<int> rule)
        {
            rule.Define(default(int));
        }

        [UIRule]
        public Rule<float> PanelHeight { get; set; }
        protected virtual void ConfigurePanelHeight(Rule<float> rule)
        {
            rule.Define(() => HeavyHorizontalBraceMaster.Value?.PanelHeight.Value ?? PanelHeightDefault.Value);
            rule.Min(12);
            rule.Max(60);
        }

        public Rule<float> PanelHeightDefault { get; set; }
        protected virtual void ConfigurePanelHeightDefault(Rule<float> rule)
        {
            rule.Define(0);
        }

        [UIRule]
        public ReadOnlyRule<float> OffsetFromBottom { get; set; }
        protected virtual void ConfigureOffsetFromBottom(ReadOnlyRule<float> rule)
        {
            rule.Define(default(float));
        }

        public Rule<bool> CutForRearDoubler { get; set; }
        protected virtual void ConfigureCutForRearDoubler(Rule<bool> rule)
        {
            rule.Define(false);
        }

        public Rule<bool> CutForFrontDoubler { get; set; }
        protected virtual void ConfigureCutForFrontDoubler(Rule<bool> rule)
        {
            rule.Define(false);
        }

        [CadRule(CadRuleUsageType.Parameter)]
        public Rule<float> RearMiterAngle { get; set; }
        protected virtual void ConfigureRearMiterAngle(Rule<float> rule)
        {
            rule.Define(0);
        }

        public Rule<string> UprightType { get; set; }
        protected virtual void ConfigureUprightType(Rule<string> rule)
        {
            rule.Define(default(string));
        }

        public Rule<string> DoubleHeavyBasePartNumber { get; set; }
        protected virtual void ConfigureDoubleHeavyBasePartNumber(Rule<string> rule)
        {
            rule.Define(default(string));
        }

        public Rule<string> NonDoubleHeavyBasePartNumber { get; set; }
        protected virtual void ConfigureNonDoubleHeavyBasePartNumber(Rule<string> rule)
        {
            rule.Define(default(string));
        }

        public Rule<string> BasePartNumberSuffix { get; set; }
        protected virtual void ConfigureBasePartNumberSuffix(Rule<string> rule)
        {
            rule.Define(default(string));
        }

        public Rule<int> HoleQty { get; set; }
        protected virtual void ConfigureHoleQty(Rule<int> rule)
        {
            rule.Define(() =>
            {
                if (HasFrontHole.Value && HasRearHole.Value)
                {
                    return 2;
                }
                else if (HasFrontHole.Value || HasRearHole.Value)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            });
        }

        public Rule<bool> HasFrontHole { get; set; }
        protected virtual void ConfigureHasFrontHole(Rule<bool> rule)
        {
            rule.Define(false);
        }

        public Rule<bool> HasRearHole { get; set; }
        protected virtual void ConfigureHasRearHole(Rule<bool> rule)
        {
            rule.Define(false);
        }

        protected virtual void ConfigureChannelSize(Rule<string> rule)
        {
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(true);
            });
        }

        #endregion

        #region Private Methods


        #endregion

        #region IBom Implementation

        protected virtual void ConfigurePartNumberPrefix(Rule<string> rule)
        {
            rule.Define(() =>
            {
                var baseVal = UprightType.Value == "D" ? DoubleHeavyBasePartNumber.Value : NonDoubleHeavyBasePartNumber.Value;

                return $"{baseVal}{BasePartNumberSuffix.Value}-";
            });
        }

        #endregion

        #region CPQ.IBomPart Implementation

        protected override void ConfigureDescriptionPrefix(Rule<string> rule)
        {
            rule.Define(() => $"Hvy Horiz {PartNumberPrefix.Value.Replace("-", "")}");
        }

        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {
            child.PartNumber.Define(PartNumber);
            child.Description.Define(BOMUIDescription);
            child.MaterialPartNumber.Define(MaterialPartNumber);
            child.MaterialQty.Define(() => Length.Value / 12);
            child.ItemMaterialCost.Define(() => child.ItemRawMaterialWeight.Value * child.MaterialUnitCost.Value);
        }

        #endregion

        #region Labor Rules

        public Rule<double> BundleQty { get; set; }
        protected virtual void ConfigureBundleQty(Rule<double> rule)
        {
            rule.Define(() => CsiPartService.GetItemBundle(MaterialPartNumber.Value, Bom.PlantCode.Value));
        }

        public Rule<double> PartCarriersPerHour { get; set; }
        protected virtual void ConfigurePartCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(false);
        }

        #endregion

        #region Graphics


        #endregion


    }
}
