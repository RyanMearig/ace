﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using Ruler.Rules;
using Ruler.Cpq;
using Ruler.Rules.Graphics;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class UprightColumnAssembly : PaintedPart, IRevisablePart, IFrameUprightData, Ruler.Cpq.IBomPart
    {
        #region Constructors

        public UprightColumnAssembly(ModelContext modelContext, StorageConfiguratorRepository repo) : base(modelContext)
        {
            Repository = repo;
        }

        public UprightColumnAssembly(string name, Part parent, StorageConfiguratorRepository repo) : base(name, parent)
        {
            Repository = repo;
        }

        #endregion

        #region Rules

        public Rule<UprightColumnAssembly> ColumnAssyMaster { get; set; }
        protected virtual void ConfigureColumnAssyMaster(Rule<UprightColumnAssembly> rule)
        {
            rule.Define(default(UprightColumnAssembly));
        }

        public Rule<bool> MasterSupplied { get; set; }
        protected virtual void ConfigureMasterSupplied(Rule<bool> rule)
        {
            rule.Define(() => ColumnAssyMaster.Value != default(UprightColumnAssembly));
        }

        public Rule<bool> IsStandalone { get; set; }
        protected virtual void ConfigureIsStandalone(Rule<bool> rule)
        {
            rule.Define(() => (Parent == null || Parent.GetType() == typeof(ShippableParts)));
        }

        public Rule<double> Qty { get; set; }
        protected virtual void ConfigureQty(Rule<double> rule)
        {
            rule.Define(1);
            rule.AddToUI(ui =>
            {
                ui.Label.Define("Quantity");
                ui.ReadOnly.Define(() => !IsStandalone.Value);
            });
        }

        public Rule<bool> IsFramePostColumn { get; set; }
        protected virtual void ConfigureIsFramePostColumn(Rule<bool> rule)
        {
            rule.Define(IsStandalone);
        }

        [CadRule(CadRuleUsageType.Property, "IsFramePostColumn")]
        public Rule<string> IsFramePostColumnProp { get; set; }
        protected virtual void ConfigureIsFramePostColumnProp(Rule<string> rule)
        {
            rule.Define(() => IsFramePostColumn.Value.ToString());
        }

        public Rule<float> ColumnHeight { get; set; }
        protected virtual void ConfigureColumnHeight(Rule<float> rule)
        {
            rule.Define(() => ColumnAssyMaster.Value?.ColumnHeight.Value ?? 120);
            rule.Min(HeightMin);
            rule.Max(MaxHeight);
            rule.MultipleOf(4);
            rule.AddToCAD(cad =>
            {
                cad.CadRuleUsageType = CadRuleUsageType.Parameter;
                cad.NameInCad = "Height";
            });
            rule.AddToUI();
        }

        public Rule<float> HeightMin { get; set; }
        protected virtual void ConfigureHeightMin(Rule<float> rule)
        {
            rule.Define(() => UprightType.Value == "B" ? 80 : 12);
        }

        public Rule<bool> HasDoubler { get; set; }
        protected virtual void ConfigureHasDoubler(Rule<bool> rule)
        {
            rule.Define(() => ColumnAssyMaster.Value?.HasDoubler.Value ?? HasDoublerDefault.Value);
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => HasFrontDoubler.UIMetadata.ReadOnly.Value);
            });
        }

        public Rule<bool> HasDoublerDefault { get; set; }
        protected virtual void ConfigureHasDoublerDefault(Rule<bool> rule)
        {
            rule.Define(HasFrontDoubler);
        }

        [CadRule(CadRuleUsageType.Property, "HasDoubler")]
        public Rule<string> HasDoublerProp { get; set; }
        protected virtual void ConfigureHasDoublerProp(Rule<string> rule)
        {
            rule.Define(() => HasDoubler.Value.ToString());
        }

        public Rule<float> DoublerHeight { get; set; }
        protected virtual void ConfigureDoublerHeight(Rule<float> rule)
        {
            rule.Define(() => ColumnAssyMaster.Value?.DoublerHeight.Value ?? DoublerHeightDefault.Value);
            rule.Min(FrontDoublerHeightMin);
            rule.MultipleOf(4);
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !HasDoubler.Value);
            });
            rule.AddToCAD(cad => cad.CadRuleUsageType = CadRuleUsageType.Property);
        }

        public Rule<float> DoublerHeightDefault { get; set; }
        protected virtual void ConfigureDoublerHeightDefault(Rule<float> rule)
        {
            rule.Define(() => HasDoubler.Value ? ColumnHeight.Value/2 : 0);
        }

        public Rule<string> FootPlateType { get; set; }
        protected virtual void ConfigureFootPlateType(Rule<string> rule)
        {
            rule.Define(() => ColumnAssyMaster.Value?.FootPlateType.Value ?? FootPlateTypeDefault.Value);
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() => BasePlateTypeChoiceList.Value);
            });
        }

        public Rule<string> FootPlateTypeDefault { get; set; }
        protected virtual void ConfigureFootPlateTypeDefault(Rule<string> rule)
        {
            rule.Define(() =>
            {
                BasePlateTypeData bpData = AllBasePlateData.Value.FirstOrDefault(d =>
                {
                    return (d.HasDoubler == HasDoubler.Value &&
                        d.PostProtector == PostProtectorType.Value &&
                        d.AnchorQty == AnchorQty.Value);
                });

                if (bpData != null)
                {
                    return bpData.BasePlateType;
                }
                return BasePlateTypeChoiceList.Value.First().Text;
            });
        }

        public Rule<int> AnchorQty { get; set; }
        protected virtual void ConfigureAnchorQty(Rule<int> rule)
        {
            rule.Define(2);
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() => FrontAnchorQty.UIMetadata.ChoiceList.Value);
            });
        }

        public Rule<string> AnchorSize { get; set; }
        protected virtual void ConfigureAnchorSize(Rule<string> rule)
        {
            rule.Define(() => rule.UIMetadata.ChoiceList.Value.First().Value.ToString());
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() => FrontAnchorSize.UIMetadata.ChoiceList.Value);
            });
        }

        public Rule<string> AnchorType { get; set; }
        protected virtual void ConfigureAnchorType(Rule<string> rule)
        {
            rule.Define(() => rule.UIMetadata.ChoiceList.Value.First().Value.ToString());
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() => this.GetAnchorChoiceList(float.Parse(AnchorSize.Value)));
            });
        }

        public Rule<AnchorData> ActiveAnchorData { get; set; }
        protected virtual void ConfigureActiveAnchorData(Rule<AnchorData> rule)
        {
            rule.Define(() => (AllAnchorData.Value.FirstOrDefault(d => d.Type == AnchorType.Value)));
        }

        public Rule<string> BullNoseAnchorType { get; set; }
        protected virtual void ConfigureBullNoseAnchorType(Rule<string> rule)
        {
            rule.Define(() => PostProtectorType.Value.ToLower().Contains("bull") ? AnchorType.Value : "");
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() => AllAnchorTypesChoiceList.Value);
                ui.ReadOnly.Define(() => !PostProtectorType.Value.ToLower().Contains("bull"));
            });
        }

        public Rule<AnchorData> ActiveBullnoseAnchorData { get; set; }
        protected virtual void ConfigureActiveBullnoseAnchorData(Rule<AnchorData> rule)
        {
            rule.Define(() => (AllAnchorData.Value.FirstOrDefault(d => d.Type == BullNoseAnchorType.Value)));
        }

        public Rule<string> PostProtectorType { get; set; }
        protected virtual void ConfigurePostProtectorType(Rule<string> rule)
        {
            rule.Define(() => ColumnAssyMaster.Value?.PostProtectorType.Value ?? "none");
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() => PostProtectorChoices.Value);
                ui.ReadOnly.Define(() => FrontPostProtector.UIMetadata.ReadOnly.Value);
            });
            rule.ValueSet += (sender, e) =>
            {
                PostProtectorHeight.Reset();
            };
            rule.ValueReset += (sender, e) =>
            {
                PostProtectorHeight.Reset();
            };
        }

        public Rule<float> PostProtectorHeight { get; set; }
        protected virtual void ConfigurePostProtectorHeight(Rule<float> rule)
        {
            rule.Define(() => PostProtectorType.Value == "bullnose" ? 3.75f : 12);
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => PostProtectorType.Value == "none");
                ui.ChoiceList.Define(() =>
                {
                    if (PostProtectorType.Value == "bullnose")
                    {
                        return new List<ChoiceListItem>()
                            {
                                new ChoiceListItem(2.75, "3"),
                                new ChoiceListItem(3.75, "4"),
                                new ChoiceListItem(4.75, "5"),
                                new ChoiceListItem(5.75, "6"),
                                new ChoiceListItem(7.75, "8"),
                            };
                    }
                    else
                    {
                        return new List<ChoiceListItem>();
                    }
                });
            });
        }

        public Rule<bool> IsFrontUprightColumn { get; set; }
        protected virtual void ConfigureIsFrontUprightColumn(Rule<bool> rule)
        {
            rule.Define(false);
        }
        public Rule<bool> IsColumnToSlant { get; set; }
        protected virtual void ConfigureIsColumnToSlant(Rule<bool> rule)
        {
            rule.Define(false);
        }

        public Rule<bool> IsSlantType { get; set; }
        protected virtual void ConfigureIsSlantType(Rule<bool> rule)
        {
            rule.Define(() => (UprightType.Value == "L" || UprightType.Value == "LS"));
        }

        public Rule<bool> IsBentType { get; set; }
        protected virtual void ConfigureIsBentType(Rule<bool> rule)
        {
            rule.Define(() => UprightType.Value == "B");
        }

        public Rule<float> ColumnOffsetonFootPlate { get; set; }
        protected virtual void ConfigureColumnOffsetonFootPlate(Rule<float> rule)
        {
            rule.Define(() =>
            {
                switch (FootPlateType.Value)
                {
                    case "FP1K-A":
                    case "FP1K-B":
                    case "FP3-A":
                    case "FP3-B":
                        return 0.25f;
                    case "FP1K-C":
                    case "FP1K-D":
                    case "FP3-C":
                    case "FP3-D":
                    case "FP5-C":
                    case "FP5-D":
                        if (UprightColumnChannel.ChannelWidth.Value <= 3)
                            return 1.4971f;
                        else
                            return 1.8486f;
                    case "FP2K-A":
                    case "FP2K-B":
                        return 2.875f;
                    case "FP4-A":
                    case "FP4-B":
                        if (UprightColumnChannel.ChannelWidth.Value <= 3)
                            return 2.625f;
                        else
                            return 3.125f;
                    case "FP5-A":
                        return Footplate.BasePlateLength.Value / 2 - UprightColumnChannel.ChannelDepth.Value / 2;
                    case "FP5-B":
                        return Footplate.BasePlateLength.Value / 2;
                    case "FP6-A":
                    case "FP6-B":
                        return 2.875f;
                    default:
                        return 0;
                }
            });
        }

        [CadRule(CadRuleUsageType.Parameter, "Width")]
        public Rule<float> OverallWidth { get; set; }
        protected virtual void ConfigureOverallWidth(Rule<float> rule)
        {
            rule.Define(() => UprightColumnChannel.ChannelWidth.Value);
        }

        public Rule<float> AnglePostProtectorHypotenuse { get; set; }
        protected virtual void ConfigureAnglePostProtectorHypotenuse(Rule<float> rule)
        {
            rule.Define(() =>
            {
                //use Pythagorean Theorem
                float a = (AnglePostProtector.AngleWidth.Value);
                float b = (AnglePostProtector.AngleHeight.Value);
                return (float)Math.Sqrt((Math.Pow(a, 2) + Math.Pow(b, 2)));
            });
        }

        public Rule<float> AnglePostProtectorXOffset { get; set; }
        protected virtual void ConfigureAnglePostProtectorXOffset(Rule<float> rule)
        {
            rule.Define(() =>
            {
                //use Pythagorean Theorem
                float b = (AnglePostProtectorHypotenuse.Value / 2);
                float c = (AnglePostProtector.AngleHeight.Value);
                return (float)Math.Sqrt((Math.Pow(c, 2) - Math.Pow(b, 2)));
            });
        }

        public Rule<string> AnglePostProtectorAngleSize { get; set; }
        protected virtual void ConfigureAnglePostProtectorAngleSize(Rule<string> rule)
        {
            rule.Define(() =>
            {
                if (UprightColumnSize.Value.ToUpper().Contains("C3"))
                    return @"2 x 2 x 1/4";
                else if (UprightColumnSize.Value.ToUpper().Contains("C4"))
                    return @"2-1/2 x 2-1/2 x 1/4";
                else
                    return @"3 x 3 x 1/4";
            });
        }

        public Rule<float> BullnoseOverlap { get; set; }
        protected virtual void ConfigureBullnoseOverlap(Rule<float> rule)
        {
            rule.Define(0.375f);
        }

        public Rule<bool> HasHeavyBtmHorizontal { get; set; }
        protected virtual void ConfigureHasHeavyBtmHorizontal(Rule<bool> rule)
        {
            rule.Define(false);
        }

        public Rule<float> HeavyHorizontalAngleYOffset { get; set; }
        protected virtual void ConfigureHeavyHorizontalAngleYOffset(Rule<float> rule)
        {
            rule.Define(6);
        }

        public Rule<float> BentTangentLineElev { get; set; }
        protected virtual void ConfigureBentTangentLineElev(Rule<float> rule)
        {
            rule.Define(() => SlantElev.Value - 9.5f);
        }

        public Rule<float> ColumnChannelVertOffset { get; set; }
        protected virtual void ConfigureColumnChannelVertOffset(Rule<float> rule)
        {
            rule.Define(() =>
            {
                float retVal = Footplate.Thickness.Value;

                if (IsSlantType.Value && IsColumnToSlant.Value)
                    retVal = retVal + SlantElev.Value;

                return retVal;
            });
        }

        public Rule<float> ColumnChannelWidth { get; set; }
        protected virtual void ConfigureColumnChannelWidth(Rule<float> rule)
        {
            rule.Define(() => UprightColumnChannel.ChannelWidth.Value);
        }

        public Rule<float> HoleSpacing { get; set; }
        protected virtual void ConfigureHoleSpacing(Rule<float> rule)
        {
            rule.Define(4);
        }
        
        public Rule<string> rulerDrawingXml { get; set; }
        protected virtual void ConfigurerulerDrawingXml(Rule<string> rule)
        {
            rule.Define("Post");
            rule.AddToCAD(cad =>
            {
                cad.CadRuleUsageType = CadRuleUsageType.Property;
                cad.IsActive.Define(IsFramePostColumn);
            });
        }

        public Rule<List<AnchorDefinition>> AnchorDefinitions { get; set; }
        protected virtual void ConfigureAnchorDefinitions(Rule<List<AnchorDefinition>> rule)
        {
            rule.Define(() =>
            {
                var anchorDefs = new List<AnchorDefinition>();

                anchorDefs.Add(new AnchorDefinition()
                {
                    AnchorData = ActiveAnchorData.Value,
                    Qty = AnchorQty.Value
                });

                if (PostProtectorType.Value == "bullnose")
                {
                    anchorDefs.Add(new AnchorDefinition()
                    {
                        AnchorData = ActiveBullnoseAnchorData.Value,
                        Qty = 1
                    });
                }

                return anchorDefs;
            });
        }

        #endregion

        #region IFrameUprightData Implementation

        public Rule<string> UprightType { get; set; }
        public Rule<bool> Slanted { get; set; }
        public Rule<float> SlantElev { get; set; }
        public Rule<float> SlantInset { get; set; }
        public Rule<float> SlantInsetMax { get; set; }
        public Rule<float> BendToFirstHorizOffset { get; set; }
        public Rule<string> UprightColumnSize { get; set; }

        public Rule<float> FootplateWidth { get; set; }
        protected virtual void ConfigureFootplateWidth(Rule<float> rule)
        {
            rule.Define(() => ColumnAssyMaster.Value?.FootplateWidth.Value ?? (UprightType.Value == "S" ? ColumnChannelWidth.Value + 1 : ColumnChannelWidth.Value + 2));
        }

        public Rule<float> FootplateThickness { get; set; }
        protected virtual void ConfigureFootplateThickness(Rule<float> rule)
        {
            rule.Define(() => ColumnAssyMaster.Value?.FootplateThickness.Value ?? 0.375f);
        }

        public Rule<float> HoleStartPunch { get; set; }

        public Rule<string> FrontPostProtector { get; set; }
        public Rule<float> FrontPostProtectorHeight { get; set; }
        public Rule<bool> HasFrontDoubler { get; set; }
        public Rule<float> FrontDoublerHeight { get; set; }
        public Rule<float> FrontDoublerHeightMin { get; set; }
        public Rule<string> FrontFootPlateType { get; set; }

        public Rule<bool> HasRearDoubler { get; set; }
        public Rule<float> RearDoublerHeight { get; set; }
        public Rule<float> RearDoublerHeightMin { get; set; }
        public Rule<string> RearPostProtector { get; set; }
        public Rule<float> RearPostProtectorHeight { get; set; }
        public Rule<string> RearFootPlateType { get; set; }
        public Rule<int> FrontAnchorQty { get; set; }
        public Rule<int> RearAnchorQty { get; set; }
        public Rule<string> FrontAnchorSize { get; set; }
        public Rule<string> RearAnchorSize { get; set; }
        public Rule<string> FrontAnchorType { get; set; }
        public Rule<string> FrontBullnoseAnchorType { get; set; }
        public Rule<string> RearAnchorType { get; set; }
        public Rule<string> RearBullnoseAnchorType { get; set; }
        public Rule<List<AnchorData>> AllAnchorData { get; set; }
        public Rule<List<ChoiceListItem>> AllAnchorTypesChoiceList { get; set; }
        public Rule<float> MaxHeight { get; set; }
        public Rule<string> Location { get; set; }
        protected virtual void ConfigureLocation(Rule<string> rule)
        {
            rule.ValueSet += (sender, e) =>
            {
                if (ColumnHeight.Value > MaxHeight.Value)
                {
                    ColumnHeight.Reset();
                }
            };
        }

        #endregion

        #region IBasePlateData Implementation

        public Rule<List<BasePlateTypeData>> AllBasePlateData { get; set; }
        public Rule<List<ChoiceListItem>> BasePlateTypeChoiceList { get; set; }
        public Rule<List<ChoiceListItem>> PostProtectorChoices { get; set; }

        #endregion

        #region IBomPart Implementation

        protected virtual void ConfigurePartNumberPrefix(Rule<string> rule)
        {
            rule.Define(() =>
            {
                var retVal = $"POST{UprightColumnChannel.ChannelId.Value}";

                if (HasDoubler.Value)
                {
                    retVal = retVal + "D";
                }

                return $"{retVal}-";
            });
        }

        protected virtual void ConfigureDescription(Rule<string> rule)
        {
            rule.Define(() => $"Post {UprightColumnChannel.ChannelId.Value}" + (HasDoubler.Value ? "D" : ""));
        }

        protected virtual void ConfigureBOMDescription(Rule<string> rule)
        {
            rule.Define(() =>
            {
                var doublerHeightVal = HasDoubler.Value ? $",{DoublerHeight.Value}" : "";
                var retVal = $"{Description.Value} ({ColumnHeight.Value}{doublerHeightVal})";

                retVal = retVal + $@"|Upright Post {UprightColumnSize.Value}# x {ColumnHeight.Value}"" long";

                if (HasDoubler.Value)
                {
                    retVal = retVal + $@", dbl {DoublerHeight.Value}""";
                }

                retVal = retVal + $"|Footplate " +
                                $"{Helper.fractionString(Footplate.Thickness.Value, feetToo: false)} " +
                                $"x {Helper.fractionString(Footplate.Width.Value, feetToo: false)} " +
                                $"x {Helper.fractionString(Footplate.BasePlateLength.Value, feetToo: false)}, " +
                                Footplate.Type.Value;
                retVal = retVal + $"|FP Hole A {Helper.fractionString(Footplate.HoleDiameter.Value, feetToo: false)}, " +
                                    Color.Value;
                retVal = retVal + $"|Upright punched on {HoleSpacing.Value:#.##}\" centers";

                return retVal;
            });
        }

        protected virtual void ConfigurePartWeight(Rule<double> rule)
        {
            rule.Define(() => IsFramePostColumn.Value ? this.GetPartWeightWithoutPaint() : 0);
        }
        
        #endregion

        #region IRevisablePartImplementation

        public FactoryRevisionRule<string> FactoryRevision { get; set; }

        #endregion

        #region CPQ.IBomPart Implementation

        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {
            child.IsActive.Define(IsFramePostColumn);
            child.PartNumber.Define(PartNumber);
            child.ItemQty.Define(Qty);
            child.Description.Define(BOMDescription1);
        }

        #endregion

        #region Labor Rules

        public Rule<double> WeldBaseLabor { get; set; }
        protected virtual void ConfigureWeldBaseLabor(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> PostHeightWeld { get; set; }
        protected virtual void ConfigurePostHeightWeld(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> PostFootPlateWeld { get; set; }
        protected virtual void ConfigurePostFootPlateWeld(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> DoublerWeldAddon { get; set; }
        protected virtual void ConfigureDoublerWeldAddon(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> EDH { get; set; }
        protected virtual void ConfigureEDH(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> DoublerCapWeldAddon { get; set; }
        protected virtual void ConfigureDoublerCapWeldAddon(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> AngleProtectorHeightFactor { get; set; }
        protected virtual void ConfigureAngleProtectorHeightFactor(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> AngleProtectorWeldAddon { get; set; }
        protected virtual void ConfigureAngleProtectorWeldAddon(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> BullnoseWeldAddon { get; set; }
        protected virtual void ConfigureBullnoseWeldAddon(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public double DoublerEDH(double doublerLength)
        {
            double lim1 = 108;
            double lim2 = 216;
            double lim3 = 324;

            double returnVal = doublerLength;

            switch (doublerLength)
            {
                case double n when (n > lim3):
                    returnVal += (doublerLength - lim3) + (doublerLength - lim2) + (doublerLength - lim1);
                    break;
                case double n when (n > lim2):
                    returnVal += (doublerLength - lim2) + (doublerLength - lim1);
                    break;
                case double n when (n > lim1):
                    returnVal += (doublerLength - lim1);
                    break;
            }

            return returnVal;
        }

        public Rule<double> PaintCarriersPerHour { get; set; }
        protected virtual void ConfigurePaintCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> PartsPerCarrier { get; set; }
        protected virtual void ConfigurePartsPerCarrier(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> PartsPerCarrierFactor { get; set; }
        protected virtual void ConfigurePartsPerCarrierFactor(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> UnloadCarriersPerHour { get; set; }
        protected virtual void ConfigureUnloadCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> Weight { get; set; }
        protected virtual void ConfigureWeight(Rule<double> rule)
        {
            rule.Define(() => Bom.UnitWeight.Value);
        }

        #endregion

        #region Children

        public UprightColumn UprightColumnChannel { get; set; }
        protected virtual void ConfigureUprightColumnChannel(UprightColumn child)
        {
            child.IsBentLegColumn.Define(() => IsBentType.Value && IsColumnToSlant.Value);
            child.IsFrontColumn.Define(IsFrontUprightColumn);
            child.IsDoubler.Define(false);
            child.ChannelSize.Define(UprightColumnSize);
            child.HoleSpacing.Define(HoleSpacing);
            child.Length.Define(() => (IsSlantType.Value && IsColumnToSlant.Value) ? (ColumnHeight.Value - SlantElev.Value) : ColumnHeight.Value);
            child.Elevation.Define(SlantElev);
            child.Inset.Define(SlantInset);
            child.IsNavigable.Define(false);
            child.Position.Define(() => Helper.Vy(ColumnChannelVertOffset.Value));
            child.Rotation.Define(() => Helper.CreateRotation(Vector3.UnitX, -90));
        }

        public UprightColumn Doubler { get; set; }
        protected virtual void ConfigureDoubler(UprightColumn child)
        {
            child.IsActive.Define(HasDoubler);
            child.IsFrontColumn.Define(IsFrontUprightColumn);
            child.IsDoubler.Define(true);
            child.ChannelSize.Define(UprightColumnSize);
            child.Length.Define(() => IsSlantType.Value && IsFrontUprightColumn.Value ? (DoublerHeight.Value - SlantElev.Value) : DoublerHeight.Value);
            child.IsBentLegColumn.Define(() => UprightColumnChannel.IsBentLegColumn.Value);
            child.IsReversed.Define(true);
            child.Elevation.Define(SlantElev);
            child.Inset.Define(SlantInset);
            child.IsNavigable.Define(false);
            child.Position.Define(() => Helper.Vxy(UprightColumnChannel.ChannelDepth.Value + child.ChannelDepth.Value, ColumnChannelVertOffset.Value));
            child.Rotation.Define(() => Quaternion.CreateFromYawPitchRoll(0, Helper.DegToRad(-90), Helper.DegToRad(180)));
        }

        public DoublerCap DoublerCap { get; set; }
        protected virtual void ConfigureDoublerCap(DoublerCap child)
        {
            child.IsActive.Define(HasDoubler);
            child.OutsideWidth.Define(() => Doubler.ChannelWidth.Value);
            child.Position.Define(() => Doubler.Position.Value + new Vector3(0, Doubler.Length.Value + child.SlantOffset.Value, 0));
            child.Rotation.Define(() => Quaternion.CreateFromAxisAngle(Vector3.UnitZ, Helper.DegToRad(180 - child.TiltAnlge.Value)));
        }


        public TubeCapPlate TubeCapPlate { get; set; }
        protected virtual void ConfigureTubeCapPlate(TubeCapPlate child)
        {
            child.IsActive.Define(() => IsSlantType.Value && IsColumnToSlant.Value);
            child.HasDoubler.Define(HasDoubler);
            child.ColumnWidth.Define(() => UprightColumnChannel.ChannelWidth.Value);
            child.Position.Define(() => Helper.Vxy((child.Width.Value/2)-0.25f, ColumnChannelVertOffset.Value));
        }

        public SlantLegColumnTube SlantLegColumnTube { get; set; }
        protected virtual void ConfigureSlantLegColumnTube(SlantLegColumnTube child)
        {
            child.IsActive.Define(() => IsSlantType.Value && IsColumnToSlant.Value);
            child.UprightType.Define(UprightType);
            child.Depth.Define(() => UprightColumnChannel.ChannelWidth.Value);
            child.Position.Define(() => Helper.Vy(ColumnChannelVertOffset.Value - TubeCapPlate.Thickness.Value));
            child.Rotation.Define(() => Helper.CreateRotation(Vector3.UnitZ, child.MiterAngle.Value));
            
        }

        public Footplate Footplate { get; set; }
        protected virtual void ConfigureFootplate(Footplate child)
        {
            child.BasePlateType.Define(FootPlateType);
            child.BeamSize.Define(() => UprightColumnChannel.ChannelSize.Value);
            child.BeamType.Define(UprightType);
            child.ChannelWidth.Define(() => UprightColumnChannel.ChannelWidth.Value);
            child.AnchorSize.Define(AnchorSize);
            child.Width.Define(FootplateWidth);
            child.Thickness.Define(FootplateThickness);
            child.IsFront.Define(IsFrontUprightColumn);
            child.Position.Define(() =>
            {
                float xOffset = -ColumnOffsetonFootPlate.Value;
                if (IsColumnToSlant.Value && (IsSlantType.Value || IsBentType.Value))
                    xOffset = xOffset + SlantInset.Value;

                return Helper.Vx(xOffset);
            });
            child.Rotation.Define(() => Helper.CreateRotation(Vector3.UnitY, -90));
        }

        public AnglePostProtector AnglePostProtector { get; set; }
        protected virtual void ConfigureAnglePostProtector(AnglePostProtector child)
        {
            child.IsActive.Define(() => (PostProtectorType.Value == "angle"));
            child.Length.Define(PostProtectorHeight);
            child.AngleType.Define(AnglePostProtectorAngleSize);
            child.Position.Define(() => Helper.Vxy(-AnglePostProtectorXOffset.Value, Footplate.Thickness.Value));
            child.Rotation.Define(() => Quaternion.CreateFromYawPitchRoll(0, Helper.DegToRad(-90), Helper.DegToRad(-45)));
        }

        public AngleProtectorDustCap AngleProtectorDustCap { get; set; }
        protected virtual void ConfigureAngleProtectorDustCap(AngleProtectorDustCap child)
        {
            child.IsActive.Define(() => AnglePostProtector.IsActive.Value);
            child.ColumnWidth.Define(() => UprightColumnChannel.ChannelWidth.Value);
            child.Position.Define(() => Helper.Vy(PostProtectorHeight.Value + Footplate.Thickness.Value));
            child.Rotation.Define(() => Helper.CreateRotation(Vector3.UnitY, 180));
        }

        public Bullnose BullnosePostProtector { get; set; }
        protected virtual void ConfigureBullnosePostProtector(Bullnose child)
        {
            child.IsActive.Define(() => (PostProtectorType.Value == "bullnose"));
            child.Height.Define(PostProtectorHeight);
            child.ColumnWidth.Define(() => UprightColumnChannel.ChannelWidth.Value);
            child.Position.Define(() => Helper.Vxy(BullnoseOverlap.Value, Footplate.Thickness.Value));
            child.Rotation.Define(() => Helper.CreateRotation(Vector3.UnitY, 180));
        }

        public BullnoseCoverPlate BullnoseCover { get; set; }
        protected virtual void ConfigureBullnoseCover(BullnoseCoverPlate child)
        {
            child.IsActive.Define(() => BullnosePostProtector.IsActive.Value);
            child.ColumnWidth.Define(() => UprightColumnChannel.ChannelWidth.Value);
            child.Position.Define(() => BullnosePostProtector.Position.Value + 
                                        Helper.Vxy(child.Depth.Value - BullnosePostProtector.Depth.Value, PostProtectorHeight.Value));
            child.Rotation.Define(() => BullnosePostProtector.Rotation.Value);
        }

        public BoltonHeavyHorizontalAngle BoltOnHvyHorizontalAngle { get; set; }
        protected virtual void ConfigureBoltOnHvyHorizontalAngle(BoltonHeavyHorizontalAngle child)
        {
            child.IsActive.Define(HasHeavyBtmHorizontal);
            //child.AttachmentChannelId.Define(() => UprightColumnChannel.ChannelId.Value);
            child.Length.Define(() => UprightColumnChannel.ChannelWidth.Value - 1);
            child.Position.Define(() =>
            {
                float xOffset = UprightColumnChannel.ChannelWebThickness.Value;

                if (HasDoubler.Value)
                    xOffset = UprightColumnChannel.ChannelDepth.Value + Doubler.ChannelDepth.Value;

                return Helper.Vxy(xOffset, HeavyHorizontalAngleYOffset.Value);
            });
        }

        //public UCS UCS { get; set; }
        //protected virtual void ConfigureUCS(UCS child)
        //{
        //    child.Diameter.Define(0.25f);
        //    child.Position.Define(() => (Helper.Vy(0)));
        //}

        //public UCS UCSt { get; set; }
        //protected virtual void ConfigureUCSt(UCS child)
        //{
        //    child.Diameter.Define(0.25f);
        //    child.Position.Define(() => (Helper.Vxy(12, 0)));
        //}

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(Colors.Lime);
        }

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define("UprightColumnAssembly");
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(true);
        }

        #endregion

        #region Protected Methods

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureBomPartRules();
            this.ConfigureAnchorDataRules();
            this.ConfigureRevisablePart();
            this.ConfigureFrameUprightDataRules();
        }

        #endregion

    }
}
