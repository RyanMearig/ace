﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;
using Ruler.Cpq;
using StorageConfigurator.Data;
using Ruler.Rules.Graphics;

namespace StorageConfigurator.Rules
{
    public class BullnoseCoverPlate : AdvancePart, IRevisablePart, IBomPart, Ruler.Cpq.IBomPart
    {
        #region Constructors

        public BullnoseCoverPlate(ModelContext modelContext) : base(modelContext) { }

        public BullnoseCoverPlate(string name, Part parent) : base(name, parent) { }

        #endregion

        #region Rules

        public Rule<float> ColumnWidth { get; set; }
        protected virtual void ConfigureColumnWidth(Rule<float> rule)
        {
            rule.Define(3);
        }

        [CadRule(CadRuleUsageType.Parameter)]
        public Rule<float> Width { get; set; }
        protected virtual void ConfigureWidth(Rule<float> rule)
        {
            rule.Define(() => ColumnWidth.Value + 0.375f);
        }

        [CadRule(CadRuleUsageType.Parameter)]
        public Rule<float> Depth { get; set; }
        protected virtual void ConfigureDepth(Rule<float> rule)
        {
            rule.Define(() => ColumnWidth.Value < 5 ? 2.875f : 3);
        }

        [CadRule(CadRuleUsageType.Parameter)]
        public Rule<float> FlangeDepth { get; set; }
        protected virtual void ConfigureFlangeDepth(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (ColumnWidth.Value <= 3)
                {
                    return 1.25f;
                }
                else if (ColumnWidth.Value < 5)
                {
                    return 0.75f;
                }
                return 1.375f;
            });
        }

        #endregion

        #region IRevisablePartImplementation

        public FactoryRevisionRule<string> FactoryRevision { get; set; }

        #endregion

        #region IBomPart Implementation

        public Rule<string> DatabasePartNumber { get; set; }
        public PartNumberRule<string> PartNumber { get; set; }
        public Rule<string> PartNumberPrefix { get; set; }
        public Rule<string> Description { get; set; }
        public Rule<string> BOMDescription { get; set; }
        public Rule<string> BOMDescription1 { get; set; }
        public Rule<string> BOMDescription2 { get; set; }
        public Rule<string> BOMDescription3 { get; set; }
        public Rule<string> BOMDescription4 { get; set; }
        public Rule<string> BOMDescription5 { get; set; }
        public Rule<string> BOMDescription6 { get; set; }
        public Rule<string> BOMDescription7 { get; set; }
        public Rule<string> BOMDescription8 { get; set; }
        public Rule<string> BOMDescription9 { get; set; }
        public Rule<string> BOMDescription10 { get; set; }
        public Rule<string> BOMDescription11 { get; set; }
        public Rule<string> Color { get; set; }
        public ReadOnlyRule<AdvanceColor> AdvanceColor { get; set; }
        public ReadOnlyRule<string> ColorDescription { get; set; }
        public StorageConfiguratorRepository Repository { get; set; }
        public Rule<string> ColorBOMName { get; set; }
        public ReadOnlyRule<string> ColorShortDescription { get; set; }

        public Rule<double> PartWeight { get; set; }
        public Rule<double> PartPaintWeight { get; set; }
        public Rule<float> WeightPerGallon { get; set; }
        public CsiRule<double> PaintCostPerGallon { get; set; }
        public Rule<float> PaintCost { get; set; }

        #endregion

        #region CPQ.IBomPart Implementation

        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {
            child.PartNumber.Define(PartNumber);
            child.Description.Define(() => $"C{ColumnWidth.Value} Bullnose Cover Plate");
            child.MaterialPartNumber.Define(MaterialPartNumber);
            child.MaterialQty.Define(() => Width.Value / 12);
        }

        #endregion

        #region Material Rules

        public Rule<string> MaterialPartNumber { get; set; }
        protected virtual void ConfigureMaterialPartNumber(Rule<string> rule)
        {
            rule.Define(default(string));
        }

        #endregion

        #region Labor Rules

        public Rule<double> PartCarriersPerHour { get; set; }
        protected virtual void ConfigurePartCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        #endregion

        #region Overrides

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(false);
        }

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define("BullnoseCoverPlate");
        }

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureRevisablePart();
            this.ConfigureBomPartRules();
        }

        #endregion

        #region Graphics

        //public Block Block { get; set; }
        //protected virtual void ConfigureBlock(Block geo)
        //{
        //    geo.Width.Define(Depth);
        //    geo.Height.Define(0.25f);
        //    geo.Depth.Define(Width);
        //    geo.Position.Define(() => Helper.Vxy(geo.Width.Value/2, geo.Height.Value / 2));
        //}

        //public UCS UCS { get; set; }
        //protected virtual void ConfigureUCS(UCS child)
        //{
        //    child.Diameter.Define(0.25f);
        //    child.Position.Define(() => (Helper.Vy(0)));
        //}

        #endregion

    }
}
