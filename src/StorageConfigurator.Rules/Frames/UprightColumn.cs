﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;
using Ruler.Cpq;
using Ruler.Rules;
using Ruler.Rules.Graphics;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class UprightColumn : Channel, Ruler.Cpq.IBomPart
    {
        #region Private Fields

        public AdvanceCSIItemService CsiPartService { get; }

        #endregion

        #region Constructors

        public UprightColumn(ModelContext modelContext, StorageConfiguratorRepository repo, AdvanceCSIItemService csiPartService) : base(modelContext, repo)
        {
            CsiPartService = csiPartService;
        }

        public UprightColumn(string name, Part parent, StorageConfiguratorRepository repo, AdvanceCSIItemService csiPartService) : base(name, parent, repo)
        {
            CsiPartService = csiPartService;
        }

        #endregion

        #region Rules

        public Rule<bool> IsBentLegColumn { get; set; }
        protected virtual void ConfigureIsBentLegColumn(Rule<bool> rule)
        {
            rule.Define(false);
        }

        //This will only be true for bent leg column doublers.
        public Rule<bool> IsReversed { get; set; }
        protected virtual void ConfigureIsReversed(Rule<bool> rule)
        {
            rule.Define(false);
        }

        public Rule<float> Elevation { get; set; }
        protected virtual void ConfigureElevation(Rule<float> rule)
        {
            rule.Define(0);
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(IsBentLegColumn);
                cad.CadRuleUsageType = CadRuleUsageType.Parameter;
            });
        }

        public Rule<float> BentTangentLineElev { get; set; }
        protected virtual void ConfigureBentTangentLineElev(Rule<float> rule)
        {
            rule.Define(() => Elevation.Value - 9.5f);
        }

        public Rule<float> Inset { get; set; }
        protected virtual void ConfigureInset(Rule<float> rule)
        {
            rule.Define(0);
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(IsBentLegColumn);
                cad.CadRuleUsageType = CadRuleUsageType.Parameter;
            });
        }

        public Rule<float> BendRadius { get; set; }
        protected virtual void ConfigureBendRadius(Rule<float> rule)
        {
            rule.Define(48);
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(IsBentLegColumn);
                cad.CadRuleUsageType = CadRuleUsageType.Parameter;
            });
        }

        public Rule<float> BendHyp { get; set; }
        protected virtual void ConfigureBendHyp(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (IsBentLegColumn.Value)
                {
                    //use Pythagorean Theorem
                    float a = Inset.Value;
                    float b = BentTangentLineElev.Value;
                    return (float)Math.Sqrt((Math.Pow(a, 2) + Math.Pow(b, 2)));
                }
                else
                    return 0;
            });
        }

        public Rule<float> SlantAngle { get; set; }
        protected virtual void ConfigureSlantAngle(Rule<float> rule)
        {
            rule.Define(() => Helper.RadToDeg((float)Math.Asin(Inset.Value / BendHyp.Value)));
        }

        [CadRule(CadRuleUsageType.Parameter)]
        public Rule<float> HasColumnHoles { get; set; }
        protected virtual void ConfigureHasColumnHoles(Rule<float> rule)
        {
            rule.Define(1);
        }
        public Rule<float> HoleSpacing { get; set; }
        protected virtual void ConfigureHoleSpacing(Rule<float> rule)
        {
            rule.Define(4);
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<bool> IsFrontColumn { get; set; }
        protected virtual void ConfigureIsFrontColumn(Rule<bool> rule)
        {
            rule.Define(false);
        }

        public Rule<bool> IsDoubler { get; set; }
        protected virtual void ConfigureIsDoubler(Rule<bool> rule)
        {
            rule.Define(false);
        }

        public Rule<string> BasePartNumber { get; set; }
        protected virtual void ConfigureBasePartNumber(Rule<string> rule)
        {
            rule.Define(default(string));
        }

        public Rule<string> BasePartNumberSuffix { get; set; }
        protected virtual void ConfigureBasePartNumberSuffix(Rule<string> rule)
        {
            rule.Define(default(string));
        }

        #endregion

        #region IChannel Overrides

        protected override void ConfigureChannelSizesAvailable(Rule<List<string>> rule)
        {
            rule.Define(() => (new List<string> { "C3x3.5", "C3x4.1", "C4x4.5", "C4x5.4", "C5x6.1", "C5x6.7" }));
        }

        #endregion

        #region IBomPart Overrides

        protected virtual void ConfigurePartNumberPrefix(Rule<string> rule)
        {
            rule.Define(() => BasePartNumber.Value + BasePartNumberSuffix.Value);
        }

        protected override void ConfigureDescription(Rule<string> rule)
        {
            rule.Define(() => $"{ChannelSize.Value}x{Length.Value}");
        }

        protected override void ConfigureBOMDescription(Rule<string> rule)
        {
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(false);
            });
        }

        #endregion

        //#region IRevisablePartImplementation

        //public FactoryRevisionRule<string> FactoryRevision { get; set; }

        //#endregion

        #region Protected Methods

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureChannelRules();
        }

        #endregion

        #region CPQ.IBomPart Implementation

        protected override void ConfigureDescriptionPrefix(Rule<string> rule)
        {
            rule.Define(() =>
            {
                return $"{((IsDoubler.Value) ? "Doubler" : "Column")} {PartNumberPrefix.Value?.TrimEnd('-') ?? ""}";
            });
        }

        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {
            child.PartNumber.Define(PartNumber);
            child.Description.Define(BOMUIDescription);
            child.MaterialPartNumber.Define(MaterialPartNumber);
            child.MaterialQty.Define(() => Length.Value / 12);
            child.ItemMaterialCost.Define(() => child.ItemRawMaterialWeight.Value * child.MaterialUnitCost.Value);

        }

        #endregion

        #region Part Overrides

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define(() =>
            {
                if (IsBentLegColumn.Value)
                {
                    if (IsReversed.Value)
                        return "ReverseBentLegColumnChannel";
                    else
                        return "BentLegColumnChannel";
                }
                else
                    return "Channel";
            });
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(true);
        }

        #endregion

        #region Labor Rules

        public Rule<double> BundleQty { get; set; }
        protected virtual void ConfigureBundleQty(Rule<double> rule)
        {
            rule.Define(() => CsiPartService.GetItemBundle(MaterialPartNumber.Value, Bom.PlantCode.Value));
        }

        #endregion  

        #region Graphics

        #region Graphic Overrides
        protected override void ConfigureChannelWebBlock(Block geo)
        {
            geo.Width.Define(ChannelWebThickness);
            geo.Height.Define(ChannelWidth);
            geo.Depth.Define(() => IsBentLegColumn.Value ? Length.Value - BentTangentLineElev.Value : Length.Value);
            geo.Position.Define(() =>
            {
                if (IsBentLegColumn.Value)
                    return Helper.Vxz(ChannelWebThickness.Value / 2, (geo.Depth.Value / 2) + BentTangentLineElev.Value);
                else
                    return Helper.Vxz(ChannelWebThickness.Value / 2, Length.Value / 2);
            });
        }

        protected override void ConfigureChannelTopFlangeBlock(Block geo)
        {
            geo.Width.Define(ChannelDepth);
            geo.Height.Define(ChannelWebThickness);
            geo.Depth.Define(() => IsBentLegColumn.Value ? Length.Value - BentTangentLineElev.Value : Length.Value);
            geo.Position.Define(() =>
            {
                if (IsBentLegColumn.Value)
                    return Helper.Vxyz(geo.Width.Value / 2, (ChannelWidth.Value / 2) - (geo.Height.Value / 2), (geo.Depth.Value / 2) + BentTangentLineElev.Value);
                else
                    return Helper.Vxyz(geo.Width.Value / 2, (ChannelWidth.Value / 2) - (geo.Height.Value / 2), Length.Value / 2);
            });
        }

        protected override void ConfigureChannelBtmFlangeBlock(Block geo)
        {
            geo.Width.Define(ChannelDepth);
            geo.Height.Define(ChannelWebThickness);
            geo.Depth.Define(() => IsBentLegColumn.Value ? Length.Value - BentTangentLineElev.Value : Length.Value);
            geo.Position.Define(() =>
            {
                if (IsBentLegColumn.Value)
                    return Helper.Vxyz(geo.Width.Value / 2, (-ChannelWidth.Value / 2) + (geo.Height.Value / 2), (geo.Depth.Value / 2) + BentTangentLineElev.Value);
                else
                    return Helper.Vxyz(geo.Width.Value / 2, (-ChannelWidth.Value / 2) + (geo.Height.Value / 2), Length.Value / 2);
            });
        }

        #endregion

        public ChannelGfx BentChannelBlock { get; set; }

        protected virtual void ConfigureBentChannelBlock(ChannelGfx child)
        {
            child.IsActive.Define(IsBentLegColumn);
            child.ChannelWidth.Define(ChannelWidth);
            child.ChannelDepth.Define(ChannelDepth);
            child.WebThickness.Define(ChannelWebThickness);
            child.Length.Define(BendHyp);
            child.Position.Define(() => Helper.Vxyz(0, 0, BentTangentLineElev.Value));
            child.Rotation.Define(() =>
            {
                float angle = IsReversed.Value ? -SlantAngle.Value : SlantAngle.Value;

                return Quaternion.Concatenate(Helper.CreateRotation(Vector3.UnitY, angle), Helper.CreateRotation(Vector3.UnitX, 180));
            });
        }

        //public UCS UCS { get; set; }
        //protected virtual void ConfigureUCS(UCS child)
        //{
        //    child.Diameter.Define(0.125f);
        //    child.Position.Define(() => (Helper.Vy(0)));
        //}

        #endregion

    }
}
