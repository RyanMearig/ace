﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;
using Ruler.Rules;
using Ruler.Cpq;
using Ruler.Rules.Validation;
using Ruler.Rules.Graphics;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class HorizontalBrace : Angle, IBomPart, Ruler.Cpq.IBomPart
    {
        #region Constructors

        public HorizontalBrace(ModelContext modelContext) : base(modelContext) { }

        public HorizontalBrace(string name, Part parent) : base(name, parent) { }

        #endregion

        #region Angle Overrides

        protected virtual void ConfigureAngleType(Rule<string> rule)
        {
            rule.Define(() => HorizontalBraceMaster.Value?.AngleType.Value ?? AngleTypeDefault.Value);
        }

        #endregion

        #region IBomPart Implementation

        public Rule<string> DatabasePartNumber { get; set; }
        public PartNumberRule<string> PartNumber { get; set; }
        public Rule<string> PartNumberPrefix { get; set; }
        public Rule<string> BOMDescription { get; set; }
        public Rule<string> BOMDescription1 { get; set; }
        public Rule<string> BOMDescription2 { get; set; }
        public Rule<string> BOMDescription3 { get; set; }
        public Rule<string> BOMDescription4 { get; set; }
        public Rule<string> BOMDescription5 { get; set; }
        public Rule<string> BOMDescription6 { get; set; }
        public Rule<string> BOMDescription7 { get; set; }
        public Rule<string> BOMDescription8 { get; set; }
        public Rule<string> BOMDescription9 { get; set; }
        public Rule<string> BOMDescription10 { get; set; }
        public Rule<string> BOMDescription11 { get; set; }
        public Rule<string> Color { get; set; }
        public ReadOnlyRule<AdvanceColor> AdvanceColor { get; set; }
        public ReadOnlyRule<string> ColorDescription { get; set; }
        public StorageConfiguratorRepository Repository { get; set; }
        public Rule<string> ColorBOMName { get; set; }
        public ReadOnlyRule<string> ColorShortDescription { get; set; }

        public Rule<double> PartWeight { get; set; }
        public Rule<double> PartPaintWeight { get; set; }
        public Rule<float> WeightPerGallon { get; set; }
        public CsiRule<double> PaintCostPerGallon { get; set; }
        public Rule<float> PaintCost { get; set; }

        #endregion

        #region CPQ.IBomPart Implementation

        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {
            child.PartNumber.Define(PartNumber);
            child.Description.Define(BOMUIDescription);
            child.MaterialPartNumber.Define(MaterialPartNumber);
            child.MaterialQty.Define(() => Length.Value / 12);
            child.ItemMaterialCost.Define(() => child.ItemRawMaterialWeight.Value * child.MaterialUnitCost.Value);
        }

        #endregion

        #region Labor Rules

        public Rule<double> PartCarriersPerHour { get; set; }
        protected virtual void ConfigurePartCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        #endregion

        #region Rules

        public Rule<HorizontalBrace> HorizontalBraceMaster { get; set; }
        protected virtual void ConfigureHorizontalBraceMaster(Rule<HorizontalBrace> rule)
        {
            rule.Define(default(HorizontalBrace));
        }

        public Rule<string> AngleTypeDefault { get; set; }
        protected virtual void ConfigureAngleTypeDefault(Rule<string> rule)
        {
            rule.Define("1-1/2 x 1-1/2 x 1/8");
        }

        public Rule<int> Id { get; set; }
        protected virtual void ConfigureId(Rule<int> rule)
        {
            rule.Define(default(int));
        }

        [UIRule]
        public Rule<float> PanelHeight { get; set; }
        protected virtual void ConfigurePanelHeight(Rule<float> rule)
        {
            rule.Define(() => HorizontalBraceMaster.Value?.PanelHeight.Value ?? PanelHeightDefault.Value);
            rule.Min(12);
            rule.Max(60);
        }

        public Rule<float> PanelHeightDefault { get; set; }
        protected virtual void ConfigurePanelHeightDefault(Rule<float> rule)
        {
            rule.Define(0);
        }

        [UIRule]
        public ReadOnlyRule<float> OffsetFromBottom { get; set; }
        protected virtual void ConfigureOffsetFromBottom(ReadOnlyRule<float> rule)
        {
            rule.Define(default(float));
        }

        public Rule<float> ColumnChannelDepth { get; set; }
        protected virtual void ConfigureColumnChannelDepth(Rule<float> rule)
        {
            rule.Define(default(float));
        }

        public Rule<float> ColumnChannelThickness { get; set; }
        protected virtual void ConfigureColumnChannelThickness(Rule<float> rule)
        {
            rule.Define(default(float));
        }

        public Rule<bool> CutForRearDoubler { get; set; }
        protected virtual void ConfigureCutForRearDoubler(Rule<bool> rule)
        {
            rule.Define(false);
        }

        public Rule<bool> CutForFrontDoubler { get; set; }
        protected virtual void ConfigureCutForFrontDoubler(Rule<bool> rule)
        {
            rule.Define(false);
        }

        public Rule<float> DiagonalOffsetFromVertical { get; set; }
        protected virtual void ConfigureDiagonalOffsetFromVertical(Rule<float> rule)
        {
            rule.Define(0.5f);
        }

        public Rule<float> OffsetFromColumnWebToToeEnd { get; set; }
        protected virtual void ConfigureOffsetFromColumnWebToToeEnd(Rule<float> rule)
        {
            rule.Define(() => (ColumnChannelDepth.Value - ColumnChannelThickness.Value));
        }

        public Rule<float> FrontDiagZOffset { get; set; }
        protected virtual void ConfigureFrontDiagZOffset(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (CutForFrontDoubler.Value)
                    return DiagonalOffsetFromVertical.Value;
                else
                    return OffsetFromColumnWebToToeEnd.Value + DiagonalOffsetFromVertical.Value;
            });
        }

        public Rule<Vector3> FrontDiagOffsetConnection { get; set; }
        protected virtual void ConfigureFrontDiagOffsetConnection(Rule<Vector3> rule)
        {
            rule.Define(() => GetVzFromOrigin(FrontDiagZOffset.Value));
        }

        public Rule<float> RearDiagZOffset { get; set; }
        protected virtual void ConfigureRearDiagZOffset(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (CutForRearDoubler.Value)
                    return CutLength.Value - DiagonalOffsetFromVertical.Value;
                else
                    return CutLength.Value - (DiagonalOffsetFromVertical.Value + OffsetFromColumnWebToToeEnd.Value);
            });
        }

        public Rule<Vector3> RearDiagOffsetConnection { get; set; }
        protected virtual void ConfigureRearDiagOffsetConnection(Rule<Vector3> rule)
        {
            rule.Define(() => GetVzFromOrigin(RearDiagZOffset.Value));
        }

        [CadRule(CadRuleUsageType.Parameter)]
        public Rule<float> RearMiterAngle { get; set; }
        protected virtual void ConfigureRearMiterAngle(Rule<float> rule)
        {
            rule.Define(0);
        }

        public Rule<bool> MiteredEnd { get; set; }
        protected virtual void ConfigureMiteredEnd(Rule<bool> rule)
        {
            rule.Define(() => RearMiterAngle.Value != 0 ? true : false);
        }

        protected override void ConfigureDescriptionPrefix(Rule<string> rule)
        {
            rule.Define(() => $"HORIZ {PartNumberPrefix.Value?.Replace("-", "") ?? ""}");
        }

        #endregion

        #region Private Methods

        private Vector3 GetVzFromOrigin(float zOffset)
        {
            return Helper.Vz(zOffset);
            return (Vector3.Subtract(this.Position.Value, Helper.Vz(zOffset)));
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(false);
        }

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureBomPartRules();
        }

        #endregion

        #region Graphics

        public WorkPoint FrontDiagOffsetConnectionPoint { get; set; }
        protected virtual void ConfigureFrontDiagOffsetConnectionPoint(WorkPoint child)
        {
            child.Position.Define(FrontDiagOffsetConnection);
        }

        public WorkPoint RearDiagOffsetConnectionPoint { get; set; }
        protected virtual void ConfigureRearDiagOffsetConnectionPoint(WorkPoint child)
        {
            child.Position.Define(RearDiagOffsetConnection);
        }

        //public UCS FrontDiagOffsetConnectionUCS { get; set; }
        //protected virtual void ConfigureFrontDiagOffsetConnectionUCS(UCS child)
        //{
        //    child.Visible.Define(true);
        //    child.Diameter.Define(0.25f);
        //    child.Position.Define(() => FrontDiagOffsetConnection.Value);
        //}

        //public UCS RearDiagOffsetConnectionUCS { get; set; }
        //protected virtual void ConfigureRearDiagOffsetConnectionUCS(UCS child)
        //{
        //    child.Visible.Define(false);
        //    child.Diameter.Define(0.25f);
        //    child.Position.Define(() => RearDiagOffsetConnection.Value);
        //}

        #endregion

    }
}
