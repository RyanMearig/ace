﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;
using Ruler.Rules;
using Ruler.Cpq;
using Ruler.Rules.Graphics;
using Ruler.Rules.Validation;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class BoltOnHorizontalBrace : PaintedChannel, IRevisablePart, Ruler.Cpq.IBomPart
    {
        #region Private Fields

        public AdvanceCSIItemService CsiPartService { get; }

        #endregion

        #region Constructors

        public BoltOnHorizontalBrace(ModelContext modelContext, StorageConfiguratorRepository repo, AdvanceCSIItemService csiPartService) : base(modelContext, repo)
        {
            CsiPartService = csiPartService;
        }

        public BoltOnHorizontalBrace(string name, Part parent, StorageConfiguratorRepository repo, AdvanceCSIItemService csiPartService) : base(name, parent, repo)
        {
            CsiPartService = csiPartService;
        }

        #endregion

        #region Rules

        public Rule<int> Id { get; set; }
        protected virtual void ConfigureId(Rule<int> rule)
        {
            rule.Define(default(int));
        }

        [UIRule]
        public Rule<float> PanelHeight { get; set; }
        protected virtual void ConfigurePanelHeight(Rule<float> rule)
        {
            rule.Define(0);
            rule.Min(12);
            rule.Max(60);
        }

        [UIRule]
        public ReadOnlyRule<float> OffsetFromBottom { get; set; }
        protected virtual void ConfigureOffsetFromBottom(ReadOnlyRule<float> rule)
        {
            rule.Define(default(float));
        }

        public Rule<bool> CutForRearDoubler { get; set; }
        protected virtual void ConfigureCutForRearDoubler(Rule<bool> rule)
        {
            rule.Define(false);
        }

        public Rule<bool> CutForFrontDoubler { get; set; }
        protected virtual void ConfigureCutForFrontDoubler(Rule<bool> rule)
        {
            rule.Define(false);
        }

        #endregion

        #region IRevisablePartImplementation

        public FactoryRevisionRule<string> FactoryRevision { get; set; }

        #endregion

        #region IBom Implementation

        protected virtual void ConfigurePartNumberPrefix(Rule<string> rule)
        {
            rule.Define(() => $"BOHEAVY{ChannelId.Value}-");
        }

        protected virtual void ConfigurePartWeight(Rule<double> rule)
        {
            rule.Define(() => this.GetPartWeightWithoutPaint());
        }

        #endregion

        #region CPQ.IBomPart Implementation

        protected override void ConfigureDescriptionPrefix(Rule<string> rule)
        {
            rule.Define(() => $"Bolt-On Horiz {PartNumberPrefix.Value.Replace("-", "")}");
        }

        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {
            child.PartNumber.Define(PartNumber);
            child.Description.Define(BOMUIDescription);
        }

        #endregion

        #region Labor Hours

        public Rule<double> PaintCarriersPerHour { get; set; }
        protected virtual void ConfigurePaintCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> UnloadCarriersPerHour { get; set; }
        protected virtual void ConfigureUnloadCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> PartsPerCarrier { get; set; }
        protected virtual void ConfigurePartsPerCarrier(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        #endregion

        #region Children

        public BoltOnHorizontalChannel BoltOnChannel { get; set; }
        protected virtual void ConfigureBoltOnChannel(BoltOnHorizontalChannel child)
        {
            child.ChannelSize.Define(ChannelSize);
            child.Length.Define(Length);
            child.Description.Define(Description);
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(Colors.Red);
        }

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define("BoltOnHorizontal");
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(false);
        }

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureRevisablePart();
            this.ConfigureBomPartRules();
        }

        #endregion

    }
}
