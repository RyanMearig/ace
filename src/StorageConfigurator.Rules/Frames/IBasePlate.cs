﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ruler.Rules;
using Ruler.Rules.Validation;

namespace StorageConfigurator.Rules
{
    public interface IBasePlate : IBasePlateData
    {
        Rule<string> BasePlateType { get; set; }
        //Rule<List<BasePlateTypeData>> AllBasePlateData { get; set; }
        //Rule<List<ChoiceListItem>> BasePlateTypeChoiceList { get; set; }
        Rule<BasePlateTypeData> ActiveBasePlateData { get; set; }
        Rule<bool> HasDoubler { get; set; }
        Rule<string> PostProtector { get; set; }
        Rule<float> AnchorQty { get; set; }
        Rule<string> AnchorSize { get; set; }
        Rule<float> BasePlateLength { get; set; }
        Rule<bool> HasBullnose { get; set; }
    }

    public static class IBasePlateExtensions
    {
        public static void ConfigureBasePlateRules(this IBasePlate comp)
        {
            comp.ConfigureBasePlateDataRules();

            comp.BasePlateType.Define(DefaultBasePlateType);
            comp.BasePlateType.AddToUI(ui =>
            {
                ui.ReadOnly.Define(true);
                ui.ChoiceList.Define(() => comp.BasePlateTypeChoiceList.Value);
            });

            comp.ActiveBasePlateData.Define(() => (comp.AllBasePlateData?.Value?.FirstOrDefault(d => d.BasePlateType == comp.BasePlateType.Value)));

            comp.HasDoubler.Define(() => comp.ActiveBasePlateData.Value != null ? comp.ActiveBasePlateData.Value.HasDoubler : false);
            comp.PostProtector.Define(() => comp.ActiveBasePlateData.Value != null ? comp.ActiveBasePlateData?.Value.PostProtector : "none");
            comp.PostProtector.AddToUI(ui => 
            {
                ui.ChoiceList.Define(() => comp.PostProtectorChoices.Value);
            });

            comp.AnchorQty.Define(() => comp.ActiveBasePlateData.Value != null ? comp.ActiveBasePlateData.Value.AnchorQty : 1);
            comp.AnchorQty.AddToUI(ui =>
            {
                ui.ChoiceList.Define(new List<ChoiceListItem>()
                {
                    new ChoiceListItem(1),
                    new ChoiceListItem(2),
                    new ChoiceListItem(4)
                });
            });

            comp.AnchorSize.Define(DefaultAnchorSize);
            comp.AnchorSize.AddToUI(ui =>
            {
                ui.ChoiceList.Define(new List<ChoiceListItem>()
                {
                    new ChoiceListItem("0.5", @"1/2"""),
                    new ChoiceListItem("0.625", @"5/8"""),
                    new ChoiceListItem("0.75", @"3/4""")
                });
            });

            comp.BasePlateLength.Define(() => comp.ActiveBasePlateData.Value != null ? comp.ActiveBasePlateData.Value.Length : 0.01f);

            comp.HasBullnose.Define(() => comp.ActiveBasePlateData.Value != null ? comp.ActiveBasePlateData.Value.PostProtector.Contains("bull") : false);
        }

        const string DefaultBasePlateType = "FP1K-A";
        const string DefaultAnchorSize = "0.5";
    }
}
