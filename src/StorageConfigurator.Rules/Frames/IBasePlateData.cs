﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ruler.Rules;
using Ruler.Rules.Validation;

namespace StorageConfigurator.Rules
{
    public interface IBasePlateData
    {
        Rule<List<BasePlateTypeData>> AllBasePlateData { get; set; }
        Rule<List<ChoiceListItem>> BasePlateTypeChoiceList { get; set; }
        Rule<List<ChoiceListItem>> PostProtectorChoices { get; set; }
    }

    public static class IBasePlateDataExtensions
    {
        public static void ConfigureBasePlateDataRules(this IBasePlateData comp)
        {
            comp.AllBasePlateData.Define(() =>
            {
                List<BasePlateTypeData> plateData = new List<BasePlateTypeData>();

                plateData.Add(new BasePlateTypeData() { BasePlateType = "FP1K-A", HasDoubler = false, PostProtector = "none", AnchorQty = 2, Length = 7 });
                plateData.Add(new BasePlateTypeData() { BasePlateType = "FP1K-B", HasDoubler = true, PostProtector = "none", AnchorQty = 2, Length = 7 });
                plateData.Add(new BasePlateTypeData() { BasePlateType = "FP1K-C", HasDoubler = false, PostProtector = "angle", AnchorQty = 2, Length = 7 });
                plateData.Add(new BasePlateTypeData() { BasePlateType = "FP1K-D", HasDoubler = true, PostProtector = "angle", AnchorQty = 2, Length = 7 });
                //ASP IMPROVEMENT: ADDED THE MISSING FOOTPLATES
                plateData.Add(new BasePlateTypeData() { BasePlateType = "FP1K-AR", HasDoubler = false, PostProtector = "none", AnchorQty = 2, Length = 7 });
                plateData.Add(new BasePlateTypeData() { BasePlateType = "FP1K-BR", HasDoubler = true, PostProtector = "none", AnchorQty = 2, Length = 7 });
                plateData.Add(new BasePlateTypeData() { BasePlateType = "FP1M-A", HasDoubler = false, PostProtector = "none", AnchorQty = 4, Length = 7 });
                plateData.Add(new BasePlateTypeData() { BasePlateType = "FP1M-B", HasDoubler = true, PostProtector = "none", AnchorQty = 4, Length = 7 });
                plateData.Add(new BasePlateTypeData() { BasePlateType = "FP1M-C", HasDoubler = false, PostProtector = "angle", AnchorQty = 4, Length = 7 });
                plateData.Add(new BasePlateTypeData() { BasePlateType = "FP1M-D", HasDoubler = true, PostProtector = "angle", AnchorQty = 4, Length = 7 });
                plateData.Add(new BasePlateTypeData() { BasePlateType = "FP2K-A", HasDoubler = false, PostProtector = "bullnose", AnchorQty = 2, Length = 7 });
                plateData.Add(new BasePlateTypeData() { BasePlateType = "FP2K-B", HasDoubler = true, PostProtector = "bullnose", AnchorQty = 2, Length = 8 });
                plateData.Add(new BasePlateTypeData() { BasePlateType = "FP3-A", HasDoubler = false, PostProtector = "none", AnchorQty = 1, Length = 7 });
                plateData.Add(new BasePlateTypeData() { BasePlateType = "FP3-B", HasDoubler = true, PostProtector = "none", AnchorQty = 1, Length = 7 });
                plateData.Add(new BasePlateTypeData() { BasePlateType = "FP3-C", HasDoubler = false, PostProtector = "angle", AnchorQty = 1, Length = 7 });
                plateData.Add(new BasePlateTypeData() { BasePlateType = "FP3-D", HasDoubler = true, PostProtector = "angle", AnchorQty = 1, Length = 7 });
                plateData.Add(new BasePlateTypeData() { BasePlateType = "FP4-A", HasDoubler = false, PostProtector = "bullnose", AnchorQty = 1, Length = 8 });
                plateData.Add(new BasePlateTypeData() { BasePlateType = "FP4-B", HasDoubler = true, PostProtector = "bullnose", AnchorQty = 1, Length = 9.5f });
                plateData.Add(new BasePlateTypeData() { BasePlateType = "FP5-A", HasDoubler = false, PostProtector = "none", AnchorQty = 4, Length = 7f });
                plateData.Add(new BasePlateTypeData() { BasePlateType = "FP5-B", HasDoubler = true, PostProtector = "none", AnchorQty = 4, Length = 7f });
                plateData.Add(new BasePlateTypeData() { BasePlateType = "FP5-C", HasDoubler = false, PostProtector = "angle", AnchorQty = 4, Length = 7f });
                plateData.Add(new BasePlateTypeData() { BasePlateType = "FP5-D", HasDoubler = true, PostProtector = "angle", AnchorQty = 4, Length = 7f });
                plateData.Add(new BasePlateTypeData() { BasePlateType = "FP6-A", HasDoubler = false, PostProtector = "bullnose", AnchorQty = 4, Length = 7f });
                plateData.Add(new BasePlateTypeData() { BasePlateType = "FP6-B", HasDoubler = true, PostProtector = "bullnose", AnchorQty = 4, Length = 7f });

                return plateData;
            });

            comp.BasePlateTypeChoiceList.Define(() =>
            {
                List<ChoiceListItem> choices = new List<ChoiceListItem>();

                if (comp.AllBasePlateData != null && comp.AllBasePlateData.Value != null && comp.AllBasePlateData.Value.Any())
                    comp.AllBasePlateData.Value.ForEach(d => choices.Add(new ChoiceListItem(d.BasePlateType, d.BasePlateType)));

                return choices;
            });

            comp.PostProtectorChoices.Define(() =>
            {
                return new List<ChoiceListItem>()
                {
                    new ChoiceListItem("none", "None"),
                    new ChoiceListItem("angle", "Angle"),
                    new ChoiceListItem("bullnose", "Bullnose")
                };
            });
        }
    }
}
