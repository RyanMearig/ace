﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;
using Ruler.Rules;
using Ruler.Cpq;
using Ruler.Rules.Graphics;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class WeldedFrameTie : FrameTieComponent, IAngle, IRevisablePart, Ruler.Cpq.IBomPart
    {
        #region Constructors

        public WeldedFrameTie(ModelContext modelContext, StorageConfiguratorRepository repo) : base(modelContext, repo) { }

        public WeldedFrameTie(string name, Part parent, StorageConfiguratorRepository repo) : base(name, parent, repo) { }

        #endregion

        #region Rules

        public Rule<double> Qty { get; set; }
        protected virtual void ConfigureQty(Rule<double> rule)
        {
            rule.Define(1);
            rule.AddToUI(ui =>
            {
                ui.Label.Define("Quantity");
                ui.ReadOnly.Define(true);
            });
        }

        public Rule<float> AngleLength { get; set; }
        protected virtual void ConfigureAngleLength(Rule<float> rule)
        {
            rule.Define(() => OverallLength.Value - (FrontEndPlate.Thickness.Value * 2));
        }

        public Rule<float> BeamWidth { get; set; }
        protected virtual void ConfigureBeamWidth(Rule<float> rule)
        {
            rule.Define(default(float));
        }

        public Rule<float> PlateWidth { get; set; }
        protected virtual void ConfigurePlateWidth(Rule<float> rule)
        {
            rule.Define(() => BeamWidth.Value > 1 ? (BeamWidth.Value - 1) : 2);
        }

        public Rule<float> EndPlateVerticalOffset { get; set; }
        protected virtual void ConfigureEndPlateVerticalOffset(Rule<float> rule)
        {
            rule.Define(2.25f);
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> EndPlatePartNumber { get; set; }
        protected virtual void ConfigureEndPlatePartNumber(Rule<string> rule)
        {
            rule.Define(() => FrontEndPlate.PartNumber.Value);
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> EndPlateDescription { get; set; }
        protected virtual void ConfigureEndPlateDescription(Rule<string> rule)
        {
            rule.Define(() => FrontEndPlate.Description.Value);
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> AnglePartNumber { get; set; }
        protected virtual void ConfigureAnglePartNumber(Rule<string> rule)
        {
            rule.Define(() => Angle.PartNumber.Value);
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> AngleMaterialDescription { get; set; }
        protected virtual void ConfigureAngleMaterialDescription(Rule<string> rule)
        {
            rule.Define(() => Angle.Description.Value);
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<float> AngleCutLength { get; set; }
        protected virtual void ConfigureAngleCutLength(Rule<float> rule)
        {
            rule.Define(() => Angle.CutLength.Value);
        }

        public Rule<string> BeamSize { get; set; }
        protected virtual void ConfigureBeamSize(Rule<string> rule)
        {
            rule.Define(default(string));
        }

        public Rule<string> BeamID { get; set; }
        protected virtual void ConfigureBeamID(Rule<string> rule)
        {
            rule.Define(default(string));
        }

        #endregion

        #region FrameTieComponent Overrides

        protected override void ConfigureFrameTieDescription1(ReadOnlyRule<string> rule)
        {
            rule.Define("WELDED");
        }

        protected override void ConfigureFrameTieDescription2(ReadOnlyRule<string> rule)
        {
            rule.Define(@"ROW/FRAME SPACER");
        }

        protected virtual void ConfigureBOMDescription(Rule<string> rule)
        {
            rule.Define(() =>
            {
                var insideConnections = "NO"; //TODO:Logic for this
                var desc = $"Welded Row / Frame Spacer {OverallLength.Value:#.##}\" Long";
                desc += $"|{this.GetAngleShortDesc(AngleType.Value)} material with 3/16\" x 6 endplates";
                desc += $"|Connecting to {BeamSize.Value}# column";
                desc += $"|w/{insideConnections} inside connections, {ColorBOMName.Value}";
                return desc;
            });
        }

        #endregion

        #region IAngleData Implementation
        public Rule<List<AngleData>> AllAngleData { get; set; }
        public Rule<List<ChoiceListItem>> AllAngleTypesChoiceList { get; set; }
        public Rule<AngleData> ActiveAngleData { get; set; }
        public Rule<List<string>> AngleSizesAvailable { get; set; }
        public Rule<string> AngleType { get; set; }
        public Rule<float> AngleWidth { get; set; }
        public Rule<float> AngleHeight { get; set; }
        public Rule<float> AngleThickness { get; set; }
        public Rule<string> MaterialPartNumber { get; set; }

        #endregion

        #region IRevisablePartImplementation

        public FactoryRevisionRule<string> FactoryRevision { get; set; }

        #endregion

        #region CPQ.IBomPart Implementation

        //This is the description used only by the BOM Summary.
        public Rule<string> BOMUIDescription { get; set; }
        protected virtual void ConfigureBOMUIDescription(Rule<string> rule)
        {
            rule.Define(() => $"{Type.Value.ToUpper()}({Helper.fractionString(OverallLength.Value, feetToo: false)}, {BeamID.Value}, {AngleType.Value})");
        }

        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {
            child.PartNumber.Define(PartNumber);
            child.Description.Define(BOMUIDescription);
            child.ItemQty.Define(Qty);
        }

        #endregion

        #region Labor Rules

        public Rule<double> WeldPartsPerHour { get; set; }
        protected virtual void ConfigureWeldPartsPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> PaintCarriersPerHour { get; set; }
        protected virtual void ConfigurePaintCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> UnloadCarriersPerHour { get; set; }
        protected virtual void ConfigureUnloadCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> PartsPerCarrier { get; set; }
        protected virtual void ConfigurePartsPerCarrier(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        #endregion

        #region Children

        public FrameTieAngle Angle { get; set; }
        protected virtual void ConfigureAngle(FrameTieAngle child)
        {
            child.AngleType.Define(AngleType);
            child.Length.Define(AngleLength);
            child.Position.Define(() => Helper.Vxz(-FrontEndPlate.Thickness.Value, child.AngleWidth.Value / 2));
            child.Rotation.Define(() => Quaternion.CreateFromYawPitchRoll(Helper.DegToRad(-90), 0, Helper.DegToRad(180)));
        }

        public FrameTieFourHoleEndPlate FrontEndPlate { get; set; }
        protected virtual void ConfigureFrontEndPlate(FrameTieFourHoleEndPlate child)
        {
            child.FrameTieType.Define(Type);
            child.Thickness.Define(0.1875f);
            child.Width.Define(PlateWidth);
            child.Height.Define(6);
            child.HoleDiameter.Define(0.5625f);
            child.HoleVerticalOffset.Define(1);
            child.HoleHorizOffset.Define(0.375f);
            child.Position.Define(() => Helper.Vy(EndPlateVerticalOffset.Value));
        }

        public FrameTieFourHoleEndPlate RearEndPlate { get; set; }
        protected virtual void ConfigureRearEndPlate(FrameTieFourHoleEndPlate child)
        {
            child.FrameTieType.Define(Type);
            child.Thickness.Define(0.1875f);
            child.Width.Define(PlateWidth);
            child.Height.Define(6);
            child.HoleDiameter.Define(0.5625f);
            child.HoleVerticalOffset.Define(1);
            child.HoleHorizOffset.Define(0.375f);
            child.Position.Define(() => Helper.Vxy(-OverallLength.Value, EndPlateVerticalOffset.Value));
            child.Rotation.Define(() => Helper.CreateRotation(Vector3.UnitY, 180));
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(Colors.Blue);
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(false);
        }

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define("WeldedFrameTie");
        }

        #endregion

        #region IBomPart Overrides

        protected virtual void ConfigurePartWeight(Rule<double> rule)
        {
            rule.Define(() => this.GetPartWeightWithoutPaint());
        }

        #endregion

        #region Graphics

        //public UCS UCS { get; set; }
        //protected virtual void ConfigureUCS(UCS child)
        //{
        //    child.Diameter.Define(0.0125f);
        //    child.Position.Define(() => (Helper.Vy(0)));
        //}

        #endregion

        #region Protected Methods

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureAngleRules();
            this.ConfigureRevisablePart();
        }

        #endregion
    }
}
