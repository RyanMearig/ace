﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class HeavyHorizontalDoubler : Channel
    {
        #region Constructors

        public HeavyHorizontalDoubler(ModelContext modelContext, StorageConfiguratorRepository repo) : base(modelContext, repo)
        {
            Repository = repo;
        }

        public HeavyHorizontalDoubler(string name, Part parent, StorageConfiguratorRepository repo) : base(name, parent, repo)
        {
            Repository = repo;
        }

        #endregion

    }
}
