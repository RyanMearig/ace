﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;
using Ruler.Cpq;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class TubeCapPlate : FlatPlate, Ruler.Cpq.IBomPart
    {
        #region Constructors

        public TubeCapPlate(ModelContext modelContext) : base(modelContext) { }

        public TubeCapPlate(string name, Part parent) : base(name, parent) { }

        #endregion

        #region Rules

        public Rule<float> ColumnWidth { get; set; }
        protected virtual void ConfigureColumnWidth(Rule<float> rule)
        {
            rule.Define(3);
        }

        public Rule<bool> HasDoubler { get; set; }      
        protected virtual void ConfigureHasDoubler(Rule<bool> rule)
        {
            rule.Define(false);
        }

        #endregion

        #region CPQ.IBomPart Implementation

        protected override void ConfigureDescriptionPrefix(Rule<string> rule)
        {
            rule.Define("Tube Cap Plate");
        }

        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {
            child.PartNumber.Define(PartNumber);
            child.Description.Define(BOMUIDescription);
            child.MaterialPartNumber.Define(MaterialPartNumber);
            child.MaterialQty.Define(() => Width.Value * Length.Value / 144);
            child.ItemMaterialCost.Define(() => child.ItemRawMaterialWeight.Value * child.MaterialUnitCost.Value);
        }

        #endregion

        #region Material Rules

        public Rule<string> MaterialPartNumber { get; set; }
        protected virtual void ConfigureMaterialPartNumber(Rule<string> rule)
        {
            rule.Define(default(string));
        }

        #endregion

        #region Labor Rules

        public Rule<double> PartCarriersPerHour { get; set; }
        protected virtual void ConfigurePartCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        #endregion

        #region Overrides

        protected override void ConfigureWidth(Rule<float> rule)
        {
            rule.Define(() => HasDoubler.Value ? 4.5f : 3.5f);
        }

        protected override void ConfigureLength(Rule<float> rule)
        {
            rule.Define(() => ColumnWidth.Value + 0.5f);
        }

        protected override void ConfigureThickness(Rule<float> rule)
        {
            rule.Define(0.25f);
        }

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureBomPartRules();
        }

        #endregion

    }
}
