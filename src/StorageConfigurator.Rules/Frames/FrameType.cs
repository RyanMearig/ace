﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ruler.Cpq;
using Ruler.Rules;
using Ruler.Rules.Graphics;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class FrameType : Part, IBasePlateData, IDepthMaster, IFrameUprightData, IColorComponent, IRevisablePart, Ruler.Cpq.IBomPart
    {
        #region Constructors

        public FrameType(ModelContext modelContext, StorageConfiguratorRepository repo) : base(modelContext)
        {
            Repository = repo;
        }

        public FrameType(string name, Part parent, StorageConfiguratorRepository repo) : base(name, parent)
        {
            this.setIndex();
            Repository = repo;

        }

        #endregion

        #region Rules

        public LookupRule<StorageSystem> StorageSystemComp { get; set; }

        public Rule<FrameType> FrameTypeMaster { get; set; }
        protected virtual void ConfigureFrameTypeMaster(Rule<FrameType> rule)
        {
            rule.Define(default(FrameType));
        }

        public Rule<int> Index { get; set; }
        protected virtual void ConfigureIndex(Rule<int> rule)
        {
            rule.Define(() => FrameTypeMaster.Value?.Index.Value ?? 0);
        }

        [UIRule]
        public Rule<string> FrameTypeName { get; set; }
        protected virtual void ConfigureFrameTypeName(Rule<string> rule)
        {
            rule.Define(() =>
            {
                return FrameTypeMaster.Value?.FrameTypeName.Value ?? $"FT{Index.Value}";
            });
            rule.Unique();
        }

        [UIRule]
        public Rule<float> Height { get; set; }
        protected virtual void ConfigureHeight(Rule<float> rule)
        {
            rule.Define(() => FrameTypeMaster.Value?.Height.Value ?? 360);
            rule.Min(HeightMin);
            rule.Max(480);
            rule.MultipleOf(4);
            rule.AddToCAD(cad =>
            {
                cad.CadRuleUsageType = CadRuleUsageType.Parameter;
            });
        }

        public Rule<float> HeightMin { get; set; }
        protected virtual void ConfigureHeightMin(Rule<float> rule)
        {
            rule.Define(() => UprightType.Value == "B" ? 80 : 12);
        }

        [UIRule]
        public ReadOnlyRule<int> PanelQty { get; set; }
        protected virtual void ConfigurePanelQty(ReadOnlyRule<int> rule)
        {
            //To be overridden to pull data from front frame
            rule.Define(1);
        }

        [CadRule(CadRuleUsageType.Parameter, "Depth")]
        public Rule<float> Width { get; set; }
        protected virtual void ConfigureWidth(Rule<float> rule)
        {
            rule.Define(() => FrameTypeMaster.Value?.Width.Value ?? 1);
        }

        [CadRule(CadRuleUsageType.Property, "IsDoubleDeep")]
        public Rule<string> IsDoubleDeepProp { get; set; }
        protected virtual void ConfigureIsDoubleDeepProp(Rule<string> rule)
        {
            rule.Define(() => IsDoubleDeep.Value.ToString());
        }

        [CadRule(CadRuleUsageType.Property, "IsDoubleDeepPost")]
        public Rule<string> IsDoubleDeepPostProp { get; set; }
        protected virtual void ConfigureIsDoubleDeepPostProp(Rule<string> rule)
        {
            rule.Define(() => IsDoubleDeepPost.Value.ToString());
        }

        [CadRule(CadRuleUsageType.Property, "HasFrontDoubler")]
        public Rule<string> HasFrontDoublerProp { get; set; }
        protected virtual void ConfigureHasFrontDoublerProp(Rule<string> rule)
        {
            rule.Define(() => HasFrontDoubler.Value.ToString());
        }

        [CadRule(CadRuleUsageType.Property, "HasRearDoubler")]
        public Rule<string> HasRearDoublerProp { get; set; }
        protected virtual void ConfigureHasRearDoublerProp(Rule<string> rule)
        {
            rule.Define(() => HasRearDoubler.Value.ToString());
        }

        public Rule<int> DefaultAnchorQtyPerFootplate { get; set; }
        protected virtual void ConfigureDefaultAnchorQtyPerFootplate(Rule<int> rule)
        {
            rule.Define(() => FrameTypeMaster.Value?.DefaultAnchorQtyPerFootplate.Value ?? 2);
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() => FrontAnchorQty.UIMetadata.ChoiceList.Value);
            });
        }

        public Rule<string> DefaultAnchorSizePerFootplate { get; set; }
        protected virtual void ConfigureDefaultAnchorSizePerFootplate(Rule<string> rule)
        {
            rule.Define(() => FrameTypeMaster.Value?.DefaultAnchorSizePerFootplate.Value ?? FrontAnchorSize.Value);
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() => FrontAnchorSize.UIMetadata.ChoiceList.Value);
            });
            rule.ValueSet += (sender, e) =>
            {
                DefaultAnchorTypePerFootplate.Reset();
            };
            rule.ValueReset += (sender, e) =>
            {
                DefaultAnchorTypePerFootplate.Reset();
            };
        }

        public Rule<string> DefaultAnchorTypePerFootplate { get; set; }
        protected virtual void ConfigureDefaultAnchorTypePerFootplate(Rule<string> rule)
        {
            rule.Define(() => FrameTypeMaster.Value?.DefaultAnchorTypePerFootplate.Value ?? rule.UIMetadata.ChoiceList.Value.First().Value.ToString());
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() => this.GetAnchorChoiceList(float.Parse(DefaultAnchorSizePerFootplate.Value)));
            });
        }

        public Rule<float> FrontFrameDepthDisplay { get; set; }
        protected virtual void ConfigureFrontFrameDepthDisplay(Rule<float> rule)
        {
            rule.Define(44);
            rule.AddToUI(ui =>
            {
                ui.Label.Define("Front Frame Depth");
            });
        }

        public Rule<float> RearFrameDepthDisplay { get; set; }
        protected virtual void ConfigureRearFrameDepthDisplay(Rule<float> rule)
        {
            rule.Define(44);
            rule.AddToUI(ui =>
            {
                ui.Label.Define("Rear Frame Depth");
            });
        }

        public Rule<string> FrameTieBeamSize { get; set; }
        protected virtual void ConfigureFrameTieBeamSize(Rule<string> rule)
        {
            rule.Define(() => FrameTypeMaster.Value?.FrameTieBeamSize.Value ?? UprightColumnSize.Value);
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => FrameTieType.Value != "rfswhd");
                ui.ChoiceList.Define(() => HeavyWeldedFrameTieChoices.Value);
            });
        }

        public Rule<List<ChoiceListItem>> HeavyWeldedFrameTieChoices { get; set; }
        protected virtual void ConfigureHeavyWeldedFrameTieChoices(Rule<List<ChoiceListItem>> rule)
        {
            rule.Define(() => new List<ChoiceListItem>());
        }

        [UIRule]
        public ReadOnlyRule<bool> IsUsedInFrameLine { get; set; }
        protected virtual void ConfigureIsUsedInFrameLine(ReadOnlyRule<bool> rule)
        {
            rule.Define(() =>
            {
                if (StorageSystemComp.Value != null && StorageSystemComp.Value.FrameTypesUsedInFrameLines.Value.Any())
                {
                    return StorageSystemComp.Value.FrameTypesUsedInFrameLines.Value.Any(f => f != null && f == this.Path);
                }
                else
                    return false;
            });
        }

        //[CadRule(CadRuleUsageType.Property)]
        //public LookupRule<string> LineItemName { get; set; }
        //protected virtual void ConfigureLineItemName(LookupRule<string> rule)
        //{
        //    rule.Define("[[LineItemName]]");
        //}

        #endregion

        #region IBasePlateData Implementation

        public Rule<List<BasePlateTypeData>> AllBasePlateData { get; set; }
        public Rule<List<ChoiceListItem>> BasePlateTypeChoiceList { get; set; }
        public Rule<List<ChoiceListItem>> PostProtectorChoices { get; set; }

        #endregion

        #region IDepthMaster Implementation

        public Rule<bool> IsDoubleDeep { get; set; }
        public Rule<string> DoubleDeepType { get; set; }
        public Rule<bool> IsDoubleDeepPost { get; set; }
        public Rule<float> FrontFrameDepth { get; set; }
        public Rule<float> RearFrameDepth { get; set; }
        public Rule<bool> HasFrameTies { get; set; }
        public Rule<string> FrameTieType { get; set; }
        public Rule<float> FrameTieLengthMin { get; set; }
        public Rule<float> FrameTieLength { get; set; }

        [CadRule(CadRuleUsageType.Parameter, "Width")]
        public ReadOnlyRule<float> OverallDepth { get; set; }
        protected virtual void ConfigureOverallDepth(ReadOnlyRule<float> rule)
        {
            rule.Define(() =>
            {
                if (IsDoubleDeep.Value)
                {
                    if (IsDoubleDeepPost.Value)
                        return FrontFrameDepthDisplay.Value + RearFrameDepthDisplay.Value;
                    else
                        return FrontFrameDepthDisplay.Value + RearFrameDepthDisplay.Value + FrameTieLength.Value;
                }
                else
                    return FrontFrameDepthDisplay.Value;
            });
        }

        #endregion

        #region IColorComponent Implementation

        public Rule<string> Color { get; set; }
        public ReadOnlyRule<AdvanceColor> AdvanceColor { get; set; }
        public ReadOnlyRule<string> ColorDescription { get; set; }
        public StorageConfiguratorRepository Repository { get; set; }
        public Rule<string> ColorBOMName { get; set; }
        public ReadOnlyRule<string> ColorShortDescription { get; set; }

        public Rule<double> PartWeight { get; set; }
        public Rule<double> PartPaintWeight { get; set; }
        public Rule<float> WeightPerGallon { get; set; }
        public CsiRule<double> PaintCostPerGallon { get; set; }
        public Rule<float> PaintCost { get; set; }


        #endregion

        #region IFrameUprightData Implementation

        public Rule<string> UprightType { get; set; }
        protected virtual void ConfigureUprightType(Rule<string> rule)
        {
            rule.Define(() => FrameTypeMaster.Value?.UprightType.Value ?? "S");
        }
        public Rule<bool> Slanted { get; set; }
        protected virtual void ConfigureSlanted(Rule<bool> rule)
        {
            rule.Define(() => FrameTypeMaster.Value?.Slanted.Value ?? (UprightType.Value == "B" || UprightType.Value == "L" || UprightType.Value == "LS"));
        }

        public Rule<float> SlantElev { get; set; }
        protected virtual void ConfigureSlantElev(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (FrameTypeMaster.Value != default(FrameType))
                {
                    return FrameTypeMaster.Value.SlantElev.Value;
                }
                else
                {
                    if (UprightType.Value == "L")
                    {
                        return 56;
                    }
                    else if (UprightType.Value == "LS")
                    {
                        return 36;
                    }
                    return 27;
                }
            });
        }

        public Rule<float> SlantInset { get; set; }
        protected virtual void ConfigureSlantInset(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (FrameTypeMaster.Value != default(FrameType))
                {
                    return FrameTypeMaster.Value.SlantInset.Value;
                }
                else
                {
                    if (UprightType.Value == "L" || UprightType.Value == "LS")
                    {
                        return 12;
                    }
                    else if (UprightType.Value == "B")
                    {
                        if (UprightColumnSize.Value.Contains("C3"))
                        {
                            if (SlantElev.Value < 39)
                            {
                                return 8;
                            }
                            else if (SlantElev.Value < 51)
                            {
                                return 12;
                            }
                            else if (SlantElev.Value < 63)
                            {
                                return 17;
                            }
                            else if (SlantElev.Value < 75)
                            {
                                return 20;
                            }
                            else if (SlantElev.Value < 87)
                            {
                                return 25;
                            }
                            return 29;
                        }
                        else
                        {
                            if (SlantElev.Value < 39)
                            {
                                return 6;
                            }
                            else if (SlantElev.Value < 51)
                            {
                                return 10;
                            }
                            else if (SlantElev.Value < 63)
                            {
                                return 13;
                            }
                            else if (SlantElev.Value < 75)
                            {
                                return 16;
                            }
                            else if (SlantElev.Value < 87)
                            {
                                return 19;
                            }
                            return 22;
                        }
                    }
                    return 0;
                }
            });
        }

        public Rule<float> SlantInsetMax { get; set; }
        public Rule<float> BendToFirstHorizOffset { get; set; }
        public Rule<string> UprightColumnSize { get; set; }
        protected virtual void ConfigureUprightColumnSize(Rule<string> rule)
        {
            rule.Define(() => FrameTypeMaster.Value?.UprightColumnSize.Value ?? "C3x3.5");
            rule.AddToUI(ui =>
            {
                ui.Label.Define("Default Upright Column Size");
            });
        }

        public Rule<float> FootplateWidth { get; set; }

        public Rule<float> FootplateThickness { get; set; }
        protected virtual void ConfigureFootplateThickness(Rule<float> rule)
        {
            rule.Define(() => FrameTypeMaster.Value?.FootplateThickness.Value ?? 0.375f);
            rule.AddToUI(ui =>
            {
                ui.Label.Define("Default Footplate Thickness");
            });
        }

        public Rule<float> HoleStartPunch { get; set; }

        public Rule<string> FrontPostProtector { get; set; }
        protected virtual void ConfigureFrontPostProtector(Rule<string> rule)
        {
            rule.Define(() => FrameTypeMaster.Value?.FrontPostProtector.Value ?? "none");
        }

        public Rule<float> FrontPostProtectorHeight { get; set; }
        protected virtual void ConfigureFrontPostProtectorHeight(Rule<float> rule)
        {
            rule.Define(() => FrameTypeMaster.Value?.FrontPostProtectorHeight.Value ?? (FrontPostProtector.Value == "bullnose" ? 3.75f : 12));
        }

        public Rule<bool> HasFrontDoubler { get; set; }
        protected virtual void ConfigureHasFrontDoubler(Rule<bool> rule)
        {
            rule.Define(() => FrameTypeMaster.Value?.HasFrontDoubler.Value ?? (UprightType.Value == "T" || UprightType.Value == "B"));
        }

        public Rule<float> FrontDoublerHeight { get; set; }
        public Rule<float> FrontDoublerHeightMin { get; set; }
        public Rule<string> FrontFootPlateType { get; set; }
        protected virtual void ConfigureFrontFootPlateType(Rule<string> rule)
        {
            rule.Define(() =>
            {
                if (FrameTypeMaster.Value != default(FrameType))
                {
                    return FrameTypeMaster.Value.FrontFootPlateType.Value;
                }
                else
                {
                    BasePlateTypeData bpData = AllBasePlateData.Value.FirstOrDefault(d =>
                    {
                        return (d.HasDoubler == HasFrontDoubler.Value &&
                            d.PostProtector == FrontPostProtector.Value &&
                            d.AnchorQty == FrontAnchorQty.Value);
                    });

                    if (bpData != null)
                    {
                        return bpData.BasePlateType;
                    }
                    return BasePlateTypeChoiceList.Value.First().Text;
                }
            });
        }

        public Rule<bool> HasRearDoubler { get; set; }
        protected virtual void ConfigureHasRearDoubler(Rule<bool> rule)
        {
            rule.Define(() => FrameTypeMaster.Value?.HasRearDoubler.Value ?? (UprightType.Value == "T" || UprightType.Value == "D"));
        }

        public Rule<float> RearDoublerHeight { get; set; }
        public Rule<float> RearDoublerHeightMin { get; set; }
        public Rule<string> RearPostProtector { get; set; }
        protected virtual void ConfigureRearPostProtector(Rule<string> rule)
        {
            rule.Define(() => FrameTypeMaster.Value?.RearPostProtector.Value ?? "none");
        }
        public Rule<float> RearPostProtectorHeight { get; set; }
        protected virtual void ConfigureRearPostProtectorHeight(Rule<float> rule)
        {
            rule.Define(() => FrameTypeMaster.Value?.RearPostProtectorHeight.Value ?? (RearPostProtector.Value == "bullnose" ? 3.75f : 12));
        }

        public Rule<string> RearFootPlateType { get; set; }
        protected virtual void ConfigureRearFootPlateType(Rule<string> rule)
        {
            rule.Define(() =>
            {
                if (FrameTypeMaster.Value != default(FrameType))
                {
                    return FrameTypeMaster.Value?.RearFootPlateType.Value;
                }
                else
                {
                    BasePlateTypeData bpData = AllBasePlateData.Value.FirstOrDefault(d =>
                    {
                        return (d.HasDoubler == HasRearDoubler.Value &&
                            d.PostProtector == RearPostProtector.Value &&
                            d.AnchorQty == RearAnchorQty.Value);
                    });

                    if (bpData != null)
                    {
                        return bpData.BasePlateType;
                    }
                    return BasePlateTypeChoiceList.Value.First().Text;
                }
            });
        }

        public Rule<int> FrontAnchorQty { get; set; }
        protected virtual void ConfigureFrontAnchorQty(Rule<int> rule)
        {
            rule.Define(() => FrameTypeMaster.Value?.FrontAnchorQty.Value ?? 2);
        }

        public Rule<int> RearAnchorQty { get; set; }
        protected virtual void ConfigureRearAnchorQty(Rule<int> rule)
        {
            rule.Define(() => FrameTypeMaster.Value?.RearAnchorQty.Value ?? 2);
        }

        public Rule<string> FrontAnchorSize { get; set; }
        public Rule<string> RearAnchorSize { get; set; }
        public Rule<string> FrontAnchorType { get; set; }
        public Rule<string> FrontBullnoseAnchorType { get; set; }
        public Rule<string> RearAnchorType { get; set; }
        public Rule<string> RearBullnoseAnchorType { get; set; }
        public Rule<List<AnchorData>> AllAnchorData { get; set; }
        public Rule<List<ChoiceListItem>> AllAnchorTypesChoiceList { get; set; }
        public Rule<float> MaxHeight { get; set; }
        public Rule<string> Location { get; set; }
        protected virtual void ConfigureLocation(Rule<string> rule)
        {
            rule.Define(() =>
            {
                var bomLoc = Bom?.PlantCode.Value;
                return bomLoc != null && bomLoc.ToUpper().Contains("UT") ? "UT" : "GA";
            });
            rule.ValueSet += (sender, e) =>
            {
                if (Height.Value > MaxHeight.Value)
                    Height.Reset();
            };
        }

        #endregion

        #region IRevisablePartImplementation

        public FactoryRevisionRule<string> FactoryRevision { get; set; }

        #endregion

        #region CPQ.IBomPart Implementation

        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {
            child.IsActive.Define(() => FrameTypeMaster.Value == default(FrameType));
        }

        #endregion

        #region Part Overrides

        public override bool IsCadPart
        {
            get
            {
                return FrameTypeMaster.Value == default(FrameType);
            }
        }

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(Colors.Lime);
        }

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define("FrameType");
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(() => FrameTypeMaster.Value == default(FrameType));
        }

        protected override void ConfigureAllowedChildTypes(ReadOnlyRule<IEnumerable<Type>> rule)
        {
            rule.Define(() => new List<Type>
            {
                typeof(CustomPart),
                typeof(BuyoutPart),
                typeof(FrameTie),
                typeof(Shim),
                typeof(TouchUpCan)
            });
        }

        #endregion

        #region Protected Methods

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureBasePlateDataRules();
            this.ConfigureDepthMasterRules();
            this.ConfigureFrameUprightDataRules();
            this.ConfigureColorComponent();
            this.ConfigureRevisablePart();
        }

        #endregion

        #region Private Methods

        private void setIndex()
        {
            if (Parent.GetType() == typeof(FrameTypes) && !Index.HasAdhocDefinedValue)
            {
                var existingFrameLines = Parent.ActiveChildParts.Value
                    .Where(c => c != this && c.TypeName.Contains("FrameType"));
                var index = existingFrameLines.Count() + 1;
                this.Index.SetAdhocDefaultValue(index);

            }
        }

        #endregion

    }
}
