﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;
using Ruler.Cpq;
using Ruler.Rules.Validation;
using Ruler.Rules.Graphics;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class DiagonalBrace : Angle, IBomPart, Ruler.Cpq.IBomPart
    {
        #region Private Fields

        public AdvanceCSIItemService CsiPartService { get; }

        #endregion

        #region Constructors

        public DiagonalBrace(ModelContext modelContext, AdvanceCSIItemService csiPartService) : base(modelContext)
        {
            CsiPartService = csiPartService;
        }

        public DiagonalBrace(string name, Part parent, AdvanceCSIItemService csiPartService) : base(name, parent)
        {
            CsiPartService = csiPartService;
        }

        #endregion

        #region IBomPart Implementation

        public Rule<string> DatabasePartNumber { get; set; }
        public PartNumberRule<string> PartNumber { get; set; }
        public Rule<string> PartNumberPrefix { get; set; }
        public Rule<string> BOMDescription { get; set; }
        public Rule<string> BOMDescription1 { get; set; }
        public Rule<string> BOMDescription2 { get; set; }
        public Rule<string> BOMDescription3 { get; set; }
        public Rule<string> BOMDescription4 { get; set; }
        public Rule<string> BOMDescription5 { get; set; }
        public Rule<string> BOMDescription6 { get; set; }
        public Rule<string> BOMDescription7 { get; set; }
        public Rule<string> BOMDescription8 { get; set; }
        public Rule<string> BOMDescription9 { get; set; }
        public Rule<string> BOMDescription10 { get; set; }
        public Rule<string> BOMDescription11 { get; set; }
        public Rule<string> Color { get; set; }
        public ReadOnlyRule<AdvanceColor> AdvanceColor { get; set; }
        public ReadOnlyRule<string> ColorDescription { get; set; }
        public StorageConfiguratorRepository Repository { get; set; }
        public Rule<string> ColorBOMName { get; set; }
        public ReadOnlyRule<string> ColorShortDescription { get; set; }

        public Rule<double> PartWeight { get; set; }
        public Rule<double> PartPaintWeight { get; set; }
        public Rule<float> WeightPerGallon { get; set; }
        public CsiRule<double> PaintCostPerGallon { get; set; }
        public Rule<float> PaintCost { get; set; }

        #endregion

        #region CPQ.IBomPart Implementation

        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {
            child.PartNumber.Define(PartNumber);
            child.Description.Define(BOMUIDescription);
            child.MaterialPartNumber.Define(MaterialPartNumber);
            child.MaterialQty.Define(() => Length.Value / 12);
            child.ItemMaterialCost.Define(() => child.ItemRawMaterialWeight.Value * child.MaterialUnitCost.Value);
        }

        #endregion

        #region Labor Rules

        public Rule<double> BundleQty { get; set; }
        protected virtual void ConfigureBundleQty(Rule<double> rule)
        {
            rule.Define(() => CsiPartService.GetItemBundle(MaterialPartNumber.Value, Bom.PlantCode.Value));
        }

        #endregion

        #region Rules

        public Rule<DiagonalBrace> DiagonalBraceMaster { get; set; }
        protected virtual void ConfigureDiagonalBraceMaster(Rule<DiagonalBrace> rule)
        {
            rule.Define(default(DiagonalBrace));
        }

        protected virtual void ConfigureAngleType(Rule<string> rule)
        {
            rule.Define(() => DiagonalBraceMaster.Value?.AngleType.Value ?? AngleTypeDefault.Value);
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() =>
                {
                    List<string> choices = new List<string>() { @"1-1/2 x 1-1/2 x 1/8", @"2 x 2 x 1/8", @"2 x 2 x 3/16", @"3 x 2 x 3/16" };
                    return Helper.GetFilteredChoices(AllAngleTypesChoiceList.Value, choices);
                });
            });
        }

        public Rule<string> AngleTypeDefault { get; set; }
        protected virtual void ConfigureAngleTypeDefault(Rule<string> rule)
        {
            rule.Define(@"1-1/2 x 1-1/2 x 1/8");
        }

        protected override void ConfigureDescriptionPrefix(Rule<string> rule)
        {
            rule.Define(() => $"DIAG {PartNumberPrefix.Value?.Replace("-", "") ?? ""}");
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(false);
        }

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureBomPartRules();
        }

        #endregion

    }
}
