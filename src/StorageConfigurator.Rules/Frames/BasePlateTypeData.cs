﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StorageConfigurator.Rules
{
    public class BasePlateTypeData
    {
        public string BasePlateType { get; set; }
        public bool HasDoubler { get; set; }
        public string PostProtector { get; set; }
        public int AnchorQty { get; set; }
        public float Length { get; set; }
    }
}
