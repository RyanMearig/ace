﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Numerics;
using Ruler.Rules;
using System.Linq;

namespace StorageConfigurator.Rules
{
    public class Helper
    {

        public static float DegToRad(float degrees)
        {
            return Convert.ToSingle(degrees * Math.PI / 180);
        }

        public static float RadToDeg(float radians)
        {
            return Convert.ToSingle(radians * 180 / Math.PI);
        }

        public static float Tan(float degrees)
        {
            return Convert.ToSingle(Math.Tan(Convert.ToSingle(degrees * Math.PI / 180)));
        }

        public static float ATan(float opposite, float adjacent)
        {
            return Convert.ToSingle(Math.Atan(opposite / adjacent) * 180 / Math.PI);
        }

        public static float ATan(float quotient)
        {
            return Convert.ToSingle(Math.Atan(quotient) * 180 / Math.PI);
        }

        public static float Cos(float degrees)
        {
            return Convert.ToSingle(Math.Cos(Convert.ToSingle(degrees * Math.PI / 180)));
        }

        public static float Sin(float degrees)
        {
            return Convert.ToSingle(Math.Sin(Convert.ToSingle(degrees * Math.PI / 180)));
        }

        public static Quaternion CreateRotation(Vector3 rotationAxis, float angle)
        {
            return Quaternion.CreateFromAxisAngle(rotationAxis, DegToRad(angle));
        }

        public static float AngleXYPlane(Vector3 vec1, Vector3 vec2)
        {
            var x = (vec1.X - vec2.X);
            var y = (vec1.Y - vec2.Y);

            var angle = Math.Atan(x / y);
            return RadToDeg((float)angle);
        }

        public static float RoundToNearestDecimal(float value, float nearest = 16f)
        {
            var multipliedValue = value * nearest;
            var nearestWhole = RoundToDecimal(multipliedValue, 0);
            return nearestWhole / nearest;
        }

        public static float RoundToNearest(float value, float nearest = 1)
        {
            return ((int)Math.Round(value / nearest, MidpointRounding.AwayFromZero)) * nearest;
        }

        public static float RoundDownToNearest(float value, float increment = 0.5f)
        {
            return Convert.ToSingle(Math.Floor(value / increment) * increment);
        }

        public static float RoundUpToNearest(float value, float increment = 0.5f)
        {
            return Convert.ToSingle(Math.Ceiling(value / increment) * increment);
        }

        public static float RoundToDecimal(float value, int places)
        {
            return Convert.ToSingle(Math.Round(value, places, MidpointRounding.AwayFromZero));
        }

        public static string fractionString(double value, int maxDenominator = 16,
            bool feetToo = true,
            bool suppressUnits = true,
            bool fractionHyphen = false,
            bool feetInchHyphen = true,
            bool hideZeroInches = true,
            bool hideZeroFeet = true)
        {
            //if (hideZeroInches == null)
            //    hideZeroInches = ((!feetToo) || (Math.Abs(value) < (1 - (1 / maxDenominator / 2))));

            var denominator = 0.0;
            var numerator = 0.0;

            //check the max denominator
            List<int> denominatorList = new List<int>() { 64, 32, 16, 8, 4, 2 };
            if (!denominatorList.Contains(maxDenominator))
                throw new Exception("Error in fractionString.  Denominator must be one of 2, 4, 8, 16, 32, or 64");


            double real = roundInc(value, 1.0 / maxDenominator);
            double abs = Math.Abs(real);
            double whole = Math.Floor(abs);
            double whole_inches = (feetToo ? (whole % 12) : whole);
            double feet = Math.Floor(whole / 12);
            double remainder = (abs - whole);
            bool absZero = (EqualToL(abs, 0, tolerance: 0.0001));
            bool remainderZero = (EqualToL(remainder, 0, tolerance: 0.0001));

            double roundedRemainder = roundInc(remainder, 1.0 / maxDenominator);

            if (roundedRemainder == 0)
                denominator = 0;
            else
            {
                for (int i = denominatorList.IndexOf(maxDenominator); i < (denominatorList.Count()); i++)
                {
                    int denom = denominatorList[i];

                    if (!(((roundedRemainder * denom) / 2) == Math.Floor((roundedRemainder * denom) / 2)))
                    {
                        denominator = denom;
                        break;
                    }
                }
            }

            numerator = (denominator * roundedRemainder);

            bool showFeet = (feetToo && ((feet >= 1) || !hideZeroFeet));
            bool showInches = ((whole_inches > 0) || (!hideZeroInches) || absZero);
            bool showFraction = !remainderZero;
            string signString = ((real < 0) ? "-" : "");
            string feetString = (showFeet ? string.Format("{0}'", feet.ToString()) : "");
            string feetDashString = "";
            if (showFeet && (showInches || showFraction))
            {
                if (feetInchHyphen)
                    feetDashString = "-";
                else
                    feetDashString = " ";
            }

            string inchString = (showInches ? whole_inches.ToString() : "");
            string inchDashString = "";
            if (showInches && showFraction)
            {
                if (fractionHyphen)
                    inchDashString = "-";
                else
                    inchDashString = " ";
            }

            string fracString = (showFraction ? (string.Format("{0}/", numerator.ToString()) + denominator.ToString()) : "");
            string inchUnits = (((feetToo || !suppressUnits) && (showFraction || showInches)) ? "\"" : "");

            //Concatenate the pieces together as the result
            return signString + feetString + feetDashString + inchString + inchDashString + fracString + inchUnits;
        }

        private static bool EqualToL(double num1, double num2, double tolerance = 0.001)
        {
            return Math.Abs(num1 - num2) <= tolerance;
        }

        private static double roundInc(double x, double i)
        {
            return (i * roundPlus(x / i));
        }

        private static double roundPlus(double x)
        {
            return Math.Floor(x + 0.5);
        }

        public static string GetUiDescription(IRule rule)
        {
            if(rule.UIMetadata != null)
            {
                var selectedValue = rule.UIMetadata.ChoiceList.Value.FirstOrDefault(cli => cli.Value.ToString() == rule.ValueObject.ToString());
                return selectedValue?.Text;
            }
            return null;
        }

        #region Vector2 Extensions

        public static Vector2 V2x(Vector2 offsetVector)
        {
            return offsetVector * Vector2.UnitX;
        }

        public static Vector2 V2x(float offset)
        {
            return new Vector2(offset, 0);
        }

        public static Vector2 V2y(Vector2 offsetVector)
        {
            return offsetVector * Vector2.UnitY;
        }

        public static Vector2 V2y(float offset)
        {
            return new Vector2(0, offset);
        }

        public static Vector2 V2xy(Vector2 offsetVector)
        {
            return offsetVector * new Vector2(1, 1);
        }

        #endregion

        #region Vector Conversion Extensions

        public enum Plane { XY, YZ, XZ };

        public static Vector3 V2ToV3(Vector2 vector2d, Plane plane = Plane.XY)
        {
            var offsetX = vector2d.X;
            var offsetY = vector2d.Y;
            switch (plane)
            {
                case Plane.YZ:
                    return Vyz(offsetX, offsetY);
                case Plane.XZ:
                    return Vxz(offsetX, offsetY);
                default:
                    return Vxy(offsetX, offsetY);
            };
        }

        #endregion

        #region Vector3 Extensions

        public static Vector3 Vx(Vector3 offsetVector)
        {
            return offsetVector * Vector3.UnitX;
        }

        public static Vector3 Vx(float offset)
        {
            return new Vector3(offset, 0, 0);
        }

        public static Vector3 Vy(Vector3 offsetVector)
        {
            return offsetVector * Vector3.UnitY;
        }

        public static Vector3 Vy(float offset)
        {
            return new Vector3(0, offset, 0);
        }

        public static Vector3 Vz(Vector3 offsetVector)
        {
            return offsetVector * Vector3.UnitZ;
        }

        public static Vector3 Vz(float offset)
        {
            return new Vector3(0, 0, offset);
        }

        public static Vector3 Vxy(Vector3 offsetVector)
        {
            return offsetVector * new Vector3(1, 1, 0);
        }

        public static Vector3 Vxy(float offsetX, float offsetY)
        {
            return new Vector3(offsetX, offsetY, 0);
        }

        public static Vector3 Vxz(Vector3 offsetVector)
        {
            return offsetVector * new Vector3(1, 0, 1);
        }

        public static Vector3 Vxz(float offsetX, float offsetZ)
        {
            return new Vector3(offsetX, 0, offsetZ);
        }

        public static Vector3 Vxyz(float offsetX, float offsetY, float offsetZ)
        {
            return new Vector3(offsetX, offsetY, offsetZ);
        }

        public static Vector3 Vyz(Vector3 offsetVector)
        {
            return offsetVector * new Vector3(0, 1, 1);
        }

        public static Vector3 Vyz(float offsetY, float offsetZ)
        {
            return new Vector3(0, offsetY, offsetZ);
        }

        public static Vector3 ToLocal(IRule<Vector3> vector)
        {
            var child = (Part)vector.Host;
            return Vector3.Transform(vector.Value, child.Matrix.Value);
        }

        #endregion

        #region UI Helpers

        public static IEnumerable<Rule<T>> GetChildProxyRules<T>(Part component)
        {
            return component?.Rules?.OfType<Rule<T>>()?
                                    .Where(r => r.CadMetadata != null &&
                                                r.CadMetadata.IsActive.Value &&
                                                r.CadMetadata.CadRuleUsageType == CadRuleUsageType.ChildProxy)
                             ?? Enumerable.Empty<Rule<T>>();
        }

        public static bool ValueExistsInChoices(IEnumerable<ChoiceListItem> choices, object value)
        {
            var choiceValues = choices.Select(c => c.Value);
            return choiceValues.Contains(value);
        }

        public static bool KeyExistsInChoices(List<ChoiceListItem> choices, string key)
        {
            return choices.Any(c => c.Text == key);
        }

        public static List<ChoiceListItem> GetFilteredChoices(List<ChoiceListItem> allChoices, List<string> choiceKeysToUse)
        {
            List<ChoiceListItem> choices = new List<ChoiceListItem>();

            if (choiceKeysToUse != null && choiceKeysToUse.Count > 0)
            {
                allChoices.ForEach(c =>
                {
                    if (choiceKeysToUse.Any(s => s == c.Text))
                        choices.Add(c);
                });
            }
            else
                choices = allChoices;

            return choices;
        }

        #endregion


    }
}
