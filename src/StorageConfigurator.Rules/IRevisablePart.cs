﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;

namespace StorageConfigurator.Rules
{
    public interface IRevisablePart
    {
        public FactoryRevisionRule<string> FactoryRevision { get; set; }


        //TODO: Learn how interface base method implementations work, or why they don't in this situation
        //protected void ConfigureFactoryRevision(FactoryRevisionRule<string> rule)
        //{
        //    rule.Define(() =>
        //    {
        //        if (this is Part part && !string.IsNullOrEmpty(part.FactoryName.Value))
        //        {
        //            return rule.FactoryRevisionService.GetFactoryMajorRevision(part.FactoryName.Value);
        //        }
        //        return null;
        //    });
        //    rule.AddToCAD(cad => cad.CadRuleUsageType = CadRuleUsageType.Property);
        //}
    }

    public static class RevisablePartExtensions
    {
        public static void ConfigureRevisablePart(this IRevisablePart comp)
        {
            comp.FactoryRevision.Define(() =>
            {
                if (comp is Part part && !string.IsNullOrEmpty(part.FactoryName.Value))
                {
                    return comp.FactoryRevision.FactoryRevisionService.GetFactoryMajorRevision(part.FactoryName.Value);
                }
                return null;
            });
            comp.FactoryRevision.AddToCAD(cad => cad.CadRuleUsageType = CadRuleUsageType.Property);
        }
    }

}
