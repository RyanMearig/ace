﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;
using Ruler.PartNumbers;

namespace StorageConfigurator.Rules
{
    public class PartNumberRule<T> : Rule<T>
    {

        private PartNumberService _partNumberService;

        public PartNumberRule(IRuleHost host, PartNumberService pnService) : base(host)
        {
            _partNumberService = pnService;
        }

        public PartNumberRule(string name, IRuleHost host, PartNumberService pnService) : base(name, host)
        {
            _partNumberService = pnService;
        }

        public PartNumberService PartNumberService
        {
            get
            {
                return _partNumberService;
            }
        }
    }
}
