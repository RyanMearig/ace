﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ruler.Rules;
using Ruler.Rules.Graphics;
using StorageConfigurator.Data;
using Ruler.Cpq;
using Microsoft.EntityFrameworkCore.Internal;
using Newtonsoft.Json;

namespace StorageConfigurator.Rules
{
    public class StorageSystem : ASLineItemPart, IBomPart, IRevisablePart, Ruler.Cpq.IBomPart
    {
        #region Constructors

        public StorageSystem(ModelContext modelContext, StorageConfiguratorRepository repo) : base(modelContext)
        {
            Repository = repo;
        }

        public StorageSystem(string name, Part parent, StorageConfiguratorRepository repo) : base(name, parent)
        {
            Repository = repo;
        }

        #endregion

        #region Rules

        public Rule<StorageSystem> StorageSystemComp { get; set; }
        protected virtual void ConfigureStorageSystemComp(Rule<StorageSystem> rule)
        {
            rule.Define(() => this);
        }

        [UIRule]
        public Rule<double> HardwareAdder { get; set; }
        protected virtual void ConfigureHardwareAdder(Rule<double> rule)
        {
            rule.Define(() => Repository?.GetDefaultOverhead("HardwareAdder") ?? 0);
        }

        [UIRule("Name")]
        public Rule<string> StorageSystemName { get; set; }
        protected virtual void ConfigureStorageSystemName(Rule<string> rule)
        {
            rule.Define(this.TypeName);
        }

        [UIRule]
        public ReadOnlyRule<int> BayQty { get; set; }
        protected virtual void ConfigureBayQty(ReadOnlyRule<int> rule)
        {
            rule.Define(() => Bays.BayQty.Value);
        }

        [UIRule]
        public ReadOnlyRule<int> ProductQty { get; set; }
        protected virtual void ConfigureProductQty(ReadOnlyRule<int> rule)
        {
            rule.Define(() => Products.ProductQty.Value);
        }

        [UIRule]
        public ReadOnlyRule<int> FrameLineQty { get; set; }
        protected virtual void ConfigureFrameLineQty(ReadOnlyRule<int> rule)
        {
            rule.Define(() => FrameLines.FrameLineQty.Value);
        }

        [UIRule]
        public ReadOnlyRule<int> RowTypeQty { get; set; }
        protected virtual void ConfigureRowTypeQty(ReadOnlyRule<int> rule)
        {
            rule.Define(() => RowTypes.RowTypeQty.Value);
        }

        [UIRule]
        public ReadOnlyRule<int> RowQty { get; set; }
        protected virtual void ConfigureRowQty(ReadOnlyRule<int> rule)
        {
            rule.Define(() => ActiveRowProxies.Value.Count);
        }

        public Rule<List<RowProxy>> ActiveRowProxies { get; set; }
        protected virtual void ConfigureActiveRowProxies(Rule<List<RowProxy>> rule)
        {
            rule.Define(() =>
            {
                return ActiveChildParts.Value?.Where(c => c.GetType() == typeof(RowProxy) && !string.IsNullOrWhiteSpace(((RowProxy)c).RowTypeName?.Value))
                    .Select(p => (RowProxy)p).ToList()
                    ?? new List<RowProxy>();
            });
        }

        public List<string> GetRowIds(string rowPath)
        {
            return ActiveRowProxies.Value?.Where(r => r.RowTypeName.Value == rowPath && r.IncludeInBOM.Value)?
                .Select(p => p.RowNumber.Value)?.ToList();
        }

        public Rule<List<Row>> RowList { get; set; }
        protected virtual void ConfigureRowList(Rule<List<Row>> rule)
        {
            rule.Define(() =>
            {
                return RowTypes.RowList.Value?.Where(r => r.RowQty.Value > 0)?.ToList() ?? new List<Row>();
                //return ActiveChildParts.Value?.Where(c => c.GetType() == typeof(Row))
                //        .Select(p => (Row)p).ToList()
                //        ?? new List<Row>();
            });
        }

        public Rule<List<Elevation>> UniqueElevations { get; set; }
        protected virtual void ConfigureUniqueElevations(Rule<List<Elevation>> rule)
        {
            rule.Define(() =>
            {
                List<Elevation> retList = new List<Elevation>();

                if (RowList.Value != null && RowList.Value.Any())
                {
                    List<Elevation> allRowElevations = new List<Elevation>();
                    RowList.Value.ForEach(r => allRowElevations.AddRange(r.UniqueElevations.Value));

                    allRowElevations.ForEach(e =>
                    {
                        if (retList.Count == 0 ||
                            !retList.Any(r =>
                            {
                                return (r.FrontFrame.Value == e.FrontFrame.Value &&
                                r.Bay.Value == e.Bay.Value &&
                                r.RearFrame.Value == e.RearFrame.Value);
                            }))
                        {
                            retList.Add(e);
                        }
                    });
                }

                return retList;
            });
        }

        public Rule<List<FrameLine>> FrameLinesUsedInElevations { get; set; }
        protected virtual void ConfigureFrameLinesUsedInElevations(Rule<List<FrameLine>> rule)
        {
            rule.Define(() =>
            {
                List<FrameLine> retList = new List<FrameLine>();

                if (UniqueElevations.Value.Any())
                {
                    List<FrameLine> allElevationFramelines = new List<FrameLine>();
                    UniqueElevations.Value.ForEach(e =>
                    {
                        allElevationFramelines.Add(e.FrontFrame.Value);
                        allElevationFramelines.Add(e.RearFrame.Value);
                    });

                    retList = allElevationFramelines.Distinct().ToList();
                }

                return retList;
            });
        }

        public Rule<List<Bay>> BaysUsedInElevations { get; set; }
        protected virtual void ConfigureBaysUsedInElevations(Rule<List<Bay>> rule)
        {
            rule.Define(() =>
            {
                List<Bay> retList = new List<Bay>();

                if (UniqueElevations.Value.Any())
                {
                    List<Bay> allElevationBays = UniqueElevations.Value.Select(e => e.Bay.Value).ToList();
                    UniqueElevations.Value.Where(e => e.HasRearBay.Value)?.ToList()
                                          .ForEach(e => allElevationBays.Add(e.RearBay.Value));
                    retList = allElevationBays.Distinct().ToList();
                }

                return retList;
            });
        }

        public Rule<List<string>> FrameTypesUsedInFrameLines { get; set; }
        protected virtual void ConfigureFrameTypesUsedInFrameLines(Rule<List<string>> rule)
        {
            rule.Define(() => FrameLines.FrameTypesUsedInFrameLines.Value);
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> rulerDrawingXml { get; set; }
        protected virtual void ConfigurerulerDrawingXml(Rule<string> rule)
        {
            rule.Define("StorageSystem");
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> FOBFactory { get; set; }
        protected virtual void ConfigureFOBFactory(Rule<string> rule)
        {
            rule.Define(() => Bom.PlantCode.UIMetadata.ChoiceList.Value.FirstOrDefault(c => c.Value.ToString() == Bom.PlantCode.Value)?.Text);
        }

        public ReadOnlyRule<List<HardwareGroup>> HardwareGroups { get; set; }
        protected virtual void ConfigureHardwareGroups(ReadOnlyRule<List<HardwareGroup>> rule)
        {
            rule.Define(() =>
            {
                var retVal = new List<HardwareGroup>();

                if (RowList.Value != null && RowList.Value.Any())
                {
                    retVal.AddRange(RowList.Value.SelectMany(r => GetHardwareGroups(r)));
                }

                return retVal;
            });
        }

        public ReadOnlyRule<List<HardwareKit>> HardwareKits { get; set; }
        protected virtual void ConfigureHardwareKits(ReadOnlyRule<List<HardwareKit>> rule)
        {
            rule.Define(() =>
            {
                var retVal = new List<HardwareKit>();

                if (HardwareGroups.Value != null && HardwareGroups.Value.Any())
                {
                    retVal = HardwareGroups.Value.Select(r => r.HardwareKit).ToList();
                }

                return retVal;
            });
        }

        public Rule<List<BoltDefinition>> HardwareKitDefinitions { get; set; }
        protected virtual void ConfigureHardwareKitDefinitions(Rule<List<BoltDefinition>> rule)
        {
            rule.Define(() =>
            {
                var retlist = new List<BoltDefinition>();

                retlist.AddRange(HardwareKits.Value.Select(h => new BoltDefinition()
                {
                    BoltSize = h.BoltSize.Value,
                    BoltType = h.BoltType.Value,
                    BoltMaterial = h.BoltMaterial.Value,
                    Length = h.Length.Value,
                    NutType = h.NutType.Value,
                    NutMaterial = h.NutMaterial.Value,
                    Qty = h.Qty.Value
                }));

                return retlist;
            });
        }

        public ReadOnlyRule<List<BoltDefinition>> UniqueHardwareKits { get; set; }
        protected virtual void ConfigureUniqueHardwareKits(ReadOnlyRule<List<BoltDefinition>> rule)
        {
            rule.Define(() =>
            {
                var retVal = new List<BoltDefinition>();

                if (HardwareKitDefinitions.Value != null && HardwareKitDefinitions.Value.Any())
                {
                    var hardwareKitGroups = HardwareKitDefinitions.Value.GroupBy(k => new
                    {
                        BoltSize = k.BoltSize,
                        BoltType = k.BoltType,
                        BoltMaterial = k.BoltMaterial,
                        Length = k.Length,
                        NutMaterial = k.NutMaterial,
                        NutType = k.NutType
                    });

                    retVal = hardwareKitGroups.Select(g =>
                    {
                        var kit = g.First();
                        kit.Qty = g.Sum(k => k.Qty);
                        return kit;
                    }).ToList();
                }

                return retVal;
            });
        }

        public Rule<List<Washers>> WashersInKit { get; set; }
        protected virtual void ConfigureWashersInKit(Rule<List<Washers>> rule)
        {
            rule.Define(() =>
            {
                var retVal = new List<Washers>();

                if (HardwareGroups.Value != null && HardwareGroups.Value.Any())
                {
                    retVal.AddRange(HardwareGroups.Value.Where(g => g.Washers.IsActive.Value).Select(r => r.Washers));
                }

                return retVal;
            });
        }

        public Rule<List<WasherDefinition>> WasherDefinitions { get; set; }
        protected virtual void ConfigureWasherDefinitions(Rule<List<WasherDefinition>> rule)
        {
            rule.Define(() =>
            {
                var retlist = new List<WasherDefinition>();

                retlist.AddRange(WashersInKit.Value.Select(h => new WasherDefinition()
                {
                    BoltSize = h.BoltDiameter.Value,
                    WasherType = h.WasherType.Value,
                    WasherMaterial = h.WasherMaterial.Value,
                    Qty = h.Qty.Value
                }));

                return retlist;
            });
        }

        public ReadOnlyRule<List<WasherDefinition>> UniqueWashers { get; set; }
        protected virtual void ConfigureUniqueWashers(ReadOnlyRule<List<WasherDefinition>> rule)
        {
            rule.Define(() =>
            {
                var retVal = new List<WasherDefinition>();

                if (WasherDefinitions.Value != null && WasherDefinitions.Value.Any())
                {
                    var washerGroups = WasherDefinitions.Value.GroupBy(k => new
                    {
                        BoltSize = k.BoltSize,
                        WasherType = k.WasherType,
                        WasherMaterial = k.WasherMaterial
                    });

                    retVal = washerGroups.Select(g =>
                    {
                        var wash = g.First();
                        wash.Qty = g.Sum(k => k.Qty);
                        return wash;
                    }).ToList();
                }

                return retVal;
            });
        }

        public Rule<List<AnchorDefinition>> AnchorDefinitions { get; set; }
        protected virtual void ConfigureAnchorDefinitions(Rule<List<AnchorDefinition>> rule)
        {
            rule.Define(() =>
            {
                //Due to CAD Proxy's, we'll get one instance per unique frame.
                var frameGroups = GetAllActiveRowFrames()?.GroupBy(g => g.Path);
                
                return frameGroups?.SelectMany(f => GetFrameLineAnchorDefinitions(f.FirstOrDefault(), f.Count())).ToList()
                    ?? new List<AnchorDefinition>();
            });
        }

        public Rule<List<AnchorDefinition>> UniqueAnchorDefinitions { get; set; }
        protected virtual void ConfigureUniqueAnchorDefinitions(Rule<List<AnchorDefinition>> rule)
        {
            rule.Define(() =>
            {
                return AnchorDefinitions.Value?.Where(d => d.AnchorData != null)
                            .GroupBy(k => k.AnchorData.Type)?
                            .Select(g =>
                            {
                                var def = g.First();
                                def.Qty = g.Sum(k => k.Qty);
                                return def;
                            }).ToList()
                            ?? new List<AnchorDefinition>();
            });
        }

        public Rule<int> BullnoseCapPlugQty { get; set; }
        protected virtual void ConfigureBullnoseCapPlugQty(Rule<int> rule)
        {
            rule.Define(() =>
            {
                //Due to CAD Proxy's, we'll get one instance per unique frame.
                return GetAllActiveRowFrames()?
                                .SelectMany(t => t.UsedFrameTypes.Value)?
                                .Where(f => f != null && f.IsActive.Value)?
                                .SelectMany(b => b.GetDescendantParts().OfType<BullnoseCoverPlate>())
                                .Where(p => p != null && p.IsActive.Value)?.Count() ?? 0;
            });
        }

        #endregion

        #region IBomPart Implementation

        public Rule<string> DatabasePartNumber { get; set; }
        public PartNumberRule<string> PartNumber { get; set; }
        protected virtual void ConfigurePartNumber(PartNumberRule<string> rule)
        {
            rule.Define(() => LineItemName.Value);
        }
        public Rule<string> PartNumberPrefix { get; set; }
        public Rule<string> Description { get; set; }
        protected virtual void ConfigureDescription(Rule<string> rule)
        {
            rule.Define("Storage System");
        }

        public Rule<string> BOMDescription { get; set; }
        protected virtual void ConfigureBOMDescription(Rule<string> rule)
        {
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(false);
            });
        }

        public Rule<string> BOMDescription1 { get; set; }
        public Rule<string> BOMDescription2 { get; set; }
        public Rule<string> BOMDescription3 { get; set; }
        public Rule<string> BOMDescription4 { get; set; }
        public Rule<string> BOMDescription5 { get; set; }
        public Rule<string> BOMDescription6 { get; set; }
        public Rule<string> BOMDescription7 { get; set; }
        public Rule<string> BOMDescription8 { get; set; }
        public Rule<string> BOMDescription9 { get; set; }
        public Rule<string> BOMDescription10 { get; set; }
        public Rule<string> BOMDescription11 { get; set; }

        public Rule<string> Color { get; set; }
        public ReadOnlyRule<AdvanceColor> AdvanceColor { get; set; }
        public ReadOnlyRule<string> ColorDescription { get; set; }
        public Rule<string> ColorBOMName { get; set; }
        public ReadOnlyRule<string> ColorShortDescription { get; set; }

        public Rule<double> PartWeight { get; set; }
        public Rule<double> PartPaintWeight { get; set; }
        public Rule<float> WeightPerGallon { get; set; }
        public CsiRule<double> PaintCostPerGallon { get; set; }
        public Rule<float> PaintCost { get; set; }

        #endregion

        #region IRevisablePartImplementation

        public FactoryRevisionRule<string> FactoryRevision { get; set; }

        #endregion

        #region Ruler.Cpq.IBomPart Implementation

        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {
            child.ShortDescription.Define(StorageSystemName);
            child.PartNumber.Define(PartNumber);
            child.BomChildren.Define(BOMChildren);
            child.Description.Define(StorageSystemName);
            child.PlantCode.Define("GA_C");
        }

        public BomData RowBom { get; set; }
        protected virtual void ConfigureRowBom(BomData child)
        {
            child.ShortDescription.Define(() => Bom.ShortDescription.Value);
            child.PartNumber.Define(() => Bom.PartNumber.Value);
            child.BomChildren.Define(() =>
            {
                var retVal = RowBomChildren.Value.ToList();
                retVal.AddRange(FirstLevelCustomBuyoutParts.Value);
                retVal.AddRange(HardwareBomParts.Value);
                return retVal;
            });
            child.Item.Define(() => Bom.Item.Value);
            child.PlantCode.Define(() => Bom.PlantCode.Value);
        }

        public Rule<double> DefaultMarginPercent { get; set; }
        protected virtual void ConfigureDefaultMarginPercent(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        protected override void ConfigureLineItemName(Rule<string> rule)
        {
            rule.Define(() => $"{ProjectNumber.Value}.{this.StorageSystemName.Value}");
        }

        protected override void ConfigureBOMChildren(Rule<IEnumerable<BomData>> rule)
        {
            rule.Define(() =>
            {
                var retVal = NonRowBomChildren.Value.ToList();
                retVal.AddRange(FirstLevelCustomBuyoutParts.Value);
                retVal.AddRange(HardwareBomParts.Value);
                return retVal;
            });
        }

        public Rule<IEnumerable<BomData>> NonRowBomChildren { get; set; }
        protected virtual void ConfigureNonRowBomChildren(Rule<IEnumerable<BomData>> rule)
        {
            rule.Define(() => RowList.Value.SelectMany(r => r.AllRowBomChildren.Value));
        }

        public Rule<IEnumerable<BomData>> RowBomChildren { get; set; }
        protected virtual void ConfigureRowBomChildren(Rule<IEnumerable<BomData>> rule)
        {
            rule.Define(() => RowList.Value.SelectMany(r => r.Boms));
        }

        public Rule<List<BomData>> FirstLevelCustomBuyoutParts { get; set; }
        protected virtual void ConfigureFirstLevelCustomBuyoutParts(Rule<List<BomData>> rule)
        {
            rule.Define(() =>
            {
                var customChildren = this.ActiveChildren.Value?.Where(c => c.TypeName == "CustomPart" || c.TypeName == "BuyoutPart");
                var childBOMs = customChildren?.Select(c =>
                {
                    if (c is Ruler.Cpq.IBomPart bomChild)
                    {
                        return bomChild.Bom;
                    }
                    else
                    {
                        return null;
                    }
                })?.ToList();

                return childBOMs ?? new List<BomData>() { };
            });
        }

        public Rule<List<BomData>> HardwareBomParts { get; set; }
        protected virtual void ConfigureHardwareBomParts(Rule<List<BomData>> rule)
        {
            rule.Define(() => GetBomChildren(this.HardwareContainer)?.ToList() ?? new List<BomData>() { });
        }

        #endregion

        #region Children

        public Products Products { get; set; }
        protected virtual void ConfigureProducts(Products child) { }

        public Bays Bays { get; set; }
        protected virtual void ConfigureBays(Bays child) { }

        public FrameTypes FrameTypes { get; set; }
        protected virtual void ConfigureFrameTypes(FrameTypes child) { }

        public FrameLines FrameLines { get; set; }
        protected virtual void ConfigureFrameLines(FrameLines child)
        {
            child.IsActive.Define(() => FrameTypes.FrameTypeQty.Value > 0);
        }

        public RowTypes RowTypes { get; set; }
        protected virtual void ConfigureRowTypes(RowTypes child) { }


        public Elevations Elevations { get; set; }
        protected virtual void ConfigureElevations(Elevations child)
        {
            child.UniqueElevationsList.Define(UniqueElevations);
        }

        //This extra layer was created due to Circular Reference issues when evaluating the Hardware Kits child list qty
        //at the Storage System level.
        public HardwareContainer HardwareContainer { get; set; }

        protected virtual void ConfigureHardwareContainer(HardwareContainer child)
        {
            child.HardwareDefinitions.Define(UniqueHardwareKits);
            child.WasherDefinitions.Define(UniqueWashers);
            child.AnchorDefinitions.Define(UniqueAnchorDefinitions);
            child.BullnoseCapPlugQty.Define(BullnoseCapPlugQty);
        }

        #endregion

        #region Methods

        public List<FrameLine> GetAllActiveRowFrames()
        {
            return RowList.Value?.SelectMany(r =>
            {
                var singleRowFrames = r.GetDescendantParts().OfType<RowFrame>()?
                                       .Select(f => f.ActiveFrame.Value)
                                       .Where(d => d!= null && d.IsActive.Value)
                                       .ToList()
                    ?? new List<FrameLine>();

                if (r.RowQty.Value == 1) { return singleRowFrames; }

                var retList = new List<FrameLine>();
                for (int i = 0; i < r.RowQty.Value; i++)
                {
                    retList.AddRange(singleRowFrames);
                }
                return retList;

            }).ToList();
        }

        public IEnumerable<BomData> GetBomChildren(IHierarchicalRuleHost part)
        {
            var bomChildren = new List<BomData>();
            foreach (var child in part.ActiveChildren.Value)
            {
                if (child is Ruler.Cpq.IBomPart bomChild && bomChild.Bom.IsActive.Value)
                {
                    bomChildren.Add(bomChild.Bom);
                }
                else if (child is IHierarchicalRuleHost hhost)
                {
                    bomChildren.AddRange(GetBomChildren(hhost));
                }
            }
            return bomChildren;
        }

        public List<AnchorDefinition> GetFrameLineAnchorDefinitions(FrameLine frameline, int framelineQty)
        {
            var uprightColAssys = frameline.UsedFrameTypes.Value?
                                        .SelectMany(f => f.GetDescendantParts().OfType<UprightColumnAssembly>())?
                                        .Where(c => c.Parent.IsActive.Value && c.IsActive.Value);
            var anchorDefs = uprightColAssys?.SelectMany(u => u.AnchorDefinitions.Value).ToList();

            return anchorDefs?.Select(d => new AnchorDefinition() { AnchorData = d.AnchorData, Qty = d.Qty * framelineQty })
                .ToList()
                ?? new List<AnchorDefinition>();
        }

        public List<HardwareGroup> GetHardwareGroups(Row row)
        {
            var singleRowList = new List<HardwareGroup>();

            //Get RowBay groups
            var rowBays = row.GetDescendantParts().OfType<RowBay>().ToList();
            singleRowList.AddRange(rowBays.SelectMany(b => b.GetDescendantParts().OfType<HardwareGroup>()));

            //Get Bay groups
            singleRowList.AddRange(rowBays.Select(b => b.ActiveBay.Value)?
                                            .Where(r => r != null)?
                                            .SelectMany(b => b.GetDescendantParts().OfType<HardwareGroup>()));

            //Get Frame groups
            var frameTypeHGroups = row.GetDescendantParts().OfType<RowFrame>()?
                                .Select(f => f.ActiveFrame.Value)?
                                .SelectMany(t => t.UsedFrameTypes.Value)?
                                .Where(f => f != null)?
                                .SelectMany(b => b.GetDescendantParts().OfType<HardwareGroup>()).ToList();

            if (frameTypeHGroups.Any())
            {
                singleRowList.AddRange(frameTypeHGroups);
            }

            singleRowList = singleRowList.Where(l => l.IsActive.Value)?.ToList();

            if (row.RowQty.Value == 1) { return singleRowList; }

            var retList = new List<HardwareGroup>();
            for (int i = 0; i < row.RowQty.Value; i++)
            {
                retList.AddRange(singleRowList);
            }

            return retList;
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(Colors.Gray);
            //material.Opacity.Define(0.9f);
        }

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define(() => RowQty.Value > 0 ? "StorageSystem" : "");
        }

        protected override void ConfigureAllowedChildTypes(ReadOnlyRule<IEnumerable<Type>> rule)
        {
            rule.Define(() => new List<Type>
            {
                typeof(RowProxy),
                typeof(CustomPart),
                typeof(BuyoutPart)
            });
        }

        #endregion

        #region Graphics

        //public UCS UCS { get; set; }
        //protected virtual void ConfigureUCS(UCS child)
        //{
        //    child.Diameter.Define(0.25f);
        //    child.Position.Define(() => (Helper.Vy(0)));
        //}

        #endregion

        #region Protected Methods

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureBomPartRules();
            this.ConfigureRevisablePart();
        }

        #endregion

    }
}
