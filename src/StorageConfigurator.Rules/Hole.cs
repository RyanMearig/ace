﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;
using Ruler.Rules.Graphics;

namespace StorageConfigurator.Rules
{
    public class Hole : Part
    {
        #region Constructors

        public Hole(ModelContext modelContext) : base(modelContext) { }

        public Hole(string name, Part parent) : base(name, parent) { }

        #endregion

        #region Rules

        public Rule<float> HoleSize { get; set; }
        protected virtual void ConfigureHoleSize(Rule<float> rule)
        {
            rule.Define(default(float));
        }

        public Rule<string> HoleType { get; set; }
        protected virtual void ConfigureHoleType(Rule<string> rule)
        {
            rule.Define("round");
        }

        public Rule<float> Depth { get; set; }
        protected virtual void ConfigureDepth(Rule<float> rule)
        {
            rule.Define(default(float));
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(Colors.Black);
            material.Opacity.Define(0.99f);
        }

        #endregion

        #region Graphics

        public Cylinder HoleGraphic { get; set; }
        protected virtual void ConfigureHoleGraphic(Cylinder geo)
        {
            geo.IsActive.Define(() => HoleType.Value == "round");
            geo.RadiusTop.Define(() => HoleSize.Value / 2);
            geo.RadiusBottom.Define(() => HoleSize.Value / 2);
            geo.Height.Define(Depth);
            geo.RadiusSegments.Define(100);
        }

        public Block SquareHoleGraphic { get; set; }
        protected virtual void ConfigureSquareHoleGraphic(Block geo)
        {
            geo.IsActive.Define(() => HoleType.Value != "round");
            geo.Width.Define(HoleSize);
            geo.Height.Define(Depth);
            geo.Depth.Define(HoleSize);
            //geo.Position.Define(() => Helper.Vxy(-geo.Width.Value / 2, -geo.Height.Value / 2));
        }

        //public UCS UCS { get; set; }
        //protected virtual void ConfigureUCS(UCS child)
        //{
        //    child.Position.Define(() => (Helper.Vy(0)));
        //}

        #endregion
    }
}
