﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class PaintedChannel : Channel
    {
        #region Constructors

        public PaintedChannel(ModelContext modelContext, StorageConfiguratorRepository repo) : base(modelContext, repo)
        {
            Repository = repo;
        }

        public PaintedChannel(string name, Part parent, StorageConfiguratorRepository repo) : base(name, parent, repo)
        {
            Repository = repo;
        }

        #endregion

        #region Rules


        #endregion

        #region Children

        public ChildList<Paint> Paint { get; set; }
        protected virtual void ConfigurePaint(ChildList<Paint> list)
        {
            list.Qty.Define(() =>
            {
                if (this is Ruler.Cpq.IBomPart bomPart)
                {
                    return bomPart.Bom.IsActive.Value && !string.IsNullOrWhiteSpace(Color.Value) ? 1 : 0;
                }
                return 0;
            });
            list.ConfigureChildren((child, index) =>
            {
                child.PartWeight.Define(PartWeight);
            });
        }

        #endregion

        #region Overrides

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureBomPartRules();
        }

        #endregion

    }
}
