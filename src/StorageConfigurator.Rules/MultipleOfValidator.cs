﻿using Ruler.Rules;
using Ruler.Rules.Validation;

namespace StorageConfigurator.Rules
{
    public class MultipleOfValidator : RuleValidator<float>
    {
        public MultipleOfValidator(float multipleOf, float startIncValue = 0, ValidatorOptions options = null) : base(options)
        {
            MultipleOf = multipleOf;
            StartIncrementValue = startIncValue;
        }

        public override bool IsValid(float value, out string message)
        {
            var result = (value - StartIncrementValue) % MultipleOf == 0;
            message = (result) ? null : $"Value must be a multiple of {MultipleOf} starting at {StartIncrementValue}.";
            return result;
        }

        public float MultipleOf { get; }
        public float StartIncrementValue { get; }
    }

    public static class MultipleOfValidatorExtensions
    {
        public static void MultipleOf(this IRule<float> rule, float multipleOf, float startIncValue = 0, ValidatorOptions options = null)
        {
            rule.AddValidator(new MultipleOfValidator(multipleOf, startIncValue, options));
        }
    }
}
