﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StorageConfigurator.Rules
{
    public class ChannelData
    {
        public string ChannelSize { get; set; }
        public string ChannelSizeDisplay { get; set; }
        public float Width { get; set; }
        public float Depth { get; set; }
        public float WebThickness { get; set; }
        public float WeightPerFt { get; set; }
    }
}
