﻿using System.Collections.Generic;
using System.Linq;
using Ruler.Rules;
using Ruler.Rules.Validation;

namespace StorageConfigurator.Rules
{
    public class DoublerInterfereWithHorizontalsValidator : RuleValidator<float>
    {
        private readonly IRule<List<float>> _locsRule;

        public DoublerInterfereWithHorizontalsValidator(IRule<List<float>> locsRule, ValidatorOptions options = null) : base(options)
        {
            _locsRule = locsRule;
            locsRule.Unbound += (sender, e) => this.RaiseUnbound();
        }

        public override bool IsValid(float startRange, out string message)
        {
            float endRange = startRange + 3;            
            var result = !_locsRule.Value.Any(c => IsInRange(c, startRange, endRange));
            message = (result) ? null : $"Value must not be in Interference Range";
            return result;
        }

        public bool IsInRange(float value, float startRange, float endRange)
        {
            return (value >= startRange && value <= endRange);
        }
    }

    public static class DoublerInterfereWithHorizontalsValidatorExtensions
    {
        public static void NoInterferenceWithHorizontals(this IRule<float> rule, IRule<List<float>> locsRule, ValidatorOptions options = null)
        {
            rule.AddValidator(new DoublerInterfereWithHorizontalsValidator(locsRule, options));
        }
    }
}
