﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;
using Ruler.Rules.Validation;
using Ruler.Rules.Graphics;
using System.Numerics;

namespace StorageConfigurator.Rules
{
    public class Angle : AdvancePart, IAngle, IRevisablePart
    {
        #region Constructors

        public Angle(ModelContext modelContext) : base(modelContext) { }

        public Angle(string name, Part parent) : base(name, parent) { }

        #endregion

        #region Rules

        public Rule<float> Length { get; set; }
        protected virtual void ConfigureLength(Rule<float> rule)
        {
            rule.Define(5);
        }

        public Rule<float> CutLength { get; set; }
        protected virtual void ConfigureCutLength(Rule<float> rule)
        {
            rule.Define(() => (Helper.RoundDownToNearest(Length.Value, increment: 0.0625f)));
            rule.AddToUI(ui =>
            {
                ui.Label.Define("Length");
            });
            rule.AddToCAD(cad =>
            {
                cad.NameInCad = "B_L";
            });
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> Description { get; set; }
        protected virtual void ConfigureDescription(Rule<string> rule)
        {
            rule.Define(() => $"{Helper.fractionString(AngleThickness.Value)} x {Helper.fractionString(AngleHeight.Value)} x {Helper.fractionString(AngleWidth.Value)} ANGLE");
        }

        public Rule<string> DescriptionPrefix { get; set; }
        protected virtual void ConfigureDescriptionPrefix(Rule<string> rule)
        {
            rule.Define(default(string));
        }

        //This is the description used only by the BOM Summary.
        public Rule<string> BOMUIDescription { get; set; }
        protected virtual void ConfigureBOMUIDescription(Rule<string> rule)
        {
            rule.Define(() => $"{DescriptionPrefix.Value}({AngleType.Value}, {Helper.fractionString(CutLength.Value, feetToo: false)})");
        }

        #endregion

        #region IAngle Implementation

        public Rule<List<AngleData>> AllAngleData { get; set; }
        public Rule<List<ChoiceListItem>> AllAngleTypesChoiceList { get; set; }
        public Rule<AngleData> ActiveAngleData { get; set; }
        public Rule<List<string>> AngleSizesAvailable { get; set; }
        public Rule<string> AngleType { get; set; }
        public Rule<float> AngleWidth { get; set; }
        protected virtual void ConfigureAngleWidth(Rule<float> rule)
        {
            rule.AddToCAD(cad =>
            {
                cad.NameInCad = "G_W";
            });
        }
        public Rule<float> AngleHeight { get; set; }
        protected virtual void ConfigureAngleHeight(Rule<float> rule)
        {
            rule.AddToCAD(cad =>
            {
                cad.NameInCad = "G_H";
            });
        }
        public Rule<float> AngleThickness { get; set; }
        protected virtual void ConfigureAngleThickness(Rule<float> rule)
        {
            rule.AddToCAD(cad =>
            {
                cad.NameInCad = "G_T";
            });
        }

        public Rule<string> MaterialPartNumber { get; set; }

        public Rule<Vector3> Rear { get; set; }
        protected virtual void ConfigureRear(Rule<Vector3> rule)
        {
            rule.Define(() => Helper.Vz(this.CutLength.Value));
        }

        #endregion

        #region IRevisablePart Implementation

        public FactoryRevisionRule<string> FactoryRevision { get; set; }

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(Colors.Green);
        }

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define("Angle");
        }

        #endregion

        #region Graphics

        public Block HorizontalLegBlock { get; set; }
        protected virtual void ConfigureHorizontalLegBlock(Block geo)
        {
            geo.Width.Define(AngleWidth);
            geo.Height.Define(AngleThickness);
            geo.Depth.Define(CutLength);
            geo.Position.Define(() => Helper.Vxyz(geo.Width.Value/2, geo.Height.Value/2, geo.Depth.Value/2));
        }

        public Block VerticalLegBlock { get; set; }

        protected virtual void ConfigureVerticalLegBlock(Block geo)
        {
            geo.Width.Define(AngleThickness);
            geo.Height.Define(AngleHeight);
            geo.Depth.Define(CutLength);
            geo.Position.Define(() => Helper.Vxyz(geo.Width.Value / 2, geo.Height.Value / 2, geo.Depth.Value / 2));
        }

        //public UCS RearUCS { get; set; }
        //protected virtual void ConfigureRearUCS(UCS child)
        //{
        //    child.Diameter.Define(0.25f);
        //    child.Position.Define(() => Rear.Value);
        //}

        //public UCS UCS { get; set; }
        //protected virtual void ConfigureUCS(UCS child)
        //{
        //    child.Diameter.Define(0.25f);
        //    child.Position.Define(() => (Helper.Vy(0)));
        //}

        #endregion

        #region Protected Methods

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureAngleRules();
            this.ConfigureRevisablePart();
        }

        #endregion

    }
}
