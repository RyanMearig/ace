﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class PaintedPart : AdvancePart, IBomPart
    {
        #region Constructors

        public PaintedPart(ModelContext modelContext) : base(modelContext) { }

        public PaintedPart(string name, Part parent) : base(name, parent) { }

        #endregion

        #region IBomPart Implementation

        public Rule<string> DatabasePartNumber { get; set; }
        public PartNumberRule<string> PartNumber { get; set; }
        public Rule<string> PartNumberPrefix { get; set; }
        public Rule<string> Description { get; set; }
        public Rule<string> BOMDescription { get; set; }
        public Rule<string> BOMDescription1 { get; set; }
        public Rule<string> BOMDescription2 { get; set; }
        public Rule<string> BOMDescription3 { get; set; }
        public Rule<string> BOMDescription4 { get; set; }
        public Rule<string> BOMDescription5 { get; set; }
        public Rule<string> BOMDescription6 { get; set; }
        public Rule<string> BOMDescription7 { get; set; }
        public Rule<string> BOMDescription8 { get; set; }
        public Rule<string> BOMDescription9 { get; set; }
        public Rule<string> BOMDescription10 { get; set; }
        public Rule<string> BOMDescription11 { get; set; }
        public Rule<string> Color { get; set; }
        public ReadOnlyRule<AdvanceColor> AdvanceColor { get; set; }
        public ReadOnlyRule<string> ColorDescription { get; set; }
        public StorageConfiguratorRepository Repository { get; set; }
        public Rule<string> ColorBOMName { get; set; }
        public ReadOnlyRule<string> ColorShortDescription { get; set; }

        public Rule<double> PartWeight { get; set; }
        public Rule<double> PartPaintWeight { get; set; }
        public Rule<float> WeightPerGallon { get; set; }
        public CsiRule<double> PaintCostPerGallon { get; set; }
        public Rule<float> PaintCost { get; set; }

        #endregion

        #region Rules


        #endregion

        #region Children

        public ChildList<Paint> Paint { get; set; }
        protected virtual void ConfigurePaint(ChildList<Paint> list)
        {
            list.Qty.Define(() =>
            {
                if (this is Ruler.Cpq.IBomPart bomPart)
                {
                    return bomPart.Bom.IsActive.Value && !string.IsNullOrWhiteSpace(Color.Value) ? 1 : 0;
                }
                return 0;
            });
            list.ConfigureChildren((child, index) =>
            {
                child.PartWeight.Define(PartWeight);
            });
        }

        #endregion

        #region Overrides

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureBomPartRules();
        }

        #endregion


    }
}
