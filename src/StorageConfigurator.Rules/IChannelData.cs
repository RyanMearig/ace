﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ruler.Rules;

namespace StorageConfigurator.Rules
{
    public interface IChannelData
    {
        Rule<List<ChannelData>> AllChannelData { get; set; }
        Rule<List<ChoiceListItem>> AllChannelSizesChoiceList { get; set; }
    }

    public static class IChannelDataExtensions
    {
        public static void ConfigureChannelDataRules(this IChannelData comp)
        {
            comp.AllChannelData.Define(() =>
            {
                List<ChannelData> channelData = new List<ChannelData>();

                channelData.Add(new ChannelData() { ChannelSize = @"C3x3.5", ChannelSizeDisplay = @"C3x3.5", Width = 3f, Depth = 1.38f, WebThickness = 0.132f, WeightPerFt = 3.5f });
                channelData.Add(new ChannelData() { ChannelSize = @"C3x4.1", ChannelSizeDisplay = @"C3x4.1", Width = 3f, Depth = 1.41f, WebThickness = 0.17f, WeightPerFt = 4.1f });
                channelData.Add(new ChannelData() { ChannelSize = @"C4x4.5", ChannelSizeDisplay = @"C4x4.5", Width = 4f, Depth = 1.58f, WebThickness = 0.125f, WeightPerFt = 4.5f });
                channelData.Add(new ChannelData() { ChannelSize = @"C4x5.4", ChannelSizeDisplay = @"C4x5.4", Width = 4f, Depth = 1.58f, WebThickness = 0.184f, WeightPerFt = 5.4f });
                channelData.Add(new ChannelData() { ChannelSize = @"C5x6.1", ChannelSizeDisplay = @"C5x6.1", Width = 5f, Depth = 1.75f, WebThickness = 0.16f, WeightPerFt = 6.1f });
                channelData.Add(new ChannelData() { ChannelSize = @"C5x6.7", ChannelSizeDisplay = @"C5x6.7", Width = 5f, Depth = 1.75f, WebThickness = 0.19f, WeightPerFt = 6.7f });
                channelData.Add(new ChannelData() { ChannelSize = @"C6x8.2", ChannelSizeDisplay = @"C6x8.2", Width = 6f, Depth = 1.92f, WebThickness = 0.2f, WeightPerFt = 8.2f });
                channelData.Add(new ChannelData() { ChannelSize = @"C7x9.8", ChannelSizeDisplay = @"C7x9.8", Width = 7f, Depth = 2.09f, WebThickness = 0.21f, WeightPerFt = 9.8f });
                channelData.Add(new ChannelData() { ChannelSize = @"C8x11.5", ChannelSizeDisplay = @"C8x11.5", Width = 8f, Depth = 2.26f, WebThickness = 0.22f, WeightPerFt = 11.5f });
                channelData.Add(new ChannelData() { ChannelSize = @"C10x15.3", ChannelSizeDisplay = @"C10x15.3", Width = 10f, Depth = 2.6f, WebThickness = 0.24f, WeightPerFt = 15.3f });

                return channelData;
            });

            comp.AllChannelSizesChoiceList.Define(() =>
            {
                List<ChoiceListItem> choices = new List<ChoiceListItem>();

                if (comp.AllChannelData != null && comp.AllChannelData.Value != null && comp.AllChannelData.Value.Any())
                    comp.AllChannelData.Value.ForEach(d => choices.Add(new ChoiceListItem(d.ChannelSize, d.ChannelSizeDisplay)));

                return choices;
            });

        }
    }
}
