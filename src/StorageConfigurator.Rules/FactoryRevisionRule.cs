﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class FactoryRevisionRule<T> : UncachedRule<T>
    {
        public FactoryRevisionRule(string name, IRuleHost host, FactoryRevisionService factoryRevService) : base(name, host)
        {
            FactoryRevisionService = factoryRevService;
        }

        public FactoryRevisionService FactoryRevisionService { get; }
    }
}
