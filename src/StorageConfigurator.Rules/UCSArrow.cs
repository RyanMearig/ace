﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ruler.Rules;
using Ruler.Rules.Graphics;
using System.Numerics;

namespace StorageConfigurator.Rules
{
    public class UCSArrow : Part
    {
        #region Constructors

        public UCSArrow(ModelContext modelContext) : base(modelContext) { }

        public UCSArrow(string name, Part parent) : base(name, parent) { }

        #endregion

        #region Rules

        public Rule<float> Diameter { get; set; }
        protected virtual void ConfigureDiameter(Rule<float> rule)
        {
            rule.Define(4f);
        }

        public Rule<float> Length { get; set; }
        protected virtual void ConfigureLength(Rule<float> rule)
        {
            rule.Define(10f);
        }

        public Rule<string> ArrowColor { get; set; }
        protected virtual void ConfigureArrowColor(Rule<string> rule)
        {
            rule.Define("Yellow");
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial uiMaterial)
        {
            uiMaterial.Color.Define(() =>
            {
                if (ArrowColor.Value.ToLower() == "red")
                    return Colors.Red;
                else if (ArrowColor.Value.ToLower() == "green")
                    return Colors.Lime;
                else if (ArrowColor.Value.ToLower() == "blue")
                    return Colors.Blue;
                else
                    return Colors.Yellow;
            });
            uiMaterial.Opacity.Define(0.9f);
        }

        #endregion

        #region Graphics

        public Cylinder Arrow { get; set; }
        protected virtual void ConfigureArrow(Cylinder geo)
        {
            geo.RadiusTop.Define(() => Diameter.Value / 2);
            geo.RadiusBottom.Define(() => Diameter.Value / 2);
            geo.Height.Define(Length);
            geo.RadiusSegments.Define(120);
            geo.Position.Define(() => (Helper.Vy(Length.Value/2)));
        }

        #endregion
    }
}
