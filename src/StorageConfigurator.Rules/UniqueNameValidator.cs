﻿using System.Linq;
using Ruler.Rules;
using Ruler.Rules.Validation;

namespace StorageConfigurator.Rules
{
    public class UniqueNameValidator : RuleValidator<string>
    {
        private readonly IRuleHost _part;
        private readonly string _ruleName;

        public UniqueNameValidator(IRuleHost part, string ruleName, ValidatorOptions options = null) : base(options)
        {
            _part = part;
            _ruleName = ruleName;
        }

        public override bool IsValid(string value, out string message)
        {
            bool result = true;
            message = "Valid";
            var parent = _part.Parent as IHierarchicalRuleHost;
            if (parent != null)
            {
                var siblings = parent.ActiveChildren.Value.Where(c => c != _part);
                var likeSiblings = siblings.Where(s => s.HasRule<string>(_ruleName));
                if (likeSiblings.Any(s => s.GetRule<string>(_ruleName).Value == value))
                {
                    result = false;
                    message = $"{value} already exists";
                }
            }
            return result;
        }
    }

    public static class UniqueNameValidatorExtensions
    {
        public static void Unique(this IRule<string> rule, ValidatorOptions options = null)
        {
            rule.AddValidator(new UniqueNameValidator(rule.Host, rule.Name, options));
        }
    }
}
