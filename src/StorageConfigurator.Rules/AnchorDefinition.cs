﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StorageConfigurator.Rules
{
    public class AnchorDefinition
    {
        public AnchorData AnchorData { get; set; }
        public int Qty { get; set; } = 1;
    }
}
