﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ruler.Rules;

namespace StorageConfigurator.Rules
{
    public class Bays : Part
    {
        #region Constructors

        public Bays(ModelContext modelContext) : base(modelContext) { }

        public Bays(string name, Part parent) : base(name, parent) { }

        #endregion

        #region Rules

        [UIRule]
        public ReadOnlyRule<int> BayQty { get; set; }
        protected virtual void ConfigureBayQty(ReadOnlyRule<int> rule)
        {
            rule.Define(() => BayList.Value.Count);
        }

        public Rule<List<Bay>> BayList { get; set; }
        protected virtual void ConfigureBayList(Rule<List<Bay>> rule)
        {
            rule.Define(() =>
            {
                List<Bay> retVal = new List<Bay>();

                if (ActiveChildParts.Value != null && ActiveChildParts.Value.Any())
                    retVal = ActiveChildParts.Value.Where(c =>
                    {
                        return (c.GetType() == typeof(Bay));
                    })
                    .Select(p => (Bay)p).ToList();

                return retVal;
            });
        }

        protected override void ConfigureAllowedChildTypes(ReadOnlyRule<IEnumerable<Type>> rule)
        {
            rule.Define(() => new List<Type>
            {
                typeof(Bay)
            });
        }

        #endregion

        #region Part Overrides

        public override bool IsCadPart => false;

        #endregion
    }
}
