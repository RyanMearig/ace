﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using Ruler.Rules;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class SelectiveLevel : Level
    {
        #region Constructors

        public SelectiveLevel(ModelContext modelContext, StorageConfiguratorRepository repo) : base(modelContext, repo) { }

        public SelectiveLevel(string name, Part parent, StorageConfiguratorRepository repo) : base(name, parent, repo) { }

        #endregion

        #region Rules



        #endregion

        #region Children

        public ShelfLoadBeam FrontLoadBeam { get; set; }
        protected virtual void ConfigureFrontLoadBeam(ShelfLoadBeam child)
        {
            child.IsActive.Define(() => !ProductOnGround.Value);
            child.ChannelSize.Define(FrontBeamSize);
            child.Width.Define(BayWidth);
            child.Orientation.Define(FrontBeamOrientation);
            child.BracketType.Define(FrontBeamBracketType);
            child.WeldDown.Define(FrontBeamWeldDown);
            child.HoleLocations.Define(BeamHoleLocations);
            child.HoleType.Define(FrontBeamHoleType);
            child.BoltType.Define(FrontBeamBoltType);
            child.HardwareSize.Define(FrontBeamHardwareSize);
            child.LoadsWide.Define(LoadsWide);
            child.Color.Define(FrontBeamColor);
        }

        public ShelfLoadBeam RearLoadBeam { get; set; }
        protected virtual void ConfigureRearLoadBeam(ShelfLoadBeam child)
        {
            child.IsActive.Define(() => !ProductOnGround.Value);
            child.ChannelSize.Define(RearBeamSize);
            child.Width.Define(BayWidth);
            child.Orientation.Define(RearBeamOrientation);
            child.BracketType.Define(RearBeamBracketType);
            child.WeldDown.Define(RearBeamWeldDown);
            child.HoleLocations.Define(BeamHoleLocations);
            child.HoleType.Define(RearBeamHoleType);
            child.BoltType.Define(RearBeamBoltType);
            child.HardwareSize.Define(RearBeamHardwareSize);
            child.LoadsWide.Define(LoadsWide);
            child.Color.Define(RearBeamColor);
            child.Position.Define(() =>
            {
                return Helper.Vxz(-FrontFrameDepth.Value, -child.Width.Value);
            });
            child.Rotation.Define(() => Helper.CreateRotation(Vector3.UnitY, 180));
        }

        public ChildList<ShelfPalletStop> PalletStops { get; set; }
        protected virtual void ConfigurePalletStops(ChildList<ShelfPalletStop> list)
        {
            list.Qty.Define(() => (ProductOnGround.Value || PalletStopStyle.Value == "NONE" ? 0 : PalletStopHoleLocations.Value.Count()));
            list.ConfigureChildren((child, index) =>
            {
                child.BeamSize.Define(RearBeamSize);
                child.Style.Define(PalletStopStyle);
                child.Width.Define(PalletStopWidth);
                child.Offset.Define(PalletStopOffset);
                child.Color.Define(PalletStopColor);
                child.BeamHoleSize.Define(RearLoadBeam.HoleSize);
                child.Position.Define(() => Helper.Vxyz(-FrontFrameDepth.Value, 0, -PalletStopHoleLocations.Value[index]));
            });
        }

        public ChildList<ShelfBeamCrossbar> Crossbars { get; set; }
        protected virtual void ConfigureCrossbars(ChildList<ShelfBeamCrossbar> list)
        {
            list.Qty.Define(() => (ProductOnGround.Value || PalletSupportType.Value == "none" ? 0 : PalletSupportLocations.Value.Count()));
            list.ConfigureChildren((child, index) =>
            {
                child.ChannelSize.Define(FrontBeamSize);
                child.FrontBeamOrientation.Define(FrontBeamOrientation);
                child.FrontBeamBoltType.Define(FrontBeamBoltType);
                child.FrontBeamHardwareSize.Define(FrontBeamHardwareSize);
                child.FrontBeamHoleType.Define(FrontBeamHoleType);
                child.RearBeamOrientation.Define(RearBeamOrientation);
                child.RearBeamBoltType.Define(RearBeamBoltType);
                child.RearBeamHardwareSize.Define(RearBeamHardwareSize);
                child.RearBeamHoleType.Define(RearBeamHoleType);
                child.Type.Define(PalletSupportType);
                child.RackDepth.Define(FrontFrameDepth);
                child.BeamSize.Define(this.FrontBeamSize);
                child.AngleType.Define(AngleType);
                child.Color.Define(PalletSupportColor);
                child.Position.Define(() =>
                {
                    float xOffset = 0;

                    if (FrontBeamOrientation.Value == "std")
                        xOffset = FrontBeamWebThickness.Value;
                    else
                        xOffset = FrontBeamDepth.Value;

                    return Helper.Vxyz(-xOffset, -1.5f, -PalletSupportLocations.Value[index]);
                });
            });
        }

        #endregion
    }
}
