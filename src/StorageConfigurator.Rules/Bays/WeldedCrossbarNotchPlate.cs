﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;
using Ruler.Cpq;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class WeldedCrossbarNotchPlate : FlatPlate, Ruler.Cpq.IBomPart
    {
        #region Constructors

        public WeldedCrossbarNotchPlate(ModelContext modelContext) : base(modelContext) { }

        public WeldedCrossbarNotchPlate(string name, Part parent) : base(name, parent) { }

        #endregion

        #region Rules


        #endregion

        #region CPQ.IBomPart Implementation

        protected override void ConfigureDescriptionPrefix(Rule<string> rule)
        {
            rule.Define("X bar Notch Plate");
        }

        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {
            child.PartNumber.Define(PartNumber);
            child.Description.Define(BOMUIDescription);
            child.MaterialPartNumber.Define(MaterialPartNumber);
            child.MaterialQty.Define(() => Length.Value / 12);
            child.ItemMaterialCost.Define(() => child.ItemRawMaterialWeight.Value * child.MaterialUnitCost.Value);
        }

        #endregion

        #region Material Rules

        public Rule<string> MaterialPartNumber { get; set; }
        protected virtual void ConfigureMaterialPartNumber(Rule<string> rule)
        {
            rule.Define(default(string));
        }

        #endregion

        #region Labor Rules

        public Rule<int> PartCarriersPerHour { get; set; }
        protected virtual void ConfigurePartCarriersPerHour(Rule<int> rule)
        {
            rule.Define(default(int));
        }

        #endregion

        #region Overrides

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureBomPartRules();
        }

        #endregion

    }
}
