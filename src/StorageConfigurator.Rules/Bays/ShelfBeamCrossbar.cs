﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ruler.Rules;
using Ruler.Rules.Graphics;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class ShelfBeamCrossbar : AdvancePart, IChannel, IAngle, IColorComponent, IRevisablePart
    {
        #region Constructors

        public ShelfBeamCrossbar(ModelContext modelContext, StorageConfiguratorRepository repo) : base(modelContext)
        {
            Repository = repo;
        }

        public ShelfBeamCrossbar(string name, Part parent, StorageConfiguratorRepository repo) : base(name, parent)
        {
            Repository = repo;
        }

        #endregion

        #region Rules

        public Rule<bool> IsStandalone { get; set; }
        protected virtual void ConfigureIsStandalone(Rule<bool> rule)
        {
            rule.Define(() => (Parent == null || Parent.GetType() != typeof(Level)));
        }

        public Rule<double> Qty { get; set; }
        protected virtual void ConfigureQty(Rule<double> rule)
        {
            rule.Define(1);
            rule.AddToUI(ui =>
            {
                ui.IsActive.Define(IsStandalone);
                ui.Label.Define("Quantity");
                ui.ReadOnly.Define(() => !IsStandalone.Value);
            });
        }

        public Rule<bool> IsDoubleDeep { get; set; }
        protected virtual void ConfigureIsDoubleDeep(Rule<bool> rule)
        {
            rule.Define(false);
            rule.AddToUI(ui =>
            {
                ui.IsActive.Define(IsStandalone);
            });
        }

        public Rule<string> Type { get; set; }
        protected virtual void ConfigureType(Rule<string> rule)
        {
            rule.Define(() =>
            {
                if (IsDoubleDeep.Value)
                    return "xbw-dd2";
                else
                {
                    if (ChannelSize.Value.ToUpper().Contains("C3") || ChannelSize.Value.ToUpper().Contains("C4"))
                        return "xb";
                    else
                        return "xbw";
                }
            });
            rule.AddToUI(ui =>
            {
                ui.IsActive.Define(IsStandalone);
                ui.ChoiceList.Define(() =>
                {
                    var cl = new List<ChoiceListItem>();

                    if ((ChannelSize.Value.ToUpper().Contains("C3") || ChannelSize.Value.ToUpper().Contains("C4") || ChannelSize.Value.ToUpper().Contains("C5")) 
                        && (FrontBeamOrientation.Value == "std" || RearBeamOrientation.Value == "std"))
                        cl.Add(new ChoiceListItem("xb", "XB"));

                    cl.Add(new ChoiceListItem("xbw", "XBW"));

                    if (IsDoubleDeep.Value)
                    {
                        cl.Add(new ChoiceListItem("xbw-dd1", "XBW-DD1"));
                        cl.Add(new ChoiceListItem("xbw-dd2", "XBW-DD2"));
                    }

                    cl.Add(new ChoiceListItem("xbwr1", "XBWR1"));
                    cl.Add(new ChoiceListItem("xbwr2", "XBWR2"));

                    return cl;
                });
            });
            rule.ValueSet += (sender, e) =>
            {
                AngleType.Reset();
            };
        }

        public Rule<float> RackDepth { get; set; }
        protected virtual void ConfigureRackDepth(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (IsDoubleDeep.Value)
                {
                    return FrontFrameDepth.Value + FrameTieLength.Value + RearFrameDepth.Value;
                }
                else
                {
                    return 20;
                }
            });
            rule.AddToUI(ui =>
            {
                ui.IsActive.Define(IsStandalone);
                ui.ReadOnly.Define(() => !IsStandalone.Value || Type.Value == "xbw-dd2");
            });
            rule.AddToCAD();
        }

        public Rule<string> FrontBeamOrientation { get; set; }
        protected virtual void ConfigureFrontBeamOrientation(Rule<string> rule)
        {
            rule.Define("std");
            rule.AddToUI(ui =>
            {
                ui.IsActive.Define(IsStandalone);
                ui.ChoiceList.Define(BeamOrientationChoices);
            });
        }

        public Rule<string> FrontBeamHoleType { get; set; }
        protected virtual void ConfigureFrontBeamHoleType(Rule<string> rule)
        {
            rule.Define(() => Type.Value == "xbwr2" ? "square" : "round");
            rule.AddToUI(ui =>
            {
                ui.IsActive.Define(IsStandalone);
                ui.ChoiceList.Define(() => HoleTypeChoices.Value);
            });
            rule.ValueSet += (sender, e) =>
            {
                if (!FrontBeamBoltType.UIMetadata.ChoiceList.Value.Any(c => c.Value.ToString() == FrontBeamBoltType.Value))
                {
                    FrontBeamBoltType.Reset();
                }

                if (rule.Value == "square" && FrontBeamHardwareSize.Value == "0.375")
                {
                    FrontBeamHardwareSize.Reset();
                }
            };
        }

        public Rule<string> FrontBeamBoltType { get; set; }
        protected virtual void ConfigureFrontBeamBoltType(Rule<string> rule)
        {
            rule.Define("Hex");
            rule.AddToUI(ui =>
            {
                ui.IsActive.Define(IsStandalone);
                ui.ChoiceList.Define(() =>
                {
                    return GetAvailableBoltTypes(FrontBeamHoleType.Value);
                });
            });
        }

        public Rule<string> FrontBeamHardwareSize { get; set; }
        protected virtual void ConfigureFrontBeamHardwareSize(Rule<string> rule)
        {
            rule.Define("0.5");
            rule.AddToUI(ui =>
            {
                ui.IsActive.Define(IsStandalone);
                ui.ChoiceList.Define(() =>
                {
                    if (FrontBeamHoleType.Value == "round")
                    {
                        return new List<ChoiceListItem>()
                        {
                            new ChoiceListItem("0.375", @"3/8"""),
                            new ChoiceListItem("0.5", @"1/2""")
                        };
                    }
                    return new List<ChoiceListItem>() { new ChoiceListItem("0.5", @"1/2""") };
                });
            });
        }

        public Rule<string> FrontInteriorBeamHoleType { get; set; }
        protected virtual void ConfigureFrontInteriorBeamHoleType(Rule<string> rule)
        {
            rule.Define("round");
            rule.AddToUI(ui =>
            {
                ui.IsActive.Define(() => IsStandalone.Value && DefaultBoltQty.Value > 2);
                ui.ChoiceList.Define(() => HoleTypeChoices.Value);
            });
            rule.ValueSet += (sender, e) =>
            {
                if (!FrontInteriorBeamBoltType.UIMetadata.ChoiceList.Value.Any(c => c.Value.ToString() == FrontInteriorBeamBoltType.Value))
                {
                    FrontInteriorBeamBoltType.Reset();
                }

                if (rule.Value == "square" && FrontInteriorBeamHardwareSize.Value == "0.375")
                {
                    FrontInteriorBeamHardwareSize.Reset();
                }
            };
        }

        public Rule<string> FrontInteriorBeamBoltType { get; set; }
        protected virtual void ConfigureFrontInteriorBeamBoltType(Rule<string> rule)
        {
            rule.Define("Hex");
            rule.AddToUI(ui =>
            {
                ui.IsActive.Define(() => IsStandalone.Value && DefaultBoltQty.Value > 2);
                ui.ChoiceList.Define(() =>
                {
                    return GetAvailableBoltTypes(FrontInteriorBeamHoleType.Value);
                });
            });
        }

        public Rule<string> FrontInteriorBeamHardwareSize { get; set; }
        protected virtual void ConfigureFrontInteriorBeamHardwareSize(Rule<string> rule)
        {
            rule.Define("0.5");
            rule.AddToUI(ui =>
            {
                ui.IsActive.Define(() => IsStandalone.Value && DefaultBoltQty.Value > 2);
                ui.ChoiceList.Define(() =>
                {
                    if (FrontInteriorBeamHoleType.Value == "round")
                    {
                        return new List<ChoiceListItem>()
                        {
                            new ChoiceListItem("0.375", @"3/8"""),
                            new ChoiceListItem("0.5", @"1/2""")
                        };
                    }
                    return new List<ChoiceListItem>() { new ChoiceListItem("0.5", @"1/2""") };
                });
            });
        }

        public Rule<string> RearInteriorBeamHoleType { get; set; }
        protected virtual void ConfigureRearInteriorBeamHoleType(Rule<string> rule)
        {
            rule.Define("round");
            rule.AddToUI(ui =>
            {
                ui.IsActive.Define(() => IsStandalone.Value && DefaultBoltQty.Value > 3);
                ui.ChoiceList.Define(() => HoleTypeChoices.Value);
            });
            rule.ValueSet += (sender, e) =>
            {
                if (!RearInteriorBeamBoltType.UIMetadata.ChoiceList.Value.Any(c => c.Value.ToString() == RearInteriorBeamBoltType.Value))
                {
                    RearInteriorBeamBoltType.Reset();
                }

                if (rule.Value == "square" && RearInteriorBeamHardwareSize.Value == "0.375")
                {
                    RearInteriorBeamHardwareSize.Reset();
                }
            };
        }

        public Rule<string> RearInteriorBeamBoltType { get; set; }
        protected virtual void ConfigureRearInteriorBeamBoltType(Rule<string> rule)
        {
            rule.Define("Hex");
            rule.AddToUI(ui =>
            {
                ui.IsActive.Define(() => IsStandalone.Value && DefaultBoltQty.Value > 3);
                ui.ChoiceList.Define(() =>
                {
                    return GetAvailableBoltTypes(RearInteriorBeamHoleType.Value);
                });
            });
        }

        public Rule<string> RearInteriorBeamHardwareSize { get; set; }
        protected virtual void ConfigureRearInteriorBeamHardwareSize(Rule<string> rule)
        {
            rule.Define("0.5");
            rule.AddToUI(ui =>
            {
                ui.IsActive.Define(() => IsStandalone.Value && DefaultBoltQty.Value > 3);
                ui.ChoiceList.Define(() =>
                {
                    if (RearInteriorBeamHoleType.Value == "round")
                    {
                        return new List<ChoiceListItem>()
                        {
                            new ChoiceListItem("0.375", @"3/8"""),
                            new ChoiceListItem("0.5", @"1/2""")
                        };
                    }
                    return new List<ChoiceListItem>() { new ChoiceListItem("0.5", @"1/2""") };
                });
            });
        }

        public Rule<string> RearBeamHoleType { get; set; }
        protected virtual void ConfigureRearBeamHoleType(Rule<string> rule)
        {
            rule.Define("round");
            rule.AddToUI(ui =>
            {
                ui.IsActive.Define(IsStandalone);
                ui.ChoiceList.Define(() => HoleTypeChoices.Value);
            });
            rule.ValueSet += (sender, e) =>
            {
                if (!RearBeamBoltType.UIMetadata.ChoiceList.Value.Any(c => c.Value.ToString() == RearBeamBoltType.Value))
                {
                    RearBeamBoltType.Reset();
                }

                if (rule.Value == "square" && RearBeamHardwareSize.Value == "0.375")
                {
                    RearBeamHardwareSize.Reset();
                }
            };
        }

        public Rule<string> RearBeamBoltType { get; set; }
        protected virtual void ConfigureRearBeamBoltType(Rule<string> rule)
        {
            rule.Define("Hex");
            rule.AddToUI(ui =>
            {
                ui.IsActive.Define(IsStandalone);
                ui.ChoiceList.Define(() =>
                {
                    return GetAvailableBoltTypes(RearBeamHoleType.Value);
                });
            });
        }

        public Rule<string> RearBeamHardwareSize { get; set; }
        protected virtual void ConfigureRearBeamHardwareSize(Rule<string> rule)
        {
            rule.Define("0.5");
            rule.AddToUI(ui =>
            {
                ui.IsActive.Define(IsStandalone);
                ui.ChoiceList.Define(() =>
                {
                    if (RearBeamHoleType.Value == "round")
                    {
                        return new List<ChoiceListItem>()
                        {
                            new ChoiceListItem("0.375", @"3/8"""),
                            new ChoiceListItem("0.5", @"1/2""")
                        };
                    }
                    return new List<ChoiceListItem>() { new ChoiceListItem("0.5", @"1/2""") };
                });
            });
        }

        public Rule<string> RearBeamOrientation { get; set; }
        protected virtual void ConfigureRearBeamOrientation(Rule<string> rule)
        {
            rule.Define("std");
            rule.AddToUI(ui =>
            {
                ui.IsActive.Define(IsStandalone);
                ui.ChoiceList.Define(BeamOrientationChoices);
            });
        }

        public ReadOnlyRule<float> Length { get; set; }
        protected virtual void ConfigureLength(ReadOnlyRule<float> rule)
        {
            rule.Define(() =>
            {
                float retVal = RackDepth.Value;
                var offsetPerSide = 0.0625f;

                if (FrontBeamOrientation.Value == "std")
                {
                    retVal = retVal - (ChannelWebThickness.Value + offsetPerSide);
                }
                else
                {
                    retVal = retVal - (ChannelDepth.Value + offsetPerSide);
                }

                if (RearBeamOrientation.Value == "std")
                {
                    retVal = retVal - (ChannelWebThickness.Value + offsetPerSide);
                }
                else
                {
                    retVal = retVal - (ChannelDepth.Value + offsetPerSide);
                }

                return retVal;

                //if (BeamOrientation.Value == "std")
                //    return Helper.RoundDownToNearest((RackDepth.Value - (ChannelWebThickness.Value * 2) - 0.0625f), 0.0625f);
                //else
                //    return Helper.RoundDownToNearest(RackDepth.Value - (BeamOffset.Value * 2) - .125f, 0.0625f);
            });
        }

        public Rule<List<ChoiceListItem>> HoleTypeChoices { get; set; }
        protected virtual void ConfigureHoleTypeChoices(Rule<List<ChoiceListItem>> rule)
        {
            rule.Define(() =>
            {
                return new List<ChoiceListItem>()
                {
                    new ChoiceListItem("round", "Round"),
                    new ChoiceListItem("square", "Square")
                };
            });
        }

        public Rule<string> HoleType { get; set; }
        protected virtual void ConfigureHoleType(Rule<string> rule)
        {
            rule.Define(FrontBeamHoleType);
            //rule.AddToUI(ui =>
            //{
            //    ui.IsActive.Define(IsStandalone);
            //    ui.ChoiceList.Define(() => HoleTypeChoices.Value);
            //});
            //rule.ValueSet += (sender, e) =>
            //{
            //    if (!AvailableBoltTypes.Value.Contains(BoltType.Value))
            //    {
            //        BoltType.Reset();
            //    }

            //    if (HoleType.Value == "square" && HardwareSize.Value == "0.375")
            //    {
            //        HardwareSize.Reset();
            //    }
            //};
        }

        public ReadOnlyRule<float> HoleSize { get; set; }
        protected virtual void ConfigureHoleSize(ReadOnlyRule<float> rule)
        {
            rule.Define(() =>
            {
                if (FrontBeamHardwareSize.Value == "0.375")
                {
                    return 0.4375f;
                }
                else if (HoleType.Value == "round")
                {
                    return 0.5625f;
                }
                return 0.53125f;
            });
            rule.AddToUI(ui =>
            {
                ui.IsActive.Define(IsStandalone);
                //ui.ChoiceList.Define(() =>
                //{
                //    if (HoleType.Value == "round")
                //    {
                //        return new List<ChoiceListItem>()
                //        {
                //            new ChoiceListItem(0.4375, @"7/16"),
                //            new ChoiceListItem(0.5625f, @"9/16")
                //        };
                //    }
                //    else
                //    {
                //        return new List<ChoiceListItem>()
                //        {
                //            new ChoiceListItem(0.53125f, @"17/32")
                //        };
                //    }
                //});
            });
            rule.AddToCAD();
        }

        public List<ChoiceListItem> BeamOrientationChoices
        {
            get
            {
                return new List<ChoiceListItem>()
                {
                    new ChoiceListItem("std", "Standard"),
                    new ChoiceListItem("rev", "Reversed")
                };
            }
        }

		public Rule<float> FrontFrameDepth { get; set; }
        protected virtual void ConfigureFrontFrameDepth(Rule<float> rule)
        {
            rule.Define(44);
            rule.AddToUI(ui =>
            {
                ui.IsActive.Define(IsStandalone);
                ui.ReadOnly.Define(() => !(IsStandalone.Value && IsDoubleDeep.Value));
            });
        }

        public Rule<float> FrameTieLength { get; set; }
        protected virtual void ConfigureFrameTieLength(Rule<float> rule)
        {
            rule.Define(8);
            rule.AddToUI(ui =>
            {
                ui.IsActive.Define(IsStandalone);
                ui.ReadOnly.Define(() => RearFrameDepth.UIMetadata.ReadOnly.Value);
            });
        }

        public Rule<float> RearFrameDepth { get; set; }
        protected virtual void ConfigureRearFrameDepth(Rule<float> rule)
        {
            rule.Define(44);
            rule.AddToUI(ui =>
            {
                ui.IsActive.Define(IsStandalone);
                ui.ReadOnly.Define(() => !(IsStandalone.Value && Type.Value == "xbw-dd2"));
            });
        }

        public Rule<string> Notch1BeamOrientation { get; set; }
        protected virtual void ConfigureNotch1BeamOrientation(Rule<string> rule)
        {
            rule.Define("std");
            rule.AddToUI(ui =>
            {
                ui.IsActive.Define(IsStandalone);
                ui.ReadOnly.Define(() => !Type.Value.Contains("xbw-dd"));
                ui.ChoiceList.Define(BeamOrientationChoices);
            });
        }

        public Rule<string> Notch2BeamOrientation { get; set; }
        protected virtual void ConfigureNotch2BeamOrientation(Rule<string> rule)
        {
            rule.Define("std");
            rule.AddToUI(ui =>
            {
                ui.IsActive.Define(IsStandalone);
                ui.ReadOnly.Define(() => RearFrameDepth.UIMetadata.ReadOnly.Value);
                ui.ChoiceList.Define(BeamOrientationChoices);
            });
        }

        public Rule<float> RackDepthDiffToCutLength { get; set; }
        protected virtual void ConfigureRackDepthDiffToCutLength(Rule<float> rule)
        {
            rule.Define(() => (RackDepth.Value - Length.Value));
        }

        //This will be used to take the Notch location from Level location to Crossbar location
        public Rule<float> NotchLocationSubtraction { get; set; }
        protected virtual void ConfigureNotchLocationSubtraction(Rule<float> rule)
        {
            rule.Define(() => (RackDepthDiffToCutLength.Value/ 2));
        }

        public Rule<string> BeamSize { get; set; }
        protected virtual void ConfigureBeamSize(Rule<string> rule)
        {
            rule.Define(default(string));
        }

        public Rule<int> DefaultBoltQty { get; set; }
        protected virtual void ConfigureDefaultBoltQty(Rule<int> rule)
        {
            rule.Define(() =>
            {
                if (Type.Value == "xbw-dd1")
                {
                    return 3;
                }
                else if (Type.Value == "xbw-dd2")
                {
                    return 4;
                }
                else
                {
                    return 2;
                }
            });
        }

        public Rule<List<BoltDefinition>> BoltDefinitions { get; set; }
        protected virtual void ConfigureBoltDefinitions(Rule<List<BoltDefinition>> rule)
        {
            rule.Define(() =>
            {
                var boltData = new List<(float, string)>
                {
                    (float.Parse(FrontBeamHardwareSize.Value), Helper.GetUiDescription(FrontBeamBoltType)),
                    (float.Parse(RearBeamHardwareSize.Value), Helper.GetUiDescription(RearBeamBoltType))
                };
                if (DefaultBoltQty.Value > 2)
                {
                    boltData.Add((float.Parse(FrontInteriorBeamHardwareSize.Value), Helper.GetUiDescription(FrontInteriorBeamBoltType)));
                }
                if (DefaultBoltQty.Value > 3)
                {
                    boltData.Add((float.Parse(RearInteriorBeamHardwareSize.Value), Helper.GetUiDescription(RearInteriorBeamBoltType)));
                }

                return boltData?.GroupBy(g => new { g.Item1, g.Item2 })?
                                .Select(d =>
                                {
                                    return new BoltDefinition()
                                    {
                                        BoltSize = d.First().Item1,
                                        BoltType = d.First().Item2,
                                        Length = 1,
                                        Qty = d.Count()
                                    };
                                })?
                                .ToList() ?? new List<BoltDefinition>();
            });
        }

        #endregion

        #region IChannel Implementation

        public Rule<List<ChannelData>> AllChannelData { get; set; }
        public Rule<List<ChoiceListItem>> AllChannelSizesChoiceList { get; set; }
        public Rule<ChannelData> ActiveChannelData { get; set; }
        public Rule<float> ChannelWidth { get; set; }
        public Rule<float> ChannelDepth { get; set; }
        public Rule<float> ChannelWebThickness { get; set; }
        public Rule<float> ChannelWeightPerFt { get; set; }
        public Rule<List<string>> ChannelSizesAvailable { get; set; }
        protected virtual void ConfigureChannelSizesAvailable(Rule<List<string>> rule)
        {
            rule.Define(() => (new List<string> { "C3x3.5", "C3x4.1", "C4x4.5", "C4x5.4", "C5x6.1", "C5x6.7", "C6x8.2", "C7x9.8", "C8x11.5", "C10x15.3" }));
        }

        public Rule<string> ChannelSize { get; set; }
        protected virtual void ConfigureChannelSize(Rule<string> rule)
        {
            rule.Define("C4x4.5");
            rule.AddToUI(ui =>
            {
                ui.IsActive.Define(IsStandalone);
                ui.Label.Define("Beam Size");
                ui.ChoiceList.Define(() => Helper.GetFilteredChoices(AllChannelSizesChoiceList.Value, ChannelSizesAvailable.Value));
            });
        }
        public Rule<string> MaterialPartNumber { get; set; }

        #endregion

        #region IAngleData Implementation
        public Rule<List<AngleData>> AllAngleData { get; set; }
        public Rule<List<ChoiceListItem>> AllAngleTypesChoiceList { get; set; }
        public Rule<AngleData> ActiveAngleData { get; set; }
        public Rule<List<string>> AngleSizesAvailable { get; set; }
        protected virtual void ConfigureAngleSizesAvailable(Rule<List<string>> rule)
        {
            rule.Define(() =>
            {
                List<string> choices = new List<string>();

                choices.Add(@"1-1/2 x 1-1/2 x 1/8");
                choices.Add(@"2 x 2 x 1/8");

                if (Type.Value != "xb")
                {
                    choices.Add(@"2 x 2 x 3/16");
                    choices.Add(@"3 x 2 x 3/16");
                    choices.Add(@"2-1/2 x 2-1/2 x 3/16");
                }

                if (Type.Value == "xbw" || Type.Value == "xbw-dd1")
                    choices.Add(@"3 x 2 x 1/4");

                return choices;
            });
        }

        public Rule<string> AngleType { get; set; }
        protected virtual void ConfigureAngleType(Rule<string> rule)
        {
            rule.Define(() =>
            {
                if (Type.Value == "xb")
                    return @"1-1/2 x 1-1/2 x 1/8";
                else
                    return @"2 x 2 x 3/16";
            });
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() => Helper.GetFilteredChoices(AllAngleTypesChoiceList.Value, AngleSizesAvailable.Value));
            });
        }
        public Rule<float> AngleWidth { get; set; }
        public Rule<float> AngleHeight { get; set; }
        public Rule<float> AngleThickness { get; set; }

        #endregion

        #region IColorComponent Implementation

        public Rule<string> Color { get; set; }
        public ReadOnlyRule<AdvanceColor> AdvanceColor { get; set; }
        public ReadOnlyRule<string> ColorDescription { get; set; }
        public StorageConfiguratorRepository Repository { get; set; }
        public Rule<string> ColorBOMName { get; set; }
        public ReadOnlyRule<string> ColorShortDescription { get; set; }

        public Rule<double> PartWeight { get; set; }
        public Rule<double> PartPaintWeight { get; set; }
        public Rule<float> WeightPerGallon { get; set; }
        public CsiRule<double> PaintCostPerGallon { get; set; }
        public Rule<float> PaintCost { get; set; }

        #endregion

        #region IRevisablePartImplementation

        public FactoryRevisionRule<string> FactoryRevision { get; set; }

        #endregion

        #region Public Methods

        public IEnumerable<ChoiceListItem> GetAvailableBoltTypes(string holeType)
        {
            var availTypes = holeType == "round" ? new List<string>() { "Hex" } : new List<string>() { "Hex", "Carriage" };
            var props = typeof(BoltTypes).GetFields().Where(b => availTypes.Contains(b.Name));
            return props?.Select(p => new ChoiceListItem(p.Name, p.GetValue(null).ToString())) ?? new List<ChoiceListItem>();
        }

        #endregion

        #region Children

        public XBCrossbar XBCrossbar { get; set; }
        protected virtual void ConfigureXBCrossbar(XBCrossbar child)
        {
            child.IsActive.Define(() => Type.Value == "xb");
            child.AngleType.Define(AngleType);
            child.OverallLength.Define(Length);
            child.BeamSize.Define(BeamSize);
            child.HoleType.Define(HoleType);
            child.HoleSize.Define(HoleSize);
            child.IsStandalone.Define(IsStandalone);
            child.Qty.Define(Qty);
            child.Position.Define(() => Helper.Vy(child.VertOffsetToHole.Value));
        }

        public WeldedCrossbar WeldedCrossbar { get; set; }
        protected virtual void ConfigureWeldedCrossbar(WeldedCrossbar child)
        {
            child.IsActive.Define(() => Type.Value != "xb");
            child.ChannelSize.Define(ChannelSize);
            child.AngleType.Define(AngleType);
            child.OverallLength.Define(Length);
            child.HoleType.Define(HoleType);
            child.HoleSize.Define(HoleSize);
            child.Type.Define(Type);
            child.BeamSize.Define(BeamSize);
            child.RackDepth.Define(RackDepth);
            child.FrontFrameDepth.Define(FrontFrameDepth);
            child.Notch1BeamOrientation.Define(Notch1BeamOrientation);
            child.FrameTieLength.Define(FrameTieLength);
            child.RearFrameDepth.Define(RearFrameDepth);
            child.Notch2BeamOrientation.Define(Notch2BeamOrientation);
            child.IsStandalone.Define(IsStandalone);
            child.Qty.Define(Qty);
        }

        public ChildList<HardwareGroup> Hardwares { get; set; }
        protected virtual void ConfigureHardwares(ChildList<HardwareGroup> list)
        {
            list.Qty.Define(() => BoltDefinitions.Value.Count);
            list.ConfigureChildren((child, index) =>
            {
                child.BoltSize.Define(() => BoltDefinitions.Value[index].BoltSize);
                child.BoltLength.Define(() => BoltDefinitions.Value[index].Length);
                child.BoltType.Define(() => BoltDefinitions.Value[index].BoltType);
                child.BoltMaterial.Define(() => BoltDefinitions.Value[index].BoltMaterial);
                child.Qty.Define(() => BoltDefinitions.Value[index].Qty);
            });
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(Colors.Blue);
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(IsStandalone);
        }

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define("ShelfBeamCrossbar");
        }

        protected override void ConfigureAllowedChildTypes(ReadOnlyRule<IEnumerable<Type>> rule)
        {
            rule.Define(() => new List<Type>
            {
                typeof(CustomPart)
            });
        }

        #endregion

        #region Graphics

        //public Block CrossbarBlock { get; set; }
        //protected virtual void ConfigureCrossbarBlock(Block geo)
        //{
        //    geo.Width.Define(Length);
        //    geo.Height.Define(AngleHeight);
        //    geo.Depth.Define(AngleWidth);
        //    geo.Position.Define(() => Helper.Vxy(-Length.Value / 2, 0));
        //}

        //public Block Notch1Block { get; set; }
        //protected virtual void ConfigureNotch1Block(Block geo)
        //{
        //    geo.Width.Define(Notch1Width);
        //    geo.Height.Define(3);
        //    geo.Depth.Define(1);
        //    geo.Position.Define(() => Helper.Vx(-(Notch1Location.Value )));
        //}

        //public UCS UCS { get; set; }
        //protected virtual void ConfigureUCS(UCS child)
        //{
        //    child.Diameter.Define(0.25f);
        //    child.Position.Define(() => (Helper.Vy(0)));
        //}

        #endregion

        #region Protected Methods

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureChannelRules();
            this.ConfigureAngleRules();
            this.ConfigureRevisablePart();
            this.ConfigureColorComponent();
        }

        #endregion

    }
}
