﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using Ruler.Rules;
using Ruler.Rules.Graphics;
using Ruler.Rules.Validation;
using StorageConfigurator.Data;
using Ruler.Cpq;

namespace StorageConfigurator.Rules
{
    public class ShelfLoadBeam : PaintedPart, IShelfLoadBeamInputs, IChannel, IRevisablePart, Ruler.Cpq.IBomPart
    {
        #region Constructors

        public ShelfLoadBeam(ModelContext modelContext, StorageConfiguratorRepository repo) : base(modelContext)
        {
            Repository = repo;
        }

        public ShelfLoadBeam(string name, Part parent, StorageConfiguratorRepository repo) : base(name, parent)
        {
            Repository = repo;
        }

        #endregion

        #region Rules

        public Rule<bool> IsStandalone { get; set; }
        protected virtual void ConfigureIsStandalone(Rule<bool> rule)
        {
            rule.Define(() => (Parent == null || Parent?.GetType() == typeof(ShippableParts)));
        }

        public Rule<string> ProductFamily { get; set; }
        protected virtual void ConfigureProductFamily(Rule<string> rule)
        {
            rule.Define("selective");
            rule.AddToUI(ui =>
            {
                ui.IsActive.Define(IsStandalone);
                ui.ChoiceList.Define(new List<ChoiceListItem>()
                {
                    new ChoiceListItem("selective", "Selective")
                    //new ChoiceListItem("pushback", "Pushback")
                });
                ui.ReadOnly.Define(() => (this.Parent != null && this.Parent.TypeName != "ShippableParts"));
            });
        }

        public Rule<double> Qty { get; set; }
        protected virtual void ConfigureQty(Rule<double> rule)
        {
            rule.Define(1);
            rule.AddToUI(ui =>
            {
                ui.IsActive.Define(IsStandalone);
                ui.Label.Define("Quantity");
                //ui.ReadOnly.Define(() => !IsStandalone.Value);
            });
        }

        [CadRule(CadRuleUsageType.Parameter, "Depth")]
        public Rule<float> Width { get; set; }
        protected virtual void ConfigureWidth(Rule<float> rule)
        {
            rule.Define(96);
            rule.Min(12);
            rule.Max(200);
            rule.AddToUI(ui =>
            {
                ui.IsActive.Define(IsStandalone);
            });
        }

        public Rule<float> ActualWidth { get; set; }
        protected virtual void ConfigureActualWidth(Rule<float> rule)
        {
            rule.Define(() => Width.Value - Offset.Value);
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> Orientation { get; set; }
        protected virtual void ConfigureOrientation(Rule<string> rule)
        {
            rule.Define("std");
            rule.AddToUI(ui =>
            {
                ui.IsActive.Define(IsStandalone);
                ui.ChoiceList.Define(() => OrientationChoiceList.Value);
            });
        }

        public Rule<string> BracketType { get; set; }
        protected virtual void ConfigureBracketType(Rule<string> rule)
        {
            rule.Define(() =>
            {
                float topBracketToBtmBeamDist = WeldDown.Value + ChannelWidth.Value;

                if (topBracketToBtmBeamDist <= 7.875f)
                    return "8";
                else if (topBracketToBtmBeamDist <= 8.875f)
                    return "9";
                else if (topBracketToBtmBeamDist <= 11.875f)
                    return "12";
                else
                    return "13";
            });
            rule.AddToUI(ui =>
            {
                ui.IsActive.Define(IsStandalone);
                ui.ChoiceList.Define(() => BracketTypeChoiceList.Value);
            });
            rule.ValueSet += (sender, e) =>
            {
                if (WeldDown.Value > WeldDownMax.Value)
                    WeldDown.Reset();
            };
        }

        [CadRule(CadRuleUsageType.Property, "BracketHeight")]
        public Rule<float> BracketSelectedHeight { get; set; }
        protected virtual void ConfigureBracketSelectedHeight(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (BracketType.Value.Contains("8"))
                {
                    return 8;
                }
                else if (BracketType.Value.Contains("9"))
                {
                    return 9;
                }
                else if (BracketType.Value.Contains("12"))
                {
                    return 12;
                }
                else
                {
                    return 13;
                }
            });
        }

        public Rule<float> WeldDown { get; set; }
        protected virtual void ConfigureWeldDown(Rule<float> rule)
        {
            rule.Define(0.4375f);
            rule.Min(WeldDownMin);
            rule.Max(WeldDownMax);
            rule.AddToUI(ui =>
            {
                ui.IsActive.Define(IsStandalone);
            });
        }

        public Rule<float> WeldDownMin { get; set; }
        protected virtual void ConfigureWeldDownMin(Rule<float> rule)
        {
            rule.Define(() => Orientation.Value == "std" ? 0.125f : 0);
        }

        public Rule<float> WeldDownMax { get; set; }
        protected virtual void ConfigureWeldDownMax(Rule<float> rule)
        {
            rule.Define(() =>
            {
                float minExtensionOfClipOverBeam = Orientation.Value == "std" ? 0.125f : 0;
                return (BracketCutHeight.Value - BeamHeight.Value) - minExtensionOfClipOverBeam;
            });
        }

        public Rule<float> BracketCutHeight { get; set; }
        protected virtual void ConfigureBracketCutHeight(Rule<float> rule)
        {
            rule.Define(() => LeftEndBracket.Height.Value);
        }

        [CadRule(CadRuleUsageType.Parameter, "Height")]
        public Rule<float> BeamHeight { get; set; }
        protected virtual void ConfigureBeamHeight(Rule<float> rule)
        {
            rule.Define(() => Beam.ChannelWidth.Value);
        }

        public Rule<float> Offset { get; set; }
        protected virtual void ConfigureOffset(Rule<float> rule)
        {
            rule.Define(() => (Orientation.Value == "std" ? 0 : 1.625f));
            rule.AddToUI(ui =>
            {
                ui.IsActive.Define(IsStandalone);
            });
        }

        public Rule<string> HoleType { get; set; }
        protected virtual void ConfigureHoleType(Rule<string> rule)
        {
            rule.Define("round");
            rule.AddToUI(ui =>
            {
                //ui.IsActive.Define(IsStandalone);
                ui.ChoiceList.Define(new List<ChoiceListItem>()
                {
                    new ChoiceListItem("round", "Round"),
                    new ChoiceListItem("square", "Square")
                });
            });
            rule.ValueSet += (sender, e) =>
            {
                if (!AvailableBoltTypes.Value.Contains(BoltType.Value))
                {
                    BoltType.Reset();
                }

                if (HoleType.Value == "square" && HardwareSize.Value == "0.375")
                {
                    HardwareSize.Reset();
                }
            };
        }

        public Rule<List<string>> AvailableBoltTypes { get; set; }
        protected virtual void ConfigureAvailableBoltTypes(Rule<List<string>> rule)
        {
            rule.Define(() =>
            {
                if (HoleType.Value == "round")
                {
                    return new List<string>() { "Hex" };
                }
                return new List<string>() { "Hex", "Carriage"};
            });
        }

        public Rule<string> BoltType { get; set; }
        protected virtual void ConfigureBoltType(Rule<string> rule)
        {
            rule.Define("Hex");
            rule.AddToUI(ui =>
            {
                //ui.IsActive.Define(IsStandalone);
                ui.Label.Define("Bolt Type");
                ui.ChoiceList.Define(() =>
                {
                    var props = typeof(BoltTypes).GetFields().Where(b => AvailableBoltTypes.Value.Contains(b.Name));
                    return props?.Select(p => new ChoiceListItem(p.Name, p.GetValue(null).ToString())) ?? new List<ChoiceListItem>();
                });
            });
        }

        public Rule<string> HardwareSize { get; set; }
        protected virtual void ConfigureHardwareSize(Rule<string> rule)
        {
            rule.Define("0.5");
            rule.AddToUI(ui =>
            {
                ui.IsActive.Define(IsStandalone);
                ui.ChoiceList.Define(() =>
                {
                    if (HoleType.Value == "round")
                    {
                        return new List<ChoiceListItem>()
                        {
                            new ChoiceListItem("0.375", @"3/8"""),
                            new ChoiceListItem("0.5", @"1/2""")
                        };
                    }
                    return new List<ChoiceListItem>() { new ChoiceListItem("0.5", @"1/2""") };
                });
            });
        }

        public ReadOnlyRule<float> HoleSize { get; set; }
        protected virtual void ConfigureHoleSize(ReadOnlyRule<float> rule)
        {
            rule.Define(() =>
            {
                if (HardwareSize.Value == "0.375")
                {
                    return 0.4375f;
                }
                else if (HoleType.Value == "round")
                {
                    return 0.5625f;
                }
                return 0.53125f;
            });
            rule.AddToUI(ui =>
            {
                ui.IsActive.Define(IsStandalone);
                //ui.ChoiceList.Define(new List<ChoiceListItem>()
                //{
                //    new ChoiceListItem(0.4375, @"7/16"""),
                //    new ChoiceListItem(0.5625, @"9/16"""),
                //    new ChoiceListItem(0.53125, @"17/32""")
                //});
            });
        }

        //TODO: Get info on Hole locations
        public Rule<List<float>> HoleLocations { get; set; }
        protected virtual void ConfigureHoleLocations(Rule<List<float>> rule)
        {
            rule.Define(default(List<float>));
        }

        public Rule<List<float>> HoleLocationsonBeam { get; set; }
        protected virtual void ConfigureHoleLocationsonBeam(Rule<List<float>> rule)
        {
            rule.Define(() =>
            {
                if (HoleLocations.Value != null && HoleLocations.Value.Any())
                {
                    float diffBtwnLoadBeamandCutBeam = ((Width.Value - BeamCutLength.Value) / 2);

                    return HoleLocations.Value.Select(h => (h - diffBtwnLoadBeamandCutBeam)).ToList();
                }
                else if (IsStandalone.Value)
                {
                    List<float> locs = new List<float>();

                    for (int i = 1; i <= LoadsWide.Value; i++)
                    {
                        float palletZLoc = ProductZLocations.Value[i - 1] + LoadOverlap.Value;
                        //frontPosition
                        locs.Add(palletZLoc + SupportOffsetFromPallet.Value);
                        //midPosition
                        locs.Add(palletZLoc + (PalletWidth.Value * 0.5f));
                        //rearPosition
                        locs.Add(palletZLoc + (PalletWidth.Value - SupportOffsetFromPallet.Value));
                    }

                    return locs;
                }

                return new List<float>();
            });
        }

        public Rule<List<float>> ProductZLocations { get; set; }
        protected virtual void ConfigureProductZLocations(Rule<List<float>> rule)
        {
            rule.Define(() =>
            {
                List<float> retList = new List<float>();
                float currentZLoc = 0;

                for (int i = 1; i <= LoadsWide.Value; i++)
                {
                    if (i == 1)
                        currentZLoc = LoadToFrameOffset.Value;
                    else
                        currentZLoc = currentZLoc + LoadWidth.Value + LoadToLoadOffset.Value;

                    retList.Add(currentZLoc);
                }

                return retList;
            });
        }

        public Rule<float> ClipToColumnGap { get; set; }
        protected virtual void ConfigureClipToColumnGap(Rule<float> rule)
        {
            rule.Define(0.195f);
        }

        public Rule<float> BeamCutOffset { get; set; }
        protected virtual void ConfigureBeamCutOffset(Rule<float> rule)
        {
            rule.Define(0.0625f);
        }

        public Rule<float> BeamLength { get; set; }
        protected virtual void ConfigureBeamLength(Rule<float> rule)
        {
            rule.Define(() => (Width.Value - ((LeftEndBracket.Thickness.Value * 2) + (ClipToColumnGap.Value*2))));
        }

        public Rule<float> BeamCutLength { get; set; }
        protected virtual void ConfigureBeamCutLength(Rule<float> rule)
        {
            rule.Define(() => (BeamLength.Value - BeamCutOffset.Value));
        }


        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> rulerDrawingXml { get; set; }
        protected virtual void ConfigurerulerDrawingXml(Rule<string> rule)
        {
            rule.Define("LoadBeam");
        }

        [CadRule(CadRuleUsageType.Property)]
        public ReadOnlyRule<int> HoleQty { get; set; }
        protected virtual void ConfigureHoleQty(ReadOnlyRule<int> rule)
        {
            rule.Define(() => HoleLocationsonBeam.Value?.Count ?? 0);
        }

        public Rule<string> BasePartNumber { get; set; }
        protected virtual void ConfigureBasePartNumber(Rule<string> rule)
        {
            rule.Define(() => $"{(Orientation.Value == "std" ? "" : "R")}B{(BracketType.Value == "8infAdj" ? "I" : "")}{Beam.ChannelId.Value}{LoadsWide.Value}S");
        }

        [CadRule(CadRuleUsageType.Property)]
        public ReadOnlyRule<string> DrawingNumber { get; set; }
        protected virtual void ConfigureDrawingNumber(ReadOnlyRule<string> rule)
        {
            rule.Define(BasePartNumber);
        }

        //This is the description used only by the BOM Summary.
        public Rule<string> BOMUIDescription { get; set; }
        protected virtual void ConfigureBOMUIDescription(Rule<string> rule)
        {
            rule.Define(() => @$"{BasePartNumber.Value} ({Helper.fractionString(Width.Value, feetToo: false)}, {BracketSelectedHeight.Value}, {Helper.fractionString(WeldDown.Value, feetToo: false)})");
        }

        #endregion

        #region ILoadBeamData Implementation
        public Rule<List<string>> BeamSizesAvailable { get; set; }
        public Rule<List<ChoiceListItem>> LoadBeamSizeChoiceList { get; set; }
        public Rule<List<ChoiceListItem>> OrientationChoiceList { get; set; }
        public Rule<List<ChoiceListItem>> BracketTypeChoiceList { get; set; }
        #endregion

        #region IChannel Implementation

        public Rule<List<ChannelData>> AllChannelData { get; set; }
        public Rule<List<ChoiceListItem>> AllChannelSizesChoiceList { get; set; }
        public Rule<ChannelData> ActiveChannelData { get; set; }
        public Rule<float> ChannelWidth { get; set; }
        public Rule<float> ChannelDepth { get; set; }
        public Rule<float> ChannelWebThickness { get; set; }
        public Rule<float> ChannelWeightPerFt { get; set; }
        public Rule<List<string>> ChannelSizesAvailable { get; set; }
        protected virtual void ConfigureChannelSizesAvailable(Rule<List<string>> rule)
        {
            rule.Define(() => (new List<string> { "C3x3.5", "C3x4.1", "C4x4.5", "C4x5.4", "C5x6.1", "C5x6.7", "C6x8.2", "C7x9.8", "C8x11.5", "C10x15.3" }));
        }

        public Rule<string> ChannelSize { get; set; }
        protected virtual void ConfigureChannelSize(Rule<string> rule)
        {
            rule.Define("C4x4.5");
            rule.AddToUI(ui =>
            {
                ui.IsActive.Define(IsStandalone);
                ui.Label.Define("Beam Size");
                ui.ChoiceList.Define(() => LoadBeamSizeChoiceList.Value);
            });
        }
        public Rule<string> MaterialPartNumber { get; set; }

        #endregion

        #region IShelfLoadBeamInputs Implementation

        public Rule<float> PalletWidth { get; set; }
        public Rule<float> LoadWidth { get; set; }
        public Rule<float> LoadOverlap { get; set; }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<int> LoadsWide { get; set; }
        public Rule<float> LoadToFrameOffset { get; set; }
        protected virtual void ConfigureLoadToFrameOffset(Rule<float> rule)
        {
            rule.Define(() =>
            {
                float totalConsumedSpace = ((LoadWidth.Value * LoadsWide.Value) + (LoadToLoadOffset.Value * (LoadsWide.Value - 1)));
                return ((Width.Value - totalConsumedSpace)/2);
            });
            rule.AddToUI(ui =>
            {
                ui.IsActive.Define(IsStandalone);
                ui.ReadOnly.Define(true);
            });
        }

        public Rule<float> LoadToFrameOffsetMin { get; set; }
        public Rule<float> LoadToLoadOffset { get; set; }
        public Rule<float> LoadToLoadOffsetMin { get; set; }
        public Rule<float> SupportOffsetFromPallet { get; set; }

        #endregion

        #region IBomPart Implementation

        protected virtual void ConfigurePartNumberPrefix(Rule<string> rule)
        {
            rule.Define(() => $"{BasePartNumber.Value}-");
        }

        protected virtual void ConfigureDescription(Rule<string> rule)
        {
            rule.Define("");
        }

        protected virtual void ConfigureBOMDescription(Rule<string> rule)
        {
            rule.Define(() =>
            {
                var slottedClips = BracketType.Value == "8infAdj";

                var desc = "";
                if(slottedClips)
                {
                    desc = $"{ChannelSize.Value}# Beam, {BeamLength.Value}\" long";
                    desc += $"|8\" Slotted Clips, {WeldDown.Value:#.###}\" drop ";
                }
                else
                {
                    desc = $"Load Beam, selective, {BracketSelectedHeight.Value}\" clips";
                    desc += $"|{ChannelSize.Value}x{BeamLength.Value}\" long, standard, {WeldDown.Value:#.###}\" drop";
                }
                desc += $"|{HoleSize.Value}\" holes";
                desc += $"|{ColorBOMName.Value}";
                return desc;

                //var retVal = $"{DrawingNumber.Value} ({Width.Value}\",{BracketSelectedHeight.Value}\",{Helper.fractionString(HoleDiameter.Value)})";

                //retVal = retVal + $"|Load Beam, selective, {BracketSelectedHeight.Value}\" clips";
                //retVal = retVal + $"|{ChannelSize.Value}x{Width.Value}\" long, standard";
                //retVal = retVal + $"|{Helper.fractionString(HoleDiameter.Value)} holes";
                //retVal = retVal + $"|GET MORE INFO";

                //return retVal;
            });
        }
        
        protected virtual void ConfigurePartWeight(Rule<double> rule)
        {
            rule.Define(() => this.GetPartWeightWithoutPaint());
        }

        #endregion

        #region IRevisablePartImplementation

        public FactoryRevisionRule<string> FactoryRevision { get; set; }

        #endregion

        #region CPQ.IBomPart Implementation

        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {
            child.PartNumber.Define(PartNumber);
            child.Description.Define(BOMUIDescription);
            child.ItemQty.Define(Qty);
        }

        #endregion

        #region Labor Hour Rules

        public Rule<float> WeldFactor { get; set; }
        protected virtual void ConfigureWeldFactor(Rule<float> rule)
        {
            rule.Define(() => 1 / WeldFactorPPH.Value);
        }

        public Rule<float> WeldFactorPPH { get; set; }
        protected virtual void ConfigureWeldFactorPPH(Rule<float> rule)
        {
            rule.Define(1);
        }

        public Rule<double> PaintCarriersPerHour { get; set; }
        protected virtual void ConfigurePaintCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<bool> LargeClipSizePerCarrier { get; set; }
        protected virtual void ConfigureLargeClipSizePerCarrier(Rule<bool> rule)
        {
            rule.Define(() => BracketSelectedHeight.Value > 8);
        }

        public Rule<double> PaintPartsPerCarrier { get; set; }
        protected virtual void ConfigurePaintPartsPerCarrier(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> PaintPartsPerCarrierFactor { get; set; }
        protected virtual void ConfigurePaintPartsPerCarrierFactor(Rule<double> rule)
        {
            rule.Define(() =>
            {
                if (Width.Value < 60)
                {
                    return 1.5;
                }
                else if (Width.Value > 109)
                {
                    return 0.5;
                }
                else
                {
                    return 1;
                }
            });
        }

        public Rule<double> UnloadQtyFactor { get; set; }
        protected virtual void ConfigureUnloadQtyFactor(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> UnloadPartsPerCarrier { get; set; }
        protected virtual void ConfigureUnloadPartsPerCarrier(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> UnloadPartsPerCarrierFinal { get; set; }
        protected virtual void ConfigureUnloadPartsPerCarrierFinal(Rule<double> rule)
        {
            rule.Define(() => UnloadPartsPerCarrierFactor.Value * UnloadPartsPerCarrier.Value);
        }

        public Rule<double> UnloadPartsPerCarrierFactor { get; set; }
        protected virtual void ConfigureUnloadPartsPerCarrierFactor(Rule<double> rule)
        {
            rule.Define(() =>
            {
                if (Width.Value < 60)
                {
                    return 1.5;
                }
                else if (Width.Value > 109)
                {
                    return 0.5;
                }
                else
                {
                    return 1;
                }
            });
        }

        public Rule<double> UnloadWeightFactor { get; set; }
        protected virtual void ConfigureUnloadWeightFactor(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> UnloadCarriersPerHour { get; set; }
        protected virtual void ConfigureUnloadCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public ReadOnlyRule<double> Weight { get; set; }
        protected virtual void ConfigureWeight(ReadOnlyRule<double> rule)
        {
            rule.Define(() => Bom.UnitWeight.Value);
        }

        #endregion

        #region Children

        public ShelfBeam Beam { get; set; }
        protected virtual void ConfigureBeam(ShelfBeam child)
        {
            child.ChannelSize.Define(ChannelSize);
            child.Length.Define(BeamCutLength);
            child.HoleLocations.Define(HoleLocationsonBeam);
            child.HoleDiameter.Define(HoleSize);
            child.SquareHoleSize.Define(() => HoleType.Value == "square" ? HoleSize.Value : 0);
            child.Position.Define(() => 
            {
                float yOffset = -child.ChannelWidth.Value / 2;

                if (Orientation.Value == "std")
                    return Helper.Vyz(yOffset, -(Width.Value / 2) + (BeamCutLength.Value / 2));
                else
                    return Helper.Vxyz(-child.ChannelDepth.Value, yOffset, -(Width.Value / 2) - (BeamCutLength.Value / 2));
            });
            child.Rotation.Define(() => Orientation.Value == "std" ? Helper.CreateRotation(Vector3.UnitY, 180) : Quaternion.Identity);
        }

        public ShelfBeamEndBracket LeftEndBracket { get; set; }
        protected virtual void ConfigureLeftEndBracket(ShelfBeamEndBracket child)
        {
            child.Type.Define(BracketType);
            child.Position.Define(() => Helper.Vyz(WeldDown.Value, -ClipToColumnGap.Value));
            child.Rotation.Define(() => Helper.CreateRotation(Vector3.UnitY, 180));
        }

        public ShelfBeamEndBracket RightEndBracket { get; set; }
        protected virtual void ConfigureRightEndBracket(ShelfBeamEndBracket child)
        {
            child.Type.Define(BracketType);
            child.Position.Define(() => Helper.Vyz(WeldDown.Value - child.Height.Value, -Width.Value + ClipToColumnGap.Value));
            child.Rotation.Define(() => Quaternion.Concatenate(Helper.CreateRotation(Vector3.UnitY, 180), Helper.CreateRotation(Vector3.UnitX, 180)));
        }

        //public UCS UCS { get; set; }
        //protected virtual void ConfigureUCS(UCS child)
        //{
        //    child.Diameter.Define(0.225f);
        //    child.Position.Define(() => (Helper.Vy(0)));
        //}

        //public UCS FrontUCS { get; set; }
        //protected virtual void ConfigureFrontUCS(UCS child)
        //{
        //    child.Diameter.Define(0.225f);
        //    child.Position.Define(() => (Helper.Vy(0)));
        //}

        //public UCS RearUCS { get; set; }
        //protected virtual void ConfigureRearUCS(UCS child)
        //{
        //    child.Diameter.Define(0.225f);
        //    child.Position.Define(() => (Helper.Vz(-Width.Value)));
        //}

        #endregion

        #region Part Overrides

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(() => IsStandalone.Value || IsAdhoc);
        }

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(Colors.Gray);
            //material.Opacity.Define(0.9f);
        }

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define("LoadBeam");
        }

        protected override void ConfigureAllowedChildTypes(ReadOnlyRule<IEnumerable<Type>> rule)
        {
            rule.Define(() => new List<Type>
            {
                typeof(CustomPart)
            });
        }

        #endregion

        #region Protected Methods

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureShelfLoadBeamInputsRules();
            this.ConfigureLoadBeamDataRules();
            this.ConfigureChannelRules();
            this.ConfigureBomPartRules();
            this.ConfigureRevisablePart();
        }

        #endregion
    }
}
