﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;
using Ruler.Rules;
using Ruler.Cpq;
using Ruler.Rules.Graphics;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class WeldedCrossbar : CrossbarComponent, IChannel, IAngle, IRevisablePart, Ruler.Cpq.IBomPart
    {
        #region Constructors

        public WeldedCrossbar(ModelContext modelContext, StorageConfiguratorRepository repo) : base(modelContext, repo) { }

        public WeldedCrossbar(string name, Part parent, StorageConfiguratorRepository repo) : base(name, parent, repo) { }

        #endregion

        #region Rules

        [CadRule(CadRuleUsageType.Property, "CrossbarType")]
        public Rule<string> Type { get; set; }
        protected virtual void ConfigureType(Rule<string> rule)
        {
            //handles xbw, xbw-dd1, xbw-dd2, xbwr1, xbwr2
            rule.Define("xbw");
        }

        [CadRule(CadRuleUsageType.Property)]
        public ReadOnlyRule<string> CrossbarDescription { get; set; }
        protected virtual void ConfigureCrossbarDescription(ReadOnlyRule<string> rule)
        {
            rule.Define($"WELDLESS\r\nCROSSBAR");
        }

        public Rule<float> RackDepth { get; set; }
        protected virtual void ConfigureRackDepth(Rule<float> rule)
        {
            rule.Define(90);
        }

        public Rule<float> FrontFrameDepth { get; set; }
        protected virtual void ConfigureFrontFrameDepth(Rule<float> rule)
        {
            rule.Define(44);
        }

        public Rule<float> FrameTieLength { get; set; }
        protected virtual void ConfigureFrameTieLength(Rule<float> rule)
        {
            rule.Define(8);
        }

        public Rule<float> RearFrameDepth { get; set; }
        protected virtual void ConfigureRearFrameDepth(Rule<float> rule)
        {
            rule.Define(44);
        }

        public Rule<float> EndCrossbarToOutsideofBeam1 { get; set; }
        protected virtual void ConfigureEndCrossbarToOutsideofBeam1(Rule<float> rule)
        {
            rule.Define(() => FrontFrameDepth.Value - ChannelWebThickness.Value);
        }

        //This will be the distance from the end of crossbar to the vertical outside face of the beam
        public Rule<float> EndCrossbarToNotch1BeamAttachFaceLocation { get; set; }
        protected virtual void ConfigureEndCrossbarToNotch1BeamAttachFaceLocation(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (Notch1BeamOrientation.Value == "std")
                {
                    return EndCrossbarToOutsideofBeam1.Value;
                }
                else
                {
                    return EndCrossbarToOutsideofBeam1.Value - ChannelDepth.Value;
                }
            });
        }

        //This is the distance that the attachment angle overlaps the notch cutout.
        public Rule<float> AttachmentAngleOverlap { get; set; }
        protected virtual void ConfigureAttachmentAngleOverlap(Rule<float> rule)
        {
            rule.Define(0.125f);
        }

        [CadRule(CadRuleUsageType.Property, "DimX")]
        public Rule<float> EndOfCrossbarAngleToNotch1RightEdge { get; set; }
        protected virtual void ConfigureEndOfCrossbarAngleToNotch1RightEdge(Rule<float> rule)
        {
            rule.Define(() =>
            {
                var retval = -AngleToChannelOffset.Value;

                if (Notch1BeamOrientation.Value == "std")
                {
                    retval = retval + EndCrossbarToNotch1BeamAttachFaceLocation.Value 
                                    + NotchAngleToLoadBeamGap.Value
                                    + AttachmentAngleOverlap.Value - Notch1Width.Value;
                }
                else
                {
                    retval = retval + EndCrossbarToNotch1BeamAttachFaceLocation.Value
                                    - NotchAngleToLoadBeamGap.Value
                                    - AttachmentAngleOverlap.Value;
                }

                return Helper.RoundToNearest(retval, 0.0625f);
            });
        }

        public Rule<float> Notch1Width { get; set; }
        protected virtual void ConfigureNotch1Width(Rule<float> rule)
        {
            rule.Define(2.75f);
        }

        public List<ChoiceListItem> BeamOrientationChoices
        {
            get
            {
                return new List<ChoiceListItem>()
                {
                    new ChoiceListItem("std", "Standard"),
                    new ChoiceListItem("rev", "Reversed")
                };
            }
        }

        public Rule<string> FrontBeamOrientation { get; set; }
        protected virtual void ConfigureFrontBeamOrientation(Rule<string> rule)
        {
            rule.Define("std");
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(BeamOrientationChoices);
            });
        }

        public Rule<string> RearBeamOrientation { get; set; }
        protected virtual void ConfigureRearBeamOrientation(Rule<string> rule)
        {
            rule.Define("std");
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(BeamOrientationChoices);
            });
        }

        public Rule<string> Notch1BeamOrientation { get; set; }
        protected virtual void ConfigureNotch1BeamOrientation(Rule<string> rule)
        {
            rule.Define("std");
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !Type.Value.Contains("xbw-dd"));
                ui.ChoiceList.Define(BeamOrientationChoices);
            });
        }

        public Rule<float> EndCrossbarToOutsideofBeam2 { get; set; }
        protected virtual void ConfigureEndCrossbarToOutsideofBeam2(Rule<float> rule)
        {
            rule.Define(() => (FrontFrameDepth.Value + FrameTieLength.Value) - ChannelWebThickness.Value);
        }

        //This will be the distance from the end of crossbar to the vertical outside face of the second beam
        public Rule<float> EndCrossbarToNotch2BeamAttachFaceLocation { get; set; }
        protected virtual void ConfigureEndCrossbarToNotch2BeamAttachFaceLocation(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (Notch2BeamOrientation.Value == "std")
                {
                    return EndCrossbarToOutsideofBeam2.Value;
                }
                else
                {
                    return EndCrossbarToOutsideofBeam2.Value + ChannelDepth.Value;
                }
            });
        }

        [CadRule(CadRuleUsageType.Property, "DimX2")]
        public Rule<float> EndOfCrossbarAngleToNotch2RightEdge { get; set; }
        protected virtual void ConfigureEndOfCrossbarAngleToNotch2RightEdge(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (Type.Value != "xbw-dd2") return 0;
                var retval = -AngleToChannelOffset.Value;

                if (Notch2BeamOrientation.Value == "std")
                {
                    retval = retval + EndCrossbarToNotch2BeamAttachFaceLocation.Value
                                   - NotchAngleToLoadBeamGap.Value
                                   - AttachmentAngleOverlap.Value;
                }
                else
                {
                    retval = retval + EndCrossbarToNotch2BeamAttachFaceLocation.Value
                                     + NotchAngleToLoadBeamGap.Value
                                     + AttachmentAngleOverlap.Value - Notch2Width.Value;
                }

                return Helper.RoundToNearest(retval, 0.0625f);
            });
        }

        public Rule<float> Notch2Width { get; set; }
        protected virtual void ConfigureNotch2Width(Rule<float> rule)
        {
            rule.Define(2.75f);
        }

        public Rule<string> Notch2BeamOrientation { get; set; }
        protected virtual void ConfigureNotch2BeamOrientation(Rule<string> rule)
        {
            rule.Define("std");
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !Type.Value.Contains("xbw-dd") || EndOfCrossbarAngleToNotch2RightEdge.Value <= 0);
                ui.ChoiceList.Define(BeamOrientationChoices);
            });
        }

        public Rule<string> HoleType { get; set; }
        protected virtual void ConfigureHoleType(Rule<string> rule)
        {
            rule.Define("round");
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(new List<ChoiceListItem>()
                {
                    new ChoiceListItem("round", "Round"),
                    new ChoiceListItem("square", "Square")
                });
            });
        }

        public Rule<float> HoleSize { get; set; }
        protected virtual void ConfigureHoleSize(Rule<float> rule)
        {
            rule.Define(0.5625f);
        }

        public Rule<bool> IsDoubleDeepCrossbar { get; set; }
        protected virtual void ConfigureIsDoubleDeepCrossbar(Rule<bool> rule)
        {
            rule.Define(() => Type.Value == "xbw-dd1" || Type.Value == "xbw-dd2");
        }

        public Rule<float> RoundedOverallLength { get; set; }
        protected virtual void ConfigureRoundedOverallLength(Rule<float> rule)
        {
            rule.Define(() => Helper.RoundToNearest(OverallLength.Value, 0.0625f));
        }

        [CadRule(CadRuleUsageType.Property, "DimC")]
        public Rule<float> AngleToChannelOffset { get; set; }
        protected virtual void ConfigureAngleToChannelOffset(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (IsDoubleDeepCrossbar.Value)
                {
                    return Helper.RoundToNearest((RoundedOverallLength.Value - AngleLength.Value)/2, 0.0625f);
                }
                else
                {
                    return (ChannelDepth.Value - (ChannelWebThickness.Value + 0.0625f) + 0.125f);
                }
            });
        }

        [CadRule(CadRuleUsageType.Property, "DimB")] 
        public Rule<float> AngleLength { get; set; }
        protected virtual void ConfigureAngleLength(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (IsDoubleDeepCrossbar.Value)
                {
                    return Helper.RoundToNearest(RackDepth.Value - ((ChannelDepth.Value + 0.125f)*2), 0.0625f);
                }
                else if (Type.Value == "xbw")
                {
                    return  OverallLength.Value - (AngleToChannelOffset.Value * 2);
                }
                else if (Type.Value == "xbwr1")
                {
                    return OverallLength.Value - (AngleToChannelOffset.Value + RearReverseBeamEndPlate.Thickness.Value);
                }
                else if (Type.Value == "xbwr2")
                {
                    return OverallLength.Value - (FrontReverseBeamEndPlate.Thickness.Value + RearReverseBeamEndPlate.Thickness.Value);
                }
                else
                {
                    return 12;
                }
            });
        }

        [CadRule(CadRuleUsageType.Property, "Part1")]
        public Rule<string> ClipPartNumber { get; set; }
        protected virtual void ConfigureClipPartNumber(Rule<string> rule)
        {
            rule.Define(() =>
            {
                switch (Type.Value)
                {
                    case "xbw":
                    case "xbwr1":
                    case "xbw-dd1":
                    case "xbw-dd2":
                        return FrontEndAngle.PartNumber.Value;                   
                    default:
                        return "";
                }
            });
            
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> ClipDescription { get; set; }
        protected virtual void ConfigureClipDescription(Rule<string> rule)
        {
            rule.Define(() =>
            {
                if (Type.Value == "xbw" || Type.Value == "xbwr1")
                {
                    string retVal = @"3/16"" x 3"" x 2"" ANGLE";

                    if (HoleType.Value == "round")
                    {
                        retVal = retVal + "(ø" + (HoleSize.Value == 0.5625f ? @"9/16""" : @"7/16""") + " ROUND)";
                    }
                    else
                    {
                        retVal = retVal + @"(17/32"" SQUARE)";
                    }

                    return retVal;
                }
                else
                    return "NEED DATA";
            });
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> AnglePartNumber { get; set; }
        protected virtual void ConfigureAnglePartNumber(Rule<string> rule)
        {
            rule.Define(() => Angle.PartNumber.Value);
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> AngleDescription { get; set; }
        protected virtual void ConfigureAngleDescription(Rule<string> rule)
        {
            rule.Define(() => AngleType.Value + " ANGLE");
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> FlatBarPartNumber { get; set; }
        protected virtual void ConfigureFlatBarPartNumber(Rule<string> rule)
        {
            rule.Define(() => NotchPlate.PartNumber.Value);
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> FlatBarDescription { get; set; }
        protected virtual void ConfigureFlatBarDescription(Rule<string> rule)
        {
            rule.Define(() => NotchPlate.Description.Value);
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<float> FlatBarCutLength { get; set; }
        protected virtual void ConfigureFlatBarCutLength(Rule<float> rule)
        {
            rule.Define(() => NotchPlate.Length.Value);
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> KSI { get; set; }
        protected virtual void ConfigureKSI(Rule<string> rule)
        {
            rule.Define(() =>
            {
                switch (AngleType.Value)
                {
                    case @"2 x 2 x 3/16":
                    case @"2-1/2 x 2-1/2 x 3/16":
                    case @"3 x 2 x 1/4":
                        return "36";
                    default:
                        return "50";
                }
            });
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> DimW { get; set; }
        protected virtual void ConfigureDimW(Rule<string> rule)
        {
            rule.Define(() =>
            {
                if (Type.Value == "xbwr2")
                {
                    switch (AngleType.Value)
                    {
                        case @"1-1/2 x 1-1/2 x 1/8":
                            return @"2-1/2""";
                        case @"2 x 2 x 1/8":
                            return @"3""";
                        default:
                            return @"4""";
                    }
                }
                else
                    return "";
            });
        }

        public Rule<float> NotchPlateLength { get; set; }
        protected virtual void ConfigureNotchPlateLength(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (Type.Value == "xbw-dd1")
                    return 8;
                else if (Type.Value == "xbw-dd2")
                {
                    float plateOverlap = 2.625f;
                    return (Math.Abs(EndOfCrossbarAngleToNotch2RightEdge.Value - EndOfCrossbarAngleToNotch1RightEdge.Value) + Notch2Width.Value + (plateOverlap * 2));
                }
                else
                    return 0;
            });
        }

        public Rule<float> HoleVertOffset { get; set; }
        protected virtual void ConfigureHoleVertOffset(Rule<float> rule)
        {
            rule.Define(1.4375f);
        }

        public Rule<float> HoleHorizOffset { get; set; }
        protected virtual void ConfigureHoleHorizOffset(Rule<float> rule)
        {
            rule.Define(1);
        }

        public Rule<Vector3> NotchPlateLocation { get; set; }
        protected virtual void ConfigureNotchPlateLocation(Rule<Vector3> rule)
        {
            rule.Define(() =>
            {
                float xLoc = -EndOfCrossbarAngleToNotch1RightEdge.Value;

                if (Type.Value == "xbw-dd2")
                {
                    xLoc = -(EndOfCrossbarAngleToNotch1RightEdge.Value 
                                + ((EndOfCrossbarAngleToNotch2RightEdge.Value - EndOfCrossbarAngleToNotch1RightEdge.Value) / 2)
                                + Notch1Width.Value);
                }
                
                return Helper.Vxy(xLoc, HoleVertOffset.Value - AngleThickness.Value);
            });
        }

        [CadRule(CadRuleUsageType.Property, "DimS")]
        public ReadOnlyRule<float> NotchOneAngleOffset { get; set; }
        protected virtual void ConfigureNotchOneAngleOffset(ReadOnlyRule<float> rule)
        {
            rule.Define(() =>
            {
                var retval = 0.0f;
                if (Notch1BeamOrientation.Value == "std")
                {
                    retval = EndCrossbarToNotch1BeamAttachFaceLocation.Value + NotchAngleToLoadBeamGap.Value;
                }
                else
                {
                    retval = EndCrossbarToNotch1BeamAttachFaceLocation.Value - NotchAngleToLoadBeamGap.Value;
                }

                return Helper.RoundToNearest(retval, 0.0625f);
            });
        }

        public Rule<float> NotchAngleToLoadBeamGap { get; set; }
        protected virtual void ConfigureNotchAngleToLoadBeamGap(Rule<float> rule)
        {
            rule.Define(0.125f);
        }

        [CadRule(CadRuleUsageType.Property, "DimS2")]
        public ReadOnlyRule<float> NotchTwoAngleOffset { get; set; }
        protected virtual void ConfigureNotchTwoAngleOffset(ReadOnlyRule<float> rule)
        {
            rule.Define(() =>
            {
                var retval = 0.0f;
                if (Notch2BeamOrientation.Value == "std")
                {
                    retval = EndCrossbarToNotch2BeamAttachFaceLocation.Value - NotchAngleToLoadBeamGap.Value;
                }
                else
                {
                    retval = EndCrossbarToNotch2BeamAttachFaceLocation.Value + NotchAngleToLoadBeamGap.Value;
                }

                return Helper.RoundToNearest(retval, 0.0625f);
            });
        }

        //This is the description used only by the BOM Summary.
        public Rule<string> BOMUIDescription { get; set; }
        protected virtual void ConfigureBOMUIDescription(Rule<string> rule)
        {
            rule.Define(() => $"X bar({AngleType.Value}, {Helper.fractionString(RackDepth.Value, feetToo: false)}, {Helper.fractionString(RoundedOverallLength.Value, feetToo: false)})");
        }

        //Rule declared in base class
        protected virtual void ConfigureBOMDescription(Rule<string> rule)
        {
            rule.Define(() =>
            {
                var desc = "";
                var angleShortDesc = this.GetAngleShortDesc(AngleType.Value);
                switch (Type.Value)
                {
                    case "xbw":
                        desc = $"Welded Cross Bar";
                        desc += $"|{angleShortDesc}, for a {this.BeamSize.Value}# beam, {ColorBOMName.Value}";
                        desc += $"|with 1/2 HEX attachment hardware";
                        desc += $"|for {Helper.fractionString(HoleSize.Value)} {HoleType.Value.ToUpper()} holes";
                        break;
                    case "xbwr1":
                        desc = $"Welded Cross Bar with a {Helper.fractionString(HoleSize.Value)} round hole and a ";
                        desc += $"|square hole for one reverse beam";
                        desc += $"|{angleShortDesc}, for a {this.BeamSize.Value}# beam, {ColorBOMName.Value}";
                        break;
                    case "xbwr2":
                        desc = $"Welded Cross Bar with square holes";
                        desc += $"|for two reverse beams";
                        desc += $"|{angleShortDesc}, for a {this.BeamSize.Value}# beam, {ColorBOMName.Value}";
                        break;
                    case "xbw-dd1":
                        desc = $"Double Deep Welded Cross Bar w/1 Notch";
                        desc += $"|{OverallLength.Value:#.###} long, {angleShortDesc} material";
                        desc += $"|{ColorBOMName.Value}";
                        break;
                    case "xbw-dd2":
                        desc = $"Double Deep Welded Cross Bar w/2 Notches";
                        desc += $"|{OverallLength.Value:#.###} long, {angleShortDesc} material";
                        desc += $"|(1) interior clip to connect to (1) interior beam";
                        desc += $"|{ColorBOMName.Value}";
                        break;
                }

                return desc;
            });
        }

        #endregion

        #region IAngleData Implementation
        public Rule<List<AngleData>> AllAngleData { get; set; }
        public Rule<List<ChoiceListItem>> AllAngleTypesChoiceList { get; set; }
        public Rule<AngleData> ActiveAngleData { get; set; }
        public Rule<List<string>> AngleSizesAvailable { get; set; }
        public Rule<string> AngleType { get; set; }
        public Rule<float> AngleWidth { get; set; }
        public Rule<float> AngleHeight { get; set; }
        public Rule<float> AngleThickness { get; set; }
        public Rule<string> MaterialPartNumber { get; set; }
        #endregion

        #region IChannel Implementation

        public Rule<List<ChannelData>> AllChannelData { get; set; }
        public Rule<List<ChoiceListItem>> AllChannelSizesChoiceList { get; set; }
        public Rule<ChannelData> ActiveChannelData { get; set; }
        public Rule<float> ChannelWidth { get; set; }
        public Rule<float> ChannelDepth { get; set; }
        public Rule<float> ChannelWebThickness { get; set; }
        public Rule<float> ChannelWeightPerFt { get; set; }
        public Rule<List<string>> ChannelSizesAvailable { get; set; }
        public Rule<string> ChannelSize { get; set; }

        #endregion

        #region IRevisablePartImplementation

        public FactoryRevisionRule<string> FactoryRevision { get; set; }

        #endregion

        #region CPQ.IBomPart Implementation

        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {
            child.PartNumber.Define(PartNumber);
            child.ItemQty.Define(Qty);
            child.Description.Define(BOMUIDescription);
        }

        #endregion

        #region Labor Rules

        public Rule<double> WeldPartsPerHour { get; set; }
        protected virtual void ConfigureWeldPartsPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> PaintCarriersPerHour { get; set; }
        protected virtual void ConfigurePaintCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> PartsPerCarrier { get; set; }
        protected virtual void ConfigurePartsPerCarrier(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> UnloadCarriersPerHour { get; set; }
        protected virtual void ConfigureUnloadCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }
        
        #endregion

        #region Private Methods

        #endregion

        #region Children

        public CrossbarAngle Angle { get; set; }
        protected virtual void ConfigureAngle(CrossbarAngle child)
        {
            child.AngleType.Define(AngleType);
            child.CrossbarType.Define(Type);
            child.Length.Define(AngleLength);
            child.Notch1Location.Define(() => EndOfCrossbarAngleToNotch1RightEdge.Value > 0 ? (EndOfCrossbarAngleToNotch1RightEdge.Value + Notch1Width.Value) : 0);
            child.Notch1Width.Define(Notch1Width);
            child.Notch2Location.Define(() => EndOfCrossbarAngleToNotch2RightEdge.Value > 0 ? EndOfCrossbarAngleToNotch2RightEdge.Value : 0);
            child.Notch2Width.Define(Notch2Width);
            child.Position.Define(() =>
            {
                float xOffset = -AngleToChannelOffset.Value;

                if (Type.Value == "xbwr2")
                    xOffset = - FrontReverseBeamEndPlate.Thickness.Value;
                
                return Helper.Vxyz(xOffset, HoleVertOffset.Value, (child.AngleThickness.Value + 1));
            });
            child.Rotation.Define(() => Quaternion.CreateFromYawPitchRoll(Helper.DegToRad(-90), 0, Helper.DegToRad(180)));
        }

        public CrossbarEndAngle FrontEndAngle { get; set; }
        protected virtual void ConfigureFrontEndAngle(CrossbarEndAngle child)
        {
            child.IsActive.Define(() => Type.Value != "xbwr2");
            child.PartNumberPrefix.Define("PC31042-");
            child.Length.Define(2);
            //child.Width.Define(3);
            //child.Height.Define(2);
            //child.Thickness.Define(0.1875f);
            child.AngleType.Define("3 x 2 x 3/16");
            child.HoleSize.Define(HoleSize);
            child.HoleType.Define(HoleType);
            child.Position.Define(() => Helper.Vz(child.HoleVertOffset.Value));
            child.Rotation.Define(() => Quaternion.CreateFromYawPitchRoll(Helper.DegToRad(0), Helper.DegToRad(-90), Helper.DegToRad(180)));
        }

        public CrossbarEndPlate FrontReverseBeamEndPlate { get; set; }
        protected virtual void ConfigureFrontReverseBeamEndPlate(CrossbarEndPlate child)
        {
            child.IsActive.Define(() => Type.Value == "xbwr2");
            child.Position.Define(() => Helper.Vyz(HoleVertOffset.Value, Angle.AngleThickness.Value));
        }

        public CrossbarEndAngle RearEndAngle { get; set; }
        protected virtual void ConfigureRearEndAngle(CrossbarEndAngle child)
        {
            child.IsActive.Define(() => Type.Value != "xbwr1" && Type.Value != "xbwr2");
            child.PartNumberPrefix.Define("PC31042-");
            child.Length.Define(2);
            //child.Width.Define(3);
            //child.Height.Define(2);
            //child.Thickness.Define(0.1875f);
            child.AngleType.Define("3 x 2 x 3/16");
            child.HoleSize.Define(HoleSize);
            child.HoleType.Define(HoleType);
            child.Position.Define(() => Helper.Vxz(-OverallLength.Value, child.HoleVertOffset.Value));
            child.Rotation.Define(() => Quaternion.CreateFromYawPitchRoll(Helper.DegToRad(0), Helper.DegToRad(90), Helper.DegToRad(0)));
        }

        public CrossbarEndPlate RearReverseBeamEndPlate { get; set; }
        protected virtual void ConfigureRearReverseBeamEndPlate(CrossbarEndPlate child)
        {
            child.IsActive.Define(() => Type.Value == "xbwr1" || Type.Value == "xbwr2");
            child.Position.Define(() => Helper.Vxyz(-OverallLength.Value, HoleVertOffset.Value, Angle.AngleThickness.Value));
            child.Rotation.Define(() => Helper.CreateRotation(Vector3.UnitY, 180));
        }

        public CrossbarEndAngle NotchOneAngle { get; set; }
        protected virtual void ConfigureNotchOneAngle(CrossbarEndAngle child)
        {
            child.IsActive.Define(() => Type.Value == "xbw-dd1" || Type.Value == "xbw-dd2");
            child.PartNumber.Define("PC31127");
            child.Length.Define(2);
            //child.Width.Define(2.5f);
            //child.Height.Define(2.5f);
            //child.Thickness.Define(0.1875f);
            child.AngleType.Define("3 x 2 x 3/16");
            child.HoleType.Define(HoleType);
            child.HoleSize.Define(HoleSize);
            child.Position.Define(() =>
            {
                float yOffset = HoleVertOffset.Value - (AngleThickness.Value + NotchPlate.Thickness.Value);
                
                return Helper.Vxy(-NotchOneAngleOffset.Value, yOffset);
            });
            child.Rotation.Define(() =>
            {
                float rotation = 0;

                if (Notch1BeamOrientation.Value == "std")
                    rotation = 180;

                return Helper.CreateRotation(Vector3.UnitY, rotation);
            });
        }

        public WeldedCrossbarNotchPlate NotchPlate { get; set; }
        protected virtual void ConfigureNotchPlate(WeldedCrossbarNotchPlate child)
        {
            child.IsActive.Define(() => Type.Value == "xbw-dd1" || Type.Value == "xbw-dd2");
            child.Length.Define(NotchPlateLength);
            child.IsCrossbarPlate.Define(true);
            child.Width.Define(() => AngleWidth.Value == 2.5f ? 2.5f : 2);
            child.Thickness.Define(0.1875f);
            child.Position.Define(NotchPlateLocation);
            child.Rotation.Define(() => Helper.CreateRotation(Vector3.UnitY, 90));
            child.PartNumberPrefix.Define(() => AngleWidth.Value == 2.5f ? "PC31172-" : "PC31173-");
        }

        public CrossbarEndAngle NotchTwoAngle { get; set; }
        protected virtual void ConfigureNotchTwoAngle(CrossbarEndAngle child)
        {
            child.IsActive.Define(() => Type.Value == "xbw-dd2");
            child.PartNumber.Define("PC31127");
            child.Length.Define(2);
            //child.Width.Define(2.5f);
            //child.Height.Define(2.5f);
            //child.Thickness.Define(0.1875f);
            child.AngleType.Define("2-1/2 x 2-1/2 x 3/16");
            child.HoleType.Define(HoleType);
            child.HoleSize.Define(HoleSize);
            child.Position.Define(() =>
            {
                float yOffset = HoleVertOffset.Value - (AngleThickness.Value + NotchPlate.Thickness.Value);
                
                return Helper.Vxy(-NotchTwoAngleOffset.Value, yOffset);
            });
            child.Rotation.Define(() =>
            {
                float rotation = 0;

                if (Notch2BeamOrientation.Value != "std")
                    rotation = 180;

                return Helper.CreateRotation(Vector3.UnitY, rotation);
            });
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(Colors.Blue);
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(true);
        }

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define("WeldedCrossbar");
        }

        protected override void ConfigureCrossbarDescription1(ReadOnlyRule<string> rule)
        {
            rule.Define(() =>
            {
                switch (Type.Value)
                {
                    case "xbwr1":
                    case "xbwr2":
                        return "WELDED CROSSBAR";
                    case "xbw-dd1":
                    case "xbw-dd2":
                        return "DOUBLE REACH";
                    default:
                        return "WELDED";
                }
            });
        }

        protected override void ConfigureCrossbarDescription2(ReadOnlyRule<string> rule)
        {
            rule.Define(() =>
            {
                switch (Type.Value)
                {
                    case "xbwr1":
                        return "1 REVERSE BEAM";
                    case "xbwr2":
                        return "2 REVERSE BEAMS";
                    case "xbw-dd1":
                    case "xbw-dd2":
                        return "WELDED CROSSBAR";
                    default:
                        return "CROSSBAR";
                }
            });
        }

        public ReadOnlyRule<string> CrossbarDescription3 { get; set; }
        protected virtual void ConfigureCrossbarDescription3(ReadOnlyRule<string> rule)
        {
            rule.Define(() =>
            {
                if (Type.Value == "xbw-dd1")
                {
                    return "SINGLE NOTCHED";
                }
                else if (Type.Value == "xbw-dd2")
                {
                    return "DOUBLE NOTCHED";
                }
                else
                {
                    return "";
                }
            });
            rule.AddToCAD(cad =>
            {
                cad.CadRuleUsageType = CadRuleUsageType.Property;
                cad.IsActive.Define(() => (Type.Value == "xbw-dd1" || Type.Value == "xbw-dd2"));
            });
        }

        public string GetBeamOrientShort(string orientation)
        {
            return (orientation == "std" ? "S" : "R");
        }

        protected virtual void ConfigurePartNumberPrefix(Rule<string> rule)
        {
            rule.Define(() =>
            {
                if (Type.Value == "xbw-dd1" || Type.Value == "xbw-dd2")
                {
                    var beamsOrientVal = $"{GetBeamOrientShort(FrontBeamOrientation.Value)}{GetBeamOrientShort(RearBeamOrientation.Value)}-";
                    return $"{Type.Value.ToUpper().Replace("-", beamsOrientVal)}-";
                }
                else
                {
                    return $"{Type.Value.ToUpper()}-";
                }
            });
        }

        #endregion

        #region IBomPart Overrides

        protected virtual void ConfigurePartWeight(Rule<double> rule)
        {
            rule.Define(() => this.GetPartWeightWithoutPaint());
        }

        #endregion

        #region Graphics

        //public Block Notch1Block { get; set; }
        //protected virtual void ConfigureNotch1Block(Block geo)
        //{
        //    geo.Width.Define(Notch1Width);
        //    geo.Height.Define(3);
        //    geo.Depth.Define(1);
        //    geo.Position.Define(() => Helper.Vx(-(Notch1Location.Value)));
        //}

        //public Block Notch2Block { get; set; }
        //protected virtual void ConfigureNotch2Block(Block geo)
        //{
        //    geo.Width.Define(Notch2Width);
        //    geo.Height.Define(3);
        //    geo.Depth.Define(1);
        //    geo.Position.Define(() => Helper.Vx(-(Notch2Location.Value)));
        //}

        //public UCS UCS { get; set; }
        //protected virtual void ConfigureUCS(UCS child)
        //{
        //    child.Diameter.Define(0.125f);
        //    child.Position.Define(() => (Helper.Vy(0)));
        //}

        //public UCS UCSt { get; set; }
        //protected virtual void ConfigureUCSt(UCS child)
        //{
        //    child.Diameter.Define(0.125f);
        //    child.Position.Define(() => (Helper.Vx(-AngleToChannelOffset.Value)));
        //}

        //public UCS UCSNotch1Loc { get; set; }
        //protected virtual void ConfigureUCSNotch1Loc(UCS child)
        //{
        //    child.Diameter.Define(0.125f);
        //    child.Position.Define(() => (Helper.Vx(-Notch1Location.Value)));
        //}

        #endregion

        #region Protected Methods

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureAngleRules();
            this.ConfigureChannelRules();
            this.ConfigureRevisablePart();
            this.ConfigureBomPartRules();
        }

        #endregion

    }
}
