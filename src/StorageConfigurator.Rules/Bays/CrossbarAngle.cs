﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;
using Ruler.Cpq;
using Ruler.Rules.Graphics;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class CrossbarAngle : Angle, IRevisablePart, IBomPart, Ruler.Cpq.IBomPart
    {

        #region Private Fields

        public AdvanceCSIItemService CsiPartService { get; }

        #endregion

        #region Constructors

        public CrossbarAngle(ModelContext modelContext, AdvanceCSIItemService csiPartService) : base(modelContext) 
        {
            CsiPartService = csiPartService;
        }

        public CrossbarAngle(string name, Part parent, AdvanceCSIItemService csiPartService) : base(name, parent)
        {
            CsiPartService = csiPartService;
        }

        #endregion

        #region Rules

        [CadRule(CadRuleUsageType.Parameter)]
        public Rule<float> Notch1Location { get; set; }
        protected virtual void ConfigureNotch1Location(Rule<float> rule)
        {
            rule.Define(default(float));
        }

        [CadRule(CadRuleUsageType.Parameter)]
        public Rule<float> Notch1Width { get; set; }
        protected virtual void ConfigureNotch1Width(Rule<float> rule)
        {
            rule.Define(default(float));
        }

        [CadRule(CadRuleUsageType.Parameter)]
        public Rule<float> Notch2Location { get; set; }
        protected virtual void ConfigureNotch2Location(Rule<float> rule)
        {
            rule.Define(default(float));
        }

        [CadRule(CadRuleUsageType.Parameter)]
        public Rule<float> Notch2Width { get; set; }
        protected virtual void ConfigureNotch2Width(Rule<float> rule)
        {
            rule.Define(default(float));
        }

        public Rule<string> CrossbarType { get; set; }
        protected virtual void ConfigureCrossbarType(Rule<string> rule)
        {
            rule.Define(default(string));
        }

        public Rule<string> DoubleDeepAngleBaseNumberPrefix { get; set; }
        protected virtual void ConfigureDoubleDeepAngleBaseNumberPrefix(Rule<string> rule)
        {
            rule.Define(default(string));
        }

        public Rule<string> NonDDAngleBaseNumberPrefix { get; set; }
        protected virtual void ConfigureNonDDAngleBaseNumberPrefix(Rule<string> rule)
        {
            rule.Define(default(string));
        }

        protected override void ConfigureDescriptionPrefix(Rule<string> rule)
        {
            rule.Define(() => $"{PartNumberPrefix.Value?.Replace("-", "") ?? ""}");
        }

        #endregion

        #region IRevisablePartImplementation

        public FactoryRevisionRule<string> FactoryRevision { get; set; }

        #endregion

        #region IBomPart Implementation

        public Rule<string> DatabasePartNumber { get; set; }
        public PartNumberRule<string> PartNumber { get; set; }

        public Rule<string> PartNumberPrefix { get; set; }
        protected virtual void ConfigurePartNumberPrefix(Rule<string> rule)
        {
            rule.Define(() => CrossbarType.Value.Contains("-dd") ? DoubleDeepAngleBaseNumberPrefix.Value : NonDDAngleBaseNumberPrefix.Value);
        }
        public Rule<string> BOMDescription { get; set; }
        public Rule<string> BOMDescription1 { get; set; }
        public Rule<string> BOMDescription2 { get; set; }
        public Rule<string> BOMDescription3 { get; set; }
        public Rule<string> BOMDescription4 { get; set; }
        public Rule<string> BOMDescription5 { get; set; }
        public Rule<string> BOMDescription6 { get; set; }
        public Rule<string> BOMDescription7 { get; set; }
        public Rule<string> BOMDescription8 { get; set; }
        public Rule<string> BOMDescription9 { get; set; }
        public Rule<string> BOMDescription10 { get; set; }
        public Rule<string> BOMDescription11 { get; set; }
        public Rule<string> Color { get; set; }
        public ReadOnlyRule<AdvanceColor> AdvanceColor { get; set; }
        public ReadOnlyRule<string> ColorDescription { get; set; }
        public StorageConfiguratorRepository Repository { get; set; }
        public Rule<string> ColorBOMName { get; set; }
        public ReadOnlyRule<string> ColorShortDescription { get; set; }

        public Rule<double> PartWeight { get; set; }
        public Rule<double> PartPaintWeight { get; set; }
        public Rule<float> WeightPerGallon { get; set; }
        public CsiRule<double> PaintCostPerGallon { get; set; }
        public Rule<float> PaintCost { get; set; }

        #endregion

        #region CPQ.IBomPart Implementation

        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {
            child.PartNumber.Define(PartNumber);
            child.Description.Define(BOMUIDescription);
            child.MaterialPartNumber.Define(MaterialPartNumber);
            child.MaterialQty.Define(() => Length.Value / 12);
            child.ItemMaterialCost.Define(() => child.ItemRawMaterialWeight.Value * child.MaterialUnitCost.Value);
        }

        #endregion

        #region Labor Rules

        public Rule<double> BundleQty { get; set; }
        protected virtual void ConfigureBundleQty(Rule<double> rule)
        {
            rule.Define(() => CsiPartService.GetItemBundle(MaterialPartNumber.Value, Bom.PlantCode.Value));
        }

        public Rule<double> PartCarriersPerHour { get; set; }
        protected virtual void ConfigurePartCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> IronworkerFactor { get; set; }
        protected virtual void ConfigureIronworkerFactor(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(Colors.Blue);
        }

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define("CrossbarAngle");
        }

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureRevisablePart();
            this.ConfigureBomPartRules();
        }

        #endregion

        #region Graphics

        //public Block Notch1Block { get; set; }
        //protected virtual void ConfigureNotch1Block(Block geo)
        //{
        //    geo.IsActive.Define(() => Notch1Location.Value > 0);
        //    geo.Width.Define(AngleWidth);
        //    geo.Height.Define(AngleHeight);
        //    geo.Depth.Define(Notch1Width);
        //    //geo.Position.Define(() => Helper.Vxyz(geo.Width.Value /2, geo.Height.Value / 2, Notch1Location.Value));
        //    geo.Position.Define(() => Helper.Vz(Notch1Location.Value - (Notch1Width.Value/2)));
        //}

        //public Block Notch2Block { get; set; }
        //protected virtual void ConfigureNotch2Block(Block geo)
        //{
        //    geo.IsActive.Define(() => Notch2Location.Value > 0);
        //    geo.Width.Define(AngleWidth);
        //    geo.Height.Define(AngleHeight);
        //    geo.Depth.Define(Notch2Width);
        //    geo.Position.Define(() => Helper.Vz(Notch2Location.Value + (Notch2Width.Value/2)));
        //}

        //public UCS UCS { get; set; }
        //protected virtual void ConfigureUCS(UCS child)
        //{
        //    child.Diameter.Define(0.25f);
        //    child.Position.Define(() => (Helper.Vy(0)));
        //}

        //public UCS UCSt { get; set; }
        //protected virtual void ConfigureUCSt(UCS child)
        //{
        //    child.Diameter.Define(0.25f);
        //    child.Position.Define(() => Helper.Vz(Notch1Location.Value));
        //}

        #endregion

    }
}


