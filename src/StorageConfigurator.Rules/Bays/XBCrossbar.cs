﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Cpq;
using Ruler.Rules;
using Ruler.Rules.Graphics;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class XBCrossbar : CrossbarComponent, IAngle, IRevisablePart, Ruler.Cpq.IBomPart
    {
        #region Constructors

        public XBCrossbar(ModelContext modelContext, StorageConfiguratorRepository repo) : base(modelContext, repo) { }

        public XBCrossbar(string name, Part parent, StorageConfiguratorRepository repo) : base(name, parent, repo) { }

        #endregion

        #region Rules

        [CadRule(CadRuleUsageType.Parameter)]
        public Rule<float> Length { get; set; }
        protected virtual void ConfigureLength(Rule<float> rule)
        {
            rule.Define(() => Helper.RoundToNearest(OverallLength.Value, 0.0625f));
        }

        public Rule<string> HoleType { get; set; }
        protected virtual void ConfigureHoleType(Rule<string> rule)
        {
            rule.Define("round");
            //rule.AddToUI(ui =>
            //{
            //    ui.ChoiceList.Define(new List<ChoiceListItem>()
            //    {
            //        new ChoiceListItem("round", "Round"),
            //        new ChoiceListItem("square", "Square")
            //    });
            //});
        }

        [CadRule(CadRuleUsageType.Parameter)]
        public Rule<float> HasRoundHole { get; set; }
        protected virtual void ConfigureHasRoundHole(Rule<float> rule)
        {
            rule.Define(() => HoleType.Value == "round" ? 1 : 0);
        }

        public Rule<float> HoleSize { get; set; }
        protected virtual void ConfigureHoleSize(Rule<float> rule)
        {
            rule.Define(0.5625f);
            //rule.AddToUI();
            rule.AddToCAD(cad =>
            {
                cad.CadRuleUsageType = CadRuleUsageType.Parameter;
            });
        }

        public Rule<float> HoleHorizOffset { get; set; }
        protected virtual void ConfigureHoleHorizOffset(Rule<float> rule)
        {
            rule.Define(0.75f);
        }

        public Rule<float> VertOffsetToHole { get; set; }
        protected virtual void ConfigureVertOffsetToHole(Rule<float> rule)
        {
            rule.Define(1.4375f);
        }

        public Rule<float> EndBlockHeight { get; set; }
        protected virtual void ConfigureEndBlockHeight(Rule<float> rule)
        {
            rule.Define(2.125f);
        }

        [CadRule(CadRuleUsageType.Property, "CrossbarType")]
        public ReadOnlyRule<string> Type { get; set; }
        protected virtual void ConfigureType(ReadOnlyRule<string> rule)
        {
            rule.Define(() => HoleType.Value == "round" ? "xbrnd" : "xbsqr");
        }

        public Rule<float> EndFlangeFlatPatternOffset { get; set; }
        protected virtual void ConfigureEndFlangeFlatPatternOffset(Rule<float> rule)
        {
            rule.Define(3.0625f);
        }

        public Rule<float> InteriorFlangeFlatPatternOffset { get; set; }
        protected virtual void ConfigureInteriorFlangeFlatPatternOffset(Rule<float> rule)
        {
            rule.Define(0.8125f);
        }

        public Rule<float> VertOffsetFromBtmFlangeToHole { get; set; }
        protected virtual void ConfigureVertOffsetFromBtmFlangeToHole(Rule<float> rule)
        {
            rule.Define(0.6875f);
        }

        [CadRule(CadRuleUsageType.Property, "DimB")]
        public ReadOnlyRule<float> AngleCutLength { get; set; }
        protected virtual void ConfigureAngleCutLength(ReadOnlyRule<float> rule)
        {
            rule.Define(() => Helper.RoundToNearest(Length.Value + EndFlangeFlatPatternOffset.Value, 0.0625f));
        }

        [CadRule(CadRuleUsageType.Property, "DimC")]
        public ReadOnlyRule<float> InteriorCutLength { get; set; }
        protected virtual void ConfigureInteriorCutLength(ReadOnlyRule<float> rule)
        {
            rule.Define(()  => Helper.RoundToNearest(Length.Value - InteriorFlangeFlatPatternOffset.Value, 0.0625f));
        }

        [CadRule(CadRuleUsageType.Property, "DimD")]
        public ReadOnlyRule<float> FlatPatternHoleToEdge { get; set; }
        protected virtual void ConfigureFlatPatternHoleToEdge(ReadOnlyRule<float> rule)
        {
            rule.Define(() => Helper.RoundToNearest(AngleCutLength.Value - VertOffsetFromBtmFlangeToHole.Value, 0.0625f));
        }

        [CadRule(CadRuleUsageType.Property, "Part1")]
        public Rule<string> AnglePartNumber { get; set; }
        protected virtual void ConfigureAnglePartNumber(Rule<string> rule)
        {
            rule.Define(() => AngleWidth.Value == 1.5f ? "P40099" : "P40099-1");
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> AngleDescription { get; set; }
        protected virtual void ConfigureAngleDescription(Rule<string> rule)
        {
            rule.Define(() => AngleType.Value + " ANGLE");
        }

        public LookupRule<float> RackDepth { get; set; }

        //This is the description used only by the BOM Summary.
        public Rule<string> BOMUIDescription { get; set; }
        protected virtual void ConfigureBOMUIDescription(Rule<string> rule)
        {
            rule.Define(() => $"X bar({AngleType.Value}, {Helper.fractionString(RackDepth.Value, feetToo: false)}, {Helper.fractionString(Length.Value, feetToo: false)})");
        }

        #endregion

        #region IAngleData Implementation
        public Rule<List<AngleData>> AllAngleData { get; set; }
        public Rule<List<ChoiceListItem>> AllAngleTypesChoiceList { get; set; }
        public Rule<AngleData> ActiveAngleData { get; set; }
        public Rule<List<string>> AngleSizesAvailable { get; set; }
        public Rule<string> AngleType { get; set; }
        public Rule<float> AngleWidth { get; set; }
        protected virtual void ConfigureAngleWidth(Rule<float> rule)
        {
            rule.AddToCAD(cad =>
            {
                cad.NameInCad = "Width";
            });
        }

        public Rule<float> AngleHeight { get; set; }
        protected virtual void ConfigureAngleHeight(Rule<float> rule)
        {
            rule.AddToCAD(cad =>
            {
                cad.NameInCad = "Height";
            });
        }
        public Rule<float> AngleThickness { get; set; }
        protected virtual void ConfigureAngleThickness(Rule<float> rule)
        {
            //rule.AddToCAD(cad =>
            //{
            //    cad.NameInCad = "Thickness";
            //});
        }

        public Rule<string> MaterialPartNumber { get; set; }

        #endregion

        #region IRevisablePartImplementation

        public FactoryRevisionRule<string> FactoryRevision { get; set; }

        #endregion

        #region CPQ.IBomPart Implementation

        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {
            child.IsPurchased.Define(false);
            child.PartNumber.Define(PartNumber);
            child.Description.Define(BOMUIDescription);
            child.ItemQty.Define(Qty);
        }

        #endregion

        #region Labor Hour Rules

        public Rule<double> PaintCarriersPerHour { get; set; }
        protected virtual void ConfigurePaintCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> PaintPartsPerCarrier { get; set; }
        protected virtual void ConfigurePaintPartsPerCarrier(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> UnloadQtyFactor { get; set; }
        protected virtual void ConfigureUnloadQtyFactor(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> UnloadPartsPerCarrier { get; set; }
        protected virtual void ConfigureUnloadPartsPerCarrier(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> UnloadWeightFactor { get; set; }
        protected virtual void ConfigureUnloadWeightFactor(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> UnloadCarriersPerHour { get; set; }
        protected virtual void ConfigureUnloadCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public ReadOnlyRule<double> Weight { get; set; }
        protected virtual void ConfigureWeight(ReadOnlyRule<double> rule)
        {
            rule.Define(() => Bom.ItemWeight.Value);
        }

        #endregion

        #region Overrides

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(Colors.Blue);
        }

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define("XBCrossbar");
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(false);
        }


        #endregion

        #region CrossbarOverrides


        protected override void ConfigureCrossbarDescription1(ReadOnlyRule<string> rule)
        {
            rule.Define("WELDLESS");
        }

        protected override void ConfigureCrossbarDescription2(ReadOnlyRule<string> rule)
        {
            rule.Define("CROSSBAR");
        }

        //Rule declared in base class
        protected virtual void ConfigureBOMDescription(Rule<string> rule)
        {
            rule.Define(() =>
            {
                var desc = $"Weldless Safety Bar {Length.Value}\", {this.GetAngleShortDesc(AngleType.Value)}";
                desc += $"|With {Helper.fractionString(HoleSize.Value)} {UpperFirstChar(HoleType.Value)} Holes";
                desc += $"|for a {this.BeamSize.Value }# beam, {ColorBOMName.Value}";

                return desc;
            });
        }

        #endregion

        #region IBomPart Overrides

        protected virtual void ConfigurePartWeight(Rule<double> rule)
        {
            rule.Define(() => this.GetPartWeightWithoutPaint());
        }

        #endregion

        #endregion

        #region Public Methods

        public string UpperFirstChar(string text)
        {
            return string.IsNullOrEmpty(text) ? string.Empty : char.ToUpper(text[0]) + text.Substring(1);
        }

        #endregion

        #region Children

        public XBCrossbarAngle XBCrossbarAngle { get; set; }
        protected virtual void ConfigureXBCrossbarAngle(XBCrossbarAngle child)
        {
            child.PartNumberPrefix.Define(() => $"{AnglePartNumber.Value}-");
            child.AngleType.Define(AngleType);
            child.AngleCutLength.Define(AngleCutLength);
            child.Description.Define(AngleDescription);
        }

        #endregion

        #region Graphics

        public Block HorizontalLegBlock { get; set; }
        protected virtual void ConfigureHorizontalLegBlock(Block geo)
        {
            geo.Width.Define(Length);
            geo.Height.Define(AngleThickness);
            geo.Depth.Define(AngleWidth);
            geo.Position.Define(() => Helper.Vxyz(-geo.Width.Value / 2, (-geo.Height.Value / 2), (-geo.Depth.Value / 2) + HoleHorizOffset.Value));
        }

        public Block VerticalLegBlock { get; set; }
        protected virtual void ConfigureVerticalLegBlock(Block geo)
        {
            geo.Width.Define(Length);
            geo.Height.Define(AngleHeight);
            geo.Depth.Define(AngleThickness);
            geo.Position.Define(() => Helper.Vxyz(-geo.Width.Value / 2, (-geo.Height.Value / 2), (-geo.Depth.Value / 2) + HoleHorizOffset.Value));
        }

        public Block FrontEndBlock { get; set; }
        protected virtual void ConfigureFrontEndBlock(Block geo)
        {
            geo.Width.Define(AngleThickness);
            geo.Height.Define(EndBlockHeight);
            geo.Depth.Define(AngleWidth);
            geo.Position.Define(() => Helper.Vxyz(-geo.Width.Value / 2, (-geo.Height.Value / 2), (-geo.Depth.Value / 2) + HoleHorizOffset.Value));
        }

        public Block RearEndBlock { get; set; }
        protected virtual void ConfigureRearEndBlock(Block geo)
        {
            geo.Width.Define(AngleThickness);
            geo.Height.Define(EndBlockHeight);
            geo.Depth.Define(AngleWidth);
            geo.Position.Define(() => Helper.Vxyz((geo.Width.Value / 2) - Length.Value, (-geo.Height.Value / 2), (-geo.Depth.Value / 2) + HoleHorizOffset.Value));
        }

        //public UCS UCS { get; set; }
        //protected virtual void ConfigureUCS(UCS child)
        //{
        //    child.Diameter.Define(0.125f);
        //    child.Position.Define(() => (Helper.Vy(0)));
        //}

        //public UCS HoleUCS { get; set; }
        //protected virtual void ConfigureHoleUCS(UCS child)
        //{
        //    child.Diameter.Define(0.125f);
        //    child.Position.Define(() => (Helper.Vy(-VertOffsetToHole.Value)));
        //}

        #endregion

        #region Protected Methods

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureAngleRules();
            this.ConfigureRevisablePart();
        }

        #endregion

    }
}
