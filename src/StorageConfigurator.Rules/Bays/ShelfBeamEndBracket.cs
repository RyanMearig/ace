﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Cpq;
using Ruler.Rules;
using Ruler.Rules.Graphics;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class ShelfBeamEndBracket : AdvancePart, IBomPart, IRevisablePart, Ruler.Cpq.IBomPart
    {
        #region Constructors

        public ShelfBeamEndBracket(ModelContext modelContext) : base(modelContext) { }

        public ShelfBeamEndBracket(string name, Part parent) : base(name, parent) { }

        #endregion

        #region Rules

        public Rule<string> Type { get; set; }
        protected virtual void ConfigureType(Rule<string> rule)
        {
            rule.Define(default(string));
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(new List<ChoiceListItem>()
                {
                    new ChoiceListItem("8", @"8"""),
                    new ChoiceListItem("8infAdj", @"8"" inf adj"),
                    new ChoiceListItem("9", @"9"""),
                    new ChoiceListItem("12", @"12"""),
                    new ChoiceListItem("13", @"13""")
                });
            });
        }

        public Rule<float> Thickness { get; set; }
        protected virtual void ConfigureThickness(Rule<float> rule)
        {
            //7ga.
            rule.Define(0.179f);
            rule.AddToCAD();
        }

        public Rule<float> Height { get; set; }
        protected virtual void ConfigureHeight(Rule<float> rule)
        {
            rule.Define(() =>
            {
                float retVal = 0;

                if (Type.Value.Contains("8"))
                {
                    retVal = 8;
                }
                else if (Type.Value.Contains("9"))
                {
                    retVal = 9;
                }
                else if (Type.Value.Contains("12"))
                {
                    retVal = 12;
                }
                else
                {
                    retVal = 13;
                }

                return (retVal - 0.125f);
            });
            rule.AddToCAD();
        }

        public Rule<float> MountFlangeLength { get; set; }
        protected virtual void ConfigureMountFlangeLength(Rule<float> rule)
        {
            rule.Define(() => 1.595f + Thickness.Value);
            rule.AddToCAD();
        }
        
        public Rule<float> BeamFlangeLength { get; set; }
        protected virtual void ConfigureBeamFlangeLength(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (Type.Value == "8infAdj")
                {
                    return 2.59375f + Thickness.Value;
                }
                else if (Height.Value > 9)
                {
                    return 2.25f;
                }
                else
                {
                    return 1.75f;
                }
            });
            rule.AddToCAD();
        }

        [CadRule(CadRuleUsageType.Parameter)]
        public Rule<float> HoleSpacing { get; set; }
        protected virtual void ConfigureHoleSpacing(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (Type.Value.Contains("9") || Type.Value.Contains("13"))
                    return 1;
                else
                    return 2;
            });
        }

        [CadRule(CadRuleUsageType.Parameter)]
        public Rule<float> HoleOffset { get; set; }
        protected virtual void ConfigureHoleOffset(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (Type.Value == "8infAdj")
                {
                    return 1.0625f;
                }
                else if (Type.Value.Contains("13"))
                {
                    return 1.074f;
                }
                else
                {
                    return 1.070f;
                }
            });
        }

        public Rule<float> HoleVertOffset { get; set; }
        protected virtual void ConfigureHoleVertOffset(Rule<float> rule)
        {
            rule.Define(() => (Type.Value == "8infAdj") ? 0.96875f : 0.9375f);
        }

        [CadRule(CadRuleUsageType.Parameter)]
        public Rule<float> HoleQty { get; set; }
        protected virtual void ConfigureHoleQty(Rule<float> rule)
        {
            rule.Define(() => (float)Math.Ceiling((Height.Value - HoleVertOffset.Value)/HoleSpacing.Value));
        }

        [CadRule(CadRuleUsageType.Parameter)]
        public Rule<float> SlotLength { get; set; }
        protected virtual void ConfigureSlotLength(Rule<float> rule)
        {
            rule.Define(() => (Type.Value == "8infAdj") ? 2.5f : 0);
        }


        [CadRule(CadRuleUsageType.Property)]
        public ReadOnlyRule<string> MaterialPartNumber { get; set; }
        protected virtual void ConfigureMaterialPartNumber(ReadOnlyRule<string> rule)
        {
            rule.Define(DatabasePartNumber);
        }

        public Dictionary<string, string> ClipDescriptions = new Dictionary<string, string>()
        {
            { "8", "7-7/8\" FORMED CLIP, 2\" ADJ."},
            { "8infAdj", "7-7/8\" FORMED SLOTTED CLIP, INF ADJ."},
            { "9", "8-7/8\" FORMED CLIP, 2\" ADJ."},
            { "12", "11-7/8\" FORMED CLIP, 2\" ADJ."},
            { "13", "12-7/8\" FORMED CLIP, 2\" ADJ."},
        };
        [CadRule(CadRuleUsageType.Property)]
        public ReadOnlyRule<string> ClipDesc { get; set; }
        protected virtual void ConfigureClipDesc(ReadOnlyRule<string> rule)
        {
            rule.Define(() => ClipDescriptions.TryGetValue(Type.Value, out var desc) ? desc : string.Empty);
        }

        #endregion

        #region IRevisablePartImplementation

        public FactoryRevisionRule<string> FactoryRevision { get; set; }

        #endregion

        #region IBomPart Implementation

        public Rule<string> DatabasePartNumber { get; set; }
        public PartNumberRule<string> PartNumber { get; set; }
        public Rule<string> PartNumberPrefix { get; set; }
        public Rule<string> Description { get; set; }
        public Rule<string> BOMDescription { get; set; }
        protected virtual void ConfigureBOMDescription(Rule<string> rule)
        {
            rule.Define(ClipDesc);
        }
        public Rule<string> BOMDescription1 { get; set; }
        public Rule<string> BOMDescription2 { get; set; }
        public Rule<string> BOMDescription3 { get; set; }
        public Rule<string> BOMDescription4 { get; set; }
        public Rule<string> BOMDescription5 { get; set; }
        public Rule<string> BOMDescription6 { get; set; }
        public Rule<string> BOMDescription7 { get; set; }
        public Rule<string> BOMDescription8 { get; set; }
        public Rule<string> BOMDescription9 { get; set; }
        public Rule<string> BOMDescription10 { get; set; }
        public Rule<string> BOMDescription11 { get; set; }
        public Rule<string> Color { get; set; }
        protected virtual void ConfigureColor(Rule<string> rule)
        {
            rule.Define("");
        }
        public ReadOnlyRule<AdvanceColor> AdvanceColor { get; set; }
        //protected virtual void ConfigureAdvanceColor(ReadOnlyRule<AdvanceColor> rule)
        //{
        //    rule.Define(() => null);
        //}
        public ReadOnlyRule<string> ColorDescription { get; set; }
        protected virtual void ConfigureColorDescription(ReadOnlyRule<string> rule)
        {
            rule.Define("");
        }
        public StorageConfiguratorRepository Repository { get; set; }
        public Rule<string> ColorBOMName { get; set; }
        public ReadOnlyRule<string> ColorShortDescription { get; set; }

        public Rule<double> PartWeight { get; set; }
        public Rule<double> PartPaintWeight { get; set; }
        public Rule<float> WeightPerGallon { get; set; }
        public CsiRule<double> PaintCostPerGallon { get; set; }
        public Rule<float> PaintCost { get; set; }

        #endregion

        #region CPQ.IBomPart Implementation

        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {
            child.PartNumber.Define(PartNumber);
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(16753920);
            material.Opacity.Define(0.9f);
        }

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define("LoadBeamClip");
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(false);
        }

        #endregion

        #region Graphics

        public Block MountFlangeBlock { get; set; }
        protected virtual void ConfigureMountFlangeBlock(Block geo)
        {
            geo.Width.Define(Thickness);
            geo.Height.Define(Height);
            geo.Depth.Define(MountFlangeLength);
            geo.Position.Define(() => Helper.Vxyz(-(Thickness.Value / 2), -Height.Value / 2, (-MountFlangeLength.Value / 2) + Thickness.Value));
        }

        public Block BeamFlangeBlock { get; set; }
        protected virtual void ConfigureBeamFlangeBlock(Block geo)
        {
            geo.Width.Define(BeamFlangeLength);
            geo.Height.Define(Height);
            geo.Depth.Define(Thickness);
            geo.Position.Define(() => Helper.Vxyz((geo.Width.Value / 2) - Thickness.Value, -Height.Value / 2, geo.Depth.Value / 2));
        }

        //public UCS UCS { get; set; }
        //protected virtual void ConfigureUCS(UCS child)
        //{
        //    child.Diameter.Define(0.125f);
        //    child.Position.Define(() => (Helper.Vy(0)));
        //}

        #endregion

        #region Protected Methods

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureBomPartRules();
            this.ConfigureRevisablePart();
        }

        #endregion
    }
}
