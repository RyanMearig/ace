﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Cpq;
using Ruler.Rules;
using Ruler.Rules.Graphics;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class ShelfPalletStopMaterial : AdvancePart, Ruler.Cpq.IBomPart, IBomPart
    {
        #region Constructors

        public ShelfPalletStopMaterial(ModelContext modelContext, StorageConfiguratorRepository repo) : base(modelContext)
        {
            Repository = repo;
        }

        public ShelfPalletStopMaterial(string name, Part parent, StorageConfiguratorRepository repo) : base(name, parent)
        {
            Repository = repo;
        }

        #endregion

        #region Rules

        public ReadOnlyRule<float> RawLength { get; set; }
        protected virtual void ConfigureRawLength(ReadOnlyRule<float> rule)
        {
            rule.Define(default(float));
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> Style { get; set; }
        protected virtual void ConfigureStyle(Rule<string> rule)
        {
            rule.Define(default(string));
        }

        public Rule<float> Width { get; set; }
        protected virtual void ConfigureWidth(Rule<float> rule)
        {
            rule.Define(default(float));
        }

        public Rule<float> Thickness { get; set; }
        protected virtual void ConfigureThickness(Rule<float> rule)
        {
            rule.Define(default(float));
        }

        public Rule<float> BeamHeight { get; set; }
        protected virtual void ConfigureBeamHeight(Rule<float> rule)
        {
            rule.Define(default(float));
        }

        public Rule<string> MaterialPartNumber { get; set; }
        protected virtual void ConfigureMaterialPartNumber(Rule<string> rule)
        {
            rule.Define(default(string));
        }

        public Rule<string> SelectivePartNumberPrefix { get; set; }
        protected virtual void ConfigureSelectivePartNumberPrefix(Rule<string> rule)
        {
            rule.Define(default(string));
        }

        public Rule<string> DDPartNumberPrefix { get; set; }
        protected virtual void ConfigureDDPartNumberPrefix(Rule<string> rule)
        {
            rule.Define(default(string));
        }

        #endregion

        #region Labor Hour Rules

        public Rule<double> IronworkerCutPartCarriersPerHour { get; set; }
        protected virtual void ConfigureIronworkerCutPartCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> IronworkerPunchPartCarriersPerHour { get; set; }
        protected virtual void ConfigureIronworkerPunchPartCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> AccurpressForm1PartCarriersPerHour { get; set; }
        protected virtual void ConfigureAccurpressForm1PartCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> AccurpressForm2PartCarriersPerHour { get; set; }
        protected virtual void ConfigureAccurpressForm2PartCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        #endregion

        #region IBomPart Implementation

        public Rule<string> DatabasePartNumber { get; set; }
        public Rule<string> PartNumberPrefix { get; set; }
        protected virtual void ConfigurePartNumberPrefix(Rule<string> rule)
        {
            //rule.Define(() => (Style.Value == "PS-R-X") ? SelectivePartNumberPrefix.Value : DDPartNumberPrefix.Value);
            rule.Define(() =>
            {
                return (Style.Value == "PS-R-X") ? SelectivePartNumberPrefix.Value : DDPartNumberPrefix.Value;
            });
        }
        public PartNumberRule<string> PartNumber { get; set; }
        public Rule<string> Description { get; set; }
        public Rule<string> BOMDescription { get; set; }
        public Rule<string> BOMDescription1 { get; set; }
        public Rule<string> BOMDescription2 { get; set; }
        public Rule<string> BOMDescription3 { get; set; }
        public Rule<string> BOMDescription4 { get; set; }
        public Rule<string> BOMDescription5 { get; set; }
        public Rule<string> BOMDescription6 { get; set; }
        public Rule<string> BOMDescription7 { get; set; }
        public Rule<string> BOMDescription8 { get; set; }
        public Rule<string> BOMDescription9 { get; set; }
        public Rule<string> BOMDescription10 { get; set; }
        public Rule<string> BOMDescription11 { get; set; }

        public Rule<string> Color { get; set; }
        public ReadOnlyRule<AdvanceColor> AdvanceColor { get; set; }
        public ReadOnlyRule<string> ColorDescription { get; set; }
        public StorageConfiguratorRepository Repository { get; set; }
        public Rule<string> ColorBOMName { get; set; }
        public ReadOnlyRule<string> ColorShortDescription { get; set; }

        public Rule<double> PartWeight { get; set; }
        public Rule<double> PartPaintWeight { get; set; }
        protected virtual void ConfigurePartPaintWeight(Rule<double> rule)
        {
            rule.Define(0);
        }
        public Rule<float> WeightPerGallon { get; set; }
        public CsiRule<double> PaintCostPerGallon { get; set; }
        public Rule<float> PaintCost { get; set; }


        #endregion

        #region Ruler CPQ IBomPart Implementation

        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {
            child.IsPurchased.Define(false);
            child.PartNumber.Define(PartNumber);
            child.Description.Define(Description);
            child.MaterialPartNumber.Define(MaterialPartNumber);
            child.MaterialQty.Define(() =>
            {
                return RawLength.Value / 12;
            });
            child.ItemMaterialCost.Define(() => child.ItemRawMaterialWeight.Value * child.MaterialUnitCost.Value);
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(Colors.Gray);
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(false);
        }

        public override bool IsCadPart => false;

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureBomPartRules();
        }

        #endregion

    }
}
