﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class CrossbarComponent : PaintedPart
    {
        #region Constructors

        public CrossbarComponent(ModelContext modelContext, StorageConfiguratorRepository repo) : base(modelContext)
        {
            Repository = repo;
        }

        public CrossbarComponent(string name, Part parent, StorageConfiguratorRepository repo) : base(name, parent)
        {
            Repository = repo;
        }

        #endregion

        #region Rules

        public Rule<bool> IsStandalone { get; set; }
        protected virtual void ConfigureIsStandalone(Rule<bool> rule)
        {
            rule.Define(() => (Parent == null || Parent.Parent?.GetType() == typeof(ShippableParts)));
        }

        public Rule<double> Qty { get; set; }
        protected virtual void ConfigureQty(Rule<double> rule)
        {
            rule.Define(1);
            rule.AddToUI(ui =>
            {
                ui.Label.Define("Quantity");
                ui.ReadOnly.Define(() => !IsStandalone.Value);
            });
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<float> OverallLength { get; set; }
        protected virtual void ConfigureOverallLength(Rule<float> rule)
        {
            rule.Define(default(float));
        }

        public Rule<string> BeamSize { get; set; }
        protected virtual void ConfigureBeamSize(Rule<string> rule)
        {
            rule.Define(default(string));
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> rulerDrawingXml { get; set; }
        protected virtual void ConfigurerulerDrawingXml(Rule<string> rule)
        {
            rule.Define("Crossbar");
        }

        [CadRule(CadRuleUsageType.Property)]
        public ReadOnlyRule<string> CrossbarDescription1 { get; set; }
        protected virtual void ConfigureCrossbarDescription1(ReadOnlyRule<string> rule)
        {
            rule.Define("");
        }

        [CadRule(CadRuleUsageType.Property)]
        public ReadOnlyRule<string> CrossbarDescription2 { get; set; }
        protected virtual void ConfigureCrossbarDescription2(ReadOnlyRule<string> rule)
        {
            rule.Define($"CROSSBAR");
        }

        

        #endregion


    }
}
