﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;
using Ruler.Cpq;
using Ruler.Rules.Graphics;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class ShelfPalletStop : PaintedPart, ILoadBeamData, IRevisablePart, Ruler.Cpq.IBomPart
    {
        #region Constructors

        public ShelfPalletStop(ModelContext modelContext, StorageConfiguratorRepository repo) : base(modelContext)
        {
            Repository = repo;
        }

        public ShelfPalletStop(string name, Part parent, StorageConfiguratorRepository repo) : base(name, parent)
        {
            Repository = repo;
        }

        #endregion

        #region Rules

        public Rule<string> Style { get; set; }
        protected virtual void ConfigureStyle(Rule<string> rule)
        {
            rule.Define("PS-R-X");
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(new List<ChoiceListItem>()
                {
                    new ChoiceListItem("PS-R-X", "PS-R-X"),
                    new ChoiceListItem("PS-L-DD-X", "PS-L-DD-X")
                });
            });
            rule.ValueSet += (sender, e) =>
            {
                Width.Reset();
                Offset.Reset();
            };
        }

        public Rule<bool> IsStandalone { get; set; }
        protected virtual void ConfigureIsStandalone(Rule<bool> rule)
        {
            rule.Define(() => (Parent == null || Parent.GetType() != typeof(Level)));
        }

        public Rule<double> Qty { get; set; }
        protected virtual void ConfigureQty(Rule<double> rule)
        {
            rule.Define(1);
            rule.AddToUI(ui =>
            {
                ui.Label.Define("Quantity");
                ui.ReadOnly.Define(() => !IsStandalone.Value);
            });
        }

        public Rule<string> BeamSize { get; set; }
        protected virtual void ConfigureBeamSize(Rule<string> rule)
        {
            rule.Define("C4x4.5");
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() => LoadBeamSizeChoiceList.Value);
            });
        }

        public Rule<float> BeamHeight { get; set; }
        protected virtual void ConfigureBeamHeight(Rule<float> rule)
        {
            rule.Define(() =>
            {
                string retVal = "";

                if (BeamSize.Value.ToUpper().Contains("C10"))
                    retVal = BeamSize.Value.Substring(1, 2);
                else
                    retVal = BeamSize.Value.Substring(1, 1);

                return (string.IsNullOrWhiteSpace(retVal) ? 3 : Convert.ToSingle(retVal));
            });
        }

        public Rule<float> Height { get; set; }
        protected virtual void ConfigureHeight(Rule<float> rule)
        {
            rule.Define(() =>
            {
                float retVal = 5.5f; //Offset from top to hole location

                if (BeamSize.Value.ToUpper().Contains("C3"))
                    retVal = retVal + 2.125f;
                else if (BeamSize.Value.ToUpper().Contains("C4"))
                    retVal = retVal + 3.125f;
                else if (BeamSize.Value.ToUpper().Contains("C5"))
                    retVal = retVal + 4.125f;
                else if (BeamSize.Value.ToUpper().Contains("C6"))
                    retVal = retVal + 5.125f;
                else
                    retVal = retVal + 6.125f;

                return retVal;
            });
            rule.AddToCAD();
        }

        public Rule<float> RawLength { get; set; }
        protected virtual void ConfigureRawLength(Rule<float> rule)
        {
            rule.Define(() => 
            {
                if (Style.Value == "PS-R-X")
                {
                    return Height.Value + Offset.Value + 0.4375f;
                }
                return Height.Value + 0.875f;
            });
        }

        public Rule<float> Width { get; set; }
        protected virtual void ConfigureWidth(Rule<float> rule)
        {
            rule.Define(4);
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() =>
                {
                    if (Style.Value == "PS-R-X")
                    {
                        return new List<ChoiceListItem>()
                        {
                            new ChoiceListItem(3),
                            new ChoiceListItem(4),
                            new ChoiceListItem(8),
                            new ChoiceListItem(12)
                        };
                    }
                    else if (Style.Value == "PS-L-DD-X")
                    {
                        return new List<ChoiceListItem>()
                        {
                            new ChoiceListItem(4)
                        };
                    }
                    else
                    {
                        return new List<ChoiceListItem>();
                    }
                });
            });
            rule.AddToCAD();
        }

        public Rule<float> Offset { get; set; }
        protected virtual void ConfigureOffset(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (Style.Value == "PS-L-DD-X")
                    return 0;
                else
                    return 2;
            });
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => (Style.Value == "PS-L-DD-X"));
                ui.ChoiceList.Define(() =>
                {
                    if (Style.Value == "PS-R-X")
                    {
                        return new List<ChoiceListItem>()
                        {
                            new ChoiceListItem(2),
                            new ChoiceListItem(3),
                            new ChoiceListItem(4)
                        };
                    }
                    else if (Style.Value == "PS-L-DD-X")
                    {
                        return new List<ChoiceListItem>()
                        {
                            new ChoiceListItem(0)
                        };
                    }
                    else
                    {
                        return new List<ChoiceListItem>();
                    }
                });
            });
            rule.AddToCAD();
        }

        public Rule<float> HoleDia { get; set; }
        protected virtual void ConfigureHoleDia(Rule<float> rule)
        {
            rule.Define(0.5625f);
        }

        public Rule<float> Thickness { get; set; }
        protected virtual void ConfigureThickness(Rule<float> rule)
        {
            rule.Define(0.375f);
        }

        public Rule<float> VerticalOffset { get; set; }
        protected virtual void ConfigureVerticalOffset(Rule<float> rule)
        {
            rule.Define(4);
        }

        public Rule<float> BeamHoleSize { get; set; }
        protected virtual void ConfigureBeamHoleSize(Rule<float> rule)
        {
            rule.Define(0.4375f);
        }

        #endregion

        #region IChannelData Implementation

        public Rule<List<ChannelData>> AllChannelData { get; set; }
        public Rule<List<ChoiceListItem>> AllChannelSizesChoiceList { get; set; }

        #endregion

        #region ILoadBeamData Implementation
        public Rule<List<string>> BeamSizesAvailable { get; set; }
        public Rule<List<ChoiceListItem>> LoadBeamSizeChoiceList { get; set; }
        public Rule<List<ChoiceListItem>> OrientationChoiceList { get; set; }
        public Rule<List<ChoiceListItem>> BracketTypeChoiceList { get; set; }
        #endregion

        #region IBomPart Implementation

        protected virtual void ConfigurePartNumberPrefix(Rule<string> rule)
        {
            rule.Define(() =>
            {
                if (Style.Value == "PS-R-X")
                {
                    return $"PSR{BeamHeight.Value}-";
                }
                else
                {
                    return $"PSLDD{BeamHeight.Value}-";
                }
            });
        }

        protected virtual void ConfigureDescription(Rule<string> rule)
        {
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(false);
            });
        }

        protected virtual void ConfigureBOMDescription(Rule<string> rule)
        {
            rule.Define(() => $"Rear Pallet Stop {BeamHeight.Value}\" Beam");
        }

        protected virtual void ConfigurePartWeight(Rule<double> rule)
        {
            rule.Define(() => this.GetPartWeightWithoutPaint());
        }

        #endregion

        #region IRevisablePartImplementation

        public FactoryRevisionRule<string> FactoryRevision { get; set; }

        #endregion

        #region CPQ.IBomPart Implementation

        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {
            child.PartNumber.Define(PartNumber);
            child.ItemQty.Define(Qty);
            child.Description.Define(BOMDescription1);
        }

        #endregion

        #region Labor Rules

        public Rule<double> PartsPerCarrier { get; set; }
        protected virtual void ConfigurePartsPerCarrier(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> PaintCarriersPerHour { get; set; }
        protected virtual void ConfigurePaintCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> UnloadCarriersPerHour { get; set; }
        protected virtual void ConfigureUnloadCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        #endregion

        #region Children

        public ShelfPalletStopMaterial ShelfPalletStopMaterial { get; set; }
        protected virtual void ConfigureShelfPalletStopMaterial(ShelfPalletStopMaterial child)
        {
            child.RawLength.Define(RawLength);
            child.Style.Define(Style);
            child.Width.Define(Width);
            child.Thickness.Define(Thickness);
            child.BeamHeight.Define(BeamHeight);
            child.Description.Define(BOMDescription1);
        }

        public HardwareGroup Hardware { get; set; }
        protected virtual void ConfigureHardware(HardwareGroup child)
        {
            child.BoltLength.Define(1.75f);
            child.BoltSize.Define(() => BeamHoleSize.Value <= .4375f ? .375f : 0.5f);
            child.BoltType.Define(BoltTypes.TapHex);
            child.BoltMaterial.Define(HardwareMaterial.Grade5);
            child.Qty.Define(1);
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(Colors.Blue);
            material.Opacity.Define(0.9f);
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(IsStandalone);
        }

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define(() =>
            {
                if (Style.Value == "PS-R-X")
                    return "SelectivePalletStop";
                else
                    return "DoubleDeepPalletStop";
            });
        }

        #endregion

        #region Graphics

        public Block PalletStopBlock { get; set; }
        protected virtual void ConfigurePalletStopBlock(Block geo)
        {
            geo.Width.Define(Thickness);
            geo.Height.Define(() => (Style.Value == "PS-R-X") ? 4 : Height.Value);
            geo.Depth.Define(Width);
            geo.Position.Define(() =>
            {
                if (Style.Value == "PS-R-X")
                    return Helper.Vxy(-(Offset.Value + (geo.Width.Value / 2)), geo.Height.Value / 2);
                else
                    return Helper.Vxy(-((geo.Width.Value / 2)), -(geo.Height.Value / 2) + 4);
            });
        }

        public Block MidHorizontalBlock { get; set; }
        protected virtual void ConfigureMidHorizontalBlock(Block geo)
        {
            geo.IsActive.Define(() => (Style.Value == "PS-R-X"));
            geo.Width.Define(() => Offset.Value + Thickness.Value);
            geo.Height.Define(Thickness);
            geo.Depth.Define(Width);
            geo.Position.Define(() => Helper.Vxy(-geo.Width.Value / 2, -geo.Height.Value / 2));
        }

        public Block BtmVerticalBlock { get; set; }
        protected virtual void ConfigureBtmVerticalBlock(Block geo)
        {
            geo.IsActive.Define(() => (Style.Value == "PS-R-X"));
            geo.Width.Define(Thickness);
            geo.Height.Define(() => Height.Value - 4);
            geo.Depth.Define(Width);
            geo.Position.Define(() => Helper.Vxy(-geo.Width.Value / 2, -geo.Height.Value/2));
        }

        public Block BtmHorizontalBlock { get; set; }
        protected virtual void ConfigureBtmHorizontalBlock(Block geo)
        {
            geo.Width.Define(1.5f);
            geo.Height.Define(Thickness);
            geo.Depth.Define(Width);
            geo.Position.Define(() => Helper.Vxy((geo.Width.Value / 2) - Thickness.Value, -((geo.Height.Value / 2) + BtmVerticalBlock.Height.Value)));
        }

        //public UCS UCS { get; set; }
        //protected virtual void ConfigureUCS(UCS child)
        //{
        //    child.Diameter.Define(0.25f);
        //    child.Position.Define(() => (Helper.Vy(0)));
        //}

        #endregion

        #region Protected Methods

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureLoadBeamDataRules();
            this.ConfigureBomPartRules();
            this.ConfigureRevisablePart();
        }

        #endregion

    }
}
