﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ruler.Cpq;
using Ruler.Rules;
using Ruler.Rules.Graphics;
using Ruler.Rules.Validation;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class Bay : AdvancePart, IDepthMaster, IColorComponent, IRevisablePart, Ruler.Cpq.IBomPart
    {
        #region Constructors

        public Bay(ModelContext modelContext, StorageConfiguratorRepository repo) : base(modelContext)
        {
            Repository = repo;
        }

        public Bay(string name, Part parent, StorageConfiguratorRepository repo) : base(name, parent)
        {
            this.SetIndex();
            Repository = repo;
        }

        #endregion

        #region Rules

        public LookupRule<StorageSystem> StorageSystemComp { get; set; }

        public Rule<Bays> BaysParent { get; set; }
        protected virtual void ConfigureBaysParent(Rule<Bays> rule)
        {
            rule.Define(() => (Bays)Parent);
        }

        protected override void ConfigureDisplayName(Rule<string> rule)
        {
            rule.Define(BayName);
        }

        //Left in to work with old projects
        public Rule<string> BayDisplay { get; set; }
        protected virtual void ConfigureBayDisplay(Rule<string> rule)
        {
            rule.Define("");
        }

        [UIRule]
        public Rule<string> BayName { get; set; }
        protected virtual void ConfigureBayName(Rule<string> rule)
        {
            rule.Define(() => string.IsNullOrWhiteSpace(BayDisplay.Value) ? $"Bay {Index.Value}" : BayDisplay.Value);
            rule.Unique();
        }

        public Rule<string> BayDrawingNumber { get; set; }
        protected virtual void ConfigureBayDrawingNumber(Rule<string> rule)
        {
            rule.Define(() =>
            {
                var id = Convert.ToChar(64 + Index.Value);
                return $"{id}1";
            });
            rule.Unique();
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(IsUsedInElevation);
            });
        }

        public Rule<int> Index { get; set; }
        protected virtual void ConfigureIndex(Rule<int> rule)
        {
            rule.Define(0);
        }

        [UIRule]
        public ReadOnlyRule<float> BayDepth { get; set; }
        protected virtual void ConfigureBayDepth(ReadOnlyRule<float> rule)
        {
            rule.Define(OverallDepth);
            rule.AddToCAD(cad =>
            {
                cad.CadRuleUsageType = CadRuleUsageType.Parameter;
                cad.NameInCad = "Width";
            });
        }

        public Rule<float> FirstLevelConsumedSpace { get; set; }
        protected virtual void ConfigureFirstLevelConsumedSpace(Rule<float> rule)
        {
            rule.Define(() => ((FirstActiveLevel.Value.LoadWidth.Value * LoadsWide.Value) + (FirstActiveLevel.Value.LoadToLoadOffset.Value * (LoadsWide.Value - 1))));
        }

        [UIRule]
        public Rule<float> BayWidth { get; set; }
        protected virtual void ConfigureBayWidth(Rule<float> rule)
        {
            rule.Define(() => FirstLevelConsumedSpace.Value + 8);
            rule.Min(BayWidthMin);
            rule.AddToCAD(cad =>
            {
                cad.CadRuleUsageType = CadRuleUsageType.Parameter;
                cad.NameInCad = "Depth";
            });
        }

        public Rule<float> BayWidthMin { get; set; }
        protected virtual void ConfigureBayWidthMin(Rule<float> rule)
        {
            rule.Define(() => FirstLevelConsumedSpace.Value + (FirstActiveLevel.Value.LoadToFrameOffsetMin.Value*2));
        }

        [UIRule]
        public ReadOnlyRule<float> BayHeight { get; set; }
        protected virtual void ConfigureBayHeight(ReadOnlyRule<float> rule)
        {
            rule.Define(() => LastActiveLevel.Value.Elevation.Value + LastActiveLevel.Value.PalletHeight.Value);
            rule.AddToCAD(cad =>
            {
                cad.CadRuleUsageType = CadRuleUsageType.Parameter;
                cad.NameInCad = "Height";
            });
        }

        public Rule<int> LoadsWide { get; set; }
        protected virtual void ConfigureLoadsWide(Rule<int> rule)
        {
            rule.Define(1);
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() => (new List<ChoiceListItem>()
                {
                    new ChoiceListItem(1),
                    new ChoiceListItem(2),
                    new ChoiceListItem(3),
                    new ChoiceListItem(4)
                }));
            });
        }

        [UIRule]
        public Rule<int> LevelQty { get; set; }
        protected virtual void ConfigureLevelQty(Rule<int> rule)
        {
            rule.Define(4);
            rule.Min(1);
        }

        [UIRule]
        public Rule<bool> ShowProducts { get; set; }
        protected virtual void ConfigureShowProducts(Rule<bool> rule)
        {
            rule.Define(true);
        }

        public Rule<Level> FirstActiveLevel { get; set; }
        protected virtual void ConfigureFirstActiveLevel(Rule<Level> rule)
        {
            rule.Define(() => IsDoubleDeep.Value ? (Level)DoubleDeepLevel.FirstOrDefault() : (Level)SelectiveLevel.FirstOrDefault());
        }

        public Rule<Level> LastActiveLevel { get; set; }
        protected virtual void ConfigureLastActiveLevel(Rule<Level> rule)
        {
            rule.Define(() => IsDoubleDeep.Value ? (Level)DoubleDeepLevel.LastOrDefault() : (Level)SelectiveLevel.LastOrDefault());
        }

        [CadRule(CadRuleUsageType.Property, "IsDoubleDeep")]
        public Rule<string> IsDoubleDeepProp { get; set; }
        protected virtual void ConfigureIsDoubleDeepProp(Rule<string> rule)
        {
            rule.Define(() => IsDoubleDeep.Value.ToString());
        }

        [CadRule(CadRuleUsageType.Property, "IsDoubleDeepPost")]
        public Rule<string> IsDoubleDeepPostProp { get; set; }
        protected virtual void ConfigureIsDoubleDeepPostProp(Rule<string> rule)
        {
            rule.Define(() => IsDoubleDeepPost.Value.ToString());
        }

        [UIRule]
        public ReadOnlyRule<bool> IsUsedInElevation { get; set; }
        protected virtual void ConfigureIsUsedInElevation(ReadOnlyRule<bool> rule)
        {
            rule.Define(() =>
            {
                if (StorageSystemComp.Value != null && StorageSystemComp.Value.BaysUsedInElevations.Value.Any())
                {
                    return StorageSystemComp.Value.BaysUsedInElevations.Value.Any(f => f.Path == this.Path);
                }
                else
                    return false;
            });
        }

        [CadRule(CadRuleUsageType.Property)]
        public LookupRule<string> LineItemName { get; set; }
        protected virtual void ConfigureLineItemName(LookupRule<string> rule)
        {
            rule.Define("[[LineItemName]]");
        }

        public PartNumberRule<string> PartNumber { get; set; }
        protected virtual void ConfigurePartNumber(PartNumberRule<string> rule)
        {
            rule.Define(() => $"{LineItemName.Value}_{BayDrawingNumber.Value}");
            rule.AddToCAD(cad =>
            {
                cad.CadRuleUsageType = CadRuleUsageType.Property;
                cad.NameInCad = "Part Number";
            });
        }

        #endregion

        #region IDepthMaster Implementation

        public Rule<bool> IsDoubleDeep { get; set; }
        public Rule<string> DoubleDeepType { get; set; }
        public Rule<bool> IsDoubleDeepPost { get; set; }
        public Rule<float> FrontFrameDepth { get; set; }
        protected virtual void ConfigureFrontFrameDepth(Rule<float> rule)
        {
            rule.Define(() =>  
            {
                float retVal = 0;

                if (FirstActiveLevel.Value.ActiveProduct.Value != null)
                {
                    if (IsDoubleDeep.Value)
                    {
                        if (IsDoubleDeepPost.Value)
                            retVal = FirstActiveLevel.Value.ActiveProduct.Value.PalletLoadDepth.Value + 1;
                        else
                            retVal = FirstActiveLevel.Value.ActiveProduct.Value.PalletLoadDepth.Value;
                    }
                    else
                        retVal = FirstActiveLevel.Value.ActiveProduct.Value.PalletLoadDepth.Value;
                }
                else
                    retVal = 48;

                return retVal;
            });
            rule.AddToUI(ui =>
            {
                ui.Label.Define(() => IsDoubleDeep.Value ? "Front Frame Depth" : "Frame Depth");
            });
        }

        public Rule<float> RearFrameDepth { get; set; }
        protected virtual void ConfigureRearFrameDepth(Rule<float> rule)
        {
            rule.Define(() =>
            {
                float retVal = 0;

                if (FirstActiveLevel.Value.ActiveProduct.Value != null)
                {
                    if (IsDoubleDeep.Value)
                    {
                        if (IsDoubleDeepPost.Value)
                            retVal = FirstActiveLevel.Value.ActiveProduct.Value.PalletLoadDepth.Value - 1;
                        else
                            retVal = FirstActiveLevel.Value.ActiveProduct.Value.PalletLoadDepth.Value;
                    }
                    else
                        retVal = 0;
                }
                else
                    retVal = FrontFrameDepth.Value;

                return retVal;
            });
            rule.AddToUI();
        }

        public Rule<bool> HasFrameTies { get; set; }
        public Rule<string> FrameTieType { get; set; }
        public Rule<float> FrameTieLengthMin { get; set; }
        public Rule<float> FrameTieLength { get; set; }
        public ReadOnlyRule<float> OverallDepth { get; set; }

        #endregion

        #region IColorComponent Implementation

        public Rule<string> Color { get; set; }
        public ReadOnlyRule<AdvanceColor> AdvanceColor { get; set; }
        public ReadOnlyRule<string> ColorDescription { get; set; }
        public StorageConfiguratorRepository Repository { get; set; }
        public Rule<string> ColorBOMName { get; set; }
        public ReadOnlyRule<string> ColorShortDescription { get; set; }

        public Rule<double> PartWeight { get; set; }
        public Rule<double> PartPaintWeight { get; set; }
        public Rule<float> WeightPerGallon { get; set; }
        public CsiRule<double> PaintCostPerGallon { get; set; }
        public Rule<float> PaintCost { get; set; }

        #endregion

        #region IRevisablePartImplementation

        public FactoryRevisionRule<string> FactoryRevision { get; set; }

        #endregion

        #region CPQ.IBomPart Implementation

        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {

        }

        #endregion

        #region Children

        public ChildList<SelectiveLevel> SelectiveLevel { get; set; }
        protected virtual void ConfigureSelectiveLevel(ChildList<SelectiveLevel> list)
        {
            list.Qty.Define(() => IsDoubleDeep.Value ? 0 : LevelQty.Value);
            list.ConfigureChildren((child, index) =>
            {
                child.IsDoubleDeep.Define(IsDoubleDeep);
                child.DoubleDeepType.Define(DoubleDeepType);
                child.LoadsWide.Define(LoadsWide);
                child.BayWidth.Define(BayWidth);
                child.PreviousLevel.Define(() => (index == 0 ? default(Level) : list[index - 1]));
                child.NextLevel.Define(() => (child.LevelIndex.Value >= list.Qty.Value ? default(Level) : list[index + 1]));
                child.LevelIndex.Define(() => index + 1);
                child.FrontFrameDepth.Define(FrontFrameDepth);
                child.ShowProducts.Define(ShowProducts);
            });
        }

        public ChildList<DoubleDeepLevel> DoubleDeepLevel { get; set; }

        protected virtual void ConfigureDoubleDeepLevel(ChildList<DoubleDeepLevel> list)
        {
            list.Qty.Define(() => IsDoubleDeep.Value ? LevelQty.Value : 0);
            list.ConfigureChildren((child, index) =>
            {
                child.IsDoubleDeep.Define(IsDoubleDeep);
                child.DoubleDeepType.Define(DoubleDeepType);
                child.FrameTieLength.Define(FrameTieLength);
                child.LoadsWide.Define(LoadsWide);
                child.BayWidth.Define(BayWidth);
                child.PreviousLevel.Define(() => (index == 0 ? default(Level) : (Level)list[index - 1]));
                child.NextLevel.Define(() => (child.LevelIndex.Value >= list.Qty.Value ? default(Level) : list[index + 1]));
                child.LevelIndex.Define(() => index + 1);
                child.FrontFrameDepth.Define(FrontFrameDepth);
                child.RearFrameDepth.Define(RearFrameDepth);
                child.ShowProducts.Define(ShowProducts);
            });
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(Colors.Yellow);
        }

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define("Bay");
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(true);
        }

        protected override void ConfigureAllowedChildTypes(ReadOnlyRule<IEnumerable<Type>> rule)
        {
            rule.Define(() => new List<Type>
            {
                typeof(CustomPart),
                typeof(BuyoutPart),
                typeof(ShelfLoadBeam),
                typeof(ShelfBeamCrossbar),
                typeof(ShelfPalletStop),
                typeof(TouchUpCan)
            });
        }

        #endregion

        #region Graphics

        //public UCS UCS { get; set; }
        //protected virtual void ConfigureUCS(UCS child)
        //{
        //    child.Position.Define(() => (Helper.Vy(0)));
        //}

        #endregion

        #region Protected Methods

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureDepthMasterRules();
            this.ConfigureColorComponent();
            this.ConfigureRevisablePart();
        }

        #endregion

        #region Private Methods

        public void SetIndex()
        {
            var existingBays = Parent.ActiveChildParts.Value
                    .Where(c => c != this && c.TypeName.Contains("Bay"));
            var index = existingBays.Count() + 1;
            this.Index.SetAdhocDefaultValue(index);
        }

        #endregion

    }
}
