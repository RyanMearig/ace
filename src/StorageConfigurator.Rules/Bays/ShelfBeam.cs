﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;
using Ruler.Rules.Validation;
using Ruler.Rules.Graphics;
using System.Linq;
using System.Numerics;
using StorageConfigurator.Data;
using Ruler.Cpq;

namespace StorageConfigurator.Rules
{
    public class ShelfBeam : Channel, Ruler.Cpq.IBomPart
    {
        #region Constructors

        public ShelfBeam(ModelContext modelContext, StorageConfiguratorRepository repo) : base(modelContext, repo) { }

        public ShelfBeam(string name, Part parent, StorageConfiguratorRepository repo) : base(name, parent, repo) { }

        #endregion

        #region Rules

        public Rule<bool> HasSquareHoles { get; set; }
        protected virtual void ConfigureHasSquareHoles(Rule<bool> rule)
        {
            rule.Define(() => SquareHoleSize.Value > 0);
        }

        public Rule<float> SquareHoleSize { get; set; }
        protected virtual void ConfigureSquareHoleSize(Rule<float> rule)
        {
            rule.Define(0);
            rule.AddToCAD();
        }

        public ReadOnlyRule<float> HoleDiameter { get; set; }
        protected virtual void ConfigureHoleDiameter(ReadOnlyRule<float> rule)
        {
            rule.Define(0.4375f);
            rule.AddToCAD();
        }

        public Rule<float> HoleVerticalOffset { get; set; }
        protected virtual void ConfigureHoleVerticalOffset(Rule<float> rule)
        {
            rule.Define(1.5f);
            rule.AddToCAD();
        }

        public Rule<List<float>> HoleLocations { get; set; }
        protected virtual void ConfigureHoleLocations(Rule<List<float>> rule)
        {
            rule.Define(default(List<float>));
        }

        public Rule<float> Hole1Offset { get; set; }
        protected virtual void ConfigureHole1Offset(Rule<float> rule)
        {
            rule.Define(() => GetLocationOrDefault(1));
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(() => !HasSquareHoles.Value);
                //cad.NameInCad = (HasSquareHoles.Value ? "Square" : string.Empty) + "Hole1Offset";
            });
        }

        public Rule<float> Hole2Offset { get; set; }
        protected virtual void ConfigureHole2Offset(Rule<float> rule)
        {
            rule.Define(() => GetLocationOrDefault(2));
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(() => !HasSquareHoles.Value);
            });
        }

        public Rule<float> Hole3Offset { get; set; }
        protected virtual void ConfigureHole3Offset(Rule<float> rule)
        {
            rule.Define(() => GetLocationOrDefault(3));
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(() => !HasSquareHoles.Value);
            });
        }

        public Rule<float> Hole4Offset { get; set; }
        protected virtual void ConfigureHole4Offset(Rule<float> rule)
        {
            rule.Define(() => GetLocationOrDefault(4));
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(() => !HasSquareHoles.Value);
            });
        }

        public Rule<float> Hole5Offset { get; set; }
        protected virtual void ConfigureHole5Offset(Rule<float> rule)
        {
            rule.Define(() => GetLocationOrDefault(5));
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(() => !HasSquareHoles.Value);
            });
        }

        public Rule<float> Hole6Offset { get; set; }
        protected virtual void ConfigureHole6Offset(Rule<float> rule)
        {
            rule.Define(() => GetLocationOrDefault(6));
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(() => !HasSquareHoles.Value);
            });
        }

        public Rule<float> Hole7Offset { get; set; }
        protected virtual void ConfigureHole7Offset(Rule<float> rule)
        {
            rule.Define(() => GetLocationOrDefault(7));
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(() => !HasSquareHoles.Value);
            });
        }

        public Rule<float> Hole8Offset { get; set; }
        protected virtual void ConfigureHole8Offset(Rule<float> rule)
        {
            rule.Define(() => GetLocationOrDefault(8));
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(() => !HasSquareHoles.Value);
            });
        }

        public Rule<float> Hole9Offset { get; set; }
        protected virtual void ConfigureHole9Offset(Rule<float> rule)
        {
            rule.Define(() => GetLocationOrDefault(9));
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(() => !HasSquareHoles.Value);
            });
        }

        public Rule<float> Hole10Offset { get; set; }
        protected virtual void ConfigureHole10Offset(Rule<float> rule)
        {
            rule.Define(() => GetLocationOrDefault(10));
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(() => !HasSquareHoles.Value);
            });
        }

        public Rule<float> Hole11Offset { get; set; }
        protected virtual void ConfigureHole11Offset(Rule<float> rule)
        {
            rule.Define(() => GetLocationOrDefault(11));
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(() => !HasSquareHoles.Value);
            });
        }

        public Rule<float> Hole12Offset { get; set; }
        protected virtual void ConfigureHole12Offset(Rule<float> rule)
        {
            rule.Define(() => GetLocationOrDefault(12));
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(() => !HasSquareHoles.Value);
            });
        }

        public Rule<float> SquareHole1Offset { get; set; }
        protected virtual void ConfigureSquareHole1Offset(Rule<float> rule)
        {
            rule.Define(() => GetLocationOrDefault(1));
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(HasSquareHoles);
            });
        }

        public Rule<float> SquareHole2Offset { get; set; }
        protected virtual void ConfigureSquareHole2Offset(Rule<float> rule)
        {
            rule.Define(() => GetLocationOrDefault(2));
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(HasSquareHoles);
            });
        }

        public Rule<float> SquareHole3Offset { get; set; }
        protected virtual void ConfigureSquareHole3Offset(Rule<float> rule)
        {
            rule.Define(() => GetLocationOrDefault(3));
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(HasSquareHoles);
            });
        }

        public Rule<float> SquareHole4Offset { get; set; }
        protected virtual void ConfigureSquareHole4Offset(Rule<float> rule)
        {
            rule.Define(() => GetLocationOrDefault(4));
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(HasSquareHoles);
            });
        }

        public Rule<float> SquareHole5Offset { get; set; }
        protected virtual void ConfigureSquareHole5Offset(Rule<float> rule)
        {
            rule.Define(() => GetLocationOrDefault(5));
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(HasSquareHoles);
            });
        }

        public Rule<float> SquareHole6Offset { get; set; }
        protected virtual void ConfigureSquareHole6Offset(Rule<float> rule)
        {
            rule.Define(() => GetLocationOrDefault(6));
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(HasSquareHoles);
            });
        }

        public Rule<float> SquareHole7Offset { get; set; }
        protected virtual void ConfigureSquareHole7Offset(Rule<float> rule)
        {
            rule.Define(() => GetLocationOrDefault(7));
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(HasSquareHoles);
            });
        }

        public Rule<float> SquareHole8Offset { get; set; }
        protected virtual void ConfigureSquareHole8Offset(Rule<float> rule)
        {
            rule.Define(() => GetLocationOrDefault(8));
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(HasSquareHoles);
            });
        }

        public Rule<float> SquareHole9Offset { get; set; }
        protected virtual void ConfigureSquareHole9Offset(Rule<float> rule)
        {
            rule.Define(() => GetLocationOrDefault(9));
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(HasSquareHoles);
            });
        }

        public Rule<float> SquareHole10Offset { get; set; }
        protected virtual void ConfigureSquareHole10Offset(Rule<float> rule)
        {
            rule.Define(() => GetLocationOrDefault(10));
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(HasSquareHoles);
            });
        }

        public Rule<float> SquareHole11Offset { get; set; }
        protected virtual void ConfigureSquareHole11Offset(Rule<float> rule)
        {
            rule.Define(() => GetLocationOrDefault(11));
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(HasSquareHoles);
            });
        }

        public Rule<float> SquareHole12Offset { get; set; }
        protected virtual void ConfigureSquareHole12Offset(Rule<float> rule)
        {
            rule.Define(() => GetLocationOrDefault(12));
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(HasSquareHoles);
            });
        }

        //[CadRule(CadRuleUsageType.Parameter)]
        public Rule<string> HoleLocationsString { get; set; }
        protected virtual void ConfigureHoleLocationsString(Rule<string> rule)
        {
            rule.Define(() =>
            {
                string retVal = "";

                if (HoleLocations.Value != null && HoleLocations.Value.Any())
                {
                    foreach (float loc in HoleLocations.Value)
                    {
                        if (string.IsNullOrWhiteSpace(retVal))
                            retVal = loc.ToString();
                        else
                            retVal = retVal + ", " + loc.ToString();
                    }
                }

                return retVal;
            });
        }

        public LookupRule<int> LoadsWide { get; set; }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> BasePartNumber { get; set; }
        protected virtual void ConfigureBasePartNumber(Rule<string> rule)
        {
            rule.Define(default(string));
        }

        protected virtual void ConfigurePartNumberPrefix(Rule<string> rule)
        {
            rule.Define(() => $"{BasePartNumber.Value}-");
        }

        #endregion

        #region CPQ.IBomPart Implementation

        protected override void ConfigureDescriptionPrefix(Rule<string> rule)
        {
            rule.Define(() => $"Beam {BasePartNumber.Value}");
        }

        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {
            child.PartNumber.Define(PartNumber);
            child.Description.Define(BOMUIDescription);
            child.MaterialPartNumber.Define(MaterialPartNumber);
            child.MaterialQty.Define(() => Length.Value/12);
            child.ItemMaterialCost.Define(() => child.ItemRawMaterialWeight.Value * child.MaterialUnitCost.Value);
        }

        #endregion

        #region BOM Rules

        public Rule<string> MaterialPartNumber { get; set; }
        protected virtual void ConfigureMaterialPartNumber(Rule<string> rule)
        {
            rule.Define(() => MaterialPartNumber.Value);
        }

        //public Rule<double> MaterialWeight { get; set; }
        //protected virtual void ConfigureMaterialWeight(Rule<double> rule)
        //{
        //    rule.Define(() => ChannelWeightPerFt.Value * (Length.Value / 12));
        //}

        public Rule<double> LBeamLaborFactor { get; set; }
        protected virtual void ConfigureLBeamLaborFactor(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> LBeamBase { get; set; }
        protected virtual void ConfigureLBeamBase(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> LBeamMatLabor { get; set; }
        protected virtual void ConfigureLBeamMatLabor(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<bool> IsHeavyChannel { get; set; }
        protected virtual void ConfigureIsHeavyChannel(Rule<bool> rule)
        {
            rule.Define(() =>
            {
                switch (ChannelSize.Value)
                {
                    case "C7x9.8":
                    case "C8x11.5":
                    case "C10x15.3":
                        return true;
                    default:
                        return false;
                }
            });
        }

        #endregion

        #region Public Methods

        public float GetLocationOrDefault(int holeIndex, float defaultVal = 0)
        {
            float retVal = defaultVal;

            if (HoleLocations.Value != null && HoleLocations.Value.Any())
            {
                if (holeIndex <= HoleLocations.Value.Count)
                    retVal = HoleLocations.Value[holeIndex - 1];
            }

            return retVal;
        }

        #endregion

        #region Graphics

        public ChildList<Hole> Holes { get; set; }
        protected virtual void ConfigureHoles(ChildList<Hole> list)
        {
            list.Qty.Define(() => (HoleLocations == null || HoleLocations.Value == null) ? 0 : HoleLocations.Value.Count());
            list.ConfigureChildren((child, index) =>
            {
                child.HoleType.Define(() => HasSquareHoles.Value ? "square" : "round");
                child.HoleSize.Define(HoleDiameter);
                child.Depth.Define(ChannelDepth);
                child.Position.Define(() =>
                {
                    return Helper.Vxyz((ChannelDepth.Value / 2) - 0.025f, (ChannelWidth.Value/2) - HoleVerticalOffset.Value, HoleLocations.Value[index]);
                });
                child.Rotation.Define(() =>
                {
                    return Helper.CreateRotation(Vector3.UnitZ, 90);
                });
            });
        }

        #endregion

        #region IChannel Overrides

        protected override void ConfigureChannelSizesAvailable(Rule<List<string>> rule)
        {
            rule.Define(() => (new List<string> { "C3x3.5", "C3x4.1", "C4x4.5", "C4x5.4", "C5x6.1", "C5x6.7", "C6x8.2", "C7x9.8", "C8x11.5", "C10x15.3" }));
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            //Orange
            material.Color.Define(16753920);
            material.Opacity.Define(0.9f);
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(false);
        }

        #endregion

        #region Protected Methods

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureChannelRules();
        }

        #endregion
    }
}
