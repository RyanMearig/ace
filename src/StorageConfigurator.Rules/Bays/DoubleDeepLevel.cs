﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using Ruler.Rules;
using Ruler.Rules.Graphics;
using Ruler.Rules.Validation;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class DoubleDeepLevel : Level
    {
        #region Constructors

        public DoubleDeepLevel(ModelContext modelContext, StorageConfiguratorRepository repo) : base(modelContext, repo) { }

        public DoubleDeepLevel(string name, Part parent, StorageConfiguratorRepository repo) : base(name, parent, repo) { }

        #endregion

        #region Rules

        public Rule<bool> PalletSupportFullLength { get; set; }
        protected virtual void ConfigurePalletSupportFullLength(Rule<bool> rule)
        {
            rule.Define(() => PalletSupportType.Value.Contains("xbw-dd"));
        }

        public Rule<float> BeamDropbownforContinuousSupport { get; set; }
        protected virtual void ConfigureBeamDropbownforContinuousSupport(Rule<float> rule)
        {
            rule.Define(0.4375f);
        }

        public Rule<ShelfLoadBeam> Notch1LoadBeam { get; set; }
        protected virtual void ConfigureNotch1LoadBeam(Rule<ShelfLoadBeam> rule)
        {
            rule.Define(() =>
            {
                ShelfLoadBeam retVal = null;

                if (PalletSupportFullLength.Value)
                    retVal = FrontFrameInternalLoadBeam;

                return retVal;
            });
        }

        public Rule<string> Notch1BeamOrientation { get; set; }
        protected virtual void ConfigureNotch1BeamOrientation(Rule<string> rule)
        {
            rule.Define(() =>
            {
                if (Notch1LoadBeam.Value != null)
                    return Notch1LoadBeam.Value?.Orientation.Value;
                else
                    return "std";
            });
        }

        public Rule<ShelfLoadBeam> Notch2LoadBeam { get; set; }
        protected virtual void ConfigureNotch2LoadBeam(Rule<ShelfLoadBeam> rule)
        {
            rule.Define(() =>
            {
                if (PalletSupportType.Value == "xbw-dd2")
                    return RearFrameInternalLoadBeam;
                else
                    return null;
            });
        }

        public Rule<string> Notch2BeamOrientation { get; set; }
        protected virtual void ConfigureNotch2BeamOrientation(Rule<string> rule)
        {
            rule.Define(() =>
            {
                if (Notch2LoadBeam.Value != null)
                    return Notch2LoadBeam.Value?.Orientation.Value;
                else
                    return "std";
            });
        }

        public Rule<bool> IncludeInternalPalletSupports { get; set; }
        protected virtual void ConfigureIncludeInternalPalletSupports(Rule<bool> rule)
        {
            rule.Define(() => (IsFirstLevel.Value || PreviousLevel.Value == null) ? false : ((DoubleDeepLevel)PreviousLevel.Value).IncludeInternalPalletSupports.Value);
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => ProductOnGround.Value || IsDoubleDeepPost.Value ||
                    PalletSupportFullLength.Value || PalletSupportType.Value == "none");
            });
        }

        #endregion

        #region Level Overrides


        #endregion

        #region Private Methods

        private Vector3 GetRearProductPosition(int index, float productDepth)
        {
            float totalOverlap = (RearFrameDepth.Value - productDepth);
            float xOffset = -FrontFrameDepth.Value - (RearFrameDepth.Value / 2) - (totalOverlap / 2);
            float zOffset = 0;

            xOffset = xOffset - RearProductOverhang.Value;

            if (IsDoubleDeepPost.Value)
            {
                //xOffset = xOffset + 0.5f;
            }
            else
                xOffset = xOffset - FrameTieLength.Value;

            if (index == 0)
                zOffset = LoadToFrameOffset.Value;
            else
            {
                Product prevChild = RearProducts[index - 1];
                zOffset = Math.Abs(prevChild.Position.Value.Z) + prevChild.PalletLoadWidth.Value + LoadToLoadOffset.Value;
            }

            return Helper.Vxz(xOffset, -zOffset);
        }

        #endregion

        #region Children

        public ChildList<Product> RearProducts { get; set; }
        protected virtual void ConfigureRearProducts(ChildList<Product> list)
        {
            list.Qty.Define(() => ActiveProduct.Value != null ? LoadsWide.Value : 0);
            list.ConfigureChildren((child, index) =>
            {
                child.Index.Define(() => ActiveProduct.Value.Index.Value);
                child.ProductName.Define(() => ActiveProduct.Value.ProductName.Value);
                child.LiftOff.Define(() => ActiveProduct.Value.LiftOff.Value);
                child.LoadWidth.Define(() => ActiveProduct.Value.LoadWidth.Value);
                child.LoadHeight.Define(() => ActiveProduct.Value.LoadHeight.Value);
                child.LoadDepth.Define(() => ActiveProduct.Value.LoadDepth.Value);
                child.PalletType.Define(() => ActiveProduct.Value.PalletType.Value);
                child.PalletWidth.Define(() => ActiveProduct.Value.PalletWidth.Value);
                child.PalletHeight.Define(() => ActiveProduct.Value.PalletHeight.Value);
                child.PalletDepth.Define(() => ActiveProduct.Value.PalletDepth.Value);
                child.Weight.Define(() => ActiveProduct.Value.Weight.Value);
                child.ShowGraphics.Define(ShowProducts);
                child.Position.Define(() => GetRearProductPosition(index, child.LoadDepth.Value));
            });
        }

        public ShelfLoadBeam FrontLoadBeam { get; set; }
        protected virtual void ConfigureFrontLoadBeam(ShelfLoadBeam child)
        {
            child.IsActive.Define(() => !ProductOnGround.Value);
            child.ChannelSize.Define(FrontBeamSize);
            child.Width.Define(BayWidth);
            child.Orientation.Define(FrontBeamOrientation);
            child.BracketType.Define(FrontBeamBracketType);
            child.WeldDown.Define(FrontBeamWeldDown);
            child.HoleLocations.Define(BeamHoleLocations);
            child.HoleType.Define(FrontBeamHoleType);
            child.BoltType.Define(FrontBeamBoltType);
            child.HardwareSize.Define(FrontBeamHardwareSize);
            child.LoadsWide.Define(LoadsWide);
            child.Color.Define(FrontBeamColor);
        }

        public ShelfLoadBeam FrontFrameInternalLoadBeam { get; set; }
        protected virtual void ConfigureFrontFrameInternalLoadBeam(ShelfLoadBeam child)
        {
            child.IsActive.Define(() => !ProductOnGround.Value);
            child.ChannelSize.Define(FrontInteriorBeamSize);
            child.Width.Define(BayWidth);
            child.Orientation.Define(FrontInteriorBeamOrientation);
            child.BracketType.Define(FrontInteriorBeamBracketType);
            child.Color.Define(FrontInteriorBeamColor);
            child.WeldDown.Define(FrontInteriorBeamWeldDown);
            child.HoleLocations.Define(BeamHoleLocations);
            child.HoleType.Define(FrontInteriorBeamHoleType);
            child.BoltType.Define(FrontInteriorBeamBoltType);
            child.HardwareSize.Define(FrontInteriorBeamHardwareSize);
            child.LoadsWide.Define(LoadsWide);
            child.Position.Define(() =>
            {
                float yOffset = 0;

                if (PalletSupportFullLength.Value)
                    yOffset = -BeamDropbownforContinuousSupport.Value;

                return Helper.Vxyz(-FrontFrameDepth.Value, yOffset, -child.Width.Value);
            });
            child.Rotation.Define(() => Helper.CreateRotation(Vector3.UnitY, 180));
        }

        public ShelfLoadBeam RearFrameInternalLoadBeam { get; set; }
        protected virtual void ConfigureRearFrameInternalLoadBeam(ShelfLoadBeam child)
        {
            child.IsActive.Define(() => !ProductOnGround.Value && !IsDoubleDeepPost.Value);
            child.ChannelSize.Define(RearInteriorBeamSize);
            child.Width.Define(BayWidth);
            child.Orientation.Define(RearInteriorBeamOrientation);
            child.BracketType.Define(RearInteriorBeamBracketType);
            child.Color.Define(RearInteriorBeamColor);
            child.WeldDown.Define(RearInteriorBeamWeldDown);
            child.HoleLocations.Define(BeamHoleLocations);
            child.HoleType.Define(RearInteriorBeamHoleType);
            child.BoltType.Define(RearInteriorBeamBoltType);
            child.HardwareSize.Define(RearInteriorBeamHardwareSize);
            child.LoadsWide.Define(LoadsWide);
            child.Position.Define(() =>
            {
                float yOffset = 0;

                if (PalletSupportFullLength.Value)
                    yOffset = -BeamDropbownforContinuousSupport.Value;

                return Helper.Vxy(-(FrontFrameDepth.Value + FrameTieLength.Value), yOffset);
            });
        }

        public ShelfLoadBeam RearLoadBeam { get; set; }
        protected virtual void ConfigureRearLoadBeam(ShelfLoadBeam child)
        {
            child.IsActive.Define(() => !ProductOnGround.Value);
            child.ChannelSize.Define(RearBeamSize);
            child.Width.Define(BayWidth);
            child.Orientation.Define(RearBeamOrientation);
            child.BracketType.Define(RearBeamBracketType);
            child.WeldDown.Define(RearBeamWeldDown);
            child.HoleLocations.Define(BeamHoleLocations);
            child.HoleType.Define(RearBeamHoleType);
            child.BoltType.Define(RearBeamBoltType);
            child.HardwareSize.Define(RearBeamHardwareSize);
            child.LoadsWide.Define(LoadsWide);
            child.Color.Define(RearBeamColor);
            child.Position.Define(() =>
            {
                return Helper.Vxz(-OverallDepth.Value, -child.Width.Value);
            });
            child.Rotation.Define(() => Helper.CreateRotation(Vector3.UnitY, 180));
        }

        public ChildList<ShelfPalletStop> PalletStops { get; set; }
        protected virtual void ConfigurePalletStops(ChildList<ShelfPalletStop> list)
        {
            list.Qty.Define(() => (ProductOnGround.Value || PalletStopStyle.Value == "NONE" ? 0 : PalletStopHoleLocations.Value.Count()));
            list.ConfigureChildren((child, index) =>
            {
                child.BeamSize.Define(RearBeamSize);
                child.Style.Define(PalletStopStyle);
                child.Width.Define(PalletStopWidth);
                child.Color.Define(PalletStopColor);
                child.Offset.Define(PalletStopOffset);
                child.Position.Define(() => Helper.Vxyz(-OverallDepth.Value, 0, -PalletStopHoleLocations.Value[index]));
            });
        }

        public ChildList<ShelfBeamCrossbar> FrontFrameCrossbars { get; set; }
        protected virtual void ConfigureFrontFrameCrossbars(ChildList<ShelfBeamCrossbar> list)
        {
            list.Qty.Define(() => (ProductOnGround.Value || PalletSupportType.Value == "none" ? 0 : PalletSupportLocations.Value.Count()));
            list.ConfigureChildren((child, index) =>
            {
                child.ChannelSize.Define(FrontBeamSize);
                child.FrontBeamOrientation.Define(FrontBeamOrientation);
                child.FrontBeamBoltType.Define(FrontBeamBoltType);
                child.FrontBeamHardwareSize.Define(FrontBeamHardwareSize);
                child.FrontBeamHoleType.Define(FrontBeamHoleType);
                child.RearBeamOrientation.Define(() => PalletSupportFullLength.Value ? RearBeamOrientation.Value : FrontInteriorBeamOrientation.Value);
                child.RearBeamBoltType.Define(() => PalletSupportFullLength.Value ? RearBeamBoltType.Value : FrontInteriorBeamBoltType.Value);
                child.RearBeamHardwareSize.Define(() => PalletSupportFullLength.Value ? RearBeamHardwareSize.Value : FrontInteriorBeamHardwareSize.Value);
                child.RearBeamHoleType.Define(() => PalletSupportFullLength.Value ? RearBeamHoleType.Value : FrontInteriorBeamHoleType.Value);
                child.FrontInteriorBeamBoltType.Define(FrontInteriorBeamBoltType);
                child.FrontInteriorBeamHardwareSize.Define(FrontInteriorBeamHardwareSize);
                child.FrontInteriorBeamHoleType.Define(FrontInteriorBeamHoleType);
                child.RearInteriorBeamBoltType.Define(RearInteriorBeamBoltType);
                child.RearInteriorBeamHardwareSize.Define(RearInteriorBeamHardwareSize);
                child.RearInteriorBeamHoleType.Define(RearInteriorBeamHoleType);
                child.Type.Define(PalletSupportType);
                child.RackDepth.Define(() => PalletSupportFullLength.Value ? OverallDepth.Value : FrontFrameDepth.Value);
                child.FrontFrameDepth.Define(FrontFrameDepth);
                child.Notch1BeamOrientation.Define(Notch1BeamOrientation);
                child.FrameTieLength.Define(FrameTieLength);
                child.RearFrameDepth.Define(RearFrameDepth);
                child.Notch2BeamOrientation.Define(Notch2BeamOrientation);
                child.BeamSize.Define(this.FrontBeamSize);
                child.AngleType.Define(AngleType);
                child.Color.Define(PalletSupportColor);
                child.Position.Define(() =>
                {
                    float xOffset = (child.RackDepthDiffToCutLength.Value/2);

                    return Helper.Vxyz(-(child.RackDepthDiffToCutLength.Value / 2), -1.5f, -PalletSupportLocations.Value[index]);
                });
            });
        }

        public ChildList<ShelfBeamCrossbar> RearFrameCrossbars { get; set; }
        protected virtual void ConfigureRearFrameCrossbars(ChildList<ShelfBeamCrossbar> list)
        {
            list.Qty.Define(() => (ProductOnGround.Value || PalletSupportFullLength.Value || PalletSupportType.Value == "none" ? 0 : PalletSupportLocations.Value.Count()));
            list.ConfigureChildren((child, index) =>
            {
                child.ChannelSize.Define(RearBeamSize);
                child.FrontBeamOrientation.Define(RearInteriorBeamOrientation);
                child.RearBeamOrientation.Define(RearBeamOrientation);
                child.FrontBeamBoltType.Define(() => IsDoubleDeepPost.Value ? FrontInteriorBeamBoltType.Value : RearInteriorBeamBoltType.Value);
                child.FrontBeamHardwareSize.Define(() => IsDoubleDeepPost.Value ? FrontInteriorBeamHardwareSize.Value : RearInteriorBeamHardwareSize.Value);
                child.FrontBeamHoleType.Define(() => IsDoubleDeepPost.Value ? FrontInteriorBeamHoleType.Value : RearInteriorBeamHoleType.Value);
                child.RearBeamBoltType.Define(RearBeamBoltType);
                child.RearBeamHardwareSize.Define(RearBeamHardwareSize);
                child.RearBeamHoleType.Define(RearBeamHoleType);
                child.Type.Define(PalletSupportType);
                child.RackDepth.Define(RearFrameDepth);
                child.BeamSize.Define(this.FrontBeamSize);
                child.AngleType.Define(AngleType);
                child.Color.Define(PalletSupportColor);
                child.Position.Define(() =>
                {
                    float xOffset = FrontFrameDepth.Value;

                    if (IsDoubleDeepPost.Value)
                    {
                        if (FrontInteriorBeamOrientation.Value != "std")
                            xOffset = xOffset - (FrontInteriorBeamDepth.Value - FrontInteriorBeamWebThickness.Value);
                    }
                    else
                    {
                        xOffset = xOffset + FrameTieLength.Value;

                        if (RearInteriorBeamOrientation.Value == "std")
                            xOffset = xOffset + RearInteriorBeamWebThickness.Value;
                        else
                            xOffset = xOffset + RearInteriorBeamDepth.Value;
                    }

                    return Helper.Vxyz(-xOffset, -1.5f, -PalletSupportLocations.Value[index]);
                });
            });
        }

        public ChildList<ShelfBeamCrossbar> InternalCrossbars { get; set; }
        protected virtual void ConfigureInternalCrossbars(ChildList<ShelfBeamCrossbar> list)
        {
            list.Qty.Define(() =>
            {
                if (!IncludeInternalPalletSupports.Value || ProductOnGround.Value || IsDoubleDeepPost.Value ||
                    PalletSupportFullLength.Value || PalletSupportType.Value == "none") 
                {
                    return 0;
                }
                return PalletSupportLocations.Value.Count();
            });
            list.ConfigureChildren((child, index) =>
            {
                child.ChannelSize.Define(FrontInteriorBeamSize);
                child.FrontBeamOrientation.Define(FrontInteriorBeamOrientation);
                child.RearBeamOrientation.Define(RearInteriorBeamOrientation);
                child.FrontBeamBoltType.Define(FrontInteriorBeamBoltType);
                child.FrontBeamHardwareSize.Define(FrontInteriorBeamHardwareSize);
                child.FrontBeamHoleType.Define(FrontInteriorBeamHoleType);
                child.RearBeamBoltType.Define(RearInteriorBeamBoltType);
                child.RearBeamHardwareSize.Define(RearInteriorBeamHardwareSize);
                child.RearBeamHoleType.Define(RearInteriorBeamHoleType);
                child.Type.Define(PalletSupportType);
                child.RackDepth.Define(FrameTieLength);
                child.BeamSize.Define(this.FrontBeamSize);
                child.AngleType.Define(AngleType);
                child.Color.Define(PalletSupportColor);
                child.Position.Define(() =>
                {
                    float xOffset = FrontFrameDepth.Value;

                    if (FrontInteriorBeamOrientation.Value != "std")
                        xOffset = xOffset - (FrontInteriorBeamDepth.Value - FrontInteriorBeamWebThickness.Value);

                    return Helper.Vxyz(-xOffset, -1.5f, -PalletSupportLocations.Value[index]);
                });
            });
        }

        #endregion

        #region Graphics


        #endregion

    }
}
