﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Ruler.Rules;

namespace StorageConfigurator.Rules
{
    public interface ILoadBeamData : IChannelData
    {
        Rule<List<string>> BeamSizesAvailable { get; set; }
        Rule<List<ChoiceListItem>> LoadBeamSizeChoiceList { get; set; }
        Rule<List<ChoiceListItem>> OrientationChoiceList { get; set; }
        Rule<List<ChoiceListItem>> BracketTypeChoiceList { get; set; }
    }

    public static class ILoadBeamDataExtensions
    {
        public static void ConfigureLoadBeamDataRules(this ILoadBeamData comp)
        {
            comp.ConfigureChannelDataRules();

            comp.BeamSizesAvailable.Define(() => (new List<string> { "C3x3.5", "C3x4.1", "C4x4.5", "C4x5.4", "C5x6.1", "C5x6.7", "C6x8.2", "C7x9.8", "C8x11.5", "C10x15.3" }));
          
            comp.LoadBeamSizeChoiceList.Define(() =>
            {
                return Helper.GetFilteredChoices(comp.AllChannelSizesChoiceList.Value, comp.BeamSizesAvailable.Value);
            });

            comp.OrientationChoiceList.Define(() =>
            {
                return new List<ChoiceListItem>()
                {
                    new ChoiceListItem("std", "Standard"),
                    new ChoiceListItem("rev", "Reversed")
                };
            });

            comp.BracketTypeChoiceList.Define(() =>
            {
                return new List<ChoiceListItem>()
                {
                    new ChoiceListItem("8", @"8"""),
                    new ChoiceListItem("8infAdj", @"8"" inf adj"),
                    new ChoiceListItem("9", @"9"""),
                    new ChoiceListItem("12", @"12"""),
                    new ChoiceListItem("13", @"13""")
                };
            });

        }

    }
}
