﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Ruler.Rules;
using Ruler.Rules.Validation;
using System.Numerics;
using Ruler.Rules.Graphics;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class Level : Part, IShelfLoadBeamInputs, IDepthMaster, IAngle, IColorComponent, IRevisablePart
    {
        #region Constructors

        public Level(ModelContext modelContext, StorageConfiguratorRepository repo) : base(modelContext) 
        {
            Repository = repo;
        }

        public Level(string name, Part parent, StorageConfiguratorRepository repo) : base(name, parent)
        {
            Repository = repo;
        }

        #endregion

        #region Rules

        #region Level Rules

        [UIRule]
        public Rule<int> LevelIndex { get; set; }
        protected virtual void ConfigureLevelIndex(Rule<int> rule)
        {
            rule.Define(default(int));
        }

        public Rule<bool> IsFirstLevel { get; set; }
        protected virtual void ConfigureIsFirstLevel(Rule<bool> rule)
        {
            rule.Define(() => LevelIndex.Value == 1);
        }

        public Rule<bool> IsLastLevel { get; set; }
        protected virtual void ConfigureIsLastLevel(Rule<bool> rule)
        {
            rule.Define(() => NextLevel.Value == null || NextLevel.Value == default(Level));
        }

        public Rule<Level> PreviousLevel { get; set; }
        protected virtual void ConfigurePreviousLevel(Rule<Level> rule)
        {
            rule.Define(default(Level));
        }

        public Rule<Level> NextLevel { get; set; }
        protected virtual void ConfigureNextLevel(Rule<Level> rule)
        {
            rule.Define(default(Level));
        }

        public Rule<Bay> ParentComp { get; set; }
        protected virtual void ConfigureParentComp(Rule<Bay> rule)
        {
            rule.Define(() => Parent != null ? (Bay)Parent : null);
        }

        public Rule<bool> ProductOnGround { get; set; }
        protected virtual void ConfigureProductOnGround(Rule<bool> rule)
        {
            rule.Define(() => IsFirstLevel.Value && Elevation.Value == 0);
        }

        [UIRule]
        public Rule<float> PalletHeight { get; set; }
        protected virtual void ConfigurePalletHeight(Rule<float> rule)
        {
            rule.Define(() =>
            {
                //if (Elevation.IsOverridden)
                //    return (Elevation.Value - (LiftOff.Value + MaxBeamHeight.Value));
                if (ActiveProduct.Value != null)
                    return ActiveProduct.Value.LoadHeight.Value;
                else
                    return 10;
            });
            rule.ValueSet += (sender, e) =>
            {
                Elevation.Reset();
            };
        }


        public Rule<float> LiftOff { get; set; }
        protected virtual void ConfigureLiftOff(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (IsLastLevel.Value || NextLevel.Value == null)
                    return ActiveProduct.Value != null ? ActiveProduct.Value.LiftOff.Value : 6;
                else
                {
                    return NextLevel.Value.Elevation.Value - (Elevation.Value + PalletHeight.Value + MaxBeamHeight.Value);
                }
            });
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(true);
            });
        }

        [UIRule]
        public Rule<float> Elevation { get; set; }
        protected virtual void ConfigureElevation(Rule<float> rule)
        {
            rule.Define(BeginningElevation);
            rule.Min(ElevationMinimum);
            rule.ValueSet += (sender, e) =>
            {
                PalletHeight.Reset();
            };
        }

        public Rule<float> BeginningElevation { get; set; }
        protected virtual void ConfigureBeginningElevation(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (IsFirstLevel.Value || PreviousLevel.Value == null)
                    return 0;
                else
                {
                    float prevLevelElevation = PreviousLevel.Value.Elevation.Value;
                    float prevLevelPalletHeight = PreviousLevel.Value.PalletHeight.Value;
                    float prevLevelLiftOff = ActiveProduct.Value != null ? ActiveProduct.Value.LiftOff.Value : 6;

                    return (prevLevelElevation + prevLevelPalletHeight + prevLevelLiftOff + MaxBeamHeight.Value);
                }
            });
        }

        public Rule<float> ElevationMinimum { get; set; }
        protected virtual void ConfigureElevationMinimum(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (IsFirstLevel.Value || PreviousLevel.Value == null)
                    return 0;
                else
                {
                    float prevLevelElevation = PreviousLevel.Value.Elevation.Value;
                    float prevLevelPalletHeight = PreviousLevel.Value.PalletHeight.Value;

                    return (prevLevelElevation + prevLevelPalletHeight + MaxBeamHeight.Value);
                }
            });
        }


        public ReadOnlyRule<float> PalletLoadWidth { get; set; }
        protected virtual void ConfigurePalletLoadWidth(ReadOnlyRule<float> rule)
        {
            rule.Define(() =>
            {
                if (PalletWidth.Value > LoadWidth.Value)
                    return PalletWidth.Value;
                else
                    return LoadWidth.Value;
            });
        }

        //[UIRule]
        //public Rule<float> BayDepth { get; set; }
        //protected virtual void ConfigureBayDepth(Rule<float> rule)
        //{
        //    rule.Define(() => ActiveProduct.Value != null ? ActiveProduct.Value.PalletLoadDepth.Value : 48);
        //}

        public Rule<float> BayWidth { get; set; }
        protected virtual void ConfigureBayWidth(Rule<float> rule)
        {
            rule.Define(200);
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(true);
            });
            rule.AddToCAD(cad =>
            {
                cad.CadRuleUsageType = CadRuleUsageType.Parameter;
                cad.NameInCad = "Depth";
            });
            //rule.ValueSet += (sender, e) =>
            //{
            //    LoadToLoadOffset.Reset();
            //};
        }

        public Rule<float> BayWidthMinimum { get; set; }
        protected virtual void ConfigureBayWidthMinimum(Rule<float> rule)
        {
            rule.Define(() =>
            {
                return ((PalletLoadWidth.Value * LoadsWide.Value) + (LoadToLoadOffsetMin.Value * (LoadsWide.Value - 1)) + (2 * LoadToFrameOffsetMin.Value));
            });
        }

        [UIRule]
        public Rule<bool> ShowProducts { get; set; }
        protected virtual void ConfigureShowProducts(Rule<bool> rule)
        {
            rule.Define(true);
        }

        public Rule<float> DefaultLocationProductOverlap { get; set; }
        protected virtual void ConfigureDefaultLocationProductOverlap(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (IsDoubleDeep.Value && (ActiveProduct.Value != null))
                {
                    var totalProductDepth = ActiveProduct.Value.LoadDepth.Value * 2;
                    var totalOverlap = totalProductDepth - OverallDepth.Value;

                    return (totalOverlap <= 0 ? 0 : totalOverlap);
                }
                else
                {
                    return 0;
                }
            });
        }

        public Rule<float> FrontProductOverhang { get; set; }
        protected virtual void ConfigureFrontProductOverhang(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (PreviousLevel.Value != null && ActiveProduct.Value == PreviousLevel.Value.ActiveProduct.Value)
                {
                    return PreviousLevel.Value.FrontProductOverhang.Value;
                }
                else if (!IsDoubleDeep.Value)
                {
                    return 0;
                }
                else
                {
                    if (DefaultLocationProductOverlap.Value > 0)
                    {
                        var frontOverlap = 0f;

                        if (IsDoubleDeepPost.Value)
                        {
                            frontOverlap = ActiveProduct.Value.LoadDepth.Value - (FrontFrameDepth.Value - 1);
                        }
                        else
                        {
                            frontOverlap = ActiveProduct.Value.LoadDepth.Value - (FrontFrameDepth.Value + (FrameTieLength.Value / 2));
                        }

                        if (frontOverlap > 0)
                        {
                            return frontOverlap;
                        }
                        else
                        {
                            return 0;
                        }
                    }
                    else
                    {
                        return 0;
                    }
                }
            });
            rule.AddToUI(ui =>
            {
                ui.Label.Define(() => IsDoubleDeep.Value ? "Front Product Overhang" : "Product Overhang");
            });
        }

        [UIRule]
        public Rule<float> RearProductOverhang { get; set; }
        protected virtual void ConfigureRearProductOverhang(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (PreviousLevel.Value != null && ActiveProduct.Value == PreviousLevel.Value.ActiveProduct.Value)
                {
                    return PreviousLevel.Value.RearProductOverhang.Value;
                }
                else if (!IsDoubleDeep.Value)
                {
                    return 0;
                }
                else
                {
                    if (DefaultLocationProductOverlap.Value > 0)
                    {
                        var rearFrameOverlap = 0f;

                        if (IsDoubleDeepPost.Value)
                        {
                            rearFrameOverlap = ActiveProduct.Value.LoadDepth.Value - (RearFrameDepth.Value + 1);
                        }
                        else
                        {
                            rearFrameOverlap = ActiveProduct.Value.LoadDepth.Value - (RearFrameDepth.Value + (FrameTieLength.Value / 2));
                        }

                        if (rearFrameOverlap > 0)
                        {
                            return rearFrameOverlap;
                        }
                        else
                        {
                            return 0;
                        }
                    }
                    else
                    {
                        return 0;
                    }
                }
            });
        }

        [CadRule(CadRuleUsageType.Property, "IsDoubleDeep")]
        public Rule<string> IsDoubleDeepProp { get; set; }
        protected virtual void ConfigureIsDoubleDeepProp(Rule<string> rule)
        {
            rule.Define(() => IsDoubleDeep.Value.ToString());
        }

        [CadRule(CadRuleUsageType.Property, "IsDoubleDeepPost")]
        public Rule<string> IsDoubleDeepPostProp { get; set; }
        protected virtual void ConfigureIsDoubleDeepPostProp(Rule<string> rule)
        {
            rule.Define(() => IsDoubleDeepPost.Value.ToString());
        }

        #endregion

        #region Front Beam Props

        public Rule<string> FrontBeamSize { get; set; }
        protected virtual void ConfigureFrontBeamSize(Rule<string> rule)
        {
            rule.Define(() => (IsFirstLevel.Value || PreviousLevel.Value == null) ? "C4x4.5" : PreviousLevel.Value.FrontBeamSize.Value);
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() => LoadBeamSizeChoiceList.Value);
            });
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> FrontBeamOrientation { get; set; }
        protected virtual void ConfigureFrontBeamOrientation(Rule<string> rule)
        {
            rule.Define(() => (IsFirstLevel.Value || PreviousLevel.Value == null) ? "std" : PreviousLevel.Value.FrontBeamOrientation.Value);
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() => OrientationChoiceList.Value);
            });
        }

        public Rule<float> FrontBeamWeldDown { get; set; }
        protected virtual void ConfigureFrontBeamWeldDown(Rule<float> rule)
        {
            rule.Define(() => (IsFirstLevel.Value || PreviousLevel.Value == null) ? 0.4375f : PreviousLevel.Value.FrontBeamWeldDown.Value);
            rule.Min(FrontBeamWeldDownMin);
            //rule.Max(ChannelWidth);
            rule.AddToUI();
        }

        public Rule<float> FrontBeamWeldDownMin { get; set; }
        protected virtual void ConfigureFrontBeamWeldDownMin(Rule<float> rule)
        {
            rule.Define(() => FrontBeamOrientation.Value == "std" ? 0.125f : 0);
        }

        public Rule<string> FrontBeamBracketType { get; set; }
        protected virtual void ConfigureFrontBeamBracketType(Rule<string> rule)
        {
            rule.Define(() =>
            {
                if ((IsFirstLevel.Value || PreviousLevel.Value == null))
                {
                    float topBracketToBtmBeamDist = FrontBeamWeldDown.Value + FrontBeamHeight.Value;

                    return GetBracketTypeDefault(topBracketToBtmBeamDist);
                }
                else
                    return PreviousLevel.Value.FrontBeamBracketType.Value;
            });
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() => BracketTypeChoiceList.Value);
            });
        }

        public Rule<string> FrontBeamColor { get; set; }
        protected virtual void ConfigureFrontBeamColor(Rule<string> rule)
        {
            rule.Define(() => (IsFirstLevel.Value || PreviousLevel.Value == null) ? Color.Value : PreviousLevel.Value.FrontBeamColor.Value);
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() => Color.UIMetadata.ChoiceList.Value);
            });
        }

        public Rule<string> FrontBeamHoleType { get; set; }
        protected virtual void ConfigureFrontBeamHoleType(Rule<string> rule)
        {
            rule.Define(() => (IsFirstLevel.Value || PreviousLevel.Value == null) ? "round" : PreviousLevel.Value.FrontBeamHoleType.Value);
            rule.AddToUI(ui =>
            {
                //ui.IsActive.Define(IsStandalone);
                ui.ChoiceList.Define(() => HoleTypeChoices.Value);
            });
            rule.ValueSet += (sender, e) =>
            {
                if (!FrontBeamBoltType.UIMetadata.ChoiceList.Value.Any(c => c.Value.ToString() == FrontBeamBoltType.Value))
                {
                    FrontBeamBoltType.Reset();
                }

                if (rule.Value == "square" && FrontBeamHardwareSize.Value == "0.375")
                {
                    FrontBeamHardwareSize.Reset();
                }
            };
        }

        public Rule<string> FrontBeamBoltType { get; set; }
        protected virtual void ConfigureFrontBeamBoltType(Rule<string> rule)
        {
            rule.Define(() => (IsFirstLevel.Value || PreviousLevel.Value == null) ? "Hex" : PreviousLevel.Value.FrontBeamBoltType.Value);
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() =>
                {
                    return GetAvailableBoltTypes(FrontBeamHoleType.Value);
                });
            });
        }

        public Rule<string> FrontBeamHardwareSize { get; set; }
        protected virtual void ConfigureFrontBeamHardwareSize(Rule<string> rule)
        {
            rule.Define(() => (IsFirstLevel.Value || PreviousLevel.Value == null) ? "0.5" : PreviousLevel.Value.FrontBeamHardwareSize.Value);
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() =>
                {
                    if (FrontBeamHoleType.Value == "round")
                    {
                        return new List<ChoiceListItem>()
                        {
                            new ChoiceListItem("0.375", @"3/8"""),
                            new ChoiceListItem("0.5", @"1/2""")
                        };
                    }
                    return new List<ChoiceListItem>() { new ChoiceListItem("0.5", @"1/2""") };
                });
            });
        }

        #endregion

        #region Interior Beams

        public Rule<string> FrontInteriorBeamSize { get; set; }
        protected virtual void ConfigureFrontInteriorBeamSize(Rule<string> rule)
        {
            rule.Define(() =>
            {
                if (IsFirstLevel.Value || PreviousLevel.Value == null)
                {
                    if (IsDoubleDeepPost.Value)
                        return "C5x6.7";
                    else
                        return "C4x4.5";
                }
                else
                    return PreviousLevel.Value.FrontInteriorBeamSize.Value;
            });
            rule.AddToUI(ui =>
            {
                ui.Label.Define(() => IsDoubleDeepPost.Value ? "Interior Beam Size" : "Front Interior Beam Size");
                ui.ChoiceList.Define(() => LoadBeamSizeChoiceList.Value);
            });
        }

        public Rule<string> FrontInteriorBeamOrientation { get; set; }
        protected virtual void ConfigureFrontInteriorBeamOrientation(Rule<string> rule)
        {
            rule.Define(() => (IsFirstLevel.Value || PreviousLevel.Value == null) ? "std" : PreviousLevel.Value.FrontInteriorBeamOrientation.Value);
            rule.AddToUI(ui =>
            {
                ui.Label.Define(() => IsDoubleDeepPost.Value ? "Interior Beam Orientation" : "Front Interior Beam Orientation");
                ui.ChoiceList.Define(() => OrientationChoiceList.Value);
            });
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(IsDoubleDeep);
                cad.CadRuleUsageType = CadRuleUsageType.Property;
            });
        }

        public Rule<float> FrontInteriorBeamWeldDown { get; set; }
        protected virtual void ConfigureFrontInteriorBeamWeldDown(Rule<float> rule)
        {
            rule.Define(() => (IsFirstLevel.Value || PreviousLevel.Value == null) ? 0.875f : PreviousLevel.Value.FrontInteriorBeamWeldDown.Value);
            rule.Min(FrontInteriorBeamWeldDownMin);
            //rule.Max(ChannelWidth);
            rule.AddToUI(ui =>
            {
                ui.Label.Define(() => IsDoubleDeepPost.Value ? "Interior Beam Weld Down" : "Front Interior Beam Weld Down");
            });
        }

        public Rule<float> FrontInteriorBeamWeldDownMin { get; set; }
        protected virtual void ConfigureFrontInteriorBeamWeldDownMin(Rule<float> rule)
        {
            rule.Define(() => FrontInteriorBeamOrientation.Value == "std" ? 0.125f : 0);
        }

        public Rule<string> FrontInteriorBeamBracketType { get; set; }
        protected virtual void ConfigureFrontInteriorBeamBracketType(Rule<string> rule)
        {
            rule.Define(() => (IsFirstLevel.Value || PreviousLevel.Value == null) ? FrontBeamBracketType.Value : PreviousLevel.Value.FrontInteriorBeamBracketType.Value);
            rule.AddToUI(ui =>
            {
                ui.Label.Define(() => IsDoubleDeepPost.Value ? "Interior Beam Bracket Type" : "Front Interior Beam Bracket Type");
                ui.ChoiceList.Define(() => BracketTypeChoiceList.Value);
            });
        }

        public Rule<float> FrontInteriorBeamHeight { get; set; }
        protected virtual void ConfigureFrontInteriorBeamHeight(Rule<float> rule)
        {
            rule.Define(() => GetChannelHeight(FrontInteriorBeamSize.Value));
        }

        public Rule<float> FrontInteriorBeamWebThickness { get; set; }
        protected virtual void ConfigureFrontInteriorBeamWebThickness(Rule<float> rule)
        {
            rule.Define(() => GetChannelData(FrontInteriorBeamSize.Value).WebThickness);
        }

        public Rule<float> FrontInteriorBeamDepth { get; set; }
        protected virtual void ConfigureFrontInteriorBeamDepth(Rule<float> rule)
        {
            rule.Define(() => GetChannelData(FrontInteriorBeamSize.Value).Depth);
        }

        public Rule<string> FrontInteriorBeamColor { get; set; }
        protected virtual void ConfigureFrontInteriorBeamColor(Rule<string> rule)
        {
            rule.Define(() => (IsFirstLevel.Value || PreviousLevel.Value == null) ? Color.Value : PreviousLevel.Value.FrontInteriorBeamColor.Value);
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() => Color.UIMetadata.ChoiceList.Value);
            });
        }

        public Rule<string> FrontInteriorBeamHoleType { get; set; }
        protected virtual void ConfigureFrontInteriorBeamHoleType(Rule<string> rule)
        {
            rule.Define(() => (IsFirstLevel.Value || PreviousLevel.Value == null) ? "round" : PreviousLevel.Value.FrontInteriorBeamHoleType.Value);
            rule.AddToUI(ui =>
            {
                //ui.IsActive.Define(IsStandalone);
                ui.ChoiceList.Define(() => HoleTypeChoices.Value);
            });
            rule.ValueSet += (sender, e) =>
            {
                if (!FrontInteriorBeamBoltType.UIMetadata.ChoiceList.Value.Any(c => c.Value.ToString() == FrontInteriorBeamBoltType.Value))
                {
                    FrontInteriorBeamBoltType.Reset();
                }

                if (rule.Value == "square" && FrontInteriorBeamHardwareSize.Value == "0.375")
                {
                    FrontInteriorBeamHardwareSize.Reset();
                }
            };
        }

        public Rule<string> FrontInteriorBeamBoltType { get; set; }
        protected virtual void ConfigureFrontInteriorBeamBoltType(Rule<string> rule)
        {
            rule.Define(() => (IsFirstLevel.Value || PreviousLevel.Value == null) ? "Hex" : PreviousLevel.Value.FrontInteriorBeamBoltType.Value);
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() =>
                {
                    return GetAvailableBoltTypes(FrontInteriorBeamHoleType.Value);
                });
            });
        }

        public Rule<string> FrontInteriorBeamHardwareSize { get; set; }
        protected virtual void ConfigureFrontInteriorBeamHardwareSize(Rule<string> rule)
        {
            rule.Define(() => (IsFirstLevel.Value || PreviousLevel.Value == null) ? "0.5" : PreviousLevel.Value.FrontInteriorBeamHardwareSize.Value);
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() =>
                {
                    if (FrontInteriorBeamHoleType.Value == "round")
                    {
                        return new List<ChoiceListItem>()
                        {
                            new ChoiceListItem("0.375", @"3/8"""),
                            new ChoiceListItem("0.5", @"1/2""")
                        };
                    }
                    return new List<ChoiceListItem>() { new ChoiceListItem("0.5", @"1/2""") };
                });
            });
        }


        public Rule<string> RearInteriorBeamSize { get; set; }
        protected virtual void ConfigureRearInteriorBeamSize(Rule<string> rule)
        {
            rule.Define(() => (IsFirstLevel.Value || PreviousLevel.Value == null) ? FrontInteriorBeamSize.Value : PreviousLevel.Value.RearInteriorBeamSize.Value);
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() => LoadBeamSizeChoiceList.Value);
            });
        }

        public Rule<string> RearInteriorBeamOrientation { get; set; }
        protected virtual void ConfigureRearInteriorBeamOrientation(Rule<string> rule)
        {
            rule.Define(() => (IsFirstLevel.Value || PreviousLevel.Value == null) ? "std" : PreviousLevel.Value.RearInteriorBeamOrientation.Value);
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() => OrientationChoiceList.Value);
            });
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(IsDoubleDeepPost);
                cad.CadRuleUsageType = CadRuleUsageType.Property;
            });
        }

        public Rule<float> RearInteriorBeamWeldDown { get; set; }
        protected virtual void ConfigureRearInteriorBeamWeldDown(Rule<float> rule)
        {
            rule.Define(() => (IsFirstLevel.Value || PreviousLevel.Value == null) ? FrontInteriorBeamWeldDown.Value : PreviousLevel.Value.RearInteriorBeamWeldDown.Value);
            rule.Min(RearInteriorBeamWeldDownMin);
            //rule.Max(ChannelWidth);
            rule.AddToUI();
        }

        public Rule<float> RearInteriorBeamWeldDownMin { get; set; }
        protected virtual void ConfigureRearInteriorBeamWeldDownMin(Rule<float> rule)
        {
            rule.Define(() => RearInteriorBeamOrientation.Value == "std" ? 0.125f : 0);
        }

        public Rule<string> RearInteriorBeamBracketType { get; set; }
        protected virtual void ConfigureRearInteriorBeamBracketType(Rule<string> rule)
        {
            rule.Define(() => (IsFirstLevel.Value || PreviousLevel.Value == null) ? FrontInteriorBeamBracketType.Value : PreviousLevel.Value.RearInteriorBeamBracketType.Value);
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() => BracketTypeChoiceList.Value);
            });
        }

        public Rule<float> RearInteriorBeamHeight { get; set; }
        protected virtual void ConfigureRearInteriorBeamHeight(Rule<float> rule)
        {
            rule.Define(() => GetChannelHeight(RearInteriorBeamSize.Value));
        }

        public Rule<float> RearInteriorBeamWebThickness { get; set; }
        protected virtual void ConfigureRearInteriorBeamWebThickness(Rule<float> rule)
        {
            rule.Define(() => GetChannelData(RearInteriorBeamSize.Value).WebThickness);
        }

        public Rule<float> RearInteriorBeamDepth { get; set; }
        protected virtual void ConfigureRearInteriorBeamDepth(Rule<float> rule)
        {
            rule.Define(() => GetChannelData(RearInteriorBeamSize.Value).Depth);
        }

        public Rule<string> RearInteriorBeamColor { get; set; }
        protected virtual void ConfigureRearInteriorBeamColor(Rule<string> rule)
        {
            rule.Define(() => (IsFirstLevel.Value || PreviousLevel.Value == null) ? Color.Value : PreviousLevel.Value.RearInteriorBeamColor.Value);
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() => Color.UIMetadata.ChoiceList.Value);
            });
        }

        public Rule<string> RearInteriorBeamHoleType { get; set; }
        protected virtual void ConfigureRearInteriorBeamHoleType(Rule<string> rule)
        {
            rule.Define(() => (IsFirstLevel.Value || PreviousLevel.Value == null) ? "round" : PreviousLevel.Value.RearInteriorBeamHoleType.Value);
            rule.AddToUI(ui =>
            {
                //ui.IsActive.Define(IsStandalone);
                ui.ChoiceList.Define(() => HoleTypeChoices.Value);
            });
            rule.ValueSet += (sender, e) =>
            {
                if (!RearInteriorBeamBoltType.UIMetadata.ChoiceList.Value.Any(c => c.Value.ToString() == RearInteriorBeamBoltType.Value))
                {
                    RearInteriorBeamBoltType.Reset();
                }

                if (rule.Value == "square" && RearInteriorBeamHardwareSize.Value == "0.375")
                {
                    RearInteriorBeamHardwareSize.Reset();
                }
            };
        }

        public Rule<string> RearInteriorBeamBoltType { get; set; }
        protected virtual void ConfigureRearInteriorBeamBoltType(Rule<string> rule)
        {
            rule.Define(() => (IsFirstLevel.Value || PreviousLevel.Value == null) ? "Hex" : PreviousLevel.Value.RearInteriorBeamBoltType.Value);
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() =>
                {
                    return GetAvailableBoltTypes(RearInteriorBeamHoleType.Value);
                });
            });
        }

        public Rule<string> RearInteriorBeamHardwareSize { get; set; }
        protected virtual void ConfigureRearInteriorBeamHardwareSize(Rule<string> rule)
        {
            rule.Define(() => (IsFirstLevel.Value || PreviousLevel.Value == null) ? "0.5" : PreviousLevel.Value.RearInteriorBeamHardwareSize.Value);
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() =>
                {
                    if (RearInteriorBeamHoleType.Value == "round")
                    {
                        return new List<ChoiceListItem>()
                        {
                            new ChoiceListItem("0.375", @"3/8"""),
                            new ChoiceListItem("0.5", @"1/2""")
                        };
                    }
                    return new List<ChoiceListItem>() { new ChoiceListItem("0.5", @"1/2""") };
                });
            });
        }

        #endregion

        #region Rear Beam Props

        public Rule<string> RearBeamSize { get; set; }
        protected virtual void ConfigureRearBeamSize(Rule<string> rule)
        {
            rule.Define(() => (IsFirstLevel.Value || PreviousLevel.Value == null) ? FrontBeamSize.Value : PreviousLevel.Value.RearBeamSize.Value);
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() => LoadBeamSizeChoiceList.Value);
            });
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> RearBeamOrientation { get; set; }
        protected virtual void ConfigureRearBeamOrientation(Rule<string> rule)
        {
            rule.Define(() => (IsFirstLevel.Value || PreviousLevel.Value == null) ? FrontBeamOrientation.Value : PreviousLevel.Value.RearBeamOrientation.Value);
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() => OrientationChoiceList.Value);
            });
        }

        public Rule<float> RearBeamWeldDown { get; set; }
        protected virtual void ConfigureRearBeamWeldDown(Rule<float> rule)
        {
            rule.Define(() => (IsFirstLevel.Value || PreviousLevel.Value == null) ? FrontBeamWeldDown.Value : PreviousLevel.Value.RearBeamWeldDown.Value);
            rule.Min(RearBeamWeldDownMin);
            //rule.Max(ChannelWidth);
            rule.AddToUI();
        }

        public Rule<float> RearBeamWeldDownMin { get; set; }
        protected virtual void ConfigureRearBeamWeldDownMin(Rule<float> rule)
        {
            rule.Define(() => RearBeamOrientation.Value == "std" ? 0.125f : 0);
        }

        public Rule<string> RearBeamBracketType { get; set; }
        protected virtual void ConfigureRearBeamBracketType(Rule<string> rule)
        {
            rule.Define(() => (IsFirstLevel.Value || PreviousLevel.Value == null) ? FrontBeamBracketType.Value : PreviousLevel.Value.RearBeamBracketType.Value);
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() => BracketTypeChoiceList.Value);
            });
        }

        public Rule<string> RearBeamColor { get; set; }
        protected virtual void ConfigureRearBeamColor(Rule<string> rule)
        {
            rule.Define(() => (IsFirstLevel.Value || PreviousLevel.Value == null) ? Color.Value : PreviousLevel.Value.RearBeamColor.Value);
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() => Color.UIMetadata.ChoiceList.Value);
            });
        }

        public Rule<string> RearBeamHoleType { get; set; }
        protected virtual void ConfigureRearBeamHoleType(Rule<string> rule)
        {
            rule.Define(() => (IsFirstLevel.Value || PreviousLevel.Value == null) ? "round" : PreviousLevel.Value.RearBeamHoleType.Value);
            rule.AddToUI(ui =>
            {
                //ui.IsActive.Define(IsStandalone);
                ui.ChoiceList.Define(() => HoleTypeChoices.Value);
            });
            rule.ValueSet += (sender, e) =>
            {
                if (!RearBeamBoltType.UIMetadata.ChoiceList.Value.Any(c => c.Value.ToString() == RearBeamBoltType.Value))
                {
                    RearBeamBoltType.Reset();
                }

                if (rule.Value == "square" && RearBeamHardwareSize.Value == "0.375")
                {
                    RearBeamHardwareSize.Reset();
                }
            };
        }

        public Rule<string> RearBeamBoltType { get; set; }
        protected virtual void ConfigureRearBeamBoltType(Rule<string> rule)
        {
            rule.Define(() => (IsFirstLevel.Value || PreviousLevel.Value == null) ? "Hex" : PreviousLevel.Value.RearBeamBoltType.Value);
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() =>
                {
                    return GetAvailableBoltTypes(RearBeamHoleType.Value);
                });
            });
        }

        public Rule<string> RearBeamHardwareSize { get; set; }
        protected virtual void ConfigureRearBeamHardwareSize(Rule<string> rule)
        {
            rule.Define(() => (IsFirstLevel.Value || PreviousLevel.Value == null) ? "0.5" : PreviousLevel.Value.RearBeamHardwareSize.Value);
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() =>
                {
                    if (RearBeamHoleType.Value == "round")
                    {
                        return new List<ChoiceListItem>()
                        {
                            new ChoiceListItem("0.375", @"3/8"""),
                            new ChoiceListItem("0.5", @"1/2""")
                        };
                    }
                    return new List<ChoiceListItem>() { new ChoiceListItem("0.5", @"1/2""") };
                });
            });
        }

        #endregion

        #region Pallet Stops

        public Rule<int> PalletStopQtyPerPalletPosition { get; set; }
        protected virtual void ConfigurePalletStopQtyPerPalletPosition(Rule<int> rule)
        {
            rule.Define(() => (IsFirstLevel.Value || PreviousLevel.Value == null) ? 0 : PreviousLevel.Value.PalletStopQtyPerPalletPosition.Value);
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() => (new List<ChoiceListItem>()
                {
                    new ChoiceListItem(0),
                    new ChoiceListItem(1),
                    new ChoiceListItem(2)
                }));
            });
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> PalletStopStyle { get; set; }
        protected virtual void ConfigurePalletStopStyle(Rule<string> rule)
        {
            rule.Define(() =>
            {
                if (IsFirstLevel.Value || PreviousLevel.Value == null)
                {
                    if (IsDoubleDeep.Value)
                        return "PS-L-DD-X";
                    else
                        return "PS-R-X";
                }
                else
                    return PreviousLevel.Value.PalletStopStyle.Value;
            });
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => PalletStopQtyPerPalletPosition.Value == 0);
                ui.ChoiceList.Define(new List<ChoiceListItem>()
                {
                    new ChoiceListItem("PS-R-X", "PS-R-X"),
                    new ChoiceListItem("PS-L-DD-X", "PS-L-DD-X"),
                    new ChoiceListItem("NONE", "NONE")
                });
            });
        }

        public Rule<float> PalletStopWidth { get; set; }
        protected virtual void ConfigurePalletStopWidth(Rule<float> rule)
        {
            rule.Define(() => (IsFirstLevel.Value || PreviousLevel.Value == null) ? 4 : PreviousLevel.Value.PalletStopWidth.Value);
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => PalletStopQtyPerPalletPosition.Value == 0 || PalletStopStyle.Value == "NONE");
                ui.ChoiceList.Define(() =>
                {
                    if (PalletStopStyle.Value == "PS-R-X")
                    {
                        return new List<ChoiceListItem>()
                        {
                            new ChoiceListItem(3),
                            new ChoiceListItem(4),
                            new ChoiceListItem(6),
                            new ChoiceListItem(12)
                        };
                    }
                    else
                    {
                        return new List<ChoiceListItem>()
                        {
                            new ChoiceListItem(4)
                        };
                    }
                });
            });
        }

        public Rule<float> PalletStopOffset { get; set; }
        protected virtual void ConfigurePalletStopOffset(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (IsFirstLevel.Value || PreviousLevel.Value == null)
                {
                    if (PalletStopStyle.Value == "PS-L-DD-X")
                        return 0;
                    else
                        return 2;
                }
                else
                    return PreviousLevel.Value.PalletStopOffset.Value;
            });
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => PalletStopQtyPerPalletPosition.Value == 0 || (PalletStopStyle.Value != "PS-R-X"));
                ui.ChoiceList.Define(() =>
                {
                    if (PalletStopStyle.Value == "PS-R-X")
                    {
                        return new List<ChoiceListItem>()
                        {
                            new ChoiceListItem(2),
                            new ChoiceListItem(3),
                            new ChoiceListItem(4)
                        };
                    }
                    else if (PalletStopStyle.Value == "PS-L-DD-X")
                    {
                        return new List<ChoiceListItem>()
                        {
                            new ChoiceListItem(0)
                        };
                    }
                    else
                    {
                        return new List<ChoiceListItem>();
                    }
                });
            });
        }

        public Rule<string> PalletStopColor { get; set; }
        protected virtual void ConfigurePalletStopColor(Rule<string> rule)
        {
            rule.Define(() => (IsFirstLevel.Value || PreviousLevel.Value == null) ? Color.Value : PreviousLevel.Value.PalletStopColor.Value);
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => PalletStopQtyPerPalletPosition.Value == 0 || (PalletStopStyle.Value == "NONE"));
                ui.ChoiceList.Define(() => Color.UIMetadata.ChoiceList.Value);
            });
        }

        public Rule<List<float>> PalletStopHoleLocations { get; set; }
        protected virtual void ConfigurePalletStopHoleLocations(Rule<List<float>> rule)
        {
            rule.Define(() =>
            {
                List<float> locs = new List<float>();

                if (this.FrontProducts != null && this.FrontProducts.Any() && PalletStopQtyPerPalletPosition.Value > 0)
                {
                    foreach (var prod in this.FrontProducts)
                    {
                        Vector3 prodPos = prod.Position.Value;
                        float prodPalletPos = Math.Abs(prodPos.Z) + prod.LoadOverlap.Value;
                        float prodWidth = prod.PalletWidth.Value;
                        float frontPos = prodPalletPos + SupportOffsetFromPallet.Value;
                        float halfPos = prodPalletPos + (prodWidth * 0.5f);
                        float rearPos = prodPalletPos + (prodWidth - SupportOffsetFromPallet.Value);

                        if (PalletStopQtyPerPalletPosition.Value == 1)
                            locs.Add(halfPos);
                        else if (PalletStopQtyPerPalletPosition.Value == 2)
                        {
                            locs.Add(frontPos);
                            locs.Add(rearPos);
                        }
                    }
                }

                return locs;
            });
        }

        #endregion

        #region Pallet Support

        public Rule<string> PalletSupportColor { get; set; }
        protected virtual void ConfigurePalletSupportColor(Rule<string> rule)
        {
            rule.Define(() => (IsFirstLevel.Value || PreviousLevel.Value == null) ? Color.Value : PreviousLevel.Value.PalletSupportColor.Value);
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() => Color.UIMetadata.ChoiceList.Value);
            });
        }

        public Rule<int> PalletSupportQtyPerLoad { get; set; }
        protected virtual void ConfigurePalletSupportQtyPerLoad(Rule<int> rule)
        {
            rule.Define(() => (IsFirstLevel.Value || PreviousLevel.Value == null) ? 2 : PreviousLevel.Value.PalletSupportQtyPerLoad.Value);
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() => (new List<ChoiceListItem>()
                {
                    new ChoiceListItem(0),
                    new ChoiceListItem(1),
                    new ChoiceListItem(2),
                    new ChoiceListItem(3)
                }));
            });
        }

        public Rule<string> PalletSupportAngleTypeDefault { get; set; }
        protected virtual void ConfigurePalletSupportAngleTypeDefault(Rule<string> rule)
        {
            rule.Define(() =>
            {
                if (IsFirstLevel.Value || PreviousLevel.Value == null)
                {
                    if (PalletSupportType.Value == "xb")
                        return @"1-1/2 x 1-1/2 x 1/8";
                    else
                        return @"2 x 2 x 3/16";
                }
                else
                    return PreviousLevel.Value.AngleType.Value;
            });
        }

        public Rule<string> PalletSupportTypeDefault { get; set; }
        protected virtual void ConfigurePalletSupportTypeDefault(Rule<string> rule)
        {
            rule.Define(() =>
            {
                if (IsFirstLevel.Value || PreviousLevel.Value == null)
                {
                    if (IsDoubleDeep.Value)
                    {
                        if (IsDoubleDeepPost.Value)
                            return "xbw-dd1";
                        else
                            return "xbw-dd2";
                    }
                    else
                    {
                        if (FrontBeamSize.Value.ToUpper().Contains("C3") || FrontBeamSize.Value.ToUpper().Contains("C4"))
                            return "xb";
                        else
                            return "xbw";
                    }
                }
                else
                    return PreviousLevel.Value.PalletSupportType.Value;
            });
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> PalletSupportType { get; set; }
        protected virtual void ConfigurePalletSupportType(Rule<string> rule)
        {
            rule.Define(PalletSupportTypeDefault);
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() =>
                {
                    var cl = new List<ChoiceListItem>();

                    if ((FrontBeamSize.Value.ToUpper().Contains("C3") || FrontBeamSize.Value.ToUpper().Contains("C4") || FrontBeamSize.Value.ToUpper().Contains("C5"))
                        && FrontBeamOrientation.Value == "std")
                        cl.Add(new ChoiceListItem("xb", "XB"));

                    cl.Add(new ChoiceListItem("xbw", "XBW"));

                    if (IsDoubleDeep.Value && !IsDoubleDeepPost.Value)
                        cl.Add(new ChoiceListItem("xbw-dd2", "XBW-DD2"));

                    if (IsDoubleDeepPost.Value)
                        cl.Add(new ChoiceListItem("xbw-dd1", "XBW-DD1"));


                    cl.Add(new ChoiceListItem("xbwr1", "XBWR1"));
                    cl.Add(new ChoiceListItem("xbwr2", "XBWR2"));
                    cl.Add(new ChoiceListItem("none", "NONE"));

                    return cl;
                });
            });
        }

        public Rule<List<float>> PalletSupportLocations { get; set; }
        protected virtual void ConfigurePalletSupportLocations(Rule<List<float>> rule)
        {
            rule.Define(() =>
            {
                List<float> locs = new List<float>();

                if (this.FrontProducts != null && this.FrontProducts.Any() && PalletSupportQtyPerLoad.Value > 0)
                {
                    foreach (var prod in this.FrontProducts)
                    {
                        Vector3 prodPos = prod.Position.Value;
                        float prodPalletPos = Math.Abs(prodPos.Z) + prod.LoadOverlap.Value;
                        float prodWidth = prod.PalletWidth.Value;
                        float frontPos = prodPalletPos + SupportOffsetFromPallet.Value;
                        float halfPos = prodPalletPos + (prodWidth * 0.5f);
                        float rearPos = prodPalletPos + (prodWidth - SupportOffsetFromPallet.Value);

                        if (PalletSupportQtyPerLoad.Value == 1)
                            locs.Add(halfPos);
                        else if (PalletSupportQtyPerLoad.Value == 2)
                        {
                            locs.Add(frontPos);
                            locs.Add(rearPos);
                        }
                        else if (PalletSupportQtyPerLoad.Value == 3)
                        {
                            locs.Add(frontPos);
                            locs.Add(halfPos);
                            locs.Add(rearPos);
                        }
                    }
                }

                return locs;
            });
        }

        #endregion

        public Rule<List<ChoiceListItem>> HoleTypeChoices { get; set; }
        protected virtual void ConfigureHoleTypeChoices(Rule<List<ChoiceListItem>> rule)
        {
            rule.Define(() =>
            {
                return new List<ChoiceListItem>()
                {
                    new ChoiceListItem("round", "Round"),
                    new ChoiceListItem("square", "Square")
                };
            });
        }

        public Rule<List<float>> BeamHoleLocations { get; set; }
        protected virtual void ConfigureBeamHoleLocations(Rule<List<float>> rule)
        {
            rule.Define(() =>
            {
                List<float> locs = new List<float>();

                if (PalletStopHoleLocations.Value != null && PalletStopHoleLocations.Value.Any())
                    locs.AddRange(PalletStopHoleLocations.Value);

                if (PalletSupportLocations.Value != null && PalletSupportLocations.Value.Any())
                    locs.AddRange(PalletSupportLocations.Value);

                //remove any duplicates
                if (locs != null && locs.Any())
                {
                    locs = locs.Distinct().ToList();
                    locs = locs.OrderBy(s => s).ToList();
                }
                
                return locs;
            });
        }

        [CadRule(CadRuleUsageType.Parameter, "Height")]
        public Rule<float> FrontBeamHeight { get; set; }
        protected virtual void ConfigureFrontBeamHeight(Rule<float> rule)
        {
            rule.Define(() => GetChannelHeight(FrontBeamSize.Value));
        }

        public Rule<float> FrontBeamWebThickness { get; set; }
        protected virtual void ConfigureFrontBeamWebThickness(Rule<float> rule)
        {
            rule.Define(() => GetChannelData(FrontBeamSize.Value).WebThickness);
        }

        public Rule<float> FrontBeamDepth { get; set; }
        protected virtual void ConfigureFrontBeamDepth(Rule<float> rule)
        {
            rule.Define(() => GetChannelData(FrontBeamSize.Value).Depth);
        }

        public Rule<float> RearBeamHeight { get; set; }
        protected virtual void ConfigureRearBeamHeight(Rule<float> rule)
        {
            rule.Define(() => GetChannelHeight(RearBeamSize.Value));
        }

        public Rule<float> MaxBeamHeight { get; set; }
        protected virtual void ConfigureMaxBeamHeight(Rule<float> rule)
        {
            rule.Define(() => ((FrontBeamHeight.Value > RearBeamHeight.Value) ? FrontBeamHeight.Value : RearBeamHeight.Value));
        }

        public LookupRule<StorageSystem> StorageSystemComp { get; set; }

        public Rule<List<Product>> ProductOptions { get; set; }
        protected virtual void ConfigureProductOptions(Rule<List<Product>> rule)
        {
            rule.Define(() =>
            {
                List<Product> retVal = new List<Product>();

                if (StorageSystemComp != null && StorageSystemComp.Value != null)
                {
                    List<Product> prods = StorageSystemComp.Value?.Products?.ProductList.Value;

                    if (prods != null && prods.Any())
                        retVal = prods;
                }

                return retVal;
            });
        }

        public Rule<string> ProductName { get; set; }
        protected virtual void ConfigureProductName(Rule<string> rule)
        {
            rule.Define(() =>
            {
                if (IsFirstLevel.Value)
                {
                    if (ProductChoices.Value.Count > 0)
                        return ProductChoices.Value.First().Value.ToString();
                    else
                        return "";
                }
                else
                    return PreviousLevel.Value?.ProductName.Value;
            });
            rule.AddToUI(ui =>
            {
                ui.Label.Define("Product");
                ui.ChoiceList.Define(() => ProductChoices.Value);
            });
        }

        public Rule<List<ChoiceListItem>> ProductChoices { get; set; }
        protected virtual void ConfigureProductChoices(Rule<List<ChoiceListItem>> rule)
        {
            rule.Define(() =>
            {
                List<ChoiceListItem> retList = new List<ChoiceListItem>();

                if (ProductOptions.Value != null)
                {
                    //TODO: Figure out what ID we can use to avoid errors when customer deletes a product.
                    //ProductOptions.Value.Select((x, index) => new
                    //{
                    //    item = x,
                    //    id = index
                    //})
                    //.ToList()
                    //.ForEach(prod =>
                    //{
                    //    retList.Add(new ChoiceListItem(prod.id.ToString(), prod.item.ProductName.Value));
                    //});
                    ProductOptions.Value.ForEach(prod =>
                    {
                        retList.Add(new ChoiceListItem(prod.Path, prod.ProductName.Value));
                    });
                }

                return retList;
            });
        }

        public Rule<Product> ActiveProduct { get; set; }
        protected virtual void ConfigureActiveProduct(Rule<Product> rule)
        {
            rule.Define(() =>
            {
                Product retVal = null;

                if (ProductChoices.Value.Any() && ProductChoices.Value.Any(p => p.Value.ToString() == ProductName.Value))
                {
                    retVal = ProductOptions.Value.FirstOrDefault(p => p.Path == ProductName.Value);
                    //Set Product child values
                }

                return retVal;
            });
        }

        #endregion

        #region Public Methods

        public IEnumerable<ChoiceListItem> GetAvailableBoltTypes(string holeType)
        {
            var availTypes = holeType == "round" ? new List<string>() { "Hex" } : new List<string>() { "Hex", "Carriage" };
            var props = typeof(BoltTypes).GetFields().Where(b => availTypes.Contains(b.Name));
            return props?.Select(p => new ChoiceListItem(p.Name, p.GetValue(null).ToString())) ?? new List<ChoiceListItem>();
        }

        public Vector3 GetFrontProductPosition(int index, float productDepth)
        {
            float totalOverlap = (productDepth - FrontFrameDepth.Value);
            float xOffset = -(FrontFrameDepth.Value / 2) - (totalOverlap/2);
            float zOffset = 0;

            xOffset = xOffset + FrontProductOverhang.Value;

            if (IsDoubleDeepPost.Value)
            {
                //xOffset = xOffset + 0.5f;
            }

            if (index == 0)
                zOffset = LoadToFrameOffset.Value;
            else
            {
                Product prevChild = FrontProducts[index - 1];
                zOffset = Math.Abs(prevChild.Position.Value.Z) + prevChild.PalletLoadWidth.Value + LoadToLoadOffset.Value;
            }

            return Helper.Vxz(xOffset, -zOffset);
        }

        public ChannelData GetChannelData(string beamSize)
        {
            return AllChannelData.Value.FirstOrDefault(d => d.ChannelSize == beamSize);
        }

        public float GetChannelHeight(string beamSize)
        {
            return GetChannelData(beamSize).Width;
        }

        public string GetBracketTypeDefault(float topBracketToBtmBeamDist)
        {
            if (topBracketToBtmBeamDist <= 7.875f)
                return "8";
            else if (topBracketToBtmBeamDist <= 8.875f)
                return "9";
            else if (topBracketToBtmBeamDist <= 11.875f)
                return "12";
            else
                return "13";
        }

        #endregion

        #region IShelfLoadBeamInputs Implementation

        public Rule<float> PalletWidth { get; set; }
        protected virtual void ConfigurePalletWidth(Rule<float> rule)
        {
            rule.Define(() => ActiveProduct.Value != null ? ActiveProduct.Value.PalletWidth.Value : 20);
        }

        public Rule<float> LoadWidth { get; set; }
        protected virtual void ConfigureLoadWidth(Rule<float> rule)
        {
            rule.Define(() => ActiveProduct.Value != null ? ActiveProduct.Value.LoadWidth.Value : PalletWidth.Value);
        }

        public Rule<float> LoadOverlap { get; set; }
        public Rule<int> LoadsWide { get; set; }
        protected virtual void ConfigureLoadsWide(Rule<int> rule)
        {
            rule.Define(() =>
            {
                if (IsFirstLevel.Value || PreviousLevel.Value == null)
                {
                    if (ParentComp.Value == null)
                        return 1;
                    else
                        return ParentComp.Value.LoadsWide.Value;
                }
                else
                    return PreviousLevel.Value.LoadsWide.Value;
            });
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(true);
            });
        }

        public Rule<float> LoadToFrameOffset { get; set; }
        protected virtual void ConfigureLoadToFrameOffset(Rule<float> rule)
        {
            rule.Define(() =>
            {
                float totalConsumedSpace = ((LoadWidth.Value * LoadsWide.Value) + (LoadToLoadOffset.Value * (LoadsWide.Value - 1)));
                return ((BayWidth.Value - totalConsumedSpace) / 2);
            });
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(true);
            });
        }

        public Rule<float> LoadToFrameOffsetMin { get; set; }
        public Rule<float> LoadToLoadOffset { get; set; }
        protected virtual void ConfigureLoadToLoadOffset(Rule<float> rule)
        {
            rule.Define(() => (IsFirstLevel.Value || PreviousLevel.Value == null) ? 8 : PreviousLevel.Value.LoadToLoadOffset.Value);
            rule.Min(LoadToLoadOffsetMin);
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => (LoadsWide.Value <= 1));
            });
        }

        public Rule<float> LoadToLoadOffsetMin { get; set; }
        public Rule<float> SupportOffsetFromPallet { get; set; }

        #endregion

        #region IChannelData Implementation

        public Rule<List<ChannelData>> AllChannelData { get; set; }
        public Rule<List<ChoiceListItem>> AllChannelSizesChoiceList { get; set; }

        #endregion

        #region ILoadBeamData Implementation
        public Rule<List<string>> BeamSizesAvailable { get; set; }
        public Rule<List<ChoiceListItem>> LoadBeamSizeChoiceList { get; set; }
        public Rule<List<ChoiceListItem>> OrientationChoiceList { get; set; }
        public Rule<List<ChoiceListItem>> BracketTypeChoiceList { get; set; }
        #endregion

        #region IDepthMaster Implementation

        public Rule<bool> IsDoubleDeep { get; set; }
        public Rule<string> DoubleDeepType { get; set; }
        public Rule<bool> IsDoubleDeepPost { get; set; }
        public Rule<float> FrontFrameDepth { get; set; }
        protected virtual void ConfigureFrontFrameDepth(Rule<float> rule)
        {
            rule.Define(() =>
            {
                float retVal = 0;

                if (ActiveProduct.Value != null)
                {
                    if (IsDoubleDeep.Value)
                    {
                        if (IsDoubleDeepPost.Value)
                            retVal = ActiveProduct.Value.PalletLoadDepth.Value + 1;
                        else
                            retVal = ActiveProduct.Value.PalletLoadDepth.Value;
                    }
                    else
                        retVal = ActiveProduct.Value.PalletLoadDepth.Value;
                }
                else
                    retVal = 48;

                //retVal = retVal - (FrontProductOverhang.Value + RearProductOverhang.Value);

                return retVal;
            });
        }

        public Rule<float> RearFrameDepth { get; set; }
        protected virtual void ConfigureRearFrameDepth(Rule<float> rule)
        {
            rule.Define(() =>
            {
                float retVal = 0;

                if (ActiveProduct.Value != null)
                {
                    if (IsDoubleDeep.Value)
                    {
                        if (IsDoubleDeepPost.Value)
                            retVal = ActiveProduct.Value.PalletLoadDepth.Value -1;
                        else
                            retVal = ActiveProduct.Value.PalletLoadDepth.Value;
                    }
                    else
                        retVal = 0;
                }
                else
                    retVal = FrontFrameDepth.Value;

                //retVal = retVal - (FrontProductOverhang.Value + RearProductOverhang.Value);

                return retVal;
            });
        }

        public Rule<bool> HasFrameTies { get; set; }
        public Rule<string> FrameTieType { get; set; }
        public Rule<float> FrameTieLengthMin { get; set; }
        public Rule<float> FrameTieLength { get; set; }

        [CadRule(CadRuleUsageType.Parameter, "Width")]
        public ReadOnlyRule<float> OverallDepth { get; set; }

        #endregion

        #region IAngleData Implementation
        public Rule<List<AngleData>> AllAngleData { get; set; }
        public Rule<List<ChoiceListItem>> AllAngleTypesChoiceList { get; set; }
        public Rule<AngleData> ActiveAngleData { get; set; }
        public Rule<List<string>> AngleSizesAvailable { get; set; }
        protected virtual void ConfigureAngleSizesAvailable(Rule<List<string>> rule)
        {
            rule.Define(() =>
            {
                List<string> choices = new List<string>();

                choices.Add(@"1-1/2 x 1-1/2 x 1/8");
                choices.Add(@"2 x 2 x 1/8");

                if (PalletSupportType.Value != "xb")
                {
                    choices.Add(@"2 x 2 x 3/16");
                    choices.Add(@"3 x 2 x 3/16");
                    choices.Add(@"2-1/2 x 2-1/2 x 3/16");
                }

                if (PalletSupportType.Value == "xbw" || PalletSupportType.Value == "xbw-dd1")
                    choices.Add(@"3 x 2 x 1/4");

                return choices;
            });
        }

        public Rule<string> AngleType { get; set; }
        protected virtual void ConfigureAngleType(Rule<string> rule)
        {
            rule.Define(PalletSupportAngleTypeDefault);
            rule.AddToUI(ui =>
            {
                ui.Label.Define("Pallet Support Angle Type");
                ui.ChoiceList.Define(() => Helper.GetFilteredChoices(AllAngleTypesChoiceList.Value, AngleSizesAvailable.Value));
            });
        }
        public Rule<float> AngleWidth { get; set; }
        public Rule<float> AngleHeight { get; set; }
        public Rule<float> AngleThickness { get; set; }
        public Rule<string> MaterialPartNumber { get; set; }
        #endregion

        #region IColorComponent Implementation

        public Rule<string> Color { get; set; }
        public ReadOnlyRule<AdvanceColor> AdvanceColor { get; set; }
        public ReadOnlyRule<string> ColorDescription { get; set; }
        public StorageConfiguratorRepository Repository { get; set; }
        public Rule<string> ColorBOMName { get; set; }
        public ReadOnlyRule<string> ColorShortDescription { get; set; }

        public Rule<double> PartWeight { get; set; }
        public Rule<double> PartPaintWeight { get; set; }
        public Rule<float> WeightPerGallon { get; set; }
        public CsiRule<double> PaintCostPerGallon { get; set; }
        public Rule<float> PaintCost { get; set; }

        #endregion

        #region IRevisablePartImplementation

        public FactoryRevisionRule<string> FactoryRevision { get; set; }

        #endregion

        #region Children

        public ChildList<Product> FrontProducts { get; set; }
        protected virtual void ConfigureFrontProducts(ChildList<Product> list)
        {
            list.Qty.Define(() => ActiveProduct.Value != null ? LoadsWide.Value : 0);
            list.ConfigureChildren((child, index) =>
            {
                child.Index.Define(() => ActiveProduct.Value.Index.Value);
                child.ProductName.Define(() => ActiveProduct.Value.ProductName.Value);
                child.LiftOff.Define(() => ActiveProduct.Value.LiftOff.Value);
                child.LoadWidth.Define(() => ActiveProduct.Value.LoadWidth.Value);
                child.LoadHeight.Define(() => ActiveProduct.Value.LoadHeight.Value);
                child.LoadDepth.Define(() => ActiveProduct.Value.LoadDepth.Value);
                child.PalletType.Define(() => ActiveProduct.Value.PalletType.Value);
                child.PalletWidth.Define(() => ActiveProduct.Value.PalletWidth.Value);
                child.PalletHeight.Define(() => ActiveProduct.Value.PalletHeight.Value);
                child.PalletDepth.Define(() => ActiveProduct.Value.PalletDepth.Value);
                child.Weight.Define(() => ActiveProduct.Value.Weight.Value);
                child.ShowGraphics.Define(ShowProducts);
                child.Position.Define(() => GetFrontProductPosition(index, child.LoadDepth.Value));
            });
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(Colors.Gray);
            //material.Opacity.Define(0.9f);
        }

        protected sealed override void ConfigurePosition(ReadOnlyRule<Vector3> rule)
        {
            rule.Define(() => (Helper.Vy(Elevation.Value)));
        }

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define("Level");
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(false);
        }

        #endregion

        #region Graphics

        //public Block bBlock { get; set; }
        //protected virtual void ConfigurebBlock(Block geo)
        //{
        //    geo.Width.Define(ChannelWebThickness);
        //    geo.Height.Define(ChannelWidth);
        //    geo.Depth.Define(Length);
        //    geo.Position.Define(() => Helper.Vxz(ChannelWebThickness.Value / 2, Length.Value / 2));
        //}


        //public UCS UCS { get; set; }
        //protected virtual void ConfigureUCS(UCS child)
        //{
        //    child.Diameter.Define(0.25f);
        //    child.Position.Define(() => (Helper.Vy(0)));
        //}

        #endregion

        #region Protected Methods

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureShelfLoadBeamInputsRules();
            this.ConfigureLoadBeamDataRules();
            this.ConfigureDepthMasterRules();
            this.ConfigureAngleRules();
            this.ConfigureColorComponent();
            this.ConfigureRevisablePart();
        }

        #endregion

    }
}
