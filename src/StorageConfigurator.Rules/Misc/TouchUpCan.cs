﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ruler.Cpq;
using Ruler.Rules;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class TouchUpCan : AdvancePart, Ruler.Cpq.IBomPart, IBomPart
    {
        #region Constructors

        public TouchUpCan(ModelContext modelContext) : base(modelContext) { }

        public TouchUpCan(string name, Part parent) : base(name, parent) { }

        #endregion

        #region Rules

        public Rule<string> Type { get; set; }
        protected virtual void ConfigureType(Rule<string> rule)
        {
            rule.Define(() => rule.UIMetadata.ChoiceList.Value.First().Value.ToString());
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() => TypeChoices.Value);
            });
        }

        public Rule<List<ChoiceListItem>> TypeChoices { get; set; }
        protected virtual void ConfigureTypeChoices(Rule<List<ChoiceListItem>> rule)
        {
            rule.Define(() =>
            {
                return new List<ChoiceListItem>()
                {
                    new ChoiceListItem("OT.PNT.C.BYEL", @"A-2008-79848 Yellow Spray Cans"),
                    new ChoiceListItem("OT.PNT.C.GRE", @"A-2008-79852 Vista Green Spray Cans"),
                    new ChoiceListItem("OT.PNT.C.GRY", @"A-2008-3978 Gray Spray Cans"),
                    new ChoiceListItem("OT.PNT.C.IVO", @"Colonial Ivory Spray Cans"),
                    new ChoiceListItem("OT.PNT.C.LTGRAY", @"Light Gray Spray Cans"),
                    new ChoiceListItem("OT.PNT.C.RBLU", @"A-2008-79850 Dark Blue Spray Cans"),
                    new ChoiceListItem("OT.PNT.C.SORG", @"A-2008-4606 Orange Spray Cans"),
                    new ChoiceListItem("OT.PNT.C.SRED", @"A-2008-79851 Red Spray Cans")
                };
            });
        }

        public Rule<double> Qty { get; set; }
        protected virtual void ConfigureQty(Rule<double> rule)
        {
            rule.Define(1);
            rule.AddToUI(ui =>
            {
                ui.Label.Define("Quantity");
                //ui.ReadOnly.Define(() => !IsStandalone.Value);
            });
        }

        #endregion

        #region IBomPart Implementation

        public Rule<string> DatabasePartNumber { get; set; }
        public Rule<string> PartNumberPrefix { get; set; }
        public PartNumberRule<string> PartNumber { get; set; }
        protected virtual void ConfigurePartNumber(PartNumberRule<string> rule)
        {
            rule.Define(Type);
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(true);
            });
        }

        public Rule<string> Description { get; set; }
        protected virtual void ConfigureDescription(Rule<string> rule)
        {
            rule.Define(() =>
            {
                var selectedValue = Type.UIMetadata.ChoiceList.Value.FirstOrDefault(cli => cli.Value.ToString() == Type.Value);
                return selectedValue?.Text;
            });
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(true);
            });
        }

        public Rule<string> BOMDescription { get; set; }
        protected virtual void ConfigureBOMDescription(Rule<string> rule)
        {
            rule.Define(Description);
        }
        public Rule<string> BOMDescription1 { get; set; }
        public Rule<string> BOMDescription2 { get; set; }
        public Rule<string> BOMDescription3 { get; set; }
        public Rule<string> BOMDescription4 { get; set; }
        public Rule<string> BOMDescription5 { get; set; }
        public Rule<string> BOMDescription6 { get; set; }
        public Rule<string> BOMDescription7 { get; set; }
        public Rule<string> BOMDescription8 { get; set; }
        public Rule<string> BOMDescription9 { get; set; }
        public Rule<string> BOMDescription10 { get; set; }
        public Rule<string> BOMDescription11 { get; set; }

        public Rule<string> Color { get; set; }
        public ReadOnlyRule<AdvanceColor> AdvanceColor { get; set; }
        public ReadOnlyRule<string> ColorDescription { get; set; }
        public StorageConfiguratorRepository Repository { get; set; }
        public Rule<string> ColorBOMName { get; set; }
        public ReadOnlyRule<string> ColorShortDescription { get; set; }

        public Rule<double> PartWeight { get; set; }
        public Rule<double> PartPaintWeight { get; set; }
        public Rule<float> WeightPerGallon { get; set; }
        public CsiRule<double> PaintCostPerGallon { get; set; }
        public Rule<float> PaintCost { get; set; }


        #endregion

        #region Ruler CPQ IBomPart Implementation

        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {
            child.ItemQty.Define(Qty);
            child.Description.Define(Description);
            child.PartNumber.Define(PartNumber);
            child.IsPurchased.Define(true);
            child.AddedCostList.Define(() => new List<AddedCost>
            {
                new AddedCost("Paint", PaintCost.Value)
            });
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define("");
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(true);
        }

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureBomPartRules();
        }

        #endregion


    }
}
