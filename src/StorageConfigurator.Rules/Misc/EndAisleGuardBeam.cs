﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;
using Ruler.Rules.Validation;
using Ruler.Rules.Graphics;
using System.Linq;
using System.Numerics;
using StorageConfigurator.Data;
using Ruler.Cpq;

namespace StorageConfigurator.Rules
{
    public class EndAisleGuardBeam : Channel, Ruler.Cpq.IBomPart
    {

        #region Private Fields

        public AdvanceCSIItemService CsiPartService { get; }

        #endregion

        #region Constructors

        public EndAisleGuardBeam(ModelContext modelContext, StorageConfiguratorRepository repo, AdvanceCSIItemService csiPartService) : base(modelContext, repo) 
        {
            Repository = repo;
            CsiPartService = csiPartService;
        }

        public EndAisleGuardBeam(string name, Part parent, StorageConfiguratorRepository repo, AdvanceCSIItemService csiPartService) : base(name, parent, repo) 
        {
            Repository = repo;
            CsiPartService = csiPartService;
        }

        #endregion

        #region Rules



        #endregion

        #region CPQ.IBomPart Implementation

        protected override void ConfigureDescriptionPrefix(Rule<string> rule)
        {
            rule.Define(() => $"Beam {PartNumberPrefix.Value.Replace("-", "")}");
        }

        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {
            child.PartNumber.Define(PartNumber);
            child.Description.Define(BOMUIDescription);
            child.MaterialPartNumber.Define(MaterialPartNumber);
            child.MaterialQty.Define(() => Length.Value / 12);
            child.ItemMaterialCost.Define(() => child.ItemRawMaterialWeight.Value * child.MaterialUnitCost.Value);
        }

        #endregion

        #region Labor Rules

        public Rule<double> BundleQty { get; set; }
        protected virtual void ConfigureBundleQty(Rule<double> rule)
        {
            rule.Define(() => CsiPartService.GetItemBundle(MaterialPartNumber.Value, Bom.PlantCode.Value));
        }

        #endregion

        #region IChannel Overrides

        protected override void ConfigureChannelSizesAvailable(Rule<List<string>> rule)
        {
            rule.Define(() => (new List<string> { "C4x4.5", "C6x8.2"}));
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(Colors.Yellow);
            material.Opacity.Define(0.9f);
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(false);
        }

        #endregion

        #region Protected Methods

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureChannelRules();
        }

        #endregion

    }
}
