﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;
using Ruler.Cpq;
using StorageConfigurator.Data;
using Ruler.Rules.Graphics;
using System.Numerics;

namespace StorageConfigurator.Rules
{
    public class EndAisleGuardCap : AdvancePart, IRevisablePart, IBomPart, Ruler.Cpq.IBomPart
    {
        #region Constructors

        public EndAisleGuardCap(ModelContext modelContext) : base(modelContext) { }

        public EndAisleGuardCap(string name, Part parent) : base(name, parent) { }

        #endregion

        #region Rules

        public Rule<float> ChannelWidth { get; set; }
        protected virtual void ConfigureChannelWidth(Rule<float> rule)
        {
            rule.Define(4);
        }

        public Rule<float> Thickness { get; set; }
        protected virtual void ConfigureThickness(Rule<float> rule)
        {
            rule.Define(0.1196f);
        }

        [CadRule(CadRuleUsageType.Parameter)]
        public Rule<float> Height { get; set; }
        protected virtual void ConfigureHeight(Rule<float> rule)
        {
            rule.Define(ChannelWidth);
        }

        [CadRule(CadRuleUsageType.Parameter)]
        public Rule<float> Width { get; set; }
        protected virtual void ConfigureWidth(Rule<float> rule)
        {
            rule.Define(() =>
            {
                switch (ChannelWidth.Value)
                {
                    case 3:
                        return 1.563f;
                    case 4:
                        return 1.8f;
                    case 5:
                        return 2f;
                    default:
                        return 2.1f;
                }
            });
        }

        [CadRule(CadRuleUsageType.Parameter)]
        public Rule<float> FlangeLength { get; set; }
        protected virtual void ConfigureFlangeLength(Rule<float> rule)
        {
            rule.Define(() =>
            {
                switch (ChannelWidth.Value)
                {
                    case 3:
                        return 0.809f;
                    case 4:
                        return 0.912f;
                    case 5:
                        return 1.008f;
                    default:
                        return 1.08f;
                }
            });
        }

        public Rule<float> Depth { get; set; }
        protected virtual void ConfigureDepth(Rule<float> rule)
        {
            rule.Define(() => Helper.Sin(60) * Width.Value);
        }

        //Used to determine whether component is purchased. UT = Purchased
        public Rule<string> Location { get; set; }
        protected virtual void ConfigureLocation(Rule<string> rule)
        {
            rule.Define(() =>
            {
                var bomLoc = Bom?.PlantCode.Value;
                return bomLoc != null && bomLoc.ToUpper().Contains("UT") ? "UT" : "GA";
            });
        }

        #endregion

        #region IRevisablePartImplementation

        public FactoryRevisionRule<string> FactoryRevision { get; set; }

        #endregion

        #region IBomPart Implementation

        public Rule<string> DatabasePartNumber { get; set; }
        public PartNumberRule<string> PartNumber { get; set; }
        public Rule<string> PartNumberPrefix { get; set; }
        public Rule<string> Description { get; set; }
        public Rule<string> BOMDescription { get; set; }
        public Rule<string> BOMDescription1 { get; set; }
        public Rule<string> BOMDescription2 { get; set; }
        public Rule<string> BOMDescription3 { get; set; }
        public Rule<string> BOMDescription4 { get; set; }
        public Rule<string> BOMDescription5 { get; set; }
        public Rule<string> BOMDescription6 { get; set; }
        public Rule<string> BOMDescription7 { get; set; }
        public Rule<string> BOMDescription8 { get; set; }
        public Rule<string> BOMDescription9 { get; set; }
        public Rule<string> BOMDescription10 { get; set; }
        public Rule<string> BOMDescription11 { get; set; }
        public Rule<string> Color { get; set; }
        public ReadOnlyRule<AdvanceColor> AdvanceColor { get; set; }
        public ReadOnlyRule<string> ColorDescription { get; set; }
        public StorageConfiguratorRepository Repository { get; set; }
        public Rule<string> ColorBOMName { get; set; }
        public ReadOnlyRule<string> ColorShortDescription { get; set; }

        public Rule<double> PartWeight { get; set; }
        public Rule<double> PartPaintWeight { get; set; }
        public Rule<float> WeightPerGallon { get; set; }
        public CsiRule<double> PaintCostPerGallon { get; set; }
        public Rule<float> PaintCost { get; set; }

        #endregion

        #region CPQ.IBomPart Implementation

        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {
            child.PartNumber.Define(PartNumber);
            child.Description.Define(() => $"End Cap for C{ChannelWidth.Value} Channel");
            child.MaterialPartNumber.Define(MaterialPartNumber);
            child.MaterialQty.Define(() => Width.Value / 12);
        }

        #endregion

        #region Material Rules

        public Rule<string> MaterialPartNumber { get; set; }
        protected virtual void ConfigureMaterialPartNumber(Rule<string> rule)
        {
            rule.Define(default(string));
        }

        #endregion

        #region Labor Rules

        public Rule<double> BurnPartCarriersPerHour { get; set; }
        protected virtual void ConfigureBurnPartCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> FormPartCarriersPerHour { get; set; }
        protected virtual void ConfigureFormPartCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        #endregion

        #region Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(Colors.Yellow);
            material.Opacity.Define(0.9f);
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(false);
        }

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define("EndAisleGuardCap");
        }

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureRevisablePart();
            this.ConfigureBomPartRules();
        }

        #endregion

        #region Graphics

        public StlGeometry CapSTL { get; set; }
        protected virtual void ConfigureCapSTL(StlGeometry geo)
        {
            geo.Url.Define(() => $"assets/STL/EndAisleGuardCap{Height.Value}.stl");
        }

        //public Block Block { get; set; }
        //protected virtual void ConfigureBlock(Block geo)
        //{
        //    geo.Width.Define(Thickness);
        //    geo.Height.Define(Height);
        //    geo.Depth.Define(Depth);
        //    geo.Position.Define(() => Helper.Vxz(geo.Width.Value / 2, geo.Depth.Value / 2));
        //    //geo.Rotation.Define(() => Helper.CreateRotation(Vector3.UnitY, 30));
        //}

        //public Block AngleBlock { get; set; }
        //protected virtual void ConfigureAngleBlock(Block geo)
        //{
        //    geo.Width.Define(Thickness);
        //    geo.Height.Define(Height);
        //    geo.Depth.Define(Width);
        //    geo.Position.Define(() => Helper.Vxz((Helper.Sin(30) * (geo.Depth.Value/2)) / Helper.Tan(30), Helper.Sin(30) * geo.Depth.Value));
        //    geo.Rotation.Define(() => Helper.CreateRotation(Vector3.UnitY, 30));
        //}

        //public UCS UCS { get; set; }
        //protected virtual void ConfigureUCS(UCS child)
        //{
        //    child.Diameter.Define(0.25f);
        //    child.Position.Define(() => (Helper.Vy(0)));
        //}

        #endregion

    }
}
