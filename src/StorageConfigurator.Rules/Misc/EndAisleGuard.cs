﻿using Ruler.Rules;
using Ruler.Rules.Validation;
using Ruler.Rules.Graphics;
using System.Numerics;
using System.Linq;
using StorageConfigurator.Data;
using Ruler.Cpq;
using System.Collections.Generic;

namespace StorageConfigurator.Rules
{
    public class EndAisleGuard : PaintedPart, IChannel, IRevisablePart, Ruler.Cpq.IBomPart
    {
        
        #region Constructors

        public EndAisleGuard(ModelContext modelContext, StorageConfiguratorRepository repo) : base(modelContext)
        {
            Repository = repo;
        }

        public EndAisleGuard(string name, Part parent, StorageConfiguratorRepository repo) : base(name, parent)
        {
            Repository = repo;
        }

        #endregion

        #region Constants

        public const float BeamEndCapOffset = 1.375f;
        public const float PostToBracketGap = 0.0625f;
        public const float MultipieceBeamGap = 0.0625f;

        #endregion

        #region Rules

        public Rule<bool> IsStandalone { get; set; }
        protected virtual void ConfigureIsStandalone(Rule<bool> rule)
        {
            rule.Define(() => (Parent == null || (Parent.GetType() != typeof(Row))));
        }

        public Rule<string> Type { get; set; }
        protected virtual void ConfigureType(Rule<string> rule)
        {
            rule.Define("eag");
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !IsStandalone.Value);
                ui.ChoiceList.Define(new List<ChoiceListItem>()
                {
                    new ChoiceListItem("eag", "One Piece"),
                    new ChoiceListItem("eagFront", "Two Piece Front"),
                    new ChoiceListItem("eagRear", "Two Piece Rear")
                });
            });
        }

        public Rule<string> EndAisleGuardChannelSize { get; set; }
        protected virtual void ConfigureEndAisleGuardChannelSize(Rule<string> rule)
        {
            rule.Define("C4x4.5");
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(new List<ChoiceListItem>()
                {
                    new ChoiceListItem("C4x4.5", "C4x4.5"),
                    new ChoiceListItem("C6x8.2", "C6x8.2")
                });
            });
        }

        public Rule<double> Qty { get; set; }
        protected virtual void ConfigureQty(Rule<double> rule)
        {
            rule.Define(1);
            rule.AddToUI(ui =>
            {
                ui.Label.Define("Quantity");
                ui.ReadOnly.Define(() => !IsStandalone.Value);
            });
        }

        //[UIRule]
        public ReadOnlyRule<float> BeamLength { get; set; }
        protected virtual void ConfigureBeamLength(ReadOnlyRule<float> rule)
        {
            rule.Define(() => OverallBracketToBracketDistance.Value
                            + (HasLeftCap.Value ? -BeamEndCapOffset : (BracketThickness.Value - MultipieceBeamGap))
                            + (HasRightCap.Value ? -BeamEndCapOffset : (BracketThickness.Value - MultipieceBeamGap)));
        }

        public Rule<float> BracketThickness { get; set; }
        protected virtual void ConfigureBracketThickness(Rule<float> rule)
        {
            rule.Define(0.179f);
        }

        public Rule<float> TotalPostSpace { get; set; }
        protected virtual void ConfigureTotalPostSpace(Rule<float> rule)
        {
            rule.Define(() => SpaceInfo.Value.Sum(s => s.Item2));
        }

        public Rule<float> OverallBracketToBracketDistance { get; set; }
        protected virtual void ConfigureOverallBracketToBracketDistance(Rule<float> rule)
        {
            rule.Define(() => BracketInfo.Value.Skip(1).Sum(s => s.Item2));
        }

        //To be used by parent for positioning two and three piece.
        public Rule<float> LengthFromGuardOriginToRear { get; set; }
        protected virtual void ConfigureLengthFromGuardOriginToRear(Rule<float> rule)
        {
            rule.Define(() => OverallBracketToBracketDistance.Value + BracketInfo.Value.First().Item2 +
                                    (BracketInfo.Value.Last().Item1 == "rev" ? BracketThickness.Value : 0));
        }

        #region Post Inputs

        public Rule<int> SpaceQty { get; set; }
        protected virtual void ConfigureSpaceQty(Rule<int> rule)
        {
            rule.Define(1);
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !IsStandalone.Value);
            });
            rule.Max(10);
        }

        public Rule<string> Post1Orientation { get; set; }
        protected virtual void ConfigurePost1Orientation(Rule<string> rule)
        {
            rule.Define("std");
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !IsStandalone.Value);
                ui.ChoiceList.Define(OrientationChoices);
            });
        }

        public Rule<float> Space1 { get; set; }
        protected virtual void ConfigureSpace1(Rule<float> rule)
        {
            rule.Define(12);
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !IsStandalone.Value);
            });
        }

        public Rule<string> Post2Orientation { get; set; }
        protected virtual void ConfigurePost2Orientation(Rule<string> rule)
        {
            rule.Define("rev");
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !IsStandalone.Value);
                ui.ChoiceList.Define(OrientationChoices);
            });
        }

        public Rule<float> Space2 { get; set; }
        protected virtual void ConfigureSpace2(Rule<float> rule)
        {
            rule.Define(12);
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !IsStandalone.Value || SpaceQty.Value < 2);
            });
        }
        public Rule<string> Post3Orientation { get; set; }
        protected virtual void ConfigurePost3Orientation(Rule<string> rule)
        {
            rule.Define(() => (SpaceQty.Value == 2) ? "rev" : "std");
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !IsStandalone.Value || SpaceQty.Value < 2);
                ui.ChoiceList.Define(OrientationChoices);
            });
        }

        public Rule<float> Space3 { get; set; }
        protected virtual void ConfigureSpace3(Rule<float> rule)
        {
            rule.Define(12);
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !IsStandalone.Value || SpaceQty.Value < 3);
            });
        }

        public Rule<string> Post4Orientation { get; set; }
        protected virtual void ConfigurePost4Orientation(Rule<string> rule)
        {
            rule.Define(() => (SpaceQty.Value == 3) ? "rev" : "std");
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !IsStandalone.Value || SpaceQty.Value < 3);
                ui.ChoiceList.Define(OrientationChoices);
            });
        }

        public Rule<float> Space4 { get; set; }
        protected virtual void ConfigureSpace4(Rule<float> rule)
        {
            rule.Define(12);
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !IsStandalone.Value || SpaceQty.Value < 4);
            });
        }

        public Rule<string> Post5Orientation { get; set; }
        protected virtual void ConfigurePost5Orientation(Rule<string> rule)
        {
            rule.Define(() => (SpaceQty.Value == 4) ? "rev" : "std");
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !IsStandalone.Value || SpaceQty.Value < 4);
                ui.ChoiceList.Define(OrientationChoices);
            });
        }

        public Rule<float> Space5 { get; set; }
        protected virtual void ConfigureSpace5(Rule<float> rule)
        {
            rule.Define(12);
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !IsStandalone.Value || SpaceQty.Value < 5);
            });
        }

        public Rule<string> Post6Orientation { get; set; }
        protected virtual void ConfigurePost6Orientation(Rule<string> rule)
        {
            rule.Define(() => (SpaceQty.Value == 5) ? "rev" : "std");
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !IsStandalone.Value || SpaceQty.Value < 5);
                ui.ChoiceList.Define(OrientationChoices);
            });
        }

        public Rule<float> Space6 { get; set; }
        protected virtual void ConfigureSpace6(Rule<float> rule)
        {
            rule.Define(12);
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !IsStandalone.Value || SpaceQty.Value < 6);
            });
        }

        public Rule<string> Post7Orientation { get; set; }
        protected virtual void ConfigurePost7Orientation(Rule<string> rule)
        {
            rule.Define(() => (SpaceQty.Value == 6) ? "rev" : "std");
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !IsStandalone.Value || SpaceQty.Value < 6);
                ui.ChoiceList.Define(OrientationChoices);
            });
        }

        public Rule<float> Space7 { get; set; }
        protected virtual void ConfigureSpace7(Rule<float> rule)
        {
            rule.Define(12);
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !IsStandalone.Value || SpaceQty.Value < 7);
            });
        }

        public Rule<string> Post8Orientation { get; set; }
        protected virtual void ConfigurePost8Orientation(Rule<string> rule)
        {
            rule.Define(() => (SpaceQty.Value == 7) ? "rev" : "std");
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !IsStandalone.Value || SpaceQty.Value < 7);
                ui.ChoiceList.Define(OrientationChoices);
            });
        }

        public Rule<float> Space8 { get; set; }
        protected virtual void ConfigureSpace8(Rule<float> rule)
        {
            rule.Define(12);
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !IsStandalone.Value || SpaceQty.Value < 8);
            });
        }

        public Rule<string> Post9Orientation { get; set; }
        protected virtual void ConfigurePost9Orientation(Rule<string> rule)
        {
            rule.Define(() => (SpaceQty.Value == 8) ? "rev" : "std");
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !IsStandalone.Value || SpaceQty.Value < 8);
                ui.ChoiceList.Define(OrientationChoices);
            });
        }

        public Rule<float> Space9 { get; set; }
        protected virtual void ConfigureSpace9(Rule<float> rule)
        {
            rule.Define(12);
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !IsStandalone.Value || SpaceQty.Value < 9);
            });
        }

        public Rule<string> Post10Orientation { get; set; }
        protected virtual void ConfigurePost10Orientation(Rule<string> rule)
        {
            rule.Define(() => (SpaceQty.Value == 9) ? "rev" : "std");
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !IsStandalone.Value || SpaceQty.Value < 9);
                ui.ChoiceList.Define(OrientationChoices);
            });
        }

        public Rule<float> Space10 { get; set; }
        protected virtual void ConfigureSpace10(Rule<float> rule)
        {
            rule.Define(12);
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !IsStandalone.Value || SpaceQty.Value < 10);
            });
        }

        public Rule<string> Post11Orientation { get; set; }
        protected virtual void ConfigurePost11Orientation(Rule<string> rule)
        {
            rule.Define(() => (SpaceQty.Value == 10) ? "rev" : "std");
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !IsStandalone.Value || SpaceQty.Value < 10);
                ui.ChoiceList.Define(OrientationChoices);
            });
        }

        #endregion

        public Rule<IEnumerable<ChoiceListItem>> OrientationChoices { get; set; }
        protected virtual void ConfigureOrientationChoices(Rule<IEnumerable<ChoiceListItem>> rule)
        {
            rule.Define(new List<ChoiceListItem>()
                {
                    new ChoiceListItem("std", "Standard(toes toward Rear)"),
                    new ChoiceListItem("rev", "Reverse(toes toward Front)")
                });
        }        

        //Option to override this from a parent. Inputs shall be used for standalone part
        public Rule<List<(string, float)>> SpaceInfo { get; set; }
        protected virtual void ConfigureSpaceInfo(Rule<List<(string, float)>> rule)
        {
            rule.Define(() =>
            {
                var retList = new List<(string, float)>();
                retList.Add((Post1Orientation.Value, 0));
                retList.Add((Post2Orientation.Value, Space1.Value));

                if (SpaceQty.Value > 1) retList.Add((Post3Orientation.Value, Space2.Value));
                if (SpaceQty.Value > 2) retList.Add((Post4Orientation.Value, Space3.Value));
                if (SpaceQty.Value > 3) retList.Add((Post5Orientation.Value, Space4.Value));
                if (SpaceQty.Value > 4) retList.Add((Post6Orientation.Value, Space5.Value));
                if (SpaceQty.Value > 5) retList.Add((Post7Orientation.Value, Space6.Value));
                if (SpaceQty.Value > 6) retList.Add((Post8Orientation.Value, Space7.Value));
                if (SpaceQty.Value > 7) retList.Add((Post9Orientation.Value, Space8.Value));
                if (SpaceQty.Value > 8) retList.Add((Post10Orientation.Value, Space9.Value));
                if (SpaceQty.Value > 9) retList.Add((Post11Orientation.Value, Space10.Value));

                return retList;
            });
        }

        public Rule<List<(string, float)>> BracketInfo { get; set; }
        protected virtual void ConfigureBracketInfo(Rule<List<(string, float)>> rule)
        {
            rule.Define(() => SpaceInfo.Value?.Select((s, index) => GetBracketSpaceWithOffsets(index, s.Item1, s.Item2))?.ToList() 
                                                                    ?? new List<(string, float)>());
        }

        //return format = (orientation of bracket, xOffset from Previous bracket)
        public (string, float) GetBracketSpaceWithOffsets(int index, string orientation, float space)
        {
            var curBracketOffset = orientation == "std" ? -PostToBracketGap : PostToBracketGap;

            if (index == 0)
            {
                if (Type.Value == "eagRear")
                {
                    return orientation == "std" ? (orientation, -PostToBracketGap)
                                                : (GetOppositeOrientation(orientation), PostToBracketGap + (BracketThickness.Value * 2));
                }
                return (orientation, curBracketOffset);
            }

            var prevOffset = SpaceInfo.Value[index - 1].Item1 == "std" ? PostToBracketGap :
                            (Type.Value != "eagRear" || index != 1 ? -PostToBracketGap : -(PostToBracketGap + (BracketThickness.Value * 2)));

            if (Type.Value == "eagFront" && orientation == "std" && index + 1 == SpaceInfo.Value.Count())
            {
                return (GetOppositeOrientation(orientation), space + prevOffset - (PostToBracketGap + (BracketThickness.Value*2)));
            }

            return (orientation, space + prevOffset + curBracketOffset);
        }

        public string GetOppositeOrientation(string orientation)
        {
            return orientation == "std" ? "rev" : "std";
        }

        public float GetPostToBracketOffset(string orientation)
        {
            return orientation == "std" ? PostToBracketGap : -PostToBracketGap;
        }

        public Rule<float> BeamXOffset { get; set; }
        protected virtual void ConfigureBeamXOffset(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (Type.Value == "eag")
                {
                    return -((OverallBracketToBracketDistance.Value / 2) + BracketInfo.Value.First().Item2 + (BeamLength.Value/2));
                }
                else if (Type.Value == "eagFront")
                {
                    return -(BeamLength.Value + BracketInfo.Value.First().Item2 + BeamEndCapOffset);
                }

                return -(BeamLength.Value + BracketInfo.Value.First().Item2 - (BracketThickness.Value - MultipieceBeamGap));
            });
        }

        public Rule<bool> HasLeftCap { get; set; }
        protected virtual void ConfigureHasLeftCap(Rule<bool> rule)
        {
            rule.Define(() => Type.Value != "eagFront");
        }

        public Rule<bool> HasRightCap { get; set; }
        protected virtual void ConfigureHasRightCap(Rule<bool> rule)
        {
            rule.Define(() => Type.Value != "eagRear");
        }

        protected virtual void ConfigureColor(Rule<string> rule)
        {
            rule.Define("OT.PNT.I.BYEL");
        }

        #endregion

        #region Labor Rules

        public Rule<double> GuardClipQty { get; set; }
        protected virtual void ConfigureGuardClipQty(Rule<double> rule)
        {
            rule.Define(() => GuardBrackets.Qty.Value);
        }

        public Rule<double> CapQty { get; set; }
        protected virtual void ConfigureCapQty(Rule<double> rule)
        {
            rule.Define(() => 0.0 + (HasLeftCap.Value ? 1.0 : 0) + (HasRightCap.Value ? 1.0 : 0));
        }

        public Rule<double> WeldHrsPerClip { get; set; }
        protected virtual void ConfigureWeldHrsPerClip(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> WeldHrsPerCap { get; set; }
        protected virtual void ConfigureWeldHrsPerCap(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> PaintCarriersPerHour { get; set; }
        protected virtual void ConfigurePaintCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> UnloadCarriersPerHour { get; set; }
        protected virtual void ConfigureUnloadCarriersPerHour(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        public Rule<double> PartsPerCarrier { get; set; }
        protected virtual void ConfigurePartsPerCarrier(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        #endregion

        #region IChannel Implementation

        public Rule<List<ChannelData>> AllChannelData { get; set; }
        public Rule<List<ChoiceListItem>> AllChannelSizesChoiceList { get; set; }
        public Rule<ChannelData> ActiveChannelData { get; set; }
        public Rule<string> ChannelSize { get; set; }
        protected virtual void ConfigureChannelSize(Rule<string> rule)
        {
            rule.Define(EndAisleGuardChannelSize);
        }
        public Rule<float> ChannelWidth { get; set; }
        public Rule<float> ChannelDepth { get; set; }
        public Rule<float> ChannelWebThickness { get; set; }
        public Rule<List<string>> ChannelSizesAvailable { get; set; }
        protected virtual void ConfigureChannelSizesAvailable(Rule<List<string>> rule)
        {
            rule.Define(() => (new List<string> { "C4x4.5", "C6x8.2" }));
        }

        public Rule<float> ChannelWeightPerFt { get; set; }

        public Rule<string> MaterialPartNumber { get; set; }

        #endregion

        #region IBomPart Implementation

        protected virtual void ConfigurePartNumberPrefix(Rule<string> rule)
        {
            rule.Define(() =>
            {
                var suffix = Type.Value == "eagFront" ? "F-" : (Type.Value == "eagRear" ? "R-" : "");
                return $"EAG-{suffix}";
            });
        }

        protected virtual void ConfigureDescription(Rule<string> rule)
        {
            rule.Define(() =>
            {
                var prefix = PartNumberPrefix.Value.TrimEnd('-');
                var suffix = " ";
                SpaceInfo.Value?.ForEach(s => suffix = suffix + GetSpaceDescription(s.Item1, s.Item2));
                return  $"{prefix}{suffix}";
            });
            rule.AddToCAD(cad =>
            {
                cad.CadRuleUsageType = CadRuleUsageType.Property;
            });
        }

        protected virtual void ConfigurePartWeight(Rule<double> rule)
        {
            rule.Define(() => this.GetPartWeightWithoutPaint());
        }

        #endregion

        #region CPQ.IBomPart Implementation

        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {
            child.PartNumber.Define(PartNumber);
            child.ItemQty.Define(Qty);
            child.Description.Define(Description);
        }

        #endregion

        #region IRevisablePartImplementation

        public FactoryRevisionRule<string> FactoryRevision { get; set; }

        #endregion

        #region Public Methods

        public string GetSpaceDescription(string orientation, float space)
        {
            var bracket = orientation == "std" ? "[" : "]";
            var spaceText = space > 0 ? space.ToString() : "";
            return $"{spaceText}{bracket}";
        }

        public Vector3 GetBracketPositions(int index)
        {
            var xOffset = index > 0 ? GuardBrackets[index - 1].Position.Value.X : 0;
            xOffset = xOffset - BracketInfo.Value[index].Item2;
            var yOffset = (GuardBrackets[0].Height.Value / 2) - (EndAisleGuardBeam.ChannelWidth.Value / 2);
            if (BracketInfo.Value[index].Item1 == "std")
            {
                yOffset = -(GuardBrackets[0].Height.Value / 2) - (EndAisleGuardBeam.ChannelWidth.Value / 2);
            }
            return Helper.Vxy(xOffset, yOffset);
        }

        public Quaternion GetBracketRotation(string orientation)
        {
            return orientation == "std" ? Helper.CreateRotation(Vector3.UnitZ, 180) : Quaternion.Identity;
        }

        #endregion

        #region Children

        public EndAisleGuardBeam EndAisleGuardBeam { get; set; }
        protected virtual void ConfigureEndAisleGuardBeam(EndAisleGuardBeam child)
        {
            child.ChannelSize.Define(ChannelSize);
            child.Length.Define(BeamLength);
            child.Position.Define(() => Helper.Vxyz(BeamXOffset.Value,-child.ChannelWidth.Value / 2, child.ChannelDepth.Value + GuardBrackets[0].Thickness.Value));
            child.Rotation.Define(() => Helper.CreateRotation(Vector3.UnitY, 90));
        }

        public ChildList<ShelfBeamEndBracket> GuardBrackets { get; set; }
        protected virtual void ConfigureGuardBrackets(ChildList<ShelfBeamEndBracket> list)
        {
            list.Qty.Define(() => BracketInfo.Value?.Count() ?? 0);
            list.ConfigureChildren((child, index) =>
            {
                child.Type.Define("12");
                child.Position.Define(() => GetBracketPositions(index));
                child.Rotation.Define(() => GetBracketRotation(BracketInfo.Value[index].Item1));
            });
        }

        public EndAisleGuardCap RightCap { get; set; }
        protected virtual void ConfigureRightCap(EndAisleGuardCap child)
        {
            child.IsActive.Define(HasRightCap);
            child.ChannelWidth.Define(() => EndAisleGuardBeam.ChannelWidth.Value);
            child.Position.Define(() => EndAisleGuardBeam.Position.Value + Helper.Vxz(EndAisleGuardBeam.Length.Value, -0.081f));
            child.Rotation.Define(() => Helper.CreateRotation(Vector3.UnitX, 180));
        }

        public EndAisleGuardCap LeftCap { get; set; }
        protected virtual void ConfigureLeftCap(EndAisleGuardCap child)
        {
            child.IsActive.Define(HasLeftCap);
            child.ChannelWidth.Define(() => EndAisleGuardBeam.ChannelWidth.Value);
            child.Position.Define(() => EndAisleGuardBeam.Position.Value + Helper.Vz(-0.081f));
            child.Rotation.Define(() => Helper.CreateRotation(Vector3.UnitY, 180));
        }

        #endregion

        #region Protected Methods

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureBomPartRules();
            this.ConfigureRevisablePart();
            this.ConfigureChannelRules();
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(Colors.Yellow);
        }

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define("EndAisleGuard");
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(IsStandalone);
        }

        #endregion

        #region Graphics

        //public Block FrameUprightBlock { get; set; }
        //protected virtual void ConfigureFrameUprightBlock(Block geo)
        //{
        //    geo.Width.Define(Depth);
        //    geo.Height.Define(Height);
        //    geo.Depth.Define(() => RearUprightColumn.ChannelWidth.Value);
        //    geo.Position.Define(() => Helper.Vy(Height.Value / 2));
        //}

        public UCS UCS { get; set; }
        protected virtual void ConfigureUCS(UCS child)
        {
            child.IsActive.Define(IsStandalone);
            child.Diameter.Define(0.125f);
            child.Position.Define(() => (Helper.Vy(0)));
        }

        #endregion

    }
}
