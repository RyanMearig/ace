﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ruler.Cpq;
using Ruler.Rules;
using Ruler.Rules.Graphics;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class Washers : AdvancePart, IBomPart, IRevisablePart, Ruler.Cpq.IBomPart
    {

        #region Constructors

        public Washers(ModelContext modelContext) : base(modelContext)
        {
        }

        public Washers(string name, Part parent) : base(name, parent)
        {
        }

        #endregion

        #region Rules

        public Rule<bool> IsStandalone { get; set; }
        protected virtual void ConfigureIsStandalone(Rule<bool> rule)
        {
            rule.Define(() => (Parent == null || Parent?.GetType() == typeof(ShippableParts)));
        }

        public Rule<bool> RenderModel { get; set; }
        protected virtual void ConfigureRenderModel(Rule<bool> rule)
        {
            rule.Define(false);
        }

        public LookupRule<StorageSystem> StorageSystemComp { get; set; }

        [UIRule]
        public Rule<float> BoltDiameter { get; set; }
        protected virtual void ConfigureBoltDiameter(Rule<float> rule)
        {
            rule.Define(0.5f);
        }

        public Rule<int> Qty { get; set; }
        protected virtual void ConfigureQty(Rule<int> rule)
        {
            rule.Define(1);
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !IsStandalone.Value);
            });
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(RenderModel);
                cad.CadRuleUsageType = CadRuleUsageType.Property;
            });
        }

        public Rule<int> BOMQty { get; set; }
        protected virtual void ConfigureBOMQty(Rule<int> rule)
        {
            rule.Define(() =>
            {
                if (StorageSystemComp.Value != null)
                {
                    return (int)Math.Ceiling(Qty.Value + (Qty.Value * StorageSystemComp.Value.HardwareAdder.Value));
                }
                return Qty.Value;
            });
        }

        public Rule<string> WasherType { get; set; }
        protected virtual void ConfigureWasherType(Rule<string> rule)
        {
            rule.Define(() =>
            {
                return WasherTypeInput.UIMetadata.ChoiceList.Value?.First(c => c.Value.ToString() == WasherTypeInput.Value)?.Text.ToString() ?? WasherTypes.Clipped;
            });
        }

        public Rule<string> WasherTypeInput { get; set; }
        protected virtual void ConfigureWasherTypeInput(Rule<string> rule)
        {
            rule.Define(WasherTypes.Clipped);
            rule.AddToUI(ui =>
            {
                ui.Label.Define("Washer Type");
                ui.ChoiceList.Define(() =>
                {
                    var props = typeof(WasherTypes).GetFields();
                    return props?.Select(p => new ChoiceListItem(p.Name, p.GetValue(null).ToString())) ?? new List<ChoiceListItem>();
                });
                ui.ReadOnly.Define(() => !IsStandalone.Value);
            });
        }

        public Rule<string> WasherMaterial { get; set; }
        protected virtual void ConfigureWasherMaterial(Rule<string> rule)
        {
            rule.Define(() =>
            {
                return WasherMaterialInput.UIMetadata.ChoiceList.Value?.First(c => c.Value.ToString() == WasherMaterialInput.Value)?.Text.ToString() ?? HardwareMaterial.ZincPlated;
            });
        }

        public Rule<string> WasherMaterialInput { get; set; }
        protected virtual void ConfigureWasherMaterialInput(Rule<string> rule)
        {
            rule.Define("ZincPlated");
            rule.AddToUI(ui =>
            {
                ui.Label.Define("Washer Material");
                ui.ChoiceList.Define(() =>
                {
                    var props = typeof(HardwareMaterial).GetFields();
                    return props?.Select(p => new ChoiceListItem(p.Name, p.GetValue(null).ToString())) ?? new List<ChoiceListItem>();
                });
                ui.ReadOnly.Define(() => !IsStandalone.Value);
            });
        }

        public Rule<string> WasherDescription { get; set; }
        protected virtual void ConfigureWasherDescription(Rule<string> rule)
        {
            rule.Define(() => $"{WasherType.Value} Washers {WasherMaterial.Value}");
        }

        #endregion

        #region IBomPart Implementation

        public Rule<string> DatabasePartNumber { get; set; }
        public PartNumberRule<string> PartNumber { get; set; }
        public Rule<string> PartNumberPrefix { get; set; }
        public Rule<string> Description { get; set; }
        public Rule<string> BOMDescription { get; set; }
        protected virtual void ConfigureBOMDescription(Rule<string> rule)
        {
            rule.Define(() =>
            {
                return $"{Helper.fractionString(BoltDiameter.Value, feetToo: false, suppressUnits: false)} {WasherDescription.Value} ";
            });
        }
        public Rule<string> BOMDescription1 { get; set; }
        public Rule<string> BOMDescription2 { get; set; }
        public Rule<string> BOMDescription3 { get; set; }
        public Rule<string> BOMDescription4 { get; set; }
        public Rule<string> BOMDescription5 { get; set; }
        public Rule<string> BOMDescription6 { get; set; }
        public Rule<string> BOMDescription7 { get; set; }
        public Rule<string> BOMDescription8 { get; set; }
        public Rule<string> BOMDescription9 { get; set; }
        public Rule<string> BOMDescription10 { get; set; }
        public Rule<string> BOMDescription11 { get; set; }
        public Rule<string> Color { get; set; }
        protected virtual void ConfigureColor(Rule<string> rule)
        {
            rule.Define("");
        }
        public ReadOnlyRule<AdvanceColor> AdvanceColor { get; set; }
        //protected virtual void ConfigureAdvanceColor(ReadOnlyRule<AdvanceColor> rule)
        //{
        //    rule.Define(() => null);
        //}
        public ReadOnlyRule<string> ColorDescription { get; set; }
        protected virtual void ConfigureColorDescription(ReadOnlyRule<string> rule)
        {
            rule.Define("");
        }
        public StorageConfiguratorRepository Repository { get; set; }
        public Rule<string> ColorBOMName { get; set; }
        public ReadOnlyRule<string> ColorShortDescription { get; set; }

        public Rule<double> PartWeight { get; set; }
        public Rule<double> PartPaintWeight { get; set; }
        public Rule<float> WeightPerGallon { get; set; }
        public CsiRule<double> PaintCostPerGallon { get; set; }
        public Rule<float> PaintCost { get; set; }

        #endregion

        #region IRevisablePartImplementation

        public FactoryRevisionRule<string> FactoryRevision { get; set; }

        #endregion

        #region CPQ.IBomPart Implementation

        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {
            child.IsActive.Define(() => RenderModel.Value || IsStandalone.Value);
            child.ItemQty.Define(() => (double)BOMQty.Value);
            child.PartNumber.Define(PartNumber);
            child.Description.Define(BOMDescription1);
            child.IsPurchased.Define(true);
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(Colors.Gray);
        }

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define(() => RenderModel.Value ? "Washers" : "");
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(() => IsStandalone.Value);
        }

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureBomPartRules();
            this.ConfigureRevisablePart();
        }

        #endregion
    }
}
