﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StorageConfigurator.Rules
{
    public static class BoltTypes
    {
        public const string Hex = "Hex Bolt";
        public const string Carriage = "Carriage Bolt";
        public const string TapHex = "Tap Hex Bolt";
        public const string ButtonHead = "Button Head Socket";
        public const string CapScrew = "Cap Screw";
        public const string HexTekScrew = "Hex Tek Screw";
    }
}
