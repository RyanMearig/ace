﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StorageConfigurator.Rules
{
    public static class NutTypes
    {
        public const string Hex = "Hex";
        public const string Flange = "Flange";
        public const string HeavyHex = "Heavy Hex";
        public const string NylonInsert = "Nylon Insert";
    }
}
