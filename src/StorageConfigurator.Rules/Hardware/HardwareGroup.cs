﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;
using Ruler.Rules.Graphics;

namespace StorageConfigurator.Rules
{
    public class HardwareGroup : Part
    {
        #region Constructors

        public HardwareGroup(ModelContext modelContext) : base(modelContext) { }

        public HardwareGroup(string name, Part parent) : base(name, parent) { }

        #endregion

        #region Rules

        public Rule<float> BoltSize { get; set; }
        protected virtual void ConfigureBoltSize(Rule<float> rule)
        {
            rule.Define(default(float));
        }

        public Rule<float> BoltLength { get; set; }
        protected virtual void ConfigureBoltLength(Rule<float> rule)
        {
            rule.Define(default(float));
        }

        public Rule<int> Qty { get; set; }
        protected virtual void ConfigureQty(Rule<int> rule)
        {
            rule.Define(default(int));
        }

        public Rule<string> BoltType { get; set; }
        protected virtual void ConfigureBoltType(Rule<string> rule)
        {
            rule.Define(BoltTypes.Hex);
        }

        public Rule<string> BoltMaterial { get; set; }
        protected virtual void ConfigureBoltMaterial(Rule<string> rule)
        {
            rule.Define(HardwareMaterial.Grade5);
        }

        public Rule<string> NutType { get; set; }
        protected virtual void ConfigureNutType(Rule<string> rule)
        {
            rule.Define(NutTypes.Hex);
        }

        public Rule<string> NutMaterial { get; set; }
        protected virtual void ConfigureNutMaterial(Rule<string> rule)
        {
            rule.Define(BoltMaterial);
        }

        public Rule<bool> HasWashers { get; set; }
        protected virtual void ConfigureHasWashers(Rule<bool> rule)
        {
            rule.Define(default(bool));
        }

        public Rule<int> WasherQtyPerBolt { get; set; }
        protected virtual void ConfigureWasherQtyPerBolt(Rule<int> rule)
        {
            rule.Define(1);
        }

        public ReadOnlyRule<int> WasherQty { get; set; }
        protected virtual void ConfigureWasherQty(ReadOnlyRule<int> rule)
        {
            rule.Define(() => HasWashers.Value ? Qty.Value * WasherQtyPerBolt.Value : 0);
        }

        public Rule<string> WasherType { get; set; }
        protected virtual void ConfigureWasherType(Rule<string> rule)
        {
            rule.Define(WasherTypes.Clipped);
        }

        public Rule<string> WasherMaterial { get; set; }
        protected virtual void ConfigureWasherMaterial(Rule<string> rule)
        {
            rule.Define(HardwareMaterial.ZincPlated);
        }

        #endregion

        #region IRevisablePartImplementation

        public FactoryRevisionRule<string> FactoryRevision { get; set; }

        #endregion

        #region Children

        public HardwareKit HardwareKit { get; set; }
        protected virtual void ConfigureHardwareKit(HardwareKit child)
        {
            child.IsActive.Define(() => Qty.Value > 0);
            child.Length.Define(BoltLength);
            child.BoltSize.Define(BoltSize);
            child.BoltType.Define(BoltType);
            child.BoltMaterial.Define(BoltMaterial);
            child.NutType.Define(NutType);
            child.NutMaterial.Define(NutMaterial);
            child.Qty.Define(Qty);
        }

        public Washers Washers { get; set; }
        protected virtual void ConfigureWashers(Washers child)
        {
            child.IsActive.Define(() => HasWashers.Value && WasherQty.Value > 0);
            child.BoltDiameter.Define(BoltSize);
            child.WasherType.Define(WasherType);
            child.WasherMaterial.Define(WasherMaterial);
            child.Qty.Define(WasherQty);
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(Colors.Red);
        }

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define(string.Empty);
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(false);
        }

        #endregion

    }
}
