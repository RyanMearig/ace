﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StorageConfigurator.Rules
{
    public class WasherDefinition
    {
        public float BoltSize { get; set; }
        public string WasherType { get; set; } = WasherTypes.Clipped;
        public string WasherMaterial { get; set; } = HardwareMaterial.ZincPlated;
        public int Qty { get; set; } = 0;
    }
}
