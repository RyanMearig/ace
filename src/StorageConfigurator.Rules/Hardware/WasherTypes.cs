﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StorageConfigurator.Rules
{
    public static class WasherTypes
    {
        public const string Flat = "Flat";
        public const string Fender = "Fender";
        public const string SplitLock = "Split Lock";
        public const string Clipped = "Clipped";
        public const string StructuralFlat = "Structural Flat";
        public const string NarrowFlat = "Narrow Flat";
    }
}
