﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ruler.Cpq;
using Ruler.Rules;
using Ruler.Rules.Graphics;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class Nut : AdvancePart, IBomPart, IRevisablePart, Ruler.Cpq.IBomPart
    {
        #region Constructors

        public Nut(ModelContext modelContext) : base(modelContext) { }

        public Nut(string name, Part parent) : base(name, parent) { }

        #endregion

        #region Rules

        public Rule<bool> IsStandalone { get; set; }
        protected virtual void ConfigureIsStandalone(Rule<bool> rule)
        {
            rule.Define(() => (Parent == null || Parent?.GetType() == typeof(ShippableParts)));
        }

        [UIRule]
        public Rule<float> BoltDiameter { get; set; }
        protected virtual void ConfigureBoltDiameter(Rule<float> rule)
        {
            rule.Define(0.5f);
        }

        public Rule<int> Qty { get; set; }
        protected virtual void ConfigureQty(Rule<int> rule)
        {
            rule.Define(1);
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !IsStandalone.Value);
            });
        }

        public Rule<string> NutType { get; set; }
        protected virtual void ConfigureNutType(Rule<string> rule)
        {
            rule.Define(() =>
            {
                return NutTypeInput.UIMetadata.ChoiceList.Value?.First(c => c.Value.ToString() == NutTypeInput.Value)?.Text.ToString() ?? NutTypes.Hex;
            });
        }

        public Rule<string> NutTypeInput { get; set; }
        protected virtual void ConfigureNutTypeInput(Rule<string> rule)
        {
            rule.Define("Hex");
            rule.AddToUI(ui =>
            {
                ui.Label.Define("Nut Type");
                ui.ChoiceList.Define(() =>
                {
                    var props = typeof(NutTypes).GetFields();
                    return props?.Select(p => new ChoiceListItem(p.Name, p.GetValue(null).ToString())) ?? new List<ChoiceListItem>();
                });
                ui.ReadOnly.Define(() => !IsStandalone.Value);
            });
        }

        public Rule<string> NutMaterial { get; set; }
        protected virtual void ConfigureNutMaterial(Rule<string> rule)
        {
            rule.Define(() =>
            {
                return NutMaterialInput.UIMetadata.ChoiceList.Value?.First(c => c.Value.ToString() == NutMaterialInput.Value)?.Text.ToString() ?? HardwareMaterial.Grade5;
            });
        }

        public Rule<string> NutMaterialInput { get; set; }
        protected virtual void ConfigureNutMaterialInput(Rule<string> rule)
        {
            rule.Define("Grade5");
            rule.AddToUI(ui =>
            {
                ui.Label.Define("Nut Material");
                ui.ChoiceList.Define(() =>
                {
                    var props = typeof(HardwareMaterial).GetFields();
                    return props?.Select(p => new ChoiceListItem(p.Name, p.GetValue(null).ToString())) ?? new List<ChoiceListItem>();
                });
                ui.ReadOnly.Define(() => !IsStandalone.Value);
            });
        }

        #endregion

        #region IBomPart Implementation

        public Rule<string> DatabasePartNumber { get; set; }
        public PartNumberRule<string> PartNumber { get; set; }
        public Rule<string> PartNumberPrefix { get; set; }

        public Rule<string> Description { get; set; }
        public Rule<string> BOMDescription { get; set; }
        protected virtual void ConfigureBOMDescription(Rule<string> rule)
        {
            rule.Define(() =>
            {
                return $"{Helper.fractionString(BoltDiameter.Value, feetToo: false, suppressUnits: false)} {NutType.Value} Nut {NutMaterial.Value}";
            });
        }
        public Rule<string> BOMDescription1 { get; set; }
        public Rule<string> BOMDescription2 { get; set; }
        public Rule<string> BOMDescription3 { get; set; }
        public Rule<string> BOMDescription4 { get; set; }
        public Rule<string> BOMDescription5 { get; set; }
        public Rule<string> BOMDescription6 { get; set; }
        public Rule<string> BOMDescription7 { get; set; }
        public Rule<string> BOMDescription8 { get; set; }
        public Rule<string> BOMDescription9 { get; set; }
        public Rule<string> BOMDescription10 { get; set; }
        public Rule<string> BOMDescription11 { get; set; }
        public Rule<string> Color { get; set; }
        protected virtual void ConfigureColor(Rule<string> rule)
        {
            rule.Define("");
        }
        public ReadOnlyRule<AdvanceColor> AdvanceColor { get; set; }

        public ReadOnlyRule<string> ColorDescription { get; set; }
        protected virtual void ConfigureColorDescription(ReadOnlyRule<string> rule)
        {
            rule.Define("");
        }
        public StorageConfiguratorRepository Repository { get; set; }
        public Rule<string> ColorBOMName { get; set; }
        public ReadOnlyRule<string> ColorShortDescription { get; set; }

        public Rule<double> PartWeight { get; set; }
        public Rule<double> PartPaintWeight { get; set; }
        public Rule<float> WeightPerGallon { get; set; }
        public CsiRule<double> PaintCostPerGallon { get; set; }
        public Rule<float> PaintCost { get; set; }

        #endregion

        #region IRevisablePartImplementation

        public FactoryRevisionRule<string> FactoryRevision { get; set; }

        #endregion

        #region CPQ.IBomPart Implementation

        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {
            child.ItemQty.Define(() => (double)Qty.Value);
            child.PartNumber.Define(PartNumber);
            child.Description.Define(BOMDescription1);
            child.IsPurchased.Define(true);
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(Colors.Gray);
        }

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define("Nut");
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(IsStandalone);
        }

        #endregion

        #region Overrides

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureBomPartRules();
            this.ConfigureRevisablePart();
        }

        #endregion

    }
}
