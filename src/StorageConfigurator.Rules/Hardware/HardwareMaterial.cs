﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StorageConfigurator.Rules
{
    public static class HardwareMaterial
    {
        public const string Grade2 = "Gr. 2";
        public const string Grade5 = "Gr. 5";
        public const string Grade8 = "Gr. 8";
        public const string A325 = "A325";
        public const string A563 = "A563";
        public const string F436 = "F436";
        public const string Nylon = "Nylon";
        public const string ZincPlated = "Zinc Plated";
        public const string ChromePlated = "Chrome Plated";
    }
}
