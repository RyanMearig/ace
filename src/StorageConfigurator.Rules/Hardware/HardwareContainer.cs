﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ruler.Rules;
using Ruler.Rules.Graphics;

namespace StorageConfigurator.Rules
{
    public class HardwareContainer : Part, IRevisablePart
    {
        #region Constructors

        public HardwareContainer(ModelContext modelContext) : base(modelContext) { }

        public HardwareContainer(string name, Part parent) : base(name, parent) { }

        #endregion

        #region Rules

        public LookupRule<string> LineItemName { get; set; }
        protected virtual void ConfigureLineItemName(LookupRule<string> rule)
        {
            rule.Define(default(string));
        }

        public ReadOnlyRule<List<BoltDefinition>> HardwareDefinitions { get; set; }
        protected virtual void ConfigureHardwareDefinitions(ReadOnlyRule<List<BoltDefinition>> rule)
        {
            rule.Define(() => new List<BoltDefinition>());
        }
        
        public ReadOnlyRule<List<WasherDefinition>> WasherDefinitions { get; set; }
        protected virtual void ConfigureWasherDefinitions(ReadOnlyRule<List<WasherDefinition>> rule)
        {
            rule.Define(() => new List<WasherDefinition>());
        }

        public ReadOnlyRule<List<AnchorDefinition>> AnchorDefinitions { get; set; }
        protected virtual void ConfigureAnchorDefinitions(ReadOnlyRule<List<AnchorDefinition>> rule)
        {
            rule.Define(() => new List<AnchorDefinition>());
        }

        public Rule<int> BullnoseCapPlugQty { get; set; }
        protected virtual void ConfigureBullnoseCapPlugQty(Rule<int> rule)
        {
            rule.Define(0);
        }

        
        public PartNumberRule<string> PartNumber { get; set; }
        protected virtual void ConfigurePartNumber(PartNumberRule<string> rule)
        {
            rule.Define(() => $"{LineItemName.Value}_HardwareContainer");
            rule.AddToCAD(cad =>
            {
                cad.CadRuleUsageType = CadRuleUsageType.Property;
                cad.NameInCad = "Part Number";
            });
        }

        #endregion

        #region IRevisablePartImplementation

        public FactoryRevisionRule<string> FactoryRevision { get; set; }

        #endregion

        #region Children

        public ChildList<AnchorBolt> StorageSystemAnchors { get; set; }
        protected virtual void ConfigureStorageSystemAnchors(ChildList<AnchorBolt> list)
        {
            list.Qty.Define(() => AnchorDefinitions.Value.Count());
            list.ConfigureChildren((child, index) =>
            {
                child.RenderModel.Define(true);
                child.AnchorBoltDefinition.Define(() => AnchorDefinitions.Value[index]);
            });
        }

        public ChildList<HardwareKit> StorageSystemHardwareKits { get; set; }
        protected virtual void ConfigureStorageSystemHardwareKits(ChildList<HardwareKit> list)
        {
            list.Qty.Define(() => HardwareDefinitions.Value.Count());
            list.ConfigureChildren((child, index) =>
            {
                child.RenderKit.Define(true);
                child.BoltSize.Define(() => HardwareDefinitions.Value[index].BoltSize);
                child.BoltType.Define(() => HardwareDefinitions.Value[index].BoltType);
                child.BoltMaterial.Define(() => HardwareDefinitions.Value[index].BoltMaterial);
                child.Length.Define(() => HardwareDefinitions.Value[index].Length);
                child.NutType.Define(() => HardwareDefinitions.Value[index].NutType);
                child.NutMaterial.Define(() => HardwareDefinitions.Value[index].NutMaterial);
                child.Qty.Define(() => HardwareDefinitions.Value[index].Qty);
            });
        }

        public ChildList<Washers> StorageSystemWashers { get; set; }
        protected virtual void ConfigureStorageSystemWashers(ChildList<Washers> list)
        {
            list.Qty.Define(() => WasherDefinitions.Value.Count());
            list.ConfigureChildren((child, index) =>
            {
                child.RenderModel.Define(true);
                child.BoltDiameter.Define(() => WasherDefinitions.Value[index].BoltSize);
                child.WasherType.Define(() => WasherDefinitions.Value[index].WasherType);
                child.WasherMaterial.Define(() => WasherDefinitions.Value[index].WasherMaterial);
                child.Qty.Define(() => WasherDefinitions.Value[index].Qty);
            });
        }

        public BullnoseCapPlug BullnoseCapPlugs { get; set; }
        protected virtual void ConfigureBullnoseCapPlugs(BullnoseCapPlug child)
        {
            child.IsActive.Define(() => BullnoseCapPlugQty.Value > 0);
            child.RenderModel.Define(true);
            child.Qty.Define(BullnoseCapPlugQty);
        }


        #endregion

        #region Part Overrides

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(false);
        }


        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define("HardwareContainer");
            this.ConfigureRevisablePart();
        }

        #endregion

    }
}
