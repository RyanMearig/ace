﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ruler.Cpq;
using Ruler.Rules;
using Ruler.Rules.Graphics;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class BullnoseCapPlug : AdvancePart, Ruler.Cpq.IBomPart, IBomPart
    {
        #region Constructors

        public BullnoseCapPlug(ModelContext modelContext) : base(modelContext) { }

        public BullnoseCapPlug(string name, Part parent) : base(name, parent) { }

        #endregion

        #region Rules

        public Rule<bool> IsStandalone { get; set; }
        protected virtual void ConfigureIsStandalone(Rule<bool> rule)
        {
            rule.Define(() => (Parent == null || Parent?.GetType() == typeof(ShippableParts)));
        }

        public Rule<bool> RenderModel { get; set; }
        protected virtual void ConfigureRenderModel(Rule<bool> rule)
        {
            rule.Define(false);
        }

        public Rule<int> Qty { get; set; }
        protected virtual void ConfigureQty(Rule<int> rule)
        {
            rule.Define(1);
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !IsStandalone.Value);
            });
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(RenderModel);
                cad.CadRuleUsageType = CadRuleUsageType.Property;
            });
        }

        #endregion

        #region IBomPart Implementation

        public Rule<string> DatabasePartNumber { get; set; }
        public Rule<string> PartNumberPrefix { get; set; }
        public PartNumberRule<string> PartNumber { get; set; }
        protected virtual void ConfigurePartNumber(PartNumberRule<string> rule)
        {
            rule.Define("HW.CAP.PLUG");
        }

        public Rule<string> Description { get; set; }
        protected virtual void ConfigureDescription(Rule<string> rule)
        {
            rule.Define(@"1.906""x.562"" Bullnose Cap Plug");
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(true);
            });
        }

        public Rule<string> BOMDescription { get; set; }
        protected virtual void ConfigureBOMDescription(Rule<string> rule)
        {
            rule.Define(Description);
        }
        public Rule<string> BOMDescription1 { get; set; }
        public Rule<string> BOMDescription2 { get; set; }
        public Rule<string> BOMDescription3 { get; set; }
        public Rule<string> BOMDescription4 { get; set; }
        public Rule<string> BOMDescription5 { get; set; }
        public Rule<string> BOMDescription6 { get; set; }
        public Rule<string> BOMDescription7 { get; set; }
        public Rule<string> BOMDescription8 { get; set; }
        public Rule<string> BOMDescription9 { get; set; }
        public Rule<string> BOMDescription10 { get; set; }
        public Rule<string> BOMDescription11 { get; set; }

        public Rule<string> Color { get; set; }
        public ReadOnlyRule<AdvanceColor> AdvanceColor { get; set; }
        public ReadOnlyRule<string> ColorDescription { get; set; }
        public StorageConfiguratorRepository Repository { get; set; }
        public Rule<string> ColorBOMName { get; set; }
        public ReadOnlyRule<string> ColorShortDescription { get; set; }

        public Rule<double> PartWeight { get; set; }
        public Rule<double> PartPaintWeight { get; set; }
        public Rule<float> WeightPerGallon { get; set; }
        public CsiRule<double> PaintCostPerGallon { get; set; }
        public Rule<float> PaintCost { get; set; }


        #endregion

        #region Ruler CPQ IBomPart Implementation

        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {
            child.IsActive.Define(() => RenderModel.Value || IsStandalone.Value);
            child.ItemQty.Define(() => (double)Qty.Value);
            child.Description.Define(Description);
            child.PartNumber.Define(PartNumber);
            child.IsPurchased.Define(true);
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define(() => RenderModel.Value ? "BullnoseCapPlug" : "");
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(false);
        }

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureBomPartRules();
        }

        #endregion


    }
}
