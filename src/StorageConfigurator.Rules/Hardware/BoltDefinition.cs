﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StorageConfigurator.Rules
{
    public class BoltDefinition
    {
        public float BoltSize { get; set; }
        public float Length { get; set; }
        public string BoltType { get; set; } = BoltTypes.Hex;
        public string BoltMaterial { get; set; } = HardwareMaterial.Grade5;
        public string NutType { get; set; } = NutTypes.Hex;
        public string NutMaterial { get; set; } = HardwareMaterial.Grade5;
        public bool HasWashers { get; set; } = false;
        public string WasherType { get; set; } = WasherTypes.Clipped;
        public string WasherMaterial { get; set; } = HardwareMaterial.Grade2;
        public int Qty { get; set; } = 0;
    }
}
