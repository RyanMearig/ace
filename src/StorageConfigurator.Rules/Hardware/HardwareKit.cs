﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Cpq;
using Ruler.Rules;
using Ruler.Rules.Graphics;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class HardwareKit : AdvancePart, IBomPart, IRevisablePart, Ruler.Cpq.IBomPart
    {

        #region Constructors
        public HardwareKit(ModelContext modelContext) : base(modelContext)
        {
        }

        public HardwareKit(string name, Part parent) : base(name, parent)
        {
        }

        #endregion

        #region Rules

        [CadRule(CadRuleUsageType.Property)]
        public LookupRule<string> LineItemName { get; set; }

        public LookupRule<StorageSystem> StorageSystemComp { get; set; }

        public Rule<bool> RenderKit { get; set; }
        protected virtual void ConfigureRenderKit(Rule<bool> rule)
        {
            rule.Define(false);
        }

        public Rule<float> BoltSize { get; set; }
        protected virtual void ConfigureBoltSize(Rule<float> rule)
        {
            rule.Define(default(float));
        }

        public Rule<float> Length { get; set; }
        protected virtual void ConfigureLength(Rule<float> rule)
        {
            rule.Define(default(float));
        }

        public Rule<int> Qty { get; set; }
        protected virtual void ConfigureQty(Rule<int> rule)
        {
            rule.Define(default(int));
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(RenderKit);
                cad.CadRuleUsageType = CadRuleUsageType.Property;
            });
        }

        public Rule<int> BOMQty { get; set; }
        protected virtual void ConfigureBOMQty(Rule<int> rule)
        {
            rule.Define(() =>
            {
                if (StorageSystemComp.Value != null)
                {
                    return (int)Math.Ceiling(Qty.Value + (Qty.Value * StorageSystemComp.Value.HardwareAdder.Value));
                }
                return Qty.Value;
            });
        }

        public Rule<string> BoltType { get; set; }
        protected virtual void ConfigureBoltType(Rule<string> rule)
        {
            rule.Define(BoltTypes.Hex);
        }

        public Rule<string> BoltMaterial { get; set; }
        protected virtual void ConfigureBoltMaterial(Rule<string> rule)
        {
            rule.Define(HardwareMaterial.Grade5);
        }

        public Rule<string> NutType { get; set; }
        protected virtual void ConfigureNutType(Rule<string> rule)
        {
            rule.Define(NutTypes.Hex);
        }

        public Rule<string> NutMaterial { get; set; }
        protected virtual void ConfigureNutMaterial(Rule<string> rule)
        {
            rule.Define(BoltMaterial);
        }

        public Rule<string> NutDescription { get; set; }
        protected virtual void ConfigureNutDescription(Rule<string> rule)
        {
            rule.Define(() => $"{NutType.Value} Nuts {NutMaterial.Value}");
        }

        #endregion

        #region IBomPart Implementation

        public Rule<string> DatabasePartNumber { get; set; }
        public PartNumberRule<string> PartNumber { get; set; }
        public Rule<string> PartNumberPrefix { get; set; }
        protected virtual void ConfigurePartNumberPrefix(Rule<string> rule)
        {
            rule.Define(() =>
            {
                return $"{LineItemName.Value}_HDWKIT-";
            });
        }
        public Rule<string> Description { get; set; }
        public Rule<string> BOMDescription { get; set; }
        protected virtual void ConfigureBOMDescription(Rule<string> rule)
        {
            rule.Define(() =>
            {
                return $"Hdw Kit {Helper.fractionString(BoltSize.Value, feetToo: false, suppressUnits: false)}x{Helper.fractionString(Length.Value, feetToo: false, suppressUnits: false)} {BoltType.Value} Bolts w/ {NutDescription.Value}";
            });
        }
        public Rule<string> BOMDescription1 { get; set; }
        public Rule<string> BOMDescription2 { get; set; }
        public Rule<string> BOMDescription3 { get; set; }
        public Rule<string> BOMDescription4 { get; set; }
        public Rule<string> BOMDescription5 { get; set; }
        public Rule<string> BOMDescription6 { get; set; }
        public Rule<string> BOMDescription7 { get; set; }
        public Rule<string> BOMDescription8 { get; set; }
        public Rule<string> BOMDescription9 { get; set; }
        public Rule<string> BOMDescription10 { get; set; }
        public Rule<string> BOMDescription11 { get; set; }
        public Rule<string> Color { get; set; }
        protected virtual void ConfigureColor(Rule<string> rule)
        {
            rule.Define("");
        }
        public ReadOnlyRule<AdvanceColor> AdvanceColor { get; set; }
        //protected virtual void ConfigureAdvanceColor(ReadOnlyRule<AdvanceColor> rule)
        //{
        //    rule.Define(() => null);
        //}
        public ReadOnlyRule<string> ColorDescription { get; set; }
        protected virtual void ConfigureColorDescription(ReadOnlyRule<string> rule)
        {
            rule.Define("");
        }
        public StorageConfiguratorRepository Repository { get; set; }
        public Rule<string> ColorBOMName { get; set; }
        public ReadOnlyRule<string> ColorShortDescription { get; set; }

        public Rule<double> PartWeight { get; set; }
        public Rule<double> PartPaintWeight { get; set; }
        public Rule<float> WeightPerGallon { get; set; }
        public CsiRule<double> PaintCostPerGallon { get; set; }
        public Rule<float> PaintCost { get; set; }

        #endregion

        #region IRevisablePartImplementation

        public FactoryRevisionRule<string> FactoryRevision { get; set; }

        #endregion

        #region CPQ.IBomPart Implementation

        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {
            child.IsActive.Define(RenderKit);
            child.ItemQty.Define(() => (double)BOMQty.Value);
            child.PartNumber.Define(PartNumber);
            child.Description.Define(BOMDescription1);
        }

        #endregion

        #region Children

        public Bolt Bolt { get; set; }
        protected virtual void ConfigureBolt(Bolt child)
        {
            child.IsActive.Define(RenderKit);
            child.Bom.Item.Define(() => $"{Bom.Item.Value}-01");
            //child.Qty.Define(Qty);
            child.BoltType.Define(BoltType);
            child.BoltDiameter.Define(BoltSize);
            child.BoltMaterial.Define(BoltMaterial);
            child.Length.Define(Length);
        }

        public Nut Nut { get; set; }
        protected virtual void ConfigureNut(Nut child)
        {
            child.IsActive.Define(RenderKit);
            //child.Qty.Define(Qty);
            child.Bom.Item.Define(() => $"{Bom.Item.Value}-02");
            child.BoltDiameter.Define(BoltSize);
            child.NutType.Define(NutType);
            child.NutMaterial.Define(NutMaterial);
        }

        //public Washers Washers { get; set; }
        //protected virtual void ConfigureWashers(Washers child)
        //{
        //    child.IsActive.Define(HasWashers);
        //    child.Qty.Define(Qty);
        //    child.BoltSize.Define(BoltSize);
        //    child.WasherType.Define(WasherType);
        //}

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(Colors.Gray);
        }

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define(() => RenderKit.Value ? "HardwareKit" : "");
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(false);
        }

        #endregion

        #region Overrides

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureBomPartRules();
            this.ConfigureRevisablePart();
        }

        #endregion
    }
}
