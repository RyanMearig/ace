﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ruler.Rules;
using Ruler.Rules.Graphics;
using System.Numerics;

namespace StorageConfigurator.Rules
{
    public class UCS: Part
    {

        #region Constructors

        public UCS(ModelContext modelContext) : base(modelContext) { }

        public UCS(string name, Part parent) : base(name, parent) { }

        #endregion

        #region Rules

        public Rule<float> Diameter { get; set; }
        protected virtual void ConfigureDiameter(Rule<float> rule)
        {
            rule.Define(2f);
        }

        public Rule<float> Length { get; set; }
        protected virtual void ConfigureLength(Rule<float> rule)
        {
            rule.Define(12f);
        }

        public Rule<bool> Visible { get; set; }
        protected virtual void ConfigureVisible(Rule<bool> rule)
        {
            rule.Define(true);
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial uiMaterial)
        {
            uiMaterial.Color.Define(Colors.Purple);
            uiMaterial.Opacity.Define(0.8f);
        }

        #endregion

        #region Children

        public UCSArrow YArrow { get; set; }
        protected virtual void ConfigureYArrow(UCSArrow child)
        {
            child.IsActive.Define(Visible);
            child.Diameter.Define(Diameter);
            child.Length.Define(Length);
            child.ArrowColor.Define("Green");
        }

        public UCSArrow XArrow { get; set; }
        protected virtual void ConfigureXArrow(UCSArrow child)
        {
            child.IsActive.Define(Visible);
            child.Diameter.Define(Diameter);
            child.Length.Define(Length);
            child.ArrowColor.Define("Red");
            child.Rotation.Define(() => Helper.CreateRotation(Vector3.UnitZ, -90));
        }

        public UCSArrow ZArrow { get; set; }
        protected virtual void ConfigureZArrow(UCSArrow child)
        {
            child.IsActive.Define(Visible);
            child.Diameter.Define(Diameter);
            child.Length.Define(Length);
            child.ArrowColor.Define("Blue");
            child.Rotation.Define(() => Helper.CreateRotation(Vector3.UnitX, 90));
        }


        #endregion

        #region Graphics

        //public Cylinder Arrow { get; set; }
        //protected virtual void ConfigureArrow(Cylinder geo)
        //{
        //    geo.RadiusTop.Define(() => Diameter.Value / 2);
        //    geo.RadiusBottom.Define(() => Diameter.Value / 2);
        //    geo.Height.Define(Length);
        //    geo.RadiusSegments.Define(120);
        //}

        #endregion

    }
}
