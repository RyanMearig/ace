﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class CsiRule<T> : Rule<T>
    {
        public CsiRule(IRuleHost host, AdvanceCSIItemService csiPartService) : base(host)
        {
            CsiPartService = csiPartService;
        }

        public CsiRule(string name, IRuleHost host, AdvanceCSIItemService csiPartService) : base(name, host)
        {
            CsiPartService = csiPartService;
        }

        public AdvanceCSIItemService CsiPartService { get; }

        public double GetItemCost(string partNumber, string plant)
        {
            return CsiPartService.GetItemCost(partNumber, plant);
        }
    }
}
