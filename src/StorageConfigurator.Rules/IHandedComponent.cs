﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;
using Ruler.Rules.Validation;

namespace StorageConfigurator.Rules
{
    public interface IHandedComponent
    {

        Rule<string> Hand { get; set; }
        ReadOnlyRule<bool> IsLeftHand { get; set; }
        ReadOnlyRule<bool> IsRightHand { get; set; }
        ReadOnlyRule<int> HandFactor { get; set; }

    }

    public static class IHandedComponentExtensions
    {
        public static void ConfigureHandedComponentRules(this IHandedComponent comp)
        {
            comp.Hand.Define(DefaultHand);
            comp.Hand.AddToUI(ui =>
            {
                ui.ChoiceList.Define(new List<ChoiceListItem>()
                { new ChoiceListItem("LH", "Left"), new ChoiceListItem("RH", "Right")});
                //ui.ReadOnly.Define(comp.DisableUserInputs);
            });
            comp.IsLeftHand.Define(() => comp.Hand.Value == "LH");
            comp.IsRightHand.Define(() => comp.Hand.Value == "RH");

            comp.HandFactor.Define(() => comp.IsLeftHand.Value ? -1 : 1);

        }

        const string DefaultHand = "LH";
    }
}
