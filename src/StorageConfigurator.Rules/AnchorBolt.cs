﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ruler.Cpq;
using Ruler.Rules;
using Ruler.Rules.Graphics;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class AnchorBolt : AdvancePart, IBomPart, Ruler.Cpq.IBomPart, IAnchorData
    {
        #region Constructors

        public AnchorBolt(ModelContext modelContext) : base(modelContext) { }

        public AnchorBolt(string name, Part parent) : base(name, parent) { }

        #endregion

        #region Rules

        public Rule<bool> IsStandalone { get; set; }
        protected virtual void ConfigureIsStandalone(Rule<bool> rule)
        {
            rule.Define(() => (Parent == null || Parent?.GetType() == typeof(ShippableParts)));
        }

        public Rule<bool> RenderModel { get; set; }
        protected virtual void ConfigureRenderModel(Rule<bool> rule)
        {
            rule.Define(false);
        }

        public Rule<string> AnchorSize { get; set; }
        protected virtual void ConfigureAnchorSize(Rule<string> rule)
        {
            rule.Define(() => rule.UIMetadata.ChoiceList.Value.First().Value.ToString());
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(new List<ChoiceListItem>()
                {
                    new ChoiceListItem("0.5", @"1/2"""),
                    new ChoiceListItem("0.625", @"5/8"""),
                    new ChoiceListItem("0.75", @"3/4""")
                });
            });
            rule.ValueSet += (sender, e) =>
            {
                AnchorType.Reset();
            };
            rule.ValueReset += (sender, e) =>
            {
                AnchorType.Reset();
            };
        }

        public Rule<string> AnchorType { get; set; }
        protected virtual void ConfigureAnchorType(Rule<string> rule)
        {
            rule.Define(() =>
            {
                if (rule.UIMetadata.ChoiceList.Value.Any())
                {
                    return rule.UIMetadata.ChoiceList?.Value?.First()?.Value?.ToString() ?? "";
                }
                return "";
            });
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() => this.GetAnchorChoiceList(float.Parse(AnchorSize.Value)));
            });
        }

        public Rule<AnchorData> ActiveAnchorData { get; set; }
        protected virtual void ConfigureActiveAnchorData(Rule<AnchorData> rule)
        {
            rule.Define(() => string.IsNullOrWhiteSpace(AnchorType.Value) ? null : (AllAnchorData.Value.FirstOrDefault(d => d.Type == AnchorType.Value)));
        }

        public Rule<AnchorDefinition> AnchorBoltDefinition { get; set; }
        protected virtual void ConfigureAnchorBoltDefinition(Rule<AnchorDefinition> rule)
        {
            rule.Define(() =>
            {
                if (!IsStandalone.Value)
                {
                    return default(AnchorDefinition);
                }
                return new AnchorDefinition()
                {
                    AnchorData = ActiveAnchorData.Value,
                    Qty = 1
                };
            });
        }

        public Rule<bool> AnchorDefinitionSupplied { get; set; }
        protected virtual void ConfigureAnchorDefinitionSupplied(Rule<bool> rule)
        {
            rule.Define(() => AnchorBoltDefinition.Value != default(AnchorDefinition) && AnchorBoltDefinition.Value.AnchorData != null);
        }

        public Rule<float> Diameter { get; set; }
        protected virtual void ConfigureDiameter(Rule<float> rule)
        {
            rule.Define(() => AnchorDefinitionSupplied.Value ? AnchorBoltDefinition.Value.AnchorData.BoltDiameter : default(float));
        }

        public Rule<float> Length { get; set; }
        protected virtual void ConfigureLength(Rule<float> rule)
        {
            rule.Define(() => AnchorDefinitionSupplied.Value ? AnchorBoltDefinition.Value.AnchorData.Length : default(float));
        }

        public Rule<int> Qty { get; set; }
        protected virtual void ConfigureQty(Rule<int> rule)
        {
            rule.Define(() => AnchorDefinitionSupplied.Value ? AnchorBoltDefinition.Value.Qty : default(int));
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => !IsStandalone.Value);
            });
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(RenderModel);
                cad.CadRuleUsageType = CadRuleUsageType.Property;
            });
        }

        #endregion

        #region IAnchorData Implementation

        public Rule<List<AnchorData>> AllAnchorData { get; set; }
        public Rule<List<ChoiceListItem>> AllAnchorTypesChoiceList { get; set; }

        #endregion

        #region IBomPart Implementation

        public Rule<string> DatabasePartNumber { get; set; }
        public PartNumberRule<string> PartNumber { get; set; }
        protected virtual void ConfigurePartNumber(PartNumberRule<string> rule)
        {
            rule.Define(() => AnchorDefinitionSupplied.Value ? AnchorBoltDefinition.Value.AnchorData.Type : default(string));
        }
        public Rule<string> PartNumberPrefix { get; set; }

        public Rule<string> Description { get; set; }
        protected virtual void ConfigureDescription(Rule<string> rule)
        {
            rule.Define(() => AnchorDefinitionSupplied.Value ? AnchorBoltDefinition.Value.AnchorData.TypeDisplay : default(string));
        }

        public Rule<string> BOMDescription { get; set; }
        protected virtual void ConfigureBOMDescription(Rule<string> rule)
        {
            rule.Define(Description);
        }

        public Rule<string> BOMDescription1 { get; set; }
        public Rule<string> BOMDescription2 { get; set; }
        public Rule<string> BOMDescription3 { get; set; }
        public Rule<string> BOMDescription4 { get; set; }
        public Rule<string> BOMDescription5 { get; set; }
        public Rule<string> BOMDescription6 { get; set; }
        public Rule<string> BOMDescription7 { get; set; }
        public Rule<string> BOMDescription8 { get; set; }
        public Rule<string> BOMDescription9 { get; set; }
        public Rule<string> BOMDescription10 { get; set; }
        public Rule<string> BOMDescription11 { get; set; }

        public Rule<string> Color { get; set; }
        protected virtual void ConfigureColor(Rule<string> rule)
        {
            rule.Define("");
        }
        public ReadOnlyRule<AdvanceColor> AdvanceColor { get; set; }
        public ReadOnlyRule<string> ColorDescription { get; set; }
        public StorageConfiguratorRepository Repository { get; set; }
        public Rule<string> ColorBOMName { get; set; }
        public ReadOnlyRule<string> ColorShortDescription { get; set; }
        protected virtual void ConfigureColorShortDescription(ReadOnlyRule<string> rule)
        {
            rule.Define(default(string));
        }

        public Rule<double> PartWeight { get; set; }
        public Rule<double> PartPaintWeight { get; set; }
        public Rule<float> WeightPerGallon { get; set; }
        public CsiRule<double> PaintCostPerGallon { get; set; }
        public Rule<float> PaintCost { get; set; }

        #endregion

        #region CPQ.IBomPart Implementation

        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {
            child.ItemQty.Define(() => (double)Qty.Value);
            child.PartNumber.Define(PartNumber);
            child.Description.Define(BOMDescription1);
            child.IsPurchased.Define(true);
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(Colors.Gray);
        }

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define(() => RenderModel.Value ? "AnchorBolts" : "");
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(() => IsStandalone.Value);
        }

        #endregion

        #region Overrides

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureBomPartRules();
            this.ConfigureAnchorDataRules();
        }

        #endregion
    }
}
