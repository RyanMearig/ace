﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StorageConfigurator.Rules
{
    public class AnchorData
    {
        public string Type { get; set; }
        public string TypeDisplay { get; set; }
        public float BoltDiameter { get; set; }
        public float Length { get; set; }
    }
}
