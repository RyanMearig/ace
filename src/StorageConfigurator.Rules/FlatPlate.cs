﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;
using Ruler.Rules.Graphics;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class FlatPlate : AdvancePart, IRevisablePart, IBomPart
    {
        #region Constructors

        public FlatPlate(ModelContext modelContext) : base(modelContext) { }

        public FlatPlate(string name, Part parent) : base(name, parent) { }

        #endregion

        #region Rules

        [CadRule(CadRuleUsageType.Parameter)]
        public Rule<float> Width { get; set; }
        protected virtual void ConfigureWidth(Rule<float> rule)
        {
            rule.Define(2);
        }

        [CadRule(CadRuleUsageType.Parameter)]
        public Rule<float> Length { get; set; }
        protected virtual void ConfigureLength(Rule<float> rule)
        {
            rule.Define(8);
        }

        public Rule<float> Thickness { get; set; }
        protected virtual void ConfigureThickness(Rule<float> rule)
        {
            rule.Define(0.1875f);
        }

        public Rule<bool> IsCrossbarPlate { get; set; }
        protected virtual void ConfigureIsCrossbarPlate(Rule<bool> rule)
        {
            rule.Define(false);
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> Description { get; set; }
        protected virtual void ConfigureDescription(Rule<string> rule)
        {
            rule.Define(() => $"{Helper.fractionString(Thickness.Value)} x {Helper.fractionString(Width.Value)} FLAT BAR");
        }

        public Rule<string> DescriptionPrefix { get; set; }
        protected virtual void ConfigureDescriptionPrefix(Rule<string> rule)
        {
            rule.Define(default(string));
        }

        //This is the description used only by the BOM Summary.
        public Rule<string> BOMUIDescription { get; set; }
        protected virtual void ConfigureBOMUIDescription(Rule<string> rule)
        {
            rule.Define(() =>
            {
                return $"{DescriptionPrefix.Value}({Helper.fractionString(Thickness.Value, feetToo: false)} x " +
                $"{Helper.fractionString(Width.Value, feetToo: false)} x " +
                $"{Helper.fractionString(Length.Value, feetToo: false)})";
            });
        }

        #endregion

        #region IRevisablePartImplementation

        public FactoryRevisionRule<string> FactoryRevision { get; set; }

        #endregion

        #region IBomPart Implementation

        public Rule<string> DatabasePartNumber { get; set; }

        [CadRule(CadRuleUsageType.Property)]
        public PartNumberRule<string> PartNumber { get; set; }
        public Rule<string> PartNumberPrefix { get; set; }
        public Rule<string> BOMDescription { get; set; }
        public Rule<string> BOMDescription1 { get; set; }
        public Rule<string> BOMDescription2 { get; set; }
        public Rule<string> BOMDescription3 { get; set; }
        public Rule<string> BOMDescription4 { get; set; }
        public Rule<string> BOMDescription5 { get; set; }
        public Rule<string> BOMDescription6 { get; set; }
        public Rule<string> BOMDescription7 { get; set; }
        public Rule<string> BOMDescription8 { get; set; }
        public Rule<string> BOMDescription9 { get; set; }
        public Rule<string> BOMDescription10 { get; set; }
        public Rule<string> BOMDescription11 { get; set; }
        public Rule<string> Color { get; set; }
        public ReadOnlyRule<AdvanceColor> AdvanceColor { get; set; }
        public ReadOnlyRule<string> ColorDescription { get; set; }
        public StorageConfiguratorRepository Repository { get; set; }
        public Rule<string> ColorBOMName { get; set; }
        public ReadOnlyRule<string> ColorShortDescription { get; set; }

        public Rule<double> PartWeight { get; set; }
        public Rule<double> PartPaintWeight { get; set; }
        public Rule<float> WeightPerGallon { get; set; }
        public CsiRule<double> PaintCostPerGallon { get; set; }
        public Rule<float> PaintCost { get; set; }

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(Colors.Red);
        }

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define("FlatPlate");
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(true);
        }

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureRevisablePart();
            this.ConfigureBomPartRules();
        }

        #endregion

        #region Graphics

        public Block Block { get; set; }
        protected virtual void ConfigureBlock(Block geo)
        {
            geo.Width.Define(Width);
            geo.Height.Define(Thickness);
            geo.Depth.Define(Length);
            geo.Position.Define(() => Helper.Vy(-geo.Height.Value / 2));
        }

        //public UCS UCS { get; set; }
        //protected virtual void ConfigureUCS(UCS child)
        //{
        //    child.Diameter.Define(0.125f);
        //    child.Position.Define(() => (Helper.Vy(0)));
        //}

        #endregion

    }
}
