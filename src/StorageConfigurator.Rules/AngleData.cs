﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StorageConfigurator.Rules
{
    public class AngleData
    {
        public string Type { get; set; }
        public string TypeDisplay { get; set; }
        public float Width { get; set; }
        public float Height { get; set; }
        public float Thickness { get; set; }
        public float Length { get; set; }
    }
}
