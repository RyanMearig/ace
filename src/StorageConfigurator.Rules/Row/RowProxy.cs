﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using Ruler.Rules;
using Ruler.Rules.Graphics;

namespace StorageConfigurator.Rules
{
    public class RowProxy : Part
    {
        #region Constructors

        public RowProxy(ModelContext modelContext) : base(modelContext) { }

        public RowProxy(string name, Part parent) : base(name, parent) 
        {
            this.SetIndex();
        }

        #endregion

        #region Rules

        public LookupRule<StorageSystem> StorageSystemComp { get; set; }

        protected override void ConfigureDisplayName(Rule<string> rule)
        {
            rule.Define(RowNumber);
        }

        public Rule<int> Index { get; set; }
        protected virtual void ConfigureIndex(Rule<int> rule)
        {
            rule.Define(0);
        }

        [UIRule]
        public Rule<string> RowNumber { get; set; }
        protected virtual void ConfigureRowNumber(Rule<string> rule)
        {
            rule.Define(() => $"Row {Index.Value}");
            rule.Unique();
            rule.AddToCAD();
        }

        [UIRule]
        public Rule<string> RowTypeName { get; set; }
        protected virtual void ConfigureRowTypeName(Rule<string> rule)
        {
            rule.Define(() => RowTypeChoices.Value?.FirstOrDefault()?.Value.ToString() ?? "");
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() =>
                {
                    if (RowTypeChoices.Value.Any())
                        return RowTypeChoices.Value;
                    else
                        return new List<ChoiceListItem>();
                });
            });
        }

        [UIRule]
        public Rule<bool> IncludeInBOM { get; set; }
        protected virtual void ConfigureIncludeInBOM(Rule<bool> rule)
        {
            rule.Define(true);
        }

        public Rule<List<Row>> AllAvailableRowTypes { get; set; }
        protected virtual void ConfigureAllAvailableRowTypes(Rule<List<Row>> rule)
        {
            rule.Define(() =>
            {
                List<Row> retVal = new List<Row>();

                if (StorageSystemComp != null && StorageSystemComp.Value != null)
                {
                    if (StorageSystemComp.Value.RowTypes.RowList.Value.Any())
                    {
                        retVal = StorageSystemComp.Value.RowTypes.RowList.Value;
                    }
                }

                return retVal;
            });
        }

        public Rule<List<ChoiceListItem>> RowTypeChoices { get; set; }
        protected virtual void ConfigureRowTypeChoices(Rule<List<ChoiceListItem>> rule)
        {
            rule.Define(() =>
            {
                List<ChoiceListItem> retList = new List<ChoiceListItem>();

                if (AllAvailableRowTypes.Value != null)
                {
                    AllAvailableRowTypes.Value.ForEach(b => retList.Add(new ChoiceListItem(b.Path, b.RowTypeName.Value)));
                }

                return retList;
            });
        }

        public Rule<Row> ActiveRow { get; set; }
        protected virtual void ConfigureActiveRow(Rule<Row> rule)
        {
            rule.Define(() =>
            {
                Row retVal = null;

                if (RowTypeChoices.Value.Any() && RowTypeChoices.Value.Any(p => p.Value.ToString() == RowTypeName.Value))
                    retVal = AllAvailableRowTypes.Value.FirstOrDefault(f => f.Path == RowTypeName.Value);

                return retVal;
            });
            //rule.AddToCAD(cad =>
            //{
            //    cad.IsActive.Define(() => ActiveRow.Value != null);
            //    cad.CadRuleUsageType = CadRuleUsageType.ChildProxy;
            //    cad.ChildProxyMatrixFactory = () => Matrix4x4.CreateTranslation(Helper.Vz(0));
            //});
        }

        #endregion

        #region Private Methods

        public void SetIndex()
        {
            var existingRows = Parent.ActiveChildParts.Value
                    .Where(c => c != this && c.TypeName.Contains("RowProxy"));
            var index = existingRows.Count() + 1;
            this.Index.SetAdhocDefaultValue(index);
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(true);
        }

        public override bool IsCadPart => false;

        #endregion

    }
}
