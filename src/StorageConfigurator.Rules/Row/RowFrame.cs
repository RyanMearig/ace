﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using Ruler.Rules;
using Ruler.Rules.Graphics;

namespace StorageConfigurator.Rules
{
    public class RowFrame : Part, IRevisablePart
    {
        #region Constructors

        public RowFrame(ModelContext modelContext) : base(modelContext) { }

        public RowFrame(string name, Part parent) : base(name, parent) { }

        #endregion

        #region Rules

        public LookupRule<Row> RowComp { get; set; }

        public Rule<int> FrameIndex { get; set; }
        protected virtual void ConfigureFrameIndex(Rule<int> rule)
        {
            rule.Define(default(int));
        }

        public Rule<bool> IsFirstFrame { get; set; }
        protected virtual void ConfigureIsFirstFrame(Rule<bool> rule)
        {
            rule.Define(() => FrameIndex.Value == 1);
        }

        public Rule<RowFrame> PreviousFrame { get; set; }
        protected virtual void ConfigurePreviousFrame(Rule<RowFrame> rule)
        {
            rule.Define(default(RowFrame));
        }

        public Rule<RowBay> PreviousBay { get; set; }
        protected virtual void ConfigurePreviousBay(Rule<RowBay> rule)
        {
            rule.Define(default(RowBay));
        }

        public Rule<RowBay> NextBay { get; set; }
        protected virtual void ConfigureNextBay(Rule<RowBay> rule)
        {
            rule.Define(default(RowBay));
        }

        public Rule<float> BayDepth { get; set; }
        protected virtual void ConfigureBayDepth(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (IsFirstFrame.Value) {
                    return NextBay.Value?.ActiveBay.Value?.OverallDepth.Value ?? 12;
                }
                return PreviousBay.Value?.ActiveBay.Value?.OverallDepth.Value ?? 12;
            });
        }

        public Rule<float> OverallDepth { get; set; }
        protected virtual void ConfigureOverallDepth(Rule<float> rule)
        {
            rule.Define(() => ActiveFrame.Value == null ? 1 : ActiveFrame.Value.OverallDepth.Value);
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(true);
            });
        }

        public Rule<float> OverallWidth { get; set; }
        protected virtual void ConfigureOverallWidth(Rule<float> rule)
        {
            rule.Define(() => ActiveFrame.Value == null ? 1 : ActiveFrame.Value.Width.Value);
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(true);
            });
        }

        public Rule<float> OverallHeight { get; set; }
        protected virtual void ConfigureOverallHeight(Rule<float> rule)
        {
            rule.Define(() => ActiveFrame.Value == null ? 1 : ActiveFrame.Value.Height.Value);
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(true);
            });
        }

        [UIRule]
        public Rule<string> FrameName { get; set; }
        protected virtual void ConfigureFrameName(Rule<string> rule)
        {
            rule.Define(() =>
            {
                string retVal = "";

                if (IsFirstFrame.Value || PreviousFrame.Value == null)
                {
                    retVal = FrameChoices.Value?.First().Value?.ToString() ?? "";
                }
                else if (FrameChoices.Value.Any(c => c.Value.ToString() == PreviousFrame.Value.FrameName.Value))
                {
                    retVal = PreviousFrame.Value.FrameName.Value;
                }
                else
                {
                    retVal = FrameChoices.Value.First().Value.ToString();
                }

                return retVal;
            });
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(() =>
                {
                    if (FrameChoices.Value.Any())
                        return FrameChoices.Value;
                    else
                        return new List<ChoiceListItem>();
                });
            });
            rule.ValueSet += (sender, e) =>
            {
                if (IsFirstFrame.Value)
                {
                    var oldValFrame = GetFrameLineInstance(e.OldValue);
                    var newValFrame = GetFrameLineInstance(e.NewValue);

                    if (oldValFrame.GetType() != newValFrame.GetType())
                    {
                        ResetRowFrameNames();
                    }
                    else if (newValFrame.GetType() == typeof(SelectiveFrameLine) &&
                             ((SelectiveFrameLine)newValFrame).IsBackToBack.Value != ((SelectiveFrameLine)oldValFrame).IsBackToBack.Value)
                    {
                        ResetRowFrameNames();
                    }   
                }

                if (RowComp != null && RowComp.Value != null && !string.IsNullOrEmpty(e.OldValue) && (e.NewValue != e.OldValue))
                {
                    RowComp.Value.ResetBaysAfter(FrameIndex.Value);
                }
            };
            rule.ValueReset += (sender, e) =>
            {
                if (IsFirstFrame.Value)
                {
                    ResetRowFrameNames();
                }
            };
        } 

        public Rule<List<FrameLine>> AllAvailableFrames { get; set; }
        protected virtual void ConfigureAllAvailableFrames(Rule<List<FrameLine>> rule)
        {
            rule.Define(() => RowComp.Value.AllAvailableFrames.Value);
        }

        public Rule<List<FrameLine>> FilteredFrames { get; set; }
        protected virtual void ConfigureFilteredFrames(Rule<List<FrameLine>> rule)
        {
            rule.Define(() =>
            {
                List<FrameLine> retVal = new List<FrameLine>();
                if (AllAvailableFrames.Value != null && AllAvailableFrames.Value.Any())
                {
                    if (IsFirstFrame.Value)
                    {
                        retVal = AllAvailableFrames.Value;
                    }
                    else
                    {
                        if (PreviousFrame.Value.ActiveFrame.Value != null && PreviousFrame.Value.ActiveFrameIsSelective.Value)
                        {
                            var prevFrame = (SelectiveFrameLine)PreviousFrame.Value.ActiveFrame.Value;
                            IEnumerable<FrameLine> result = null;
                            if (PreviousBay.Value != null && PreviousBay.Value.IsEmptyBay.Value)
                            {
                                result = AllAvailableFrames.Value.Where(f =>
                                {
                                    return FrameLineIsSelective(f) &&
                                            ((SelectiveFrameLine)f).IsBackToBack.Value == prevFrame.IsBackToBack.Value;
                                });
                            }
                            else
                            {
                                result = AllAvailableFrames.Value.Where(f =>
                                {
                                    return FrameLineIsSelective(f) &&
                                            CompatibleSelectiveFrameLines((SelectiveFrameLine)f, prevFrame);
                                });
                            }

                            if (result != null && result.Any())
                            {
                                retVal = result.ToList();
                            }
                        }
                    }
                }

                return retVal;
            });
        }

        public Rule<List<ChoiceListItem>> FrameChoices { get; set; }
        protected virtual void ConfigureFrameChoices(Rule<List<ChoiceListItem>> rule)
        {
            rule.Define(() =>
            {
                List<ChoiceListItem> retList = new List<ChoiceListItem>();

                if (FilteredFrames.Value != null)
                {
                    //TODO: Figure out what ID we can use to avoid errors when customer deletes a Frame or changes a name.
                    FilteredFrames.Value.ForEach(b => retList.Add(new ChoiceListItem(b.Path, b.FrameName.Value)));
                }

                return retList;
            });
        }

        public Rule<List<KeyValuePair<FrameType, float>>> ActiveFrameTypes { get; set; }
        protected virtual void ConfigureActiveFrameTypes(Rule<List<KeyValuePair<FrameType, float>>> rule)
        {
            rule.Define(() =>
            {
                var retVal = new List<KeyValuePair<FrameType, float>>();

                if (ActiveFrame.Value != null)
                {
                    if (ActiveFrameIsSelective.Value)
                    {
                        var frame = (SelectiveFrameLine)ActiveFrame.Value;
                        retVal.Add(new KeyValuePair<FrameType, float>(frame.FrontFrame.Value, 0));

                        if (frame.IsBackToBack.Value)
                        {
                            retVal.Add(new KeyValuePair<FrameType, float>(frame.RearFrame.Value, 
                                                    frame.FrontFrame.Value.OverallDepth.Value + frame.FrameTiesSpace.OverallDepth.Value));
                        }
                    }

                    //TODO: Find a better way to get this info once creating Pushback
                    //Order them based on their X location in space so the front frame is the first one.
                }

                return retVal;
            });
        }

        public Rule<bool> ActiveFrameIsSelective { get; set; }
        protected virtual void ConfigureActiveFrameIsSelective(Rule<bool> rule)
        {
            rule.Define(() => FrameLineIsSelective(ActiveFrame.Value));
        }

        public Rule<bool> ActiveFrameIsBackToBack { get; set; }
        protected virtual void ConfigureActiveFrameIsBackToBack(Rule<bool> rule)
        {
            rule.Define(() => ActiveFrameIsSelective.Value && ((SelectiveFrameLine)ActiveFrame.Value).IsBackToBack.Value);
        }

        #endregion

        #region Methods

        public bool FrameLineIsSelective(FrameLine fLine)
        {
            return fLine != null && fLine.GetType() == typeof(SelectiveFrameLine);
        }

        public FrameLine GetFrameLineInstance(string path)
        {
            FrameLine retVal = null;

            if (FrameChoices.Value.Any() && FrameChoices.Value.Any(p => p.Value.ToString() == path))
            {
                retVal = AllAvailableFrames.Value.FirstOrDefault(f => f.Path == path);
            }

            return retVal;
        }

        public void ResetRowFrameNames()
        {
            RowComp.Value.Frames.ToList().ForEach(f =>
            {
                if (f.Path != this.Path && f.FrameName.IsOverridden)
                {
                    f.FrameName.Reset();
                }
            });
        }

        public bool CompatibleSelectiveFrameLines(SelectiveFrameLine line1, SelectiveFrameLine line2)
        {
            return line1.IsBackToBack.Value == line2.IsBackToBack.Value &&
                    CompatibleFrameTypes(line1.FrontFrame.Value, line2.FrontFrame.Value);
        }

        public bool CompatibleFrameTypes(FrameType fType, FrameType fType2)
        {
            return GetFrameTypeFirstFrameDepth(fType) == GetFrameTypeFirstFrameDepth(fType2);
        }

        public float GetFrameTypeFirstFrameDepth(FrameType fType)
        {
            if (fType.GetType() == typeof(SelectiveFrameType))
            {
                return ((SelectiveFrameType)fType).Frame.Frame.Depth.Value;
            }
            else if (fType.GetType() == typeof(DoubleDeepFrameType))
            {
                return ((DoubleDeepFrameType)fType).FrontFrame.Frame.Depth.Value;
            }
            return 0;
        }

        #endregion

        #region Children

        public Rule<FrameLine> ActiveFrame { get; set; }
        protected virtual void ConfigureActiveFrame(Rule<FrameLine> rule)
        {
            rule.Define(() => GetFrameLineInstance(FrameName.Value));
            rule.AddToCAD(cad =>
            {
                //cad.IsActive.Define(RenderInCad);
                cad.CadRuleUsageType = CadRuleUsageType.ChildProxy;
                cad.ChildProxyMatrixFactory = () => Matrix4x4.CreateTranslation(Helper.Vz(-rule.Value.Width.Value / 2));
            });
        }

        #endregion

        #region IRevisablePartImplementation

        public FactoryRevisionRule<string> FactoryRevision { get; set; }

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(Colors.Blue);
            material.Opacity.Define(0.5f);
        }

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define("RowFrame");
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(false);
        }

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureRevisablePart();
        }

        #endregion

        #region Graphics

        public ChildList<BoundingCube> FrameTypeBlocks { get; set; }
        protected virtual void ConfigureFrameTypeBlocks(ChildList<BoundingCube> list)
        {
            list.Qty.Define(() => ActiveFrameTypes.Value.Count());
            list.ConfigureChildren((cube, index) =>
            {
                cube.OverallWidth.Define(() => ActiveFrameTypes.Value[index].Key.Width.Value);
                cube.OverallHeight.Define(() => ActiveFrameTypes.Value[index].Key.Height.Value);
                cube.OverallDepth.Define(() => ActiveFrameTypes.Value[index].Key.OverallDepth.Value);
                cube.CubeColor.Define(Colors.Blue);
                cube.Position.Define(() => Helper.Vx(-ActiveFrameTypes.Value[index].Value));
            });
        }

        //public UCS UCS { get; set; }
        //protected virtual void ConfigureUCS(UCS child)
        //{
        //    child.Position.Define(() => (Helper.Vy(0)));
        //}

        #endregion

    }
}
