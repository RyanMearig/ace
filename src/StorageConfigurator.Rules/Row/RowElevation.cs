﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;

namespace StorageConfigurator.Rules
{
    public class RowElevation : Part
    {
        #region Constructors

        public RowElevation(ModelContext modelContext) : base(modelContext) { }

        public RowElevation(string name, Part parent) : base(name, parent) { }

        #endregion

        #region Rules

        [UIRule]
        public Rule<string> ElevationName { get; set; }
        protected virtual void ConfigureElevationName(Rule<string> rule)
        {
            rule.Define("E");
        }

        public Rule<RowFrame> FrontFrame { get; set; }
        protected virtual void ConfigureFrontFrame(Rule<RowFrame> rule)
        {
            rule.Define(default(RowFrame));
        }

        public Rule<RowBay> Bay { get; set; }
        protected virtual void ConfigureBay(Rule<RowBay> rule)
        {
            rule.Define(default(RowBay));
        }

        public Rule<RowFrame> RearFrame { get; set; }
        protected virtual void ConfigureRearFrame(Rule<RowFrame> rule)
        {
            rule.Define(default(RowFrame));
        }

        #endregion

    }
}
