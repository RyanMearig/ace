﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Ruler.Rules;
using Ruler.Rules.Validation;
using Ruler.Rules.Graphics;
using StorageConfigurator.Data;
using Ruler.Cpq;
using System.Numerics;

namespace StorageConfigurator.Rules
{
    public class Row : AdvancePart, IRevisablePart, IBomPart, Ruler.Cpq.IBomPart
    {
        #region Constructors

        public Row(ModelContext modelContext) : base(modelContext) { }

        public Row(string name, Part parent) : base(name, parent) 
        {
            this.SetIndex();
        }

        #endregion

        #region Constants

        public const float MinOffsetBetweenAisleGuardLevels = 12;

        #endregion

        #region Rules

        public LookupRule<StorageSystem> StorageSystemComp { get; set; }
        public LookupRule<string> LineItemName { get; set; }
        protected virtual void ConfigureLineItemName(LookupRule<string> rule)
        {
            rule.Define("[[LineItemName]]");
        }

        public Rule<Row> RowComp { get; set; }
        protected virtual void ConfigureRowComp(Rule<Row> rule)
        {
            rule.Define(() => this);
        }

        protected override void ConfigureDisplayName(Rule<string> rule)
        {
            rule.Define(RowTypeName);
        }

        public Rule<int> Index { get; set; }
        protected virtual void ConfigureIndex(Rule<int> rule)
        {
            rule.Define(0);
        }

        [UIRule]
        public Rule<string> RowTypeName { get; set; }
        protected virtual void ConfigureRowTypeName(Rule<string> rule)
        {
            rule.Define(() =>
            {
                var id = Convert.ToChar(64 + Index.Value);
                return $"Type {id}";
            });
            rule.Unique();
            rule.AddToCAD();
        }

        [UIRule]
        public ReadOnlyRule<bool> IsUsedRowType { get; set; }
        protected virtual void ConfigureIsUsedRowType(ReadOnlyRule<bool> rule)
        {
            rule.Define(() =>
            {
                if (StorageSystemComp.Value != null && StorageSystemComp.Value.ActiveRowProxies.Value.Any())
                {
                    return StorageSystemComp.Value.ActiveRowProxies.Value.Any(p => p != null && p.RowTypeName.Value == this.Path);
                }
                else
                {
                    return false;
                }
            });
        }

        //[UIRule]
        //public Rule<string> RowName { get; set; }
        //protected virtual void ConfigureRowName(Rule<string> rule)
        //{
        //    rule.Define(() =>
        //    {
        //        var indexString = this.Name.Substring(this.Name.LastIndexOf(':') + 1);
        //        var index = int.TryParse(indexString, out var result) ? result : 0;
        //        return $"Row {index}";
        //    });
        //    rule.Unique();
        //    rule.AddToCAD();
        //}

        [UIRule]
        public Rule<int> RowQty { get; set; }
        protected virtual void ConfigureRowQty(Rule<int> rule)
        {
            rule.Define(() => RowNumbers.Value.Count());
        }

        [UIRule]
        public Rule<int> BayQty { get; set; }
        protected virtual void ConfigureBayQty(Rule<int> rule)
        {
            rule.Define(1);
            rule.Min(1, new Ruler.Rules.Validation.ValidatorOptions(true));
        }

        [UIRule]
        public Rule<int> ElevationQty { get; set; }
        protected virtual void ConfigureElevationQty(Rule<int> rule)
        {
            rule.Define(() => Elevations.Count());
        }

        [UIRule]
        public ReadOnlyRule<float> OverallLength { get; set; }
        protected virtual void ConfigureOverallLength(ReadOnlyRule<float> rule)
        {
            rule.Define(() =>
            {
                float allFramesLength = Frames.Sum(f => f.OverallWidth.Value);
                float allBaysLength = Bays.Sum(b => b.OverallWidth.Value);

                return allFramesLength + allBaysLength;
            });
        }

        [UIRule]
        public ReadOnlyRule<float> OverallHeight { get; set; }
        protected virtual void ConfigureOverallHeight(ReadOnlyRule<float> rule)
        {
            rule.Define(() =>
            {
                float tallestFrame = Frames.Max(f => f.OverallHeight.Value);
                float tallestBay = Bays.Max(b => b.OverallHeight.Value);

                if (tallestFrame > tallestBay)
                    return tallestFrame;
                else
                    return tallestBay;
            });
        }

        [UIRule]
        public ReadOnlyRule<float> OverallDepth { get; set; }
        protected virtual void ConfigureOverallDepth(ReadOnlyRule<float> rule)
        {
            rule.Define(() => Frames.First().OverallDepth.Value);
        }

        public Rule<string> EndAisleGuardLocations { get; set; }
        protected virtual void ConfigureEndAisleGuardLocations(Rule<string> rule)
        {
            rule.Define("none");
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(new List<ChoiceListItem>()
                {
                    new ChoiceListItem("none", "None"),
                    new ChoiceListItem("front", "Front"),
                    new ChoiceListItem("rear", "Rear"),
                    new ChoiceListItem("frontRear", "Front & Rear")
                });
            });
        }

        public Rule<string> EndAisleGuardChannelSize { get; set; }
        protected virtual void ConfigureEndAisleGuardChannelSize(Rule<string> rule)
        {
            rule.Define("C4x4.5");
            rule.AddToUI(ui =>
            {
                ui.ChoiceList.Define(new List<ChoiceListItem>()
                {
                    new ChoiceListItem("C4x4.5", "C4x4.5"),
                    new ChoiceListItem("C6x8.2", "C6x8.2")
                });
            });
        }

        public Rule<int> EndAisleGuardLevels { get; set; }
        protected virtual void ConfigureEndAisleGuardLevels(Rule<int> rule)
        {
            rule.Define(3);
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => EndAisleGuardLocations.Value == "none");
                ui.ChoiceList.Define(new List<ChoiceListItem>()
                {
                    new ChoiceListItem(1),
                    new ChoiceListItem(2),
                    new ChoiceListItem(3),
                    new ChoiceListItem(4)
                });
            });
        }

        public Rule<float> MinAisleGuardHeightAboveGround { get; set; }
        protected virtual void ConfigureMinAisleGuardHeightAboveGround(Rule<float> rule)
        {
            rule.Define(() =>
            {
                if (EndAisleGuardLocations.Value != "none")
                {
                    var frameToUse = EndAisleGuardLocations.Value.ToLower().Contains("front") ? Frames.FirstOrDefault().ActiveFrame.Value
                                                                                              : Frames.LastOrDefault().ActiveFrame.Value;
                    var protectors = GetFrameLineUprightColumnAssys(frameToUse)?
                                .Select(c => (c.PostProtectorType.Value, c.PostProtectorHeight.Value))?
                                .Where(t => t.Item1 != "none");

                    if (protectors != null && protectors.Any())
                    {
                        if (protectors.Any(p => p.Item1 == "angle"))
                        {
                            return Helper.RoundUpToNearest(protectors.Where(p => p.Item1 == "angle").Max(a => a.Item2) + 10.5f, 0.5f);
                        }
                        return Helper.RoundUpToNearest(protectors.Where(p => p.Item1 == "bullnose").Max(a => a.Item2) + 8.5f, 0.5f);
                    }
                }
                
                return 8.5f;
            });
        }

        public Rule<float> EndAisleGuardLevel1HeightAboveGround { get; set; }
        protected virtual void ConfigureEndAisleGuardLevel1HeightAboveGround(Rule<float> rule)
        {
            rule.Define(MinAisleGuardHeightAboveGround);
            rule.MultipleOf(2, 8.5f);
            rule.Min(MinAisleGuardHeightAboveGround);
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => EndAisleGuardLocations.Value == "none");
                ui.Label.Define("Aisle Guard Level 1 Elevation");
            });
        }

        public Rule<float> EndAisleGuardLevel2HeightAboveGround { get; set; }
        protected virtual void ConfigureEndAisleGuardLevel2HeightAboveGround(Rule<float> rule)
        {
            rule.Define(() => EndAisleGuardLevel1HeightAboveGround.Value + MinOffsetBetweenAisleGuardLevels);
            rule.MultipleOf(2, 8.5f);
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => EndAisleGuardLocations.Value == "none" || EndAisleGuardLevels.Value < 2);
                ui.Label.Define("Aisle Guard Level 2 Elevation");
            });
        }

        public Rule<float> EndAisleGuardLevel3HeightAboveGround { get; set; }
        protected virtual void ConfigureEndAisleGuardLevel3HeightAboveGround(Rule<float> rule)
        {
            rule.Define(() => EndAisleGuardLevel2HeightAboveGround.Value + MinOffsetBetweenAisleGuardLevels);
            rule.MultipleOf(2, 8.5f);
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => EndAisleGuardLocations.Value == "none" || EndAisleGuardLevels.Value < 3);
                ui.Label.Define("Aisle Guard Level 3 Elevation");
            });
        }

        public Rule<float> EndAisleGuardLevel4HeightAboveGround { get; set; }
        protected virtual void ConfigureEndAisleGuardLevel4HeightAboveGround(Rule<float> rule)
        {
            rule.Define(() => EndAisleGuardLevel3HeightAboveGround.Value + MinOffsetBetweenAisleGuardLevels);
            rule.MultipleOf(2, 8.5f);
            rule.AddToUI(ui =>
            {
                ui.ReadOnly.Define(() => EndAisleGuardLocations.Value == "none" || EndAisleGuardLevels.Value < 4);
                ui.Label.Define("Aisle Guard Level 4 Elevation");
            });
        }

        public Rule<int> EndAisleGuardSideQty { get; set; }
        protected virtual void ConfigureEndAisleGuardSideQty(Rule<int> rule)
        {
            rule.Define(() => EndAisleGuardLevels.Value * EndAisleGuardPiecesPerLevel.Value);
        }

        public Rule<int> FrontEndAisleGuardQty { get; set; }
        protected virtual void ConfigureFrontEndAisleGuardQty(Rule<int> rule)
        {
            rule.Define(() => EndAisleGuardLocations.Value.Contains("front") ? EndAisleGuardSideQty.Value : 0);
        }

        public Rule<int> EndAisleGuardPiecesPerLevel { get; set; }
        protected virtual void ConfigureEndAisleGuardPiecesPerLevel(Rule<int> rule)
        {
            rule.Define(() =>  (int)Math.Ceiling(Frames.First().OverallDepth.Value/200));
        }

        public Rule<int> RearEndAisleGuardQty { get; set; }
        protected virtual void ConfigureRearEndAisleGuardQty(Rule<int> rule)
        {
            rule.Define(() => EndAisleGuardLocations.Value.ToLower().Contains("rear") ? EndAisleGuardSideQty.Value : 0);
        }

        public Rule<List<Bay>> AllAvailableBays { get; set; }
        protected virtual void ConfigureAllAvailableBays(Rule<List<Bay>> rule)
        {
            rule.Define(() =>
            {
                List<Bay> retVal = new List<Bay>();

                if (StorageSystemComp != null && StorageSystemComp.Value != null)
                {
                    if (StorageSystemComp.Value.Bays.BayList.Value.Any())
                        retVal = StorageSystemComp.Value.Bays.BayList.Value;
                }

                return retVal;
            });
        }

        public Rule<List<FrameLine>> AllAvailableFrames { get; set; }
        protected virtual void ConfigureAllAvailableFrames(Rule<List<FrameLine>> rule)
        {
            rule.Define(() =>
            {
                List<FrameLine> retVal = new List<FrameLine>();

                if (StorageSystemComp != null && StorageSystemComp.Value != null)
                {
                    if (StorageSystemComp.Value.FrameLines.FrameLineList.Value.Any())
                        retVal = StorageSystemComp.Value.FrameLines.FrameLineList.Value;
                }

                return retVal;
            });
        }

        public Rule<List<string>> RowNumbers { get; set; }
        protected virtual void ConfigureRowNumbers(Rule<List<string>> rule)
        {
            rule.Define(() =>
            {
                List<string> retVal = new List<string>();

                if (StorageSystemComp != null && StorageSystemComp.Value != null)
                {
                    var rowNums = StorageSystemComp.Value.GetRowIds(this.Path);

                    retVal = rowNums ?? new List<string>();
                }

                return retVal;
            });
        }

        public Rule<bool> IsBackToBackRow { get; set; }
        protected virtual void ConfigureIsBackToBackRow(Rule<bool> rule)
        {
            rule.Define(() => Frames.FirstOrDefault()?.ActiveFrameIsBackToBack.Value ?? false);
        }

        public Rule<List<Elevation>> UniqueElevations { get; set; }
        protected virtual void ConfigureUniqueElevations(Rule<List<Elevation>> rule)
        {
            rule.Define(() =>
            {
                List<Elevation> retList = new List<Elevation>();

                Elevations.ToList().ForEach(e =>
                {
                    if (e.Bay.Value != null &&
                        (retList.Count == 0 ||
                        !retList.Any(r =>
                        {
                            return (r.FrontFrame.Value == e.FrontFrame.Value &&
                            r.Bay.Value == e.Bay.Value &&
                            r.RearBay.Value == e.RearBay.Value &&
                            r.RearFrame.Value == e.RearFrame.Value);
                        })))
                    {
                        retList.Add(e);
                    }
                });

                return retList;
            });
        }

        #endregion

        #region IRevisablePartImplementation

        public FactoryRevisionRule<string> FactoryRevision { get; set; }

        #endregion

        #region IBomPart Implementation

        public Rule<string> DatabasePartNumber { get; set; }
        public PartNumberRule<string> PartNumber { get; set; }
        protected virtual void ConfigurePartNumber(PartNumberRule<string> rule)
        {
            rule.Define(() => $"{LineItemName.Value}_{RowTypeName.Value}");
        }
        public Rule<string> PartNumberPrefix { get; set; }
        public Rule<string> Description { get; set; }
        public Rule<string> BOMDescription { get; set; }
        public Rule<string> BOMDescription1 { get; set; }
        public Rule<string> BOMDescription2 { get; set; }
        public Rule<string> BOMDescription3 { get; set; }
        public Rule<string> BOMDescription4 { get; set; }
        public Rule<string> BOMDescription5 { get; set; }
        public Rule<string> BOMDescription6 { get; set; }
        public Rule<string> BOMDescription7 { get; set; }
        public Rule<string> BOMDescription8 { get; set; }
        public Rule<string> BOMDescription9 { get; set; }
        public Rule<string> BOMDescription10 { get; set; }
        public Rule<string> BOMDescription11 { get; set; }
        public Rule<string> Color { get; set; }
        public ReadOnlyRule<AdvanceColor> AdvanceColor { get; set; }
        public ReadOnlyRule<string> ColorDescription { get; set; }
        public StorageConfiguratorRepository Repository { get; set; }
        public Rule<string> ColorBOMName { get; set; }
        public ReadOnlyRule<string> ColorShortDescription { get; set; }

        public Rule<double> PartWeight { get; set; }
        public Rule<double> PartPaintWeight { get; set; }
        public Rule<float> WeightPerGallon { get; set; }
        public CsiRule<double> PaintCostPerGallon { get; set; }
        public Rule<float> PaintCost { get; set; }

        #endregion

        #region CPQ.IBomPart Implementation

        // This is only used when getting the Row BOM Summary
        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {
            child.PartNumber.Define(PartNumber);
            child.Item.Define(RowTypeName);
            child.BomChildren.Define(AllRowBomChildren);
        }

        public ChildList<BomData> Boms { get; set; }
        protected virtual void ConfigureBoms(ChildList<BomData> list)
        {
            list.Qty.Define(RowQty);
            list.ConfigureChildren((child, index) =>
            {
                child.PartNumber.Define(() => $"{RowInfo[index].RowNumber.Value}-{RowTypeName.Value}");
                child.Description.Define(() => child.PartNumber.Value);
                child.BomChildren.Define(SingleRowBomChildren);
            });
        }

        public Rule<IEnumerable<IHierarchicalRuleHost>> RowProxyChildren { get; set; }
        protected virtual void ConfigureRowProxyChildren(Rule<IEnumerable<IHierarchicalRuleHost>> rule)
        {
            rule.Define(() =>
            {
                var activeFrames = Frames.Select(f => f.ActiveFrame.Value);
                IEnumerable<IHierarchicalRuleHost> rowProxyChildren = activeFrames?.SelectMany(f => f.UsedFrameTypes.Value);
                rowProxyChildren = rowProxyChildren.Concat(activeFrames?.SelectMany(f => f.UsedFrameTieSpaces.Value));
                rowProxyChildren = rowProxyChildren.Concat(Bays.Select(f => f.ActiveBay.Value).Where(b => b != null));

                if (RearBays.Any())
                {
                    rowProxyChildren = rowProxyChildren.Concat(RearBays.Select(f => f.ActiveBay.Value).Where(b => b != null));
                }

                return rowProxyChildren;
            });
        }

        public Rule<List<BomData>> EndAisleGuardRailParts { get; set; }
        protected virtual void ConfigureEndAisleGuardRailParts(Rule<List<BomData>> rule)
        {
            rule.Define(() =>
            {
                var guardChildren = this.ActiveChildren.Value?.Where(c => c.TypeName == "EndAisleGuard");
                var childBOMs = guardChildren?.Select(c => GetChildBOM(c))?.ToList();

                return childBOMs ?? new List<BomData>() { };
            });
        }

        public Rule<List<BomData>> FirstLevelCustomBuyoutParts { get; set; }
        protected virtual void ConfigureFirstLevelCustomBuyoutParts(Rule<List<BomData>> rule)
        {
            rule.Define(() =>
            {
                var customChildren = this.ActiveChildren.Value?.Where(c => c.IsAdhoc);
                var childBOMs = customChildren?.Select(c => GetChildBOM(c))?.ToList();

                return childBOMs ?? new List<BomData>() { };
            });
        }

        public BomData GetChildBOM(IRuleHost part)
        {
            if (part != null)
            {
                if (part is Ruler.Cpq.IBomPart bomChild && bomChild.Bom.IsActive.Value)
                {
                    return bomChild.Bom;
                }
                else if (part is IHierarchicalRuleHost hhost)
                {
                    if (hhost.ActiveChildren.Value?.FirstOrDefault() is IHierarchicalRuleHost chost)
                    {
                        return GetChildBOM(chost);
                    }
                }
            }

            return null;
        }

        public BomData GetChildBOM(IHierarchicalRuleHost part)
        {
            if (part != null)
            {
                if (part is Ruler.Cpq.IBomPart bomChild && bomChild.Bom.IsActive.Value)
                {
                    return bomChild.Bom;
                }
                else if (part is IHierarchicalRuleHost hhost)
                {
                    if (hhost.ActiveChildren.Value?.FirstOrDefault() is IHierarchicalRuleHost chost)
                    {
                        return GetChildBOM(chost);
                    }
                }
            }

            return null;
        }

        public Rule<IEnumerable<BomData>> SingleRowBomChildren { get; set; }
        protected virtual void ConfigureSingleRowBomChildren(Rule<IEnumerable<BomData>> rule)
        {
            rule.Define(() =>
            {
                var retVal = RowProxyChildren.Value.SelectMany(r => GetSingleRowBomChildren(r));

                if (FirstLevelCustomBuyoutParts.Value.Any())
                {
                    retVal = retVal.Concat(FirstLevelCustomBuyoutParts.Value);
                }
                retVal = retVal.Concat(EndAisleGuardRailParts.Value);
                return retVal;
            });
        }

        
        public Rule<IEnumerable<BomData>> AllRowBomChildren { get; set; }
        protected virtual void ConfigureAllRowBomChildren(Rule<IEnumerable<BomData>> rule)
        {
            //rule.Define(() => RowProxyChildren.Value.SelectMany(r => GetSumBomChildren(r)));
            rule.Define(() =>
            {
                var totalBOMChildren = new List<BomData>();

                for (int i = 0; i < RowQty.Value; i++)
                {
                    totalBOMChildren.AddRange(SingleRowBomChildren.Value);
                }

                return totalBOMChildren;
            });
        }

        [UIRule]
        public ReadOnlyRule<double> RowWeldHours { get; set; }
        protected virtual void ConfigureRowWeldHours(ReadOnlyRule<double> rule)
        {
            rule.Define(() => Math.Round(
            AllRowBomChildren.Value.SelectMany(c => c.LaborItems.Value)
            .Where(li => li.Category == "Weld")
            .Sum(li => li.Hours)
            , 4));
        }

        [UIRule]
        public ReadOnlyRule<double> RowCost { get; set; }
        protected virtual void ConfigureRowCost(ReadOnlyRule<double> rule)
        {
            rule.Define(() => Math.Round(AllRowBomChildren.Value.Sum(c => c.TotalCost.Value), 2));
        }

        [UIRule]
        public ReadOnlyRule<double> RowPrice { get; set; }
        protected virtual void ConfigureRowPrice(ReadOnlyRule<double> rule)
        {
            rule.Define(() => Math.Round(AllRowBomChildren.Value.Sum(c => c.TotalPrice.Value), 2));
        }

        [UIRule]
        public ReadOnlyRule<double> RowMargin { get; set; }
        protected virtual void ConfigureRowMargin(ReadOnlyRule<double> rule)
        {
            rule.Define(() => ((RowPrice.Value - RowCost.Value) / RowPrice.Value) * 100) ;
        }

        private IEnumerable<BomData> GetSumBomChildren(IHierarchicalRuleHost part)
        {
            var bomChildren = new List<BomData>();

            if (part != null)
            {
                foreach (var child in part?.ActiveChildren.Value)
                {
                    if (child is Ruler.Cpq.IBomPart bomChild && bomChild.Bom.IsActive.Value)
                    {
                        for (int i = 0; i < RowQty.Value; i++)
                        {
                            bomChildren.Add(bomChild.Bom);
                        }   
                    }
                    else if (child is IHierarchicalRuleHost hhost)
                    {
                        bomChildren.AddRange(GetSumBomChildren(hhost));
                    }
                }
            }
                
            return bomChildren;
        }

        private IEnumerable<BomData> GetSingleRowBomChildren(IHierarchicalRuleHost part)
        {
            var bomChildren = new List<BomData>();
            
            if (part != null)
            {
                foreach (var child in part.ActiveChildren.Value)
                {
                    if (child is Ruler.Cpq.IBomPart bomChild && bomChild.Bom.IsActive.Value)
                    {
                        bomChildren.Add(bomChild.Bom);
                    }
                    else if (child is IHierarchicalRuleHost hhost)
                    {
                        bomChildren.AddRange(GetSingleRowBomChildren(hhost));
                    }
                }
            }
            
            return bomChildren;
        }

        #endregion

        #region Public Methods

        public void ResetFrameTypesBeforeAndAfter(int bayIndex)
        {
            RowFrame frontFrame = Frames[bayIndex - 1];
            RowFrame rearFrame = Frames[bayIndex];

            if (frontFrame.FrameChoices.Value.Count > 0 && !frontFrame.FrameChoices.Value.Any(f => f.Value.ToString() == frontFrame.FrameName.Value))
                frontFrame.FrameName.Reset();

            if (rearFrame.FrameChoices.Value.Count > 0 && !rearFrame.FrameChoices.Value.Any(f => f.Value.ToString() == rearFrame.FrameName.Value))
                rearFrame.FrameName.Reset();

            //Frames[bayIndex - 1].FrameName.Reset();
            //Frames[bayIndex].FrameName.Reset();
        }

        public void ResetBaysAfter(int frameIndex)
        {
            //reset all bays after the frame line has changed
            Bays.Skip(frameIndex-1).ToList().ForEach(b =>
            {
                if (!b.BayChoices.Value.Any(c => c.Value.ToString() == b.BayName.Value))
                {
                    b.BayName.Reset();
                }
            });

            if (IsBackToBackRow.Value)
            {
                RearBays.Skip(frameIndex).ToList().ForEach(b =>
                {
                    if (!b.BayChoices.Value.Any(c => c.Value.ToString() == b.BayName.Value))
                    {
                        b.BayName.Reset();
                    }
                });
            }
        }

        public Vector3 GetFrontEndAisleGuardPosition(int index, string type)
        {
            var currentLevel = (int)Math.Ceiling((index + 1.0) / EndAisleGuardPiecesPerLevel.Value);
            var xOffset = (type != "eagRear") ? 0.0f : FrontEndAisleGuards[index - 1].TotalPostSpace.Value;
            
            return Helper.Vxy(-xOffset, GetAisleGuardVerticalOffset(currentLevel));
        }

        public Vector3 GetRearEndAisleGuardPosition(int index, string type)
        {
            var currentLevel = (int)Math.Ceiling((index + 1.0) / EndAisleGuardPiecesPerLevel.Value);
            var yOffset = GetAisleGuardVerticalOffset(currentLevel) - (EndAisleGuardChannelSize.Value == "C4x4.5" ? 4 : 6);
            var xOffset = (type != "eagRear") ? 0.0f : -RearEndAisleGuards[index - 1].TotalPostSpace.Value;

            return Helper.Vxyz(xOffset, yOffset, -OverallLength.Value);
        }

        public float GetAisleGuardVerticalOffset(int level)
        {
            switch (level)
            {
                case 1:
                    return EndAisleGuardLevel1HeightAboveGround.Value;
                case 2:
                    return EndAisleGuardLevel2HeightAboveGround.Value;
                case 3:
                    return EndAisleGuardLevel3HeightAboveGround.Value;
                case 4:
                    return EndAisleGuardLevel4HeightAboveGround.Value;
                default:
                    return EndAisleGuardLevel1HeightAboveGround.Value;
            }
        }

        public string GetEndAisleGuardType(int index)
        {
            if (EndAisleGuardPiecesPerLevel.Value == 1) return "eag";
            if (EndAisleGuardPiecesPerLevel.Value == 2)
            {
                //odd index is Front pieces
                if ((index + 1) % 2 != 0) return "eagFront";
                return "eagRear";
            }
            return "eag";
        }

        public List<(string, float)> GetFramelineAisleGuardData(string type, RowFrame rowFrame)
        {
            var framelinePosts = GetFrameLineUprightColumnAssys(rowFrame.ActiveFrame.Value).ToList();
            
            if (type == "eagFront")
            {
                framelinePosts = framelinePosts.Take((int)Math.Ceiling((framelinePosts.Count() / 2.0))).ToList();
            }
            
            if (type != "eagRear")
            {
                return framelinePosts.Select((p, index) => index == 0 ? (GetPostOrientation(p), 0)
                            : (GetPostOrientation(p), Vector3.Distance(p.WorldPosition.Value, framelinePosts[index - 1].WorldPosition.Value))).ToList();
            }
            else
            {
                //get eagRear posts
                framelinePosts = framelinePosts.Skip((int)Math.Ceiling((framelinePosts.Count() / 2.0))-1).ToList();

                return framelinePosts.Select((p, index) => index == 0 ? (GetPostOrientation(p), 0)
                                : (GetPostOrientation(p), Vector3.Distance(p.WorldPosition.Value, framelinePosts[index - 1].WorldPosition.Value))).ToList();
            }
        }

        public string GetPostOrientation(UprightColumnAssembly post)
        {
            return post.WorldRotation.Value.Y > 0 ? "std" : "rev";
        }

        public IEnumerable<UprightColumnAssembly> GetFrameLineUprightColumnAssys(FrameLine frameline)
        {
            return frameline.GetDescendantParts().OfType<UprightColumnAssembly>()?
                                                    .Where(c => c.Parent.IsActive.Value && c.IsActive.Value)?
                                                    .OrderByDescending(r => r.WorldPosition.Value.X);
        }

        #endregion

        #region Private Methods

        public void SetIndex()
        {
            var existingRows = Parent.ActiveChildParts.Value
                .Where(c => c != this && c.TypeName == "Row");
            var index = existingRows.Count() + 1;
            this.Index.SetAdhocDefaultValue(index);
        }

        #endregion

        #region Children

        public ChildList<RowInfo> RowInfo { get; set; }
        protected virtual void ConfigureRowInfo(ChildList<RowInfo> list)
        {
            list.Qty.Define(RowQty);
            list.ConfigureChildren((child, index) =>
            {
                child.RowNumber.Define(() => RowNumbers.Value[index]);
            });
        }

        public ChildList<RowFrame> Frames { get; set; }
        protected virtual void ConfigureFrames(ChildList<RowFrame> list)
        {
            list.Qty.Define(() => BayQty.Value + 1);
            list.ConfigureChildren((child, index) =>
            {
                child.FrameIndex.Define(() => index + 1);
                child.PreviousFrame.Define(() => (child.IsFirstFrame.Value ? default(RowFrame) : list[index - 1]));
                child.PreviousBay.Define(() => (child.IsFirstFrame.Value ? default(RowBay) : Bays[index - 1]));
                child.NextBay.Define(() => (child.FrameIndex.Value == list.Qty.Value ? default(RowBay) : Bays[index]));
                child.Position.Define(() =>
                {
                    if (child.IsFirstFrame.Value)
                        return Helper.Vz(0);
                    else
                    {
                        return Helper.Vz(-(Math.Abs(child.PreviousBay.Value.Position.Value.Z) + child.PreviousBay.Value.OverallWidth.Value));
                    }
                });
            });
        }

        public ChildList<RowBay> Bays { get; set; }
        protected virtual void ConfigureBays(ChildList<RowBay> list)
        {
            list.Qty.Define(BayQty);
            list.ConfigureChildren((child, index) =>
            {
                child.BayIndex.Define(() => index + 1);
                child.IsLastBay.Define(() => child.BayIndex.Value == BayQty.Value);
                child.FirstBay.Define(() => child.IsFirstBay.Value ? null : Bays.First());
                child.PreviousBay.Define(() => (index == 0 ? default(RowBay) : list[index - 1]));
                child.AllAvailableBays.Define(AllAvailableBays);
                child.LeftFrame.Define(() => Frames[index].ActiveFrame.Value);
                child.RightFrame.Define(() => Frames[index + 1].ActiveFrame.Value);
                child.Position.Define(() =>
                {
                    RowFrame frameForPos = Frames[index];
                    return Helper.Vz(-(Math.Abs(frameForPos.Position.Value.Z) + frameForPos.OverallWidth.Value));
                });
            });
        }

        public ChildList<RowBay> RearBays { get; set; }
        protected virtual void ConfigureRearBays(ChildList<RowBay> list)
        {
            list.Qty.Define(() => IsBackToBackRow.Value ? BayQty.Value : 0);
            list.ConfigureChildren((child, index) =>
            {
                child.BayIndex.Define(() => index + 1);
                child.BackToBackFrontBay.Define(() => Bays[index]);
                child.IsLastBay.Define(() => child.BayIndex.Value == BayQty.Value);
                child.FirstBay.Define(() => child.IsFirstBay.Value ? null : RearBays.First());
                child.PreviousBay.Define(() => (index == 0 ? default(RowBay) : list[index - 1]));
                child.AllAvailableBays.Define(AllAvailableBays);
                child.LeftFrame.Define(() => Frames[index].ActiveFrame.Value);
                child.RightFrame.Define(() => Frames[index + 1].ActiveFrame.Value);
                child.Position.Define(() =>
                {
                    var xOffset = 0.0f;
                    RowFrame frameForPos = Frames[index];
                    if (frameForPos.ActiveFrameIsSelective.Value)
                    {
                        xOffset = ((SelectiveFrameLine)frameForPos.ActiveFrame.Value).RearFrameXOffset.Value;
                    }
                    return Helper.Vxz(xOffset, -(Math.Abs(frameForPos.Position.Value.Z) + frameForPos.OverallWidth.Value + child.OverallWidth.Value) );
                });
                child.Rotation.Define(() => Helper.CreateRotation(Vector3.UnitY, 180));
            });
        }

        public ChildList<EndAisleGuard> FrontEndAisleGuards { get; set; }
        protected virtual void ConfigureFrontEndAisleGuards(ChildList<EndAisleGuard> list)
        {
            list.Qty.Define(FrontEndAisleGuardQty);
            list.ConfigureChildren((child, index) =>
            {
                child.Type.Define(() => GetEndAisleGuardType(index));
                child.SpaceInfo.Define(() => GetFramelineAisleGuardData(child.Type.Value, Frames.First()));
                child.EndAisleGuardChannelSize.Define(EndAisleGuardChannelSize);
                child.Position.Define(() => GetFrontEndAisleGuardPosition(index, child.Type.Value));
            });
        }

        public ChildList<EndAisleGuard> RearEndAisleGuards { get; set; }
        protected virtual void ConfigureRearEndAisleGuards(ChildList<EndAisleGuard> list)
        {
            list.Qty.Define(RearEndAisleGuardQty);
            list.ConfigureChildren((child, index) =>
            {
                child.Type.Define(() => GetEndAisleGuardType(index));
                child.SpaceInfo.Define(() => GetFramelineAisleGuardData(child.Type.Value, Frames.Last()));
                child.EndAisleGuardChannelSize.Define(EndAisleGuardChannelSize);
                child.Position.Define(() => GetRearEndAisleGuardPosition(index, child.Type.Value));
                child.Rotation.Define(() => Helper.CreateRotation(Vector3.UnitX, 180));
            });
        }

        public ChildList<Elevation> Elevations { get; set; }
        protected virtual void ConfigureElevations(ChildList<Elevation> list)
        {
            list.Qty.Define(BayQty);
            list.ConfigureChildren((child, index) =>
            {
                child.RenderInCad.Define(false);
                child.FrontFrame.Define(() => Frames[index].ActiveFrame.Value);
                child.Bay.Define(() => Bays[index].ActiveBay.Value);
                child.RearBay.Define(() => IsBackToBackRow.Value ? RearBays[index].ActiveBay.Value : default(Bay));
                child.RearBayXOffset.Define(() => IsBackToBackRow.Value ? Math.Abs(RearBays[index].Position.Value.X) : 0);
                child.RearFrame.Define(() => Frames[index + 1].ActiveFrame.Value);
            });
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(Colors.Gray);
        }

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define("Row");
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(true);
        }

        protected override void ConfigureAllowedChildTypes(ReadOnlyRule<IEnumerable<Type>> rule)
        {
            rule.Define(() => new List<Type>
            {
                typeof(CustomPart),
                typeof(BuyoutPart),
                typeof(FrameUpright),
                typeof(UprightColumnAssembly),
                typeof(FrameTie),
                typeof(ShelfLoadBeam),
                typeof(ShelfBeamCrossbar),
                typeof(ShelfPalletStop),
                typeof(Shim),
                typeof(CapacitySignKit),
                typeof(RackInstallGuide),
                typeof(TouchUpCan)
            });
        }

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureRevisablePart();
            this.ConfigureBomPartRules();
        }
        public override bool IsCadPart => RowQty.Value > 0;

        #endregion

        #region Graphics

        //public Block RowBlock { get; set; }
        //protected virtual void ConfigureRowBlock(Block geo)
        //{
        //    geo.Width.Define(Depth);
        //    geo.Height.Define(Height);
        //    geo.Depth.Define(() => Width);
        //    geo.Position.Define(() => Helper.Vy(Height.Value / 2));
        //}

        public UCS UCS { get; set; }
        protected virtual void ConfigureUCS(UCS child)
        {
            child.Position.Define(() => (Helper.Vy(0)));
        }

        #endregion

    }
}
