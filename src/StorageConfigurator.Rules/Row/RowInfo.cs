﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;

namespace StorageConfigurator.Rules
{
    public class RowInfo : Part
    {
        #region Constructors

        public RowInfo(ModelContext modelContext) : base(modelContext) { }

        public RowInfo(string name, Part parent) : base(name, parent) { }

        #endregion

        #region Rules

        [UIRule]
        public Rule<string> RowNumber { get; set; }
        protected virtual void ConfigureRowNumber(Rule<string> rule)
        {
            rule.Define(default(string));
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(false);
        }

        #endregion

    }
}
