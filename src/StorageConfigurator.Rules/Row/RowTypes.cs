﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ruler.Rules;

namespace StorageConfigurator.Rules
{
    public class RowTypes : Part
    {
        #region Constructors

        public RowTypes(ModelContext modelContext) : base(modelContext) { }

        public RowTypes(string name, Part parent) : base(name, parent) { }

        #endregion

        #region Rules

        [UIRule]
        public ReadOnlyRule<int> RowTypeQty { get; set; }
        protected virtual void ConfigureRowTypeQty(ReadOnlyRule<int> rule)
        {
            rule.Define(() => RowList.Value.Count);
        }

        public Rule<List<Row>> RowList { get; set; }
        protected virtual void ConfigureRowList(Rule<List<Row>> rule)
        {
            rule.Define(() =>
            {
                List<Row> retVal = new List<Row>();

                if (ActiveChildParts.Value != null && ActiveChildParts.Value.Any())
                    retVal = ActiveChildParts.Value.Where(c => c.GetType() == typeof(Row))
                                                    .Select(p => (Row)p).ToList();

                return retVal;
            });
        }

        [CadRule(CadRuleUsageType.Property)]
        public LookupRule<string> LineItemName { get; set; }
        protected virtual void ConfigureLineItemName(LookupRule<string> rule)
        {
            rule.Define(() => $"[[LineItemName]]");
        }

        protected override void ConfigureAllowedChildTypes(ReadOnlyRule<IEnumerable<Type>> rule)
        {
            rule.Define(() => new List<Type>
            {
                typeof(Row)
            });
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define("RowContainer");
        }

        public override bool IsCadPart => true;

        #endregion

    }
}
