﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using Ruler.Rules;
using Ruler.Rules.Graphics;

namespace StorageConfigurator.Rules
{
    public class RowBay : BoundingCube, IRevisablePart
    {
        #region Constructors

        public RowBay(ModelContext modelContext) : base(modelContext) { }

        public RowBay(string name, Part parent) : base(name, parent) { }

        #endregion

        #region Rules

        public LookupRule<Row> RowComp { get; set; }

        [UIRule]
        public Rule<int> BayIndex { get; set; }
        protected virtual void ConfigureBayIndex(Rule<int> rule)
        {
            rule.Define(default(int));
        }

        public Rule<bool> IsFirstBay { get; set; }
        protected virtual void ConfigureIsFirstBay(Rule<bool> rule)
        {
            rule.Define(() => BayIndex.Value == 1);
        }

        public Rule<bool> IsLastBay { get; set; }
        protected virtual void ConfigureIsLastBay(Rule<bool> rule)
        {
            rule.Define(default(bool));
        }

        public Rule<RowBay> PreviousBay { get; set; }
        protected virtual void ConfigurePreviousBay(Rule<RowBay> rule)
        {
            rule.Define(default(RowBay));
        }

        public Rule<RowBay> FirstBay { get; set; }
        protected virtual void ConfigureFirstBay(Rule<RowBay> rule)
        {
            rule.Define(default(RowBay));
        }

        public Rule<RowBay> BackToBackFrontBay { get; set; }
        protected virtual void ConfigureBackToBackFrontBay(Rule<RowBay> rule)
        {
            rule.Define(default(RowBay));
        }

        public Rule<bool> IsRearBackToBackBay { get; set; }
        protected virtual void ConfigureIsRearBackToBackBay(Rule<bool> rule)
        {
            rule.Define(() => BackToBackFrontBay.Value != default(RowBay));
        }

        public Rule<bool> IsInBackToBackBay { get; set; }
        protected virtual void ConfigureIsInBackToBackBay(Rule<bool> rule)
        {
            rule.Define(() => LeftFrame.Value.IsSelectiveFrameLine.Value ? ((SelectiveFrameLine)LeftFrame.Value).IsBackToBack.Value : false);
        }

        public Rule<bool> IsEmptyBay { get; set; }
        protected virtual void ConfigureIsEmptyBay(Rule<bool> rule)
        {
            rule.Define(() => !string.IsNullOrWhiteSpace(BayName.Value) && BayName.Value.ToLower() == "empty");
        }

        public Rule<string> BayName { get; set; }
        protected virtual void ConfigureBayName(Rule<string> rule)
        {
            rule.Define(() =>
            {
                if (IsFirstBay.Value || PreviousBay.Value == null)
                {
                    return BayChoices.Value.Any() ? BayChoices.Value.First().Value.ToString() : "";
                }
                else
                {
                    if (PreviousBay.Value.IsEmptyBay.Value)
                    {
                        if (IsRearBackToBackBay.Value && !Helper.KeyExistsInChoices(BayChoices.Value, PreviousBay.Value.PreviousBay.Value.BayName.Value))
                        {
                            return BackToBackFrontBay.Value.BayName.Value;
                        }
                        return PreviousBay.Value.PreviousBay.Value.BayName.Value;
                    }
                    else
                    {
                        if (IsRearBackToBackBay.Value && !Helper.KeyExistsInChoices(BayChoices.Value, PreviousBay.Value.BayName.Value))
                        {
                            return BackToBackFrontBay.Value.BayName.Value;
                        }
                        return PreviousBay.Value.BayName.Value;
                    }
                }
            });
            rule.AddToUI(ui =>
            {
                ui.Label.Define(() =>
                {
                    if (IsInBackToBackBay.Value)
                    {
                        return IsRearBackToBackBay.Value ? "Left Bay Name" : "Right Bay Name";
                    }
                    return "Bay Name";
                });
                ui.ChoiceList.Define(() =>
                {
                    if (BayChoices.Value.Any())
                        return BayChoices.Value;
                    else
                        return new List<ChoiceListItem>();
                });
            });
            //rule.ValueSet += (sender, e) =>
            //{
            //    if (RowComp != null && RowComp.Value != null && !string.IsNullOrEmpty(e.OldValue) && (e.NewValue != e.OldValue))
            //    {
            //        RowComp.Value.ResetFrameTypesBeforeAndAfter(BayIndex.Value);
            //    }
            //};
        }

        [UIRule]
        public ReadOnlyRule<bool> IsDoubleDeep { get; set; }
        protected virtual void ConfigureIsDoubleDeep(ReadOnlyRule<bool> rule)
        {
            rule.Define(() => ActiveBay.Value == null ? false : ActiveBay.Value.IsDoubleDeep.Value);
        }

        [UIRule]
        public ReadOnlyRule<string> DoubleDeepType { get; set; }
        protected virtual void ConfigureDoubleDeepType(ReadOnlyRule<string> rule)
        {
            rule.Define(() => ActiveBay.Value == null ? "frameFrame" : ActiveBay.Value.DoubleDeepType.Value);
        }

        public Rule<List<Bay>> AllAvailableBays { get; set; }
        protected virtual void ConfigureAllAvailableBays(Rule<List<Bay>> rule)
        {
            rule.Define(() => new List<Bay>());
        }

        public Rule<List<Bay>> FilteredBays { get; set; }
        protected virtual void ConfigureFilteredBays(Rule<List<Bay>> rule)
        {
            rule.Define(() =>
            {
                if (LeftFrame.Value.GetType() == typeof(SelectiveFrameLine))
                {
                    var selLeftFrame = (SelectiveFrameLine)LeftFrame.Value;

                    if (IsRearBackToBackBay.Value)
                    {
                        return AllAvailableBays.Value?.Where(b =>
                        {
                            return (selLeftFrame.RearFrameIsDoubleDeep.Value == b.IsDoubleDeep.Value &&
                                selLeftFrame.RearFrameIsDoubleDeepPost.Value == b.IsDoubleDeepPost.Value &&
                                selLeftFrame.RearFrame.Value.OverallDepth.Value == b.OverallDepth.Value &&
                                selLeftFrame.RearFrameUprightDepth.Value == b.FrontFrameDepth.Value &&
                                BackToBackFrontBay.Value.OverallWidth.Value == b.BayWidth.Value);
                        })?.ToList() ?? new List<Bay>();
                    }
                    else
                    {
                        return AllAvailableBays.Value?.Where(b =>
                        {
                            return (selLeftFrame.FrontFrameIsDoubleDeep.Value == b.IsDoubleDeep.Value &&
                                selLeftFrame.FrontFrameIsDoubleDeepPost.Value == b.IsDoubleDeepPost.Value &&
                                selLeftFrame.FrontFrame.Value.OverallDepth.Value == b.OverallDepth.Value &&
                                selLeftFrame.FrontFrameUprightDepth.Value == b.FrontFrameDepth.Value);
                        })?.ToList() ?? new List<Bay>();
                    }
                }
                else if (AllAvailableBays.Value != null && AllAvailableBays.Value.Any())
                {
                    return AllAvailableBays.Value;
                }

                return new List<Bay>();
            });
        }

        public Rule<List<ChoiceListItem>> BayChoices { get; set; }
        protected virtual void ConfigureBayChoices(Rule<List<ChoiceListItem>> rule)
        {
            rule.Define(() =>
            {
                List<ChoiceListItem> retList = new List<ChoiceListItem>();

                if (FilteredBays.Value != null)
                {
                    //TODO: Figure out what ID we can use to avoid errors when customer deletes a bay or changes a name.
                    FilteredBays.Value.ForEach(b => retList.Add(new ChoiceListItem(b.Path, b.BayName.Value)));

                    if (!IsFirstBay.Value && !IsLastBay.Value)
                    {
                        retList.Add(new ChoiceListItem("empty", "Empty"));
                    }
                }

                return retList;
            });
        }

        public Rule<Bay> ActiveBay { get; set; }
        protected virtual void ConfigureActiveBay(Rule<Bay> rule)
        {
            rule.Define(() =>
            {
                Bay retVal = null;

                if (!IsEmptyBay.Value && BayChoices.Value.Any() && 
                BayChoices.Value.Any(p => p.Value.ToString() == BayName.Value))
                {
                    retVal = AllAvailableBays.Value.FirstOrDefault(p => p.Path == BayName.Value);
                }
                    
                return retVal;
            });
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(() => ActiveBay.Value != null);
                cad.CadRuleUsageType = CadRuleUsageType.ChildProxy;
                cad.ChildProxyMatrixFactory = () => Matrix4x4.CreateTranslation(Helper.Vz(0));
            });
        }

        public ReadOnlyRule<FrameLine> LeftFrame { get; set; }
        protected virtual void ConfigureLeftFrame(ReadOnlyRule<FrameLine> rule)
        {
            rule.Define(default(FrameLine));
        }

        public ReadOnlyRule<FrameLine> RightFrame { get; set; }
        protected virtual void ConfigureRightFrame(ReadOnlyRule<FrameLine> rule)
        {
            rule.Define(default(FrameLine));
        }

        public ReadOnlyRule<List<(ShelfLoadBeam, UprightColumnAssembly)>> BeamColumnConnections { get; set; }
        protected virtual void ConfigureBeamColumnConnections(ReadOnlyRule<List<(ShelfLoadBeam, UprightColumnAssembly)>> rule)
        {
            rule.Define(() =>
            {
                var retVal = new List<(ShelfLoadBeam, UprightColumnAssembly)>();

                if (ActiveBay.Value != null && LeftFrame.Value != default(FrameLine) && RightFrame.Value != default(FrameLine))
                {
                    var leftFrameColumns = GetFrameLineUprightColumnAssys(LeftFrame.Value, IsRearBackToBackBay.Value);
                    var rightFrameColumns = GetFrameLineUprightColumnAssys(RightFrame.Value, IsRearBackToBackBay.Value);
                    var loadBeams = Enumerable.Empty<ShelfLoadBeam>();
                    if (ActiveBay.Value.IsDoubleDeep.Value)
                    {
                        loadBeams = ActiveBay.Value.DoubleDeepLevel.SelectMany(b => b.GetDescendantParts().OfType<ShelfLoadBeam>())?
                                                                   .Where(s => s.IsActive.Value)?
                                                                   .OrderByDescending(l => l.WorldPosition.Value.X);
                    }
                    else
                    {
                        loadBeams = ActiveBay.Value.SelectiveLevel.SelectMany(b => b.GetDescendantParts().OfType<ShelfLoadBeam>())?
                                                                   .Where(s => s.IsActive.Value)?
                                                                   .OrderByDescending(l => l.WorldPosition.Value.X);
                    }

                    // Check to see if the X location is within a 6" range between the load beam and post
                    retVal = loadBeams.SelectMany(b =>
                    {
                        var retList = new List<(ShelfLoadBeam, UprightColumnAssembly)>();

                        retList = leftFrameColumns.Where(f => IsInRange(f.WorldPosition.Value.X, b.WorldPosition.Value.X))?
                                                  .Select(c => (b, c))?
                                                  .ToList();
                        
                        retList.AddRange(rightFrameColumns.Where(f => IsInRange(f.WorldPosition.Value.X, b.WorldPosition.Value.X))?
                                                  .Select(c => (b, c))?
                                                  .ToList());

                        return retList;
                    })?.ToList();
                    
                }

                return retVal;
            });
        }

        public bool IsInRange(float numToCompare, float location, float range = 6)
        {
            return numToCompare >= (location - (range / 2)) && numToCompare <= (location + (range / 2));
        }


        public IEnumerable<UprightColumnAssembly> GetFrameLineUprightColumnAssys(FrameLine frameline, bool getRearIfBackToBack = false)
        {
            if (frameline.IsSelectiveFrameLine.Value && ((SelectiveFrameLine)frameline).IsBackToBack.Value)
            {
                if (getRearIfBackToBack)
                {
                    return GetActivePostAssemblies(frameline.UsedFrameTypes.Value?.LastOrDefault());
                }
                return GetActivePostAssemblies(frameline.UsedFrameTypes.Value?.FirstOrDefault());
            }
            return frameline.UsedFrameTypes.Value?.SelectMany(f => GetActivePostAssemblies(f));
            //return frameline.UsedFrameTypes.Value?
            //                            .SelectMany(f => f.GetDescendantParts().OfType<UprightColumnAssembly>())?
            //                            .Where(c => c.Parent.IsActive.Value && c.IsActive.Value)?
            //                            .OrderBy(r => r.Position.Value.X);
        }

        public IEnumerable<UprightColumnAssembly> GetActivePostAssemblies(FrameType fType)
        {
            return fType.GetDescendantParts().OfType<UprightColumnAssembly>()?
                                        .Where(c => c.Parent.IsActive.Value && c.IsActive.Value)?
                                        .OrderByDescending(r => r.WorldPosition.Value.X);
        }

        public ReadOnlyRule<List<BoltDefinition>> BoltDefinitions { get; set; }
        protected virtual void ConfigureBoltDefinitions(ReadOnlyRule<List<BoltDefinition>> rule)
        {
            rule.Define(() =>
            {
                var definitions = BeamColumnConnections.Value.Select(c => GetBoltDefinition(c.Item1.LeftEndBracket, c.Item2)).ToList();
                var consolidatedDefs = definitions
                    .Distinct()
                    .Select(def => new BoltDefinition()
                    {
                        BoltSize = def.BoltSize,
                        BoltType = def.BoltType,
                        Length = def.Length,
                        HasWashers = def.HasWashers,
                        WasherType = def.WasherType,
                        WasherMaterial = def.WasherMaterial,
                        Qty = definitions.Where(d => d == def).Sum(d => d.Qty)
                    });
                return consolidatedDefs.ToList();
            });
        }

        public ReadOnlyRule<List<BoltDefinition>> UniqueBoltDefinitions { get; set; }
        protected virtual void ConfigureUniqueBoltDefinitions(ReadOnlyRule<List<BoltDefinition>> rule)
        {
            rule.Define(() =>
            {
                var retVal = new List<BoltDefinition>();

                if (BoltDefinitions.Value != null && BoltDefinitions.Value.Any())
                {
                    var hardwareKitGroups = BoltDefinitions.Value.GroupBy(k => new
                    {
                        BoltSize = k.BoltSize,
                        BoltType = k.BoltType,
                        BoltMaterial = k.BoltMaterial,
                        Length = k.Length,
                        NutType = k.NutType,
                        NutMaterial = k.NutMaterial,
                        HasWashers = k.HasWashers,
                        WasherMaterial = k.WasherMaterial
                    });

                    retVal = hardwareKitGroups.Select(g =>
                    {
                        var kit = g.First();
                        kit.Qty = g.Sum(k => k.Qty);
                        return kit;
                    }).ToList();
                }

                return retVal;
            });
        }

        #endregion

        #region IRevisablePartImplementation

        public FactoryRevisionRule<string> FactoryRevision { get; set; }

        #endregion

        #region Children

        public ChildList<HardwareGroup> Hardwares { get; set; }
        protected virtual void ConfigureHardwares(ChildList<HardwareGroup> list)
        {
            list.Qty.Define(() => UniqueBoltDefinitions.Value.Count);
            list.ConfigureChildren((child, index) =>
            {
                child.BoltSize.Define(() => UniqueBoltDefinitions.Value[index].BoltSize);
                child.BoltLength.Define(() => UniqueBoltDefinitions.Value[index].Length);
                child.BoltType.Define(() => UniqueBoltDefinitions.Value[index].BoltType);
                child.Qty.Define(() => UniqueBoltDefinitions.Value[index].Qty);
                child.HasWashers.Define(() => UniqueBoltDefinitions.Value[index].HasWashers);
                child.WasherType.Define(() => UniqueBoltDefinitions.Value[index].WasherType);
                child.WasherMaterial.Define(() => UniqueBoltDefinitions.Value[index].WasherMaterial);
            });
        }

        #endregion

        #region Overrides

        protected override void ConfigureOverallDepth(Rule<float> rule)
        {
            rule.Define(() => ActiveBay.Value == null ? 1 : ActiveBay.Value.BayDepth.Value);
            rule.AddToUI(ui =>
            {
                ui.Label.Define("Bay Depth");
                ui.ReadOnly.Define(true);
            });
        }

        protected override void ConfigureOverallWidth(Rule<float> rule)
        {
            rule.Define(() => 
            {
                if (IsEmptyBay.Value)
                {
                    return 100;
                }
                else if (ActiveBay.Value == null)
                {
                    return 1;
                }
                else
                {
                    return ActiveBay.Value.BayWidth.Value;
                }
                });
            rule.AddToUI(ui =>
            {
                ui.Label.Define("Bay Width");
                ui.ReadOnly.Define(() => !IsEmptyBay.Value);
            });
        }

        protected override void ConfigureOverallHeight(Rule<float> rule)
        {
            rule.Define(() => ActiveBay.Value == null ? 1 : ActiveBay.Value.BayHeight.Value);
            rule.AddToUI(ui =>
            {
                ui.Label.Define("Bay Height");
                ui.ReadOnly.Define(true);
            });
        }

        protected override void ConfigureCubeColor(Rule<int> rule)
        {
            //orange
            rule.Define(16753920);
        }

        protected override void ConfigureGraphicsActive(Rule<bool> rule)
        {
            rule.Define(() => !IsEmptyBay.Value);
        }

        #endregion

        #region Part Overrides

        public override bool IsCadPart => !IsEmptyBay.Value;

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(CubeColor);
            material.Opacity.Define(0.5f);
        }

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define(() => IsEmptyBay.Value ? "" : "RowBay");
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(false);
        }

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureRevisablePart();
        }

        #endregion

        #region Graphics



        #endregion

        #region Helper Methods

        private BoltDefinition GetBoltDefinition(ShelfBeamEndBracket clip, UprightColumnAssembly column)
        {
            var qty = clip.Height.Value > 9 ? 3 : 2;

            var doublerHeight = column.HasDoubler.Value ? column.DoublerHeight.Value : 0;
            var doubler = doublerHeight >= clip.WorldPosition.Value.Y;
            var length = 0f;
            var hasWashers = false;

            if(column.ColumnChannelWidth.Value <= 4)
            {
                length = doubler ? 4 : 1;
            }
            else
            {
                length = doubler ? 4.5f : 1.25f;
                hasWashers = !doubler;
            }

            return new BoltDefinition()
            {
                Qty = qty,
                BoltSize = 0.5f,
                BoltType = BoltTypes.Hex,
                Length = length,
                HasWashers = hasWashers,
                WasherType = WasherTypes.Clipped,
                WasherMaterial = HardwareMaterial.ZincPlated
            };
        }

        #endregion


    }
}
