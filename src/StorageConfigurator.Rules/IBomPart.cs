﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;
using Ruler.Rules.Validation;
using System.Linq;

namespace StorageConfigurator.Rules
{
    public interface IBomPart : IColorComponent
    {
        Rule<string> DatabasePartNumber { get; set; }
        PartNumberRule<string> PartNumber { get; set; }
        Rule<string> PartNumberPrefix { get; set; }
        Rule<string> Description { get; set; }

        //This will be a pipe delimited list which will be split to separate lines in the BOM Sheet output.
        Rule<string> BOMDescription { get; set; }
        Rule<string> BOMDescription1 { get; set; }
        Rule<string> BOMDescription2 { get; set; }
        Rule<string> BOMDescription3 { get; set; }
        Rule<string> BOMDescription4 { get; set; }
        Rule<string> BOMDescription5 { get; set; }
        Rule<string> BOMDescription6 { get; set; }
        Rule<string> BOMDescription7 { get; set; }
        Rule<string> BOMDescription8 { get; set; }
        Rule<string> BOMDescription9 { get; set; }
        Rule<string> BOMDescription10 { get; set; }
        Rule<string> BOMDescription11 { get; set; }

    }

    public static class IBomPartExtensions
    {
        public static void ConfigureBomPartRules(this IBomPart comp)
        {
            comp.DatabasePartNumber.Define(string.Empty);
            comp.PartNumberPrefix.Define(() => string.Empty);
            comp.PartNumber.Define(() =>
            {
                if (!string.IsNullOrWhiteSpace(comp.PartNumberPrefix.Value))
                {
                    var pnService = comp.PartNumber.PartNumberService;
                    return pnService.GetOrAddPartNumberByHash(comp.PartNumberPrefix.Value, ((Part)comp).CadHash.Value);
                }
                else if (!string.IsNullOrWhiteSpace(comp.DatabasePartNumber.Value))
                {
                    return comp.DatabasePartNumber.Value;
                }
                else
                {
                    return string.Empty;
                }
            });
            comp.PartNumber.AddToCAD(cad =>
            {
                cad.CadRuleUsageType = CadRuleUsageType.Property;
                cad.NameInCad = "Part Number";
            });
            comp.PartNumber.AddToUI();

            comp.Description.Define("");
            comp.Description.AddToCAD(cad =>
            {
                cad.CadRuleUsageType = CadRuleUsageType.Property;
            });

            comp.BOMDescription.Define("");
            //comp.BOMDescription.Define(() => comp is Part part ? part.DisplayName.Value : ""); //Display name is temporary while waiting on logic
            //comp.BOMDescription.AddToCAD(cad =>
            //{
            //    cad.CadRuleUsageType = CadRuleUsageType.Property;
            //});

            comp.BOMDescription1.Define(() => GetBomDescription(comp.BOMDescription.Value, 1));
            comp.BOMDescription1.AddToCAD(cad => cad.CadRuleUsageType = CadRuleUsageType.Property);

            comp.BOMDescription2.Define(() => GetBomDescription(comp.BOMDescription.Value, 2));
            comp.BOMDescription2.AddToCAD(cad => cad.CadRuleUsageType = CadRuleUsageType.Property);

            comp.BOMDescription3.Define(() => GetBomDescription(comp.BOMDescription.Value, 3));
            comp.BOMDescription3.AddToCAD(cad => cad.CadRuleUsageType = CadRuleUsageType.Property);

            comp.BOMDescription4.Define(() => GetBomDescription(comp.BOMDescription.Value, 4));
            comp.BOMDescription4.AddToCAD(cad => cad.CadRuleUsageType = CadRuleUsageType.Property);

            comp.BOMDescription5.Define(() => GetBomDescription(comp.BOMDescription.Value, 5));
            comp.BOMDescription5.AddToCAD(cad => cad.CadRuleUsageType = CadRuleUsageType.Property);

            comp.BOMDescription6.Define(() => GetBomDescription(comp.BOMDescription.Value, 6));
            comp.BOMDescription6.AddToCAD(cad => cad.CadRuleUsageType = CadRuleUsageType.Property);

            comp.BOMDescription7.Define(() => GetBomDescription(comp.BOMDescription.Value, 7));
            comp.BOMDescription7.AddToCAD(cad => cad.CadRuleUsageType = CadRuleUsageType.Property);

            comp.BOMDescription8.Define(() => GetBomDescription(comp.BOMDescription.Value, 8));
            comp.BOMDescription8.AddToCAD(cad => cad.CadRuleUsageType = CadRuleUsageType.Property);

            comp.BOMDescription9.Define(() => GetBomDescription(comp.BOMDescription.Value, 9));
            comp.BOMDescription9.AddToCAD(cad => cad.CadRuleUsageType = CadRuleUsageType.Property);

            comp.BOMDescription10.Define(() => GetBomDescription(comp.BOMDescription.Value, 10));
            comp.BOMDescription10.AddToCAD(cad => cad.CadRuleUsageType = CadRuleUsageType.Property);

            comp.BOMDescription11.Define(() => GetBomDescription(comp.BOMDescription.Value, 11));
            comp.BOMDescription11.AddToCAD(cad => cad.CadRuleUsageType = CadRuleUsageType.Property);

            comp.ColorShortDescription.AddToCAD(cad =>
            {
                cad.CadRuleUsageType = CadRuleUsageType.Property;
                cad.NameInCad = "Color";
            });

            comp.ConfigureColorComponent();
        }

        private static string GetBomDescription(string descConcat, int lineNum)
        {
            var split = descConcat.Split('|');
            if(split.Length >= lineNum)
            {
                return split[lineNum - 1];
            }
            return null;
        }
    }
}
