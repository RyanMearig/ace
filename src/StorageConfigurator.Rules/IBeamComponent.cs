﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;

namespace StorageConfigurator.Rules
{
    public interface IBeamComponent
    {
        Rule<string> BeamSize { get; set; }
        Rule<string> BeamType { get; set; }
    }
}
