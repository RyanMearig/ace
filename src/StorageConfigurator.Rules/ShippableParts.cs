﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ruler.Cpq;
using Ruler.Rules;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class ShippableParts : ASLineItemPart, IRevisablePart, IBomPart, Ruler.Cpq.IBomPart
    {
        #region Constructors

        public ShippableParts(ModelContext modelContext, StorageConfiguratorRepository repo) : base(modelContext)
        {
            Repository = repo;
        }

        public ShippableParts(string name, Part parent, StorageConfiguratorRepository repo) : base(name, parent)
        {
            Repository = repo;
        }

        #endregion

        #region Rules

        [UIRule("Name")]
        public Rule<string> ShippablePartsName { get; set; }
        protected virtual void ConfigureShippablePartsName(Rule<string> rule)
        {
            rule.Define("Parts");
        }

        [CadRule(CadRuleUsageType.Property)]
        public Rule<string> FOBFactory { get; set; }
        protected virtual void ConfigureFOBFactory(Rule<string> rule)
        {
            rule.Define(() => Bom.PlantCode.UIMetadata.ChoiceList.Value.FirstOrDefault(c => c.Value.ToString() == Bom.PlantCode.Value)?.Text);
        }

        #endregion

        #region IRevisablePartImplementation

        public FactoryRevisionRule<string> FactoryRevision { get; set; }

        #endregion

        #region IBomPart Implementation

        public Rule<string> DatabasePartNumber { get; set; }
        public PartNumberRule<string> PartNumber { get; set; }
        protected virtual void ConfigurePartNumber(PartNumberRule<string> rule)
        {
            rule.Define(() => LineItemName.Value);
        }
        public Rule<string> PartNumberPrefix { get; set; }
        public Rule<string> Description { get; set; }
        protected virtual void ConfigureDescription(Rule<string> rule)
        {
            rule.Define("Parts");
        }

        public Rule<string> BOMDescription { get; set; }
        protected virtual void ConfigureBOMDescription(Rule<string> rule)
        {
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(false);
            });
        }

        public Rule<string> BOMDescription1 { get; set; }
        public Rule<string> BOMDescription2 { get; set; }
        public Rule<string> BOMDescription3 { get; set; }
        public Rule<string> BOMDescription4 { get; set; }
        public Rule<string> BOMDescription5 { get; set; }
        public Rule<string> BOMDescription6 { get; set; }
        public Rule<string> BOMDescription7 { get; set; }
        public Rule<string> BOMDescription8 { get; set; }
        public Rule<string> BOMDescription9 { get; set; }
        public Rule<string> BOMDescription10 { get; set; }
        public Rule<string> BOMDescription11 { get; set; }

        public Rule<string> Color { get; set; }
        public ReadOnlyRule<AdvanceColor> AdvanceColor { get; set; }
        public ReadOnlyRule<string> ColorDescription { get; set; }
        public Rule<string> ColorBOMName { get; set; }
        public ReadOnlyRule<string> ColorShortDescription { get; set; }

        public Rule<double> PartWeight { get; set; }
        public Rule<double> PartPaintWeight { get; set; }
        public Rule<float> WeightPerGallon { get; set; }
        public CsiRule<double> PaintCostPerGallon { get; set; }
        public Rule<float> PaintCost { get; set; }

        #endregion

        #region Ruler.Cpq.IBomPart Implementation

        public BomData Bom { get; set; }
        protected virtual void ConfigureBom(BomData child)
        {
            child.ShortDescription.Define(ShippablePartsName);
            //child.ItemMarginPercent.Define(DefaultMarginPercent);
            child.PartNumber.Define(PartNumber);
            child.PlantCode.Define("GA_C");
        }

        protected override void ConfigureBOMChildren(Rule<IEnumerable<BomData>> rule)
        {
            rule.Define(() => Bom.BomChildren.Value);
        }

        protected override void ConfigureLineItemName(Rule<string> rule)
        {
            rule.Define(() => $"{ProjectNumber.Value}.{this.TypeName}");
        }

        public Rule<double> DefaultMarginPercent { get; set; }
        protected virtual void ConfigureDefaultMarginPercent(Rule<double> rule)
        {
            rule.Define(default(double));
        }

        #endregion

        #region Part Overrides

        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define(() => ActiveChildParts.Value.Count() > 0 ? "ShippableParts" : "");
        }

        protected override void ConfigureAllowedChildTypes(ReadOnlyRule<IEnumerable<Type>> rule)
        {
            rule.Define(() => new List<Type>
            {
                typeof(FrameUprightKit),
                typeof(UprightColumnAssembly),
                typeof(FrameTie),
                typeof(ShelfLoadBeam),
                typeof(ShelfBeamCrossbar),
                typeof(ShelfPalletStop),
                typeof(Shim),
                typeof(CapacitySignKit),
                typeof(RackInstallGuide),
                typeof(CustomPart),
                typeof(BuyoutPart),
                typeof(TouchUpCan),
                typeof(AnchorBolt),
                typeof(Nut),
                typeof(Bolt),
                typeof(Washers),
                typeof(EndAisleGuard)
            });
        }

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureRevisablePart();
        }

        #endregion

    }
}
