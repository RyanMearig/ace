﻿using System;
using System.Collections.Generic;
using System.Text;
using Ruler.Rules;
using Ruler.Rules.Validation;
using Ruler.Rules.Graphics;
using StorageConfigurator.Data;

namespace StorageConfigurator.Rules
{
    public class Channel : AdvancePart, IChannel, IBomPart, IRevisablePart
    {
        #region Constructors

        public Channel(ModelContext modelContext, StorageConfiguratorRepository repo) : base(modelContext)
        {
            Repository = repo;
        }

        public Channel(string name, Part parent, StorageConfiguratorRepository repo) : base(name, parent)
        {
            Repository = repo;
        }
        #endregion

        #region Rules

        public Rule<float> Length { get; set; }
        protected virtual void ConfigureLength(Rule<float> rule)
        {
            rule.Define(default(float));
            rule.AddToCAD(cad =>
            {
                cad.NameInCad = "B_L";
            });
        }

        [CadRule(CadRuleUsageType.Property)]
        public ReadOnlyRule<float> KSI { get; set; }
        protected virtual void ConfigureKSI(ReadOnlyRule<float> rule)
        {
            rule.Define(() =>
            {
                if(ChannelWidth.Value <= 4)
                {
                    return 50;
                }
                else if (ChannelWidth.Value <= 6)
                {
                    return 42;
                }
                else
                {
                    return 36;
                }
            });
        }

        public Dictionary<string, string> ChannelIds = new Dictionary<string, string>()
        {
            { "C3x3.5", "A"},
            { "C3x4.1", "B"},
            { "C4x4.5", "C"},
            { "C4x5.4", "D"},
            { "C5x6.1", "EL"},
            { "C5x6.7", "E"},
            { "C6x8.2", "F"},
            { "C7x9.8", "G"},
            { "C8x11.5", "H"}
        };
        public ReadOnlyRule<string> ChannelId { get; set; }
        protected virtual void ConfigureChannelId(ReadOnlyRule<string> rule)
        {
            rule.Define(() => ChannelIds.TryGetValue(ChannelSize.Value, out var id) ? id : string.Empty);
        }

        public Rule<string> DescriptionPrefix { get; set; }
        protected virtual void ConfigureDescriptionPrefix(Rule<string> rule)
        {
            rule.Define(default(string));
        }

        //This is the description used only by the BOM Summary.
        public Rule<string> BOMUIDescription { get; set; }
        protected virtual void ConfigureBOMUIDescription(Rule<string> rule)
        {
            rule.Define(() => $"{DescriptionPrefix.Value}({ChannelSize.Value}, {Helper.fractionString(Length.Value, feetToo: false)})");
        }

        #endregion

        #region IChannel Implementation

        public Rule<List<ChannelData>> AllChannelData { get; set; }
        public Rule<List<ChoiceListItem>> AllChannelSizesChoiceList { get; set; }
        public Rule<ChannelData> ActiveChannelData { get; set; }
        public Rule<string> ChannelSize { get; set; }
        public Rule<float> ChannelWidth { get; set; }
        protected virtual void ConfigureChannelWidth(Rule<float> rule)
        {
            rule.AddToCAD(cad =>
            {
                cad.NameInCad = "G_H";
            });
        }
        public Rule<float> ChannelDepth { get; set; }
        protected virtual void ConfigureChannelDepth(Rule<float> rule)
        {
            rule.AddToCAD(cad =>
            {
                cad.NameInCad = "G_W";
            });
        }
        public Rule<float> ChannelWebThickness { get; set; }
        protected virtual void ConfigureChannelWebThickness(Rule<float> rule)
        {
            rule.AddToCAD(cad =>
            {
                cad.NameInCad = "G_T";
            });
        }
        public Rule<List<string>> ChannelSizesAvailable { get; set; }
        protected virtual void ConfigureChannelSizesAvailable(Rule<List<string>> rule)
        {
            rule.Define(() => new List<string>());
        }

        public Rule<float> ChannelWeightPerFt { get; set; }

        public Rule<string> MaterialPartNumber { get; set; }

        #endregion

        #region IBomPart Implementation

        public Rule<string> DatabasePartNumber { get; set; }
        public PartNumberRule<string> PartNumber { get; set; }

        public Rule<string> PartNumberPrefix { get; set; }
        //protected virtual void ConfigurePartNumberPrefix(Rule<string> rule)
        //{
        //    rule.Define(() => $"{ChannelSize.Value}x{Length.Value}-");
        //}

        public Rule<string> Description { get; set; }
        protected virtual void ConfigureDescription(Rule<string> rule)
        {
            rule.Define(() => $"{ChannelSize.Value.Replace("x", "\" X ")}# CHANNEL");
        }

        public Rule<string> BOMDescription { get; set; }
        protected virtual void ConfigureBOMDescription(Rule<string> rule)
        {
            rule.AddToCAD(cad =>
            {
                cad.IsActive.Define(false);
            });
        }

        public Rule<string> BOMDescription1 { get; set; }
        public Rule<string> BOMDescription2 { get; set; }
        public Rule<string> BOMDescription3 { get; set; }
        public Rule<string> BOMDescription4 { get; set; }
        public Rule<string> BOMDescription5 { get; set; }
        public Rule<string> BOMDescription6 { get; set; }
        public Rule<string> BOMDescription7 { get; set; }
        public Rule<string> BOMDescription8 { get; set; }
        public Rule<string> BOMDescription9 { get; set; }
        public Rule<string> BOMDescription10 { get; set; }
        public Rule<string> BOMDescription11 { get; set; }

        public Rule<string> Color { get; set; }
        public ReadOnlyRule<AdvanceColor> AdvanceColor { get; set; }
        public ReadOnlyRule<string> ColorDescription { get; set; }
        public StorageConfiguratorRepository Repository { get; set; }
        public Rule<string> ColorBOMName { get; set; }
        public ReadOnlyRule<string> ColorShortDescription { get; set; }

        public Rule<double> PartWeight { get; set; }
        public Rule<double> PartPaintWeight { get; set; }
        public Rule<float> WeightPerGallon { get; set; }
        public CsiRule<double> PaintCostPerGallon { get; set; }
        public Rule<float> PaintCost { get; set; }


        #endregion

        #region IRevisablePartImplementation

        public FactoryRevisionRule<string> FactoryRevision { get; set; }

        #endregion

        #region Part Overrides

        protected override void ConfigureUIMaterial(UIMaterial material)
        {
            material.Color.Define(Colors.Red);
            material.Opacity.Define(0.9f);
        }

        //TODO: Make this work with Content Center
        protected override void ConfigureFactoryName(Rule<string> rule)
        {
            rule.Define("Channel");
        }

        protected override void ConfigureIsNavigable(ReadOnlyRule<bool> rule)
        {
            rule.Define(false);
        }

        #endregion

        #region Graphics

        public Block ChannelWebBlock { get; set; }
        protected virtual void ConfigureChannelWebBlock(Block geo)
        {
            geo.Width.Define(ChannelWebThickness);
            geo.Height.Define(ChannelWidth);
            geo.Depth.Define(Length);
            geo.Position.Define(() => Helper.Vxz(ChannelWebThickness.Value / 2, Length.Value / 2));
        }

        public Block ChannelTopFlangeBlock { get; set; }
        protected virtual void ConfigureChannelTopFlangeBlock(Block geo)
        {
            geo.Width.Define(ChannelDepth);
            geo.Height.Define(ChannelWebThickness);
            geo.Depth.Define(Length);
            geo.Position.Define(() => new System.Numerics.Vector3(geo.Width.Value / 2, (ChannelWidth.Value / 2) - (geo.Height.Value / 2), Length.Value / 2));
        }

        public Block ChannelBtmFlangeBlock { get; set; }

        protected virtual void ConfigureChannelBtmFlangeBlock(Block geo)
        {
            geo.Width.Define(ChannelDepth);
            geo.Height.Define(ChannelWebThickness);
            geo.Depth.Define(Length);
            geo.Position.Define(() => new System.Numerics.Vector3(geo.Width.Value / 2, (-ChannelWidth.Value / 2) + (geo.Height.Value / 2), Length.Value / 2));
        }

        //public UCS UCS { get; set; }
        //protected virtual void ConfigureUCS(UCS child)
        //{
        //    child.Position.Define(() => (Helper.Vy(0)));
        //}

        #endregion

        #region Protected Methods

        protected override void ConfigureRules()
        {
            base.ConfigureRules();
            this.ConfigureChannelRules();
            this.ConfigureBomPartRules();
            this.ConfigureRevisablePart();
        }

        #endregion


    }
}
