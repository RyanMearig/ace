﻿using Ruler.Configurator.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using StorageConfigurator.Plugin;
using Microsoft.Extensions.Hosting;

namespace StorageConfigurator.Api
{
    public class Startup
    {
        private readonly IConfiguration _configuration;
        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            var connection = _configuration.GetConnectionString("StorageConfigurator");
            var options = new ConfiguratorBuilderOptions(ctxOptionsBuilder => ctxOptionsBuilder.UseSqlServer(connection));

            services.AddConfigurator(_configuration, options)
                .AddPlugin(new StoragePlugin());
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (!env.IsDevelopment())
            {
                app.UseHsts();
                app.UseHttpsRedirection();
            }

            app.UseConfigurator();
            app.UseDefaultFiles();
            app.UseStaticFiles();
        }
    }
}
