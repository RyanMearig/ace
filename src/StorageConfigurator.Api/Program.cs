﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Sinks.RollingFileAlternate;

namespace StorageConfigurator.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.Title = "Storage Configurator";

            Log.Logger = ConfigureLogger();

            try
            {
                Log.Information("Starting up");
                CreateHostBuilder(args)
                    .Build()
                    .Run();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Application start-up failed");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        private static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                    webBuilder.UseSerilog();
                });
        }

        private static ILogger ConfigureLogger()
        {
            var config = new LoggerConfiguration()
                .Enrich.FromLogContext();

            if (IsDevelopment())
            {
                config.WriteTo.Console();
                config.MinimumLevel.Information();
            }
            else
            {
                config.WriteTo.RollingFileAlternate(".\\logs", fileSizeLimitBytes: 1048576);
                config.MinimumLevel.Error();
            }

            return config.CreateLogger();
        }

        private static bool IsDevelopment()
        {
            return Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";
        }
    }
}
