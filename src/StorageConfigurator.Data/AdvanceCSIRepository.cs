﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using System;

namespace StorageConfigurator.Data
{
    public class AdvanceCSIRepository
    {

        #region Private Fields

        //private List<CustomerData> _customerDatas = new List<CustomerData>();
        //private List<CustomerAddressData> _customerAddressDatas = new List<CustomerAddressData>();
        private readonly AdvanceCSIContextFactory _csiContextFactory;
        private readonly AdvanceCSIConnectionFactory _csiConnectionFactory;

        #endregion

        #region Constructors

        public AdvanceCSIRepository(AdvanceCSIContextFactory contextFactory, AdvanceCSIConnectionFactory connectionFactory)
        {
            //_customerDatas.AddRange(context.CustomerData);
            //_customerAddressDatas.AddRange(context.CustomerAddressData);
            _csiContextFactory = contextFactory;
            _csiConnectionFactory = connectionFactory;
        }

        #endregion

        #region Public Methods

        public async Task<Customer> GetCustomerAsync(string customerNumber, int customerSeq)
        {
            if (!string.IsNullOrWhiteSpace(customerNumber))
            {
                using (var context = _csiContextFactory.Create())
                {
                    var customer = new Customer();
                    customer.CustomerNumber = customerNumber;
                    customer.CustomerSeq = customerSeq;
                    var customerData = await context.CustomerData?.FirstOrDefaultAsync(c => c.CustomerNumber.Trim() == customerNumber.Trim() &&
                                                                c.CustomerSeq == customerSeq);
                    if (customerData != null)
                    {
                        customer.Contact1 = customerData.Contact1;
                        customer.Contact2 = customerData.Contact2;
                        customer.Contact3 = customerData.Contact3;
                        customer.Phone1 = customerData.Phone1;
                        customer.Phone2 = customerData.Phone2;
                        customer.Phone3 = customerData.Phone3;
                        customer.TermsCode = customerData.TermsCode;
                        customer.CustomerType = customerData.CustomerType;
                    }

                    var customerAddrData = await context.CustomerAddressData?.FirstOrDefaultAsync(c => c.CustomerNumber.Trim() == customerNumber.Trim() &&
                                                                c.CustomerSeq == customerSeq);
                    if (customerAddrData != null)
                    {
                        customer.CustomerName = customerAddrData.CustomerName;
                        customer.Address1 = customerAddrData.Address1;
                        customer.Address2 = customerAddrData.Address2;
                        customer.City = customerAddrData.City;
                        customer.State = customerAddrData.State;
                        customer.Zip = customerAddrData.Zip;
                        customer.CustomerContactEmail = customerAddrData.CustomerContactEmail;
                        customer.ShipToEmail = customerAddrData.ShipToEmail;
                    }

                    return customer;
                }
            }
            else
            {
                return null;
            }
        }

        public async Task<CustomerData> GetCustomerData(AdvanceCSIContext csiContext, string customerNumber, int customerSeq)
        {
            return await csiContext.CustomerData?.FirstOrDefaultAsync(c => c.CustomerNumber.Trim() == customerNumber.Trim() &&
                                                                c.CustomerSeq == customerSeq);
        }

        public async Task<CustomerAddressData> GetCustomerAddressData(AdvanceCSIContext csiContext, string customerNumber, int customerSeq)
        {
            return await csiContext.CustomerAddressData?.FirstOrDefaultAsync(c => c.CustomerNumber.Trim() == customerNumber.Trim() &&
                                                                c.CustomerSeq == customerSeq);
        }

        public async Task<List<int>> GetCustomerSequences(string customerNumber)
        {
            if (string.IsNullOrWhiteSpace(customerNumber)) return null;

            using (var context = _csiContextFactory.Create())
            {
                return await context.CustomerData?.Where(c => c.CustomerNumber.Trim() == customerNumber.Trim())?
                                            .Select(s => s.CustomerSeq)?
                                            .OrderBy(n => n).ToListAsync();
            }
        }

        public async Task<double> GetLatestSteelSurcharge()
        {
            using (var context = _csiContextFactory.Create())
            {
                try
                {
                    var data = await context.SteelSurchargeData?.OrderByDescending(s => s.RecordDate)?.FirstOrDefaultAsync();
                    return data == null ? 0.0 : (double)data.Surcharge;
                }
                catch (Exception ex)
                {}
                return 0.0;
            }
        }

        #endregion
    }
}
