﻿using Dapper;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace StorageConfigurator.Data
{
    public class FactoryRevisionService
    {
        public FactoryRevisionService(IMemoryCache memoryCache, IConfiguration configuration)
        {
            _memoryCache = memoryCache;
            _connectionString = configuration.GetConnectionString("StorageConfigurator");
        }

        private readonly IMemoryCache _memoryCache;
        private readonly string _connectionString;

        public string GetFactoryMajorRevision(string factoryName)
        {
            var key = $"FN_{factoryName}";

            if(_memoryCache.TryGetValue(key, out string primaryRev))
            {
                return primaryRev;
            }

            var sql = "SELECT * FROM FactoryRevisions WHERE FactoryName = @FactoryName;";
            using(var conn = new SqlConnection(_connectionString))
            {
                var rev = conn.QueryFirstOrDefault<FactoryRevision>(sql, new { FactoryName = factoryName });
                primaryRev = rev?.PrimaryRevision;
            }

            _memoryCache.Set(key, primaryRev, new MemoryCacheEntryOptions()
            {
                SlidingExpiration = TimeSpan.FromSeconds(15)
            }); ;
            return primaryRev;
        }
    }
}
