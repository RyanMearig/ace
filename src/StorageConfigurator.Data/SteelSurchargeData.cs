﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace StorageConfigurator.Data
{
    [Table("_ASP_steel_sur_mst")]
    public class SteelSurchargeData
    {
        [Key]
        [Column("Id")]
        public Int32 Id { get; set; }

        [Column("CreateDate")]
        public DateTime CreateDate { get; set; }

        [Column("RecordDate")]
        public DateTime RecordDate { get; set; }

        [Column("Steel_sur")]
        public decimal Surcharge { get; set; }
    }
}
