﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace StorageConfigurator.Data
{
    public class AdvanceMargin
    {
        [Key]
        public string Family { get; set; }
        [Key]
        public string CustomerType { get; set; }
        [Key]
        public double CostMin { get; set; }
        public double CostMax { get; set; }
        public double Margin { get; set; }
    }
}
