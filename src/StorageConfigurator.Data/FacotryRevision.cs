﻿using System.ComponentModel.DataAnnotations;

namespace StorageConfigurator.Data
{
    public class FactoryRevision
    {
        [Key]
        public string FactoryName { get; set; }

        public string PrimaryRevision { get; set; }

    }
}
