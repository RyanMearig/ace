﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace StorageConfigurator.Data
{
    [Table("custaddr_mst")]
    public class CustomerAddressData
    {

        [Key]
        [Column("cust_num")]
        public string CustomerNumber { get; set; }

        [Key]
        [Column("cust_seq")]
        public int CustomerSeq { get; set; }

        [Column("name")]
        public string CustomerName { get; set; }

        [Column("city")]
        public string City { get; set; }

        [Column("state")]
        public string State { get; set; }

        [Column("zip")]
        public string Zip { get; set; }

        [Column("addr##1")]
        public string Address1 { get; set; }

        [Column("addr##2")]
        public string Address2 { get; set; }

        //[Column("addr##3")]
        //public string Address3 { get; set; }

        //[Column("addr##4")]
        //public string Address4 { get; set; }

        [Column("external_email_addr")]
        public string CustomerContactEmail { get; set; }

        [Column("ship_to_email")]
        public string ShipToEmail { get; set; }

    }
}
