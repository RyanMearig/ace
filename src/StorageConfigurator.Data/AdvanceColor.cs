﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace StorageConfigurator.Data
{
    public class AdvanceColor
    {
        [Key]
        public string Item { get; set; }
        public string Name { get; set; }
        public string ShortDescription { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public bool Available { get; set; }
    }
}
