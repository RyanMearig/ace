﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace StorageConfigurator.Data
{
    [Table("customer_mst")]
    public class CustomerData
    {
        [Key]
        [Column("cust_num")]
        public string CustomerNumber { get; set; }

        [Key]
        [Column("cust_seq")]
        public int CustomerSeq { get; set; }

        [Column("contact##1")]
        public string Contact1 { get; set; }

        [Column("contact##2")]
        public string Contact2 { get; set; }

        [Column("contact##3")]
        public string Contact3 { get; set; }

        [Column("phone##1")]
        public string Phone1 { get; set; }

        [Column("phone##2")]
        public string Phone2 { get; set; }

        [Column("phone##3")]
        public string Phone3 { get; set; }

        [Column("terms_code")]
        public string TermsCode { get; set; }

        [Column("end_user_type")]
        public string CustomerType { get; set; }
    }
}
