﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace StorageConfigurator.Data
{
    public class AdvanceCSIContextFactory
    {

        #region Private Fields

        private readonly DbContextOptions<AdvanceCSIContext> _options;

        #endregion

        #region Constructors

        public AdvanceCSIContextFactory(DbContextOptions<AdvanceCSIContext> options)
        {
            _options = options;
        }

        #endregion

        #region Public Methods

        public AdvanceCSIContext Create()
        {
            return new AdvanceCSIContext(_options);
        }

        #endregion

    }
}
