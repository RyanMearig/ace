﻿using System.Collections.Generic;
using System.Linq;

namespace StorageConfigurator.Data
{
    public class StorageConfiguratorRepository
    {
        #region Private Fields
        private readonly List<AdvanceColor> _colors;
        private readonly List<AdvanceMargin> _margins;
        private readonly List<AdvanceOverhead> _overheads;
        #endregion

        #region Constructors
        public StorageConfiguratorRepository(StorageConfiguratorContext context)
        {
            _colors = context.Colors.ToList();
            _margins = context.AdvanceMargins.ToList();
            _overheads = context.AdvanceOverheads.ToList();
        }

        #endregion

        #region Public Methods

        public List<AdvanceColor> GetAllColors()
        {
            return _colors;
        }

        public AdvanceColor GetColorByKey(string key)
        {
            var retVal = _colors.FirstOrDefault(c => c.Item == key);

            //This code was added since old projects were added by name
            return retVal != null ? retVal : _colors.FirstOrDefault(c => c.Name == key);
        }

        public List<AdvanceMargin> GetAllMargins()
        {
            return _margins;
        }

        public AdvanceMargin GetAdvanceMargin(string family, string customerType, double cost)
        {
            return _margins?.FirstOrDefault(m => m.Family.ToLower() == family.ToLower() &&
                                        m.CustomerType.ToLower().Contains(customerType.ToLower()) &&
                                        cost >= m.CostMin && cost < m.CostMax);
        }

        public double GetMinMargin(string family, string customerType, double cost)
        {
            return GetAdvanceMargin(family, customerType, cost)?.Margin ?? 0;
        }

        public double GetDefaultOverhead(string overheadName)
        {
            return _overheads.FirstOrDefault(h => h.Name.ToLower() == overheadName.ToLower())?.Value ?? 0;
        }

        #endregion

    }
}
