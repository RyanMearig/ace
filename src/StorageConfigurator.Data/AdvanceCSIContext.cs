﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace StorageConfigurator.Data
{
    public class AdvanceCSIContext : DbContext
    {
        public AdvanceCSIContext(DbContextOptions<AdvanceCSIContext> options) : base(options) { }

        public DbSet<CustomerData> CustomerData { get; set; }
        public DbSet<CustomerAddressData> CustomerAddressData { get; set; }
        public DbSet<SteelSurchargeData> SteelSurchargeData { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<CustomerData>().HasKey(c => new { c.CustomerNumber, c.CustomerSeq });
            builder.Entity<CustomerAddressData>().HasKey(ca => new { ca.CustomerNumber, ca.CustomerSeq });
            builder.Entity<SteelSurchargeData>().HasKey(ss => new { ss.Id });
        }

    }
}
