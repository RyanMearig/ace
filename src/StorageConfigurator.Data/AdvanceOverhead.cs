﻿using System.ComponentModel.DataAnnotations;

namespace StorageConfigurator.Data
{
    public class AdvanceOverhead
    {
        [Key]
        public string Name { get; set; }
        public double Value { get; set; }
    }
}
