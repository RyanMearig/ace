﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using StorageConfigurator.Data;

namespace StorageConfigurator.Data.Migrations
{
    [DbContext(typeof(StorageConfiguratorContext))]
    partial class StorageConfiguratorContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.1")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("StorageConfigurator.Data.AdvanceColor", b =>
                {
                    b.Property<string>("Item")
                        .HasColumnType("nvarchar(450)");

                    b.Property<bool>("Available")
                        .HasColumnType("bit");

                    b.Property<string>("Category")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Description")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("ShortDescription")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Item");

                    b.ToTable("Colors");
                });

            modelBuilder.Entity("StorageConfigurator.Data.AdvanceMargin", b =>
                {
                    b.Property<string>("Family")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("CustomerType")
                        .HasColumnType("nvarchar(450)");

                    b.Property<double>("CostMin")
                        .HasColumnType("float");

                    b.Property<double>("CostMax")
                        .HasColumnType("float");

                    b.Property<double>("Margin")
                        .HasColumnType("float");

                    b.HasKey("Family", "CustomerType", "CostMin");

                    b.ToTable("AdvanceMargins");
                });

            modelBuilder.Entity("StorageConfigurator.Data.AdvanceOverhead", b =>
                {
                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(450)");

                    b.Property<double>("Value")
                        .HasColumnType("float");

                    b.HasKey("Name");

                    b.ToTable("AdvanceOverheads");
                });

            modelBuilder.Entity("StorageConfigurator.Data.FactoryRevision", b =>
                {
                    b.Property<string>("FactoryName")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("PrimaryRevision")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("FactoryName");

                    b.ToTable("FactoryRevisions");
                });
#pragma warning restore 612, 618
        }
    }
}
