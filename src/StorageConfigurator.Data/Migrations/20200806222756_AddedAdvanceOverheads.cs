﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace StorageConfigurator.Data.Migrations
{
    public partial class AddedAdvanceOverheads : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AdvanceOverheads",
                columns: table => new
                {
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdvanceOverheads", x => x.Name);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AdvanceOverheads");
        }
    }
}
