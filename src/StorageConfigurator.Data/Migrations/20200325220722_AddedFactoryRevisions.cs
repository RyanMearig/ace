﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace StorageConfigurator.Data.Migrations
{
    public partial class AddedFactoryRevisions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "FactoryRevisions",
                columns: table => new
                {
                    FactoryName = table.Column<string>(nullable: false),
                    PrimaryRevision = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FactoryRevisions", x => x.FactoryName);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FactoryRevisions");
        }
    }
}
