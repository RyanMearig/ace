﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace StorageConfigurator.Data.Migrations
{
    public partial class AddedAdvanceMargins : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AdvanceMargins",
                columns: table => new
                {
                    Family = table.Column<string>(nullable: false),
                    CustomerType = table.Column<string>(nullable: false),
                    CostMin = table.Column<double>(nullable: false),
                    CostMax = table.Column<double>(nullable: false),
                    Margin = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdvanceMargins", x => new { x.Family, x.CustomerType, x.CostMin });
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AdvanceMargins");
        }
    }
}
