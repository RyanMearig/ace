﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StorageConfigurator.Data
{
    public class CsiItem
    {
        string item { get; set; }
        string unit_cost { get; set; }
        string u_m { get; set; }
    }
}
