﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dapper;

namespace StorageConfigurator.Data
{
    public class AdvanceCSIItemService
    {
        #region Private Fields

        private readonly AdvanceCSIConnectionFactory _csiConnectionFactory;

        #endregion

        #region Constructors

        public AdvanceCSIItemService(AdvanceCSIConnectionFactory connectionFactory)
        {
            _csiConnectionFactory = connectionFactory;
        }

        #endregion

        #region Public Methods

        public double GetItemCost(string partNumber, string plant)
        {
            double retVal = GetItemVendCost(partNumber, plant);

            if (retVal <= 0)
            {
                retVal = GetItemTableCost(partNumber, plant);
            }

            return retVal;
        }

        public double GetItemVendCost(string partNumber, string plant)
        {
            var query = "SELECT [brk_cost##1] FROM dbo.itemvendprice_mst " +
                $"WHERE item = '{partNumber}' order by effect_date desc";
            using (var connection = _csiConnectionFactory.GetConnection(plant))
            {
                return connection.Query<double>(query).FirstOrDefault();
            }
        }

        public double GetItemTableCost(string partNumber, string plant)
        {
            var query = "SELECT [unit_cost] FROM dbo.item_mst " +
                $"WHERE item = '{partNumber}'";
            using (var connection = _csiConnectionFactory.GetConnection(plant))
            {
                return connection.Query<double>(query).FirstOrDefault();
            }
        }

        public string GetItemUoM(string partNumber, string plant)
        {
            var query = "SELECT [u_m] FROM dbo.item_mst " +
                $"WHERE item = '{partNumber}'";
            using (var connection = _csiConnectionFactory.GetConnection(plant))
            {
                return connection.Query<string>(query).FirstOrDefault();
            }
        }

        public double GetItemWeight(string partNumber, string plant)
        {
            var query = "SELECT [unit_weight] FROM dbo.item_mst " +
                $"WHERE item = '{partNumber}'";
            using (var connection = _csiConnectionFactory.GetConnection(plant))
            {
                return connection.Query<double>(query).FirstOrDefault();
            }
        }

        public double GetItemBundle(string partNumber, string plant)
        {
            if (string.IsNullOrWhiteSpace(plant)) plant = "GA_C";

            var query = "SELECT [Uf_ASP_Items_RawMatlBundle] FROM dbo.item_mst " +
                $"WHERE item = '{partNumber}'";
            using (var connection = _csiConnectionFactory.GetConnection(plant))
            {
                return connection.Query<double>(query).FirstOrDefault();
            }
        }

        #endregion
    }
}
