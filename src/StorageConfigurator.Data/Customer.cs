﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StorageConfigurator.Data
{
    public class Customer
    {
        public string CustomerNumber { get; set; }
        public int CustomerSeq { get; set; }
        public string CustomerName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Contact1 { get; set; }
        public string Contact2 { get; set; }
        public string Contact3 { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Phone3 { get; set; }
        public string CustomerContactEmail { get; set; }
        public string ShipToEmail { get; set; }
        public string TermsCode { get; set; }
        public string CustomerType { get; set; }

    }
}
