﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace StorageConfigurator.Data
{
    public class StorageConfiguratorContext : DbContext
    {
        public StorageConfiguratorContext(DbContextOptions<StorageConfiguratorContext> options) : base(options) { }

        public DbSet<AdvanceColor> Colors { get; set; }
        public DbSet<AdvanceMargin> AdvanceMargins { get; set; }
        public DbSet<AdvanceOverhead> AdvanceOverheads { get; set; }
        public DbSet<FactoryRevision> FactoryRevisions { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<AdvanceMargin>().HasKey(c => new { c.Family, c.CustomerType, c.CostMin });
        }
    }
}
