﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace StorageConfigurator.Data
{
    public class AdvanceCSIConnectionFactory
    {

        #region Private Fields

        private readonly IConfiguration _configuration;

        #endregion

        #region Constructors

        public AdvanceCSIConnectionFactory(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        #endregion

        #region Public Methods

        public SqlConnection GetConnection(string plant)
        {
            var csiConnString = _configuration.GetConnectionString($"CSIDbConnection-{plant}");
            return new SqlConnection(csiConnString);
        }

        #endregion

    }
}
