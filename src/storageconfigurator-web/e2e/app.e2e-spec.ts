import { StorageConfiguratorWebPage } from './app.po';

describe('storageconfigurator-web App', () => {
  let page: StorageConfiguratorWebPage;

  beforeEach(() => {
    page = new StorageConfiguratorWebPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
