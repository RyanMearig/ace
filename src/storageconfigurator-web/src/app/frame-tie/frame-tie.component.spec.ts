import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrameTieComponent } from './frame-tie.component';

describe('FrameTieComponent', () => {
  let component: FrameTieComponent;
  let fixture: ComponentFixture<FrameTieComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrameTieComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrameTieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
