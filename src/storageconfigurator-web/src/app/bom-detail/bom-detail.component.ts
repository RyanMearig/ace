import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { BomService } from '../bom.service';
import { Part, PartType, ModelContextService, LineItem } from '@ruler/core';
import { LineItemBomService } from '../line-item-bom.service';
import { CostSummary } from '../cost-summary';

@Component({
  selector: 'bom-detail',
  templateUrl: './bom-detail.component.html',
  styleUrls: ['./bom-detail.component.scss']
})
export class BomDetailComponent implements OnInit {

  @Input()
  part: Part;

  bomSummary: any;
  allLaborCategories: Array<string> = new Array<string>();
  costSummary: CostSummary;
  weldHours: number;

  constructor(private _modelContext: ModelContextService,
    private _bomService: BomService,
    private _lineItemBomService: LineItemBomService,
    private _router: Router) { }

  ngOnInit() {
    this.getBom();
  }

  ngOnChanges() {
    this.getBom();
  }

  getBom() {
    this._bomService.getBom(this.part.lineItem.id)
      .subscribe(bomSummary => {
        this.bomSummary = bomSummary; this.getAllLaborCategories(bomSummary);
        this.weldHours = this.getWeldHours(bomSummary);
        this.createCostSummary();
        this.setCostSummary();
      });
  }

  getAllLaborCategories(bom: any) {
    if (bom) {
      bom.labor.map(l => {
          const category = l.category;
          if (category && !this.allLaborCategories.includes(category)) {
            this.allLaborCategories.push(category);
          }
      });
      bom.children.map(c => this.getAllLaborCategories(c));
    }
  }

  getWeldHours(bomItem: any): number {
    let hours = 0;
    const labor = bomItem.labor.filter(l => l.category === 'Weld');
    hours = labor.reduce((hours, current) => hours + current.hours, 0);

    bomItem.children.map(c => hours = hours + (this.getWeldHours(c) * c.qty ));
    if (hours === 0) { return null; }
    return hours;
  }

  createCostSummary() {
    const summary = new CostSummary();
    summary.lineItemId = this.part.lineItem.id;
    summary.name = this.bomSummary.shortDescription;
    summary.description = this.bomSummary.description;
    summary.cost = this.bomSummary.unitCost;
    summary.margin = this.bomSummary.unitMargin;
    summary.qty = this.bomSummary.qty;

    const extraDataJson = JSON.parse(this.bomSummary.extraData);
    extraDataJson.weldHours = this.weldHours;
    summary.extraData = JSON.stringify(extraDataJson);
    this.costSummary = summary;
  }

  setCostSummary() {
    this._lineItemBomService.setCostSummary(this.part.lineItem.id, this.costSummary).subscribe();
  }

}
