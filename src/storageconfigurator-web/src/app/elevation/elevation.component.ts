import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Part, PartType, ModelContextService } from '@ruler/core';

@Component({
  selector: 'elevation',
  templateUrl: './elevation.component.html',
  styleUrls: ['./elevation.component.scss']
})
export class ElevationComponent implements OnInit {

  @Input()
  part: Part;

  frontFrame: Part;
  bay: Part;
  rearFrame: Part;

  constructor(private _modelContext: ModelContextService,
    private _router: Router) { }

  addChild(partTypeCode: string) {
    const lineItemId = this.part.lineItem.id;
    const projectId = this.part.lineItem.project.id;

    this._modelContext.addPart(this.part.lineItem.id, {
      partPath: this.part.path,
      partTypeCode: partTypeCode
    }).subscribe((path) => {
      this._router.navigate(['/project', projectId, 'item', lineItemId, 'part', path]);
    });
  }

  ngOnInit() {
  }

  ngOnChanges() {
    if (this.part) {
      // this.frontFrame = this.part.children.filter(c => c.partType === 'FrameLine' && c.name.includes('Front'))[0];
      // this.bay = this.part.children.filter(c => c.partType === 'Bay')[0];
      // this.rearFrame = this.part.children.filter(c => c.partType === 'FrameLine' && c.name.includes('Rear'))[0];
    }
  }

}
