import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CostSummaryRuleComponent } from './cost-summary-rule.component';

describe('CostSummaryRuleComponent', () => {
  let component: CostSummaryRuleComponent;
  let fixture: ComponentFixture<CostSummaryRuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CostSummaryRuleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CostSummaryRuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
