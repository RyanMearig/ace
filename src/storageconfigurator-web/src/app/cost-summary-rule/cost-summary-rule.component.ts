import { Component, OnInit, OnChanges, Input, HostBinding } from '@angular/core';
import { Part, Rule, ModelContextService } from '@ruler/core';
import { BomService } from '../bom.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'cost-summary-rule',
  templateUrl: './cost-summary-rule.component.html',
  styleUrls: ['./cost-summary-rule.component.scss']
})
export class CostSummaryRuleComponent implements OnInit, OnChanges {
  @Input()
  rule: Rule<any>;

  @Input()
  labelText: string;

  @Input()
  allowReset = true;

  @Input()
  formatCurrency = false;

  @Input()
  step = 1;

  @Input()
  suffix: string;

  @HostBinding('hidden')
  isHidden = false;

  constructor(private _modelContext: ModelContextService,
              private _bomService: BomService,
              private _snackBar: MatSnackBar) { }

  ngOnInit() { }

  ngOnChanges() {
    if (this.rule) {
      this.isHidden = false;
      if (!this.labelText) {
        this.labelText = this.rule.label;
      }
    } else {
      this.isHidden = true;
    }
              }

  setValue(value, element) {
                this._modelContext.setRuleValue(this.rule, value)
                  .subscribe(null, err => this.handleError(err, element));
              }

  setNumberValue(value, element) {
    const retVal = Number(value.replace(/[$,]/g, ''));
    this.setValue(retVal, element);
  }

  reset(element) {
                this._modelContext.resetRule(this.rule)
                .subscribe(null, err => this.handleError(err, element));
            }

  get inputType(): string {
              if (this.rule.dataType === 'date') {
                  return 'date';
              } else if (this.rule.dataType === 'boolean') {
                  return 'checkbox';
              } else if (this.rule.dataType === 'integer' || this.rule.dataType === 'number') {
                  return 'number';
              } else {
                  return 'text';
              }
          }

  get resetEnabled(): boolean {
              return !this.readOnly && this.allowReset && this.rule.allowReset && this.rule.isOverridden;
          }

  get readOnly(): boolean {
            return this.rule.readOnly || this.rule.part.lineItem.isReadOnly;
          }

  private showSnackbar(message: string): void {
            this._snackBar.open(message, null, { duration: 1000 });
          }

  private handleError(err, element) {
            // Revert to previous value
            if (element) {
              element.value = this.rule.value;
            }

            // Show message to user
            this.showSnackbar(err.message);
          }

}
