import { Component, OnInit, OnDestroy, AfterViewChecked } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { CurrencyPipe } from '@angular/common';
import { ModelContextService, LineItemType, LineItem, BreadcrumbsService, LineItemUpdateRequest, BusyService } from '@ruler/core';
import { AddLineItemsDialogComponent } from '@ruler/material';
import { MatDialog } from '@angular/material/dialog';
import { Subscription, forkJoin } from 'rxjs';
import { filter, exhaustMap, pairwise } from 'rxjs/operators';
import { StorageConfiguratorProject } from '../storage-configurator-project';
import { ProjectBomService } from '../project-bom.service';
import { CostSummary } from '../cost-summary';
import { CsiService } from '../csi.service';

@Component({
  selector: 'project-margins',
  templateUrl: './project-margins.component.html',
  styleUrls: ['./project-margins.component.scss']
})
export class ProjectMarginsComponent implements OnInit, OnDestroy, AfterViewChecked {
  form: FormGroup;

  private _projectSub: Subscription;
  private _lineItemsSub: Subscription;
  private _formSub: Subscription;

  project: StorageConfiguratorProject;
  lineItems: Array<LineItem>;
  costSummaries: Array<CostSummary>;

  heldPriceLabel: string;

  constructor(private _modelContext: ModelContextService,
    private _breadcrumbs: BreadcrumbsService,
    private _router: Router,
    private _dialog: MatDialog,
    private _fb: FormBuilder,
    private _projectBomService: ProjectBomService,
    private _csiService: CsiService,
    private _busyService: BusyService,
    private _currencyPipe: CurrencyPipe
    ) { }

  ngOnInit() {
    this._projectSub = this._modelContext.project.pipe(
      filter(proj => proj !== null)
    ).subscribe(proj => {
      this.onProjectUpdate(proj as StorageConfiguratorProject);
      if (this.project) {
        this.getCostSummaries();
      }
    });

    this._lineItemsSub = this._modelContext.allLineItems
      .subscribe(items => this.lineItems = items.sort((item1, item2): number => {
        if (item1.createdDateTime < item2.createdDateTime) {
          return -1;
        }

        if (item1.createdDateTime > item2.createdDateTime) {
          return 1;
        }

        return  0;
      }));

    this._breadcrumbs.setSelectedPart(null);
  }

  private onProjectUpdate(proj: StorageConfiguratorProject) {
    this.project = proj as StorageConfiguratorProject;

    if (this.form) {
      if (this.form.controls.holdPrice.value) {
        this.heldPriceLabel = 'Held Price';
      } else {
        this.heldPriceLabel = 'Price';
      }
      this.updateForm(this.project);
    } else {
      this.form = this.buildForm(this.project);

      this.form.get('mfdMaterialCalcPrice').valueChanges.subscribe(value => {
        if (!this.form.controls.holdPrice.value) {
          this.updateControlValueIfChanged(this.form.controls.mfdMaterialPrice, value, true);
        }
      });
      this.form.get('mfdMaterialPrice').valueChanges.subscribe(value => {
        if (this.form.controls.holdPrice.value) {
          this.updateTotalPrice();
        }
      });
      this.form.get('buyoutMaterialCalcPrice').valueChanges.subscribe(value => {
        if (!this.form.controls.holdPrice.value) {
          this.updateControlValueIfChanged(this.form.controls.buyoutMaterialPrice, value, true);
        }
      });
      this.form.get('buyoutMaterialPrice').valueChanges.subscribe(value => {
        if (this.form.controls.holdPrice.value) {
          this.updateTotalPrice();
        }
      });
      this.form.get('engCost').valueChanges.subscribe(value =>  {
        this.form.controls.engCalcPrice.setValue(this.getPrice(value, this.form.controls.engMarginPercent.value));
      });
      this.form.get('engMarginPercent').valueChanges.subscribe(value =>  {
        this.updateControlValueIfChanged(this.form.controls.engCalcPrice, this.getPrice(this.form.controls.engCost.value, value), true);
      });
      this.form.get('engCalcPrice').valueChanges.subscribe(value => {
        if (!this.form.controls.holdPrice.value) {
          this.updateControlValueIfChanged(this.form.controls.engPrice, value, true, false);
        }

        this.form.controls.engMargin.setValue(value - this.form.controls.engCost.value, {emitEvent: false});
        this.form.controls.engAdjustment.setValue(this.form.controls.engPrice.value - value, {emitEvent: false});
      });
      this.form.get('engPrice').valueChanges.subscribe(value => {
        if (this.form.controls.holdPrice.value) {
          this.updateTotalPrice();
        }
      });
      this.form.get('installCost').valueChanges.subscribe(value =>  {
        this.updateControlValueIfChanged(this.form.controls.installCalcPrice,
                      this.getPrice(value, this.form.controls.installMarginPercent.value), true);
      });
      this.form.get('installMarginPercent').valueChanges.subscribe(value =>  {
        this.updateControlValueIfChanged(this.form.controls.installCalcPrice,
                                          this.getPrice(this.form.controls.installCost.value, value), true);
      });
      this.form.get('installCalcPrice').valueChanges.subscribe(value => {
        if (!this.form.controls.holdPrice.value) {
          this.updateControlValueIfChanged(this.form.controls.installPrice, value, true);
        }

        this.form.controls.installMargin.setValue(value - this.form.controls.installCost.value, {emitEvent: false});
        this.form.controls.installAdjustment.setValue(this.form.controls.installPrice.value - value, {emitEvent: false});
      });
      this.form.get('installPrice').valueChanges.subscribe(value => {
        if (this.form.controls.holdPrice.value) {
          this.updateTotalPrice();
        }
      });
      this.form.get('freightCost').valueChanges.subscribe(value =>  {
        this.form.controls.freightCalcPrice.setValue(this.getPrice(value, this.form.controls.freightMarginPercent.value));
      });
      this.form.get('freightMarginPercent').valueChanges.subscribe(value =>  {
        this.updateControlValueIfChanged(this.form.controls.freightCalcPrice,
                                        this.getPrice(this.form.controls.freightCost.value, value), true);
      });
      this.form.get('freightCalcPrice').valueChanges.subscribe(value => {
        if (!this.form.controls.holdPrice.value) {
          this.updateControlValueIfChanged(this.form.controls.freightPrice, value, true, false);
        }

        this.form.controls.freightMargin.setValue(value - this.form.controls.freightCost.value, {emitEvent: false});
        this.form.controls.freightAdjustment.setValue(this.form.controls.freightPrice.value - value, {emitEvent: false});
      });
      this.form.get('freightPrice').valueChanges.subscribe(value => {
        if (this.form.controls.holdPrice.value) {
          this.updateTotalPrice();
        }
      });
      this.form.get('otherCost').valueChanges.subscribe(value =>  {
        this.form.controls.otherCalcPrice.setValue(this.getPrice(value, this.form.controls.otherMarginPercent.value));
      });
      this.form.get('otherMarginPercent').valueChanges.subscribe(value =>  {
        this.updateControlValueIfChanged(this.form.controls.otherCalcPrice, this.getPrice(this.form.controls.otherCost.value, value), true);
      });
      this.form.get('otherCalcPrice').valueChanges.subscribe(value => {
        if (!this.form.controls.holdPrice.value) {
          this.updateControlValueIfChanged(this.form.controls.otherPrice, value, true, false);
        }

        this.form.controls.otherMargin.setValue(value - this.form.controls.otherCost.value, {emitEvent: false});
        this.form.controls.otherAdjustment.setValue(this.form.controls.otherPrice.value - value, {emitEvent: false});
      });
      this.form.get('otherPrice').valueChanges.subscribe(value => {
        if (this.form.controls.holdPrice.value) {
          this.updateTotalPrice();
        }
      });
      this.form.get('totalCalcPrice').valueChanges.subscribe(value => {
        this.form.controls.totalMargin.setValue(value - this.form.controls.totalCost.value, {emitEvent: false});
        this.form.controls.totalMarginPercent.setValue(this.getMarginPercent(this.form.controls.totalCost.value, value),
                                                          {emitEvent: false});

        if (this.form.controls.holdPrice.value) {
          this.updateControlValueIfChanged(this.form.controls.totalAdjustment,
            this.form.controls.totalPrice.value - value, true);
        } else {
          this.updateTotalPrice();
        }
      });
      this.form.get('totalPrice').valueChanges.subscribe(value => {
        this.updateControlValueIfChanged(this.form.controls.totalAdjustment,
          value - this.form.controls.totalCalcPrice.value, true);
      });
      this.form.get('materialTaxed').valueChanges.subscribe(value =>  {
        this.updateControlValueIfChanged(this.form.controls.totalWTaxes, this.getTotalWTaxes(), true);
      });
      this.form.get('engTaxed').valueChanges.subscribe(value =>  {
        this.updateControlValueIfChanged(this.form.controls.totalWTaxes, this.getTotalWTaxes(), true);
      });
      this.form.get('installTaxed').valueChanges.subscribe(value =>  {
        this.updateControlValueIfChanged(this.form.controls.totalWTaxes, this.getTotalWTaxes(), true);
      });
      this.form.get('freightTaxed').valueChanges.subscribe(value =>  {
        this.updateControlValueIfChanged(this.form.controls.totalWTaxes, this.getTotalWTaxes(), true);
      });
      this.form.get('otherTaxed').valueChanges.subscribe(value =>  {
        this.updateControlValueIfChanged(this.form.controls.totalWTaxes, this.getTotalWTaxes(), true);
      });
      this.form.get('taxRate').valueChanges.subscribe(value =>  {
        this.updateControlValueIfChanged(this.form.controls.totalWTaxes, this.getTotalWTaxes(), true);
      });
    }

    if (this.project.isReadOnly) {
      this.form.disable();
    } else {
      this.form.enable();
    }
  }

  private buildForm(proj: StorageConfiguratorProject): FormGroup {
    const numberPattern = '^[0-9.,]+$';
    return this._fb.group({
      drawingNumber: [ proj.drawingNumber, { updateOn: 'blur' }],
      name: [ proj.name, { updateOn: 'blur' }],

      mfdMaterialCost: [proj.mfdMaterialCost, { updateOn: 'blur' }],
      mfdMaterialPrice: [proj.mfdMaterialPrice, { updateOn: 'blur' }],
      mfdMaterialCalcPrice: [proj.mfdMaterialCalcPrice, { updateOn: 'blur' }],
      mfdMaterialMarginPercent: [ this.getMarginPercent(proj.mfdMaterialCost, proj.mfdMaterialCalcPrice).toString()
                                , { updateOn: 'blur', validators: [ Validators.min(0), Validators.max(100) ] } ],
      mfdMaterialMargin: [ '0',  { updateOn: 'blur' }],
      mfdMaterialAdjustment: [proj.mfdMaterialPrice - proj.mfdMaterialCalcPrice, {updateOn: 'blur'} ],

      buyoutMaterialCost: [proj.buyoutMaterialCost, { updateOn: 'blur' }],
      buyoutMaterialCalcPrice: [proj.buyoutMaterialCalcPrice, { updateOn: 'blur' }],
      buyoutMaterialPrice: [proj.buyoutMaterialPrice, { updateOn: 'blur' }],
      buyoutMaterialMargin: [ '0',  { updateOn: 'blur' }],
      buyoutMaterialMarginPercent: [ this.getMarginPercent(proj.buyoutMaterialCost, proj.buyoutMaterialCalcPrice).toString()
                                  ,  { updateOn: 'blur' , validators: [ Validators.min(0), Validators.max(100) ] } ],
      buyoutMaterialAdjustment: [proj.buyoutMaterialPrice - proj.buyoutMaterialCalcPrice, {updateOn: 'blur'} ],

      materialTaxed: [proj.materialTaxed, { updateOn: 'blur' }],

      engCalcPrice: [proj.engCalcPrice, { updateOn: 'blur' }],
      engPrice: [proj.engPrice, { updateOn: 'blur' }],
      engCost: [proj.engCost, {updateOn: 'blur'} ],
      engMarginPercent: [proj.engMarginPercent, {updateOn: 'blur' , validators: [ Validators.min(0), Validators.max(100) ]} ],
      engMargin: [proj.engCalcPrice - proj.engCost, {updateOn: 'blur'} ],
      engTaxed: [proj.engTaxed, { updateOn: 'blur' }],
      engAdjustment: [proj.engPrice - proj.engCalcPrice, {updateOn: 'blur'} ],

      installCalcPrice: [proj.installCalcPrice, { updateOn: 'blur' }],
      installPrice: [proj.installPrice, { updateOn: 'blur' }],
      installCost: [proj.installCost, {updateOn: 'blur'} ],
      installMarginPercent: [proj.installMarginPercent, {updateOn: 'blur' , validators: [ Validators.min(0), Validators.max(100) ]} ],
      installMargin: [proj.installCalcPrice - proj.installCost, {updateOn: 'blur'} ],
      installTaxed: [proj.installTaxed, { updateOn: 'blur' }],
      installAdjustment: [proj.installPrice - proj.installCalcPrice, {updateOn: 'blur'} ],

      freightCalcPrice: [proj.freightCalcPrice, { updateOn: 'blur' }],
      freightPrice: [proj.freightPrice, { updateOn: 'blur' }],
      freightCost: [proj.freightCost, {updateOn: 'blur'} ],
      freightMarginPercent: [proj.freightMarginPercent, {updateOn: 'blur' , validators: [ Validators.min(0), Validators.max(100) ]} ],
      freightMargin: [proj.freightCalcPrice - proj.freightCost, {updateOn: 'blur'} ],
      freightTaxed: [proj.freightTaxed, { updateOn: 'blur' }],
      freightAdjustment: [proj.freightPrice - proj.freightCalcPrice, {updateOn: 'blur'} ],

      otherCalcPrice: [proj.otherCalcPrice, { updateOn: 'blur' }],
      otherPrice: [proj.otherPrice, { updateOn: 'blur' }],
      otherCost: [proj.otherCost, {updateOn: 'blur'} ],
      otherMarginPercent: [proj.otherMarginPercent, {updateOn: 'blur' , validators: [ Validators.min(0), Validators.max(100) ]} ],
      otherMargin: [proj.otherCalcPrice - proj.otherCost, {updateOn: 'blur'} ],
      otherTaxed: [proj.otherTaxed, { updateOn: 'blur' }],
      otherAdjustment: [proj.otherPrice - proj.otherCalcPrice, {updateOn: 'blur'} ],

      totalCalcPrice: [proj.totalCalcPrice, { updateOn: 'blur' }],
      totalPrice: [proj.totalPrice, { updateOn: 'blur' }],
      totalCost: [proj.totalCost, { updateOn: 'blur' }],
      totalMarginPercent: [this.getMarginPercent(proj.totalCost, proj.totalCalcPrice).toString(),
                           { updateOn: 'blur' , validators: [ Validators.min(0), Validators.max(100) ]}],
      totalMargin: [proj.totalCalcPrice, { updateOn: 'blur' }],
      totalAdjustment: [proj.totalAdjustment, {updateOn: 'blur'} ],

      taxRate: [proj.taxRate, { updateOn: 'blur' }],
      totalWTaxes: [ proj.totalWTaxes, { updateOn: 'blur' } ],

      holdPrice: [proj.holdPrice, {updateOn: 'blur'}]
    });
  }

  private updateForm(proj: StorageConfiguratorProject) {
    this.form.patchValue(proj);
    this.form.markAsPristine();
  }

  onSubmit() {
    this._formSub = this.form.valueChanges.pipe(
      filter(() => this.form.dirty),
      exhaustMap(changes => {
        const proj = {...this.project, ...changes};
        return this._modelContext.updateProject<StorageConfiguratorProject>(proj);
      })
    )
    .subscribe();
  }

  ngAfterViewChecked() {
    if (this._formSub || !this.form) {
      return;
    }

    this._formSub = this.form.valueChanges.pipe(
      filter(() => this.form.dirty),
      exhaustMap(changes => {
        const proj = {...this.project, ...changes};
        return this._modelContext.updateProject<StorageConfiguratorProject>(proj);
      })
    )
    .subscribe();
  }

  updateProjectMargins() {
    const customerTyp = this.form.controls.customerType.value;
    if (customerTyp !== undefined && customerTyp !== null) {
      const ttlCost = this.form.controls.totalCost.value;
      forkJoin([this._projectBomService.getAdvanceMargin('Eng', customerTyp, ttlCost),
                this._projectBomService.getAdvanceMargin('Install', customerTyp, ttlCost),
                this._projectBomService.getAdvanceMargin('Freight', customerTyp, ttlCost),
                this._projectBomService.getAdvanceMargin('Other', customerTyp, ttlCost)])
      .subscribe(async (results) => {
        this.updateControlValueIfChanged(this.form.controls.engMarginPercent, results[0], true);
        await this.delay(100);
        this.updateControlValueIfChanged(this.form.controls.installMarginPercent, results[1], true);
        await this.delay(100);
        this.updateControlValueIfChanged(this.form.controls.freightMarginPercent, results[2], true);
        await this.delay(100);
        this.updateControlValueIfChanged(this.form.controls.otherMarginPercent, results[3], true);
      });
    }
  }

  private delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  updateControlValueIfChanged(control: AbstractControl, value: any, event: boolean = false, markDirty: boolean = true) {
    if (value !== undefined && value !== null && control.value !== value) {
      control.setValue(value, {emitEvent: event});

      if (markDirty) {
        this.form.markAsDirty();
      }
    }
  }

  updateCalculatedPricing() {
    const ttlCost = this.form.controls.mfdMaterialCost.value +
                  this.form.controls.buyoutMaterialCost.value +
                  +this.form.controls.engCost.value +
                  +this.form.controls.installCost.value +
                  +this.form.controls.freightCost.value +
                  +this.form.controls.otherCost.value;
    this.updateControlValueIfChanged(this.form.controls.totalCost, ttlCost, true);

    const ttlCalcPrice = this.form.controls.mfdMaterialCalcPrice.value +
                  this.form.controls.buyoutMaterialCalcPrice.value +
                  +this.form.controls.engCalcPrice.value +
                  +this.form.controls.installCalcPrice.value +
                  +this.form.controls.freightCalcPrice.value +
                  +this.form.controls.otherCalcPrice.value;
    this.updateControlValueIfChanged(this.form.controls.totalCalcPrice, ttlCalcPrice, true);
  }

  updateTotalPrice() {
    if (this.form.controls.holdPrice.value) {
      const ttlPrice = this.form.controls.mfdMaterialPrice.value +
                  this.form.controls.buyoutMaterialPrice.value +
                  +this.form.controls.engPrice.value +
                  +this.form.controls.installPrice.value +
                  +this.form.controls.freightPrice.value +
                  +this.form.controls.otherPrice.value;
      this.updateControlValueIfChanged(this.form.controls.totalPrice, ttlPrice, true);
    } else {
        this.updateControlValueIfChanged(this.form.controls.totalPrice, this.form.controls.totalCalcPrice.value, true);
    }
  }

  getCostSummaries() {
    this._projectBomService.getCostSummaries(this.project.id)
    .subscribe(c => {
      this.costSummaries = c;
      this.setValuesFromCostSummaries();
    });
  }

  ngOnDestroy() {
    this._projectSub.unsubscribe();
    this._lineItemsSub.unsubscribe();
    this._formSub.unsubscribe();
  }

  onHoldPriceChange(heldPriceVal: boolean) {
    if (!heldPriceVal) {
          this.updateControlValueIfChanged(this.form.controls.mfdMaterialPrice,
                                            this.form.controls.mfdMaterialCalcPrice.value, false, false);
          this.updateControlValueIfChanged(this.form.controls.buyoutMaterialPrice,
                                            this.form.controls.buyoutMaterialCalcPrice.value, false, false);
          this.updateControlValueIfChanged(this.form.controls.installPrice, this.form.controls.installCalcPrice.value, false, false);
          this.updateControlValueIfChanged(this.form.controls.freightPrice, this.form.controls.freightCalcPrice.value, false, false);
          this.updateControlValueIfChanged(this.form.controls.engPrice, this.form.controls.engCalcPrice.value, false, false);
          this.updateControlValueIfChanged(this.form.controls.otherPrice, this.form.controls.otherCalcPrice.value, false, false);

          this.updateControlValueIfChanged(this.form.controls.totalPrice, this.form.controls.totalCalcPrice.value, false);
          this.updateControlValueIfChanged(this.form.controls.totalAdjustment,
            this.form.controls.totalPrice.value - this.form.controls.totalCalcPrice.value, false);
          this.updateControlValueIfChanged(this.form.controls.totalWTaxes, this.getTotalWTaxes(), false);
    }
  }

  getPrice(cost: number, margin: number): number {
    const marginDec = margin / 100;
    return cost / (1 - marginDec);
  }

  setValuesFromCostSummaries() {
    if (this.costSummaries !== null && this.costSummaries !== undefined) {
      this.setManufacturedMaterialValues();
      this.setBuyoutMaterialValues();
      this.updateCalculatedPricing();
    }
  }

  setManufacturedMaterialValues() {
    const mfdCost = this.getCostSummariesExtraDataSum('manMaterialCost');
    const mfdPrice = this.getCostSummariesExtraDataSum('manMaterialPrice');
    this.updateControlValueIfChanged(this.form.controls.mfdMaterialCost, mfdCost, true);
    this.updateControlValueIfChanged(this.form.controls.mfdMaterialCalcPrice, mfdPrice, true);
    this.updateControlValueIfChanged(this.form.controls.mfdMaterialMargin, mfdPrice - mfdCost, false);
    this.updateControlValueIfChanged(this.form.controls.mfdMaterialMarginPercent, this.getMarginPercent(mfdCost, mfdPrice), true);
    this.updateControlValueIfChanged(this.form.controls.mfdMaterialAdjustment,
                                    this.form.controls.mfdMaterialPrice.value - mfdPrice);
  }

  setBuyoutMaterialValues() {
    const cost = this.getCostSummariesExtraDataSum('buyoutMaterialCost');
    const price = this.getCostSummariesExtraDataSum('buyoutMaterialPrice');
    this.updateControlValueIfChanged(this.form.controls.buyoutMaterialCost, cost, true);
    this.updateControlValueIfChanged(this.form.controls.buyoutMaterialCalcPrice, price, true);
    this.updateControlValueIfChanged(this.form.controls.buyoutMaterialMargin, price - cost, false);
    this.updateControlValueIfChanged(this.form.controls.buyoutMaterialMarginPercent, this.getMarginPercent(cost, price), true);
    this.updateControlValueIfChanged(this.form.controls.buyoutMaterialAdjustment,
                                    this.form.controls.buyoutMaterialPrice.value - price);
  }

  getCostSummariesExtraDataSum(extraDataName: string): number {
    return this.costSummaries.reduce((sum, current) =>
                    sum + this.getExtraDataNumberValue(current.extraData, extraDataName, 0), 0);
  }

  getMarginPercent(cost, price, defaultVal: number = 0): number {
    if (price === undefined || cost === undefined || price === 0) { return defaultVal; }
    return ((price - cost) / price) * 100;
  }

  getTotalWTaxes(): number {
    let ttlToTax = 0.0;

    if (this.form.controls.materialTaxed.value) {
      ttlToTax = ttlToTax + this.form.controls.mfdMaterialPrice.value;
      ttlToTax = ttlToTax + this.form.controls.buyoutMaterialPrice.value;
    }
    if (this.form.controls.otherTaxed.value) { ttlToTax = ttlToTax + this.form.controls.engPrice.value; }
    if (this.form.controls.installTaxed.value) { ttlToTax = ttlToTax + this.form.controls.installPrice.value; }
    if (this.form.controls.freightTaxed.value) { ttlToTax = ttlToTax + this.form.controls.freightPrice.value; }
    if (this.form.controls.otherTaxed.value) { ttlToTax = ttlToTax + this.form.controls.otherPrice.value; }

    return this.form.controls.totalPrice.value + (ttlToTax * (this.form.controls.taxRate.value * 0.01));
  }

  getExtraDataNumberValue(extraData: string, valueName: string, defaultVal: number = 0): number {
    const obj = JSON.parse(extraData);
    return obj[valueName] != null ? obj[valueName] : defaultVal;
  }

  getExtraDataValue(extraData: string, valueName: string, defaultVal: string = ''): string {
    const obj = JSON.parse(extraData);
    return obj[valueName] != null ? obj[valueName] : defaultVal;
  }

  formatMoney(value) {
    if (value === undefined || value === null) { value = 0.0; }
    const temp = `${value}`.replace(/\,/g, '');
    return this._currencyPipe.transform(temp.replace(/\$/g, ''));
  }

}
