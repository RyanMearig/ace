import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CurrencyPipe } from '@angular/common';

import { ConfiguratorModule, RulerConfig  } from '@ruler/core';
import { RulerMaterialModule } from '@ruler/material';
import { GraphicsViewerModule } from '@ruler/graphics-viewer';

import { MaterialModule } from './modules/material/material.module';

import { routing, appRoutingProviders } from './app.routes';
import { environment } from '../environments/environment';

import { PartChildrenNavComponent } from './part-children-nav/part-children-nav.component';

import { AppComponent } from './app.component';
import { DesignSummaryComponent } from './design-summary/design-summary.component';
import { ProjectComponent } from './project/project.component';
import { ProjectDetailsComponent } from './project-details/project-details.component';
import { ProjectMarginsComponent } from './project-margins/project-margins.component';
import { LineItemComponent } from './line-item/line-item.component';
import { PartPropertiesComponent } from './part-properties/part-properties.component';
import { OutputsService } from './outputs.service';
import { ShippablePartsComponent } from './shippable-parts/shippable-parts.component';
import { StorageSystemComponent } from './storage-system/storage-system.component';
import { RowComponent } from './row/row.component';
import { FrameUprightComponent } from './frame-upright/frame-upright.component';
import { FrameUprightKitComponent } from './frame-upright-kit/frame-upright-kit.component';
import { PostComponent } from './post/post.component';
import { BaysComponent } from './bays/bays.component';
import { BayComponent } from './bay/bay.component';
import { ProductsComponent } from './products/products.component';
import { FrameLinesComponent } from './frame-lines/frame-lines.component';
import { SelectiveFrameLineComponent } from './selective-frame-line/selective-frame-line.component';
import { FrameTypesComponent } from './frame-types/frame-types.component';
import { FrameTypeComponent } from './frame-type/frame-type.component';
import { ElevationComponent } from './elevation/elevation.component';
import { ElevationsComponent } from './elevations/elevations.component';
import { ShelfLoadBeamComponent } from './shelf-load-beam/shelf-load-beam.component';
import { FrameTieComponent } from './frame-tie/frame-tie.component';
import { EndAisleGuardComponent } from './end-aisle-guard/end-aisle-guard.component';
import { CustomPartComponent } from './custom-part/custom-part.component';
import { BuyoutPartComponent } from './buyout-part/buyout-part.component';
import { ShelfBeamCrossbarComponent } from './shelf-beam-crossbar/shelf-beam-crossbar.component';
import { RowTypesComponent } from './row-types/row-types.component';
import { CreateOutputsDialogComponent } from './create-outputs-dialog/create-outputs-dialog.component';
import { BomSummaryViewComponent } from './bom-summary-view/bom-summary-view.component';
import { BomDetailViewComponent } from './bom-detail-view/bom-detail-view.component';
import { BomSummaryComponent } from './bom-summary/bom-summary.component';
import { BomDetailComponent } from './bom-detail/bom-detail.component';
import { BomComponent } from './bom/bom.component';
import { CostSummaryComponent } from './cost-summary/cost-summary.component';
import { CostSummaryRuleComponent } from './cost-summary-rule/cost-summary-rule.component';
import { AddMultiPartsDialogComponent } from './add-multi-parts-dialog/add-multi-parts-dialog.component';
import { CsiService } from './csi.service';
import { BomEditingRowComponent } from './bom-editing-row/bom-editing-row.component';
import { CurrencyMaskInputMode, NgxCurrencyModule } from 'ngx-currency';

export const customCurrencyMaskConfig = {
  align: 'left',
  allowNegative: true,
  allowZero: true,
  decimal: '.',
  precision: 2,
  prefix: '$',
  suffix: '',
  thousands: ',',
  nullable: true,
  min: null,
  max: null,
  inputMode: CurrencyMaskInputMode.NATURAL
};

const rulerConfig = new RulerConfig ();
rulerConfig.apiUrl = environment.apiUrl;
rulerConfig.signalrUrl = environment.signalrUrl;

@NgModule({
  declarations: [
    AppComponent,
    DesignSummaryComponent,
    ProjectComponent,
    ProjectDetailsComponent,
    ProjectMarginsComponent,
    LineItemComponent,
    PartPropertiesComponent,
    PartChildrenNavComponent,
    ShippablePartsComponent,
    StorageSystemComponent,
    RowComponent,
    FrameUprightComponent,
    FrameUprightKitComponent,
    PostComponent,
    BaysComponent,
    BayComponent,
    ProductsComponent,
    FrameLinesComponent,
    SelectiveFrameLineComponent,
    FrameTypesComponent,
    FrameTypeComponent,
    ElevationComponent,
    ElevationsComponent,
    ShelfLoadBeamComponent,
    FrameTieComponent,
    EndAisleGuardComponent,
    CustomPartComponent,
    BuyoutPartComponent,
    ShelfBeamCrossbarComponent,
    RowTypesComponent,
    CreateOutputsDialogComponent,
    BomSummaryViewComponent,
    BomDetailViewComponent,
    BomSummaryComponent,
    BomDetailComponent,
    BomComponent,
    CostSummaryComponent,
    CostSummaryRuleComponent,
    AddMultiPartsDialogComponent,
    BomEditingRowComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    routing,
    FlexLayoutModule,
    NgxCurrencyModule,
    NgxCurrencyModule.forRoot(customCurrencyMaskConfig),
    ConfiguratorModule.forRoot(rulerConfig),
    RulerMaterialModule.forRoot(rulerConfig),
    GraphicsViewerModule,
    MaterialModule
  ],
  entryComponents: [
    CreateOutputsDialogComponent,
    AddMultiPartsDialogComponent
  ],
  providers: [
    appRoutingProviders,
    OutputsService,
    CsiService,
    CurrencyPipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
