import { Injectable } from '@angular/core';
import { CostSummary } from './cost-summary';
import { HttpClient, HttpEvent, HttpResponse } from '@angular/common/http';
import { environment } from '../environments/environment';
import { Observable, Subject, BehaviorSubject, throwError } from 'rxjs';
import { withLatestFrom, tap, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LineItemBomService {

  constructor(private _http: HttpClient) { }

  private _costSummary: Subject<CostSummary> = new BehaviorSubject<CostSummary>(null);

  get costSummary(): Observable<CostSummary> {
    return this._costSummary;
  }

  setCostSummary(lineItemId: string, costSummary: CostSummary) {
    const url = `${environment.apiUrl}lineItemCostSummary/${lineItemId}`;
    return this._http.put<any>(url, costSummary).pipe(
      tap(summary => { this._costSummary.next(summary); }),
      catchError((error: any) => this.handleError(error)));
  }

  private handleError(error: any): Observable<any> {
    // TODO: Add more robust error handling
    console.error(error);
    const returnError = (error && error.error) ? error.error :
      { message: 'An error occurred', details: '' };
    return throwError(returnError);
  }


}
