import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomPartComponent } from './custom-part.component';

describe('CustomPartComponent', () => {
  let component: CustomPartComponent;
  let fixture: ComponentFixture<CustomPartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomPartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomPartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
