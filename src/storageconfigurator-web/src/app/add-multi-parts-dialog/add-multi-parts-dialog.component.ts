import { Component, OnInit, OnDestroy, Input, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar } from '@angular/material';
import { Observable ,  Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { ModelContextService, Project, LineItemType, LineItem, Part, BusyService } from '@ruler/core';
import { BomService } from '../bom.service';
import { CommonDialogsService } from '@ruler/material';

@Component({
  selector: 'add-multi-parts-dialog',
  templateUrl: './add-multi-parts-dialog.component.html',
  styleUrls: ['./add-multi-parts-dialog.component.scss']
})
export class AddMultiPartsDialogComponent implements OnInit, OnDestroy {

partTypeCode: string;
part: Part;
subscriptions: Array<Subscription> = [];
title: string;
qty: number;

constructor(private _modelContext: ModelContextService,
            private _busyService: BusyService,
            private _bomService: BomService,
            private _snackBar: MatSnackBar,
            public dialogRef: MatDialogRef<AddMultiPartsDialogComponent>,
            @Inject(MAT_DIALOG_DATA) data: any) {
              this.part = data.part;
              this.partTypeCode = data.partTypeCode;
            }

ngOnInit() {
    // this.subscriptions.push(this._modelContext.project.subscribe(proj => this.project = proj));
    if (this.partTypeCode === 'rowproxy') {
      this.title = 'Row';
    } else {
      this.title = this.part.partType;
    }
    this.qty = 1;
  }

addChildren() {
    this._busyService.addRequest();
    this._bomService.addParts(this.part.lineItem.id, this.qty, {
      partPath: this.part.path,
      partTypeCode: this.partTypeCode
    }).subscribe(null, err => this.handleError(err),
    () =>
      this._modelContext.loadLineItem(this.part.lineItem.id)
      .subscribe(loadedLi => {
        this._busyService.removeRequest();
      })
    );
  }

private handleError(err) {
  this.showSnackbar(err.message);
  this._busyService.removeRequest();
}

private showSnackbar(message: string): void {
  this._snackBar.open(message, null, { duration: 1000 });
}

private delay(ms: number)
{
  return new Promise(resolve => setTimeout(resolve, ms));
}

ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe);
  }
}
