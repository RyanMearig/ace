import { Component, OnInit, OnChanges, Input } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Part, ModelContextService } from '@ruler/core';
import { CommonDialogsService } from '@ruler/material';
import { stringify } from 'querystring';

@Component({
  selector: 'advance-part-children-nav',
  templateUrl: './part-children-nav.component.html',
  styleUrls: ['./part-children-nav.component.scss']
})
export class PartChildrenNavComponent implements OnInit, OnChanges {
  private _lineItemId: string;

  @Input()
  part: Part;

  navigableChildren: Array<Part>;
  groupNames: Array<string>;
  specialGroupNames: Array<string>;
  readOnly = true;

  nonGroupedPartTypes: Array<string> = [];
  specialGroupedPartTypes: Array<string> = ['RowProxy'];
  showBack: boolean;

  constructor(private _modelContext: ModelContextService,
    private _route: ActivatedRoute,
    private _dialogSvc: CommonDialogsService) { }

  deletePart(path: string) {
    this._dialogSvc.showConfirmation('Are you sure you would like to delete this part?', 'Are You Sure?').subscribe(confirmed => {
      if (confirmed === true) {
        this._modelContext.deletePart(this.part.lineItem.id, path).subscribe();
      }
    });
  }

  ngOnInit() {
    this._route.params
      .subscribe((params: Params) => {
        // TODO: Should lineitemid be passed in as input rather than looking at params? ** this can be found using this.part.lineItem.id
        this._lineItemId = params.id;
      });
  }

  ngOnChanges() {
    this.readOnly = !(this.part && this.part.lineItem.isCurrentRevision);
    this.calculateNavParts();
  }

  calculateNavParts() {
    if (this.part && this.part.partType === 'ShippableParts') {
      this.groupNames = this.part.children
      .filter(c => !this.nonGroupedPartTypes.includes(c.partType))
      .filter(c => c.isNavigable)
      .map(c => c.partType)
      .filter(this.onlyUnique);
      this.navigableChildren = [];
      this.specialGroupNames = [];
    } else if (this.part) {
      this.groupNames = [];

      this.navigableChildren = this.part.children
      .filter(c => !this.specialGroupedPartTypes.includes(c.partType))
      .filter(c => c.isNavigable);

      this.specialGroupNames = this.part.children
      .filter(c => this.specialGroupedPartTypes.includes(c.partType))
      .filter(c => c.isNavigable)
      .map(c => c.partType)
      .filter(this.onlyUnique);
      if (this.specialGroupNames.length > 0) { this.groupNames = this.groupNames.concat(this.specialGroupNames); }
    } else {
      this.groupNames = [];
      this.navigableChildren = [];
    }
    this.showBack = false;
  }

  onlyUnique(value, index, self) {
      return self.indexOf(value) === index;
  }

  pluralizeName(name: string) {
    const retName = name.replace(/([A-Z])/g, ' $1') ;
    return retName.replace(' Proxy', '') + 's';
  }

  partTypeQty(name: string) {
    return this.part.children.filter(c => c.partType === name).length;
  }

  setNavigable(partType: string) {
    if (partType === '') {
      this.calculateNavParts();
      return;
    }
    this.navigableChildren = this.part.children.filter(c => c.partType === partType);
    this.groupNames = [];
    this.showBack = true;
  }

  evaluatePartBoolRule(prt: Part, ruleName: string) {
    const usageRule = this.getPartRule(prt, ruleName);

    if (usageRule != null) {
      return usageRule.value;
    } else {
      return false;
    }
  }

  isUsed(prt: Part) {
    if (prt !== null && prt !== undefined) {
      if (prt.partType === 'Row') {
        return this.evaluatePartBoolRule(prt, 'isUsedRowType');
      } else if (prt.partType === 'SelectiveFrameType' || prt.partType === 'DoubleDeepFrameType') {
        return this.evaluatePartBoolRule(prt, 'isUsedInFrameLine');
      } else {
        return this.evaluatePartBoolRule(prt, 'isUsedInElevation');
      }
    }
    return false;
  }

  isLineItem(prt: Part) {
    return prt.partType === 'StorageSystem' || prt.partType === 'ShippableParts';
  }

  isStorageSystem(prt: Part) {
    const truth = prt.partType === 'StorageSystem';
    return truth;
  }

  getBomPart(prt: Part) {
    return prt.children.filter(c => c.name === 'Bom')[0];
  }

  getPartRule(prt: Part, rulename: String) {
    const rules = prt.rules.filter(r => r.name === rulename);

    if (rules.length > 0) {
      return rules[0];
    } else {
      return null;
    }

    // return prt.rules.filter(r => r.name === rulename)[0];
  }
}
