/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { PartChildrenNavComponent } from './part-children-nav.component';

describe('PartChildrenNavComponent', () => {
  let component: PartChildrenNavComponent;
  let fixture: ComponentFixture<PartChildrenNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartChildrenNavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartChildrenNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
