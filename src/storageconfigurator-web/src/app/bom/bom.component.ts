import { Component, OnInit, Input } from '@angular/core';
import { Part } from '@ruler/core';

@Component({
  selector: 'bom',
  templateUrl: './bom.component.html',
  styleUrls: ['./bom.component.scss']
})
export class BomComponent implements OnInit {

  constructor() { }

@Input()
  part: Part;

  ngOnInit() {
  }

}
