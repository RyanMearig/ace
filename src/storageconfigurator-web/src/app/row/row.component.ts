import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Part, PartType, ModelContextService } from '@ruler/core';
import { MatSnackBar } from '@angular/material';
import { BomService } from '../bom.service';
import { groupBy } from 'rxjs/operators';

@Component({
  selector: 'row',
  templateUrl: './row.component.html',
  styleUrls: ['./row.component.scss']
})
export class RowComponent implements OnInit {

  @Input()
  part: Part;

  bays: Array<Part>;
  frames: Array<Part>;

  constructor(private _modelContext: ModelContextService,
    private _snackBar: MatSnackBar,
    private _bomService: BomService,
    private _router: Router) { }

  addChild(partTypeCode: string) {
    const lineItemId = this.part.lineItem.id;
    const projectId = this.part.lineItem.project.id;

    this._modelContext.addPart(this.part.lineItem.id, {
      partPath: this.part.path,
      partTypeCode: partTypeCode
    }).subscribe((path) => {
      this._router.navigate(['/project', projectId, 'item', lineItemId, 'part', path]);
    });
  }

  ngOnInit() {
  }

  ngOnChanges() {
    if (this.part) {
      this.bays = this.part.children.filter(c => c.partType === 'RowBay');
      // this.bays = this.sortBays(this.bays).reverse();
      this.frames = this.part.children.filter(c => c.partType === 'RowFrame');
    }
  }

  private sortBays(parts: Array<Part>) {
    return parts.sort((a, b): number => {
      const aVal = this.getPartRule(a, 'bayIndex').value;
      const bVal = this.getPartRule(b, 'bayIndex').value;
      if (aVal < bVal) {
        return -1;
      } else if (aVal > bVal) {
        return 1;
      }

      return  0;
    });
  }

  copy() {
    const lineItemId = this.part.lineItem.id;
    const projectId = this.part.lineItem.project.id;

    this._bomService.copyParts(this.part.lineItem.id, this.part.path)
    .subscribe((path) => {
        this._modelContext.loadLineItem(this.part.lineItem.id)
      .subscribe(loadedLi => {
        this._router.navigate(['/project', projectId, 'item', lineItemId, 'part', path]);
      });
    });
  }

  getPartRule(prt: Part, rulename: string) {
    const rules = prt.rules.filter(r => r.name === rulename);

    if (rules.length > 0) {
      return rules[0];
    } else {
      return null;
    }
  }

  private handleError(err) {
    this.showSnackbar(err.message);
    // this._busyService.removeRequest();
  }

  private showSnackbar(message: string): void {
    this._snackBar.open(message, null, { duration: 1000 });
  }

}
