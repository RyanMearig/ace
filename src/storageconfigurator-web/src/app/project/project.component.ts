import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { ModelContextService, NotificationService, ProjectLoadOptions } from '@ruler/core';
import { CommonDialogsService } from '@ruler/material';
import { StorageConfiguratorProject } from '../storage-configurator-project';
import { OutputsService } from '../outputs.service';
import { CreateOutputsDialogComponent } from '../create-outputs-dialog/create-outputs-dialog.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss']
})
export class ProjectComponent implements OnInit {
  project: StorageConfiguratorProject;

  constructor(private _modelContext: ModelContextService,
    private _route: ActivatedRoute,
    private _router: Router,
    private _dialog: MatDialog,
    private _dialogSvc: CommonDialogsService,
    private _outputsService: OutputsService,
    private _notificationService: NotificationService) { }

  get allowOutputs(): boolean {
    return (this.project && this.project.name && this.project.drawingNumber
      && this.project.lineItems && this.project.lineItems.length > 0) ? true : false;
  }

  closeProject() {
    this._notificationService.unsubscribeProject();
    this._modelContext.closeProject();
    this._router.navigate(['']);
  }

  showDraftOutputs() {
    this.showCreateOutputs('DRAFT', true, false, false, true);
  }

  showQuoteOutputs() {
    this.showCreateOutputs('QUOTE', true, false, false, false);
  }

  submitOrder() {
    // INTEGRATION WITH CSI
  }

  showCADOutputs() {
    this.showCreateOutputs('CAD', false, false, true, false);
  }

  showCreateOutputs(dialogTitle, disableElevations, disableBOMS, disableQuoteForm, watermarkDoc) {
    const dialogRef = this._dialog.open<CreateOutputsDialogComponent>(CreateOutputsDialogComponent,
                                      { data: { projectId: this.project.id, title: dialogTitle,
                                                disableElevations, disableBOMS, disableQuoteForm, watermarkDoc }});
    dialogRef.afterClosed().subscribe();
  }

  queueOutputs() {
    this._outputsService.queueOutputs(this.project.id)
      .subscribe(() => {
        this._dialogSvc.showAlert('Output job was successfully queued', 'Success');
      }, err => {
        this._dialogSvc.showAlert('An error occurred while queueing output job', 'Error');
      });
  }

  ngOnInit() {
    this._modelContext.project.subscribe(proj => {
      this.project = <StorageConfiguratorProject>proj;
    });

    this._route.params.pipe(
      switchMap((params: Params) => {
        const options = new ProjectLoadOptions();
        options.inflateLineItems = false;
        return this._modelContext.loadProject(params['id'], options);
      })
    )
    .subscribe(proj => {
      this._notificationService.subscribeProject(proj.id);
    }, () => this._router.navigate(['']));
  }
}
