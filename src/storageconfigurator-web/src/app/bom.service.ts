import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { HttpClient } from '@angular/common/http';
import { BusyService, AdhocChildCreateRequest } from '@ruler/core';
import { tap, catchError, map } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { AdhocChildCreateResponse } from '@ruler/core/lib/configurator/adhoc-child-create-response';

@Injectable({
  providedIn: 'root'
})
export class BomService {

  constructor(private _http: HttpClient, private _busyService: BusyService) { }

  getBom(lineItemId: string) {
    const url = `${environment.apiUrl}bom/${lineItemId}`;
    return this._http.get<any>(url);
  }

  getPartBom(lineItemId: string, bomPartPath: string) {
    const url = `${environment.apiUrl}bom/${lineItemId}/parts/${bomPartPath}`;
    return this._http.get<any>(url);
  }

  getRowBom(lineItemId: string) {
    const url = `${environment.apiUrl}rowbom/${lineItemId}`;
    return this._http.get<any>(url);
  }

  setLineItemDefaultMargins(lineItemId: string) {
    const url = `${environment.apiUrl}setDefaultMargins/${lineItemId}`;
    this._busyService.addRequest();

    return this._http.post(url, null, {withCredentials: true}).pipe(
      tap(() => this._busyService.removeRequest()),
      catchError((error: any) => this.handleError(error))
    );
  }

  setAllMargins(lineItemId: string, margin: number) {
    const url = `${environment.apiUrl}setAllMargins/${lineItemId}?margin=${margin}`;
    this._busyService.addRequest();

    return this._http.post(url, null, {withCredentials: true}).pipe(
      tap(() => this._busyService.removeRequest()),
      catchError((error: any) => this.handleError(error))
    );
  }

  setMarginsByPartNumber(lineItemId: string, partNumber: string, margin: number) {
    const url = `${environment.apiUrl}setMarginsByPartNumber/${lineItemId}?partNumber=${partNumber}&` +
                                                      `margin=${margin}`;
    this._busyService.addRequest();

    return this._http.post(url, null, {withCredentials: true}).pipe(
      tap(() => this._busyService.removeRequest()),
      catchError((error: any) => this.handleError(error))
    );
  }

  setItemByPartNumber(lineItemId: string, partNumber: string, item: string) {
    const url = `${environment.apiUrl}setItemByPartNumber/${lineItemId}?partNumber=${partNumber}&` +
                                                      `item=${item}`;
    this._busyService.addRequest();

    return this._http.post(url, null, {withCredentials: true}).pipe(
      tap(() => this._busyService.removeRequest()),
      catchError((error: any) => this.handleError(error))
    );
  }

  addFrameType(lineItemId: string, createRequest: AdhocChildCreateRequest): Observable<string> {
    this._busyService.addRequest();
    const url = `${environment.apiUrl}lineitems/${lineItemId}/addFrameType`;

    return this._http.post<AdhocChildCreateResponse>(url, createRequest, {withCredentials: true}).pipe(
      tap(() => this._busyService.removeRequest()),
      map(result => result.newChildPath),
      catchError((error: any) => this.handleError(error))
    );
  }

  addParts(lineItemId: string, qty: number, createRequest: AdhocChildCreateRequest): Observable<string> {
    this._busyService.addRequest();
    const url = `${environment.apiUrl}lineitems/${lineItemId}/addParts/${qty}`;

    return this._http.post<AdhocChildCreateResponse>(url, createRequest, {withCredentials: true}).pipe(
      tap(() => this._busyService.removeRequest()),
      catchError((error: any) => this.handleError(error))
    );
  }

  copyParts(lineItemId: string, partPath: string): Observable<string> {
    this._busyService.addRequest();
    const url = `${environment.apiUrl}lineitems/${lineItemId}/parts/${partPath}/Advance`;

    return this._http.post<AdhocChildCreateResponse>(url, null, {withCredentials: true}).pipe(
      tap(() => this._busyService.removeRequest()),
      map(latest => latest.newChildPath),
      catchError((error: any) => this.handleError(error))
    );
  }

  private handleError(error: any): Observable<any> {
    console.error(error);
    const returnError = (error && error.error) ? error.error :
      { message: 'An error occurred', details: '' };
    return throwError(returnError);
  }
}
