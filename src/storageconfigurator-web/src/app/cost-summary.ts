export class CostSummary {
  lineItemId: string;
  name: string;
  description: string;
  cost: number;
  margin: number;
  include: boolean;
  qty: number;
  extraData: string;
}
