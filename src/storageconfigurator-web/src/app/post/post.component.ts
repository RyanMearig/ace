import { Component, OnInit, OnChanges, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Part, PartType, Rule, LineItem, ModelContextService } from '@ruler/core';
import {MatTabsModule } from '@angular/material/tabs';

@Component({
  selector: 'post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit, OnChanges {

  @Input()
  part: Part;

  constructor(private _modelContext: ModelContextService,
    private _router: Router) { }

  ngOnInit() {
  }

  ngOnChanges() {
  }

}
