import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShelfLoadBeamComponent } from './shelf-load-beam.component';

describe('ShelfLoadBeamComponent', () => {
  let component: ShelfLoadBeamComponent;
  let fixture: ComponentFixture<ShelfLoadBeamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShelfLoadBeamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShelfLoadBeamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
