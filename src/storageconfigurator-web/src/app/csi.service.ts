import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { HttpClient } from '@angular/common/http';
import { ModelContextService, BusyService, RulerConfig } from '@ruler/core';
import { Observable, throwError } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

@Injectable()
export class CsiService {

  constructor(private _http: HttpClient, private _busyService: BusyService,
    private _modelContext: ModelContextService) { }

  getSteelSurcharge(): Observable<number> {
    const url = `${environment.apiUrl}csi/getSteelSurcharge`;
    this._busyService.addRequest();

    return this._http.get<object>(url).pipe(
      tap(() => this._busyService.removeRequest()),
      catchError((error: any) => this.handleError(error))
    );
  }

  getCustomerDefault(custNum: string) {
    return this.getCustomer(custNum, 0);
  }

  getCustomer(custNum: string, custSeq: number): Observable<object> {
    const url = `${environment.apiUrl}csi/customer/${custNum}/${custSeq}`;
    this._busyService.addRequest();

    return this._http.get<object>(url).pipe(
      tap(() => this._busyService.removeRequest()),
      catchError((error: any) => this.handleError(error))
    );
  }

  getCustomerSequences(custNum: string): Observable<Array<number>> {
    const url = `${environment.apiUrl}csi/customer/${custNum}/getSequences`;
    this._busyService.addRequest();

    return this._http.get<Array<number>>(url).pipe(
      tap(() => this._busyService.removeRequest()),
      catchError((error: any) => this.handleError(error))
    );
  }

  private handleError(error: any): Observable<any> {
    // TODO: Add more robust error handling
    console.error(error);

    const returnError = (error && error.error) ? error.error :
      { message: 'An error occurred', details: '' };

    this._busyService.removeRequest();
    return throwError(returnError);
  }
}
