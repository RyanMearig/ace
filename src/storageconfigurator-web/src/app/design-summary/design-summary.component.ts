import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { ModelContextService } from '@ruler/core';
import { StorageConfiguratorProject } from '../storage-configurator-project';
import { MatDialog } from '@angular/material/dialog';
import { AddRevisionDialogComponent, ViewRevisionsDialogComponent, CommonDialogsService } from '@ruler/material';

@Component({
  selector: 'app-design-summary',
  templateUrl: './design-summary.component.html',
  styleUrls: ['./design-summary.component.scss']
})
export class DesignSummaryComponent implements OnInit {
  projects: Array<StorageConfiguratorProject>;
  pageNumber: number = 1;
  pageSize: number;
  startRecord: number;
  endRecord: number;
  totalCount: number;
  allowNext: boolean = false;
  allowPrevious: boolean = false;
  searchCriteria = new FormControl();

  constructor(private _modelContext: ModelContextService,
    private _router: Router,
    private _dialog: MatDialog,
    private _dialogSvc: CommonDialogsService) {
      this.searchCriteria.valueChanges.pipe(
        debounceTime(500),
        distinctUntilChanged()
      )
      .subscribe(searchString => {
        this.refreshProjects();
      });
    }

  createProject() {
    this._modelContext.createProject()
      .subscribe(project => {
        this.openProject(project.id);
      });
  }

  openProject(id: string) {
    this._router.navigate(['/project', id]);
  }

  copyProject(id: string) {
    this._modelContext.copyProject(id)
      .subscribe(() => this.refreshProjects());
  }

  deleteProject(id: string) {
    this._dialogSvc.showConfirmation('Are you sure you would like to delete this project?', 'Are You Sure?').subscribe(confirmed => {
      if (confirmed === true) {
        this._modelContext.deleteProject(id)
          .subscribe(() => this.refreshProjects());
      }
    });
  }

  createRevision(id: string) {
    const dialogRef = this._dialog.open<AddRevisionDialogComponent>(AddRevisionDialogComponent, { data: { projectId: id}});
    dialogRef.afterClosed()
      .subscribe(revisionId => {
        if (revisionId) {
          this.refreshProjects();
        }
      });
  }

  viewRevisions(id: string) {
    const dialogRef = this._dialog.open<ViewRevisionsDialogComponent>(ViewRevisionsDialogComponent, { data: { projectId: id } });

    dialogRef.afterClosed()
      .subscribe(result => {
        if (result && result.dirty === true) {
          this.refreshProjects();
        }
      });
  }

  nextPage() {
    if (this.allowNext) {
      this.pageNumber++;
      this.refreshProjects();
    }
  }

  previousPage() {
    if (this.allowPrevious) {
      this.pageNumber--;
      this.refreshProjects();
    }
  }

  private refreshProjects() {
    this._modelContext.searchProjects(this.searchCriteria.value, this.pageNumber)
      .subscribe(result => {
        this.projects = <StorageConfiguratorProject[]>result.projects;
        this.totalCount = result.totalCount;
        this.updatePaging();
      });
  }

  private updatePaging() {
    this.startRecord = ((this.pageNumber - 1) * this.pageSize) + 1;
    this.endRecord = this.startRecord + (this.projects.length - 1);
    this.allowNext = (this.endRecord < this.totalCount);
    this.allowPrevious = (this.pageNumber > 1);
  }

  ngOnInit() {
    this.pageSize = this._modelContext.projectSearchPageSize;
    this.refreshProjects();
  }
}
