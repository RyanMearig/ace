import { Component, OnInit, AfterViewInit } from '@angular/core';
import { distinctUntilChanged } from 'rxjs/operators';
import { BusyService } from '@ruler/core';
import { ClientSettingsService } from '@ruler/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {
  isBusy = false;
  environment: string;
  themeClass: string;

  constructor(private _busyService: BusyService, private _clientSettingsService: ClientSettingsService) {
    this.environment = _clientSettingsService.settings.environment;

/*     if (this.environment === 'Development') {
      this.themeClass = 'test-theme';
    } */
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    // setTimeout is required to avoid ExpressionChangedAfterItHasBeenCheckedError
    //  This is being tracked in https://github.com/angular/angular/issues/17572 and might
    //  eventually allow just binding busy indicator directly to busyService.isBusy
    this._busyService.isBusy.pipe(
      distinctUntilChanged()
    )
    .subscribe(isBusy => setTimeout(() => this.isBusy = isBusy));
  }
}
