
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import { ModelContextService, BusyService } from '@ruler/core';
import { environment } from '../environments/environment';

@Injectable()
export class OutputsService {

  constructor(private _http: HttpClient, private _busyService: BusyService,
    private _modelContext: ModelContextService) {}

  queueOutputs(id: string): Observable<string> {
    const url = `${environment.apiUrl}projects/${id}/outputs`;
    this._busyService.addRequest();

    return this._http.post(url, null, {withCredentials: true}).pipe(
      tap(() => this._busyService.removeRequest()),
      catchError((error: any) => this.handleError(error))
    );
  }

  downloadOutputs(projectId: string, includeQuoteForm: boolean, includeCustomerBOMSum: boolean,
                  includeInternalBOMSum: boolean, includeRowBOMSum: boolean, includeBOMDetail: boolean) {

    const url = `${environment.apiUrl}projects/${projectId}/downloadOutputs?` +
    `includeQuoteForm=${includeQuoteForm}&` +
    `includeCustomerBOMSummary=${includeCustomerBOMSum}&` +
    `includeInternalBOMSummary=${includeInternalBOMSum}&` +
    `includeRowBOMSummary=${includeRowBOMSum}&` +
    `includeBOMDetail=${includeBOMDetail}`;

    this.downloadZipFile(url, 'application/zip');
  }

  private downloadZipFile(url: string, contentType: string) {
    this._busyService.addRequest();

    const headers = new HttpHeaders().set('Accept', contentType);
    this._http.get(url, { responseType: 'arraybuffer', headers: headers, observe: 'response' })
      .subscribe(resp => {
        const blob = new Blob([resp.body], { type: contentType });
        const localUrl = window.URL.createObjectURL(blob);
        const anchor = document.createElement('a');
        const nameRegEx = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
        const matches = nameRegEx.exec(resp.headers.get('content-disposition'));
        const filename = (matches && matches[1]) ? matches[1].split('"').join('') : 'download';

        anchor.download = filename;
        anchor.href = localUrl;
        anchor.click();
        anchor.remove();
      }, undefined, () => this._busyService.removeRequest());
  }

  private handleError(error: any): Observable<any> {
    // TODO: Add more robust error handling
    console.error(error);

    const returnError = (error && error.error) ? error.error :
      { message: 'An error occurred', details: '' };

    this._busyService.removeRequest();
    return throwError(returnError);
  }
}
