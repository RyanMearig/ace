import { Component, OnInit, Input } from '@angular/core';
import { Part} from '@ruler/core';

@Component({
  selector: 'elevations',
  templateUrl: './elevations.component.html',
  styleUrls: ['./elevations.component.scss']
})
export class ElevationsComponent implements OnInit {

  constructor() { }
  @Input()
  part: Part;
  elevations: Array<Part>;

  ngOnInit() {
    this.getElevations();
  }

  getElevations() {
    if (this.part) {
      this.elevations = this.part.children;
    }
  }
}
