import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElevationsComponent } from './elevations.component';

describe('ElevationsComponent', () => {
  let component: ElevationsComponent;
  let fixture: ComponentFixture<ElevationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElevationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElevationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
