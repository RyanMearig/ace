import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShelfBeamCrossbarComponent } from './shelf-beam-crossbar.component';

describe('CustomPartComponent', () => {
  let component: ShelfBeamCrossbarComponent;
  let fixture: ComponentFixture<ShelfBeamCrossbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShelfBeamCrossbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShelfBeamCrossbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
