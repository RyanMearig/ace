import { Routes, RouterModule } from '@angular/router';
import { DesignSummaryComponent } from './design-summary/design-summary.component';
import { ProjectComponent } from './project/project.component';
import { ProjectDetailsComponent } from './project-details/project-details.component';
import { LineItemComponent } from './line-item/line-item.component';
import { AuthGuardService } from '@ruler/core';

const routes: Routes = [
  {
    path: '',
    component: DesignSummaryComponent,
    pathMatch: 'full',
    canActivate: [AuthGuardService]
  },
  {
    path: 'project/:id',
    component: ProjectComponent,
    canActivate: [AuthGuardService],
    children: [
      {
        path: '',
        component: ProjectDetailsComponent,
        canActivate: [AuthGuardService]
      },
      {
        path: 'item/:id',
        component: LineItemComponent,
        canActivate: [AuthGuardService]
      },
      {
        path: 'item/:id/part/:path',
        component: LineItemComponent,
        canActivate: [AuthGuardService]
      }
    ]
  },
  {
    path: 'index.html',
    redirectTo: ''
  },
  {
    path: '**',
    redirectTo: ''
  }
];

export const appRoutingProviders: any[] = [];

export const routing = RouterModule.forRoot(routes);
