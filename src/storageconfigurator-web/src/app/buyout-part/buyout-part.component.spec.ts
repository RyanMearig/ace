import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyoutPartComponent } from './buyout-part.component';

describe('BuyoutPartComponent', () => {
  let component: BuyoutPartComponent;
  let fixture: ComponentFixture<BuyoutPartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuyoutPartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyoutPartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
