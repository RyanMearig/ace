import { Component, OnInit, OnChanges, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Part, PartType, Rule, LineItem, ModelContextService } from '@ruler/core';
import {MatTabsModule } from '@angular/material/tabs';

@Component({
  selector: 'buyout-part',
  templateUrl: './buyout-part.component.html',
  styleUrls: ['./buyout-part.component.scss']
})
export class BuyoutPartComponent implements OnInit, OnChanges {

  @Input()
  part: Part;

  customLaborItems: Array<Part>;

  constructor(private _modelContext: ModelContextService,
    private _router: Router) { }

  ngOnInit() {

  }

  ngOnChanges() {
    if (this.part) {
      this.customLaborItems = this.part.children.filter(c => c.partType === 'CustomLaborItem');
    }
  }

  addChild(partTypeCode: string) {
    const lineItemId = this.part.lineItem.id;
    const projectId = this.part.lineItem.project.id;

    this._modelContext.addPart(this.part.lineItem.id, {
      partPath: this.part.path,
      partTypeCode: partTypeCode
    }).subscribe((path) => {
      this._router.navigate(['/project', projectId, 'item', lineItemId, 'part', path]);
    });
  }

  copy() {
    const lineItemId = this.part.lineItem.id;
    const projectId = this.part.lineItem.project.id;

    this._modelContext.copyPart(this.part.lineItem.id, this.part.path)
      .subscribe((path) => {
        this._router.navigate(['/project', projectId, 'item', lineItemId, 'part', path]);
      });
  }

}
