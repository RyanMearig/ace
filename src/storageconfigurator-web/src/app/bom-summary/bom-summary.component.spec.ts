import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BomSummaryComponent } from './bom-summary.component';

describe('BomSummaryComponent', () => {
  let component: BomSummaryComponent;
  let fixture: ComponentFixture<BomSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BomSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BomSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
