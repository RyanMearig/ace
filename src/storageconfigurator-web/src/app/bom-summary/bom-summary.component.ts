import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { BomService } from '../bom.service';
import { Part, PartType, ModelContextService } from '@ruler/core';

@Component({
  selector: 'bom-summary',
  templateUrl: './bom-summary.component.html',
  styleUrls: ['./bom-summary.component.scss']
})
export class BomSummaryComponent implements OnInit {

  @Input()
  part: Part;

  @Input()
  useRowBom = false;

  bomSummary: any;
  allLaborCategories: Array<string> = new Array<string>();

  constructor(private _modelContext: ModelContextService,
    private _bomService: BomService,
    private _router: Router) { }

  ngOnInit() {
    if (this.useRowBom) { this.getRowBom(); } else { this.getBom(); }
  }

  ngOnChanges() {
    if (this.useRowBom) { this.getRowBom(); } else { this.getBom(); }
  }

  getBom() {
    this._bomService.getBom(this.part.lineItem.id)
      .subscribe(bomSummary => {
        this.bomSummary = bomSummary; this.getAllLaborCategories(bomSummary);
      });
  }

  getAllLaborCategories(bom: any) {
    if (bom) {
      bom.labor.map(l => {
          const category = l.category;
          if (category && !this.allLaborCategories.includes(category)) {
            this.allLaborCategories.push(category);
          }
      });
      bom.children.map(c => this.getAllLaborCategories(c));
    }
  }

  getRowBom() {
    this._bomService.getRowBom(this.part.lineItem.id)
      .subscribe(bomSummary => {
        this.bomSummary = bomSummary; this.getAllLaborCategories(bomSummary);
      });
  }

  getFirstLevelChildBomDepth() {
    if (this.useRowBom) { return 2; } else { return 1; }
  }

}
