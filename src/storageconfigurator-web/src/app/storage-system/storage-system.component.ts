import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { BomService } from '../bom.service';
import { Part, PartType, ModelContextService } from '@ruler/core';
import { MatSnackBar, MatDialog } from '@angular/material';
import { CommonDialogsService } from '@ruler/material';
import { AddMultiPartsDialogComponent } from '../add-multi-parts-dialog/add-multi-parts-dialog.component';

@Component({
  selector: 'storage-system',
  templateUrl: './storage-system.component.html',
  styleUrls: ['./storage-system.component.scss']
})
export class StorageSystemComponent implements OnInit {

  @Input()
  part: Part;

  rows: Array<Part>;
  bomChildren: Array<Part>;
  distinctBomChildrenByPath: Array<Part>;
  distinctBomChildren: Array<Part>;

  constructor(private _modelContext: ModelContextService,
    private _bomService: BomService,
    private _dialogSvc: CommonDialogsService,
    private _dialog: MatDialog,
    private _router: Router,
    private _snackBar: MatSnackBar) { }

  ngOnInit() {
  }

  ngOnChanges() {
    if (this.part) {
      this.rows = this.part.children.filter(c => c.partType === 'RowProxy');
      // tslint:disable-next-line:no-string-literal
      this.distinctBomChildren = this.formatPartArray(this.part['orderedDistinctBomChildrenByPath']);
      // this.distinctBomChildren = this.formatPartArray(this.part.rules.filter(r => r.name === 'orderedDistinctBomChildrenByPath')[0].value);
    }
  }

  addChild(partTypeCode: string) {
    const lineItemId = this.part.lineItem.id;
    const projectId = this.part.lineItem.project.id;

    if (partTypeCode === 'rowproxy') {
      this.showMultiPartDialog(partTypeCode);
    } else {
      this._modelContext.addPart(this.part.lineItem.id, {
        partPath: this.part.path,
        partTypeCode: partTypeCode
      }).subscribe((path) => {
        this._router.navigate(['/project', projectId, 'item', lineItemId, 'part', path]);
      });
    }
  }

  showMultiPartDialog(prtTypeCode: string) {
    const dialogRef = this._dialog.open<AddMultiPartsDialogComponent>(AddMultiPartsDialogComponent,
                                      {data: { part: this.part, partTypeCode: prtTypeCode }});
    dialogRef.afterClosed().subscribe();
  }

  formatPartArray(prtPathArray: Array<string>) {
    if (prtPathArray !== undefined && prtPathArray !== null) {
      return prtPathArray.map(c => this.getPartByPath(c));
    }
    return null;
  }

  getPartByPath(path: string) {
    return this.part.lineItem.root.findPart(path);
  }

  getPartRule(prt: Part, rulename: string) {
    const rules = prt.rules.filter(r => r.name === rulename);

    if (rules.length > 0) {
      return rules[0];
    } else {
      return null;
    }
  }

  setDefaultMargins() {
    this._bomService.setLineItemDefaultMargins(this.part.lineItem.id)
      .subscribe(() => {
        this._modelContext.updateLineItem({
          id: this.part.lineItem.id,
          name: this.part.lineItem.name
        }).subscribe();
      });
  }

  addChildBtnDisabled(childType: string) {
    if (childType === 'rowproxy') {
      return this.getPartRule(this.part, 'bayQty').value === 0 ||
      this.getPartRule(this.part, 'frameLineQty').value === 0 ||
      this.getPartRule(this.part, 'rowTypeQty').value === 0;
    }
    return false;
  }

  setNameValue(rule, value, element) {
    const lineItem = this.part.lineItem;

    this._modelContext.setRuleValue(rule, value)
      .subscribe(null, err => this.handleError(rule, err, element));

    if (value !== lineItem.name) {
      this._modelContext.updateLineItem({
        id: lineItem.id,
        name: value
      }).subscribe();
    }
  }

  private handleError(rule, err, element) {
    // Revert to previous value
    if (element) {
      element.value = rule.value;
    }

    // Show message to user
    this.showSnackbar(err.message);
  }

  private showSnackbar(message: string): void {
    this._snackBar.open(message, null, { duration: 1000 });
  }

}
