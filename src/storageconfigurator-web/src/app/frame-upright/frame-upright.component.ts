import { Component, OnInit, OnChanges, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Part, PartType, Rule, LineItem, ModelContextService } from '@ruler/core';
import {MatTabsModule } from '@angular/material/tabs';

@Component({
  selector: 'frame-upright',
  templateUrl: './frame-upright.component.html',
  styleUrls: ['./frame-upright.component.scss']
})
export class FrameUprightComponent implements OnInit, OnChanges {

  @Input()
  part: Part;
  HorizontalBraces: Array<Part>;
  HeavyHorizontalBraces: Array<Part>;
  DiagonalBraces: Array<Part>;

  constructor(private _modelContext: ModelContextService,
    private _router: Router) { }

  ngOnInit() {
    if (this.part) {
      this.HorizontalBraces = this.part.children.filter(c => c.partType === 'HorizontalBrace').reverse();
      this.HeavyHorizontalBraces = this.part.children.filter(c => c.partType === 'HeavyHorizontalBrace'
                                                            && !c.displayName.includes('Dblr')).reverse();
      this.DiagonalBraces = this.part.children.filter(c => c.partType === 'DiagonalBrace').reverse();
    }
  }

  ngOnChanges() {
    if (this.part) {
      this.HorizontalBraces = this.part.children.filter(c => c.partType === 'HorizontalBrace').reverse();
      this.HeavyHorizontalBraces = this.part.children.filter(c => c.partType === 'HeavyHorizontalBrace'
                                                            && !c.displayName.includes('Dblr')).reverse();
      this.DiagonalBraces = this.part.children.filter(c => c.partType === 'DiagonalBrace').reverse();
    }
  }

  getBraceDisplayName(display: string): string {
    if (display.endsWith(' 1')) {
      return display.replace(' 1', ' Bottom');
    }
    return display;
  }

  addChild(partTypeCode: string) {
    const lineItemId = this.part.lineItem.id;
    const projectId = this.part.lineItem.project.id;

    this._modelContext.addPart(this.part.lineItem.id, {
      partPath: this.part.path,
      partTypeCode: partTypeCode
    }).subscribe((path) => {
      this._router.navigate(['/project', projectId, 'item', lineItemId, 'part', path]);
    });
  }

}
