import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrameUprightComponent } from './frame-upright.component';

describe('FrameUprightComponent', () => {
  let component: FrameUprightComponent;
  let fixture: ComponentFixture<FrameUprightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrameUprightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrameUprightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
