import { Project } from '@ruler/core';

export class StorageConfiguratorProject extends Project {
  name: string;
  drawingNumber: string;

  advancedSalesName: string;
  advancedSalesContactEmail: string;
  advancedSalesContactPhone: string;
  advancedProjectManagerName: string;

  shipToCustomerSeq: string;
  jobsiteName: string;
  jobsiteAddress: string;
  jobsiteCity: string;
  jobsiteState: string;
  jobsiteZip: string;
  jobsiteContactName: string;
  jobsiteContactPhone: string;
  jobsiteContactEmail: string;

  csiCustomerNumber: string;
  customerName: string;
  customerAddress: string;
  customerCity: string;
  customerState: string;
  customerZip: string;
  customerComments: string;
  customerContactName: string;
  customerContactPhone: string;
  customerContactEmail: string;
  leadTime: string;
  terms: string;
  customerType: string;

  seismicEngName: string;
  seismicEngAddress: string;
  seismicEngCity: string;
  seismicEngState: string;
  seismicEngZip: string;
  seismicEngContactName: string;
  seismicEngContactPhone: string;
  seismicEngContactEmail: string;

  jobStatus: string;
  expectedCloseDate: string;
  expectedShipDate: string;
  winPercentage: string;

  steelSurcharge: number;

  mfdMaterialCost: number;
  mfdMaterialCalcPrice: number;
  mfdMaterialPrice: number;
  buyoutMaterialCost: number;
  buyoutMaterialCalcPrice: number;
  buyoutMaterialPrice: number;
  materialTaxed: boolean;

  engCalcPrice: number;
  engPrice: number;
  engCost: number;
  engMarginPercent: number;
  engTaxed: boolean;

  installCalcPrice: number;
  installPrice: number;
  installCost: number;
  installMarginPercent: number;
  installTaxed: boolean;

  freightCalcPrice: number;
  freightPrice: number;
  freightCost: number;
  freightMarginPercent: number;
  freightTaxed: boolean;

  otherCalcPrice: number;
  otherPrice: number;
  otherCost: number;
  otherMarginPercent: number;
  otherTaxed: boolean;

  taxRate: number;
  totalCalcPrice: number;
  totalPrice: number;
  totalCost: number;
  totalMarginPercent: number;
  totalWTaxes: number;
  totalAdjustment: number;

  holdPrice: boolean;
  paymentMilestonesJson: string;
}
