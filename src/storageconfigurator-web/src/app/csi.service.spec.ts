import { TestBed } from '@angular/core/testing';

import { CsiService } from './csi.service';

describe('CsiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CsiService = TestBed.get(CsiService);
    expect(service).toBeTruthy();
  });
});
