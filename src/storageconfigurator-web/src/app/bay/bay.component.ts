import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Part, PartType, ModelContextService } from '@ruler/core';
import { MatSnackBar } from '@angular/material';
import { BomService } from '../bom.service';

@Component({
  selector: 'bay',
  templateUrl: './bay.component.html',
  styleUrls: ['./bay.component.scss']
})
export class BayComponent implements OnInit {

  @Input()
  part: Part;

  levels: Array<Part>;

  constructor(private _modelContext: ModelContextService, private _snackBar: MatSnackBar,
    private _bomService: BomService,
    private _router: Router) { }

  addChild(partTypeCode: string) {
    const lineItemId = this.part.lineItem.id;
    const projectId = this.part.lineItem.project.id;

    this._modelContext.addPart(this.part.lineItem.id, {
      partPath: this.part.path,
      partTypeCode: partTypeCode
    }).subscribe((path) => {
      this._router.navigate(['/project', projectId, 'item', lineItemId, 'part', path]);
    });
  }

  ngOnInit() {
  }

  ngOnChanges() {
    if (this.part) {
      this.levels = this.part.children.filter(c => c.partType === 'SelectiveLevel' || c.partType === 'DoubleDeepLevel').reverse();
    }
  }

  copy() {
    const lineItemId = this.part.lineItem.id;
    const projectId = this.part.lineItem.project.id;

    this._bomService.copyParts(this.part.lineItem.id, this.part.path)
    .subscribe((path) => {
        this._modelContext.loadLineItem(this.part.lineItem.id)
      .subscribe(loadedLi => {
        this._router.navigate(['/project', projectId, 'item', lineItemId, 'part', path]);
      });
    });
  }

  private handleError(err) {
    this.showSnackbar(err.message);
    // this._busyService.removeRequest();
  }

  private showSnackbar(message: string): void {
    this._snackBar.open(message, null, { duration: 1000 });
  }

}
