import { Component, OnInit, Input } from '@angular/core';
import { Part, ModelContextService } from '@ruler/core';
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'bom-summary-view',
  templateUrl: './bom-summary-view.component.html',
  styleUrls: ['./bom-summary-view.component.scss'],
  providers: [DecimalPipe]
})
export class BomSummaryViewComponent implements OnInit {

  constructor(private _modelContext: ModelContextService, private _decimalPipe: DecimalPipe) { }

  @Input()
  bomSummaryItem: any;

  @Input()
  bomDepth: number;

  @Input()
  laborCategories: Array<string>;

  @Input()
  useQtyFactor: boolean = true;

  @Input()
  hideChildrenSubComponents = false;

  @Input()
  firstLevelChildBomDepth = 1;

  bomIndent: string;
  qtyFactor: number;

  ngOnInit() {
  }

  ngOnChanges() {
    this.qtyFactor = this.useQtyFactor && this.bomSummaryItem !== undefined ? this.bomSummaryItem.qty : 1;
    this.bomIndent = '';
    for (let i = 0; i < this.bomDepth; i++) {
      this.bomIndent = this.bomIndent + '\xa0\xa0\xa0';
    }
  }

  hoursInCategory(laborItem: any, category: string): number {
    if (laborItem.category === category) {
      return laborItem.hours;
    }
    return null;
  }

  getTotalLaborByCategory(bomItem: any, category: string): number {
    let hours = 0;
    if (bomItem === undefined) { return hours; }
    const labor = bomItem.labor.filter(l => l.category === category);
    hours = labor.reduce((hours, current) => hours + current.hours, 0);

    bomItem.children.map(c => hours = hours + (this.getTotalLaborByCategory(c, category) * c.qty ));
    if (hours === 0) { return null; }
    return hours;
  }

  getUnitMargin(margin: number): string {
    const unitMargin = this._decimalPipe.transform(margin, '1.1-3');

    if (this.bomDepth <= this.firstLevelChildBomDepth ) {
      return unitMargin + '%';
    } else {
      return '';
    }
  }

  getScrapPercentage(costPerWeight: number, costPerRawWeight: number): string {
    if (this.bomSummaryItem === undefined || this.bomSummaryItem.isPurchased) { return ''; }
    const scrap = ((costPerRawWeight - costPerWeight) / costPerWeight) * 100;
    if (scrap <= 0) { return ''; }
    return this._decimalPipe.transform(scrap, '1.1-3') + '%';
  }

  getRawWeight(): string {
    if (this.bomSummaryItem === undefined || this.bomSummaryItem.isPurchased) { return ''; }
    return this._decimalPipe.transform(this.bomSummaryItem.totalRawWeight, '1.1-2');
  }

  getTotalPrice(price: string): string {
    if (this.bomDepth <= this.firstLevelChildBomDepth ) {
      return price;
    } else {
      return '';
    }
  }

}
