import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BomSummaryViewComponent } from './bom-summary-view.component';

describe('BomSummaryViewComponent', () => {
  let component: BomSummaryViewComponent;
  let fixture: ComponentFixture<BomSummaryViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BomSummaryViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BomSummaryViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
