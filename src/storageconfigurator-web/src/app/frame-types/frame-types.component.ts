import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Part, PartType, ModelContextService, BusyService } from '@ruler/core';
import { MatSnackBar } from '@angular/material';
import { BomService } from '../bom.service';

@Component({
  selector: 'frame-types',
  templateUrl: './frame-types.component.html',
  styleUrls: ['./frame-types.component.scss']
})
export class FrameTypesComponent implements OnInit {

  @Input()
  part: Part;

  frameTypes: Array<Part>;

  constructor(private _modelContext: ModelContextService,
              private _router: Router,
              private _busyService: BusyService,
              private _bomService: BomService,
              private _snackBar: MatSnackBar) { }

  ngOnInit() {
  }

  ngOnChanges() {
    if (this.part) {
      this.frameTypes = this.part.children.filter(c => c.partType === 'SelectiveFrameType' || c.partType === 'DoubleDeepFrameType');
    }
  }

  addFrameType(partTypeCode: string) {
    this._busyService.addRequest();
    this._bomService.addFrameType(this.part.lineItem.id, {
      partPath: this.part.path,
      partTypeCode: partTypeCode
    }).subscribe((path) => {
      this._modelContext.loadLineItem(this.part.lineItem.id)
      .subscribe(loadedLi => {
        this._busyService.removeRequest();
        this._router.navigate(['/project', this.part.lineItem.project.id, 'item', this.part.lineItem.id, 'part', path]);
      });
    }, err => this.handleError(err)
    );
  }

  private handleError(err) {
    // Show message to user
    this.showSnackbar(err.message);
    this._busyService.removeRequest();
  }

  private showSnackbar(message: string): void {
    this._snackBar.open(message, null, { duration: 1000 });
  }

}
