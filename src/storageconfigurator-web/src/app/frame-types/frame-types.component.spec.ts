import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrameTypesComponent } from './frame-types.component';

describe('FrameTypesComponent', () => {
  let component: FrameTypesComponent;
  let fixture: ComponentFixture<FrameTypesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrameTypesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrameTypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
