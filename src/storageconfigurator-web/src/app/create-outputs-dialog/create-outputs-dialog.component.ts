import { Component, OnInit, OnDestroy, Input, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Observable ,  Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { ModelContextService, Project, LineItemType, LineItem } from '@ruler/core';
import { OutputsService } from '../outputs.service';
import { CommonDialogsService } from '@ruler/material';

@Component({
  selector: 'create-outputs-dialog',
  templateUrl: './create-outputs-dialog.component.html',
  styleUrls: ['./create-outputs-dialog.component.scss']
})
export class CreateOutputsDialogComponent implements OnInit, OnDestroy {
private _projectId: string;
subscriptions: Array<Subscription> = [];
title: string;
createQuoteForm = false;
disableQuoteForm: boolean;
createInternalBOMSummary = false;
disableInternalBOMSummary: boolean;
createCustomerBOMSummary = false;
disableCustomerBOMSummary: boolean;
createRowBOMSummary = false;
createBOMDetail = false;
disableBOMDetail: boolean;
createElevations: boolean;
disableElevations: boolean;
watermarkDoc: false;

constructor(private _modelContext: ModelContextService,
            private _outputsService: OutputsService,
            private _dialogSvc: CommonDialogsService,
            public dialogRef: MatDialogRef<CreateOutputsDialogComponent>,
            @Inject(MAT_DIALOG_DATA) data: any) {
              this._projectId = data.projectId;
              this.title = data.title;

              const disableBOMS = data.disableDownloads;
              this.disableBOMDetail = disableBOMS;
              this.disableInternalBOMSummary = disableBOMS;
              this.disableCustomerBOMSummary = disableBOMS;
              this.disableQuoteForm = data.disableQuoteForm;
              this.disableElevations = data.disableElevations;
              this.watermarkDoc = data.watermarkDoc;
            }

ngOnInit() {
    // this.subscriptions.push(this._modelContext.project.subscribe(proj => this.project = proj));
  }

downloadOutputs() {
  this._outputsService.downloadOutputs(this._projectId, this.createQuoteForm,
    this.createCustomerBOMSummary, this.createInternalBOMSummary, this.createRowBOMSummary, this.createBOMDetail);
}

get allowDownloadOutputs(): boolean {
  return this.createBOMDetail ||
          this.createInternalBOMSummary ||
          this.createCustomerBOMSummary ||
          this.createRowBOMSummary ||
          this.createQuoteForm;
}

queueOutputs() {
  if (this.createElevations) {
    this._outputsService.queueOutputs(this._projectId)
    .subscribe(() => {
      this._dialogSvc.showAlert('Output job was successfully queued', 'Success');
    }, err => {
      this._dialogSvc.showAlert('An error occurred while queueing output job', 'Error');
    });
  }
  }

ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe);
  }
}
