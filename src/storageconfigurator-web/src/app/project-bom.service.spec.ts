import { TestBed } from '@angular/core/testing';

import { ProjectBomService } from './project-bom.service';

describe('ProjectBomService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProjectBomService = TestBed.get(ProjectBomService);
    expect(service).toBeTruthy();
  });
});
