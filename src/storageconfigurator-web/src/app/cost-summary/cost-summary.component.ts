import { Component, OnInit, OnChanges, Input } from '@angular/core';
import { Part, ModelContextService, BusyService } from '@ruler/core';
import { BomService } from '../bom.service';
import { MatSnackBar } from '@angular/material';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'cost-summary',
  templateUrl: './cost-summary.component.html',
  styleUrls: ['./cost-summary.component.scss']
})
export class CostSummaryComponent implements OnInit, OnChanges {

  @Input()
  part: Part;

  @Input()
  isLineItem: boolean;


  bomSummary: any;
  public na = 'N/A';
  qtyFactor: number;
  weldHours = 0;
  allWeldLaborItems: Array<any> = new Array<any>();
  margin: number;
  previousMargin: number;


  constructor(private _modelContext: ModelContextService,
              private _bomService: BomService,
              private _snackBar: MatSnackBar,
              private _busyService: BusyService) { }

  ngOnInit() {
              this.isLineItem ? this.getBom() : this.getPartBom();
              if (this.isMajorItem(this.part)) { this.margin = this.getMargin(this.part); }
              this.previousMargin = this.margin;
              }

  ngOnChanges() {
                this.qtyFactor = this.bomSummary !== undefined ? this.bomSummary.qty : 1;
                this.isLineItem ? this.getBom() : this.getPartBom();
                if (this.isMajorItem(this.part)) { this.margin = this.getMargin(this.part); }
              }

  setValue(rule, value, element) {
                this._modelContext.setRuleValue(rule, value)
                  .subscribe(null, err => this.handleError(rule, err, element));
              }

  setBomMargins(value, element) {
    if (!(value < 100)) {
      this.showSnackbar('Margin must be less than 100');
      element.value = this.previousMargin.toLocaleString('en-us', {minimumFractionDigits: 2});
    } else {
      this.previousMargin = value;
      if (this.isMajorItem(this.part)) {
        this._busyService.addRequest();
        this._bomService.setAllMargins(this.part.lineItem.id, value)
          .subscribe(null, err => this.handleError(null, err, element),
          () =>
            this._modelContext.loadLineItem(this.part.lineItem.id)
            .subscribe(loadedLi => {
              this._busyService.removeRequest();
            })
          );
      } else {
        const allBoms = this.part.children.map(c => this.getBomPart(c));
        allBoms.map( b => {
          // this._busyService.addRequest();
          const rule = b.rules.filter(r => r.name === 'margin')[0];
          this._modelContext.setRuleValue(rule, value)
          .subscribe(null, err => this.handleError(rule, err, element));
        });
    }

  }

  }

  getAllFirstLevelBoms(part: Part) {
    return part.children.map(c => this.getBomPart(c));
  }

  setUnitCostAndPrice(value, element, costRule, priceRule) {
    if (this.bomSummary !== undefined) {
      const cost = this.bomSummary.unitCost;
      this._modelContext.setRuleValue(costRule, cost)
      .subscribe(null, err => this.handleError(costRule, err, element));
      this._modelContext.setRuleValue(priceRule, value)
        .subscribe(null, err => this.handleError(priceRule, err, element));
    }
  }

  readOnly(rule): boolean {
                return !this.showInfo() || rule.readOnly || rule.part === undefined || rule.part.lineItem.isReadOnly;
              }

  showInfo() {
                return this.getBomPart(this.part) !== undefined && this.bomSummary !== undefined;
              }

  getBom() {
                this._bomService.getBom(this.part.lineItem.id)
                  .subscribe(bomSummary => {
                    this.bomSummary = bomSummary;
                    this.getAllWeldLaborItems(bomSummary);
                    if (this.allWeldLaborItems.length > 0) {
                      this.weldHours = this.allWeldLaborItems.reduce((a, b) => a + b, 0);
                    } else { this.weldHours = 0; }
                  });
              }

  getPartBom() {
                this._bomService.getBom(this.part.lineItem.id)
                .subscribe(bomSummary => {
                  this.getPartBomFromFullSummary(bomSummary);
                  this.getAllWeldLaborItems(this.bomSummary);
                  if (this.allWeldLaborItems.length > 0) {
                    this.weldHours = this.allWeldLaborItems.reduce((a, b) => a + b, 0);
                  } else { this.weldHours = 0; }
                });
              }

  getAllWeldLaborItems(bom: any) {
                if (bom) {
                  bom.labor.map(l => {
                      const category = l.category;
                      if (category && category.toUpperCase() === 'WELD' && !this.allWeldLaborItems.includes(category)) {
                        this.allWeldLaborItems.push(category);
                      }
                  });
                  bom.children.map(c => this.getAllWeldLaborItems(c));
                }
              }

  hoursInCategory(laborItem: any, category: string): number {
                if (laborItem.category === category) {
                  return laborItem.hours;
                }
                return null;
              }

  getTotalLaborByCategory(bomItem: any, category: string): number {
                let hours = 0;
                if (bomItem === undefined) { return hours; }
                const labor = bomItem.labor.filter(l => l.category.toUpperCase() === category.toUpperCase());
                hours = labor.reduce((hours, current) => hours + current.hours, 0);

                bomItem.children.map(c => hours = hours + (this.getTotalLaborByCategory(c, category) * c.qty ));
                if (hours === 0) { return null; }
                return hours;
              }

  getBomPart(prt: Part) {
                return prt.children.filter(c => c.name === 'Bom')[0];
              }

  getPartRule(prt: Part, rulename: string) {
                const rules = prt.rules.filter(r => r.name === rulename);

                if (rules.length > 0) {
                  return rules[0];
                } else {
                  return null;
                }
              }

  private handleError(rule, err, element) {
                // Revert to previous value
                if (element && rule !== null) {
                  element.value = rule.value;
                }

                // Show message to user
                this.showSnackbar(err.message);
              }

  private showSnackbar(message: string): void {
                this._snackBar.open(message, null, { duration: 1000 });
              }

  private getPartBomFromFullSummary(summary: any) {
    if (summary.path === this.part.path + '.Bom') {
        this.bomSummary = summary;
      }
    summary.children.forEach(element => {
      if (element.path === this.part.path + '.Bom') {
        this.bomSummary = element;
      }
    });
  }

  getMargin(part: Part){
    const bom = this.getBomPart(part);
    const rule = this.getPartRule(bom, 'Margin');
    return rule === null ? 0 : rule.value;
  }

  checkIfShippableParts(part: Part) {
    const truth = part.partType === 'ShippableParts';
    return truth;
  }

  checkIfStorageSystem(prt: Part) {
    const truth = prt.partType === 'StorageSystem';
    return truth;
  }

  public isMajorItem(part: Part): boolean {
    return this.checkIfStorageSystem(part) || this.checkIfShippableParts(part);
  }

}
