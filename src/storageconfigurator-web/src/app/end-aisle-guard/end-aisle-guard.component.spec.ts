import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndAisleGuardComponent } from './end-aisle-guard.component';

describe('EndAisleGuardComponent', () => {
  let component: EndAisleGuardComponent;
  let fixture: ComponentFixture<EndAisleGuardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndAisleGuardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndAisleGuardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
