import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ModelContextService, LineItem, Part, BreadcrumbsService } from '@ruler/core';
import { CommonDialogsService } from '@ruler/material';
import { of, combineLatest, Subscription } from 'rxjs';
import { switchMap, tap, map ,  filter } from 'rxjs/operators';

@Component({
  selector: 'app-line-item',
  templateUrl: './line-item.component.html',
  styleUrls: ['./line-item.component.scss']
})
export class LineItemComponent implements OnInit, OnDestroy {
  private _partSubscription: Subscription;

  lineItem: LineItem;
  selectedPart: Part;
  showGraphics: boolean;

  constructor(private _modelContext: ModelContextService,
    private _breadcrumbs: BreadcrumbsService,
    private _route: ActivatedRoute,
    private _router: Router,
    private _dialogSvc: CommonDialogsService) {
      this.showGraphics = _modelContext.graphicsEnabled;
    }

  ngOnInit() {
    const lineItemId$ = this._route.params.pipe(map(params => params['id']));
    const lineItem$ = this._modelContext.createLineItemObservable(lineItemId$);
    const partPath$ = this._route.params.pipe(map(params => params['path']));
    this._partSubscription = combineLatest([lineItem$, partPath$]).pipe(
      tap(values => this.lineItem = values[0]),
      map(values => {
        const [lineItem, path] = values;

        if (path) {
          return lineItem.root.findPart(path);
        } else {
          return lineItem.root;
        }
      })
    )
    .subscribe(part => {
      this.selectedPart = part;
      this._breadcrumbs.setSelectedPart(part);
    }, () => {
      this._dialogSvc.showAlert('An error occurred while loading line item', 'Error');
      this._router.navigate(['']);
    });

    this._modelContext.graphicsSettingsChanged.subscribe(enabled => {
      this.showGraphics = enabled;

      if (enabled && this.lineItem) {
        this._modelContext.loadLineItem(this.lineItem.id).subscribe();
      }
    });
  }

  ngOnDestroy() {
    this._partSubscription.unsubscribe();
  }

  isLineItem(prt: Part) {
    return prt.partType === 'StorageSystem' || prt.partType === 'ShippableParts';
  }

  shouldShowGraphics() {
    return (this.selectedPart !== undefined &&
            this.selectedPart.partType !== 'StorageSystem' &&
            this.selectedPart.partType !== 'Bays' &&
            this.selectedPart.partType !== 'FrameLines' &&
            this.selectedPart.partType !== 'FrameTypes' &&
            this.selectedPart.partType !== 'Elevations' &&
            this.selectedPart.partType !== 'Products' &&
            this.selectedPart.partType !== 'Elevation' &&
            this.selectedPart.partType !== 'ShippableParts' &&
            this.selectedPart.partType !== 'CustomPart' &&
            this.selectedPart.partType !== 'BuyoutPart' &&
            this.selectedPart.partType !== 'RowTypes' &&
            this.selectedPart.partType !== 'RowProxy' &&
            this.selectedPart.partType !== 'CustomLaborItem' &&
            this.selectedPart.partType !== 'Shim' &&
            this.selectedPart.partType !== 'TouchUpCan' &&
            this.selectedPart.partType !== 'AnchorBolt' &&
            this.selectedPart.partType !== 'Washers' &&
            this.selectedPart.partType !== 'Bolt' &&
            this.selectedPart.partType !== 'Nut');
  }
}
