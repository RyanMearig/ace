import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BomEditingRowComponent } from './bom-editing-row.component';

describe('BomEditingRow', () => {
  let component: BomEditingRowComponent;
  let fixture: ComponentFixture<BomEditingRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BomEditingRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BomEditingRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
