import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Part, PartType, ModelContextService, BusyService } from '@ruler/core';
import { MatSnackBar } from '@angular/material';
import { BomService } from '../bom.service';

@Component({
  selector: 'bom-editing-row',
  templateUrl: './bom-editing-row.component.html',
  styleUrls: ['./bom-editing-row.component.scss']
})
export class BomEditingRowComponent implements OnInit {

  @Input()
  part: Part;

  previousMargin: number;
  previousItem: string;

  constructor(private _modelContext: ModelContextService,
              private _router: Router,
              private _snackBar: MatSnackBar,
              private _bomService: BomService,
              private _busyService: BusyService) { }

  ngOnInit() {
    if (this.part) {
      const marginRule = this.getPartRule(this.part, 'bOMDataMargin');
      this.previousMargin = marginRule.value;
      this.previousItem = this.getPartRule(this.part, 'bOMItemNumber').value;
    }
  }

  ngOnChanges() {
    if (this.part) {
    }
  }

  setValue(rule, value, element) {
    this._modelContext.setRuleValue(rule, value)
      .subscribe(null, err => this.handleError(rule, err, element));
  }

  getPartRule(prt: Part, rulename: string) {
    const rules = prt.rules.filter(r => r.name === rulename);

    if (rules.length > 0) {
      return rules[0];
    } else {
      return null;
    }
  }

  setBomMargins(value, element) {
    if (!(value < 100)) {
      this.showSnackbar('Margin must be less than 100');
      element.value = this.previousMargin.toLocaleString('en-us', {minimumFractionDigits: 2});
    } else if (this.isMajorItem(this.part)) {
      this._busyService.addRequest();
      this.previousMargin = value;
      this._bomService.setAllMargins(this.part.lineItem.id, value)
        .subscribe(null, err => this.handleError(null, err, element),
        () =>
          this._modelContext.loadLineItem(this.part.lineItem.id)
          .subscribe(loadedLi => {
            this._busyService.removeRequest();
          })
        );
    } else {
      this._busyService.addRequest();
      this.previousMargin = value;
      const partNum = this.getPartRule(this.part, 'partNumber').value;
      this._bomService.setMarginsByPartNumber(this.part.lineItem.id, partNum, value)
        .subscribe(null, err => this.handleError(null, err, element),
        () =>
          this._modelContext.loadLineItem(this.part.lineItem.id)
          .subscribe(loadedLi => {
            this._busyService.removeRequest();
          })
        );
    }
  }

  setBomItems(value, element) {

    const pattern = /^[\d\-]+$/;
    if (!pattern.test(value)) {
      this.showSnackbar('Value must be numbers and dashes only.');
      element.value = this.previousItem;
    } else {
      this._busyService.addRequest();
      const partNum = this.getPartRule(this.part, 'partNumber').value;
      this._bomService.setItemByPartNumber(this.part.lineItem.id, partNum, value)
        .subscribe(null, err => this.handleErrorPrev(this.previousItem, err, element),
        () =>
          this._modelContext.loadLineItem(this.part.lineItem.id)
          .subscribe(loadedLi => {
            this._busyService.removeRequest();
            this.previousItem = value;
          })
        );
    }
  }

  public isMajorItem(prt: Part): boolean {
    return prt.partType === 'StorageSystem' || prt.partType === 'ShippableParts';
  }

  private handleError(rule, err, element) {
    // Revert to previous value
    if (element) {
      element.value = rule.value.toLocaleString('en-us', {minimumFractionDigits: 2});
    }
    // Show message to user
    this.showSnackbar(err.message);
    this._busyService.removeRequest();
  }

  private handleErrorPrev(prevValue, err, element) {
    // Revert to previous value
    if (element) {
      element.value = prevValue;
    }
    // Show message to user
    if (err.message === undefined) {
      this.showSnackbar(err);
    } else {
      this.showSnackbar(err.message);
    }

    this._busyService.removeRequest();
    this._busyService.removeRequest();
  }

  private showSnackbar(message: string): void {
    this._snackBar.open(message, null, { duration: 1000 });
  }

}
