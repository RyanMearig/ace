import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrameLinesComponent } from './frame-lines.component';

describe('FrameLinesComponent', () => {
  let component: FrameLinesComponent;
  let fixture: ComponentFixture<FrameLinesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrameLinesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrameLinesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
