import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Part, PartType, ModelContextService } from '@ruler/core';

@Component({
  selector: 'frame-lines',
  templateUrl: './frame-lines.component.html',
  styleUrls: ['./frame-lines.component.scss']
})
export class FrameLinesComponent implements OnInit {

  @Input()
  part: Part;

  frames: Array<Part>;

  constructor(private _modelContext: ModelContextService,
    private _router: Router) { }

  addChild(partTypeCode: string) {
    const lineItemId = this.part.lineItem.id;
    const projectId = this.part.lineItem.project.id;

    this._modelContext.addPart(this.part.lineItem.id, {
      partPath: this.part.path,
      partTypeCode: partTypeCode
    }).subscribe((path) => {
      this._router.navigate(['/project', projectId, 'item', lineItemId, 'part', path]);
    });
  }

  ngOnInit() {
  }

  ngOnChanges() {
    if (this.part) {
      this.frames = this.part.children.filter(c => c.partType === 'SelectiveFrameLine');
    }
  }

}
