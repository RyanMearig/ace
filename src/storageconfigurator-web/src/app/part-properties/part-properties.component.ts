import { Component, OnInit, OnChanges, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Part, ModelContextService, Rule } from '@ruler/core';
import { BomService } from '../bom.service';

@Component({
  selector: 'part-properties',
  templateUrl: './part-properties.component.html',
  styleUrls: ['./part-properties.component.scss']
})
export class PartPropertiesComponent implements OnInit, OnChanges {
  @Input()
  part: Part;

  rules: Array<any>;

  readOnly = true;

  constructor(private _modelContext: ModelContextService,
    private _bomService: BomService,
    private _router: Router) { }

  ngOnInit() {
  }

  ngOnChanges() {
    this.readOnly = !this.part || this.part.lineItem.isReadOnly;
    this.rules = this.part ? this.sortRules(this.part.rules) : [];
  }

  addChild(partTypeCode: string) {
    const lineItemId = this.part.lineItem.id;
    const projectId = this.part.lineItem.project.id;

    this._modelContext.addPart(this.part.lineItem.id, {
      partPath: this.part.path,
      partTypeCode: partTypeCode
    }).subscribe((path) => {
      this._router.navigate(['/project', projectId, 'item', lineItemId, 'part', path]);
    });
  }

  copy() {
    const lineItemId = this.part.lineItem.id;
    const projectId = this.part.lineItem.project.id;

    this._bomService.copyParts(this.part.lineItem.id, this.part.path)
    .subscribe((path) => {
        this._modelContext.loadLineItem(this.part.lineItem.id)
      .subscribe(loadedLi => {
        this._router.navigate(['/project', projectId, 'item', lineItemId, 'part', path]);
      });
    });
  }

  trackRule(index, rule) {
    return rule ? rule.name : undefined;
  }

  private sortRules(rules: Array<any>) {
    return rules.sort((item1, item2): number => {
      if (item1.sortKey < item2.sortKey) {
        return -1;
      }

      if (item1.sortKey > item2.sortKey) {
        return 1;
      }

      return  0;
    });
  }
}
