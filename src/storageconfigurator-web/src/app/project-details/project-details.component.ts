import { Component, OnInit, OnDestroy, AfterViewChecked } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { CurrencyPipe } from '@angular/common';
import { ModelContextService, LineItemType, LineItem, BreadcrumbsService, LineItemUpdateRequest, BusyService } from '@ruler/core';
import { AddLineItemsDialogComponent } from '@ruler/material';
import { MatDialog } from '@angular/material/dialog';
import { Subscription, forkJoin } from 'rxjs';
import { filter, exhaustMap, pairwise } from 'rxjs/operators';
import { StorageConfiguratorProject } from '../storage-configurator-project';
import { ProjectBomService } from '../project-bom.service';
import { CostSummary } from '../cost-summary';
import { CsiService } from '../csi.service';

@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.scss']
})
export class ProjectDetailsComponent implements OnInit, OnDestroy, AfterViewChecked {
  form: FormGroup;

  private _projectSub: Subscription;
  private _lineItemsSub: Subscription;
  private _formSub: Subscription;

  project: StorageConfiguratorProject;
  lineItems: Array<LineItem>;
  lineItemTypes: Array<LineItemType>;
  costSummaries: Array<CostSummary>;

  public statuses: Array<string> = [ 'Open', 'Submitted', 'Won', 'On Hold', 'Lost' ];
  shipToSequences: Array<number> = [];

  constructor(private _modelContext: ModelContextService,
    private _breadcrumbs: BreadcrumbsService,
    private _router: Router,
    private _dialog: MatDialog,
    private _fb: FormBuilder,
    private _projectBomService: ProjectBomService,
    private _csiService: CsiService,
    private _busyService: BusyService,
    private _currencyPipe: CurrencyPipe
    ) { }

  addComponent(lineItemType: LineItemType) {
    if (lineItemType.allowAddMultiple) {
      this.addComponentDialog(lineItemType);
    } else {
      this.addLineItem(lineItemType.partTypeCode);
    }
  }

  addLineItem(partTypeCode: string) {
    const request = {
      projectId: this.project.id,
      partTypeCode: partTypeCode,
    };

    this._modelContext.createLineItem(request)
      .subscribe(item => {
        this.setProjectNumber(item, this.project.drawingNumber);
        this._router.navigate(['/project', this.project.id, 'item', item.id]);
      });
  }

  setLineItemProjectNumber(value: string) {
    if (this.project.lineItems.length > 0) {
      this.project.lineItems.forEach(li => {
        this._modelContext.loadLineItem(li.id)
          .subscribe(loadedLi => {
            this.setProjectNumber(loadedLi, value);
          });
      });
    }
  }

  setProjectNumber(item: LineItem, value: string) {
    const newPart = item.root;
    const projectNumberRule = newPart.rules.filter(r => r.name === 'projectNumber')[0];
    if (projectNumberRule !== undefined && projectNumberRule.value !== this.project.drawingNumber) {
      this._modelContext.setRuleValue(projectNumberRule, value)
      .subscribe();
    }
  }

  addComponentDialog(lineItemType: LineItemType) {
    const dialogRef = this._dialog.open<AddLineItemsDialogComponent>(AddLineItemsDialogComponent);
    dialogRef.componentInstance.lineItemType = lineItemType;

    return dialogRef.afterClosed();
  }

  getButtonName(lineItemType: LineItemType): string {
    if (lineItemType.allowAddMultiple) {
      return lineItemType.description + '(s)';
    } else {
      return lineItemType.description;
    }
  }

  buttonIsDisabled(lineItemType: LineItemType): boolean {
    if (this.lineItems !== null && this.lineItems.length > 0) {
      const items = this.lineItems.filter(l => l.partTypeCode === lineItemType.partTypeCode);

      return (items !== null && items.length > 0);
    } else {
      return false;
    }
  }

  ngOnInit() {
    this._modelContext.getLineItemTypes()
      .subscribe(types => this.lineItemTypes = types.reverse());

    this.getSteelSurcharge();

    this._projectSub = this._modelContext.project.pipe(
      filter(proj => proj !== null)
    ).subscribe(proj => {
      this.onProjectUpdate(proj as StorageConfiguratorProject);
      if (this.project) {
        this.getCostSummaries();

        if (this.form.controls.jobStatus.value === 'Won' || this.form.controls.jobStatus.value === 'Lost') {
          if (this.form.get('winPercentage').enabled) { this.form.get('winPercentage').disable(); }
        } else {
          if (!this.form.get('winPercentage').enabled) { this.form.get('winPercentage').enable(); }
        }
      }
    });

    this._lineItemsSub = this._modelContext.allLineItems
      .subscribe(items => this.lineItems = items.sort((item1, item2): number => {
        if (item1.createdDateTime < item2.createdDateTime) {
          return -1;
        }

        if (item1.createdDateTime > item2.createdDateTime) {
          return 1;
        }

        return  0;
      }));

    this._breadcrumbs.setSelectedPart(null);
  }

  private onProjectUpdate(proj: StorageConfiguratorProject) {
    this.project = proj as StorageConfiguratorProject;

    if (this.form) {
      this.updateForm(this.project);
    } else {
      this.form = this.buildForm(this.project);

      this.form.get('drawingNumber').valueChanges.pipe(pairwise()).subscribe(([prev, next]: [any, any]) => {
        if (prev !== next) {
          this._busyService.addRequest();
          this.setLineItemProjectNumber(next);
          this._busyService.removeRequest();
        }
      });
      this.form.get('csiCustomerNumber').valueChanges.subscribe(value => {
        this.getCustomerInfo(value);
        this.getCustomerSequences(value);
      });
      this.form.get('shipToCustomerSeq').valueChanges.subscribe(value => {
        this.getShipToInfo(value);
      });
      this.form.get('paymentMilestones').valueChanges.subscribe(value => {
        this.updateControlValueIfChanged(this.form.controls.paymentMilestonesJson, JSON.stringify(value));
      });

    }

    if (this.project.isReadOnly) {
      this.form.disable();
    } else {
      this.form.enable();
    }
  }

  private buildForm(proj: StorageConfiguratorProject): FormGroup {
    const numberPattern = '^[0-9.,]+$';
    return this._fb.group({
      drawingNumber: [ proj.drawingNumber, { updateOn: 'blur' }],
      name: [ proj.name, { updateOn: 'blur' }],

      advancedSalesName: [ proj.advancedSalesName, { updateOn: 'blur' }],
      advancedSalesContactEmail: [ proj.advancedSalesContactEmail, { updateOn: 'blur' }],
      advancedSalesContactPhone: [ proj.advancedSalesContactPhone, { updateOn: 'blur' }],
      advancedProjectManagerName: [ proj.advancedProjectManagerName, { updateOn: 'blur' }],

      shipToCustomerSeq: [ proj.shipToCustomerSeq, { updateOn: 'blur' }],
      jobsiteName: [ proj.jobsiteName, { updateOn: 'blur' }],
      jobsiteAddress: [ proj.jobsiteAddress, { updateOn: 'blur' }],
      jobsiteCity: [ proj.jobsiteCity, { updateOn: 'blur' }],
      jobsiteState: [ proj.jobsiteState, { updateOn: 'blur' }],
      jobsiteZip: [ proj.jobsiteZip, { updateOn: 'blur' }],
      jobsiteContactName: [ proj.jobsiteContactName, { updateOn: 'blur' }],
      jobsiteContactPhone: [ proj.jobsiteContactPhone, { updateOn: 'blur' }],
      jobsiteContactEmail: [ proj.jobsiteContactEmail, { updateOn: 'blur' }],

      csiCustomerNumber: [ proj.csiCustomerNumber, { updateOn: 'blur' }],
      customerName: [ proj.customerName, { updateOn: 'blur' }],
      customerAddress: [ proj.customerAddress, { updateOn: 'blur' }],
      customerCity: [ proj.customerCity, { updateOn: 'blur' }],
      customerState: [ proj.customerState, { updateOn: 'blur' }],
      customerZip: [ proj.customerZip, { updateOn: 'blur' }],
      customerComments: [ proj.customerComments, { updateOn: 'blur' }],
      customerContactName: [ proj.customerContactName, { updateOn: 'blur' }],
      customerContactPhone: [ proj.customerContactPhone, { updateOn: 'blur' }],
      customerContactEmail: [ proj.customerContactEmail, { updateOn: 'blur' }],
      leadTime: [ proj.leadTime, { updateOn: 'blur' }],
      terms: [ proj.terms, { updateOn: 'blur' }],
      customerType: [ proj.customerType, { updateOn: 'blur' }],

      seismicEngName: [ proj.seismicEngName, { updateOn: 'blur' }],
      seismicEngAddress: [ proj.seismicEngAddress, { updateOn: 'blur' }],
      seismicEngCity: [ proj.seismicEngCity, { updateOn: 'blur' }],
      seismicEngState: [ proj.seismicEngState, { updateOn: 'blur' }],
      seismicEngZip: [ proj.seismicEngZip, { updateOn: 'blur' }],
      seismicEngContactName: [ proj.seismicEngContactName, { updateOn: 'blur' }],
      seismicEngContactPhone: [ proj.seismicEngContactPhone, { updateOn: 'blur' }],
      seismicEngContactEmail: [ proj.seismicEngContactEmail, { updateOn: 'blur' }],

      jobStatus: [ proj.jobStatus, { updateOn: 'blur'}],
      expectedCloseDate: [ proj.expectedCloseDate, { updateOn: 'blur' } ],
      expectedShipDate: [ proj.expectedShipDate, { updateOn: 'blur' } ],
      winPercentage: [ proj.winPercentage, { updateOn: 'blur', validators: [ Validators.min(0), Validators.max(100) ] } ],

      steelSurcharge: [proj.steelSurcharge],

      paymentMilestones: this._fb.array(this.parseMilestones()),
      paymentMilestonesJson: [proj.paymentMilestonesJson]
    });
  }

  private updateForm(proj: StorageConfiguratorProject) {
    this.form.patchValue(proj);
    this.form.markAsPristine();
  }

  ngAfterViewChecked() {
    if (this._formSub || !this.form) {
      return;
    }

    this._formSub = this.form.valueChanges.pipe(
      filter(() => this.form.dirty),
      exhaustMap(changes => {
        const proj = {...this.project, ...changes};
        return this._modelContext.updateProject<StorageConfiguratorProject>(proj);
      })
    )
    .subscribe();
  }

  get paymentMilestones() {
    return this.form.get('paymentMilestones') as FormArray;
  }

  addPaymentMilestone() {
    const newMilestone = this.createPaymentMilestone();
    this.paymentMilestones.push(newMilestone);
  }

  deletePaymentMilestone(index: number) {
    this.paymentMilestones.removeAt(index);
  }

  createPaymentMilestone(): FormGroup {
    return this._fb.group({
      downPayment:  [0, {updateOn: 'blur'}],
      delivery:     [0, {updateOn: 'blur'}],
      comissioned:  [0, {updateOn: 'blur'}],
      notes:        ['', {updateOn: 'blur'}]
    });
  }

  parseMilestones() {
    if (typeof this.project.paymentMilestonesJson !== 'undefined' && this.project.paymentMilestonesJson !== '') {
      try {
        const milestones = JSON.parse(this.project.paymentMilestonesJson);
        const groups = milestones.map(milestone => {
          return this._fb.group({
            downPayment:  [milestone.downPayment, {updateOn: 'blur'}],
            delivery:     [milestone.delivery, {updateOn: 'blur'}],
            comissioned:  [milestone.comissioned, {updateOn: 'blur'}],
            notes:        [milestone.notes, {updateOn: 'blur'}]
          });
        });
        return groups;
      } catch (error) {
        return [];
      }
    }
    return [];
  }

  getSteelSurcharge() {
    this._csiService.getSteelSurcharge()
    .subscribe(surcharge => {
      if (surcharge !== null && surcharge !== undefined) {
        this.updateControlValueIfChanged(this.form.controls.steelSurcharge, surcharge);
      }
    });
  }

  private delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  getCustomerSequences(customerNumber: string) {
    if (customerNumber !== null && customerNumber !== undefined) {
      this._csiService.getCustomerSequences(customerNumber)
        .subscribe(seqs => {
          if (seqs !== null && seqs !== undefined) {
            this.shipToSequences = seqs;

            if (this.shipToSequences !== null && this.shipToSequences.length > 0 &&
                  !this.shipToSequences.includes(this.form.controls.shipToCustomerSeq.value)) {
              this.updateControlValueIfChanged(this.form.controls.shipToCustomerSeq, 0, true);
            }
          }
        });
    }
  }

  getShipToInfo(customerSeq: number) {
    if (customerSeq === null || customerSeq === undefined) { return ''; }
    this._csiService.getCustomer(this.form.controls.csiCustomerNumber.value, customerSeq)
      .subscribe(cust => {
        if (cust !== null && cust !== undefined) {
          this.updateShipToInfo(new Customer(cust));
        }
      });
  }

  updateShipToInfo(cust: Customer) {
    this.updateControlValueIfChanged(this.form.controls.jobsiteName, this.getStringValOrDefault(cust.customerName), true);
    this.updateControlValueIfChanged(this.form.controls.jobsiteAddress, this.getStringValOrDefault(cust.address1), true);
    this.updateControlValueIfChanged(this.form.controls.jobsiteCity, this.getStringValOrDefault(cust.city), true);
    this.updateControlValueIfChanged(this.form.controls.jobsiteState, this.getStringValOrDefault(cust.state), true);
    this.updateControlValueIfChanged(this.form.controls.jobsiteZip, this.getStringValOrDefault(cust.zip), true);
    this.updateControlValueIfChanged(this.form.controls.jobsiteContactEmail, this.getStringValOrDefault(cust.shipToEmail), true);

    if (cust.customerSeq === 0) {
      this.updateControlValueIfChanged(this.form.controls.jobsiteContactName, this.getStringValOrDefault(cust.contact1), true);
      this.updateControlValueIfChanged(this.form.controls.jobsiteContactPhone, this.getStringValOrDefault(cust.phone1), true);
    } else {
      this.updateControlValueIfChanged(this.form.controls.jobsiteContactName, this.getStringValOrDefault(cust.contact2), true);
      this.updateControlValueIfChanged(this.form.controls.jobsiteContactPhone, this.getStringValOrDefault(cust.phone2), true);
    }
  }

  getStringValOrDefault(val: string) {
    if (val === null || val === undefined) {
      return '';
    }
    return val;
  }

  getCustomerInfo(customerNumber: string) {
    if (customerNumber === null || customerNumber === undefined) { return ''; }
    this._csiService.getCustomerDefault(customerNumber)
      .subscribe(cust => {
        if (cust !== null && cust !== undefined) {
          this.updateCustomerInfo(new Customer(cust));
        }
      });
  }

  updateCustomerInfo(cust: Customer) {
    this.updateControlValueIfChanged(this.form.controls.customerName, cust.customerName, true);
    this.updateControlValueIfChanged(this.form.controls.customerAddress, cust.address1, true);
    this.updateControlValueIfChanged(this.form.controls.customerCity, cust.city, true);
    this.updateControlValueIfChanged(this.form.controls.customerState, cust.state, true);
    this.updateControlValueIfChanged(this.form.controls.customerZip, cust.zip, true);
    this.updateControlValueIfChanged(this.form.controls.customerContactName, cust.contact1, true);
    this.updateControlValueIfChanged(this.form.controls.customerContactPhone, cust.phone1, true);
    this.updateControlValueIfChanged(this.form.controls.customerContactEmail, cust.customerContactEmail, true);
    this.updateControlValueIfChanged(this.form.controls.terms, cust.termsCode, true);
    this.updateControlValueIfChanged(this.form.controls.customerType, cust.customerType, true);
  }

  updateControlValueIfChanged(control: AbstractControl, value: any, event: boolean = false) {
    if (value !== undefined && value !== null && control.value !== value) {
      control.setValue(value, {emitEvent: event}) ;
      this.form.markAsDirty();
    }
  }

  handleStatusChange(event) {
    if (event.value === 'Won') {
      this.updateControlValueIfChanged(this.form.controls.winPercentage, '100');
    } else if (event.value === 'Lost') {
      this.updateControlValueIfChanged(this.form.controls.winPercentage, '0');
    }
  }

  handleShipToSeqChange(event) {

  }

  getCostSummaries() {
    this._projectBomService.getCostSummaries(this.project.id)
    .subscribe(c => {
      this.costSummaries = c;
    });
  }

  updateBomInclusion(summary: CostSummary, val: boolean) {
    this._projectBomService.setCostInclusion(summary.lineItemId, val, this.project.id)
      .subscribe();
  }

  ngOnDestroy() {
    this._projectSub.unsubscribe();
    this._lineItemsSub.unsubscribe();
    this._formSub.unsubscribe();
  }

  getPrice(cost: number, margin: number): number {
    const marginDec = margin / 100;
    return cost / (1 - marginDec);
  }

  getCostSummariesExtraDataSum(extraDataName: string): number {
    return this.costSummaries.reduce((sum, current) =>
                    sum + this.getExtraDataNumberValue(current.extraData, extraDataName, 0), 0);
  }

  getMarginPercent(cost, price, defaultVal: number = 0): number {
    if (price === undefined || cost === undefined || price === 0) { return defaultVal; }
    return ((price - cost) / price) * 100;
  }

  getExtraDataNumberValue(extraData: string, valueName: string, defaultVal: number = 0): number {
    const obj = JSON.parse(extraData);
    return obj[valueName] != null ? obj[valueName] : defaultVal;
  }

  getExtraDataValue(extraData: string, valueName: string, defaultVal: string = ''): string {
    const obj = JSON.parse(extraData);
    return obj[valueName] != null ? obj[valueName] : defaultVal;
  }

  formatMoney(value) {
    if (value === undefined || value === null) { value = 0.0; }
    const temp = `${value}`.replace(/\,/g, '');
    return this._currencyPipe.transform(temp.replace(/\$/g, ''));
  }

}

class Customer {
  public customerNumber!: string;
  public customerSeq!: number;
  public customerName!: string;
  public address1!: string;
  public address2!: string;
  public city!: string;
  public state!: string;
  public zip!: string;
  public contact1!: string;
  public contact2!: string;
  public contact3!: string;
  public phone1!: string;
  public phone2!: string;
  public phone3!: string;
  public customerContactEmail!: string;
  public shipToEmail!: string;
  public termsCode!: string;
  public customerType!: string;
  constructor(data: Partial<Customer>) {
    Object.assign(this, data);
  }

  public static fromJSON = (json: string): Customer => {
    const jsonObject = JSON.parse(json);
    return new Customer(jsonObject);
  }
}
