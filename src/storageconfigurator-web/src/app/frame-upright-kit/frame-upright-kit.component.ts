import { Component, OnInit, OnChanges, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Part, PartType, Rule, LineItem, ModelContextService } from '@ruler/core';
import {MatTabsModule } from '@angular/material/tabs';

@Component({
  selector: 'frame-upright-kit',
  templateUrl: './frame-upright-kit.component.html',
  styleUrls: ['./frame-upright-kit.component.scss']
})
export class FrameUprightKitComponent implements OnInit, OnChanges {

  @Input()
  part: Part;
  frame: Part;
  horizontalBraces: Array<Part>;
  heavyHorizontalBraces: Array<Part>;
  diagonalBraces: Array<Part>;

  constructor(private _modelContext: ModelContextService,
    private _router: Router) { }

  ngOnInit() {
    if (this.part) {
      this.frame = this.part.children.filter(c => c.partType === 'FrameUpright')[0];
      this.horizontalBraces = this.frame.children.filter(c => c.partType === 'HorizontalBrace').reverse();
      this.heavyHorizontalBraces = this.frame.children.filter(c => c.partType === 'HeavyHorizontalBrace'
                                                            && !c.displayName.includes('Dblr')).reverse();
      this.diagonalBraces = this.frame.children.filter(c => c.partType === 'DiagonalBrace').reverse();
    }
  }

  ngOnChanges() {
    if (this.part) {
      this.frame = this.part.children.filter(c => c.partType === 'FrameUpright')[0];
      this.horizontalBraces = this.part.children.filter(c => c.partType === 'HorizontalBrace').reverse();
      this.heavyHorizontalBraces = this.part.children.filter(c => c.partType === 'HeavyHorizontalBrace'
                                                            && !c.displayName.includes('Dblr')).reverse();
      this.diagonalBraces = this.part.children.filter(c => c.partType === 'DiagonalBrace').reverse();
    }
  }

  getBraceDisplayName(display: string): string {
    if (display.endsWith(' 1')) {
      return display.replace(' 1', ' Bottom');
    }
    return display;
  }

  addChild(partTypeCode: string) {
    const lineItemId = this.part.lineItem.id;
    const projectId = this.part.lineItem.project.id;

    this._modelContext.addPart(this.part.lineItem.id, {
      partPath: this.frame.path,
      partTypeCode: partTypeCode
    }).subscribe((path) => {
      this._router.navigate(['/project', projectId, 'item', lineItemId, 'part', path]);
    });
  }

}
