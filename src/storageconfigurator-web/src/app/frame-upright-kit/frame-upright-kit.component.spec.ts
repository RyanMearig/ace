import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrameUprightKitComponent } from './frame-upright-kit.component';

describe('FrameUprightKitComponent', () => {
  let component: FrameUprightKitComponent;
  let fixture: ComponentFixture<FrameUprightKitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrameUprightKitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrameUprightKitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
