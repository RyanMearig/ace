import { Component, OnInit, Input } from '@angular/core';
import { Part, ModelContextService } from '@ruler/core';

@Component({
  selector: 'bom-detail-view',
  templateUrl: './bom-detail-view.component.html',
  styleUrls: ['./bom-detail-view.component.scss']
})
export class BomDetailViewComponent implements OnInit {

  constructor(private _modelContext: ModelContextService) { }

  @Input()
  bomSummaryItem: any;

  @Input()
  bomDepth: number;

  @Input()
  showLabor: boolean = true;

  @Input()
  laborCategories: Array<string>;

  @Input()
  useQtyFactor: boolean = true;

  bomIndent: string;
  qtyFactor: number;

  ngOnInit() {
  }

  ngOnChanges() {
    this.qtyFactor = this.useQtyFactor && this.bomSummaryItem !== undefined ? this.bomSummaryItem.qty : 1;
    this.bomIndent = '';
    for (let i = 0; i < this.bomDepth; i++) {
      this.bomIndent = this.bomIndent + '\xa0\xa0\xa0';
    }
  }

  hoursInCategory(laborItem: any, category: string): number {
    if (laborItem.category === category) {
      return laborItem.hours;
    }
    return null;
  }

  getTotalLaborByCategory(bomItem: any, category: string): number {
    let hours = 0;
    if (bomItem === undefined) { return hours; }
    const labor = bomItem.labor.filter(l => l.category === category);
    hours = labor.reduce((hours, current) => hours + current.hours, 0);

    bomItem.children.map(c => hours = hours + (this.getTotalLaborByCategory(c, category) * c.qty ));
    if (hours === 0) { return null; }
    return hours;
  }

}
