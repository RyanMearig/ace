import { TestBed } from '@angular/core/testing';

import { LineItemBomService } from './line-item-bom.service';

describe('LineItemBomService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LineItemBomService = TestBed.get(LineItemBomService);
    expect(service).toBeTruthy();
  });
});
