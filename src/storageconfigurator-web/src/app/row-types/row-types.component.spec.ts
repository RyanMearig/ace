import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RowTypesComponent } from './row-types.component';

describe('RowTypesComponent', () => {
  let component: RowTypesComponent;
  let fixture: ComponentFixture<RowTypesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RowTypesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RowTypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
