import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Part, PartType, ModelContextService } from '@ruler/core';

@Component({
  selector: 'row-types',
  templateUrl: './row-types.component.html',
  styleUrls: ['./row-types.component.scss']
})
export class RowTypesComponent implements OnInit {

  constructor(private _modelContext: ModelContextService,
    private _router: Router) { }
  @Input()
  part: Part;
  rowTypes: Array<Part>;

  ngOnInit() {
    this.getRowTypes();
  }

  addChild(partTypeCode: string) {
    const lineItemId = this.part.lineItem.id;
    const projectId = this.part.lineItem.project.id;

    this._modelContext.addPart(this.part.lineItem.id, {
      partPath: this.part.path,
      partTypeCode: partTypeCode
    }).subscribe((path) => {
      this._router.navigate(['/project', projectId, 'item', lineItemId, 'part', path]);
    });
  }

  getRowTypes() {
    if (this.part) {
      this.rowTypes = this.part.children;
    }
  }

  addChildBtnDisabled(childType: string) {
    if (childType === 'row') {
      return this.getPartRule(this.part.parent, 'bayQty').value === 0 ||
      this.getPartRule(this.part.parent, 'frameLineQty').value === 0;
    }
    return false;
  }

  getPartRule(prt: Part, rulename: string) {
    const rules = prt.rules.filter(r => r.name === rulename);

    if (rules.length > 0) {
      return rules[0];
    } else {
      return null;
    }
  }

}
