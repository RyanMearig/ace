import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent, HttpResponse } from '@angular/common/http';
import { CostSummary } from './cost-summary';
import { environment } from '../environments/environment';
import { Observable, Subject, BehaviorSubject, throwError } from 'rxjs';
import { withLatestFrom, tap, catchError } from 'rxjs/operators';
import { ProjectPriceOverride } from './project-price-override';
import { BusyService } from '@ruler/core';

@Injectable({
  providedIn: 'root'
})
export class ProjectBomService {

  constructor(private _http: HttpClient, private _busyService: BusyService) { }

  private _costSummaries: Subject<CostSummary[]> = new BehaviorSubject<CostSummary[]>(null);
  private _priceOverride: Subject<ProjectPriceOverride> = new BehaviorSubject<ProjectPriceOverride>(null);

  get costSummaries(): Observable<CostSummary[]> {
    return this._costSummaries;
  }

  get priceOverride(): Observable<ProjectPriceOverride> {
    return this._priceOverride;
  }

  getCostSummaries(projectId: string): Observable<CostSummary[]> {
    const url = environment.apiUrl + 'lineItemCostSummaries/' + projectId;
    return this._http.get<any>(url)
      .pipe(tap(summaries => {
        this._costSummaries.next(summaries);
      }));
  }

  setCostInclusion(lineItemId: string, include: boolean, projectId: string) {
    const url = `${environment.apiUrl}lineItemCostSummaryInclusion/${lineItemId}/${include}`;
    return this._http.patch<any>(url, null).pipe( tap(() => {
      this.getCostSummaries(projectId).subscribe();
    }));
  }

  getPriceOverride(projectId: string): Observable<ProjectPriceOverride> {
    const url = environment.apiUrl + 'ProjectCostSummaries/' + projectId;
    return this._http.get<any>(url)
      .pipe(tap(summaries => {
        this._priceOverride.next(summaries);
      }));
  }

  setPriceOverride(projectId: string, priceOverride: ProjectPriceOverride) {
    const url = `${environment.apiUrl}ProjectCostSummaries/${projectId}`;
    return this._http.put<any>(url, priceOverride).pipe( tap(override => {
      this._priceOverride.next(override);
    }));
  }

  getAdvanceMargin(family: string, customerType: string, cost: number): Observable<number> {
    const url = `${environment.apiUrl}getAdvanceMargin?` +
    `family=${family}&` +
    `customerType=${customerType}&` +
    `cost=${cost}`;
    this._busyService.addRequest();

    return this._http.get<object>(url).pipe(
      tap(() => this._busyService.removeRequest()),
      catchError((error: any) => this.handleError(error))
    );
  }

  private handleError(error: any): Observable<any> {
    // TODO: Add more robust error handling
    console.error(error);

    const returnError = (error && error.error) ? error.error :
      { message: 'An error occurred', details: '' };

    this._busyService.removeRequest();
    return throwError(returnError);
  }
}
