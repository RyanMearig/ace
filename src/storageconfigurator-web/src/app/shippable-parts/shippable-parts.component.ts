import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Part, PartType, ModelContextService } from '@ruler/core';
import { MatSnackBar } from '@angular/material';
import { BomService } from '../bom.service';

@Component({
  selector: 'shippable-parts',
  templateUrl: './shippable-parts.component.html',
  styleUrls: ['./shippable-parts.component.scss']
})
export class ShippablePartsComponent implements OnInit {

  @Input()
  part: Part;

  bomChildren: Array<Part>;
  distinctBomChildren: Array<Part>;
  firstLineChildTypes: Array<PartType>;
  secondLineChildTypes: Array<PartType>;

  constructor(private _modelContext: ModelContextService,
              private _router: Router,
              private _bomService: BomService,
              private _snackBar: MatSnackBar) { }

  addChild(partTypeCode: string) {
      const lineItemId = this.part.lineItem.id;
      const projectId = this.part.lineItem.project.id;

      this._modelContext.addPart(this.part.lineItem.id, {
        partPath: this.part.path,
        partTypeCode: partTypeCode
      }).subscribe((path) => {
        this._router.navigate(['/project', projectId, 'item', lineItemId, 'part', path]);
      });
    }

  ngOnInit() {
  }

  ngOnChanges() {
    if (this.part) {
      // tslint:disable-next-line:no-string-literal
      this.distinctBomChildren = this.formatPartArray(this.part['orderedDistinctBomChildrenByPath']);

      const halfNumTypes = Math.ceil(this.part.allowedChildTypes.length / 2);
      this.firstLineChildTypes = this.part.allowedChildTypes.slice(0, halfNumTypes);
      this.secondLineChildTypes = this.part.allowedChildTypes.slice(halfNumTypes, this.part.allowedChildTypes.length);
      // this.distinctBomChildren = this.formatPartArray(this.part.rules.filter(r => r.name === 'orderedDistinctBomChildren')[0].value);
    }
  }

  formatPartArray(prtPathArray: Array<string>) {
    if (prtPathArray !== undefined && prtPathArray !== null) {
      return prtPathArray.map(c => this.getPartByPath(c));
    }
    return null;
  }

  getPartByPath(path: string) {
    return this.part.lineItem.root.findPart(path);
  }

  getPartRule(prt: Part, rulename: string) {
    const rules = prt.rules.filter(r => r.name === rulename);

    if (rules.length > 0) {
      return rules[0];
    } else {
      return null;
    }
  }

  setDefaultMargins() {
    this._bomService.setLineItemDefaultMargins(this.part.lineItem.id)
      .subscribe(() => {
        this._modelContext.updateLineItem({
          id: this.part.lineItem.id,
          name: this.part.lineItem.name
        }).subscribe();
      });
  }

  setNameValue(rule, value, element) {
    const lineItem = this.part.lineItem;

    this._modelContext.setRuleValue(rule, value)
      .subscribe(null, err => this.handleError(rule, err, element));

    if (value !== lineItem.name) {
      this._modelContext.updateLineItem({
        id: lineItem.id,
        name: value
      }).subscribe();
    }
  }

  private handleError(rule, err, element) {
    // Revert to previous value
    if (element) {
      element.value = rule.value;
    }

    // Show message to user
    this.showSnackbar(err.message);
  }

  private showSnackbar(message: string): void {
    this._snackBar.open(message, null, { duration: 1000 });
  }

}
