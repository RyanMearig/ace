import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShippablePartsComponent } from './shippable-parts.component';

describe('ShippablePartsComponent', () => {
  let component: ShippablePartsComponent;
  let fixture: ComponentFixture<ShippablePartsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShippablePartsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShippablePartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
