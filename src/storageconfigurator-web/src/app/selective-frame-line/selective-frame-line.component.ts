import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Part, PartType, ModelContextService } from '@ruler/core';
import { BomService } from '../bom.service';

@Component({
  selector: 'selective-frame-line',
  templateUrl: './selective-frame-line.component.html',
  styleUrls: ['./selective-frame-line.component.scss']
})
export class SelectiveFrameLineComponent implements OnInit {

  @Input()
  part: Part;

  frameTiesSpace: Part;
  frameTies: Array<Part>;

  constructor(private _modelContext: ModelContextService,
    private _bomService: BomService,
    private _router: Router) { }

  addChild(partTypeCode: string) {
    const lineItemId = this.part.lineItem.id;
    const projectId = this.part.lineItem.project.id;

    this._modelContext.addPart(this.part.lineItem.id, {
      partPath: this.part.path,
      partTypeCode: partTypeCode
    }).subscribe((path) => {
      this._router.navigate(['/project', projectId, 'item', lineItemId, 'part', path]);
    });
  }

  ngOnInit() {
  }

  ngOnChanges() {
    if (this.part) {
      this.frameTiesSpace = this.part.children.filter(c => c.partType === 'FrameTiesSpace')[0];
      this.frameTies = this.frameTiesSpace.children.filter(c => c.partType === 'FrameTie');
    }
  }

  copy() {
    const lineItemId = this.part.lineItem.id;
    const projectId = this.part.lineItem.project.id;

    this._bomService.copyParts(this.part.lineItem.id, this.part.path)
    .subscribe((path) => {
        this._modelContext.loadLineItem(this.part.lineItem.id)
      .subscribe(loadedLi => {
        this._router.navigate(['/project', projectId, 'item', lineItemId, 'part', path]);
      });
    });
  }
}
