import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectiveFrameLineComponent } from './selective-frame-line.component';

describe('SelectiveFrameLineComponent', () => {
  let component: SelectiveFrameLineComponent;
  let fixture: ComponentFixture<SelectiveFrameLineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectiveFrameLineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectiveFrameLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
