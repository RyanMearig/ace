export const environment = {
  production: true,
  apiUrl: `${document.baseURI}api/`,
  signalrUrl: `${document.baseURI}hubs/`
};
