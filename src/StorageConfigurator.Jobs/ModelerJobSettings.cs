﻿namespace StorageConfigurator.Jobs
{
    public class ModelerJobSettings
    {
        public string ProjectFilePath { get; set; } = @"C:\Projects\Advance\Inventor Models\AS_Dev.ipj";
        public string VaultProjectFilePath { get; set; } = @"C:\Projects\Advance\Inventor Models\AS_Vault.ipj";
        public int TimeoutSeconds { get; set; } = 300;
        public string DrawingXmlFolder { get; set; } = @"C:\Projects\Advance\DrawingXML";
        public string OutputFolder { get; set; } = @"C:\Temp\Advance";
        public bool DebugMode { get; set; } = false;
        public bool CreateDrawings { get; set; } = true;
        public bool CopyFiles { get; set; } = false;
        public bool UploadToVault { get; set; } = false;
        public bool IgnoreVaultWorkflows { get; set; } = true;
        public string RootFolderName { get; set; } = "Inventor Models";

        public string NonMemberFileCategoryName { get; set; } = "Engineering Automation";
        public string NonMemberFileLifecycleDefinitionName { get; set; } = "Engineering Release Process";
        public string NonMemberFileFinalState { get; set; } = "Work In Progress";

        public string MemberFileCategoryName { get; set; } = "Engineering Automation";
        public string MemberFileLifecycleDefinitionName { get; set; } = "Engineering Release Process";
        public string MemberFileFinalState { get; set; } = "Pre-Released";

        public string TransitionStateAfterReleased { get; set; } = "Work In Progress";
        public string PreReleasedStateName { get; set; } = "Pre-Released";
    }
}
