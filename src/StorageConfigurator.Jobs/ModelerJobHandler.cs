﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Inventor;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Ruler.Documents;
using Ruler.Drawings;
using Ruler.InvUtils;
using Ruler.Jobs;
using Ruler.Modeler;
using Ruler.Modeler.Serialization;
using StorageConfigurator.Vault;
using Ruler.Vault;
using File = System.IO.File;
using Path = System.IO.Path;
using System.Diagnostics;

namespace StorageConfigurator.Jobs
{
    public class ModelerJobHandler : IJobHandler
    {
        #region Private Fields

        private readonly ModelerJobSettings _settings;
        private readonly ILogger _logger;

        #endregion

        #region Constructors

        public ModelerJobHandler(ModelerJobSettings settings, ILogger logger)
        {
            _settings = settings;
            _logger = logger;
        }

        #endregion

        #region IJobHandler Implementation

        public async Task<JobResult> HandleJobAsync(JobQueueJob job)
        {
            var assemblyResolver = new AssemblyResolver();
            IInventorHost<Application> inventorHost = null;

            try
            {
                assemblyResolver.Enable();

                if (!_settings.DebugMode)
                {
                    Thread.Sleep(1000);
                    InventorAppUtils.KillAllInventorInstances();
                    Thread.Sleep(1000);
                }

                inventorHost = this.GetInventorHost();

                var drawingNumber = GetRequiredParameterValue(job, ParameterNames.DrawingNumber);
                var projectName = GetRequiredParameterValue(job, ParameterNames.ProjectName);
                var lineItemName = GetRequiredParameterValue(job, ParameterNames.LineItemName);
                var occurrenceJson = GetRequiredParameterValue(job, ParameterNames.OccurrenceJson);
                var extraDataJson = GetOptionalParameterValue(job, ParameterNames.ExtraData);
                var extraData = JsonConvert.DeserializeObject<Dictionary<string, object>>(extraDataJson);

                var outputs = await this.RenderLineItem(inventorHost, lineItemName, drawingNumber, projectName, occurrenceJson, extraData);

                var result = new JobResult
                {
                    JobId = job.Id,
                    Status = JobStatuses.Success
                };

                result.Documents.AddRange(outputs
                    .Select(o => new Ruler.Documents.Document
                    {
                        DocumentProvider = nameof(FileSystemDocumentProvider),
                        ContainerName = System.IO.Path.GetDirectoryName(o),
                        Name = System.IO.Path.GetFileName(o)
                    })
                );

                return result;
            }
            catch (Exception ex)
            {
                var innermostException = ex;
                while (innermostException.InnerException != null)
                {
                    innermostException = innermostException.InnerException;
                }

                _logger.LogError(ex, innermostException.Message);

                return new JobResult
                {
                    JobId = job.Id.ToString(),
                    Status = JobStatuses.Failed,
                    Message = innermostException.Message
                };
            }
            finally
            {
                try
                {
                    if (_settings.DebugMode)
                    {
                        inventorHost.Inventor.SilentOperation = false;
                        inventorHost.Inventor.Visible = true;
                    }
                }
                catch (Exception) { }

                assemblyResolver.Disable();
            }
        }

        #endregion

        #region Private Methods

        private async Task<List<string>> RenderLineItem(IInventorHost<Application> inventorHost, string lineItemName, string projectNumber, string projectName, string occurrenceJson, Dictionary<string, object> extraData)
        {
            var lineItemOccurrence = Occurrence.FromJson(occurrenceJson);
            var lineItemRootName = RemoveInvalidChars(lineItemName);
            var workspaceSubFolder = $@"{RemoveInvalidChars(projectNumber)}_{RemoveInvalidChars(projectName)}\{lineItemRootName}";

            string modelPath = null;
            var docList = new List<string>();
            var cts = new CancellationTokenSource();
            cts.CancelAfter(_settings.TimeoutSeconds * 1000);

            try
            {
                await Task.Run(() =>
                {
                    _logger.LogInformation($"Rendering {lineItemName}");

                    var result = lineItemOccurrence.Render(inventorHost, 
                                      new RenderOptions
                                      { 
                                          WorkspaceSubfolder = workspaceSubFolder,
                                          Logger = _logger
                                      }, 
                                      services => services.AddJsonMemberFolderResolver(Path.Combine(inventorHost.GetProjectPathOrDefault("Factory Files"), "MemberFilePathMappings.json")));

                    modelPath = result.ComponentsByRefChain[lineItemOccurrence.RefChain].Path;
                    if (!File.Exists(modelPath)) { throw new Exception("Model missing. Expected Path: " + modelPath); }

                    docList.Add(modelPath);
                    var outputPaths = new List<(string OutputPath, string ComponentPath)>();

                    if (_settings.CreateDrawings)
                    {
                        if (Directory.Exists(_settings.DrawingXmlFolder))
                        {
                            _logger.LogInformation("Determining drawings to create");
                            var factory = new ModelPropertyDrawingFactory(inventorHost, new ModelPropertyDrawingFactoryOptions
                            {
                                XmlFolder = _settings.DrawingXmlFolder,
                                Logger = _logger,
                                ExtraData = extraData
                            });

                            ComponentFactory.RegisterComponent("AdvanceRefSketchSymbol", (p, e, o) => new StorageConfigurator.Drawings.AdvanceRefSketchSymbolComponent(p, e, o));
                            ComponentFactory.RegisterComponent("AdvanceRefAutoCADBlock", (p, e, o) => new StorageConfigurator.Drawings.AdvanceRefAutoCADBlockComponent(p, e, o));
                            ComponentFactory.RegisterComponent("ReferenceViewComponent", (p, e, o) => new StorageConfigurator.Drawings.ReferenceViewComponent(p, e, o));

                            var model = inventorHost.OpenDocument(modelPath);
                            var drawings = factory.GetDrawingCreationData(model).ToList();
                            var dwgsToRender = drawings.ToBeCreated().ToList();

                            _logger.LogInformation($"Creating {dwgsToRender.Count()} drawings");
                            var drawingsStopWatch = new Stopwatch();

                            foreach (var d in dwgsToRender)
                            {
                                _logger.LogInformation("Rendering " + d.DrawingFullPath);
                                _logger.LogInformation("Drawing Component = " + d.GetType().ToString());
                                drawingsStopWatch.Start();
                                docList.Add(d.Render());
                                drawingsStopWatch.Stop();

                                var timeSpan = drawingsStopWatch.Elapsed;
                                string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                                                        timeSpan.Hours, timeSpan.Minutes, timeSpan.Seconds, timeSpan.Milliseconds / 10);
                                _logger.LogInformation("   Rendering completed in " + elapsedTime);
                                drawingsStopWatch.Reset();
                            }

                            var drawingsToUpload = drawings.Select(ds => ds.Drawing).Select(d => (OutputPath: d.DrawingFullPath, ComponentPath: d.ModelFileName)).ToList();
                            //delete ShippableParts temp drawing and remove from upload list
                            //this is done as a workaround for a drawing error when component drawings were added.
                            drawingsToUpload.ForEach(d =>
                            {
                                if (d.OutputPath.ToLower().Contains("shippablepartstemp"))
                                {
                                    File.Delete(d.OutputPath);
                                }
                            });
                            drawingsToUpload.RemoveAll(d => d.OutputPath.ToLower().Contains("shippablepartstemp"));
                            
                            outputPaths.AddRange(drawingsToUpload);
                        }
                        else
                        {
                            throw new Exception($"Drawing XML folder does not exist: {_settings.DrawingXmlFolder}");
                        }
                    }
                    else
                    {
                        _logger.LogInformation("Skipping Drawing creation per settings.");
                    }

                    if (_settings.UploadToVault)
                    {
                        _logger.LogInformation("Begin Upload to Vault");
                        this.UploadToVault(inventorHost, lineItemName, modelPath, outputPaths);
                    }
                    else
                    {
                        _logger.LogInformation("Skipping Vault Upload per Job Settings");
                    }

                    if (_settings.CopyFiles)
                    {
                        _logger.LogInformation("Moving files to Output Folder " + _settings.OutputFolder);
                        MoveInventorFiles(inventorHost, workspaceSubFolder, modelPath, outputPaths.Select(p => p.OutputPath));
                    }
                    else
                        _logger.LogInformation("Skipping Copy Files to Output Folder per settings.");
                    
                }, cts.Token);
            }
            catch (OperationCanceledException)
            {
                throw new Exception($"Timeout occurred while rendering line item '{lineItemName}");
            }

            return docList;
        }

        private string RemoveInvalidChars(string inStr)
        {
            return Path.GetInvalidFileNameChars().Aggregate(inStr, (current, c) => current.Replace(c.ToString(), string.Empty));
        }

        private string GetOptionalParameterValue(JobQueueJob job, string parameterName)
        {
            if (!job.Parameters.TryGetValue(parameterName, out string paramValue))
            {
                paramValue = "";
            }

            return paramValue;
        }

        private string GetRequiredParameterValue(JobQueueJob job, string parameterName)
        {
            if (!job.Parameters.TryGetValue(parameterName, out string paramValue))
            {
                throw new Exception($"Missing required parameter '{parameterName}'");
            }

            return paramValue;
        }

        private IInventorHost<Application> GetInventorHost()
        {
            _logger.LogInformation("Initializing Inventor");

            var inventorApp = InventorAppUtils.GetInventorInstance();
            inventorApp.ActivateProject(_settings.ProjectFilePath);

            return new InventorApplicationHost(inventorApp, !_settings.DebugMode);
        }

        private void UploadToVault(IInventorHost<Application> inventorHost, string lineItemName, string lineItemAssemblyPath, List<(string OutputPath, string ComponentPath)> outputPaths)
        {
            _logger.LogInformation($"Initialize VaultUploader");
            var invApp = inventorHost.Inventor;
            ActivateProject(invApp, _settings.VaultProjectFilePath);
            var vaultSettings = VaultConfigurationFactory.GetVaultSettings();
            var vltUploader = new VaultUploader(_settings.RootFolderName, _logger, vaultSettings, GetVaultWorkflowHandler(), invApp)
            {
                NonMemberFileCategoryName = _settings.NonMemberFileCategoryName,
                NonMemberFileLifecycleDefinitionName = _settings.NonMemberFileLifecycleDefinitionName,
                NonMemberFileFinalState = _settings.NonMemberFileFinalState,
                MemberFileCategoryName = _settings.MemberFileCategoryName,
                MemberFileLifecycleDefinitionName = _settings.MemberFileLifecycleDefinitionName,
                MemberFileFinalState = _settings.MemberFileFinalState,
                TransitionStateAfterReleased = _settings.TransitionStateAfterReleased,
                PreReleasedStateName = _settings.PreReleasedStateName
            };
            var vaultUploadStopWatch = new Stopwatch();

            _logger.LogInformation(string.Format("Uploading {0} and outputs to Vault", lineItemName));
            vaultUploadStopWatch.Start();
            var uploadResult = vltUploader.UploadLineItemToVault(lineItemAssemblyPath, outputPaths);
            vaultUploadStopWatch.Stop();

            var uploadTimeSpan = vaultUploadStopWatch.Elapsed;
            string uploadElapsedTime = string.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                                    uploadTimeSpan.Hours, uploadTimeSpan.Minutes, uploadTimeSpan.Seconds, uploadTimeSpan.Milliseconds / 10);
            _logger.LogInformation("    Vault Upload completed in " + uploadElapsedTime);
            vaultUploadStopWatch.Reset();

            ActivateProject(invApp, _settings.ProjectFilePath);

            if (!string.IsNullOrEmpty(uploadResult))
                throw new Exception($"Error uploading to vault: {uploadResult}");
        }

        private IVaultWorkflowHandler GetVaultWorkflowHandler()
        {
            if (_settings.IgnoreVaultWorkflows)
            {
                return new DebugVaultWorkflowHandler();
            }
            else
            {
                return new VaultWorkflowHandler();
            }
        }

        private void MoveInventorFiles(IInventorHost inventorHost, string workspaceSubFolder, string sourcePath, IEnumerable<string> dwgPaths)
        {
            var inventorFilesRootDest = Path.Combine(_settings.OutputFolder, workspaceSubFolder);
            var designsFolderPath = Path.Combine(inventorFilesRootDest, "Designs");
            var memberFileFolderPath = Path.Combine(inventorFilesRootDest, "Member Files");

            if (!string.IsNullOrEmpty(_settings.OutputFolder))
            {
                if (!Directory.Exists(inventorFilesRootDest))
                    Directory.CreateDirectory(inventorFilesRootDest);

                if (!inventorHost.TryGetOpenDocument(sourcePath, out Inventor.Document doc))
                    doc = inventorHost.Documents.Open(sourcePath, false);

                if (doc != null)
                {
                    try
                    {
                        List<Inventor.Document> allDocs = GetRootComponentDocuments(doc);
                        allDocs.Add(doc);

                        //Get Member Files
                        IEnumerable<Inventor.Document> memberFileDocs = allDocs.Where(d => d.FullFileName.Contains("Member Files"));

                        if (memberFileDocs != null && memberFileDocs.Count() > 0)
                        {
                            _logger.LogInformation("Copying Member Files");
                            if (!Directory.Exists(memberFileFolderPath))
                                Directory.CreateDirectory(memberFileFolderPath);

                            memberFileDocs.ToList().ForEach(d =>
                            {
                                try
                                {
                                    File.Copy(d.FullFileName, Path.Combine(memberFileFolderPath, Path.GetFileName(d.FullFileName)), true);
                                }
                                catch (Exception ex)
                                {
                                    _logger.LogInformation($"Unable to copy {d.FullDocumentName} - {ex.Message}");
                                }
                            });



                            var memberDwgs = dwgPaths.Where(p => p.Contains("Member Files"));
                            memberDwgs.ToList().ForEach(d =>
                            {
                                try
                                {
                                    File.Copy(d, Path.Combine(memberFileFolderPath, Path.GetFileName(d)), true);
                                }
                                catch (Exception ex)
                                {
                                    _logger.LogInformation($"Unable to copy {d} - {ex.Message}");
                                }
                            });
                        }
                        //Designs Folder
                        IEnumerable<Inventor.Document> designFileDocs = allDocs.Where(d => d.FullFileName.Contains("Designs"));

                        if (designFileDocs != null && designFileDocs.Count() > 0)
                        {
                            _logger.LogInformation("Copying Design Files");
                            if (!Directory.Exists(designsFolderPath))
                                Directory.CreateDirectory(designsFolderPath);

                            CopyDirectoryFilesToNewDirectory(Path.GetDirectoryName(designFileDocs.FirstOrDefault().FullFileName), designsFolderPath);
                        }
                    }
                    finally
                    {
                        inventorHost.Documents.CloseAll();
                    }

                }
                else
                    throw new Exception("Root document unable to be opened for moving files.");
            }
            else
            {
                throw new Exception("Output folder does not exist");
            }
        }

        private void CopyDirectoryFilesToNewDirectory(string startDirectory, string newDirectory)
        {
            Directory.GetFiles(startDirectory).ToList().ForEach(f => File.Copy(f, Path.Combine(newDirectory, Path.GetFileName(f)), true));
        }

        private List<Inventor.Document> GetRootComponentDocuments(Inventor.Document doc)
        {
            IEnumerable<Inventor.ComponentOccurrence> compOccs = null;

            if (doc.DocumentType == DocumentTypeEnum.kAssemblyDocumentObject)
            {
                AssemblyDocument assy = (AssemblyDocument)doc;

                compOccs = assy.GetAllOccurrences();
            }

            if (compOccs != null && compOccs.Count() > 0)
                return compOccs.Select(c => (Inventor.Document)c.Definition.Document).ToList();
            else
                return new List<Inventor.Document>();
        }

        private void ActivateProject(Application inventorApp, string projectFilePath)
        {
            var count = inventorApp.Documents.Count;

            try
            {
                _logger.LogInformation($"Open file count = {count}");
                for (int i = 1; i <= count; i++)
                {
                    var doc = inventorApp.Documents[i];
                    _logger.LogInformation($"Closing  {doc.FullFileName}");
                    doc.Close(true);
                    if (inventorApp.Documents.Count == 0)
                    {
                        _logger.LogInformation($"All files closed");
                        break;
                    }
                }
            }
            catch (Exception)
            {

            }
            _logger.LogInformation($"Setting project file to {projectFilePath}");
            inventorApp.ActivateProject(_settings.VaultProjectFilePath);
        }
        #endregion
    }
}
