﻿using System;

namespace StorageConfigurator.Jobs
{
    public static class ParameterNames
    {
        public const string DrawingNumber = "DrawingNumber";
        public const string ProjectName = "ProjectName";
        public const string LineItemName = "LineItemName";
        public const string OccurrenceJson = "OccurrenceJson";
        public const string ExtraData = "ExtraData";
    }
}
