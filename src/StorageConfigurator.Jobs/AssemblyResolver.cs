﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace StorageConfigurator.Jobs
{
    public class AssemblyResolver
    {
        private readonly bool _useConfig;
        //private List<BindingRedirect> _redirects;
        private List<string> _attemptedRedirects = new List<string>();
        private bool _enabled = false;

        public AssemblyResolver()
        {
            var callingAssemblyPath = System.Reflection.Assembly.GetCallingAssembly().Location;
            var callingAssemblyConfigPath = Path.Combine(callingAssemblyPath, ".config");

            _useConfig = File.Exists(callingAssemblyConfigPath);
        }

        public void Enable()
        {
            if (!_enabled)
            {
                AppDomain.CurrentDomain.AssemblyResolve += this.ResolveHandler;
            }
        }

        public void Disable()
        {
            if (!_enabled)
            {
                AppDomain.CurrentDomain.AssemblyResolve -= this.ResolveHandler;
            }
        }

        private Assembly ResolveHandler(object sender, ResolveEventArgs args)
        {
            //if (_useConfig && _redirects == null)
            //{

            //}

            var shortName = args.Name.Split(',').FirstOrDefault();

            if (_attemptedRedirects.Contains(shortName))
            {
                return null;
            }
            else
            {
                _attemptedRedirects.Add(shortName);
                if (shortName.EndsWith(".resources"))
                {
                    return null;
                }
                else
                {
                    return Assembly.Load(shortName);
                }
            }
        }

        //private List<BindingRedirect> GetRedirectsFromConfig(string configPath)
        //{
        //    var configDoc = XDocument.Load(configPath);
        //    XNamespace ns = "urn:schemas-microsoft-com:asm.v1";
        //    var dependentAssys = configDoc.Descendants(ns + "dependentAssembly");

        //    _redirects.AddRange(dependentAssys.Select(da =>
        //    {
        //        var assyId = da.Element(ns + "assemblyIdentity");
        //        var bindingRedirect = da.Element(ns + "bindingRedirect");
        //        var oldVersionMatch = Regex.Match(bindingRedirect.Attribute("oldVersion").Value, @"(\d\.\d\.\d\.\d)-(\d\.\d\.\d\.\d)");

        //        return new BindingRedirect
        //        {
        //            AssemblyName = assyId.Attribute("name").Value,
        //            PublicKeyToken = assyId.Attribute("publicKeyToken").Value,
        //            OldVersionMin = oldVersionMatch.Groups[1].Value,
        //            OldVersionMax = oldVersionMatch.Groups[2].Value,
        //            NewVersion = bindingRedirect.Attribute("newVersion").Value
        //        };
        //    }));

        //}

        //private class BindingRedirect
        //{
        //    public string AssemblyName { get; set; }
        //    public string PublicKeyToken { get; set; }
        //    public string OldVersionMin { get; set; }
        //    public string OldVersionMax { get; set; }
        //    public string NewVersion { get; set; }
        //}
    }
}
