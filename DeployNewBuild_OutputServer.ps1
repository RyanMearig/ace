[CMDLetBinding()]
param
(
[Parameter(Position=0, mandatory=$true)]
[string] $BuildZipPath
)

$BuildZipPath = $BuildZipPath.Replace("""","")
$BuildZipPath = Resolve-Path -Path $BuildZipPath

if ([System.IO.File]::Exists($BuildZipPath) -eq $false)
{
    Throw ("ERROR: " + $BuildZipPath +" does not exist.")
}

$extn = [System.IO.Path]::GetExtension($BuildZipPath)

if ($extn -ne ".zip")
{
    Throw $extn
    Throw ("ERROR: " + $zipFile.FullName + " must be .ZIP format.")
}

$appPath = "C:\D3\StorageConfiguratorJobProcessor\"
$appSettings = "appsettings.json"
$configExe = "StorageSystemJobProcessor.exe.config"

Move-Item -Path ($appPath + $appSettings) -Destination ($appPath + "logs\" + $appSettings) -Force
Move-Item -Path ($appPath + $configExe) -Destination ($appPath + "logs\" + $configExe) -Force

Get-ChildItem -Path $appPath -force -Exclude "logs" | Remove-Item -Recurse -Force

[System.Reflection.Assembly]::LoadWithPartialName('System.IO.Compression.FileSystem')
[System.IO.Compression.ZipFile]::ExtractToDirectory($BuildZipPath, $appPath)

Move-Item -Path ($appPath + "logs\" + $appSettings) -Destination ($appPath + $appSettings) -Force
Move-Item -Path ($appPath + "logs\" + $configExe) -Destination ($appPath + $configExe) -Force