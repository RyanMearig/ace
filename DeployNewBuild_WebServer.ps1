[CMDLetBinding()]
param
(
[Parameter(Position=0, mandatory=$true)]
[string] $BuildZipPath,
[bool]$RestartAppPool = $true
)

$BuildZipPath = $BuildZipPath.Replace("""","")
$BuildZipPath = Resolve-Path -Path $BuildZipPath

if ([System.IO.File]::Exists($BuildZipPath) -eq $false)
{
    Throw ("ERROR: " + $BuildZipPath +" does not exist.")
}

$extn = [System.IO.Path]::GetExtension($BuildZipPath)

if ($extn -ne ".zip")
{
    Throw $extn
    Throw ("ERROR: " + $zipFile.FullName + " must be .ZIP format.")
}

$appPoolName = "StorageConfigurator"

$appPoolState = (Get-WebAppPoolState -Name $appPoolName).Value;
if ($appPoolState -eq "Started") {Stop-WebAppPool -Name $appPoolName}

$appPath = "C:\inetpub\wwwroot\" + $appPoolName + "\"

Get-ChildItem -Path $appPath -force -Exclude "logs" | Remove-Item -Recurse -Force

[System.Reflection.Assembly]::LoadWithPartialName('System.IO.Compression.FileSystem')
[System.IO.Compression.ZipFile]::ExtractToDirectory($BuildZipPath, $appPath)

if ($RestartAppPool -eq $true) {Start-WebAppPool -Name $appPoolName} 